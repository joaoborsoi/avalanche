<?php

require_once('AV_DBOperationObj.inc');

class CEP_LoadAddress extends AV_DBOperationObj
{
  
  //--Method: LoadFieldsDef
  //--Desc: Returns fields definition for insert operation
  function LoadFieldsDef()
  { 
    $this->fieldsDef[0]['fieldName'] = 'cep';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;
  }


  function Execute()
  { 
    $SQL = 'SELECT cidade.uf_codigo, cidade.cidade_codigo, bairro_descricao';
    $SQL .= ', endereco_logradouro FROM endereco, bairro, localidade, cidade';
    $SQL .= " WHERE endereco_cep='".$this->fields['cep']."'";
    $SQL .= ' AND endereco.bairro_codigo=bairro.bairro_codigo';
    $SQL .= ' AND bairro.localidade_codigo=localidade.localidade_codigo';
    $SQL .= ' AND localidade.cidade_codigo=cidade.cidade_codigo';
    $this->attrs = & $this->dbDriver->GetRow($SQL);
    if($this->attrs == NULL)
      throw new AV_Exception(NOT_FOUND,$this->pageBuilder->lang);
  } 

}

?>