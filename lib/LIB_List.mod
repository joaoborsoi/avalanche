<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Document.inc');


class LIB_List extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'path';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['default'] = '/';
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'exclusionFilters';
    $this->fieldsDef[1]['list'] = true;
    $this->fieldsDef[1]['sizeVarName'] = 'exclusionFilters_numOfOptions';
    $subfieldDef = array();
    $subfieldDef[0]['fieldName'] = 'fieldName';
    $subfieldDef[0]['required'] = true;
    $subfieldDef[0]['allowNull'] = false;
    $subfieldDef[1]['fieldName'] = 'value';
    $subfieldDef[1]['required'] = false;
    $subfieldDef[1]['allowNull'] = true;
    $subfieldDef[2]['fieldName'] = 'operator';
    $subfieldDef[2]['required'] = false;
    $subfieldDef[2]['allowNull'] = true;
    $subfieldDef[2]['default'] = '=';
    $this->fieldsDef[1]['fieldsDef'] = $subfieldDef;

  }

  protected function &CreateDocument()
  {
    return new LIB_Document($this);
  }

  protected function Execute()
  {
    $this->isArray = true;

    $exclusionFilters = NULL;
    foreach($this->fields['exclusionFilters'] as &$field)
      $exclusionFilters[$field['fieldName']][] = $field;
    $this->fields['exclusionFilters'] = $exclusionFilters;

    $doc = & $this->CreateDocument();
    $rows = & $doc->DocList($this->fields);

    // load children
    $this->LoadChildrenFromRows($rows, 'doc');
  }
}

?>
