<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_ChangeMemberEmail extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'email';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;
    $this->fieldsDef[0]['consistType'] = 'e-mail';
  }

  protected function Execute()
  {
    $user = new UM_User($this);
    $this->attrs['id'] = $user->ChangeMemberEmail($this->fields['email']);
  }
}

?>