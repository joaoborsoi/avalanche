<?php

require_once('LIB_Search.mod');
require_once('WIKI_Document.inc');

class WIKI_Search extends LIB_Search
{
  // redefine parent function to use own document class
  protected function &CreateDocument()
  {
    return new WIKI_Document($this);
  }
}