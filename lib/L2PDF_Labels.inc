<?php

require_once('L2PDF_Document.inc');

class L2PDF_Labels extends L2PDF_Document
{
  protected $module;
  protected $dbDriver;

  function __construct(& $module)
  {
    parent::__construct($module->pageBuilder);
    $this->module = & $module;
    $this->dbDriver = & $module->dbDriver;
  }

  function Init($labelsId)
  {
    // retrieves labels definitions
    $SQL = 'SELECT l.*,p.type FROM L2PDF_Labels l, L2PDF_PaperSize p ';
    $SQL .= "WHERE l.docId='$labelsId' AND l.paperSize=p.id ";
    $labelDef = $this->dbDriver->GetRow($SQL);
    if($labelDef==NULL)
      throw new AV_Exception(NOT_FOUND,$this->lang);
    
    // initiate labels document
    $this->Write('\documentclass['.$labelDef['type'].']{article}');
    $this->Write('\usepackage[psamsfonts]{amsfonts}');
    $this->Write('\usepackage[dvips]{graphicx}');
    $this->Write('\usepackage[portuges]{babel}');
    $this->Write('\usepackage[utf8]{inputenc}');
    $this->Write('\usepackage[T1]{fontenc}');
    $this->Write('\usepackage{ae,aecompl}');
    $this->Write('\usepackage[newdimens]{labels}');
    $this->Write('\LabelCols='.$labelDef['labelCols']);
    $this->Write('\LabelRows='.$labelDef['labelRows']);
    $this->Write('\LeftPageMargin='.$labelDef['leftPageMargin'].'mm');
    $this->Write('\RightPageMargin='.$labelDef['rightPageMargin'].'mm');
    $this->Write('\TopPageMargin='.$labelDef['topPageMargin'].'mm');
    $this->Write('\BottomPageMargin='.$labelDef['bottomPageMargin'].'mm');
    $this->Write('\InterLabelColumn='.$labelDef['interLabelColumn'].'mm');
    $this->Write('\InterLabelRow='.$labelDef['interLabelRow'].'mm');
    $this->Write('\LeftLabelBorder='.$labelDef['leftLabelBorder'].'mm');
    $this->Write('\RightLabelBorder='.$labelDef['rightLabelBorder'].'mm');
    $this->Write('\TopLabelBorder='.$labelDef['topLabelBorder'].'mm');
    $this->Write('\BottomLabelBorder='.$labelDef['bottomLabelBorder'].'mm');
    $this->Write('\begin{document}');
  }

  function End()
  {
    $this->Write('\end{document}');
  }
}

?>