<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_PDFMail.inc');

class LEGIS_GetPDFMail extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'params';
    $this->fieldsDef[0]['stripSlashes'] = true; 
    $this->fieldsDef[0]['trimField'] = false; 
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $acc = new LEGIS_PDFMail($this);
    $this->attrs['docId'] = $acc->Get($this->fields['params']);
  }
}

?>