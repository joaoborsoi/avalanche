<?php

require_once('AV_Exception.inc');

class IM_Manager
{
  protected $pageBuilder;
  protected $dbDriver;
  protected $lang;

  function __construct(& $module)
  {
    $this->pageBuilder = & $module->pageBuilder;
    $this->lang = $pageBuilder->lang;
    $this->dbDriver = & $module->dbDriver;
  }

  function GetUserTalks($fields)
  {
    $sessionH = & $this->pageBuilder->sessionH;
    if($sessionH->isGuest())
      throw new AV_Exception(PERMISSION_DENIED,$this->lang);

    $sessionId = $sessionH->sessionID;

    $SQL = 'SELECT talk.id, GROUP_CONCAT(fotoId) as fotoList, ';
    $SQL .= 'GROUP_CONCAT(userName) as userNameList, ';
    $SQL .= 'GROUP_CONCAT(ouser.userId) as userIdList, utalk.unread ';
    $SQL .= 'FROM IM_Talk talk, IM_TalkUser utalk, IM_TalkUser outalk, UM_User ouser, ';
    $SQL .= 'UM_UserSessions us ';
    if($fields['groupId']!=NULL)
      $SQL .= ', UM_UserGroup oug ';
    $SQL .= 'WHERE talk.id = utalk.id AND utalk.userId = us.userId ';
    $SQL .= "AND sessionId='$sessionId' ";
    $SQL .= 'AND talk.id=outalk.id AND outalk.userId <> utalk.userId ';
    $SQL .= 'AND outalk.userId=ouser.userId ';
    if($fields['groupId']!=NULL)
      $SQL .= "AND ouser.userId=oug.userId AND oug.groupId='".$fields['groupId']."' ";
    if($fields['excludeGroups']!=NULL)
    {
      $SQL .= 'AND ouser.userId NOT IN (SELECT DISTINCT userId FROM UM_UserGroup WHERE ';
      $SQL .= "groupId IN (".$fields['excludeGroups'].") ) ";
    }
    $SQL .= "AND ouser.status='1' ";
    
    $SQL .= 'GROUP BY talk.id ';
    $SQL .= 'ORDER BY outalk.lastMsgId DESC ';
    $limit=$this->pageBuilder->siteConfig->getVar("chat","maxChatRead");
    $SQL .= "LIMIT $limit ";
    return $this->dbDriver->GetAll($SQL);
  }

  function GetIndividualTalk($fields)
  {
    $sessionH = & $this->pageBuilder->sessionH;
    if($sessionH->isGuest())
      throw new AV_Exception(PERMISSION_DENIED,$this->lang);

    $sessionId = $sessionH->sessionID;
    $SQL = "SELECT userId FROM UM_UserSessions WHERE sessionId='$sessionId' ";
    $curUserId = $this->dbDriver->GetOne($SQL);

    // search for talk
    $SQL = 'SELECT u1.id FROM IM_TalkUser u1, IM_TalkUser u2, IM_Talk t ';
    $SQL .= "WHERE u1.userId = '$curUserId' ";
    $SQL .= "AND u2.userId = '".$fields['userId']."' ";
    $SQL .= 'AND u1.id = u2.id ';
    $SQL .= 'AND u1.id=t.id ';
    $SQL .= "AND individual='1' ";
    $SQL .= 'FOR UPDATE ';
    $talkId = $this->dbDriver->GetOne($SQL);

    if($talkId == NULL)
    {
      $SQL = "INSERT INTO IM_Talk (id, individual) VALUES (NULL,'1')";
      $this->dbDriver->ExecSQL($SQL);

      $talkId = $this->dbDriver->GetOne('SELECT LAST_INSERT_ID()');

      $SQL = 'INSERT INTO IM_TalkUser (id, userId) VALUES ';
      $SQL .= "('$talkId','".$fields['userId']."'),('$talkId','$curUserId')";
      $this->dbDriver->ExecSQL($SQL);
    }
    return $talkId;
  }

  function GetTalkMessages($fields)
  {
    $sessionH = & $this->pageBuilder->sessionH;
    if($sessionH->isGuest())
      throw new AV_Exception(PERMISSION_DENIED,$this->lang);

    $sessionId = $sessionH->sessionID;
    $SQL = "SELECT userId FROM UM_UserSessions WHERE sessionId='$sessionId' ";
    $userId = $this->dbDriver->GetOne($SQL);

    $SQL = 'SELECT unread FROM IM_TalkUser ';
    $SQL .= "WHERE userId='$userId' AND id='".$fields['talkId']."' ";
    $SQL .= 'FOR UPDATE ';
    $unread = $this->dbDriver->GetOne($SQL);

    $limit=$this->pageBuilder->siteConfig->getVar("chat","maxChatRead");

    $SQL = 'SELECT id, m.userId, login, fotoId, ';
    $SQL .= 'UNIX_TIMESTAMP(pubDate) as pubDate, message ';
    $SQL .= 'FROM IM_Message m, UM_User u ';
    $SQL .= "WHERE talkId='".$fields['talkId']."' ";
    if($fields['lastId']!=NULL)
      $SQL .= "AND id > '".$fields['lastId']." '";
    $SQL .= 'AND m.userId = u.userId ';
    $SQL .= 'ORDER BY id DESC ';
    if($fields['lastId']==NULL)
      $SQL .= 'LIMIT '.$limit;
    $rows = $this->dbDriver->GetAll($SQL);

    if($unread!=0)
    {
      $SQL = 'UPDATE IM_TalkUser SET unread=0 ';
      $SQL .= "WHERE userId='$userId' AND id='".$fields['talkId']."' ";
      $this->dbDriver->ExecSQL($SQL);
    }

    foreach($rows as &$row)
      $row['pubDate'] = strftime($fields['dateFormat'],(int)$row['pubDate'] );
    return $rows;
  }

  function SendMessage($fields)
  {
    $sessionH = & $this->pageBuilder->sessionH;
    if($sessionH->isGuest())
      throw new AV_Exception(PERMISSION_DENIED,$this->lang);

    $sessionId = $sessionH->sessionID;
    $SQL = "SELECT userId FROM UM_UserSessions WHERE sessionId='$sessionId' ";
    $userId = $this->dbDriver->GetOne($SQL);

    $SQL = "INSERT INTO IM_Message (talkId,userId,pubDate,message) VALUES ";
    $SQL .= "('".$fields['talkId']."','$userId', NOW(), ";
    $SQL .= "'".$fields['message']."') ";
    $this->dbDriver->ExecSQL($SQL);

    $SQL = 'UPDATE IM_TalkUser SET unread=unread+1, lastMsgId=LAST_INSERT_ID() ';
    $SQL .= "WHERE userId<>'$userId' AND id='".$fields['talkId']."' ";
    $this->dbDriver->ExecSQL($SQL);
  }
}

?>