<?php

require_once('LIB_Document.inc');

class LEGIS_History extends LIB_Document
{
  function LoadFieldsDef($docId, $path)
  {
    $fieldsDef = parent::LoadFieldsDef($docId,$path);

    $i = count($fieldsDef);
    $fieldsDef[$i]['fieldName'] = 'sendToAggregators';
    $fieldsDef[$i]['consistType'] = 'boolean';
    $fieldsDef[$i]['required'] = '0';
    $fieldsDef[$i]['allowNull'] = '1';
    return $fieldsDef;
  }

  function &GetForm($keyFields, $processId, $path, $fieldName, $sourceId, & $templates, 
		    & $formObject, $defValues = array(),$dateFormat = NULL,
		    $numberFormat = true, $short = false)
  {
    parent::GetForm($keyFields, $path, $fieldName, $sourceId, $templates, 
		    $formObject, $defValues,$dateFormat,
		    $numberFormat, $short);

    if($keyFields!=NULL)
      $docId = $keyFields['docId'];
    
    // recupera nro de agregadores
    $SQL = 'SELECT COUNT(*) FROM ';
    if($docId != NULL)
      $SQL .= 'LEGIS_History h,';
    $SQL .= 'LEGIS_ProcessClient pc,EM_LegalEntity l WHERE ';
    if($docId != NULL)
      $SQL .= "h.docId='$docId' AND h.processId=pc.processId ";
    else
      $SQL .= "pc.processId='$processId' ";
    $SQL .= 'AND pc.entityId=l.docId AND aggregator IN (1,2) ';
    $numOfAggregators = $this->dbDriver->GetOne($SQL);
    if($numOfAggregators>0) 
    {
      $sendToAggregator = new AV_ModObject('field',$this->module->pageBuilder);
      $sendToAggregator->attrs['fieldName'] = 'sendToAggregators';
      $sendToAggregator->attrs['type'] = 'boolean';
      $sendToAggregator->attrs['label'] = 'Notifica agregadores';
      $sendToAggregator->attrs['orderby'] = 45;
      $formObject->children['45'] = $sendToAggregator;
    }
    ksort($formObject->children);

    return $formObject;
  }

  function Insert($fields)
  {
    $docId = parent::Insert($fields);

    $this->NotifyAggregators($fields);

    return $docId;
  }

  function Set($fields)
  {
    parent::Set($fields);

    $this->NotifyAggregators($fields);
  }

  function NotifyAggregators(& $fields)
  {
    // envia e-mail de notificação para agregadores
    if(!$fields['sendToAggregators'])
      return;

      // carrega informações de agregadores
    $SQL = 'SELECT aggregator,l.docId, email FROM ';
    if($fields['docId'] != NULL)
      $SQL .= 'LEGIS_History h,';
    $SQL .= 'LEGIS_ProcessClient pc,EM_LegalEntity l WHERE ';
    if($fields['docId'] != NULL)
    {
      $SQL .= "h.docId='".$fields['docId']."' ";
      $SQL .= 'AND h.processId=pc.processId ';
    }
    else
      $SQL .= "pc.processId='".$fields['processId']."' ";
    $SQL .= 'AND pc.entityId=l.docId AND aggregator IN (1,2) ';
    $aggregatorList = $this->dbDriver->GetAll($SQL);

    // carrega informações do processo
    $SQL = "SELECT GROUP_CONCAT(DISTINCT number SEPARATOR ',') as number,";
    $SQL .= "folder, folderYear ";
    $SQL .= "FROM LEGIS_ProcessNumber pn,LEGIS_Process p,LEGIS_RootProcess rp";
    if($fields['docId'] != NULL)
    {
      $SQL .= ',LEGIS_History h ';
      $SQL .= "WHERE h.docId='".$fields['docId']."' ";
      $SQL .= 'AND h.processId=docId ';
    }
    else
      $SQL .= " WHERE docId='".$fields['processId']."' ";
    $SQL .= 'AND docId=p.processId AND rootProcessId=rp.processId ';
    $SQL .= 'GROUP BY p.processId';
    $process = $this->dbDriver->GetRow($SQL);

    $to = array();
    foreach($aggregatorList as $child)
    {
      if($child['email'] != NULL)
	$to[] = $child['email'];
    }

    if(count($to)>0)
    {
      $email=$this->module->pageBuilder->siteConfig->getVar("config", 
							    "mailFrom");
      $headers = "From: Câmara Advogados <$email>\r\n";
      $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";
      $headers .= "Content-Transfer-Encoding: quoted-printable \r\n";
      $subject = '[Notificação Câmara] '.$fields['subject'];
      $message = "<!DOCTYPE html PUBLIC \"-";
      $message .= "//W3C//DTD XHTML 1.0 Transitional//EN\" ";
      $message .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
      $message .= '<html><body>';
      $message .= "<p><b>Número do processo:</b> " .$process['number'] . "</p>";
      $message .= "<p><b>Pasta:</b> " . $process['folder'];
      if($process['folderYear'] != NULL)
	$message .= '/' . $process['folderYear'];
      $message .= "</p>";
      $fields['date'] = explode('-', $fields['date']);
      $message .= '<p><b>Data:</b> '.$fields['date'][2].'/'.$fields['date'][1];
      $message .= '/'.$fields['date'][0]."</p>";
      $message .= "<p><b>Assunto:</b> ".htmlentities_utf($fields['subject'])."</p>";
      $message .= "<p><b>Histórico:</b></p>" .$fields['description'];
      $message .= '</body></html>';


      // Se for Postfix
      $mail_to = implode(',',$to);
      if(!mail($mail_to, $subject, $message, $headers ,"-r".$email))
      { 
	// Se "não for Postfix"
	$headers .= "Return-Path: " . $email . "\n"; 
	if(!mail($mail_to, $subject, $message, $headers))
	  throw new AV_Exception(FAILED,$this->lang);
      }
    }
  }

}

?>