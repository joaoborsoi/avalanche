<?php

require_once('AV_DBOperationObj.inc');
require_once('IM_Manager.inc');

class IM_GetUserTalks extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'groupId';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'excludeGroups';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
  }

  protected function Execute()
  {
    $this->isArray = true;

    $im = new IM_Manager($this);

    // get rows
    $this->attrs['talks'] = $im->GetUserTalks($this->fields);
  }
}

?>