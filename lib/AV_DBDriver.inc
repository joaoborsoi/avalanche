<?php

require_once('AV_Exception.inc');

//--Class: AV_DBDriver
//--Desc: Implements abstract avalanche database driver class
abstract class AV_DBDriver
{

  public $type;

  //--Attributes
  protected $pageBuilder;
  protected $logDBOper;       //--Desc: Flag to indicate whether to print
                              //--Desc  log informations on all DB operations
                   

  //--Method: AV_DBDriver
  //--Desc: Constructor
  function __construct(& $pageBuilder)
  {
    $this->pageBuilder = & $pageBuilder;
    $this->logDBOper = $this->pageBuilder->siteConfig->getVar("debug", 
							      "logDBOper");
    $this->type = $this->pageBuilder->siteConfig->getVar("db","dbType");

  }

  abstract function ExecSQL($SQL, $reportError = true);

  //--Method: LockTables
  //--Desc: Lock tables
  function LockTables($tables)
  {
    $SQL = 'LOCK TABLES ';

    $first = true;

    foreach($tables as $table)
    {
      if($first)
	$first = false;
      else
	$SQL .= ', ';

      $SQL .= $table;
    }

    $this->ExecSQL($SQL);
  }

  function ExecBatch($commands, &$status)
  {
    $commands = explode(';', $commands);
    while(count($commands)>0)
    {
      $cmd = trim($commands[0]);
      if($cmd != '')
      {
	try
	{
	  $this->ExecSQL($commands[0]);
	}
	catch(Exception $e)
	{
	  $status = $e->getCode();
	  return implode(';',$commands); 
	}
      }
      array_shift($commands);
    }
    $status = SUCCESS;
    return NULL;
  }

  function DoTransaction($commands)
  {
    $key = ftok('AV_DBDriver.inc', 'V');
    $sem = sem_get($key);
    if(!$sem)
      throw new AV_Exception(FAILED,$this->pageBuilder->lang);

    if(!sem_acquire($sem))
      throw new AV_Exception(FAILED,$this->pageBuilder->lang);

    $this->ExecSQL("COMMIT");

    $tmpDir = $this->pageBuilder->siteConfig->getVar("dirs", "tmp");
    $fileName = $tmpDir.'/transaction';
    if(is_file($fileName))
    {
      sem_release($sem);
      throw new AV_Exception(FAILED,$this->pageBuilder->lang);
    }

    $commands = $this->ExecBatch($commands, $status);
    if($status != SUCCESS)
    {
      $handle = fopen($fileName, 'w');
      if(!$handle)
      {
	sem_release($sem);
	throw new AV_Exception(FAILED,$this->pageBuilder->lang);
      }
      fwrite($handle, $commands); 
      fclose($handle);
      sem_release($sem);
      throw new AV_Exception($status,$this->pageBuilder->lang);
    }

    sem_release($sem);
  }

  function ResumeTransaction()
  {
    $key = ftok('AV_DBDriver.inc', 'V');
    $sem = sem_get($key);
    if(!$sem)
      throw new AV_Exception(FAILED,$this->pageBuilder->lang);

    if(!sem_acquire($sem))
      throw new AV_Exception(FAILED,$this->pageBuilder->lang);

    $tmpDir = $this->pageBuilder->siteConfig->getVar("dirs", "tmp");
    $fileName = $tmpDir.'/transaction';
    if(is_file($fileName))
    {
      $handle = fopen($fileName, 'r');
      if(!$handle)
      {
	sem_release($sem);
	throw new AV_Exception(FAILED,$this->pageBuilder->lang);
      }

      $commands = fread($handle, filesize($fileName)); 
      fclose($handle);

      $commands = $this->ExecBatch($commands, $status);
      if($status != SUCCESS)
      {
	$handle = fopen($fileName, 'w');
	if(!$handle)
	{
	  sem_release($sem);
	  throw new AV_Exception(FAILED,$this->pageBuilder->lang);
	}
	fwrite($handle, $commands); 
	fclose($handle);
	sem_release($sem);
	throw new AV_Exception($status,$this->pageBuilder->lang);
      }

      unlink($fileName);
    }

    sem_release($sem);
  }


}

?>
