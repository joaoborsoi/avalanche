<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_DelGroup extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'groupId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;

    $this->fieldsDef[1]['fieldName'] = 'path';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['default'] = '/';
    $this->fieldsDef[1]['stripSlashes'] = false;
  }

  protected function Execute()
  {
    $groupMan = new UM_Node('UM_Group',$this);
    $groupMan->LoadFieldsDef();
    $groupMan->Del($this->fields);
  }
}

?>