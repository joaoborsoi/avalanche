<?php

require_once('ADMIN_Page.inc');

class ADMIN_SaveFile extends ADMIN_Page
{
  var $rootPath;

  function __construct(& $pageBuilder, $rootPath = '')
  {
    if(mb_substr($rootPath,-1) == '/')
      $rootPath = mb_substr($rootPath,0,-1);
    $this->rootPath = $rootPath;
    parent::__construct($pageBuilder);    
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminScript');
    try
    {
      if($_REQUEST['docId'] == NULL)
      {
	$_GET['path'] = $this->rootPath . $_REQUEST['path'];
	$saveFile = $this->LoadModule('LIB_InsDoc');
      }
      else
	$saveFile = $this->LoadModule('LIB_SetDoc');

      $script .= 'if(parent.opener) ';
      if($_REQUEST['docId'] == NULL || !$_REQUEST['updateImage'])
	$script .= 'parent.opener.ReloadFiles();';
      else
      {
	$title = $_REQUEST['docId'];
	for($i=1; $i<=$_REQUEST['title_numOfOptions'];$i++)
	  if($_REQUEST['title_'.$i.'__lang']==$this->lang)
	    $title = $_REQUEST['title_'.$i.'__value'];
	$script .= "parent.opener.UpdateImage('".$_REQUEST['imageListField']."',";
	$script .= $_REQUEST['docId'].",'$title','";
	$script .= $_FILES['content']['type']."');";
      }
      $script .= 'parent.close();';


    }
    catch(Exception $e)
    {
      $script .= "alert('".$e->getMessage()."');";
    }    

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }
}

?>