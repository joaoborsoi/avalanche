<?php

require_once('AV_DBOperationObj.inc');

class GAME_LoadArea extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'id';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $SQL = 'SELECT e.*, el.value as title,elementClass,name as type ';
    $SQL .= 'FROM GAME_Element e, LANG_Label el, GAME_ElementType it ';
    $SQL .= 'WHERE typeId = it.id ';
    $SQL .= "AND e.id='".$this->fields['id']."'";
    $SQL .= 'AND e.title=el.labelId ';
    $SQL .= "AND el.lang='".$this->pageBuilder->lang."' ";
    $this->attrs = $this->dbDriver->GetRow($SQL);
  }
}

?>