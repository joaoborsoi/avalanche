<?php

require_once('LIB_Search.mod');
require_once('LEGIS_Process.inc');

class LEGIS_SearchProcess extends LIB_Search
{
  protected function &CreateDocument()
  {
    return new LEGIS_Process($this);
  }
}

?>
