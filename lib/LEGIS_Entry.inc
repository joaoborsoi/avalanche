<?php

require_once('FORM_DynTable.inc');
require_once('LEGIS_Process.inc');

class LEGIS_Entry extends FORM_DynTable
{
  function Insert(& $fields, $extraFields = array())
  {
    if($fields['providerId'] != NULL)
      $fields['providerId'] = $fields['providerId']['docId'];
    if($fields['processId'] != NULL)
      $fields['processId'] = $fields['processId']['docId'];
    if($fields['customerId'] != NULL)
      $fields['customerId'] = $fields['customerId']['docId'];

    parent::Insert($fields,$extraFields);
  }

  function Set($fields, $extraFields = array())
  {
    if($fields['providerId'] != NULL)
      $fields['providerId'] = $fields['providerId']['docId'];
    if($fields['processId'] != NULL)
      $fields['processId'] = $fields['processId']['docId'];
    if($fields['customerId'] != NULL)
      $fields['customerId'] = $fields['customerId']['docId'];

    parent::Set($fields,$extraFields);

    return $this->oldValues;
  }


  protected function ParseFieldDef(& $fieldDef,$disableFileRequired,$disableRequired)
  {
    parent::ParseFieldDef($fieldDef,$disableFileRequired,$disableRequired);

    switch($fieldDef['fieldName'])
    {
    case 'providerId':
    case 'processId':
    case 'customerId':
      // gambiarra pra evitar addslashes
      $fieldDef['stripSlashes'] = true;
    break;
    }
  }

  protected function &GetField(& $fieldDef, & $values, $keyFields,
  			       & $templates, $sourceId, $dateFormat = NULL,
  			       $numberFormat = true, $fieldName = NULL)
  {
    switch ($fieldDef['fieldName'])
    {
    case 'providerId':
      return $this->GetProviderRef($fieldDef, $values);
    case 'customerId':
      return $this->GetCustomerRef($fieldDef, $values);
    case 'processId':
      return $this->GetProcessRef($fieldDef, $values);

    default:
      return parent::GetField($fieldDef,$values,$keyFields,$templates,
  			      $sourceId,$dateFormat,$numberFormat);
     }
  }

  protected function &GetCustomerRef($fieldDef,$values)
  {
    $modobj =  new AV_ModObject('field', $this->module->pageBuilder);
    $modobj->attrs = $fieldDef;

    // retorna clientes pessoa física
    if($values[$fieldDef['fieldName']]!=NULL)
    {
      $docId = $values[$fieldDef['fieldName']];
      $SQL .= "SELECT name ";
      $SQL .= "FROM EM_NaturalPerson np ";
      $SQL .= "WHERE docId='$docId' ";
      
      $value['docId'] = $docId;
      $value['name'] = $this->dbDriver->GetOne($SQL);

      if($value['name']!=NULL)
      {
	$SQL = 'SELECT s.situationId, s.name ';
	$SQL .= 'FROM LEGIS_CustomerSituation cs, LEGIS_PersonSituation s ';
	$SQL .= "WHERE cs.docId='$docId' AND cs.situationId=s.situationId ";
	$value['situationId'] = $this->dbDriver->GetAll($SQL);
      }
      else
      {
	$SQL = "SELECT name FROM EM_LegalEntity WHERE docId='$docId'";
	$value['name'] = $this->dbDriver->GetOne($SQL);
      }
      $modobj->attrs['value'] = $value;
    }
    return $modobj;
  }

  protected function &GetProviderRef($fieldDef,$values)
  {
    $modobj =  new AV_ModObject('field', $this->module->pageBuilder);
    $modobj->attrs = $fieldDef;

    // retorna clientes pessoa física
    if($values[$fieldDef['fieldName']]!=NULL)
    {
      $docId = $values[$fieldDef['fieldName']];
      $SQL .= "(SELECT name ";
      $SQL .= "FROM LEGIS_NPProvider np ";
      $SQL .= "WHERE docId='$docId') ";
      $SQL .= "UNION (SELECT name ";
      $SQL .= "FROM LEGIS_LEProvider le ";
      $SQL .= "WHERE docId='$docId') ";
      
      $value['docId'] = $docId;
      $value['name'] = $this->dbDriver->GetOne($SQL);

      $modobj->attrs['value'] = $value;
    }
    return $modobj;
  }

  protected function &GetProcessRef($fieldDef,$values)
  {
    $modobj =  new AV_ModObject('field', $this->module->pageBuilder);
    $modobj->attrs = $fieldDef;

    // retorna clientes pessoa física
    if($values[$fieldDef['fieldName']]!=NULL)
    {
      $processId = $values[$fieldDef['fieldName']];
      
      $SQL = 'SELECT p.processId,p.rootProcessId,rp.folder,rp.folderYear,';
      $SQL .= '(SELECT name FROM LEGIS_Situation s ';
      $SQL .= 'WHERE s.docId=p.situationId) AS situation, ';
      $SQL .= '(SELECT s.name FROM LEGIS_Subject s ';
      $SQL .= 'WHERE s.docId=p.subjectId) AS subject ';
      $SQL .= "FROM LEGIS_Process p, LEGIS_RootProcess rp ";
      $SQL .= "WHERE p.processId='$processId' ";
      $SQL .= 'AND p.rootProcessId=rp.processId ';
      
      $value = $this->dbDriver->GetRow($SQL);
      
      $SQL = "SELECT number FROM LEGIS_ProcessNumber WHERE processId='$processId' ";
      $value['number'] = $this->dbDriver->GetCol($SQL);
      
      $process = new LEGIS_Process($this->module);
      $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 
      $keyFields = Array('docId' => $processId);
      $process->GetForm($keyFields,NULL,'/process/', 
			'clientList', NULL, $templates, $formObject,$defValues,
			NULL,false,true);
      $value['clientList'] = $formObject->attrs['clientList'];
      $modobj->attrs['value'] = $value;
    }
    return $modobj;
  }
}

?>