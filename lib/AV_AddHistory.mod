<?php

require_once('AV_History.inc');
require_once('AV_DBOperationObj.inc');

class AV_AddHistory extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'title';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'link';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['stripSlashes'] = false;
  }


  protected function Execute()
  {
    $history = new AV_History($this);

    $history->AddHistory($this->fields['title'], $this->fields['link']);
  }
}

?>