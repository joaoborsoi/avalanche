<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');


class UM_ValidateUser extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $i = 0;
    $this->fieldsDef[$i]['fieldName'] = 'id';
    $this->fieldsDef[$i]['required'] = true;
    $this->fieldsDef[$i]['allowNull'] = false;
  }

  protected function Execute()
  {
    $user = new UM_User($this);
    $user->ValidateUser($this->fields['id']);
  }
}

?>