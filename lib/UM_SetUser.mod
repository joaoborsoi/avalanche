<?php

require_once('UM_UserOper.inc');

class UM_SetUser extends UM_UserOper
{
  protected function ParseInput(&$inputSources)
  {
    $this->disableRequired = true;
    parent::ParseInput($inputSources);
  }

  protected function Execute()
  {
    $this->userMan->Set($this->fields);
  }
}

?>