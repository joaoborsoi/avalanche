<?php

require_once('CMS_Content.inc');

class CMS_Video extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    // dummy
  }

  function LoadArea(&$fields,$path)
  {
    parent::LoadArea($fields,$path);

    // search for files at the folder
    $_GET['path'] = $path;
    $_GET['orderBy'] = 'fixedOrder';
    $_GET['order'] = '0';
    $_GET['returnFields'] = 'title,authors,descr,videoCode';
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsPerPage');
 
    $search = $this->page->LoadModule('LIB_Search');

    // encoded menu path for the links
    $encMenuPath = str_replace("%2F", "/", 
			       urlencode(stripslashes($this->menuPath)));

    if($search->attrs['totalResults']>0)
    {
      $content .= $this->LoadList($search);
      $content .= $this->page->LoadSearchNav($search,$_REQUEST['offset'],
					     $_GET['limit'],
					     'area/'.$encMenuPath);
    }

    $this->pageBuilder->rootTemplate->addText((string)$content,'mainContent');
  }

  function LoadList($search)
  {
    $content .= "<ul class='video-list'>\n";
    foreach($search->children as & $child)
    {
      $content .= "<li>\n";
      $content .= $this->LoadAreaListItem($child->attrs);
      $content .= "</li>\n";
    }
    $content .= "</ul>\n";
    return $content;
  }

  function LoadAreaListItem(& $fields)
  {
    $content .= "<h4>";
    $content .= $fields['title']."</h4>\n";
    if($fields['authors'] != NULL)
    {
      $content .= '<small>'.$fields['authors'].'</small>';
    }
    $content .= $fields['videoCode'];
    if($fields['descr'] != NULL)
    {
      $content .= '<p>'.$fields['descr'].'</p>';
    }
    return $content;
  }

  function LoadListItem(& $fields)
  {
    $content .= "<h3>";
    $content .= $fields['title']."</h3>\n";
    $content .= '<p>'.$fields['authors'].'</p>';
    $content .= $fields['videoCode'];
    return $content;
  }
}

?>
