<?php

require_once('CMS_Content.inc');

class CMS_Logout extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    // dummy
  }

  function LoadArea(&$fields, $path)
  {
    parent::LoadArea($fields,$path);
    $this->pageBuilder->sessionH->attemptLogout();
  }

  function LoadListItem(& $fields)
  {
    // dummy
  }


}
?>