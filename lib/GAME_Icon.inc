<?php

require_once('GAME_Element.inc');

abstract class GAME_Icon extends GAME_Element
{
  abstract function LoadIcon(array &$fields);
}

?>