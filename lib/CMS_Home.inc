<?php

require_once('CMS_Generic.inc');
require_once('CMS_News.inc');
require_once('CMS_Event.inc');

class CMS_Home extends CMS_Generic
{
  protected $news;
  protected $event;

  function LoadArea(&$fields,$path)
  {
    CMS_Content::LoadArea($fields,$path);

    $this->news = new CMS_News($this);
    $this->event = new CMS_Event($this);

    try
    {
      $content = $this->LoadEvents();
      $content .= $this->LoadNews();
    }
    catch(Exception $e)
    {
      $content = "<p>".$e->getMessage()."</p>\n";
     
    }
    $this->pageBuilder->rootTemplate->addText((string)$content,'mainContent');
  }

  protected function LoadEvents()
  {
    // searchs database for latest events
    $eventPath = $this->pageBuilder->siteConfig->getVar('cms', 'eventPath');
    if($eventPath==NULL)
      return '';

    $_GET['path'] = '/content/Menu/' . $eventPath;
    $_GET['orderBy'] = 'eventDate';
    $_GET['order'] = '1';
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
							 'eventDateFormat');
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = 'title,eventDate';
    $_GET['exclusionFilters_numOfOptions'] = 1;
    $_GET['exclusionFilters_1__fieldName'] = 'eventDate';
    $_GET['exclusionFilters_1__value'] = date('Y-m-d H:i:s');
    $_GET['exclusionFilters_1__operator'] = '>=';


    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
						    'eventHomeMaxItems');
    $search = $this->page->LoadModule('LIB_Search');
    if($search->attrs['totalResults'] > 0)
    {
      // loads list to screen
      $content = "<h2 class='subsection'>";
      $content .= $this->pageLabels->attrs['upcomingEventsTag']."</h2>\n";
      $content .= $this->event->LoadList($search);
    }
    return $content;
  }

  protected function LoadNews()
  {

    // searchs database for latest news
    $newsPath = $this->pageBuilder->siteConfig->getVar('cms', 'newsPath');
    if($newsPath==NULL)
      return '';

    $_GET['path'] = '/content/Menu/' . $newsPath;
    $_GET['orderBy'] = 'publicationDate';
    $_GET['order'] = '0';
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
							 'newsDateFormat');
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = 'title,publicationDate';
    $_GET['exclusionFilters_numOfOptions'] = 1;
    $_GET['exclusionFilters_1__fieldName'] = 'publicationDate';
    $_GET['exclusionFilters_1__value'] = date('Y-m-d H:i:s');
    $_GET['exclusionFilters_1__operator'] = '<=';

    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
						    'newsHomeMaxItems');
    $search = $this->page->LoadModule('LIB_Search');
    if($search->attrs['totalResults'] > 0)
    {
      // loads list to screen
      $content = "<h2 class='subsection'>";
      $content .= $this->pageLabels->attrs['latestNewsTag']."</h2>\n";
      $content .= $this->news->LoadList($search);
    }
    return $content;
  }
}

?>