<?php

require_once('AV_DBOperationObj.inc');
require_once('SHOP_Cart.inc');


class SHOP_FlushCart extends AV_DBOperationObj
{
  protected function Execute()
  {
    $cart = new SHOP_Cart($this);
    $cart->FlushCart();
  }
}

?>
