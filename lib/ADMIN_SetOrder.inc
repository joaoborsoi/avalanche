<?php

require_once('ADMIN_Files.inc');

class ADMIN_SetOrder extends ADMIN_Files
{
  function LoadFiles($path)
  {
    try
    {


      $_GET['path'] = $this->rootPath . $path;
      $_GET['folderPath'] = $this->rootPath . $_REQUEST['folderPath'];

      if($_REQUEST['type']=='file')
      {
	$_GET['orderBy'] = 'fixedOrder';
	$getOrderAsc = $this->LoadModule('LIB_GetFolderOrderAsc');
	if(!$getOrderAsc->attrs['orderAsc'])
	  $_GET['up'] = !$_GET['up'];

	$module = $this->LoadModule('LIB_SetDocOrder');
      }
      else
	$module = $this->LoadModule('LIB_SetFolderOrder');
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }    

    if($_REQUEST['type']=='folder')
      $script .= 'parent.ReloadFolders();';

    $script .= parent::LoadFiles($path);
    return $script;
  }
}

?>