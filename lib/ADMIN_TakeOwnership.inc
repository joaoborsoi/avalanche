<?php

require_once('ADMIN_Page.inc');

class ADMIN_TakeOwnership extends ADMIN_Page
{
  function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminScript');

    try
    {
      if($_REQUEST['docId'] != NULL)
	$module = $this->LoadModule('LIB_TakeDocOwnership');
      else
	$module = $this->LoadModule('LIB_TakeFolderOwnership');

      $script .= "parent.document.getElementById('login').innerHTML = \"";
      $script .= addslashes(htmlentities_utf($module->attrs['login'])) . "\";";
    }
    catch(Exception $e)
    {
      $script .= "alert('".$e->getMessage()."');";
    }    
    
    $this->pageBuilder->rootTemplate->addText($script,'script');
  }
}

?>