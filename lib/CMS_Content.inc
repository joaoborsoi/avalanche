<?php

abstract class CMS_Content
{
  protected $menuPath;
  protected $pageBuilder;
  protected $memberData;
  protected $memberGroups;
  protected $lang;
  protected $page;
  protected $pageLabels;

  function __construct(& $page)
  {
    $this->menuPath = & $page->menuPath;
    $this->pageBuilder = & $page->pageBuilder;
    $this->lang = & $page->pageBuilder->lang;
    $this->memberData = & $page->memberData;
    $this->memberGroups = & $page->memberGroups;
    $this->pageLabels = & $page->pageLabels;
    $this->page = & $page;
  }

  // Load Document is not abstract so it can be declared with one or two arguments
  function LoadDocument(& $fields, & $form)
  {
  }

  function LoadArea(&$fields, $path)
  {
    // loads document title
    $title = htmlentities_utf($fields['title']);
    $this->pageBuilder->rootTemplate->addText((string)$title,'title');

    // folder content title
    $content = "<h1>$title</h1>\n";

    // loads image list
    $content .= $this->page->LoadImages($fields['imageList']);

    // folder content
    $content .= $fields['content'];

    // prints area generic information
    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }

  abstract function LoadListItem(& $fields);

  function LoadFeedItem(& $fields, $base)
  {
    // dummy - default no RSS feed item
  }

  protected function LoadComments(& $fields,$moderators,$administrators)
  {
    // title
    $content = "<h2 class='subsection'>";
    $content .= $this->pageLabels->attrs['commentsTag']."</h2>\n";

    if($this->pageBuilder->sessionH->isGuest())
    {
      $addCommentHref = "href='cmsLoginForm.av?location=content/";
      $addCommentHref .= $fields['docId']."'";
    }

    // add comment button
    $content .= "<p class='actions'><a ";
    if($addCommentHref)
      $content .= $addCommentHref;
    else
      $content .= "onclick='AddComment(".$fields['docId'].");'";
    $content .= ">".$this->pageLabels->attrs['addCommentTag']."</a></p>\n";

    // search for comments
    $_GET['showHidden'] = (!$moderators && !$administrators)?'0':'1';
    $_GET['articleId'] = $fields['docId'];
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
								 'dateFormat');
    $getArticleComments = $this->page->LoadModule('CMS_GetComments');

    // print comments
    if(count($getArticleComments->children)>0)
    {
      $content .= "<ol class='pair'>\n";
      foreach($getArticleComments->children as & $child)
      {
	$content .= "<li id='comment".$child->attrs['docId']."'>\n";
	$content .= '<h4>'.$child->attrs['name'];
	$content .= ' <em>'.$child->attrs['creationDate']."</em></h4>\n";
	$content .= "<div id='commentTxt".$child->attrs['docId']."'>";
	$content .= $child->attrs['comment']."</div>\n";
	$content .= "<p class='actions'>";

	// remove
	if($administrators)
	{
	  $content .= "<a onclick='RemoveComment(";
	  $content .= $child->attrs['docId'].")' "."class='red'>";
	  $content .= $this->pageLabels->attrs['removeTag']."</a>";
	}

	$content .= '<a ';
	if($addCommentHref)
	  $content .= $addCommentHref;
	else
	{
	  $content .= "onclick='AddComment(".$fields['docId'].',';
	  $content .= $child->attrs['docId'].")'";
	}
	$content .= ">".$this->pageLabels->attrs['answerTag']."</a></p>\n";
	$content .= "</li>\n";
      }
      $content .= "</ol>\n";
    }

    $content .= "<div id='commentForm' class='fieldbox' style='display:none'>";
    $content .= "</div>\n";

    return $content;
  }

}

?>