<?php

require_once('LIB_Document.inc');

class LEGIS_EntityDoc extends LIB_Document
{
  protected function LoadSearchDefs(& $params)
  {
    parent::LoadSearchDefs($params);

    if($params['contractId'])
    {
      $params['searchObjs'][100005]['tables'][] = 'LEGIS_ContractEntity ce';
      $params['searchObjs'][100005]['tablesWhere'][] = 'doc.docId=ce.entityId';
      $params['searchObjs'][100005]['tablesWhere'][] = "ce.docId='".$params['contractId']."'";

      $params['searchObjs'][100006]['tables'][] = 'LEGIS_ContractEntity ce';
      $params['searchObjs'][100006]['tablesWhere'][] = 'doc.docId=ce.entityId';
      $params['searchObjs'][100006]['tablesWhere'][] = "ce.docId='".$params['contractId']."'";

      $params['searchObjs'][100007]['tables'][] = 'LEGIS_ContractEntity ce';
      $params['searchObjs'][100007]['tablesWhere'][] = 'doc.docId=ce.entityId';
      $params['searchObjs'][100007]['tablesWhere'][] = "ce.docId='".$params['contractId']."'";
    }
  }
}

?>