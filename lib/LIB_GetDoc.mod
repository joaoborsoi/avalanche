<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Document.inc');


//--Class: LIB_GetDoc
//--Parent: AV_DBOperationObj
//--Desc: Represents module's object which implements get document
//--Desc: operation.
class LIB_GetDoc extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'path';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['default'] = '/';
    $this->fieldsDef[1]['stripSlashes'] = false;

    $this->fieldsDef[2]['fieldName'] = 'fieldName';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;

    $this->fieldsDef[3]['fieldName'] = 'sourceId';
    $this->fieldsDef[3]['required'] = false;
    $this->fieldsDef[3]['allowNull'] = true;
    $this->fieldsDef[3]['consistType'] = 'integer';

    $this->fieldsDef[4]['fieldName'] = 'dateFormat';
    $this->fieldsDef[4]['required'] = false;
    $this->fieldsDef[4]['allowNull'] = true;

    $this->fieldsDef[5]['fieldName'] = 'numberFormat';
    $this->fieldsDef[5]['required'] = false;
    $this->fieldsDef[5]['allowNull'] = true;
    $this->fieldsDef[5]['consistType'] = 'boolean';
    $this->fieldsDef[5]['default'] = true;

    $this->fieldsDef[6]['fieldName'] = 'short';
    $this->fieldsDef[6]['required'] = false;
    $this->fieldsDef[6]['allowNull'] = true;
    $this->fieldsDef[6]['consistType'] = 'boolean';
    $this->fieldsDef[6]['default'] = false;
  }

  protected function GetTemplates()
  {
    if($this->fields['fieldName'] != '')
    {
      $templates['select']['fileName'] = 'loadSelectField';
      $templates['selectMult']['fileName'] = 'loadSelectMultField';
      $templates['selectOption']['fileName']  = 'loadSelectFieldOption';
      $templates['selectMultOption']['fileName']  = 'loadSelectMultFieldOption';
      $templates['radio']['fileName'] = 'radioField';
      $templates['radioOption']['fileName']  = 'radioFieldOption';
    }
    else
    {
      $templates['hidden']['fileName'] = 'hiddenField';
      $templates['hidden']['outputFuncRef'] = 'htmlentities_utf';
      $templates['text']['fileName'] = 'textField';
      $templates['text']['outputFuncRef'] = 'htmlentities_utf';
      $templates['textArea']['fileName'] = 'textAreaField';
      $templates['textArea']['outputFuncRef'] = 'htmlentities_utf';
      $templates['htmlArea']['fileName'] = 'htmlAreaField';
      $templates['htmlArea']['outputFuncRef'] = 'htmlentities_utf';
      $templates['cep']['fileName']  = 'cepField';
      $templates['cep']['outputFuncRef'] = 'htmlentities_utf';
      $templates['cnpj']['fileName']  = 'cnpjField';
      $templates['cpf']['fileName']  = 'cpfField';
      $templates['file']['fileName'] = 'fileField';
      $templates['fileList']['fileName'] = 'imageListField';
      $templates['fileListItem']['fileName'] = 'imageListItem';

      if($this->pageBuilder->lang == 'en_US')
	$templates['date']['fileName'] = 'dateField_en_US';
      else
	$templates['date']['fileName'] = 'dateField';

      if($this->pageBuilder->lang == 'en_US')
	$templates['datetime']['fileName'] = 'dateTimeField_en_US';
      else
	$templates['datetime']['fileName'] = 'dateTimeField';

      $templates['schedule']['fileName'] = 'scheduleField';
      $templates['select']['fileName'] = 'selectField';
      $templates['select']['outputFuncRef'] = 'htmlentities_utf';
      $templates['readOnlySelect']['fileName'] = 'readOnlySelect';
      $templates['selectMult']['fileName'] = 'selectMultField';
      $templates['selectMult']['outputFuncRef'] = 'htmlentities_utf';
      $templates['selectOption']['fileName']  = 'selectFieldOption';
      $templates['selectMultOption']['fileName']  = 'selectMultFieldOption';
      $templates['legalEntityRef']['fileName']  = 'legalEntityRefField';
      $templates['legalEntityRef']['outputFuncRef'] = 'htmlentities_utf';
      $templates['naturalPersonRef']['fileName']  = 'naturalPersonRefField';
      $templates['naturalPersonRef']['outputFuncRef'] = 'htmlentities_utf';
      $templates['accessRight']['fileName']  = 'accessRight';
      $templates['accessRight']['outputFuncRef'] = 'htmlentities_utf';
      $templates['langField']['fileName']  = 'langField';
      $templates['langField']['outputFuncRef'] = 'htmlentities_utf';
      $templates['langLabel']['fileName']  = 'langLabelField';
      $templates['langTextArea']['fileName']  = 'langTextAreaField';
      $templates['langHtmlArea']['fileName']  = 'langHtmlAreaField';
      $templates['langTab']['fileName']  = 'langTab';
      $templates['langTabItem']['fileName']  = 'langTabItem';
      $templates['boolean']['fileName']  = 'checkboxField';
      $templates['float']['fileName'] = 'floatField';
      $templates['percent']['fileName'] = 'percentField';
      $templates['radio']['fileName'] = 'radioField';
      $templates['radioOption']['fileName']  = 'radioFieldOption';
    }
    return $templates;
  }

  protected function &CreateDocument()
  {
    return new LIB_Document($this);
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  protected function Execute()
  {
    $templates = $this->GetTemplates();

    $doc = & $this->CreateDocument();
    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 

    if($this->fields['docId'] == NULL)
      $keyFields = NULL;
    else
      $keyFields = Array('docId' => $this->fields['docId']);

    $doc->GetForm($keyFields,$this->fields['path'], $this->fields['fieldName'],
		  $this->fields['sourceId'], $templates, 
		  $formObject,$defValues,$this->fields['dateFormat'],
		  $this->fields['numberFormat'], $this->fields['short']);

    $this->children[] = & $formObject;
  }
}

?>