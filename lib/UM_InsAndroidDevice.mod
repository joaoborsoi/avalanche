<?php

require_once('AV_DBOperationObj.inc');

class UM_InsAndroidDevice extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'registration_id';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;
  }


  protected function Execute()
  {
    $userMan = new UM_User($this);
    $this->attrs = $userMan->UpdateUserSession();

    $userMan->InsAndroidDevice($this->fields['registration_id']);
  }
}

?>