<?php

require_once('LIB_Search.mod');
require_once('LEGIS_CustomerDoc.inc');

class LEGIS_SearchCustomer extends LIB_Search
{
  protected function &CreateDocument()
  {
    return new LEGIS_CustomerDoc($this);
  }
}

?>
