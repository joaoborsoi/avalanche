<?php

require_once('AV_DBOperationObj.inc');

class CMS_GetComments extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'articleId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'showHidden';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['default'] = false;
    $this->fieldsDef[1]['consistType'] = 'boolean';

    $this->fieldsDef[2]['fieldName'] = 'dateFormat';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['default'] = '%c';
    $this->fieldsDef[2]['stripSlashes'] = true;

    $this->fieldsDef[3]['fieldName'] = 'photo';
    $this->fieldsDef[3]['required'] = false;
    $this->fieldsDef[3]['allowNull'] = true;
    $this->fieldsDef[3]['consitType'] = 'boolean';
    $this->fieldsDef[3]['default'] = false;

    $this->fieldsDef[4]['fieldName'] = 'order';
    $this->fieldsDef[4]['required'] = false;
    $this->fieldsDef[4]['allowNull'] = true;
    $this->fieldsDef[4]['consistType'] = 'boolean';
    $this->fieldsDef[4]['default'] = true;

    $this->fieldsDef[5]['fieldName'] = 'limit';
    $this->fieldsDef[5]['required'] = false;
    $this->fieldsDef[5]['allowNull'] = true;
    $this->fieldsDef[5]['consistType'] = 'integer';

    $this->fieldsDef[6]['fieldName'] = 'offset';
    $this->fieldsDef[6]['required'] = false;
    $this->fieldsDef[6]['allowNull'] = true;
    $this->fieldsDef[6]['consistType'] = 'integer';

 }

  protected function Execute()
  {
    $this->isArray = true;
    $SQL = "SELECT docId,";
    $SQL .= "DATE_FORMAT(creationDate,'".$this->fields['dateFormat'];
    $SQL .= "') as creationDate,";
    $SQL .= "comment, otherRight, CONCAT(firstName,CONCAT(' ',lastName)) as name, u.userId ";
    if($this->fields['photo'])
    {
      $SQL .= ', (SELECT imageId FROM LIB_UserImage ui WHERE u.userId=ui.userId ';
      $SQL .= "AND tableName='UM_User' AND fieldName='photo' LIMIT 1) as photo ";
    }
    $SQL .= 'FROM LIB_Node n, CMS_DocumentComments c, UM_User u ';
    $SQL .= 'WHERE nodeId=docId ';
    $SQL .= "AND articleId='".$this->fields['articleId']."' ";
    $SQL .= 'AND n.userId=u.userId ';
    if(!$this->fields['showHidden'])
      $SQL .= "AND otherRight='r' ";
    $SQL .= 'ORDER BY n.creationDate ';
    $SQL .= ($this->fields['order'] ? 'ASC ' : 'DESC ');

    if($this->fields['limit'] != NULL)
    {
      $SQL .= 'LIMIT ';
      if(isset($this->fields['offset']))
	$SQL .= $this->fields['offset'] . ',';
      $SQL .= $this->fields['limit'];
    }
       
    $comments = $this->dbDriver->GetAll($SQL);
    $this->LoadChildrenFromRows($comments,'comment');
  }
}