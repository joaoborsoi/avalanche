<?php

require_once('FORM_DynTable.inc');
require_once('LEGIS_EntityDoc.inc');
require_once('LEGIS_Process.inc');

class LEGIS_Contract extends FORM_DynTable
{
  function Insert(& $fields, $extraFields = array())
  {
    parent::Insert($fields,$extraFields);
    $this->SetListFields($fields,$extraFields);
  }

  function Set($fields, $extraFields = array())
  {
    parent::Set($fields,$extraFields);
    $this->SetListFields($fields,$extraFields);

    return $this->oldValues;
  }


  protected function SetListFields($fields,$extraFields)
  {

    if($fields['docId']!=NULL)
    {
      $docId = $fields['docId'];

      $SQL = "DELETE FROM LEGIS_ContractEntity WHERE docId='$docId'";
      $this->dbDriver->ExecSQL($SQL);

      $SQL = "DELETE FROM LEGIS_ContractProcess WHERE docId='$docId'";
      $this->dbDriver->ExecSQL($SQL);
    }
    else
      $docId = $extraFields['docId'];

    if(count($fields['contactList'])==0)
      throw new AV_Exception(FIELD_REQUIRED,$this->lang,
			     array('addMsg'=>' (Outra parte)'));

    $SQL = 'INSERT INTO LEGIS_ContractEntity (docId, entityId) VALUES ';
    foreach($fields['contactList'] as $key=>$contact)
    {
      if($key>0)
	$SQL .= ',';
      $SQL .= "($docId,'".$contact['docId']."')";
    }
    $this->dbDriver->ExecSQL($SQL);


    if(count($fields['processList'])>0)
    {
      $SQL = 'INSERT INTO LEGIS_ContractProcess (docId, processId) VALUES ';
      foreach($fields['processList'] as $key=>$contact)
      {
	if($key>0)
	  $SQL .= ',';
	$SQL .= "($docId,'".$contact['docId']."')";
      }
      $this->dbDriver->ExecSQL($SQL);
    }

  }


  protected function ParseFieldDef(& $fieldDef,$disableFileRequired,$disableRequired)
  {
    parent::ParseFieldDef($fieldDef,$disableFileRequired,$disableRequired);

    switch($fieldDef['type'])
    {
    case 'contractProcessList':
    case 'contractEntityList':
      // gambiarra pra evitar addslashes      
      $fieldDef['stripSlashes'] = true; 
    break;
    }
  }

  protected function &GetField(& $fieldDef, & $values, $keyFields,  
			       & $templates, $sourceId, $dateFormat = NULL,
			       $numberFormat = true, $fieldName = NULL)
  {
    switch ($fieldDef['type'])
    {
    case 'contractEntityList':
      return $this->GetEntityList($fieldDef, $keyFields,$dateFormat,$numberFormat);
    case 'contractProcessList':
      return $this->GetProcessList($fieldDef, $keyFields,$dateFormat,$numberFormat);

    default:
      return parent::GetField($fieldDef,$values,$keyFields,$templates,
			      $sourceId,$dateFormat,$numberFormat);
     }
  }

  protected function &GetEntityList($fieldDef,$keyFields,$dateFormat,$numberFormat)
  {
    // cria objeto para o campo de contratos
    $field =  new AV_ModObject('contractList',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    if($keyFields!=NULL)
    {
      $doc = new LEGIS_EntityDoc($this->module);
      $search['path'] = '/provider/legalEntity/,/provider/naturalPerson/,/user/';
      $search['orderBy'] = 'name';
      $search['order'] = '0';
      $search['dateFormat'] = $dateFormat;
      $search['numberFormat'] = $numberFormat;
      $search['tempTable'] = '0';
      $search['contractId'] = $keyFields['docId'];
      $field->attrs['value'] = $doc->Search($search);
    }
    
    return $field;
  }

  protected function &GetProcessList($fieldDef,$keyFields,$dateFormat,$numberFormat)
  {
    // cria objeto para o campo de processos
    $field =  new AV_ModObject('processList',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    if($keyFields!=NULL)
    {
      $doc = new LEGIS_Process($this->module);
      $search['path'] = '/process/';
      $search['orderBy'] = 'matches';
      $search['order'] = '0';
      $search['dateFormat'] = $dateFormat;
      $search['numberFormat'] = $numberFormat;
      $search['tempTable'] = '0';
      $search['contractId'] = $keyFields['docId'];
      $search['limit'] = 10;
      $field->attrs['value'] = $doc->Search($search);

      foreach($field->attrs['value'] as &$process)
	$process['number'] = explode(',',$process['number']);

    }
    
    return $field;
  }
}

?>