<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_Logout extends AV_DBOperationObj
{

  protected function Execute()
  {
    // attempt to login
    $userMan = new UM_User($this);
    $userMan->Logout();
  }
}

?>
