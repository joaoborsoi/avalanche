<?php

require_once('FORM_DynTable.inc');
require_once('LEGIS_ContractDoc.inc');

class LEGIS_Provider extends FORM_DynTable
{
  protected function &GetField(& $fieldDef, & $values, $keyFields,  
			       & $templates, $sourceId, $dateFormat = NULL,
			       $numberFormat = true, $fieldName = NULL)
  {
    switch ($fieldDef['type'])
    {
    case 'contractList':
      return $this->GetContractList($fieldDef,$keyFields,$dateFormat,
				    $numberFormat);
    case 'entryList':
      return $this->GetEntryList($fieldDef,$keyFields,$dateFormat,
				 $numberFormat);
    default:
      return parent::GetField($fieldDef,$values,$keyFields,$templates,
			      $sourceId,$dateFormat,$numberFormat);
    }
  }

  protected function &GetContractList($fieldDef,$keyFields,$dateFormat,$numberFormat)
  {
    if($keyFields==NULL)
      return NULL;

    // cria objeto para o campo de contratos
    $field =  new AV_ModObject('contractList',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    $doc = new LEGIS_ContractDoc($this->module);
    $search['path'] = '/contract/';
    $search['orderBy'] = 'expiration';
    $search['order'] = '0';
    $search['dateFormat'] = $dateFormat;
    $search['numberFormat'] = $numberFormat;
    $search['tempTable'] = '0';
    $search['entityId'] = $keyFields['docId'];
    $field->attrs['items'] = $doc->Search($search);
    
    return $field;
  }

  protected function &GetEntryList($fieldDef,$keyFields,$dateFormat,$numberFormat)
  {
    if($keyFields==NULL)
      return NULL;

    // cria objeto para o campo de contratos
    $field =  new AV_ModObject('contractList',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    $doc = new LIB_Document($this->module);
    $search['path'] = '/entry/';
    $exlusionFilter['fieldName'] = 'providerId';
    $exlusionFilter['value'] = current($keyFields);
    $exlusionFilter['operator'] = '=';
    $search['exclusionFilters']['providerId'][] = $exlusionFilter;
    $search['orderBy'] = 'expiration';
    $search['order'] = '0';
    $search['dateFormat'] = $dateFormat;
    $search['numberFormat'] = $numberFormat;
    $search['tempTable'] = '0';
    $field->attrs['items'] = $doc->Search($search);
    
    return $field;
  }

}

?>