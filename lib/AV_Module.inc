<?php

require_once("AV_Exception.inc");
require_once("AV_ModObject.inc");

//--Class: AV_Module
//--Desc: Avalanche's module abstract class. Each avalanche page construction 
//--Desc: is associated with modules creation. The module goal is to produce
//--Desc: output status variables, which are represented by objects on
//--Desc: a hierarchical structure.
abstract class AV_Module extends AV_ModObject 

{

  //
  //--Public
  //

  //--Attributes
  public $varPrefix;       //--Desc: Input/output variable prefix
  public $dbDriver;        //--Desc: Database driver handler

  protected $dbType;          //--Desc: Database type
  protected $template;        //--Desc: May store template name 
  protected $jsonInput;


  //
  //--Public
  //

  //--Method: AV_Module
  //--Desc: Module constructor
  function __construct(&$pbuilder, $itemTpl, $itemTplTag, $varPrefix, $jsonInput=false)
  {
    $this->jsonInput = $jsonInput;
    if($pbuilder->siteConfig->getVar("debug","logDBOper"))
      $pbuilder->PrintLog('Module:'.get_class($this));
    parent::__construct(get_class($this),$pbuilder,$itemTpl,$itemTplTag);

    $this->dbType = $pbuilder->siteConfig->getVar("db", "dbType");
    $this->itemTpl = $itemTpl;
    $this->itemTplTag = $itemTplTag;
    $this->varPrefix = trim($varPrefix);
    if($this->varPrefix != '')
      $this->varPrefix .= '__';
    $this->dbDriver = $pbuilder->dbDriver;
    $this->Load($this->GetInputSources());
  }


  //--Method: PrintLog
  //--Desc: Prints log information
  function PrintLog($message, $reportError)
  {
    $this->pageBuilder->PrintLog($message,$reportError);
  }

  abstract protected function Load(&$inputSources);


  //--Method: ReportError
  //--Desc: Report critical error by sending e-mail to the adress defined in 
  //--Desc: site configuration file (normally SiteConfig.xsm)
  protected function ReportError($message)
  {
    $this->pageBuilder->ReportError($message);
  }
  
  protected function &GetInputSources()
  {
    $inputSources[] = & $_GET;
    if($this->jsonInput)
      $inputSources[] = $this->pageBuilder->GetJsonInput();
    $inputSources[] = & $_POST;
    $inputSources[] = & $_FILES;

    return $inputSources;
  }

}
?>
