<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_Login extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'login';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'password';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['stripSlashes'] = false;

    $this->fieldsDef[2]['fieldName'] = 'authCode';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
  }

  protected function Execute()
  {
    // attempt to login
    $userMan = new UM_User($this);
    $this->attrs['userId'] = 
      $userMan->Login($this->fieldParser->fields['login'],
		      $this->fieldParser->fields['password'],
		      $this->fieldParser->fields['authCode']);
  }
}

?>
