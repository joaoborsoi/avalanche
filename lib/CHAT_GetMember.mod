<?php

require_once("AV_DBOperationObj.inc");
require_once("CHAT_Manager.inc");

class CHAT_GetMember extends AV_DBOperationObj
{
 
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'roomId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
  }


  function Execute()
  {   
   $this->isArray = true;
   $maganer = new  CHAT_Manager($this);  
   $maganer->LoadMessages(); 
   $rows = $maganer->GetMember($this->fieldParser->fields['roomId'],
                               $status);   

   if($status != SUCCESS) 
      return $status;

   $this->sizeVarName = 'numMembers';

   $i = 0;
   foreach($rows as $key => $row)
   {
     $this->children[$i] = new AV_ModObject("members", $this);
     $this->children[$i]->attrs = &$rows[$key];
     $i++;
   }  
 
   return SUCCESS;
  }
}

?>