<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


class LIB_CheckDocPermission extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'write';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['default'] = false;
    $this->fieldsDef[1]['consistType'] = 'boolean';
  }

  protected function Execute()
  {
    $doc = new LIB_Document($this);
    $doc->CheckDocPermission($this->fields['docId'],$this->fields['write']);
  }
}

?>
