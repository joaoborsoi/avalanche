<?php

class menu{
var $pageBuilder;
var $page;
var $str;
var $knownIds;

    function menu(&$page, $path){
        $this->pageBuilder = &$page->pageBuilder ;
        $this->page=&$page;
        //$tpt = $this->pageBuilder->loadTemplate($template);
        //$this->pageBuilder->rootTemplate->addTemplate($tpt, 'menu1');
        $this->knownIds=array_flip($this->page->sitePages);
        $this->str = $this->recurseDir($path);
       
    }
    function get_html(){
    	return $this->str;
    }
    function recurseDir($path, $level=3){
      
        $pathArray=explode('/', $path); //print_r( $pathArray); exit;
        $sPath = array_slice($pathArray, 0, ($level + 1));  //print_r( $sPath);
        $currPath = implode('/', $sPath);     
        //print_r( $sPath);exit;
        if($level==3)$menu = '<ul id="mainMenu">';
        else $menu = '<ul class="ul'.($level-2).'">';
        
        //////// ARQUIVOS  //////////
            unset($_GET['limit']);
	    //$_GET['returnFields']='title,path';
	    $_GET['orderBy']='fixedOrder';
	    $_GET['recursiveSearch']=0;
	    $_GET['path']= '/content'.$currPath;
            //$fileList = $this->page->getFiles($currPath);
            $fileList = $this->page->LoadModule('LIB_Search');
            
            //print_r($fileList->children);exit;
            $size=count($fileList->children);
            for($i=0;$i<$size;$i++){
                $child = $fileList->children[$i];
                $docId = $child->attrs['docId']; 
                $title = $child->attrs['title']; 
                $liClass=array();
                
		if($this->page->docId==$docId) $liClass[] = 'selected';
		
		if(isset($this->knownIds[$docId]))$link = $this->knownIds[$docId].'.av';
		else $link = 'sitePage/'.$docId.'.av';
		
                // LINKS
                $_GET['docId']=$docId;
                $docLinks = $this->page->LoadModule('LIB_GetDocLinks');
                $linkPath = $docLinks->children[1]->attrs['path'];
                $linkArray = explode('/',$linkPath); 
                array_shift($linkArray);
                array_shift($linkArray);
                $submenu = false;
        	if($linkArray[0]=='Páginas') {
        		$_GET['path']= $linkPath.'submenu/';
        		//$fileList = $this->page->LoadModule('LIB_Search');
        		try{
      				$module = $this->page->LoadModule('LIB_Search');
      				$nextLevel=$level+1;
      				$subPath='/'.implode('/',$linkArray).'submenu/';
      				$submenu = $this->recurseDir($subPath, $nextLevel);
    			}
    			catch(Exception $e){
         			if($e->getCode()!=INVALID_PATH)
             				throw $e;
    			}
        	}
        	
        	if($submenu)$liClass[] = 'submenu';
        	
        	$menu.='<li id="n-'.$docId.'"';
        	if(count($liClass)>0)$menu.= ' class="'.implode(' ',$liClass).'"';
        	$menu.= '><a href="'.$link.'" ><span>';
                $menu.= htmlentities_utf($title).'</span></a>';
        	if($submenu)$menu.=$submenu;
                $menu.='</li>';
            }
        
        
        $menu.='</ul>';    
        return $menu;       
        //}
    } 
    
}

?>
