<?php

require_once('UM_User.inc');
require_once('FORM_DynTable.inc');
require_once('LANG_Label.inc');

//--Class: LIB_Node
//--Desc: Library node
class LIB_Node
{
  //
  //--Private
  //
  protected $dbDriver;                 //--Desc: Database driver handler
  protected $module;                   //--Desc: Module's handler
  protected $nodeTable;
  protected $lang;
  protected $label;

  //
  //--Public
  //

  //--Method: __constructor
  //--Desc: Constructor.
  function __construct(&$module)
  {
    $this->module = & $module;
    $this->dbDriver = & $module->dbDriver;
    $this->nodeTable = new FORM_DynTable("LIB_Node", $this->module);
    $this->lang = $module->pageBuilder->lang;
    $this->label = new LANG_Label($this->module);
  }

  //--Method: GetNodeData
  //--Desc: Returns generic node attributes
  function GetNodeData($nodeId)
  {
    $keyFields['nodeId'] = $nodeId;
    $node = $this->nodeTable->Get($keyFields);
    if(count($node) == 0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    return $node;
  }


  //--Method: Insert
  //--Desc: Inserts new node to the library
  function Insert(& $fields)
  {
    $folderData = $this->GetFolderData($fields['path'], 'share');
    
    // checks out for write permission
    $data = $this->CheckNodePermission($folderData, true);
    
    // sets userId and groupId
    $extraFields['userId'] = $data['memberData']['userId'];
    if($extraFields['userId']==NULL)
      $extraFields['userId']='NULL';
    if($fields['groupId'] == NULL)
      if($folderData['defGroupId'] != NULL)
	$fields['groupId'] = $folderData['defGroupId'];
      else
	$fields['groupId'] = $data['memberGroups'][0];

    if(!isset($fields['userRight']) &&  isset($folderData['defUserRight']))
      $fields['userRight'] = $folderData['defUserRight'];
    if(!isset($fields['groupRight']) &&  isset($folderData['defGroupRight']))
      $fields['groupRight'] = $folderData['defGroupRight'];
    if(!isset($fields['otherRight']) &&  isset($folderData['defOtherRight']))
      $fields['otherRight'] = $folderData['defOtherRight'];
      

    // sets creationDate and lastChanged
    $extraFields['creationDate'] = 'NOW()';
    $extraFields['lastChanged'] = 'NOW()';

    $this->nodeTable->Insert($fields,$extraFields);

    return $folderData;
  }


  function Duplicate($path, $nodeId)
  {
    // checks out for write permission
    $folderData = $this->GetFolderData($path, 'share');
    $data = $this->CheckNodePermission($folderData, true);
    

    // checks out for read permission
    $docData = $this->GetNodeData($nodeId);
    $this->CheckNodePermission($docData, false);


    // sets userId and groupId
    $fields['userId'] = $data['memberData']['userId'];
    if($fields['userId']==NULL)
      unset($fields['userId']);

    // sets creationDate and lastChanged
    $extraFields['creationDate'] = 'NOW()';
    $extraFields['lastChanged'] = 'NOW()';

    $sourceKey['fieldName'] = 'nodeId';
    $sourceKey['fieldValue'] = $nodeId;
    $this->nodeTable->Duplicate($sourceKey, $fields, $extraFields);

    return $folderData;
  }

  //--Method: Set
  //--Desc: Sets node's attributes. 
  function Set($fields)
  {
    $nodeData = $this->GetNodeData($fields['nodeId']);

    // checks out for write permission
    $data = $this->CheckNodePermission($nodeData, true);
 
    $extraFields['lastChanged'] = 'NOW()';
    $extraFields['lastChangedUserId'] = $data['memberData']['userId'];

    $this->nodeTable->Set($fields, $extraFields);
  }



  //--Method: Del
  //--Desc: Deletes node
  function Del($fields)
  {
    $this->nodeTable->LoadFieldsDef();
    return $this->nodeTable->Del($fields);
  }


  function GetForm($keyFields, $fieldName, $sourceId, $templates, 
		   & $formObject, $defValues = array(),$dateFormat = NULL)
  {
    if($keyFields != NULL)
    {
      $nodeData = $this->GetNodeData($keyFields['nodeId']);

      // checks out for read permission
      $data = $this->CheckNodePermission($nodeData, false);
    }

    $this->nodeTable->GetForm($keyFields, $fieldName, $sourceId, $templates, 
			      $formObject, $defValues,$dateFormat);

    if($keyFields == NULL)
      unset($formObject->children[2010]);
  }

  //--Method: CheckNodePermission
  //--Desc: Checks for read/write permission on the given node ($path). 
  //--Desc: Performs locks.
  function CheckNodePermission($nodeData, $write)
  {
    if($write) 
      $perm = 'w'; 
    else  
      $perm = 'r';

    $sessionH = & $this->module->pageBuilder->sessionH;
    // checks if user is not logged and also public access right
    if($sessionH->isGuest() &&  
       strpos($nodeData['otherRight'], $perm) !== false)
      return NULL;

    // loads user member data and groups
    $userMan = new UM_User($this->module);
    $memberData = $userMan->GetMemberData('share', false, false);
    $memberGroups = $userMan->GetMemberGroupsIds('share');


    // sets array of attributes for the new node to be returned
    $data['memberData'] = & $memberData;
    $data['memberGroups'] = & $memberGroups;
    $data['isOwner'] = $memberData['userId'] == $nodeData['userId'];

    // first, if it's the owner, it should have owner rights, 
    // and don't consider the group and others right
    if($data['isOwner'])
    {
      if(strpos($nodeData['userRight'], $perm) !== false)
	return $data;
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);
    }
    
    // second, if it's a group member, it should have rights,
    // and don't consider others right
    if(in_array($nodeData['groupId'], $memberGroups))
    {
      if(strpos($nodeData['groupRight'], $perm) !== false)
	return $data;
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);
    }

    // third, check for other node groups permission
    $SQL = 'SELECT id ';
    $SQL .= 'FROM LIB_NodeGroupRights ngr, UM_UserGroup ug ';
    $SQL .= "WHERE ngr.nodeId = '".$nodeData['nodeId']."' ";
    $SQL .= "AND INSTR(ngr.groupRight,'$perm')>0 ";
    $SQL .= 'AND ngr.groupId = ug.groupId ';
    $SQL .= "AND ug.userId='".$memberData['userId']."' ";
    $id = $this->dbDriver->GetOne($SQL);
    if($id!=NULL)
      return $data;

    // forth, checks for public access right
    if(strpos($nodeData['otherRight'], $perm) !== false)
      return $data;
    throw new AV_Exception(PERMISSION_DENIED, $this->lang);
  }

  function CheckRestrictedDeletion($folderData, $memberData, $nodeId)
  {
    // if restricted deletion flag is off, so it's ok 
    if($folderData['restrictedDeletion']=='0')
      return;

    // if member is owner of the folder, it's ok.
    if($memberData['userId'] == $folderData['userId'])
      return;

    // if member is owner of the node being removed, it's ok
    $SQL = 'SELECT nodeId FROM LIB_Node ';
    $SQL .= "WHERE userId='".$memberData['userId']."' ";
    $SQL .= "AND nodeId='$nodeId' FOR UPDATE ";
    $nodeId = $this->dbDriver->GetOne($SQL);
    if($nodeId!=NULL)
      return;

    // any other case is denied
    throw new AV_Exception(PERMISSION_DENIED, $this->lang);
  }

  //--Method: PreparePath
  //--Desc: Formats input path string according to the system definitions
  function PreparePath($path)
  {
    if($path[0] != '/')
      $path = '/' . $path;

    if(mb_substr($path,- 1) != '/')
      $path .= '/';    

    return $path;
  }
  
  //--Method: ChMod
  //--Desc:  
  function ChMod($nodeId, $userRight, $groupRight, $otherRight)
  {
    $this->nodeTable->LoadFieldsDef();

    $fields['nodeId'] = $nodeId;
    $fields['userRight'] = $userRight;
    $fields['groupRight'] = $groupRight;
    $fields['otherRight'] = $otherRight;

    return $this->Set($fields);
  }

  function TakeOwnership($nodeId)
  {
    // loads user member data and groups
    $userMan = new UM_User($this->module);
    $memberData = $userMan->GetMemberData('share', false, false);
    $memberGroups = $userMan->GetMemberGroupsIds('share');

    // only administrator can take ownership
    if($memberData['userId'] != 1 && !in_array(1, $memberGroups))
    throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    $SQL = "SELECT nodeId FROM LIB_Node WHERE nodeId='$nodeId' FOR UPDATE";
    $nodeId = $this->dbDriver->GetOne($SQL);
    if($nodeId == NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $SQL = "UPDATE LIB_Node SET userId='".$memberData['userId']."' ";
    $SQL .= "WHERE nodeId='$nodeId'";
    $this->dbDriver->ExecSQL($SQL);

    return $memberData['login'];
  }

  //--Method: GetFolderData
  //--Desc: Returns folder attributes
  function GetFolderData(& $path, $lockMode)
  {
    $path = $this->PreparePath($path);

    // loads path rights
    $SQL = 'SELECT LIB_Node.*, LIB_Folder.* ';
    $SQL .= 'FROM LIB_Node, LIB_Folder ';
    $SQL .= 'WHERE ';
    $SQL .=      'nodeId = folderId AND ';
    $SQL .=      "path = '$path' ";
    switch($lockMode)
    {
    case 'share':
      $SQL .= ' LOCK IN SHARE MODE';
      if($this->dbDriver->type=='oracle')
      {
	$LOCK = 'LOCK TABLE LIB_Node,LIB_Folder IN SHARE MODE';
	$this->dbDriver->ExecSQL($LOCK);
      }

      break;
    case 'update':
      $SQL .= ' FOR UPDATE';
      break;
    }

    $folder = $this->dbDriver->GetRow($SQL);
    if(count($folder) == 0)
      throw new AV_Exception(INVALID_PATH, $this->lang,
			     array('addMsg'=>"$path"));

    return $folder;
  }

  protected function NotifyAdmin($msgTag,$fields)
  {
    $lang = $this->module->pageBuilder->siteConfig->getVar("defaults", "lang");
    $email = $this->module->pageBuilder->siteConfig->getVar("admin", 
							    "notifyAddress");

    if($email == NULL)
      return;

    $subject = $this->module->pageBuilder->siteConfig->getVar("admin",
							      "notifySubject");
    $loginUMTag = $this->label->GetLabel('loginUMTag', $lang);
    $adminDateTag = $this->label->GetLabel('adminDateTag', $lang);
    $message = $this->label->GetLabel($msgTag, $lang);
    

    $message .= "\n$loginUMTag: ";
    $message .= $this->module->pageBuilder->page->userSession->attrs['login'];
    $message .= "\n$adminDateTag: ".strftime('%c');
    foreach($fields as $key=>$field)
      $message .= "\n$key: ".$field;
    mail($email,$subject,$message,$header);    
  }

}

?>