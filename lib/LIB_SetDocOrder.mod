<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


//--Class: LIB_SetDocOrder
//--Parent: AV_DBOperationObj
//--Desc: Represents module's object which implements get folder operation.
class LIB_SetDocOrder extends AV_DBOperationObj
{
  //
  //--Public
  //

  //--Method: LoadFieldsDef
  //--Desc: Redefines parent method to define module's input fields
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;

    $this->fieldsDef[1]['fieldName'] = 'path';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['default'] = '/';
 
    $this->fieldsDef[2]['fieldName'] = 'offset';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'integer';
    $this->fieldsDef[2]['default'] = '1';
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $doc = new LIB_Document($this);
    $doc->SetOrder($this->fieldParser->fields['docId'],
		   $this->fieldParser->fields['path'],
		   $this->fieldParser->fields['offset']);
  }
}
?>
