<?php

require_once('LIB_Document.inc');

class LEGIS_EntryDoc extends LIB_Document
{
  protected function LoadSearchDefs(& $params)
  {
    parent::LoadSearchDefs($params);

    // ID do lançamento
    $params['searchObjs'][100012]['searchCriteria'][] = "CONCAT(t0.entryId,'/',t0.entryYear)";

    // pasta do processo
    $SQL = "(SELECT IF(folderYear IS NULL,folder,CONCAT(folder,'/',folderYear)) ";
    $SQL .= 'FROM LEGIS_Process p,LEGIS_RootProcess rp ';
    $SQL .= 'WHERE p.rootProcessId = rp.processId AND p.processId = t0.processId)';
    $params['searchObjs'][100012]['searchCriteria'][] = $SQL;

    // cnpj cliente
    $SQL = "(SELECT cnpj FROM EM_LegalEntity le WHERE t0.customerId=le.docId)";
    $params['searchObjs'][100012]['searchCriteria'][] = $SQL;

    // cpf cliente
    $SQL = "(SELECT cpf FROM EM_NaturalPerson np WHERE t0.customerId=np.docId)";
    $params['searchObjs'][100012]['searchCriteria'][] = $SQL;

    // rg cliente
    $SQL = "(SELECT rg FROM EM_NaturalPerson np WHERE t0.customerId=np.docId)";
    $params['searchObjs'][100012]['searchCriteria'][] = $SQL;

    // nome cliente
    $SQL = "(SELECT (SELECT name FROM EM_NaturalPerson np WHERE t0.customerId=np.docId) UNION ";
    $SQL .= "(SELECT name FROM EM_LegalEntity le WHERE t0.customerId=le.docId))";
    $params['searchObjs'][100012]['searchCriteria'][] = $SQL;

    // cnpj fornecedor
    $SQL = "(SELECT cnpj FROM LEGIS_LEProvider le WHERE t0.providerId=le.docId)";
    $params['searchObjs'][100012]['searchCriteria'][] = $SQL;

    // cpf fornecedor
    $SQL = "(SELECT cpf FROM LEGIS_NPProvider np WHERE t0.providerId=np.docId)";
    $params['searchObjs'][100012]['searchCriteria'][] = $SQL;

    // rg fornecedor
    $SQL = "(SELECT rg FROM LEGIS_NPProvider np WHERE t0.providerId=np.docId)";
    $params['searchObjs'][100012]['searchCriteria'][] = $SQL;

    // nome fornecedor
    $SQL = "(SELECT (SELECT name FROM LEGIS_LEProvider le WHERE t0.providerId=le.docId) UNION ";
    $SQL .= "(SELECT name FROM LEGIS_NPProvider np WHERE t0.providerId=np.docId))";
    $params['searchObjs'][100012]['searchCriteria'][] = $SQL;
  }
}

?>