<?php

require_once('AV_OCILexAnalyzer.inc');

class AV_OCISQLAnalyzer
{
  protected $pageBuilder;
  protected $lex;
  protected $code;
  protected $logDBOper;


  function __construct(& $pageBuilder, $logDBOper)
  {
    $this->logDBOper = $logDBOper;
    $this->pageBuilder = & $pageBuilder;
  }

  function AnalizeSQL($SQL)
  {
    if($this->logDBOper)
      $this->pageBuilder->PrintLog("Original SQL: $SQL", false);

    $ignoreList['SET NAMES UTF8'] = true;
    $ignoreList["SET LC_TIME_NAMES = 'PT_BR'"] = true;
    $ignoreList["BEGIN"] = true;

    if($ignoreList[strtoupper($SQL)])
    {
      if($this->logDBOper)
	$this->pageBuilder->PrintLog("Ignoring SQL: $SQL", false);
      return NULL;
    }

    $this->lex = new AV_OCILexAnalyzer($SQL);
    $this->code = NULL;
    while($this->lex->token != eofTk)
      $this->Token();
    return $this->code;
  }

  protected function Token()
  {
    switch($this->lex->token)
    {
    case openParenthesisTk:
      $this->Parenthesis();
      break;

    case selectTk:
      $this->Select();
      break;

    case nowTk:
      $this->Now();
      break;

    case dotTk:
    case multTk:
      $this->code .= $this->lex->value;
      $this->lex->NextToken();
      break;

    case lockTk:
      $this->Lock();
      break;

    case equalTk:
      $this->Equal();
      break;

    case lowerThenTk:
      $this->LowerThen();
      break;

    case levelTk:
      $this->code .= ' level_';
      $this->lex->NextToken();
      break;

    case sizeTk:
      $this->code .= ' size_';
      $this->lex->NextToken();
      break;

    case commentTk:
      $this->code .= ' comment_';
      $this->lex->NextToken();
      break;

    case unixTimestampTk:
      $this->UnixTimeStamp();
      break;

    case fromUnixtimeTk:
      $this->FromUnixtime();
      break;

    case ifTk:
      $this->IfStat();
      break;

    case createTk:
      $this->CreateTable();
      break;

    case integerDefTk:
      $this->code .= ' NUMBER(10,0)';
      $this->lex->NextToken();
      break;

    case unsignedTk:
      $this->lex->NextToken();
      break;

    case datetimeTk:
      $this->code  .= ' DATE';
      $this->lex->NextToken();
      break;

    case varcharTk:
      $this->code .= ' VARCHAR2';
      $this->lex->NextToken();
      break;

    case textTk:
      $this->code .= ' CLOB';
      $this->lex->NextToken();
      break;

    case groupConcatTk:
      $this->GroupConcat();
      break;

    case stringTk:
      $this->code .= ' '.stripslashes(str_replace('\\\'','\'\'',$this->lex->value));
      $this->lex->NextToken();
      break;

    case lastInsertIdTk:
      $this->LastInsertId();
      break;

    case colonTk:
      $this->Colon();
      break;

    case dateFormatTk:
      $this->DateFormat();
      break;

    case insertTk:
      $this->Insert();
      break;

    default:
      $this->code .= ' '.$this->lex->value;
      $this->lex->NextToken();
      break;
    }
  }

  protected function Select()
  {
    $firstPos = strlen($this->code);

    // eats select
    $this->code .= $this->lex->value;
    if(!$this->lex->NextToken())
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      'Unexpected eof'));

    $limit = NULL;
    $offset = NULL;
    $cont = true;
    while($cont && $this->lex->token!=eofTk)
    {
      switch($this->lex->token)
      {
      case limitTk:
	if(!$this->lex->NextToken())
	  throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							  'Unexpected eof'));
	if($this->lex->token!=integerTk)
	  throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							  'Integer expected'));
	$limit = $this->lex->value;
	$this->lex->NextToken();

	if($this->lex->token == commaTk)
	{
	  if(!$this->lex->NextToken())
	    throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							    'Unexpected eof'));
	  if($this->lex->token!=integerTk)
	    throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							    'Integer expected'));
	  $offset = $limit;
	  $limit = $this->lex->value;
	  $this->lex->NextToken();
	}
	break;

      case closeParenthesisTk:
	$cont = false;
	break;
      default:
	$this->Token();
	break;
      }
    }

    if($limit != NULL)
    {
      if($offset != NULL)
      {
	$minrow = $offset+1;
	$maxrow = $minrow + $limit;
	$code = "SELECT * FROM (";
	$code .= "SELECT a.*, ROWNUM rnum FROM (";
	$code .= substr($this->code,$firstPos);
	$code .= ") a WHERE ROWNUM <= $maxrow";
	$code .= ") WHERE rnum >= $minrow";
      }
      else
      {
	$maxrow = $limit;
	$code = "SELECT * FROM (";
	$code .= substr($this->code,$firstPos);
	$code .= ") a WHERE ROWNUM <= $maxrow";
      }

      $this->code = substr_replace($this->code, $code, $firstPos);
    }
  }

  protected function Parenthesis()
  {
      $this->code .= $this->lex->value;

      if(!$this->lex->NextToken())
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							'eof unexpected'));
      while($this->lex->token != closeParenthesisTk)
      {
	if($this->lex->token == eofTk)
	  throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							  'Unexpected eof'));
	$this->Token();
      }

      $this->code .= $this->lex->value;
      $this->lex->NextToken();
  }

  protected function Now()
  {
    $this->code .= ' SYSDATE';
    $this->lex->NextToken();
    if($this->lex->token != openParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>'( required'));

    $this->lex->NextToken();
    if($this->lex->token != closeParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>'( required'));
    $this->lex->NextToken();
  }

  protected function Lock()
  {
    // eats lock
    $this->lex->NextToken();

    switch($this->lex->token)
    {
    case inTk:
      // eats in
      $this->lex->NextToken();

      // eats share
      if($this->lex->token != shareTk)
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							'share expected'));
      $this->lex->NextToken();

      // eats mode
      if($this->lex->token != modeTk)
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							'mode expected'));
      $this->lex->NextToken();
      break;

    case tableTk:
      $this->lex->NextToken();
      $this->code .= 'LOCK TABLE';
      break;

    default:
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      'in or for expected:'.
						      $SQL));
    }
  }

  protected function Equal()
  {
    if(!$this->lex->NextToken())
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      'eof unexpected'));
    if($this->lex->token == stringTk && 
       $this->lex->value == "''" || $this->lex->value == "\"\"")
    {
      $this->code .= ' IS NULL';
      $this->lex->NextToken();
    }
    else
    {
      $this->code .= '=';
      $this->Token();
    }
  }

  protected function LowerThen()
  {
    if(!$this->lex->NextToken())
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      'eof unexpected'));
    if($this->lex->token == greaterThenTk)
    {
      if(!$this->lex->NextToken())
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							'eof unexpected'));
      if($this->lex->token == stringTk && $this->lex->value == "''")
	$this->code .= ' IS NOT NULL';
      else
	$this->code .= '<>'.$this->lex->value;
    }
    else
      $this->code .= '<'.$this->lex->value;
    $this->lex->NextToken();
  }

  protected function UnixTimeStamp()
  {
    $this->code .= '(';
    $this->lex->NextToken();

    if($this->lex->token != openParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>'( required'));
    if(!$this->lex->NextToken())
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      'eof unexpected'));

    if($this->lex->token == closeParenthesisTk)
    {
      $this->code .= 'SYSDATE';
      $this->lex->NextToken();
    }
    else
    {
      $this->code .= '(';
      $this->Token();

      while($this->lex->token != closeParenthesisTk)
      {
	if($this->lex->token == eofTk)
	  throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							  'Unexpected eof'));
	$this->Token();
      } 
      $this->code .= ')';
      $this->lex->NextToken();
    }

    $this->code .= " - TO_DATE('01-JAN-1970','DD-MON-YYYY')) * (86400)";
  }


  protected function FromUnixtime()
  {
    $this->lex->NextToken();

    if($this->lex->token != openParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>'( required'));
    if(!$this->lex->NextToken())
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      'eof unexpected'));

    if($this->lex->token == closeParenthesisTk)
    {
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      ') unexpected'));
    }

    $this->code .= '(';
    $this->Token();

    while($this->lex->token != closeParenthesisTk)
    {
      if($this->lex->token == eofTk)
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							'Unexpected eof'));
      $this->Token();
    } 
    $this->code .= ')';
    $this->lex->NextToken();

    $this->code .= "/(86400) + TO_DATE('01-JAN-1970','DD-MON-YYYY')";
  }


  protected function IfStat()
  {
    $this->code .= 'CASE WHEN ';
    $this->lex->NextToken();

    if($this->lex->token != openParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>'( required'));
    
    $this->lex->NextToken();

    while($this->lex->token != commaTk)
    {
      if($this->lex->token == eofTk)
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							'Unexpected eof'));
      $this->Token();
    }

    $this->code .= ' THEN ';

    $this->lex->NextToken();

    while($this->lex->token != commaTk)
    {
      if($this->lex->token == eofTk)
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							  'Unexpected eof'));
      $this->Token();
    }

    $this->code .= ' ELSE ';

    $this->lex->NextToken();

    while($this->lex->token != closeParenthesisTk)
    {
      if($this->lex->token == eofTk)
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							  'Unexpected eof'));
      $this->Token();
    }
    $this->lex->NextToken();

    $this->code .= ' END ';
  }

  protected function CreateTable()
  {
    $this->code .= ' CREATE';

    if(!$this->lex->NextToken())
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      'eof unexpected'));
    if($this->lex->token == temporaryTk)
    {
      $this->code .= ' GLOBAL TEMPORARY';

      if(!$this->lex->NextToken())
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							'eof unexpected'));
    }

    if($this->lex->token != tableTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      'TABLE expected'));
    $this->code .= ' TABLE';
    $this->lex->NextToken();
  }

  protected function GroupConcat()
  {
    $this->code .= ' LISTAGG(';

    if(!$this->lex->NextToken())
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      ' Unexpected eof'));

    if($this->lex->token != openParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      ' ( expected'));
    $this->lex->NextToken();

    $order = false;
    while($this->lex->token != closeParenthesisTk)
    {
      if($this->lex->token == eofTk)
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							' Unexpected eof'));

      if($this->lex->token == orderTk)
      {
	$order = true;
	$this->code .= ", ',') WITHIN GROUP (";
      }

      $this->Token();
    }

    if(!$order)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      ' ORDER BY needed for '.
						      'GROUP_CONCAT '.
						      'compatibility with '.
						      'oracle LISTAGG'));

    $this->code .= ')';

    $this->lex->NextToken();
  }

  protected function LastInsertId()
  {
    $this->code .= ' MYSQL_UTILITIES.LAST_INSERT_ID';
    $this->lex->NextToken();
    if($this->lex->token != openParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>'( required'));

    $this->lex->NextToken();
    if($this->lex->token != closeParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>'( required'));
    $this->lex->NextToken();
  }

  protected function Colon()
  {
    $this->code .= ' '.$this->lex->value;
    $this->lex->NextToken();
    if($this->lex->token==varTk)
    {
      $this->code .= $this->lex->value;
      $this->lex->NextToken();
    }
  }

  protected function DateFormat()
  {
    $this->code .= ' TO_CHAR(';
    $this->lex->NextToken();

    if($this->lex->token != openParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>'( required'));
    $this->lex->NextToken();

    while($this->lex->token != commaTk)
    {
      if($this->lex->token == eofTk)
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
							'Unexpected eof'));
      $this->Token();
    }

    $this->code .= ',';
    $this->lex->NextToken();

    if($this->lex->token != stringTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>
						      ' Expected string and got '.$this->lex->value));

    $convTable['%Y'] = 'YYYY';
    $convTable['%y'] = 'RR';
    $convTable['%b'] = 'MON';
    $convTable['%M'] = 'MONTH';
    $convTable['%m'] = 'MM';
    $convTable['%a'] = 'DY';
    $convTable['%d'] = 'DD';
    $convTable['%e'] = 'DD';
    $convTable['%H'] = 'HH24';
    $convTable['%h'] = 'HH';
    $convTable['%i'] = 'MI';
    $convTable['%s'] = 'SS';
    $convTable['%T'] = 'HH24:MI:SS';

    foreach($convTable as $key=>$conv)
      $this->lex->value = str_replace($key,$conv,$this->lex->value);
    $this->code .= $this->lex->value;

    $this->lex->NextToken();

    if($this->lex->token != closeParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>') required'));
    $this->code .= ')';

    $this->lex->NextToken();
  }

  protected function Insert()
  {
     // eats insert
    $this->code .= $this->lex->value;
    $this->lex->NextToken();

    $afterInsertPos = strlen($this->code);

    // eats into
    if($this->lex->token != intoTk)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg'=>' INTO required'));
    $this->Token();

    // eats tableName
    if($this->lex->token != varTk)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg'=>' table name required'));
    $this->Token();

    // eats column definitions
    if($this->lex->token != openParenthesisTk)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg'=>' ( required'));
    $this->Token();

    switch($this->lex->token)
    {
    case selectTk:
      // eats select expression
      $this->Token();
      break;
    case valuesTk:
      // eats values token
      $this->Token();

      $afterValuesPos = strlen($this->code);

      // eats row specifications
      if($this->lex->token != openParenthesisTk)
	throw new AV_Exception(FAILED,$this->lang,
			       array('addMsg'=>' ( required'));
      $this->Token();

      $first = true;
      while($this->lex->token == commaTk)
      {
	if($first)
	{
	  $prefix = substr($this->code,$afterInsertPos,$afterValuesPos-$afterInsertPos);
	  $this->code = substr_replace($this->code, ' ALL', $afterInsertPos, 0);
	  $first = false;
	}
	$this->lex->NextToken();
	$this->code .= $prefix;

	// eats row values
	if($this->lex->token != openParenthesisTk)
	  throw new AV_Exception(FAILED,$this->lang,
				 array('addMsg'=>' ( required'));
	$this->Token();
	
      }
      if(!$first)
	$this->code .= ' SELECT * FROM DUAL';
      break;
    default:
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg'=>'VALUES OR SELECT required'));
    }
    

    
   
  }
}

?>