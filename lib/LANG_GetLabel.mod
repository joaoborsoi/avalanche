<?php 

require_once("AV_DBOperationObj.inc");
require_once("LANG_Label.inc");

class LANG_GetLabel extends AV_DBOperationObj
{
  
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'labelId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;

    $this->fieldsDef[1]['fieldName'] = 'lang';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['stripSlashes'] = false;
  }
  
  protected function Execute()
  {
    $fields = & $this->fieldParser->fields;

    $label = new LANG_Label($this);
    $this->attrs['value'] = $label->GetLabel($fields['labelId'], 
					     $fields['lang']);
  }
}

?>