<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Qualification.inc');

class LEGIS_GetQualification extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'list';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = true;
    $this->fieldsDef[0]['trimField'] = false;
  }

  protected function Execute()
  { 
    $qualification =  new LEGIS_Qualification($this);
    $this->attrs = $qualification->Get($this->fields['list']);
  } 

}

?>