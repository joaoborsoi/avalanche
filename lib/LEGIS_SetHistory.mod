<?php

require_once('LIB_SetDoc.mod');
require_once('LEGIS_History.inc');

class LEGIS_SetHistory extends LIB_SetDoc
{
  protected function &CreateDocument()
  {
    return new LEGIS_History($this);
  }
}

?>