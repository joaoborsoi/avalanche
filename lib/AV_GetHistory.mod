<?php

require_once('AV_History.inc');
require_once('AV_DBOperationObj.inc');

class AV_GetHistory extends AV_DBOperationObj
{
  protected function Execute()
  {
    $this->isArray = true;
    $history = new AV_History($this);

    $result = $history->GetHistory();
    $this->LoadChildrenFromRows($result,'history');
  }
}

?>