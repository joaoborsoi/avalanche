<?php

require_once('GAME_Indicators.inc');

class GAME_Engine
{
  protected $module;
  protected $dbDriver;
  protected $pageBuilder;

  function __construct(AV_Module &$module)
  {
    $this->module = &$module;
    $this->dbDriver = &$module->dbDriver;
    $this->pageBuilder = &$module->pageBuilder;
  }

  function Start($restart)
  {
    $indicators = new GAME_Indicators($this->module);

    // if not restarting, check to see if there is a current game 
    // and it's no over
    if(!$restart)
    {
      $turn = $indicators->GetTurn();
      if($turn != NULL && 
	 $turn < $this->pageBuilder->siteConfig->getVar('game','lastTurn'))
	return;
    }

    // flush old games
    if($this->pageBuilder->siteConfig->getVar('game','flushOldGames'))
    {
      $SQL = 'DELETE FROM p USING GAME_Play p, GAME_PlayUser pu, UM_UserSessions us ';
      $SQL .= "WHERE sessionId = '".$this->pageBuilder->sessionH->sessionID."' ";
      $SQL .= 'AND us.userId = pu.userId AND pu.id = p.id ';
      $this->dbDriver->ExecSQL($SQL);
    }
    else
    {
      // finish old games
      $SQL = "SELECT pu.id FROM GAME_PlayUser pu, UM_UserSessions us ";
      $SQL .= "WHERE sessionId = '".$this->pageBuilder->sessionH->sessionID."' ";
      $SQL .= "AND us.userId = pu.userId AND currGame = '1' FOR UPDATE ";
      $playId = $this->dbDriver->GetOne($SQL);

      if($playId != NULL)
      {
	$SQL = "UPDATE GAME_PlayUser pu SET currGame='0' ";
	$SQL .= "WHERE id = '$playId' ";
	$this->dbDriver->ExecSQL($SQL);
      }
    }
    
    // start new game
    $SQL = 'INSERT INTO GAME_Play (id,date) VALUES (NULL,NOW()) ';
    $this->dbDriver->ExecSQL($SQL);
    $playId = $this->dbDriver->GetOne("SELECT LAST_INSERT_ID()");

    // include user to the game
    $SQL = 'INSERT INTO GAME_PlayUser (id, userId, currGame) VALUES ';
    $SQL .= "('$playId',(SELECT userId FROM UM_UserSessions ";
    $SQL .= "WHERE sessionId = '".$this->pageBuilder->sessionH->sessionID."'),'1')";
    $this->dbDriver->ExecSQL($SQL);

    // create default indicators
    $indicators = new GAME_Indicators($this->module, '*',NULL);
    $indicators->LoadFieldsDef();
    $fields['turn'] = 1;
    $fields['playId'] = $playId;
    $indicators->Insert($fields);
    $indId = $this->dbDriver->GetOne('SELECT LAST_INSERT_ID()');
    $indicators->UpdateFormulas($indId);

    // create play events
    $SQL = 'INSERT INTO GAME_PlayEvent (id, playId)  ';
    $SQL .= "SELECT id, $playId as playId FROM GAME_Event ";
    $this->dbDriver->ExecSQL($SQL);
  }

  function Play()
  {
    // update formula fields to reflect user changes
    $indicators = new GAME_Indicators($this->module, '*', NULL);
    $fieldsDef = $indicators->LoadFieldsDef();
    $id = $indicators->GetLastId();
    if($id == NULL)
      throw new AV_Exception(NOT_FOUND,$this->lang);
    $indicators->UpdateFormulas($id);


    // load event list
    $SQL = 'SELECT e.*,pe.* ';
    $SQL .= 'FROM GAME_Event e, GAME_PlayEvent pe, GAME_PlayUser pu, ';
    $SQL .= 'UM_UserSessions us ';
    $SQL .= "WHERE sessionId = '".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $SQL .= 'AND pu.id = playId AND e.id = pe.id LOCK IN SHARE MODE ';
    $eventList = $this->dbDriver->GetAll($SQL);
    if(count($eventList)==0)
      throw new AV_Exception(FAILED,$this->pageBuilder->lang);
    $playId = $eventList[0]['playId'];
    
    // select events to trigger
    $SQL = 'SELECT i.* ';
    foreach($eventList as & $event)
    {
      $event['cond'] = str_replace("%status%",$event['status'],$event['cond']);
      $SQL .= ',('.$event['cond'].') as ev_' . $event['id']. ' ';
    }
    $SQL .= 'FROM GAME_Indicators i ';
    $SQL .= "WHERE playId = '$playId' ";
    $SQL .= 'ORDER BY turn DESC LIMIT 1 LOCK IN SHARE MODE ';
    $row = $this->dbDriver->GetRow($SQL);

    // next turn
    foreach($fieldsDef as &$field)
    {
      if($field['keyField'] != '1')
      {
	$values[$field['fieldName']] = $row[$field['fieldName']];
	if($field['fieldName']=='turn')
	  $values['turn'] += 1;
      }
    }
    $indicators->Insert($values);
    $id = $this->dbDriver->GetOne('SELECT LAST_INSERT_ID()');
    $indicators->UpdateFormulas($id);

    // trigger events
    foreach($eventList as & $event)
    {
      $updateEvent = false;

      // if not triggered and condition is satisfied, then trigger
      if($event['status'] != 2 && $row['ev_'.$event['id']])
      {
	$event['status'] = 2;
	$event['triggerTurn'] = $values['turn'];
	$updateEvent = true;
      }

      // if triggered and turn after delay, do impact indicators
      if($event['status'] == 2 && 
	 $values['turn']>=$event['triggerTurn']+$event['delay'] &&
	 $values['turn']-$event['triggerTurn']-$event['delay']<=$event['duration'])
      {
	// start event - impact update
	if($event['startImpact'] != NULL &&
	   $values['turn']-$event['triggerTurn']-$event['delay']==0)
	  $indicators->ApplyImpact($id, $event['startImpact']);

	// end event
	if($values['turn']-$event['triggerTurn']-$event['delay']==$event['duration'])
	{
	  // finish event
	  $event['status'] = 3;
	  $updateEvent = true;

	  // end impact update
	  if($event['endImpact'] != NULL)
	    $indicators->ApplyImpact($id, $event['endImpact']);
	}

	// create area event
	if($values['turn']-$event['triggerTurn']-$event['delay']==0 &&
	   $event['elementId'] != NULL)
	{
	  $SQL = 'INSERT INTO GAME_AreaEvent (playId,areaElement) ';
	  $SQL .= "VALUES ('$playId','".$event['elementId']."') ";
	  $this->dbDriver->ExecSQL($SQL);
	}

	// update formulas
	$indicators->UpdateFormulas($id);

      } // if triggered

      if($updateEvent)
      {
	// update play event
	$SQL = 'SELECT id, playId FROM GAME_PlayEvent ';
	$SQL .= "WHERE id='".$event['id']."' AND playId='$playId' FOR UPDATE";
	$this->dbDriver->GetRow($SQL);
	
	$SQL = "UPDATE GAME_PlayEvent SET status='".$event['status']."' ";
	if($event['triggerTurn'] != NULL)
	  $SQL .= ", triggerTurn = '".$event['triggerTurn']."' ";
	$SQL .= "WHERE id='".$event['id']."' AND playId='$playId' ";
	$this->dbDriver->ExecSQL($SQL);
      }


    } // foreach($eventList as & $event)

  }

  function ImpactValidation()
  {
    // update formula fields to reflect user changes
    $indicators = new GAME_Indicators($this->module, '*', NULL);
    $fieldsDef = $indicators->LoadFieldsDef();
    $id = $indicators->GetLastId();
    if($id == NULL)
      throw new AV_Exception(NOT_FOUND,$this->lang);
 
    // load event list
    $SQL = 'SELECT e.*,pe.* ';
    $SQL .= 'FROM GAME_Event e, GAME_PlayEvent pe, GAME_PlayUser pu, ';
    $SQL .= 'UM_UserSessions us ';
    $SQL .= "WHERE sessionId = '".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $SQL .= 'AND pu.id = playId AND e.id = pe.id LOCK IN SHARE MODE ';
    $eventList = $this->dbDriver->GetAll($SQL);
    if(count($eventList)==0)
      throw new AV_Exception(FAILED,$this->pageBuilder->lang);
    $playId = $eventList[0]['playId'];
 
    $log = "";
    foreach($eventList as & $event)
    {
      if($event['startImpact'] != NULL)
      {
	try
	{
	  $indicators->ApplyImpact($id, $event['startImpact']);
	}
	catch(AV_Exception $e)
	{
	  $log .= '<h3>'.$e->getMessage().'</h3>';
	  $log .= '<p><label>id:</label>'.$event['id'].'<br />';
	  $log .= '<label>cond:</label>'.$event['cond'].'<br />';
	  $log .= '<label>startImpact:</label>'.$event['startImpact'].'</p>';
	}
      }

      if($event['endImpact'] != NULL)
      {
	try
	{
	  $indicators->ApplyImpact($id, $event['endImpact']);
	}
	catch(AV_Exception $e)
	{
	  $log .= '<h3>'.$e->getMessage().'</h3>';
	  $log .= '<p><label>id:</label>'.$event['id'].'<br />';
	  $log .= '<label>cond:</label>'.$event['cond'].'<br />';
	  $log .= '<label>endImpact:</label>'.$event['endImpact'].'</p>';
	}
      }
    }


    $SQL = 'SELECT q.id, message, text, impact ';
    $SQL .= 'FROM GAME_Quiz q, GAME_QuizOption qo ';
    $SQL .= 'WHERE q.id=quizId ';
    $SQL .= 'ORDER BY q.id, qo.id ';
    $quizList = $this->dbDriver->GetAll($SQL);
    foreach($quizList as & $quiz)
    {
      if($quiz['impact'] != NULL)
      {
	try
	{
	  $indicators->ApplyImpact($id, $quiz['impact']);
	}
	catch(AV_Exception $e)
	{
	  $log .= '<h3>'.$e->getMessage().'</h3>';
	  $log .= '<p><label>id:</label>'.$quiz['id'].'<br />';
	  $log .= '<label>message:</label>'.$quiz['message'].'<br />';
	  $log .= '<label>text:</label>'.$quiz['text'].'<br />';
	  $log .= '<label>Impact:</label>'.$quiz['impact'].'</p>';
	}
      }
    }

    if($log == "")
      $log = '<h3>Nenhum erro encontrado</h3>';
    return $log;
  }

}

?>