<?php

require_once('AV_DBOperationObj.inc');

class LEGIS_UpgradeEmail extends AV_DBOperationObj
{
  function Execute()
  {
    $SQL = "SELECT docId, email FROM EM_NaturalPerson WHERE email <> '' AND email IS NOT NULL ";
    $SQL .= 'UNION ';
    $SQL .= "SELECT docId, email FROM EM_LegalEntity WHERE email <> '' AND email IS NOT NULL ";
    $SQL .= 'UNION ';
    $SQL .= "SELECT docId, email FROM LEGIS_User WHERE email <> '' AND email IS NOT NULL ";
    $rows = $this->dbDriver->GetAll($SQL);

    foreach($rows as $row)
    {
      $emails = array();
      if(filter_var($row['email'], FILTER_VALIDATE_EMAIL) === false)
      {
	// não é email válido

	// tenta substituir , por . para identificar erro de digitacao
	$aux = str_replace(',','.',$row['email']);
	if(filter_var($aux, FILTER_VALIDATE_EMAIL) === false)
	{
	  // tenta quebra emails por virgula
	  $aux = explode(',',$row['email']);
	  if(count($aux)==1)
	    // tenta quebra emails por ponto e virgula
	    $aux = explode(';',$row['email']);

	  if(count($aux)>1)
	  {
	    foreach($aux as $aux2)
	    {
	      $aux2 = trim($aux2);
	      if(filter_var($aux2, FILTER_VALIDATE_EMAIL) !== false)
		$emails[] = $aux2;
	    }
	  }
	  
	}
	else
	{
	  // único email e válido
	  $emails[] = $aux;
	}
      }
      else
      {
	// único email e válido
	$emails[] = $row['email'];
      }


      if(count($emails)==0)
	continue;

      $SQL = 'INSERT INTO LIB_DocEmailList (docId, email, idx) VALUES ';
      foreach($emails as $key =>$email)
      {
	if($key>0)
	  $SQL .= ',';
	$SQL .= '('.$row['docId'].",'$email',$key)";
      }
      $this->dbDriver->ExecSQL($SQL);
    }
  }

}

?>