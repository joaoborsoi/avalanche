<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Process.inc');

class LEGIS_CancelReceipt extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'number';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'year';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['consistType'] = 'integer';

    $this->fieldsDef[2]['fieldName'] = 'observations';
    $this->fieldsDef[2]['required'] = true;
    $this->fieldsDef[2]['allowNull'] = false;
    $this->fieldsDef[2]['stripSlashes'] = false;
  }
  
  function Execute()
  {
    $process =  new LEGIS_Process($this);
    $this->attrs = $process->CancelReceipt($this->fields);
  } 
}

?>