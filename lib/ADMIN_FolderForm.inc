<?php

require_once('ADMIN_Popup.inc');

class ADMIN_FolderForm extends ADMIN_Popup
{
  protected $rootPath;

  function __construct(& $pageBuilder)
  {
    switch($_REQUEST['tab'])
    {
    case 'adminContent':
      $this->rootPath = '/content/';
      break;
    case 'adminLib':
      $this->rootPath = '/library/';
      break;
    }

    parent::__construct($pageBuilder);    
  }

  protected function LoadPopup()
  {
    $_GET['labelId'] = 'adminFolderTag';
    $_GET['lang'] = $this->lang;
    $windowTitleLabel = &$this->LoadModule('LANG_GetLabel');
    $windowTitle = htmlentities_utf($windowTitleLabel->attrs['value']);
    $this->pageBuilder->rootTemplate->addText($windowTitle, 'windowTitle');

    $folderFormTpl = $this->pageBuilder->loadTemplate('adminFolderForm');

    $this->pageBuilder->rootTemplate->addTemplate($folderFormTpl, 'content');

    try
    {
      $getFolder = $this->LoadModule('LIB_GetFolder');

      // folderPath
      $folderPath=$getFolder->children[0]->children[4110]->attrs['folderPath'];

      // esconde/exibe campos de menu
      if(!($this->rootPath == '/content/' &&
	   mb_substr($_REQUEST['folderPath'],0,6) == '/Menu/' ||
	   mb_substr($folderPath,0,14) =='/content/Menu/'))
      {
	unset($getFolder->children[0]->children['1200']); // images
	unset($getFolder->children[0]->children['4140']); // menu title
	unset($getFolder->children[0]->children['4115']); // title
	unset($getFolder->children[0]->children['4145']); // descr
	unset($getFolder->children[0]->children['4150']); // content
      }

      if(!($this->rootPath == '/content/' ||
	   mb_substr($folderPath,0,9) =='/content/'))
	unset($getFolder->children[0]->children['4120']); // folderTables

	
      $getFolder->PrintHTML($folderFormTpl);

      $this->script .= "InitiateFolderContent(\"".$_REQUEST['folderPath']."\",'";
      $this->script .= $this->lang."');";
    }
    catch(Exception $e)
    {
      $this->script .= "alert('".$e->getMessage()."');";
      $this->script .= "close();";
    }
  }

}

?>