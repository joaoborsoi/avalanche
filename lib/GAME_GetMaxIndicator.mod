<?php

require_once('AV_DBOperationObj.inc');
require_once('GAME_Indicators.inc');

class GAME_GetMaxIndicator extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'fieldName';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;
  }

  protected function Execute()
  {
    $indicators = new GAME_Indicators($this);
    $this->attrs['value'] = $indicators->GetMaxIndicator($this->fields['fieldName']);
  }
}

?>