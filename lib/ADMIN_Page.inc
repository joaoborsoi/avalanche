<?php

require_once('AV_Page.inc');

abstract class ADMIN_Page extends AV_Page
{
  protected $allowedUsers;

  protected function UpdateUserSession()
  {
    parent::UpdateUserSession();

    // only allowed users if defined
    $sessionH = &$this->pageBuilder->sessionH;

    if(!$sessionH->isMember())
      return;

    $this->allowedUsers=$this->pageBuilder->siteConfig->getVar('admin',
							       'allowedUsers');
    $this->allowedGroups=$this->pageBuilder->siteConfig->getVar('admin',
								'allowedGroups');

    if($this->allowedUsers != NULL)
    {
      $this->allowedUsers = explode(',',$this->allowedUsers);
      if(in_array($this->userSession->attrs['login'],$this->allowedUsers))
	return;
    }

    if($this->allowedGroups != NULL)
    {
      $this->allowedGroups = explode(',',$this->allowedGroups);
      $memberGroups = $this->LoadModule('UM_GetMemberGroups');
      
      foreach($memberGroups->children as $group)
	if(in_array($group->attrs['groupId'],$this->allowedGroups))
	  return;
    }

    $sessionH->attemptLogout();
    
  }
}