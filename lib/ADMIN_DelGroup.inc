<?php

require_once('ADMIN_Groups.inc');

class ADMIN_DelGroup extends ADMIN_Groups
{

  protected function LoadGroups()
  {
    try
    {
      if($_REQUEST['groupId'] == '1')
	throw AV_Exception(PERMISSION_DENIED, $this->lang);

      $delModule = $this->LoadModule('UM_DelGroup');
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }    

    $script .= parent::LoadGroups();
    return $script;
  }

    
}

?>