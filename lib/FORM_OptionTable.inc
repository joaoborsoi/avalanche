<?php

require_once('FORM_DynTable.inc');
require_once('UM_User.inc');

class FORM_OptionTable extends FORM_DynTable
{
  function __construct(&$module)
  {
    parent::__construct('FORM_OptionTable',$module);
  }

  function CreateTable($tableName, & $status)
  {
    $SQL = 'INSERT INTO FORM_Field (tableName,fieldName,labelId,type,';
    $SQL .= 'size,orderby,consistType,required,';
    $SQL .= 'allowNull,trimField,uniq,keyField,accessMode,staticProp) VALUES ';
    $SQL .= "('$tableName','id',NULL,'dummy',NULL,0,NULL,'INTEGER UNSIGNED',";
    $SQL .= "'integer',NULL,'0','1','1','1','1','rw','1'),('$tableName',";
    $SQL .= "'name','nameIRISTag','langLabel',50,1,NULL,'VARCHAR(50)',NULL,";
    $SQL .= "NULL,'1','0','1','0','0','rw','1'),";
    $SQL .= "('".$tableName."SearchFields','id','itemIRISTag','select',NULL,";
    $SQL .= "1,NULL,'INTEGER UNSIGNED','integer',NULL,'1','0','1','0','0',";
    $SQL .= "'rw','1');";
    $status = $this->dbDriver->ExecSQL($SQL);
    if($status != SUCCESS)
      return NULL;

    $SQL = "INSERT INTO FORM_SelectField (tableName,fieldName,optionTable,";
    $SQL .= "optionTableKey,selectedTable,sourceField,targetField) VALUES ";
    $SQL .= "('".$tableName."SearchFields','id','$tableName','id',NULL,NULL,";
    $SQL .= 'NULL);';
    $status = $this->dbDriver->ExecSQL($SQL);
    if($status != SUCCESS)
      return NULL;

    $SQL = "CREATE TABLE $tableName (";
    $SQL .= 'id INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,';
    $SQL .= 'name VARCHAR(50),';
    $SQL .= 'INDEX (name),';
    $SQL .= 'FOREIGN KEY (name) REFERENCES LANG_Label(labelId) ';
    $SQL .= 'ON DELETE SET NULL)TYPE = innoDB;';
    return $SQL;
  }

  function DropTable($tableName, & $status)
  {
    $SQL = "DELETE FROM FORM_Field WHERE tableName = '$tableName'";
    $status = $this->dbDriver->ExecSQL($SQL);
    if($status != SUCCESS)
      return NULL;

    return "DROP TABLE $tableName ";
  }

  function Insert(& $fields)
  {
    $prefix = 'FORM_OptionTable';
    $offset = mb_strlen($prefix) + 1;
    $SQL = "SELECT MAX(SUBSTR(tableName,$offset)+0) FROM " . $this->table;
    $SQL .= " WHERE tableName like '$prefix%' FOR UPDATE ";

    $num = $this->dbDriver->GetOne($SQL, $status);
    if($status != SUCCESS)
      return $status;

    $tableName = ($num == NULL)? $prefix.'1' : $prefix . ++$num;

    $transaction = $this->CreateTable($tableName,$status);
    if($status != SUCCESS)
      return $status;

    $extrafields = Array('tableName' => "'$tableName'");
    $status = parent::Insert($fields,$extrafields);
    if($status != SUCCESS)
      return $status;


    // do transaction
    $status = $this->dbDriver->DoTransaction($transaction);
    if($status != SUCCESS)
    {
      $userMan = new UM_User($this->module);
      $userMan->FlushUserSessions();
    }
    return $status;
  }

  function Del($fields)
  {
    $SQL = 'SELECT tableName FROM ' . $this->table . ' WHERE ';
    $SQL .= 'tableId='.$fields['tableId'].' FOR UPDATE ';
    $tableName = $this->dbDriver->GetOne($SQL, $status);
    if($status != SUCCESS)
      return $status;
    if($tableName == NULL)
      return NOT_FOUND;

    $SQL = 'SELECT COUNT(*) FROM FORM_SelectField WHERE ';
    $SQL .= "optionTable='$tableName' LOCK IN SHARE MODE ";
    $ref = $this->dbDriver->GetOne($SQL,$status);
    if($status != SUCCESS)
      return $status;
    if($ref != 0)
      return FAILED;

    $status = parent::Del($fields);
    if($status != SUCCESS)
      return $status;

    $transaction = $this->DropTable($tableName,$status);
    if($status != SUCCESS)
      return $status;

    // do transaction
    $status = $this->dbDriver->DoTransaction($transaction);
    if($status != SUCCESS)
    {
      $userMan = new UM_User($this->module);
      $userMan->FlushUserSessions();
    }
    return $status;    
  }

  //--Method: GetAll
  //--Desc: 
  function GetAll($dependence, $static, & $status)
  {
    $SQL = 'SELECT tableId, value as name, dependence, static ';
    $SQL .= 'FROM ' .  $this->table . ', LANG_Label WHERE ';
    $SQL .= "name=labelId AND lang='".$this->module->pageBuilder->lang."' ";
    if(!$dependence)
      $SQL .= "AND dependence <> '1' ";
    if(!$static)
      $SQL .= "AND static <> '1' ";
    $SQL .= 'ORDER by value LOCK IN SHARE MODE ';
    return $this->dbDriver->GetAll($SQL, $status);
  }

  function GetTableInfo($tableId, & $status)
  {
    $SQL = 'SELECT tableName, keyField FROM ' .  $this->table . ' WHERE ';
    $SQL .= "tableId=$tableId LOCK IN SHARE MODE ";
    $info = $this->dbDriver->GetRow($SQL, $status);
    if($status != SUCCESS)
      return $status;
    if(count($info) == 0)
    {
      $status = NOT_FOUND;
      return NULL;
    }
    return $info;
  }

  function GetTableId($tableName, & $status)
  {
    $SQL = 'SELECT tableId FROM ' .  $this->table . ' WHERE ';
    $SQL .= "tableName='$tableName' LOCK IN SHARE MODE ";
    $tableId = $this->dbDriver->GetOne($SQL, $status);
    if($status != SUCCESS)
      return NULL;
    if($tableId==NULL)
    {
      $status = NOT_FOUND;
      return NULL;
    }
    return $tableId;
  }
}
?>