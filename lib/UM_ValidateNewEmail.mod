<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_ValidateNewEmail extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'emailFieldName';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'id';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['stripSlashes'] = false;
  }

  protected function Execute()
  {
    $user = new UM_User($this);
    $this->attrs['email'] = 
      $user->ValidateNewEmail($this->fields['emailFieldName'],
			      $this->fields['id']);
  }
}

?>