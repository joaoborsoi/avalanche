<?php

use Ddeboer\Imap\Server;

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');
require_once('LIB_Document.inc');

class LEGIS_ImportMail extends AV_DBOperationObj
{
  protected $doc;
  protected $docFiles;

  function Execute()
  {
    // efetua login no sistema solinox com usuário importador
    $login = $this->pageBuilder->siteConfig->getVar("config",
						    "scanImportUser");
    $password = $this->pageBuilder->siteConfig->getVar("config",
						       "scanImportPasswd");

    $um = new UM_User($this);
    $um->Login($login,$password);

    $this->doc = new LIB_Document($this);
    $this->doc->LoadFieldsDef(NULL,'/mail/');

    $this->docFiles = new LIB_Document($this);
    $this->docFiles->LoadFieldsDef(NULL,'/files/');

    $host = $this->pageBuilder->siteConfig->getVar("config",
						   "mailManagerHost");
    $login = $this->pageBuilder->siteConfig->getVar("config",
						    "mailManagerLogin");
    $password = $this->pageBuilder->siteConfig->getVar("config",
						       "mailManagerPassword");


    $server = new Server($host);
    $connection = $server->authenticate($login, $password);

    $mailbox = $connection->getMailbox('INBOX');

    $messages = $mailbox->getMessages();

    try
    {
      foreach ($messages as $message) 
      {
	// $message is instance of \Ddeboer\Imap\Message
	try
	{
	  $this->ProcessMessage($message);
	  
	  $this->dbDriver->ExecSQL('COMMIT');
	}
	catch(AV_Exception $e)
	{
	  $this->dbDriver->ExecSQL('ROLLBACK');

	  if($e->getCode()==NOT_FOUND)
	    $errorMsg = 'Nenhum cadastro associado a mensagem foi encontrado';
	  else
	    $errorMsg = $e->getMessage();
	  $this->SendReply($message,"<p><b>Erro:</b> $errorMsg</p>");
	}

	$message->delete();

	$this->dbDriver->ExecSQL('BEGIN');

      } // foreach($messages as $message)
      $connection->close(CL_EXPUNGE);
    } 
    catch(Exeception $e)
    {
      $connection->close(CL_EXPUNGE);
      throw $e;
    }

  }

  protected function SendReply( & $message, $reply)
  {
    $from = $message->getFrom();
    if(!$from)
      throw new AV_Exception(FAILED,$this->lang);

    $mailTo = $from->getAddress();
    $mailFrom = $this->pageBuilder->siteConfig->getVar("cms", "mailFrom");
    $subject = 'Re: '.$message->getSubject();
	
    $body = "<!DOCTYPE html PUBLIC \"-";
    $body .= "//W3C//DTD XHTML 1.0 Strict//EN\" ";
    $body .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
    $body .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>";

    $body .= $reply;

    $body .= '<p><i>Em '.$message->getDate()->format('d/m/Y H:i').', '.$from->getFullAddress();
    $body .= ' escreveu: </i></p>';
    $body .= '<blockquote>';
    $body .= nl2br($message->getBodyText());
    $body .= '</blockquote>';
    $body .= '</body></html>';
    	 	
    $headers = "From: $mailFrom\r\n";
    $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";

    // NOW SEND
    if(!mail($mailTo, $subject, $body, $headers ,"-r".$mailFrom))
    {
      // Se "não for Postfix"
      $headers .= "Return-Path: " . $mailFrom . "\n";
      if(!mail($mailTo, $subject, $body, $headers))
  	throw new AV_Exception(FAILED, $this->lang);
    }
  }


  protected function ProcessMessage(& $message)
  {
    $login = $this->pageBuilder->siteConfig->getVar("config",
						    "mailManagerLogin");

    // variável para armazenar endereços relacionados (from, to e cc) para 
    // serem usados na pesquisa
    $addressList = array();

    // variável com campos do email para ser inserido no sistema
    $fields = array();

    $fields['path'] = '/mail/';
    $fields['fromAddress'] = array();
    $fields['toAddress'] = array();
    $fields['ccAddress'] = array();

    // processa campo from
    $inTo = false;
    $from = $message->getFrom();
    if($from)
    {
      if($from->getName())
	$fields['fromAddress']['name'] = $from->getName();
      $fields['fromAddress']['mailbox'] = $from->getMailbox();
      $fields['fromAddress']['hostname'] = $from->getHostname();
      
      $addressList[] = $from->getAddress();
    }
    //$fields['fromAddress'] = json_encode($fields['fromAddress'],JSON_UNESCAPED_UNICODE);
    $fields['fromAddress'] = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
	return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
      }, json_encode($fields['fromAddress']));

    // processa campo to
    $toList = $message->getTo();
    if($toList) 
    {
      foreach($toList as & $to)
      {
	$toAddress = array();
	if($to->getName())
	  $toAddress['name'] = $to->getName();
	$toAddress['mailbox'] = $to->getMailbox();
	$toAddress['hostname'] = $to->getHostname();
	$fields['toAddress'][] = $toAddress;
	
	$address = $to->getAddress();
	$addressList[] = $address;
	
	if($address == $login)
	  $inTo = true;
      }
    }
    //$fields['toAddress'] = json_encode($fields['toAddress'],JSON_UNESCAPED_UNICODE);
    $fields['toAddress'] = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
	return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
      }, json_encode($fields['toAddress']));

    // processa campo cc
    $ccList = $message->getCc();
    if($ccList) 
    {
      foreach($ccList as & $cc)
      {
	$ccAddress = array();
	if($cc->getName())
	  $ccAddress['name'] = $cc->getName();
	$ccAddress['mailbox'] = $cc->getMailbox();
	$ccAddress['hostname'] = $cc->getHostname();
	$fields['ccAddress'][] = $ccAddress;
	
	$addressList[] = $cc->getAddress();
      }
    }
    //$fields['ccAddress'] = json_encode($fields['ccAddress'],JSON_UNESCAPED_UNICODE);
    $fields['ccAddress'] = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
	return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
      }, json_encode($fields['ccAddress']));
    
    // recupera texto da mensagem
    $fields['message'] = $message->getBodyText();      
    
    // caso a mensagem tenha sido enviada ao sistema pelo campo "to"
    // procura por e-mails no conteúdo da mensagem
    if($inTo)
    {
      $pattern = '/[a-z0-9_\-\+]+@[a-z0-9\-]+\.([a-z]{2,3})(?:\.[a-z]{2})?/i';
      
      // preg_match_all returns an associative array
      preg_match_all($pattern, $fields['message'], $matches);
      
      $addressList = array_merge($addressList,$matches[0]);
    }
    
    $fields['subject'] = $message->getSubject();

    $fields['msgDate'] = $message->getDate()->format('Y-m-d H:i:s');

    // busca entidades associadas com a mensagem
    if(count($addressList)==0)
      throw new AV_Exception(NOT_FOUND,$this->lang,
			     array('addressList'=>$addressList));

    $addressList = "'".implode("','",$addressList)."'";

    // busca clientes / fornecedores /colaboradores
    $SQL = "SELECT DISTINCT docId FROM LIB_DocEmailList WHERE email IN ($addressList)";
    $fields['entityList'] = $this->dbDriver->GetCol($SQL);

    switch(count($fields['entityList']))
    {
    case 0:
      throw new AV_Exception(NOT_FOUND,$this->lang);
    case 1:
      $this->SendReply($message,'<p><b>Aviso:</b> '.
		       'Somente um envolvido na mensagem foi encontrado</p>');
      break;
    }


    // anexos
    $fields['attachments'] = $this->ProcessAttachments($message);

    $this->doc->Insert($fields);
  }

  protected function ProcessAttachments(& $message) 
  {
    $tmpDir = $this->pageBuilder->siteConfig->getVar("dirs", "tmp");
    $docFields = array();
    $fileList = array();

    $docFields['path'] = '/files/';
    $attachments = $message->getAttachments();
    
    foreach ($attachments as $attachment) 
    {
      // $attachment is instance of \Ddeboer\Imap\Message\Attachment

      $filePath = tempnam($tmpDir, "");
      //$fileName = substr($filePath, strlen($tmpDir));

      if(!file_put_contents($filePath,
			    $attachment->getDecodedContent()))
      {
	unlink($filePath);
	throw new AV_Exception(FAILED,$this->pageBuilder->lang,
			       array('addMsg'=>' (erro ao salvar anexo)'));
      }

      $fileName = addslashes($attachment->getFilename());

      $docFields['title'][0]['lang'] = 'pt_BR';
      $docFields['title'][0]['value'] = $fileName;
      $docFields['content']['error'] = UPLOAD_ERR_OK;
      $docFields['content']['tmp_name'] = $filePath;
      $docFields['content']['name'] = $fileName;
      $docFields['content']['type'] = mime_content_type($filePath);
      $docFields['content']['size'] = filesize($filePath);
      $docEdital = array();
      $docEdital['docId'] = $this->docFiles->Insert($docFields);
      $this->dbDriver->ExecSQL('COMMIT');
      $this->dbDriver->ExecSQL('BEGIN');
      $fileList[] = $docEdital;
    }

    return $fileList;
  }
}

?>