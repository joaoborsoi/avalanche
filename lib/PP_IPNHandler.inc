<?php

require_once('PP_IPNListener.mod');
/**
 * Interface para definição de um manipulador de Notificação
 * de Pagamento Instantânea.
 */
abstract class PP_IPNHandler
{
  protected $ipnListener;
  protected $pageBuilder;
  protected $dbDriver;
  
  function __construct(PP_IPNListener &$ipnListener)
  {
    $this->ipnListener = & $ipnListener;
    $this->pageBuilder = & $ipnListener->pageBuilder;
    $this->dbDriver = & $ipnListener->dbDriver;
  }

  /**
   * Manipula uma notificação de pagamento instantânea recebida
   * pelo PayPal.
   * @param   boolean $isVerified Identifica que a mensagem foi
   * verificada como tendo sido enviada pelo PayPal.
   * @param   array $message Mensagem completa enviada pelo
   * PayPal.
   */
  abstract function Handle(array $message);
}