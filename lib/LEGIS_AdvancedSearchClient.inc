<?php

require_once('LEGIS_AdvancedSearch.inc');

class LEGIS_AdvancedSearchClient extends LEGIS_AdvancedSearch
{
  function SearchClient(& $params, $limit, $offset, &$totalResults,
			$email = false, $tableName = NULL)
  {
    if(count($params) == 0)
      throw new AV_Exception(INVALID_FIELD,$this->lang);

    if($tableName != NULL)
      $SQL = "INSERT INTO $tableName (entityId) ";

    $onlyNaturalPerson = false;
    $SQL .= $this->SearchClientType('EM_NaturalPerson',$params,NULL,NULL,$totalResults,
				    $email,$tableName,$onlyNaturalPerson);

    if(!$onlyNaturalPerson)
    {
      $SQL .= ' UNION ';
      $SQL .= $this->SearchClientType('EM_LegalEntity',$params,NULL,NULL,$totalResults2,
				      $email,$tableName,$onlyNaturalPerson);
      $totalResults += $totalResults2;
    }

    if($limit != NULL)
    {
      $SQLLIMIT = ' LIMIT ';
      if(isset($offset))
	$SQLLIMIT .= "$offset,";
      $SQLLIMIT .= $limit;
    }

    $SQL .= $SQLLIMIT;

    // saída para etiquetas
    if($tableName != NULL)
    {
      $this->dbDriver->ExecSQL($SQL);
      return NULL;
    }

    if($email)
      return $this->dbDriver->GetCol($SQL);

    $rows = $this->dbDriver->GetAll($SQL);

    foreach($rows as &$row)
    {
      $SQL = 'SELECT ps.situationId, ps.name ';
      $SQL .= 'FROM LEGIS_CustomerSituation cs, LEGIS_PersonSituation ps ';
      $SQL .= "WHERE cs.docId='".$row['docId']."' AND cs.situationId=ps.situationId ";
      $row['situationId'] = $this->dbDriver->GetAll($SQL);
    }
    return $rows;
  }

  function SearchClientType($table,& $params, $limit, $offset, &$totalResults, 
			    $email, $tableName, &$onlyNaturalPerson)
  {
    $tables[] = "$table doc";
    $tables[] = 'LIB_Folder folder';
    $tables[] = 'LIB_DocumentLink docLink';
    $tablesWhere[] = "doc.docId=docLink.docId";
    $tablesWhere[] = 'folder.folderId=docLink.folderId';
    $where = array();
    foreach($params as $param)
    {
      switch($param['param'])
      {
	// buscas somente de pessoa física
      case 'age':
	$this->AddAge($param, $tables, $tablesWhere, $where);
	$onlyNaturalPerson = true;
	break;

      case 'func':
	$this->AddClientFunction($param, $tables, $tablesWhere, $where);
	$onlyNaturalPerson = true;
	break;

      case 'profession':
	$this->AddClientProfession($param, $tables, $tablesWhere, $where);
	$onlyNaturalPerson = true;
	break;


	// buscas gerais
      case 'aggregator':
	$this->AddClientAggregator($param, $tables, $tablesWhere, $where);
	break;

      case 'city':
	$this->AddClientCity($param, $tables, $tablesWhere, $where);
	break;

      case 'state':
	$this->AddClientState($param, $tables, $tablesWhere, $where);
	break;

      case 'folder':
	$this->AddClientFolder($param, $tables, $tablesWhere, $where);
	break;

      case 'justice':
	$this->AddClientJustice($param, $tables, $tablesWhere, $where);
	break;

      case 'commitment':
	$this->AddClientCommitment($param, $tables, $tablesWhere, $where);
	break;

      case 'actionType':
	$this->AddClientActionTypes($param, $tables, $tablesWhere, $where);
	break;

      case 'local':
	$this->AddClientLocal($param, $tables, $tablesWhere, $where);
	break;

      case 'lawyer':
	$this->AddClientLawyer($param, $tables, $tablesWhere, $where);
	break;

      case 'phases':
	$this->AddClientPhases($param, $tables, $tablesWhere, $where);
	break;

      case 'subject':
	$this->AddClientSubject($param, $tables, $tablesWhere, $where);
	break;

      case 'situation':
 	$this->AddClientSituation($param, $tables, $tablesWhere, $where);
	break;

      case 'result':
 	$this->AddClientResult($param, $tables, $tablesWhere, $where);
	break;

      case 'otherPart':
 	$this->AddClientOtherPart($param, $tables, $tablesWhere, $where);
	break;

      case 'start':
	$this->AddClientStart($param, $tables, $tablesWhere, $where);
	break;

      case 'value':
	$this->AddClientValue($param, $tables, $tablesWhere, $where);
	break;

      case 'customer':
	$this->AddCustomer($param, $tables, $tablesWhere, $where);
	break;

      default:
	throw new AV_Exception(INVALID_FIELD,$this->lang,array('data'=>$params));
      }
    }

    if($tableName == NULL)
    {
      if($email)
      {
	$returnFields = 'email';
	$tables[] = 'LIB_DocEmailList del';
	$tablesWhere[] = "doc.docId=del.docId";
      }
      else
	$returnFields = "doc.docId,doc.name,folder.path";
    }
    else
      $returnFields = "doc.docId as entityId";

    return $this->Search($returnFields, $limit, $offset, 
			 $tables, $tablesWhere, $where,$totalResults);

  }

  function AddClientAggregator(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_ProcessClient pcagr, LEGIS_ProcessClient agr', $tables))
    {
      $tables[] = 'LEGIS_ProcessClient pcagr, LEGIS_ProcessClient agr';
      $tablesWhere[] = 'doc.docId = pcagr.entityId';
      $tablesWhere[] = 'pcagr.processId = agr.processId';
    }
    $where['aggregator'][] = "agr.entityId = '" . $param['aggregatorId'] . "'";
  }

  function AddAge(& $param, & $tables, & $tablesWhere, & $where)
  {
    $date = getdate();

    if($param['age']['greater'] !== '' && $param['age']['greater'] !== NULL)
    {
      $birthday['before'] = ($date['year']-$param['age']['greater']) .'-';
      $birthday['before'] .= $date['mon'] . '-' . $date['mday'];
    }
    if($param['age']['lower'] !== '' && $param['age']['lower'] !== NULL)
    {
      $birthday['after'] = ($date['year']-$param['age']['lower']) .'-';
      $birthday['after'] .= $date['mon'] . '-' . $date['mday'];
    }
    $where['age'][] = $this->GetDateParam('doc.birthday', $birthday);

  }

  function AddClientCity(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['city'][] = 
      "doc.cidade_codigo ".($param['not']?'<>':'=')." '" . $param['cidade_codigo'] . "'";
  }

  function AddClientState(& $param, & $tables, & $tablesWhere, & $where)
  {
     $where['state'][] = 
       "doc.uf_codigo ".($param['not']?'<>':'=')." '" . $param['uf_codigo'] . "'";
  }

  function AddClientFunction(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['function'][] = 
      "doc.function ".($param['not']?'NOT ':'')."like '%".$param['func']."%'";
  }

  function AddClientProfession(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['profession'][] = 
      "doc.profession ".($param['not']?'NOT ':'')."like '%".
      $param['profession']."%'";
  }

  function AddClientFolder(&$param, &$tables, &$tablesWhere, &$where)
  {
    if(!in_array('LEGIS_Process p, LEGIS_ProcessClient pc', $tables))
    {
      $tables[] = 'LEGIS_Process p, LEGIS_ProcessClient pc';
      $tablesWhere[] = 'doc.docId = pc.entityId';
      $tablesWhere[] = 'pc.processId = p.processId';
    }
    if(!in_array('LEGIS_RootProcess rp', $tables))
    {
      $tables[] = 'LEGIS_RootProcess rp';
      $tablesWhere[] = 'p.rootProcessId = rp.processId';
    }

    $expr = "rp.folder = '" . $param['folder'] . "'";
    if($param['folderYear'] != NULL)
      $expr .= " AND rp.folderYear = '" . $param['folderYear'] . "'";

    if($param['not'])
      $expr = "NOT ($expr)";
    $where['folder'][] = $expr;
  }


  function AddClientJustice(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p, LEGIS_ProcessClient pc', $tables))
    {
      $tables[] = 'LEGIS_Process p, LEGIS_ProcessClient pc';
      $tablesWhere[] = 'doc.docId = pc.entityId';
      $tablesWhere[] = 'pc.processId = p.processId';
    }
    if(!in_array('LEGIS_RootProcess rp', $tables))
    {
      $tables[] = 'LEGIS_RootProcess rp';
      $tablesWhere[] = 'p.rootProcessId = rp.processId';
    }

    $where['justice'][] = 
      "rp.justiceId ".($param['not']?'<>':'=')." '" . $param['justiceId'] . "'";
  }

  function AddClientCommitment(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Commitment cm, LEGIS_ProcessClient cpc', $tables))
    {
      $tables[] = 'LEGIS_Commitment cm, LEGIS_ProcessClient cpc';
      $tablesWhere[] = 'doc.docId = cpc.entityId';
      $tablesWhere[] = 'cpc.processId = cm.processId';
    }

    $where['commitment'][] = $this->GetDateParam('cm.date', $param['commitment']);
  }

  function AddClientActionTypes(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p, LEGIS_ProcessClient pc', $tables))
    {
      $tables[] = 'LEGIS_Process p, LEGIS_ProcessClient pc';
      $tablesWhere[] = 'doc.docId = pc.entityId';
      $tablesWhere[] = 'pc.processId = p.processId';
    }

    $where['actionTypes'][] = 
      "p.actionTypeId ".($param['not']?'<>':'=')." '" .$param['actionTypeId']."'";
  }

  function AddClientLocal(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_ProcessLocal pl, LEGIS_ProcessClient plpc', $tables))
    {
      $tables[] = 'LEGIS_ProcessLocal pl, LEGIS_ProcessClient plpc';
      $tablesWhere[] = 'doc.docId = plpc.entityId';
      $tablesWhere[] = 'pl.processId = plpc.processId';
    }

    $expr = "pl.localId = '" . $param['localId'] . "'";
    if($param['room'] !== '' && $param['room'] !== NULL)
      $expr .= " AND pl.room = '" . $param['room'] . "'";

    if($param['not'])
      $expr = "NOT ($expr)";
    $where['local'][] = $expr;
  }

  function AddClientLawyer(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_ProcessLawyer plo, LEGIS_ProcessClient plopc', $tables))
    {
      $tables[] = 'LEGIS_ProcessLawyer plo, LEGIS_ProcessClient plopc';
      $tablesWhere[] = 'doc.docId = plopc.entityId';
      $tablesWhere[] = 'plopc.processId = plo.processId';
    }

    $where['lawyer'][] = 
      "plo.entityId ".($param['not']?'<>':'=')." '" . $param['lawyer'] . "'";
  }


  function AddClientPhases(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p, LEGIS_ProcessClient pc', $tables))
    {
      $tables[] = 'LEGIS_Process p, LEGIS_ProcessClient pc';
      $tablesWhere[] = 'doc.docId = pc.entityId';
      $tablesWhere[] = 'pc.processId = p.processId';
    }
    $where['phases'][] = 
      "p.phasesId ".($param['not']?'<>':'=')." '" . $param['phasesId'] . "'";
  }


  function AddClientSubject(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p, LEGIS_ProcessClient pc', $tables))
    {
      $tables[] = 'LEGIS_Process p, LEGIS_ProcessClient pc';
      $tablesWhere[] = 'doc.docId = pc.entityId';
      $tablesWhere[] = 'pc.processId = p.processId';
    }
    $where['subject'][] = 
      "p.subjectId ".($param['not']?'<>':'=')." '" . $param['subjectId'] . "'";
  }

  function AddClientSituation(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p, LEGIS_ProcessClient pc', $tables))
    {
      $tables[] = 'LEGIS_Process p, LEGIS_ProcessClient pc';
      $tablesWhere[] = 'doc.docId = pc.entityId';
      $tablesWhere[] = 'pc.processId = p.processId';
    }
    $where['situation'][] = 
      "p.situationId ".($param['not']?'<>':'=')." '" . $param['situationId'] . "'";
  }

  function AddClientResult(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p, LEGIS_ProcessClient pc', $tables))
    {
      $tables[] = 'LEGIS_Process p, LEGIS_ProcessClient pc';
      $tablesWhere[] = 'doc.docId = pc.entityId';
      $tablesWhere[] = 'pc.processId = p.processId';
    }
    $where['result'][] = 
      "p.resultId ".($param['not']?'<>':'=')." '" . $param['resultId'] . "'";
  }

  function AddClientOtherPart(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p, LEGIS_ProcessClient pc', $tables))
    {
      $tables[] = 'LEGIS_Process p, LEGIS_ProcessClient pc';
      $tablesWhere[] = 'doc.docId = pc.entityId';
      $tablesWhere[] = 'pc.processId = p.processId';
    }
    $where['otherPart'][] = "p.otherPart ".($param['not']?'NOT ':'').
      "like '%" . $param['otherPart'] . "%'";
  }

  function AddClientStart(& $param, & $tables, & $tablesWhere, & $where)
  {
    $SQL = "(SELECT MIN(h.date) FROM LEGIS_History h, LEGIS_ProcessClient hpc ";
    $SQL .= "WHERE doc.docId = hpc.entityId ";
    $SQL .= "AND hpc.processId = h.processId)";
    $where['start'][] = $this->GetDateParam($SQL, $param['start']);

  }

  function AddClientValue(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p, LEGIS_ProcessClient pc', $tables))
    {
      $tables[] = 'LEGIS_Process p, LEGIS_ProcessClient pc';
      $tablesWhere[] = 'doc.docId = pc.entityId';
      $tablesWhere[] = 'pc.processId = p.processId';
    }

    $expr = '';
    if($param['value']['lower'] !== '' && $param['value']['lower'] !== NULL)
      $expr = "'" . $param['value']['lower'] . "' >= p.cost";

    if($param['value']['greater'] !== '' && $param['value']['greater'] !== NULL)
    {
      if($expr != '')
	$expr .= ' AND ';
      $expr .= "p.cost >= '" . $param['value']['greater'] . "'";
    }
    $where['cost'][] = $expr;

  }

  function AddCustomer(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['customer'][] = 
      "doc.docId ".($param['not']?'<>':'=')." '" . $param['customer'] . "'";
  }


}

?>