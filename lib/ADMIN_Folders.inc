<?php

require_once('ADMIN_Page.inc');

class ADMIN_Folders extends ADMIN_Page
{
  protected $rootPath;
  protected $langFolder;
  protected $link;
  protected $script;
  protected $script2;
  protected $path;
  
  function __construct(& $pageBuilder, $fixedOrder, $link, $rootPath = '', 
		       $langFolder = false)
  {
    if(mb_substr($rootPath,-1) == '/')
      $rootPath = mb_substr($rootPath,0,-1);
    $this->rootPath = $rootPath;
    $this->langFolder = $langFolder;
    $this->fixedOrder = $fixedOrder;
    $this->link = $link;
    if($_REQUEST['path'] != '' && trim($_REQUEST['path']) != '/')
    $this->path = $_REQUEST['path'];
    if(mb_substr($this->path,-1) != '/')
      $this->path .= '/';
    parent::__construct($pageBuilder);
  }


  protected function LoadPage()
  {    
    $this->pageBuilder->rootTemplate('adminFolders');
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');
    $pageLabels = & $this->LoadModule("LANG_GetPageLabels");
    $pageLabels->PrintHTML($this->pageBuilder->rootTemplate,NULL,
			   'htmlentities_utf');
    $_GET['labelId'] = 'adminRootDirTag';
    $_GET['lang'] = $this->lang;
    $rootLabel = &$this->LoadModule('LANG_GetLabel');
    
    $this->script = "var n0=new WebFXTree('".$rootLabel->attrs['value']."','";
    $this->script .= $this->link."');\n";
    $this->script .= "n0.setBehavior('explorer');\n";
    $this->script .= "n0.target='ifiles';\n";
    

    $node = 0;
    $this->LoadNode('/', $node);
    $this->script .= "document.write(n0.toString());\n";
    $this->script .= $this->script2;
    $this->pageBuilder->rootTemplate->addText((string)$this->script,
					      'folders');      
  }
  
  protected function LoadNode($path, &$node)
  {
    $_GET['path'] = $this->rootPath . addslashes($path);
    $_GET['langFolder'] = $this->langFolder;
    $_GET['fixedOrder'] = ($this->fixedOrder)?'1':'0';
    $getFolder = $this->LoadModule('LIB_GetFolderList');

    $parentNode = $node;
    foreach ($getFolder->children as $key=>$child)
    {
      if(!$this->langFolder)
	$folderName = $child->attrs['name'];
      else
	$folderName = $child->attrs['label'];
      
      $folderId = $child->attrs['folderId'];
      $subPath = $path . $child->attrs['name']."/";
      $link = $this->link .'&path=' . urlencode($subPath);           
      $newNode = 'n'.(++$node);
      
      $this->script .= "var ".$newNode." = new WebFXTreeItem('";
      $this->script .=  addslashes($folderName)."', '".$link."');\n ";
      $this->script .= $newNode.".target='ifiles';\n";
      $this->script .= "n".$parentNode.".add(".$newNode.");\n";

      if($this->path != '/' && 
	 mb_substr($this->path,0,mb_strlen($subPath))==$subPath)
	$this->script2 .= $newNode.".expand();\n";

      $this->LoadNode($subPath, $node);
    }
  }
  
}

?>
