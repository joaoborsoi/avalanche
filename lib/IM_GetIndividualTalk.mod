<?php

require_once('AV_DBOperationObj.inc');
require_once('IM_Manager.inc');

class IM_GetIndividualTalk extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'userId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

  }

  protected function Execute()
  {
    $im = new IM_Manager($this);

    // get rows
    $this->attrs['id'] = $im->GetIndividualTalk($this->fields);
  }
}

?>