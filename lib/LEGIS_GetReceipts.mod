<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Process.inc');

class LEGIS_GetReceipts extends AV_DBOperationObj
{
  function LoadFieldsDef(& $status)
  {
    $this->fieldsDef[0]['fieldName'] = 'processId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'entityId';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['consistType'] = 'integer';
  }
  
  function Execute()
  {
    $process =  new LEGIS_Process($this);
    $this->attrs['value'] = 
      $process->GetReceiptList($this->fields['processId'],
			       $this->fields['entityId']);
  } 
}

?>