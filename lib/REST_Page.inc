<?php

require_once('JSON_Page.inc');

class REST_Page extends JSON_Page
{
  function &LoadModule($name, $transaction = true)
  {
    return parent::LoadModule($name,NULL,NULL,NULL,true,$transaction);
  }

  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $data = $this->Run();
    }
    catch(Exception $e)
    {
      $data['statusMessage'] = $e->getMessage();
      $data['statusCode'] = $e->getCode();
      $data['data'] = $e->getData();
      $data['file'] =  $e->getFile();
      $data['line'] =  $e->getLine();

      switch($e->getCode())
      {
      case PERMISSION_DENIED:
	header("HTTP/1.0 401 Unauthorized");
	break;

      case INDEX_VIOLATED:
      case ALREADY_EXISTS:
	header("HTTP/1.0 409 Conflict");
	break;

      case NOT_FOUND:
	header("HTTP/1.0 404 Not Found");
	break;

      case INVALID_RET_FIELD:
      case FOLDER_NOT_EMPTY:
      case INVALID_PATH:
      case INVALID_FIELD:
      case FIELD_REQUIRED:
      case READ_ONLY_PROP:
      case STATIC_PROP:
	header("HTTP/1.0 400 Bad request");
	break;

      case MAINTENANCE:
      case FATAL_ERROR:
      case UNKNOWN_ERROR:
      case FAILED:
      default:
	header("HTTP/1.0 500 Internal Server Error");
      }
    }

    // SUCCESS
    $this->pageBuilder->rootTemplate->addText(json_encode($data),'content');
  }

  protected function Run()
  {
    switch ($_SERVER['REQUEST_METHOD']) 
    {
    case "GET":
      return $this->Get();

    case "POST":
      return $this->Post();

    case "PUT":
      return $this->Put();

    case "DELETE":
      return $this->Delete();
    }
  }

  protected function EncodeModObject($modObject)
  {
    $results['attrs'] = $modObject->attrs;
    $results['children'] = array();
    foreach($modObject->children as & $child)
      $results['children'][] = $this->EncodeModObject($child);
    return $results;
  }

  protected function EncodeModObjectChildren($module,$fields,$handler)
  {
    $results = array();
    foreach($module->children as & $child)
    {
      if($handler!=NULL)
	$handler($child);
      if(count($fields)>0)
      {
	$row  = array();
	foreach($fields as $fieldName)
	  $row[$fieldName] = $child->attrs[$fieldName];
	$results[] = $row;
      }
      else
	$results[] = $child->attrs;
    }
    return $results;
  }

  

  protected function Get()
  {
    throw new AV_Exception(FAILED,$this->lang);
  }

  protected function Post()
  {
    throw new AV_Exception(FAILED,$this->lang);
  }

  protected function Put()
  {
    throw new AV_Exception(FAILED,$this->lang);
  }

  protected function Delete()
  {
    throw new AV_Exception(FAILED,$this->lang);
  }

}

?>