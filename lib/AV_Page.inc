<?php

abstract class AV_Page
{
  public $pageBuilder;
  protected $lang;
  protected $userSession;
  protected $pageId;

  function __construct(& $pageBuilder)
  {
    $this->pageBuilder = & $pageBuilder;
    $this->lang = $pageBuilder->lang;
    $this->pageId = $pageBuilder->pageId;
    try
    {
      $this->UpdateUserSession();
      $this->LoadPage();
      $this->Header();
    }
    catch(Exception $e)
    {
      $this->Header();
      throw $e;
    }
  }

  function LoadModule($name, $itemTpl, $itemTplTag, $varPrefix, 
		       $jsonInput = false, $transaction = true)
  {
    require_once($name . '.mod');
    return new $name($this->pageBuilder, $itemTpl, $itemTplTag, $varPrefix, 
		     $jsonInput,$transaction);
  }


  abstract protected function LoadPage();


  protected function UpdateUserSession()
  {
    try
    {
      $this->userSession = $this->LoadModule('UM_UpdateUserSession');
    }
    catch(Exception $e)
    {
      if($e->getCode()!=PERMISSION_DENIED)
	throw $e;
    }
  }

  protected function DisableCache()
  {
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  // Date in the past
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");                          // HTTP/1.0
    session_cache_limiter("");
    header("Cache-control: private");
  }

  protected function Header()
  {
    // by default, disable cache
    $this->DisableCache();
    header('Content-Type: text/html; charset=utf-8');
  }


}

?>