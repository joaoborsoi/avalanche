<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_EntryReport.inc');

class LEGIS_GetEntryReport extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'fromDate';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = true;

    $this->fieldsDef[1]['fieldName'] = 'toDate';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = true;
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $report = new LEGIS_EntryReport($this);
    $this->attrs['docId'] = $report->Get($this->fields['fromDate'],$this->fields['toDate']);
  }
}

?>