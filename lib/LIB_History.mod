<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Document.inc');

class LIB_History extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  protected function &CreateDocument()
  {
    return new LIB_Document($this);
  }

  protected function Execute()
  {
    $this->isArray = true;

    $doc = & $this->CreateDocument();
    $rows = & $doc->GetHistory($this->fields['docId']);

    // load children
    $this->LoadChildrenFromRows($rows, 'history');
  }
}

?>
