<?php

require_once('CMS_Content.inc');

class SHOP_Item extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    // loads document title
    $this->pageBuilder->rootTemplate->addText($fields['title'],'title');

    // header
    $content = '<h1>'.$fields['title']."</h1>\n";

    // content
    $content .= $fields['content'];

   // preço e botão de compra
    $content .= "<p>\n";
    $content .= "<strong class='price'>R$".$fields['price']."</strong>\n";
    if(!$fields['unavailable'])
    {
      $_GET['labelId'] = 'buyTag';
      $_GET['lang'] = $this->lang;
      $getLabel = $this->page->LoadModule('LANG_GetLabel');
      $content .= "<a href='shopCart.av?action=addToCart";
      $content .= "&amp;docId=".$fields['docId']."' class='buyButton'>";
      $content .= $getLabel->attrs['value']."</a>\n";
    }
    else
    {
      $_GET['labelId'] = 'unavailableTag';
      $_GET['lang'] = $this->lang;
      $getLabel = $this->page->LoadModule('LANG_GetLabel');
      $content .= "<span class='unavailable'>".$getLabel->attrs['value'];
      $content .= "</span>\n";
    }
    $content .= "</p>\n";

    // load products images
    $content .= $this->page->LoadImages($fields['imageList']);


    $content .= $this->LoadRelatedProducts($fields);


     // loads content to the page
    $this->pageBuilder->rootTemplate->addText($content,'mainContent');

  }

  function LoadRelatedProducts(& $fields)
  {
    // search for files at the folder
    $_GET['path'] = '/content/';
    $_GET['orderBy'] = 'matches';
    $_GET['filter'] = addslashes($fields['keywords']);
    $_GET['searchCriteria'] = 'keywords';
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = 'title,price,imageList';
    $_GET['limit'] = 
      $this->pageBuilder->siteConfig->getVar('cms',
					     'resultsRelatedProds');

    $_GET['exclusionFilters_numOfOptions'] = 1;
    $_GET['exclusionFilters_1__fieldName'] = 'docId';
    $_GET['exclusionFilters_1__value'] = $fields['docId'];
    $_GET['exclusionFilters_1__operator'] = '<>';

    $search = $this->page->LoadModule('LIB_Search');
    if($search->attrs['totalResults']>0)
    {
      // label de produtos relacionados
      $_GET['labelId'] = 'relatedProducts';
      $_GET['lang'] = $this->lang;
      $getLabel = $this->page->LoadModule('LANG_GetLabel');
      $content = "<h2 class='subsection'>";
      $content .= $getLabel->attrs['value']."</h2>\n";

      $content .= $this->page->LoadSearchInfo($search,$_REQUEST['offset'],
					      $_GET['limit']);

      // carrega lista dos produtos
      $content .= $this->LoadProductList($search);

      $content .= $this->page->LoadSearchNav($search,$_REQUEST['offset'],
					     $_GET['limit'],
					     $_SERVER['SCRIPT_URL']);
    }
    return $content;
  }


  function LoadArea(&$fields,$path)
  {
    parent::LoadArea($fields,$path);

    // search for files at the folder - content list
    $_GET['path'] = $path;
    $_GET['orderBy'] = 'fixedOrder';
    $_GET['returnFields'] = 'title,price,imageList';
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsPerPage');
    $search = $this->page->LoadModule('LIB_Search');

    if($search->attrs['totalResults'] > 0)
    {
      $_GET['labelId'] = 'ourProductsTag';
      $_GET['lang'] = $this->lang;
      $getLabel = $this->page->LoadModule('LANG_GetLabel');

      $content = "<h2 class='subsection'>";
      $content .= $getLabel->attrs['value']."</h2>\n";
      $content .= $this->page->LoadSearchInfo($search,$_REQUEST['offset'],
					      $_GET['limit']);

      $content .= $this->LoadProductList($search);

      $encMenuPath = str_replace("%2F", "/", 
				 urlencode(stripslashes($this->menuPath)));
      $content .= $this->page->LoadSearchNav($search,$_REQUEST['offset'],
					     $_GET['limit'],
					     'area/'.$encMenuPath);
    }

    // subareas
    $content .= $this->page->LoadSubareas();

    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }

  protected function LoadProductList($search)
  {
    $content .= "<ol class='product'>\n";
    foreach($search->children as & $child)
    {
      $content .= "<li>\n";
      $content .= $this->LoadListItem($child->attrs);
      $content .= "</li>\n";
    }
    $content .= "</ol>\n";
    return $content;
  }

  function LoadListItem(& $fields)
  {
    $imageId = $fields['imageList'];
    $content .= "<p><a href='content/".$fields['docId']."'>\n";;
    $content .= "<img src='thumb/$imageId' alt='";
    $content .= $fields['title']."' ";
    $content .= "title='".$this->pageLabels->attrs['detailsTag']."' /></a>\n";
    $content .= "</p>\n";
    $content .= '<h4>'.$fields['title']."</h4>\n";

    // preço e botão de compra
    $content .= "<p>\n";
    $content .= "<strong class='price'>R$".$fields['price'];
    $content .= "</strong>\n"."<a href='content/".$fields['docId'];
    $content .= "' class='buyButton'>".$this->pageLabels->attrs['detailsTag'];
    $content .= "</a>\n</p>\n";
    return $content;
  }
}

?>
