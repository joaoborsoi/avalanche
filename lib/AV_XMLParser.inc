<?php

function AV_XMLParser($prefix)
{
  global $xml2mime_prefix;
  global $xml2mime_level;
  global $varPrefix;
  global $_XML;

  $varPrefix = $prefix;
  $xml2mime_prefix = array();
  $xml2mime_level = 0;
  $_XML = array();
  $xml_parser = xml_parser_create();
  
  xml_set_element_handler($xml_parser, 'StartElement', 'EndElement');
  
  if (!xml_parse($xml_parser, $data))
  {
    die(sprintf("XML error: %s at line %d",
		xml_error_string(xml_get_error_code($xml_parser)),
		xml_get_current_line_number($xml_parser)));
  }
  xml_parser_free($xml_parser);
}


function StartElement($parser, $name, $attrs)
{
  global $xml2mime_prefix;
  global $xml2mime_level;
  global $varPrefix;
  global $_XML;

  switch($xml2mime_level)
  {
  case 0:

    break;

  case 1:
    $xml2mime_prefix[$xml2mime_level] = $prefixFlag . '__';
    break;
    
  default:
    $xml2mime_prefix[$xml2mime_level] = $xml2mime_prefix[$xml2mime_level - 1] .
      $name . '__';
    break;
  }

  foreach($attrs as $key => $attr)
  {
    $varName = $xml2mime_prefix[$xml2mime_level] . $key;
    $_XML[$varName] = htmlentities_utf(urlencode($attr));
  }
  $xml2mime_level++;
}

function EndElement($parser, $name)
{
  global $xml2mime_level;
  $xml2mime_level--;
}





?>