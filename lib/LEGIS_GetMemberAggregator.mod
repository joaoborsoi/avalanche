<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Process.inc');

class LEGIS_GetMemberAggregator extends AV_DBOperationObj
{
  function Execute()
  {
    $process =  new LEGIS_Process($this);
    $this->attrs =  $process->GetMemberAggregator();
  }
  
}

?>