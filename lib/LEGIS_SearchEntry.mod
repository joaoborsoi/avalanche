<?php

require_once('LIB_Search.mod');
require_once('LEGIS_EntryDoc.inc');

class LEGIS_SearchEntry extends LIB_Search
{
  protected function &CreateDocument()
  {
    return new LEGIS_EntryDoc($this);
  }
}

?>
