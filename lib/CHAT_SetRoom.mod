<?php 

require_once("AV_DBOperationObj.inc");
require_once("CHAT_Manager.inc");

class CHAT_SetRoom extends AV_DBOperationObj
{
 
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'name';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;

    $this->fieldsDef[1]['fieldName'] = 'maxMembers';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;

    $this->fieldsDef[2]['fieldName'] = 'descr';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;

  }

  function Execute()
  {
    $manager = new  CHAT_Manager($this);
    $manager->LoadMessages();
    return $manager->SetRoom($this->fieldParser->fields['name'],
                             $this->fieldParser->fields['maxMembers'],
			     $this->fieldParser->fields['descr']);
  
  }
}

?>
