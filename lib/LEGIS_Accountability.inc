<?php

require_once('L2PDF_Document.inc');
require_once('LIB_Document.inc');

class LEGIS_Accountability
{
  protected $module;
  protected $dbDriver;

  function __construct(& $module)
  {
    $this->module = &$module;
    $this->dbDriver = & $module->dbDriver;
  }

  function Get($params)
  {
    $locale = localeconv();

    // cria documento para prestação de contas
    $acc =  new L2PDF_Document($this->module->pageBuilder); 

    $this->Init($acc);

    $acc->Write('{\noindent\Large\textbf{'.$acc->Escape($params['title']).'}}');
    $acc->Write('\newline');

    $acc->Write('\begin{tabularx}{\linewidth}{@{\extracolsep{\fill}} X r r r r r r',false);
    foreach($params['extraCols'] as $col)
      $acc->Write(' r',false);
    $acc->Write(' r r r r r @{\extracolsep{\fill}}}');
    $acc->Write('\textbf{Cliente}&\textbf{Homologado}&\textbf{\%}&\textbf{Alvará}&\textbf{INSS}&\textbf{Imposto de renda}&\textbf{FGTS}',false);
    foreach($params['extraCols'] as $col)
      $acc->Write('&\textbf{'.$acc->Escape($col['title']).'}',false);
    $acc->Write('&\textbf{Valor Bruto}&\textbf{Honorários}&\textbf{Despesas}&\textbf{\%}&\textbf{Líquido}\\\\');
    $acc->Write('\arrayrulecolor{camara}');
    $acc->Write('\hline\hline\hline\hline');
    $acc->Write('\arrayrulecolor{black}');
    $acc->Write('\endhead');

    foreach($params['clientList'] as $key=>$client)
    {
      $acc->Write($client['name'].'&',false);
      $acc->Write(number_format($client['accepted'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      $acc->Write(number_format($client['percent'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      $acc->Write(number_format($client['charter'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      $acc->Write(number_format($client['inss'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      $acc->Write(number_format($client['ir'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      $acc->Write(number_format($client['fgts'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      foreach($params['extraCols'] as $col)
	$acc->Write(number_format($col['values'][$key],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);

      $acc->Write(number_format($client['gross'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      $acc->Write(number_format($client['fees'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      $acc->Write(number_format($client['expenses'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      $acc->Write(number_format($client['expensesPercent'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'&',false);
      $acc->Write(number_format($client['netValue'],
				$locale['frac_digits'],
				$locale['mon_decimal_point'],
				$locale['mon_thousands_sep']).'\\\\');
      $acc->Write('\hline');
    }
    $acc->Write('\end{tabularx}');

    $this->End($acc);

    $tmpDir = $this->module->pageBuilder->siteConfig->getVar("dirs", "tmp");
    $fileInfo = $acc->CreateDoc($tmpDir,true,true);
    try
    {
      $doc = new LIB_Document($this->module);

      $fields['path'] = '/files/';
      $fields['title'][0]['lang'] = 'pt_BR';
      $fields['title'][0]['value'] = 'Prestação de contas';
      $fields['content']['error'] = UPLOAD_ERR_OK;
      $fields['content']['tmp_name'] = $fileInfo['filePath'].'.pdf';
      $fields['content']['name'] = 'prestacao_de_contas.pdf';
      $fields['content']['type'] = 'application/pdf';
      $fields['content']['size'] = filesize($fileInfo['filePath'].'.pdf');
      $doc->LoadFieldsDef(NULL,$fields['path']);
      $docId = $doc->Insert($fields);
      $acc->UnlinkGarbage($fileInfo['fileName']);
      return $docId;
    }
    catch(Exception $e)
    {
      $acc->UnlinkGarbage($fileInfo['fileName']);
      throw $e;
    }

  }

  protected function Init(& $acc)
  {
    $acc->Write('\documentclass[a4paper]{article}');
    $acc->Write('\usepackage{fancyhdr}');
    $acc->Write('\usepackage[psamsfonts]{amsfonts}');
    $acc->Write('\usepackage[leqno]{amsmath}');
    $acc->Write('\usepackage[dvips]{graphicx}');
    $acc->Write('\usepackage[portuges]{babel}');
    $acc->Write('\usepackage[utf8]{inputenc}');
    $acc->Write('\usepackage[T1]{fontenc}');
    $acc->Write('\usepackage{ae,aecompl}');
    $acc->Write('\usepackage{ltablex}');
    $acc->Write('\usepackage[usenames]{color}');
    $acc->Write('\usepackage[dvipsnames]{xcolor}');
    $acc->Write('\usepackage{colortbl}');
    $acc->Write('\usepackage[landscape]{geometry}');
//    $acc->Write('\usepackage{lscape}');
    $acc->Write('\usepackage{pdfpages}');
    $acc->Write('\definecolor{camara}{rgb}{0.094,0.145,0.455}');
    $acc->Write('\setcounter{LTchunksize}{50}');
    $acc->Write('\renewcommand{\familydefault}{\sfdefault}');
    $acc->Write('\renewcommand{\doublerulesep}{1pt}');
    $acc->Write('\addtolength{\headheight}{0.5cm}');
    $acc->Write('\addtolength{\hoffset}{-3.5cm}');
    $acc->Write('\addtolength{\textwidth}{7.5cm}');
    $acc->Write('\addtolength{\voffset}{-1cm}');
    $acc->Write('\addtolength{\textheight}{3cm}');
    $acc->Write('\pagestyle{fancy}');
    $acc->Write('\lhead{{\huge\color{camara}Prestação de Contas}}');
    $acc->Write('\chead{}');
    $acc->Write('\rhead{\includegraphics[scale=0.7]{logo}}');
    $acc->Write('\newcommand{\topHeadRule}');
    $acc->Write('{{\color{camara}%');
    $acc->Write('\hrule height 2pt');
    $acc->Write('width\headwidth');
    $acc->Write('\vspace{5pt}}');
    $acc->Write('}');
    $acc->Write('\renewcommand\headrule{');
    $acc->Write('\topHeadRule}');
    $acc->Write('\renewcommand\footrule');
    $acc->Write('{{\color{camara}%');
    $acc->Write('\hrule height 2pt');
    $acc->Write('width\headwidth}}');
    $acc->Write('\lfoot{'.strftime('%c').'}');
    $acc->Write('\cfoot{}');
    $acc->Write('\rfoot{\thepage}');
    $acc->Write('\fancypagestyle{plain}{');
    $acc->Write('  \fancyhf{}');
    $acc->Write('  \rhead{\includegraphics{logo}}');
    $acc->Write('  \lfoot{}');
    $acc->Write('  \rfoot{\thepage}}');
    $acc->Write('\fancypagestyle{title}{');
    $acc->Write('  \fancyhf{}');
    $acc->Write('  \rhead{\includegraphics{logo}}');
    $acc->Write('  \lfoot{}');
    $acc->Write('  \rfoot{}}');
    $acc->Write('\begin{document}');
    //    $acc->Write('\begin{landscape}');
    $acc->Write('\newlength{\topTableCellWidth}');
    $acc->Write('\setlength{\topTableCellWidth}{0.25\textwidth}');
    $acc->Write('\addtolength{\topTableCellWidth}{-1.7\tabcolsep}');
    $acc->Write('\setlength\LTleft{0pt}');
    $acc->Write('\setlength\LTright{0pt}');
  }

  protected function End(& $acc)
  {
    //    $acc->Write('\end{landscape}');
    $acc->Write('\end{document}');
  }

}

?>