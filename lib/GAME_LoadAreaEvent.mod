<?php

require_once('AV_DBOperationObj.inc');

class GAME_LoadAreaEvent extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'next';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;
    $this->fieldsDef[0]['consistType'] = 'boolean';
    $this->fieldsDef[0]['default'] = false;
  }

  protected function Execute()
  {
    if($this->fields['next'])
    {
      $SQL = 'SELECT ae.id FROM UM_UserSessions us, GAME_PlayUser pu, GAME_AreaEvent ae ';
      $SQL .= "WHERE sessionId = '".$this->pageBuilder->sessionH->sessionID."' ";
      $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
      $SQL .= 'AND pu.id = playId ORDER BY ae.id LIMIT 1 FOR UPDATE';
      $id = $this->dbDriver->GetOne($SQL);

      if($id!=NULL)
	$this->dbDriver->ExecSQL("DELETE FROM GAME_AreaEvent WHERE id='$id'");
    }
    $SQL = 'SELECT areaElement, targetDiv, onLoadSuccess, name as type ';
    $SQL .= 'FROM UM_UserSessions us, GAME_PlayUser pu, GAME_AreaEvent ae, ';
    $SQL .= 'GAME_Element e, GAME_ElementType et ';
    $SQL .= "WHERE sessionId = '".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $SQL .= 'AND pu.id = playId ';
    $SQL .= 'AND areaElement=e.id AND typeId=et.id ORDER BY ae.id LIMIT 1';
    $this->attrs = $this->dbDriver->GetRow($SQL);
  }
}