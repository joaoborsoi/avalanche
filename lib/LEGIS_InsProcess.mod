<?php

require_once('LEGIS_ProcessOper.inc');

class LEGIS_InsProcess extends LEGIS_ProcessOper
{
  protected function Execute()
  {
    $docId = $this->doc->Insert($this->fields);
    $this->attrs['docId'] = $docId;
  }
}

?>