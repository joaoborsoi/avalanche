<?php


// $SM_rootDir = '/var/www/user/siteManager/';

$SM_rootDir = '';   // when set, make sure it ends with a slash!

// -----------------------------------------------------------------------------
// don't touch below here
// -----------------------------------------------------------------------------

// Version
$SM_versionTag = "2.2.0 production";
define('SM_VERSION', '2.2.0');

// turn on all warnings and notices
error_reporting(E_ALL);

// catch all output
ob_start();

// script start time
$microtime = explode(' ', microtime());
$SM_scriptStartTime = $microtime[1].mb_substr( $microtime[0], 1);

require($SM_rootDir."lib/errors.inc");
require($SM_rootDir."lib/smObject.inc");
require($SM_rootDir."lib/smConfig.inc");
require($SM_rootDir."lib/smRoot.inc");
require("AV_Utf8.inc");

//--Class: AV_PageBuilder
//--Parent: SM_siteManagerRoot
//--Desc: Responsible for creating dinamically the requested page (pageId). The
//--Desc: page creation consists in running associated modules. Each module
//--Desc: prints out status variables. Those variables may be printed on HTMLs
//--Desc: throught templates and tag replacement, or they can be just printed
//--Desc: as MIME or XML format.
class AV_PageBuilder extends SM_siteManagerRoot
{
  //
  //--Public
  //

  //--Attributes
  public $lang;        //--Desc: Stores current language
  public $pageId;             //--Desc: Stores page ID to be displayed
  public $page;
  public $varMode;            //--Desc: Array to keep current page input 
                           //--Desc: ($varMode[0]) and output ($varMode[1])
                           //--Desc: mode. (0 - HTML, 1 - form-url-encoded, 
                           //--Desc:        2 - XML)
  public $output;             //--Desc: Keeps output data.
  public $avVersion;
  public $dbDriver;

  protected $logFile;         //--Desc: Log file
  protected $jsonInput;

  //--Method: Initiate
  //--Desc: Initiates page builder
  function Initiate($siteConfig,$avVersion)
  {
    $this->jsonInput = false;
//     // ajusta timezone para utilizar UTC
//     date_default_timezone_set('UTC');

    $this->avVersion = $avVersion;

    // load in site settings
    $this->loadSite($siteConfig);

    $this->logFile = $this->siteConfig->getVar("dirs", "log").
      'avalanche.log';

    // connecto to dabase
    $this->dbConnect();

    // start session control
    $this->startSessions();

    // sets database to utf8
    $SQL = "SET NAMES utf8";
    $rh =$this->dbDriver->ExecSQL($SQL);

    // sets multbyte enconding to utf
    mb_internal_encoding("UTF-8");

    // sets lang and pageId variables
    if($_GET['lang'] != '')
      $this->SetLang($_GET['lang']);
    else if($_POST['lang'] != '')
      $this->SetLang($_POST['lang']);
    else if($this->sessionH->persistentVars['lang'] != '')
      $this->SetLang($this->sessionH->persistentVars['lang']);
    else
      $this->SetLang($this->siteConfig->getVar("defaults", "lang"));
    $this->sessionH->setPersistent('lang', $this->lang);


    // sets pageId
    if($_GET['pageId'] != '')
      $this->pageId = $_GET['pageId'];
    else if($_POST['pageId'] != '')
      $this->pageId = $_POST['pageId'];
    else
      $this->pageId = $this->siteConfig->getVar("defaults", "pageId");  

    // default varMode is form-url-enconded for input and html for output
    $this->varMode[0] = 1;
    $this->varMode[1] = 0;
  }

  function GetJsonInput()
  {
    if($this->jsonInput===false)
      $this->jsonInput = json_decode(file_get_contents("php://input"), true);
    return $this->jsonInput;
  }

  function SetLang($lang)
  {
    $this->lang = $lang;
    // sets location definitions
    setlocale(LC_ALL, $this->lang.'.utf-8');

    $SQL = "SET lc_time_names = '".$this->lang."'";
    $rh =$this->dbDriver->ExecSQL($SQL);
  }    


  //
  //
  //--Protected
  //
  function  _defineConnection($id, $dbSettings)
  {
    try
    {
      switch($dbSettings["dbType"])
      {
      case 'mysql':
	require_once("AV_PearDriver.inc");
	$this->dbHL[$id] = new AV_PearDriver($this,$dbSettings);
	break;
      case 'oracle':
	require_once("AV_OCIDriver.inc");
	$this->dbHL[$id] = new AV_OCIDriver($this,$dbSettings);
	break;
      }

      // if this is the first database connection, or they specified this
      // connection to be the default... make it so number one
      if (empty($this->dbH) || (isset($dbSettings['defaultConnection']) && ($dbSettings['defaultConnection'])) )
      {
	$this->dbH = &$this->dbHL[$id];
	$this->dbDriver = &$this->dbHL[$id];
      }
    }
    catch(Exception $e)
    {
      SM_fatalErrorPage($e->getMessage(),$this);
    }
  }

  //--Method: ReportError
  //--Desc: Report critical error by sending e-mail to the adress defined in 
  //--Desc: site configuration file (normally SiteConfig.xsm)
  protected function ReportError($message)
  {
    $addrs = $this->siteConfig->getVar("debug", "reportError");
    if($addrs != '')
      mail($addrs,"AVALANCHE ERROR", $message);
  }

  //--Method: PrintLog
  //--Desc: Prints log information
  function PrintLog($message, $reportError)
  {
    global $SM_siteID, $adminDir;
    // timestamp for the error entry
    $msg = date("Y-m-d H:i:s\n");
    $msg .= "-------------------\n";
    $msg .= 'host: ' . (isset($_SERVER['HTTP_HOST']) ? 
			$_SERVER['HTTP_HOST'] : 'command line') . "\n";
    $msg .= 'siteId: ' . $SM_siteID . "\n";
    $msg .= 'pageId: ' . $this->pageId . "\n";
    $msg .= 'adminDir: ' . $adminDir . "\n";
    $msg .= 'message: ' . $message . "\n\n";

    if($this->logFile != '')
      error_log($msg, 3, $this->logFile);

    if($reportError)
      $this->ReportError($msg);
  }


  //
  //--Private
  //

  //--Method: completePage
  //--Desc: Redefines siteManager method to implement avalanche methods
  //--Desc: execution
  function completePage()
  {
    if($this->pageId == '')
      SM_fatalErrorPage("page not defined!");

    $pagesDir = $this->siteConfig->getVar('dirs','pages');
    $fileName = $this->pageId . '.inc';

    if(is_array($pagesDir))
    {
      $found = false;
      foreach($pagesDir as $dir)
      {
	$fullFileName = $dir . $fileName;
	if(file_exists($fullFileName))
	{
	  $found = true;
	  break;
	}
      }
      if(!$found)
	SM_fatalErrorPage("page ".$this->pageId." not found!");
    }
    else
    {
      $fullFileName = $pagesDir . $fileName;
      if(!file_exists($fullFileName))
	SM_fatalErrorPage("page ".$this->pageId." not found!");
    }
     
    require_once($fullFileName);
    $this->page = new $this->pageId($this);

    if($this->varMode[1] == 2)
    {
      $this->output = "<XML VERSION=\"1.0\">\n" . $this->output;
      $this->output .= "</XML>";
    }

    if($this->rootTemplate !== NULL)
    {
      if($this->varMode[1] != 0)
	$this->rootTemplate->addText((string)$this->output, 'GetPageData');
      parent::completePage();
    }
  }


  //--Method: completePage
  //--Desc: Redefines siteManager method to implement dabase connection
  function dbConnect()
  {
    parent::dbConnect();

    // check for a proper connection
    if (!isset($this->dbH)) 
      SM_fatalErrorPage("Error with database connection!");
  }


}

require($SM_rootDir."lib/dataManip.inc");
require($SM_rootDir."lib/support.inc");
require($SM_rootDir."lib/security.inc");
require($SM_rootDir."lib/auth.inc");
require($SM_rootDir."lib/smartForm.inc");
require($SM_rootDir."lib/layoutTemplate.inc");
require($SM_rootDir."lib/modules.inc");
require($SM_rootDir."lib/smMembers.inc");
require($SM_rootDir."lib/sessions.inc");
require($SM_rootDir."lib/codePlate.inc");

if ($SM_develState)
    SM_debugLog("Initializing SiteManager...");

?>