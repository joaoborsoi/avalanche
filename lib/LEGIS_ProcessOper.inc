<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Process.inc');

abstract class LEGIS_ProcessOper extends AV_DBOperationObj
{
  protected $doc;

  protected function &CreateDocument()
  {
    return new LEGIS_Process($this);
  }

  protected function ParseInput(&$inputSources)
  {
    $this->doc = & $this->CreateDocument();

    parent::ParseInput($inputSources);

    $this->fieldsDef = $this->doc->LoadFieldsDef($this->fields['docId'], 
						 $this->fields['path'],
						 $this->fields['parentId']);

    // when docId is given (the document already existis), disable required
    if($this->fields['docId']!=NULL)
    {
      foreach($this->fieldsDef as &$fieldDef)
	$fieldDef['required'] = '0';
    }

    if(is_array($this->fieldParser))
    {
      foreach($this->fieldParser as $key => $parser)
      {
	$this->fieldParser[$key]->Parse($this->fieldsDef);
	$this->fields = array_merge($this->fields, 
				    $this->fieldParser[$key]->fields);
      }
    }
    else
    {
      $this->fieldParser->Parse($this->fieldsDef);
      $this->fields = array_merge($this->fields, $this->fieldParser->fields);
    }

  }

  //--Method: LoadFieldsDef
  //--Desc: Redefines parent method to define module's input fields
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'path';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['default'] = '/';
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'docId';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['consistType'] = 'integer';

    $this->fieldsDef[2]['fieldName'] = 'parentId';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'integer';
  }

}

?>