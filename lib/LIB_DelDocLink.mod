<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Document.inc');

class LIB_DelDocLink extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'path';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['default'] = '/';
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'docId';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
  }

  protected function Execute()
  {
    $doc = new LIB_Document($this);
    $doc->DelDocLink($this->fieldParser->fields['path'],
		     $this->fieldParser->fields['docId']);
  }
}

?>