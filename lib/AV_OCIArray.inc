<?php

require_once('AV_CaseInsensitiveArray.inc');

class AV_OCIArray extends AV_CaseInsensitiveArray
{
    public function __construct(Array $initial_array = array()) 
    {
      if(isset($initial_array['SIZE_']))
	$initial_array['SIZE'] = $initial_array['SIZE_'];
      if(isset($initial_array['LEVEL_']))
	$initial_array['LEVEL'] = $initial_array['LEVEL_'];
      parent::__construct($initial_array);
    }

    public function offsetGet($offset) 
    {
      $value = parent::offsetGet($offset);
      if(gettype($value)=='object' && get_class($value)=='OCI-Lob')
      {
	$blob = $value->load();
	$value->free();
	$this->offsetSet($offset,$blob);
	return $blob;
      }
      else
	return $value;
    }
}
?>