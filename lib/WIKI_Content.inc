<?php

require_once('CMS_Content.inc');

class WIKI_Content extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    if(in_array('Administrators',$this->memberGroups))
      $administrators = true;

    // if pending approval and reached by moderators, begin moderation process
    // by attributing the moderator to the article
    if(in_array('Moderators',$this->memberGroups))
    {
      $moderators = true;
      if($fields['pendingApproval']=='1')
      {
	try
	{
	  $_GET['moderator'] = $this->memberData['userId'];
	  $moderation = $this->page->LoadModule('WIKI_Moderation');
	}
	catch(Exception $e)
	{
	  $message = "<p id='errormsg'>".$e->getMessage().'</p>';
	  $this->pageBuilder->rootTemplate->addText($message,'mainContent');
	  return;
	}
      }
    }

    $mainScripts = $this->pageBuilder->loadTemplate('wikiScripts');
    $mainScripts->addText($this->pageBuilder->avVersion,'avVersion');
    $this->pageBuilder->rootTemplate->addTemplate($mainScripts,'header');
   
    // loads document title
    $this->pageBuilder->rootTemplate->addText($fields['title'],'title');

    // add to history
    if($this->pageBuilder->siteConfig->getVar('cms','history'))
    {
      if($_REQUEST['docId'] != NULL)
	$_GET['link'] = 'content/'.$_REQUEST['docId'];
      else
	$_GET['link'] = 'area/'.$this->menuPath;
      $_GET['title'] = addslashes($fields['title']);
      $this->page->LoadModule('AV_AddHistory');
    }

    // header
    $content = '<h1>'.$fields['title']."</h1>\n";

    // authors, article reference and date
    $content .= '<cite>'.$fields['authors'].', #' . $fields['docId'];
    $dateFormat = $this->pageBuilder->siteConfig->getVar('cms','dateFormat');
    $lastChanged = strftime($dateFormat,(int)$fields['lastChanged']);
    $content .= ", <span id='lastChanged'>$lastChanged</span></cite>\n";


    // keywords
    $content .= "<p class='keywords'><strong>";
    $content .= $this->pageLabels->attrs['keywordsTag'].":</strong> ";
    $content .= $fields['keywords']."</p>\n";

    // content
    $content .= $fields['content'];

    // load article's images
    $content .= $this->page->LoadImages($fields['imageList']);

   // action buttons
    $content .= "<p class='actions'>";

    // edit 
    $editRight = ($fields['userId'] == $this->memberData['userId'] && 
		  $fields['userId'] != NULL || $moderators || $administrators);
    if($editRight)
    {
      $content .= "<a href='wikiForm.av?docId=".$fields['docId'];
      $content .= "'>Editar</a>";

      // remove
      $content .= "<a onclick=\"RemoveArticle(".$fields['docId'].",'";
      $content .= str_replace("%2F","/",
			      urlencode(stripslashes($this->menuPath)))."')\" ";
      $content .= "class='red'>".$this->pageLabels->attrs['removeTag']."</a>";
    }

    // print
    $content .= "<a onclick='print();'>".$this->pageLabels->attrs['printTag'];
    $content .= "</a>";

    // end action buttons
    $content .= "</p>\n";

    // status
    if($editRight)
    {
      $content .= "<p class='status'><strong>";
      $content .= $this->pageLabels->attrs['statusTag'] . ':</strong> ';
      if($fields['otherRight']=='')
      {
	$content .= "<span id='pubTxt' class='red'>";
	$content .= $this->pageLabels->attrs['notPublishedTag']."</span> ";
	$content .= '<small>';
	$content .= "(<a onclick='Publish(".$fields['docId'].",true);' ";
	$content .= "id='pubBtn' class='green'>";
	$content .= $this->pageLabels->attrs['publishTag']."</a>)</small>";
      }
      else
      {
	$content .= "<span id='pubTxt' class='green'>";
	$content .= $this->pageLabels->attrs['publishedTag']."</span> ";
	$content .= '<small>';
	$content .= "(<a onclick='Publish(".$fields['docId'].",false);' ";
	$content .= "id='pubBtn' class='red'>";
	$content .= $this->pageLabels->attrs['unpublishTag']."</a>)</small>";
      }

      // end status
      $content .= '</p>';
    }

    $content .= $this->LoadComments($fields,$moderators,$administrators);
    $content .= $this->LoadRelatedArticles($fields);

     // loads main content to the page
    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }


  protected function LoadRelatedArticles(& $fields)
  {
    // search for files at the folder
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
								 'dateFormat');
    $_GET['path'] = '/content/Menu/';
    $_GET['orderBy'] = 'matches';
    $filter = '"'.str_replace(', ','" "',$fields['keywords']).'"';
    $_GET['filter'] = addslashes($filter);
    $_GET['searchCriteria'] = 'keywords';
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = 'authors,lastChanged,title';
    $_GET['limit'] = 
      $this->pageBuilder->siteConfig->getVar('cms',
					     'resultsRelatedDocs');
    $_GET['exclusionFilters_numOfOptions'] = 2;
    $_GET['exclusionFilters_1__fieldName'] = 'otherRight';
    $_GET['exclusionFilters_1__value'] = 'r';
    $_GET['exclusionFilters_2__fieldName'] = 'docId';
    $_GET['exclusionFilters_2__value'] = $fields['docId'];
    $_GET['exclusionFilters_2__operator'] = '<>';

    $search = $this->page->LoadModule('LIB_Search');
    if($search->attrs['totalResults']>0)
    {
      $content = "<h2 class='subsection'>";
      $content .= $this->pageLabels->attrs['relatedDocumentsTag']."</h2>\n";
      $content .= $this->LoadArticleList($search);
      if($search->attrs['totalResults']>$_GET['limit'])
      {
	$data = 'filter='.urlencode($filter);
	$data .= '&amp;sourceId='.$fields['docId'];
	$content .= "<p class='actions'>";
	$content .= "<a href='wikiRelated.av?$data'>";
	$content .= $this->pageLabels->attrs['moreTag']." &raquo;</a></p>\n";
      }
    }
    return $content;
  }

  function LoadArea(&$fields,$path)
  {
    parent::LoadArea($fields,$path);

    // search for files at the folder
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
								 'dateFormat');
    $_GET['path'] = $path;
    $_GET['orderBy'] = 'lastChanged';
    $_GET['order'] = '0';
    $_GET['returnFields'] = 'authors,lastChanged,title';
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsPerPage');

    // administrators or moderators see all articles
    if(!in_array('Moderators',$this->memberGroups) &&
       !in_array('Administrators',$this->memberGroups))
    {
      $_GET['exclusionFilters_numOfOptions'] = 1;
      $_GET['exclusionFilters_1__fieldName'] = 'otherRight';
      $_GET['exclusionFilters_1__value'] = 'r';
    }
    $search = $this->page->LoadModule('LIB_Search');

    // builds articles list
    $content = "<h2 class='subsection'>";
    $content .= $this->pageLabels->attrs['documentsTag']."</h2>\n";
    $content .= $this->page->LoadSearchInfo($search,$_REQUEST['offset'],
					    $_GET['limit']);

    // encoded menu path for the links
    $encMenuPath = str_replace("%2F", "/", 
			       urlencode(stripslashes($this->menuPath)));

    // check for write permission to enable add article button
    $content .= "<p class='actions'>";
    $location = "wikiForm.av?path=$encMenuPath";
    if($this->pageBuilder->sessionH->isGuest())
      $content.="<a href='cmsLoginForm.av?location=".urlencode($location)."'>";
    else
      $content .= "<a href='$location'>";

    $content .= $this->pageLabels->attrs['addTag']."</a></p>\n";

    if($search->attrs['totalResults']>0)
    {
      $content .= $this->LoadArticleList($search);
      $content .= $this->page->LoadSearchNav($search,$_REQUEST['offset'],
					     $_GET['limit'],
					     'area/'.$encMenuPath);
    }

    // subareas
    $content .= $this->page->LoadSubareas();

    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }

  function LoadArticleList($search)
  {
    $content .= "<ol class='pair'>\n";
    foreach($search->children as & $child)
    {
      $content .= "<li>\n";
      $content .= $this->LoadListItem($child->attrs);
      $content .= "</li>\n";
    }
    $content .= "</ol>\n";
    return $content;
  }

  function LoadListItem(& $fields)
  {
    $content = '<h4>'.$fields['authors'];
    $content .= ' <em>'.$fields['lastChanged']."</em></h4>\n";
    $content .= "<p><a href='content/".$fields['docId']."'>";
    $content .= $fields['title']."</a></p>\n";
    return $content;
  }

  function LoadFeedItem(& $fields, $base)
  {
    $content = '<title>'.$fields['title'];
    if($fields['publicationDate'] != NULL &&
       $fields['lastChanged'] != $fields['publicationDate'])
      $content .= ' ('.$this->pageLabels->attrs['changedTag'].')';
    $content .= "</title>\n";
    $content .= '<link>'.$base.'content/'.$fields['docId']."</link>\n";
    $content .= '<guid>'.$base.'content/'.$fields['docId']."</guid>\n";
    $content .= "<description>\n";
    $content .= "<![CDATA[ \n";
    $content .= $fields['content'];
    $content .= "\n]]>\n";
    $content .= "</description>\n";
    $content .= '<pubDate>'.$fields['lastChanged']."</pubDate>\n";  
    return $content;
  }

}

?>
