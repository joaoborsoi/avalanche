<?php

abstract class CMS_Menu
{
  protected $page;
  protected $pageBuilder;

  function __construct(& $page)
  {
    $this->pageBuilder = &$page->pageBuilder;
    $this->page = &$page;
  }

  abstract function LoadMenu($menuItem,&$submenu,&$subareas,&$topPath);
}

?>