<?php

require_once('CMS_Content.inc');

class CMS_Generic extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    // loads document title
    $title = htmlentities_utf($fields['title']);
    $this->pageBuilder->rootTemplate->addText($title,'title');

    // saves history
    if($this->pageBuilder->siteConfig->getVar('cms','history'))
    {
      if($_REQUEST['docId'] != NULL)
	$_GET['link'] = 'content/'.$_REQUEST['docId'];
      else
	$_GET['link'] = 'area/'.$this->menuPath;
    
      $_GET['title'] = addslashes($fields['title']);
      $this->page->LoadModule('AV_AddHistory');
    }

    // header
    $content = "<h1>$title</h1>\n";

    // loads image list
    $content .= $this->page->LoadImages($fields['imageList']);

    // content
    $content .= $fields['content'];

    // loads to the page template
    $this->pageBuilder->rootTemplate->addText($content,'mainContent');

  }

  function LoadArea(&$fields,$path)
  {
    parent::LoadArea($fields,$path);

    // search for files at the folder - content list
    $_GET['path'] = $path;
    $_GET['orderBy'] = 'fixedOrder';
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsPerPage');
    $search = $this->page->LoadModule('LIB_Search');

    if($search->attrs['totalResults'] > 0)
    {
      $_GET['labelId'] = 'documentsTag';
      $_GET['lang'] = $this->lang;
      $getLabel = $this->page->LoadModule('LANG_GetLabel');

      $content = "<h2 class='subsection'>";
      $content .= $getLabel->attrs['value']."</h2>\n";
      $content .= $this->page->LoadSearchInfo($search,$_REQUEST['offset'],
					      $_GET['limit']);

      $content .= "<ul class='genericList'>";
      foreach($search->children as & $child)
      {
	$content .= "<li>".$this->LoadListItem($child->attrs)."</li>";
      }
      $content .= "</ul>\n";
      $encMenuPath = str_replace("%2F", "/", 
				 urlencode(stripslashes($this->menuPath)));
      $content .= $this->page->LoadSearchNav($search,$_REQUEST['offset'],
					     $_GET['limit'],
					     'area/'.$encMenuPath);
    }

    // subareas
    $this->page->LoadSubareas();

    $this->pageBuilder->rootTemplate->addText((string)$content,'mainContent');
  }

  function LoadListItem(& $fields)
  {
    return "<a href='content/".$fields['docId']."'>".$fields['title']."</a>";
  }

  function LoadFeedItem(& $fields, $base)
  {
    $content = '<title>'.$fields['title']."</title>\n";
    $content .= '<link>'.$base.'content/'.$fields['docId']."</link>\n";
    $content .= '<guid>'.$base.'content/'.$fields['docId']."</guid>\n";
    $content .= "<description>\n";
    $content .= "<![CDATA[ \n";
    $content .= $fields['content'];
    $content .= "\n]]>\n";
    $content .= "</description>\n";
    $content .= '<pubDate>'.$fields['lastChanged']."</pubDate>\n";  
    return $content;
  }

}
?>