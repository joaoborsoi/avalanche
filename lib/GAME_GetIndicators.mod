<?php

require_once('AV_DBOperationObj.inc');
require_once('GAME_Indicators.inc');

class GAME_GetIndicators extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'elementId';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'typeId';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    switch($this->fields['typeId'])
    {
    default:
      $templates['text'] = 'indicador';
      $templates['float'] = 'float';
      $templates['percent'] = 'percent';
      $templates['currency'] = 'currency';
      $templates['currencySlider'] = 'currencySlider';
      break;

    case 5: // acoes
      $templates['percent'] = 'acaoPct';
      $templates['currencySlider'] = 'acaoCur';
      break;

    case 4 :// relatorio
      $templates['percent'] = 'consultaPct';
      $templates['currency'] = 'consultaCur';
      $templates['currencySlider'] = 'consultaCur';
      $templates['float'] = 'consultaText';
      $templates['text'] = 'consultaText';
      break;
    }
    $indicators = new GAME_Indicators($this,$this->fields['elementId'],
				      $this->fields['typeId']);
    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 
    $indicators->GetIndicators($formObject,$templates);
    $this->children[] = & $formObject;
  }
}

?>