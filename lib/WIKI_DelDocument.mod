<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_DelDocLink.mod');

class WIKI_DelDocument extends LIB_DelDocLink
{
  protected function Execute()
  {
    // remove comments
    $SQL = 'DELETE FROM n ';
    $SQL .= 'USING LIB_Node n, CMS_DocumentComments c ';
    $SQL .= "WHERE articleId='".$this->fields['docId']."' ";
    $SQL .= 'AND docId=nodeId ';
    $this->dbDriver->ExecSQL($SQL);

    // remove article
    $doc = new LIB_Document($this);
    $doc->DelDocLink($this->fieldParser->fields['path'],
		     $this->fieldParser->fields['docId']);
  }
}