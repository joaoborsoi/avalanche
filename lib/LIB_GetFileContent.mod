<?php


require_once('AV_DBOperationObj.inc');
require_once('LIB_Document.inc');


class LIB_GetFileContent extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'fieldName';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'docId';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['consistType'] = 'integer';

    $this->fieldsDef[2]['fieldName'] = 'size';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'integer';

    $this->fieldsDef[3]['fieldName'] = 'maxWidth';
    $this->fieldsDef[3]['required'] = false;
    $this->fieldsDef[3]['allowNull'] = true;
    $this->fieldsDef[3]['consistType'] = 'integer';

    $this->fieldsDef[4]['fieldName'] = 'maxHeight';
    $this->fieldsDef[4]['required'] = false;
    $this->fieldsDef[4]['allowNull'] = true;
    $this->fieldsDef[4]['consistType'] = 'integer';

    $this->fieldsDef[5]['fieldName'] = 'bgColor';
    $this->fieldsDef[5]['required'] = false;
    $this->fieldsDef[5]['allowNull'] = true;

    $this->fieldsDef[6]['fieldName'] = 'cropRatio';
    $this->fieldsDef[6]['required'] = false;
    $this->fieldsDef[6]['allowNull'] = true;

    $this->fieldsDef[7]['fieldName'] = 'jpegQuality';
    $this->fieldsDef[7]['required'] = false;
    $this->fieldsDef[7]['allowNull'] = true;
    $this->fieldsDef[7]['default'] = 80;
    $this->fieldsDef[7]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $doc = new LIB_Document($this);
    $fieldName = $this->fields['fieldName'];
    $this->attrs = $doc->GetFileContent($fieldName,$this->fields['docId'], 
					$this->fields['size'],
					$this->fields['jpegQuality'],
					$this->fields['maxWidth'],
					$this->fields['maxHeight'],
					$this->fields['bgColor'],
					$this->fields['cropRatio']);
  }
}

?>