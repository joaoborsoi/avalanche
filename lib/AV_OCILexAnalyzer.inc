<?php

require_once('LEX_Analyzer.inc');

$tkId = 1;
define('openParenthesisTk', $tkId++);
define('closeParenthesisTk', $tkId++);
define('semicolonTk', $tkId++);
define('colonTk', $tkId++);
define('equalTk', $tkId++);
define('greaterThenTk', $tkId++);
define('lowerThenTk', $tkId++);
define('plusTk', $tkId++);
define('minusTk', $tkId++);
define('multTk', $tkId++);
define('divTk', $tkId++);
define('dotTk', $tkId++);
define('lockTk', $tkId++);
define('inTk', $tkId++);
define('shareTk', $tkId++);
define('modeTk', $tkId++);
define('forTk', $tkId++);
define('tableTk', $tkId++);
define('levelTk', $tkId++);
define('sizeTk', $tkId++);
define('selectTk', $tkId++);
define('limitTk', $tkId++);
define('unixTimestampTk', $tkId++);
define('ifTk',$tkId++);
define('createTk',$tkId++);
define('temporaryTk',$tkId++);
define('tableTk',$tkId++);
define('integerDefTk',$tkId++);
define('unsignedTk',$tkId++);
define('datetimeTk',$tkId++);
define('varcharTk',$tkId++);
define('textTk',$tkId++);
define('groupConcatTk',$tkId++);
define('orderTk',$tkId++);
define('lastInsertIdTk',$tkId++);
define('dateFormatTk',$tkId++);
define('commentTk',$tkId++);
define('insertTk',$tkId++);
define('intoTk',$tkId++);
define('valuesTk',$tkId++);

class AV_OCILexAnalyzer extends LEX_Analyzer
{
  function __construct($code)
  {
    $symbols['('] = openParenthesisTk;
    $symbols[')'] = closeParenthesisTk;
    $symbols[';'] = semicolonTk;
    $symbols[','] = commaTk;
    $symbols[':'] = colonTk;
    $symbols['='] = equalTk;
    $symbols['>'] = greaterThenTk;
    $symbols['<'] = lowerThenTk;
    $symbols['+'] = plusTk;
    $symbols['-'] = minusTk;
    $symbols['*'] = multTk;
    $symbols['/'] = divTk;

    $symbols['NOW'] = nowTk;
    $symbols['.'] = dotTk;

    $symbols['LOCK'] = lockTk;
    $symbols['IN'] = inTk;
    $symbols['SHARE'] = shareTk;
    $symbols['MODE'] = modeTk;
    $symbols['FOR'] = forTk;
    $symbols['TABLE'] = tableTk;
    $symbols['LEVEL'] = levelTk;
    $symbols['SIZE'] = sizeTk;
    $symbols['SELECT'] = selectTk;
    $symbols['LIMIT'] = limitTk;
    $symbols['UNIX_TIMESTAMP'] = unixTimestampTk;
    $symbols['FROM_UNIXTIME'] = fromUnixtimeTk;
    $symbols['IF'] = ifTk;
    $symbols['CREATE'] = createTk;
    $symbols['TEMPORARY'] = temporaryTk;
    $symbols['TABLE'] = tableTk;
    $symbols['INTEGER'] = integerDefTk;
    $symbols['UNSIGNED'] = unsignedTk;
    $symbols['DATETIME'] = datetimeTk;
    $symbols['VARCHAR'] = varcharTk;
    $symbols['TEXT'] = textTk;
    $symbols['GROUP_CONCAT'] = groupConcatTk;
    $symbols['ORDER'] = orderTk;
    $symbols['LAST_INSERT_ID'] = lastInsertIdTk;
    $symbols['DATE_FORMAT'] = dateFormatTk;
    $symbols['COMMENT'] = commentTk;
    $symbols['INSERT'] = insertTk;
    $symbols['INTO'] = intoTk;
    $symbols['VALUES'] = valuesTk;
    parent::__construct($code, $symbols, false,'.',false);
  }
}

?>