<?php

require_once('ADMIN_Content.inc');

class ADMIN_HelpFrame extends ADMIN_Page
{
  function LoadPage()
  {
    parent::LoadPage();

    $this->pageBuilder->rootTemplate("adminHelpFrame");
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');

    if($_REQUEST['helpId'] == NULL)
      $_REQUEST['helpId'] = 'index';
    $helpTpl = $this->pageBuilder->loadTemplate('adminHelp_'.
						$_REQUEST['helpId']);
    $this->pageBuilder->rootTemplate->addTemplate($helpTpl, 'fileContent');
    return NULL;
  }
}

?>