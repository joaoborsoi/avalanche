<?php

require_once('AV_DBOperationObj.inc');
require_once('IM_Manager.inc');

class IM_GetTalkMessages extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'talkId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'lastId';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['consistType'] = 'integer';

    $this->fieldsDef[2]['fieldName'] = 'dateFormat';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['default'] = $this->pageBuilder->siteConfig->getVar('chat','dateFormat');
  }

  protected function Execute()
  {
    $this->isArray = true;

    $im = new IM_Manager($this);

    // get rows
    $rows = $im->GetTalkMessages($this->fields);
    $this->attrs['messages'] = $rows;
  }
}

?>