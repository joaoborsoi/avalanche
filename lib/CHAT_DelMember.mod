<?php 

require_once("AV_DBOperationObj.inc");
require_once("CHAT_Manager.inc");

class CHAT_DelMember extends AV_DBOperationObj
{
 
  function LoadFieldsDef()
  {    
    $this->fieldsDef[1]['fieldName'] = 'roomId';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;  
  }

  function Execute()
  {  
    $sessionId = $this->pageBuilder->sessionH->sessionID;

    $maganer = new  CHAT_Manager($this);   
    $maganer->LoadMessages();
    return $maganer->DelMember($sessionId,
                               $this->fieldParser->fields['roomId']);
  }
}

?>