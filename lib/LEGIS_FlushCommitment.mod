<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Process.inc');

class LEGIS_FlushCommitment extends AV_DBOperationObj
{
  //--Method: Execute
  //--Desc: Execute module  
  function Execute()
  {
    $process =  new LEGIS_Process($this);
    return $process->FlushCommitments();
  } 
}

?>