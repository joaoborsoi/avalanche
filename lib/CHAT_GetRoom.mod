<?php

require_once("AV_DBOperationObj.inc");
require_once("CHAT_Manager.inc");

class CHAT_GetRoom extends AV_DBOperationObj
{
 
  function Execute()
  {   
    $this->isArray = true;
   $maganer = new  CHAT_Manager($this);
   $maganer->LoadMessages();  
   $rows = $maganer->GetRoom($status);   

   if($status != SUCCESS) 
      return $status;

    $i = 0;
    foreach($rows as $key => $row)
    {
      $this->children[$i] = new AV_ModObject("room", $this);
      $this->children[$i]->attrs = &$rows[$key];
      $i++;
    }  
 
    return SUCCESS;
  }
}

?>