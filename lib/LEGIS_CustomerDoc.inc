<?php

require_once('LIB_Document.inc');

class LEGIS_CustomerDoc extends LIB_Document
{
  protected function LoadSearchDefs(& $params)
  {
    parent::LoadSearchDefs($params);

    // restringe busca para clientes - agregadores só visualizam seus clientes,
    // clientes normais não visualizam ninguém


    $user = new UM_User($this->module);
    $groups = $user->GetMemberGroups();
    $customer = false;
    $aggregator = false;
    foreach($groups as & $group)
    {
      if($group['groupId']=='5')
	$customer = true;
      if($group['groupId']=='24')
	$aggregator = true;
    }
    if($customer && !$aggregator)
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    if($aggregator)
    {
      $sessionId = & $this->module->pageBuilder->sessionH->sessionID;
      $params['searchObjs'][100003]['tables'][] = 'UM_UserSessions us';
      $params['searchObjs'][100003]['tables'][] = 'UM_User u';
      $params['searchObjs'][100003]['tables'][] = 'LEGIS_ProcessClient pc';
      $params['searchObjs'][100003]['tables'][] = 'LEGIS_ProcessClient agg';
      $params['searchObjs'][100003]['tablesWhere'][] = 'doc.docId=pc.entityId';
      $params['searchObjs'][100003]['tablesWhere'][] = 'pc.processId=agg.processId';
      $params['searchObjs'][100003]['tablesWhere'][] = 'pc.entityId<>agg.entityId';
      $params['searchObjs'][100003]['tablesWhere'][] = 'agg.entityId=u.entityId';
      $params['searchObjs'][100003]['tablesWhere'][] = 'us.userId=u.userId';
      $params['searchObjs'][100003]['tablesWhere'][] = "us.sessionId='$sessionId'";

      $params['searchObjs'][100004]['tables'][] = 'UM_UserSessions us';
      $params['searchObjs'][100004]['tables'][] = 'UM_User u';
      $params['searchObjs'][100004]['tables'][] = 'LEGIS_ProcessClient pc';
      $params['searchObjs'][100004]['tables'][] = 'LEGIS_ProcessClient agg';
      $params['searchObjs'][100004]['tablesWhere'][] = 'doc.docId=pc.entityId';
      $params['searchObjs'][100004]['tablesWhere'][] = 'pc.processId=agg.processId';
      $params['searchObjs'][100004]['tablesWhere'][] = 'pc.entityId<>agg.entityId';
      $params['searchObjs'][100004]['tablesWhere'][] = 'agg.entityId=u.entityId';
      $params['searchObjs'][100004]['tablesWhere'][] = 'us.userId=u.userId';
      $params['searchObjs'][100004]['tablesWhere'][] = "us.sessionId='$sessionId'";
    }

  }
}

?>