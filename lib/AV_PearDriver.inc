<?php

// require PEAR
require_once("DB.php");
require_once('AV_DBDriver.inc');

//--Class: AV_PearDriver
//--Desc: Implements pear database driver
class AV_PearDriver extends AV_DBDriver
{
  protected $dbH;

  function __construct(&$pageBuilder, $dbSettings)
  {
    parent::__construct($pageBuilder);

    // build a DSN for them if there isn't one
    // this is PEAR DB DSN type
    if (empty($dbSettings['DSN'])) 
    {
      $dbSettings['DSN'] = 
	$dbSettings['dbType'].'://'.
	$dbSettings['userName'].':'.
	$dbSettings['passWord'].'@'.
	$dbSettings['hostName'];
      
      // if database is set, add that in too
      if (!empty($dbSettings['dataBase'])) 
      {
	$dbSettings['DSN'] .= '/'.$dbSettings['dataBase'];
      }
    }

    // create a PEAR database object
    $baseDB = new DB;
    $this->dbH = $baseDB->connect($dbSettings['DSN'], $dbSettings['persistent']);

    // if it was a PEAR database error, puke
    if (DB::isError($this->dbH)) 
    {
      throw new AV_Exception(FAILED,$this->pageBuilder->lang,
			     array('addMsg'=>"Unable to connect to database - please notify an administrator."));
    }

    // set to associative fetch by default
    $this->dbH->setFetchMode(DB_FETCHMODE_ASSOC);

  }

  function ExecSQL($SQL, $reportError = true)
  {
    if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $rh = $this->dbH->simpleQuery($SQL);
    if(DB::isError($rh))
    {
      if($rh->code == -2 || $rh->code == -5)
	$reportError = false;
      $this->pageBuilder->PrintLog("Query error: CODE=" . $rh->code . " " . 
				   $rh->message . " SQL='$SQL'", $reportError);
      throw new AV_Exception($rh->code,$this->pageBuilder->lang,
			     array('SQL'=>$SQL));
    }

    // Frees result
    $this->dbH->freeResult($rh);
  }

  function GetAll($SQL, $reportError = true)
  {
    if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $rows = $this->dbH->getAll($SQL);
    if(DB::isError($rows)) 
    {
      $this->pageBuilder->PrintLog("Query error: CODE=" . $rows->code . " " . 
				   $rows->message . " SQL='$SQL'", $reportError);
      throw new AV_Exception($rows->code,$this->pageBuilder->lang,
			     array('SQL'=>$SQL));
    }
    return $rows;
  }

  function GetRow($SQL,$reportError = true)
  {
    if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $row = $this->dbH->getRow($SQL);
    if(DB::isError($row)) 
    {
      $this->pageBuilder->PrintLog("Query error: CODE=" . $row->code . " " . 
				   $row->message . " SQL='$SQL'", $reportError);
      throw new AV_Exception($row->code,$this->pageBuilder->lang,
			     array('SQL'=>$SQL));
    }
    return $row;
  }

  function GetCol($SQL, $reportError = true)
  {
    if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $col = $this->dbH->getCol($SQL);
    if(DB::isError($col)) 
    {
      $this->pageBuilder->PrintLog("Query error: CODE=" . $col->code . " " . 
				   $col->message . " SQL='$SQL'", $reportError);
      throw new AV_Exception($col->code,$this->pageBuilder->lang,
			     array('SQL'=>$SQL));
    }
    return $col;
  }

  function GetOne($SQL, $reportError = true)
  {
    if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

   $field = $this->dbH->getOne($SQL);
    if(DB::isError($field)) 
    {
      $this->pageBuilder->PrintLog("Query error: CODE=" . $field->code . " " . 
				   $field->message . " SQL='$SQL'", $reportError);
      throw new AV_Exception($field->code,$this->pageBuilder->lang,
			     array('SQL'=>$SQL));
    }
    return $field;
  }

}
?>