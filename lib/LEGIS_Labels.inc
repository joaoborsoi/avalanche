<?php

require_once('LEGIS_AdvancedSearchClient.inc');
require_once('LEGIS_Customer.inc');
require_once('L2PDF_Labels.inc');

class LEGIS_Labels
{
  protected $module;
  protected $dbDriver;

  function __construct(& $module)
  {
    $this->module = &$module;
    $this->dbDriver = & $module->dbDriver;
  }

  function Generate($params)
  {
    // cria tabela para armazenar clientes
    $labelsSearchTable = uniqid('LEGIS_LabelsSearch_',false);
    $SQL = "CREATE TEMPORARY TABLE $labelsSearchTable (";
    $SQL .= 'entityId INTEGER UNSIGNED NOT NULL,';
    $SQL .= 'representative INTEGER UNSIGNED DEFAULT NULL);';
    $this->dbDriver->ExecSQL($SQL);

    try 
    {
      // busca clientes
      $advSearch =  new LEGIS_AdvancedSearchClient($this->module);
      $advSearch->SearchClient($params,NULL,NULL,$totalResults,
			       false,$labelsSearchTable);
      
      // inicializa representantes para serem buscados
      $SQL = "UPDATE $labelsSearchTable SET representative=entityId";  
      $this->dbDriver->ExecSQL($SQL);

      // busca representantes recursivamente
      $customer =  new LEGIS_Customer('',$this->module);
      $customer->GetRecursiveRepresentative($labelsSearchTable);

      // recupera dados de clientes pessoa física
      $SQL = 'SELECT DISTINCT n.name,r.name as representative,r.postalCode,';
      $SQL .= 'r.postOfficeBox,r.logradouro,r.number,r.numberAux,r.bairro,';
      $SQL .= "IF(t.representative=t.entityId,'0','1') as hasRepresentative, ";
      // subselect para cidade
      $SQL .= '(SELECT cidade_descricao FROM cidade c ';
      $SQL .= 'WHERE c.cidade_codigo=r.cidade_codigo) as city,';
      // subselect para estado
      $SQL .= '(SELECT uf_sigla  FROM uf ';
      $SQL .= 'WHERE uf.uf_codigo=r.uf_codigo) as state ';
      // from
      $SQL .= "FROM EM_NaturalPerson n,EM_NaturalPerson r,$labelsSearchTable t ";
      $SQL .= 'WHERE r.docId=t.representative ';
      $SQL .= 'AND n.docId=t.entityId ';
      // endereço deve ser válido
      $SQL .= "AND r.invalidAddress='0' ";
      // cep obrigatório
      $SQL .= 'AND r.postalCode IS NOT NULL ';
      $SQL .= "AND r.postalCode <> '' ";
      // cidade/rua ou caixa postal obrigatórios
      $SQL .= 'AND (r.cidade_codigo IS NOT NULL ';
      $SQL .= 'AND r.logradouro IS NOT NULL ';
      $SQL .= "AND r.logradouro <> '' ";
      $SQL .= 'OR r.postOfficeBox IS NOT NULL ';
      $SQL .= "AND r.postOfficeBox <> '') ";
      $naturalPersonLabels = $this->dbDriver->GetAll($SQL);
      
      // recupera dados de clientes pessoa jurídica
      $SQL = 'SELECT corporateName,contact,postalCode,postOfficeBox,logradouro,';
      $SQL .= 'number,numberAux,bairro, ';
      // subselect para cidade
      $SQL .= '(SELECT cidade_descricao FROM cidade c ';
      $SQL .= 'WHERE c.cidade_codigo=le.cidade_codigo) as city,';
      // subselect para estado
      $SQL .= '(SELECT uf_sigla  FROM uf ';
      $SQL .= 'WHERE uf.uf_codigo=le.uf_codigo) as state ';
      // from
      $SQL .= "FROM EM_LegalEntity le, $labelsSearchTable t ";
      $SQL .= 'WHERE le.docId=t.representative ';
      // endereço deve ser válido
      $SQL .= "AND le.invalidAddress='0' ";
      // cep obrigatório
      $SQL .= 'AND le.postalCode IS NOT NULL ';
      $SQL .= "AND le.postalCode <> '' ";
      // cidade/rua ou caixa postal obrigatórios
      $SQL .= 'AND (le.cidade_codigo IS NOT NULL ';
      $SQL .= 'AND le.logradouro IS NOT NULL ';
      $SQL .= "AND le.logradouro <> '' ";
      $SQL .= 'OR le.postOfficeBox IS NOT NULL ';
      $SQL .= "AND le.postOfficeBox <> '') ";
      $legalEntityLabels = $this->dbDriver->GetAll($SQL);

      // libera tabela temporária
      $this->dbDriver->ExecSQL("DROP TABLE $labelsSearchTable");
    }
    catch(Exception $e)
    {
      $this->dbDriver->ExecSQL("DROP TABLE $labelsSearchTable");
      throw $e;
    }

    // finaliza caso não existam etiquetas a serem impressas
    if(count($naturalPersonLabels)==0 && count($legalEntityLabels)==0)
      throw new AV_Exception(NOT_FOUND,$this->lang);

    // recupera modelo de etiqueta da configuração
    $SQL = 'SELECT labelsId FROM CMS_Config LIMIT 1';
    $labelsId = $this->dbDriver->GetOne($SQL);
    if($labelsId==NULL)
      throw new AV_Exception(FAILED,$this->lang);

    // cria documento de etiquetas
    $labelsDoc =  new L2PDF_Labels($this->module);
    $labelsDoc->Init($labelsId);

    $labelsDoc->Write('\begin{labels}');

    foreach($naturalPersonLabels as & $label)
    {
      // nome
      if($label['hasRepresentative']=='0')
	$labelsDoc->Write('A(o) Sr.(a). '.$label['name']);
      else
      {
	$labelsDoc->Write('A(o) Representante de Sr.(a). ',false);
	$labelsDoc->Write($label['name']);
	$labelsDoc->Write('A/C Sr.(a). '.$label['representative']);
      }
      $this->GenerateAddress($label,$labelsDoc);

    } // foreach($naturalPersonLabels as & $label)

    foreach($legalEntityLabels as & $label)
    {
      // nome
      $labelsDoc->Write('A(o) '.$label['corporateName']);
      $labelsDoc->Write('A/C Sr.(a). '.$label['contact']);
      $this->GenerateAddress($label,$labelsDoc);
    } // foreach($legalEntityLabels as & $label)

    $labelsDoc->Write('\end{labels}');

    // finaliza documento
    $labelsDoc->End();

    $tmpDir = $this->module->pageBuilder->siteConfig->getVar("dirs", "tmp");
    $fileInfo = $labelsDoc->CreateDoc($tmpDir);
    try
    {
      $doc = new LIB_Document($this->module);

      $fields['path'] = '/files/';
      $fields['title'][0]['lang'] = 'pt_BR';
      $fields['title'][0]['value'] = 'Etiquetas';
      $fields['content']['error'] = UPLOAD_ERR_OK;
      $fields['content']['tmp_name'] = $fileInfo['filePath'].'.pdf';
      $fields['content']['name'] = 'etiquetas.pdf';
      $fields['content']['type'] = 'application/pdf';
      $fields['content']['size'] = filesize($fileInfo['filePath'].'.pdf');
      $fields['groupRight'] = '';
      $fields['otherRight'] = '';
      $doc->LoadFieldsDef(NULL,$fields['path']);
      $docId = $doc->Insert($fields);
      $labelsDoc->UnlinkGarbage($fileInfo['fileName']);
      return $docId;
    }
    catch(Exception $e)
    {
      $labelsDoc->UnlinkGarbage($fileInfo['fileName']);
      throw $e;
    }
    
  }

  protected function GenerateAddress(& $label, &$labelsDoc)
  {
    if($label['logradouro']!=NULL)
    {
      // rua, numero, complemento
      $labelsDoc->Write($labelsDoc->Escape($label['logradouro']),false);
      if($label['number'] != NULL)
	$labelsDoc->Write(', '.$labelsDoc->Escape($label['number']),false);
      if($label['numberAux'] != NULL)
	$labelsDoc->Write(', '.$labelsDoc->Escape($label['numberAux']),false);
      $labelsDoc->Write(''); // endl

      // bairro - cidade/estado
      if($label['bairro'] != NULL)
	$labelsDoc->Write($labelsDoc->Escape($label['bairro']).' - ',false);
      $labelsDoc->Write($labelsDoc->Escape($label['city']),false);
      $labelsDoc->Write('/'.$labelsDoc->Escape($label['state']));
    } //if($label['logradouro']!=NULL)
    else 
      // caixa postal
    {
      $labelsDoc->Write('Caixa Postal: ',false);
      $labelsDoc->Write($labelsDoc->Escape($label['postOfficeBox']));
    }

    $labelsDoc->Write('CEP: '.$labelsDoc->Escape(substr($label['postalCode'],
							0,-3)),false);
    $labelsDoc->Write('-'.$labelsDoc->Escape(substr($label['postalCode'],-3)));

    // serapação dos labels
    $labelsDoc->Write(''); // endl
  }
}

?>