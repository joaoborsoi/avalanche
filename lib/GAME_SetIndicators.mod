<?php

require_once('AV_DBOperationObj.inc');
require_once('GAME_Indicators.inc');

class GAME_SetIndicators extends AV_DBOperationObj
{
  protected $indicators;

  protected function LoadFieldsDef()
  {
    $this->indicators = new GAME_Indicators($this,'*');
    $this->fieldsDef = $this->indicators->LoadFieldsDef();
  }

  protected function Execute()
  {
    $this->fields['id'] = $this->indicators->GetLastId();
    $this->indicators->Set($this->fields);
  }
}

?>