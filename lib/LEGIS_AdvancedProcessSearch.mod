<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_AdvancedSearchProcess.inc');
require_once('LEGIS_Process.inc');

class LEGIS_AdvancedProcessSearch extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'paramList';
    $this->fieldsDef[0]['stripSlashes'] = true; 
    $this->fieldsDef[0]['trimField'] = false; 

    $this->fieldsDef[1]['fieldName'] = 'limit';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['consistType'] = 'integer';

    $this->fieldsDef[2]['fieldName'] = 'offset';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'integer';

    $this->fieldsDef[3]['fieldName'] = 'sub';
    $this->fieldsDef[3]['required'] = false;
    $this->fieldsDef[3]['allowNull'] = true;
    $this->fieldsDef[3]['consistType'] = 'boolean';
    $this->fieldsDef[3]['default'] = false;
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $process = new LEGIS_Process($this);
    $memberAggregator = $process->GetMemberAggregator();

    if(count($memberAggregator) > 0)
    {
      $this->fields['paramList'][] = Array('param'=>'aggregator',
					   'aggregatorId'=>$memberAggregator['docId']);
    }

    $advSearch = new LEGIS_AdvancedSearchProcess($this);
    $rows = $advSearch->SearchProcess($this->fields['paramList'], 
				      $this->fields['limit'],
				      $this->fields['offset'], 
				      $this->fields['sub'], 
				      $totalResults);

    $this->LoadChildrenFromRows($rows, 'process');
    $this->attrs['totalResults'] = $totalResults;
  }
}

?>
