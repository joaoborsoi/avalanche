<?php

require_once('CMS_RssPrefs.inc');

class CMS_GetRssPrefs extends AV_DBOperationObj
{
  // disable transaction - only consulting
  function __construct(&$pbuilder, $itemTpl, $itemTplTag, $varPrefix, $jsonInput=false)
  {
    parent::__construct($pbuilder, $itemTpl, $itemTplTag, $varPrefix, $jsonInput,false);
  }

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'userId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  protected function GetTemplates()
  {
    if($this->fields['fieldName'] != '')
    {
      $templates['selectMult']['fileName'] = 'loadSelectMultField';
      $templates['selectMultOption']['fileName']  = 'loadSelectMultFieldOption';
    }
    else
    {
      $templates['hidden']['fileName'] = 'hiddenField';
      $templates['hidden']['outputFuncRef'] = 'htmlentities_utf';
      $templates['selectMult']['fileName'] = 'selectMultField';
      $templates['selectMult']['outputFuncRef'] = 'htmlentities_utf';
      $templates['selectMultOption']['fileName']  = 'selectMultFieldOption';
      $templates['boolean']['fileName']  = 'checkboxField';
      $templates['rssPrefs']['fileName']  = 'contentField';
    }
    return $templates;
  }

  protected function Execute()
  {
    $templates = $this->GetTemplates();
    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 
    $table = new CMS_RssPrefs('CMS_RSSPrefs',$this);

    $keyFields = Array('userId'=>$this->fields['userId']);
    $result = $table->GetForm($keyFields,NULL,NULL,$templates,$formObject);

    $this->children[] = $result;
  }
}
?>