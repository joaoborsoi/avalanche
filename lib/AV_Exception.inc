<?php

define('UNKNOWN_ERROR',      -1);
define('INDEX_VIOLATED',     -5);
define('ALREADY_EXISTS',     -5);

define('SUCCESS',            0);
define('FAILED',             1000);
define('FATAL_ERROR',        1001);
define('STATIC_PROP',        1002);
define('READ_ONLY_PROP',     1003);
define('FIELD_REQUIRED',     1004);
define('INVALID_FIELD',      1005);
define('NOT_FOUND',          1006);
define('WARNING',            1007);

define('PERMISSION_DENIED',  2001);
define('INVALID_PATH',       2002);
define('FOLDER_NOT_EMPTY',   2003);
define('MAINTENANCE',        2004);


define('INVALID_RET_FIELD', 3001);

class AV_Exception extends Exception
{
  protected $data;

  function __construct($status, $lang, $data = NULL)
  {
    $this->data = $data;
    $message=(isset($data['message']))?
      $data['message']:$this->__getMessage($status,$lang);
    if(isset($data['addMsg']))
      $message .= $data['addMsg'];

    parent::__construct($message, $status);
  }

  final function setMessage($message)
  {
    $this->message = $message;
  }

  final function &getData()
  {
    return $this->data;
  }

  function __toString()
  {
    $message = "exception '".__CLASS__."' with message '".$this->message."' ";
    $message .= '('.$this->code.')';
    if($this->data != NULL)
      $message .= " and data '" . var_export($this->data,true) . "' ";
    return $message;
  }

  final private function __getMessage($status,$lang)
  {
    switch($lang)
    {
    default:
    case 'en_US': // US english messages
      switch($status)
      {
      case SUCCESS: return 'Success.';
      case FAILED: return 'Failed.'; 
      case FATAL_ERROR: return 'Fatal error!'; 
      case NOT_FOUND: return 'Not found.'; 
      case STATIC_PROP: return 'Static property.'; 
      case READ_ONLY_PROP: return 'Read-only property.'; 
      case FIELD_REQUIRED: return "Field required$fieldLabel."; 
      case INVALID_FIELD: return "Invalid Field$fieldLabel."; 
      case INDEX_VIOLATED: return "Index violated"; 
      case ALREADY_EXISTS: return "Already exists";
      case PERMISSION_DENIED: return 'Permission denied.'; 
      case INVALID_PATH: return 'Invalid path.'; 
      case FOLDER_NOT_EMPTY: return 'Folder not empty.'; 
      case MAINTENANCE: return 'In maintenance'; 
      case INVALID_RET_FIELD: return "Invalid returnFields."; 
      case WARNING: return "Warning.";
      default:
      case UNKNOWN_ERROR: return "Unknown error.";
      }
      break;

    case 'pt_BR': // Brazillian portuguese messages
      switch($status)
      {
      case SUCCESS: return 'Sucesso.'; 
      case FAILED: return 'Falha.'; 
      case FATAL_ERROR: return 'Erro fatal!'; 
      case NOT_FOUND: return 'Não encontrado.'; 
      case STATIC_PROP: return 'Propriedade estática.'; 
      case READ_ONLY_PROP: return 'Propriedade somente de leitura.'; 
      case FIELD_REQUIRED: return "Campo requerido$fieldLabel."; 
      case INVALID_FIELD: return "Campo inválido$fieldLabel."; 
      case INDEX_VIOLATED: return "Indice violado"; 
      case ALREADY_EXISTS: return "Já existente";
      case PERMISSION_DENIED: return 'Permissão negada.'; 
      case INVALID_PATH: return 'Caminho inválido.'; 
      case FOLDER_NOT_EMPTY: return 'Pasta não vazia.'; 
      case MAINTENANCE: return 'Em manutenção'; 
      case INVALID_RET_FIELD: return "returnFields inválido."; 
      case WARNING: return "Atenção.";
      default:
      case UNKNOWN_ERROR: return "Erro desconhecido.";
      }
      break;
    }
  }
}

?>