<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_ChangePassword extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'userId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'password';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['stripSlashes'] = false;

    $this->fieldsDef[2]['fieldName'] = 'path';
    $this->fieldsDef[2]['required'] = true;
    $this->fieldsDef[2]['allowNull'] = false;
    $this->fieldsDef[2]['default'] = '/';
    $this->fieldsDef[2]['stripSlashes'] = false;
  }

  protected function Execute()
  {
    $user = new UM_User($this);

    $user->LoadFieldsDef();

    $user->Set($this->fields);
  }
}

?>