<?php

require_once('FORM_DynTable.inc');
require_once('UM_User.inc');
require_once('LEGIS_AdvancedSearchProcess.inc');

class LEGIS_Customer extends FORM_DynTable
{
  protected $aggregator;
  protected $processList;
  protected $representedProcessList;

  function Insert(& $fields, $extraFields = array())
  {
    parent::Insert($fields,$extraFields);

    if($fields['customerUserRef']['login'] != NULL)
    {
      if($fields['customerUserRef']['password']==NULL)
	throw new AV_Exception(FIELD_REQUIRED,$this->lang,
			       array('fieldName'=>'password',
				     'addMsg'=>' (senha)'));
      $entityId = $this->dbDriver->GetOne('SELECT LAST_INSERT_ID()');
      $fields['customerUserRef']['path'] = '/client/naturalPerson/';
      $fields['customerUserRef']['entityId'] = $entityId;
      $fields['customerUserRef']['groups'][] = 5;

      $user = new UM_User($this->module);
      $user->LoadFieldsDef();
      $user->Insert($fields['customerUserRef']);
    }
  }

  function Set($fields, $extraFields = array())
  {
    if($fields['customerUserRef']['login'] != NULL)
    {
      $user = new UM_User($this->module);

      $fields['customerUserRef']['path'] = '/client/naturalPerson/';
      if($fields['customerUserRef']['userId']==NULL)
      {
	$fields['customerUserRef']['entityId'] = $fields['docId'];
	$fields['customerUserRef']['groups'][] = 5;
	$user->LoadFieldsDef();
	$user->Insert($fields['customerUserRef']);
      }
      else
      {
	$user->LoadFieldsDef(true,true);
	if($fields['customerUserRef']['password']==='')
	  unset($fields['customerUserRef']['password']);
	$user->Set($fields['customerUserRef']);
      }
    }
    
    return parent::Set($fields,$extraFields);
  }

  protected function ParseFieldDef(& $fieldDef,$disableFileRequired,$disableRequired)
  {
    parent::ParseFieldDef($fieldDef,$disableFileRequired,$disableRequired);

    switch($fieldDef['type'])
    {
    case 'customerUserRef':
      // gambiarra pra evitar addslashes      
      $fieldDef['stripSlashes'] = true; 
      break;
    case 'cpf':
      $user = new UM_User($this->module);

      // cpf nao obrigatorio para administradores
      $groups = $user->GetMemberGroupsIds('share');
      if(in_array(8, $groups))
      {
	$fieldDef['required'] = '0';
	$fieldDef['allowNull'] = '1';
      }
      break;
    }
  }


  function GetForm($keyFields, $fieldName, $sourceId, & $templates, 
		   & $formObject, $defValues = array(),$dateFormat = NULL,
		   $numberFormat = true, $short = false)
  {
    $this->aggregator = $this->GetMemberAggregator();
    parent::GetForm($keyFields, $fieldName, $sourceId, $templates, 
		    $formObject, $defValues ,$dateFormat,
		    $numberFormat, $short);


    if($this->aggregator['docId'] != NULL &&
       count($this->processList) == 0 &&
       count($this->representedProcessList) == 0)
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    return $formObject;
  }

  protected function &GetField(& $fieldDef, & $values, $keyFields,  
			       & $templates, $sourceId, $dateFormat = NULL,
			       $numberFormat = true, $fieldName = NULL)
  {
    switch ($fieldDef['type'])
    {
    case 'customerUserRef':
      return $this->GetUserRef($fieldDef,$values,$keyFields);
    case 'customerProcessList':
      return $this->GetProcessList($fieldDef,$values,$keyFields);
    case 'representedProcessList':
      return $this->GetRepresentedProcessList($fieldDef,$values,$keyFields);
    case 'representativeList':
      return $this->GetRepresentativeList($fieldDef,$values,$keyFields);
    case 'representedList':
      return $this->GetRepresentedList($fieldDef,$values,$keyFields);
    case 'entryList':
      return $this->GetEntryList($fieldDef,$keyFields,$dateFormat,
				 $numberFormat);
    default:
      if($fieldDef['fieldName'] == 'status') 
      {
	$fieldDef['accessMode'] = 'rw';
      }

      return parent::GetField($fieldDef,$values,$keyFields,$templates,
			      $sourceId,$dateFormat,$numberFormat);
     }
  }

  protected function GetUserRef($fieldDef,&$values,$keyFields)
  {
    $field =  new AV_ModObject('userRef',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    if($keyFields!=NULL)
    {
      $entityId = $keyFields['docId'];

      $SQL = 'SELECT userId, status FROM UM_User ';
      $SQL .= "WHERE entityId='$entityId' ";
      $user = $this->dbDriver->GetRow($SQL);

      if(count($user)==0)
	$field->attrs['value']['status'] = '1';
      else
	$field->attrs['value'] = $user;
    }
    else
      $field->attrs['value']['status'] = '1';

    return $field;
  }

  function GetMemberAggregator()
  {
    $sessionId = & $this->module->pageBuilder->sessionH->sessionID;
    
    $SQL = 'SELECT aggregator,l.docId ';
    $SQL .= 'FROM UM_UserSessions us, UM_User u,EM_LegalEntity l ';
    $SQL .= "WHERE sessionId='$sessionId' ";
    $SQL .= 'AND us.userId=u.userId AND u.entityId=l.docId ';
    $SQL .= 'AND aggregator IN (1,2) ';
    
    return $this->dbDriver->GetRow($SQL);
  }

  protected function GetProcessList($fieldDef,&$values,$keyFields)
  {
    if($keyFields==NULL || $values['aggregator']!=NULL)
      return NULL;

    $advSearch = new LEGIS_AdvancedSearchProcess($this->module);

    $params = array(array('param'=>'customer',
			  'customer'=>$keyFields));
    if($this->aggregator['docId'] != NULL) 
    {
      $newParam['param'] = 'aggregator';
      $newParam['aggregatorId'] = $this->aggregator['docId'];
      $params[] = $newParam;
    }
    $this->processList = $advSearch->SearchProcess($params,50,NULL,true,$totalResults);
    if($totalResults==0)
      return NULL;

    // cria objeto para o campo de lista de processos
    $field =  new AV_ModObject('processList',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;
    $field->attrs['items'] = $this->processList;
    return $field;
  }

  function GetRepresentedProcessList($fieldDef,&$values,$keyFields)
  {
    if($keyFields==NULL)
      return NULL;
    
    $entityId = $keyFields['docId'];

    $customerRespTable = uniqid('LEGIS_CustomerRepresented_',false);
    $SQL = "CREATE TEMPORARY TABLE $customerRespTable ";
    $SQL .= 'SELECT customer as entityId FROM LEGIS_Representative ';
    $SQL .= "WHERE representative='$entityId' ";
    $this->dbDriver->ExecSQL($SQL);

    try
    {
      $this->GetRecursiveRepresented($customerRespTable);

      $SQL = 'SELECT p.processId,p.rootProcessId,rp.folder,rp.folderYear,';
      $SQL .= "(SELECT GROUP_CONCAT(number) FROM LEGIS_ProcessNumber pn ";
      $SQL .= "WHERE pn.processId=p.processId) AS number, ";
      $SQL .= "(SELECT s.name FROM LEGIS_Situation s ";
      $SQL .= "WHERE s.docId=p.situationId) AS situation, ";
      $SQL .= "(SELECT s.name FROM LEGIS_Subject s ";
      $SQL .= "WHERE s.docId=p.subjectId) AS subject ";

      $SQL .= "FROM LEGIS_ProcessClient pc,$customerRespTable crt,";
      $SQL .= 'LEGIS_Process p, LEGIS_RootProcess rp ';
      if($this->aggregator['docId'] != NULL)
	$SQL .= ', LEGIS_ProcessClient pc2 ';

      $SQL .= "WHERE pc.entityId = crt.entityId ";
      $SQL .= 'AND pc.processId=p.processId ';
      $SQL .= 'AND p.rootProcessId=rp.processId ';
      if($this->aggregator['docId'] != NULL)
	$SQL.="AND pc.processId=pc2.processId AND pc2.entityId='".$this->aggregator['docId']."' ";
      $SQL .= 'ORDER BY processId DESC ';
      $this->representedProcessList = $this->dbDriver->GetAll($SQL);
      if(count($this->representedProcessList)==0)
      	return NULL;

      foreach($this->representedProcessList as &$row)
	$row['number'] = explode(',',$row['number']);

      $this->dbDriver->ExecSQL("DROP TABLE $customerRespTable");

      // cria objeto para o campo de lista de processos
      $field =  new AV_ModObject('processList',$this->module->pageBuilder);
      $field->attrs = & $fieldDef;
      
      $field->attrs['items'] = $this->representedProcessList;
      return $field;
    } 
    catch(AV_Exception $e)
    {
      $this->dbDriver->ExecSQL("DROP TABLE $customerRespTable");
      throw $e;
    }

  }

  function GetRecursiveRepresented($tableName)
  {
    // cria tabela contendo próximo nível de busca recursiva dos
    // usuários representados pelo cliente
    $tableName2 = uniqid('LEGIS_CustomerRepresented_',false);
    $SQL = "CREATE TEMPORARY TABLE $tableName2 ";
    $SQL .= 'SELECT customer as entityId ';
    $SQL .= "FROM LEGIS_Representative r, $tableName t ";
    $SQL .= "WHERE r.representative=entityId ";
    $this->dbDriver->ExecSQL($SQL);

    try
    {
      // verifica se ja chegamos no final da busca recursiva
      $count = $this->dbDriver->GetOne("SELECT count(*) FROM $tableName2");
      if($count == 0)
	return;

      // procura representados recursivamente
      $this->GetRecursiveRepresented($tableName2);

      // popula tabela original com resultados encontrados
      $SQL = "INSERT INTO $tableName (entityId) ";
      $SQL .= "SELECT entityId FROM $tableName2 ";
      $this->dbDriver->ExecSQL($SQL);

      $this->dbDriver->ExecSQL("DROP TABLE $tableName2");
    }
    catch(Exception $e)
    {
      $this->dbDriver->ExecSQL("DROP TABLE $tableName2");
      throw $e;
    }
  }

  function GetRecursiveRepresentative($tableName, $lock = false)
  {
    // cria tabela contendo próximo nível de busca recursiva dos
    // usuários representantes dos clientes
    $tableName2 = uniqid('LEGIS_CustomerRepresentative_',false);
    $SQL = "CREATE TEMPORARY TABLE $tableName2 ";
    $SQL .= 'SELECT t.entityId, r.representative, r.customer ';
    $SQL .= "FROM LEGIS_Representative r, $tableName t ";
    $SQL .= "WHERE r.customer=t.representative ";
    if($lock) $SQL .= 'FOR UPDATE ';
    $this->dbDriver->ExecSQL($SQL);

    try
    {
      // verifica se ja chegamos no final da busca recursiva
      $count = $this->dbDriver->GetOne("SELECT count(*) FROM $tableName2");
      if($count == 0)
	return;

      // remove clientes que ainda possuem representantes
      $SQL = "DELETE FROM t1 USING $tableName t1, $tableName2 t2 ";
      $SQL .= 'WHERE t1.representative=t2.customer ';
      $this->dbDriver->ExecSQL($SQL);

      // procura representantes recursivamente
      $this->GetRecursiveRepresentative($tableName2);

      // popula tabela original com resultados encontrados
      $SQL = "INSERT INTO $tableName (entityId,representative) ";
      $SQL .= "SELECT entityId,representative FROM $tableName2 ";
      $this->dbDriver->ExecSQL($SQL);

      $this->dbDriver->ExecSQL("DROP TABLE $tableName2");
    }
    catch(Exception $e)
    {
      $this->dbDriver->ExecSQL("DROP TABLE $tableName2");
      throw $e;
    }
  }

  function GetRepresentativeList($fieldDef,$values,$keyFields)
  {
    if($keyFields == NULL)
      return NULL;

    $entityId = current($keyFields);

    $SQL = 'SELECT r.docId, np.docId as entityId, np.name, t.name as type, ';
    $SQL .= '(SELECT name FROM LEGIS_Kinship WHERE id=kinship) ';
    $SQL .= 'as kinship ';

    $SQL .= 'FROM LEGIS_Representative r,EM_NaturalPerson np,';
    $SQL .= 'LEGIS_RepresentativeType t ';
    $SQL .= "WHERE customer = '$entityId' ";
    $SQL .= 'AND representative=np.docId ';
    $SQL .= 'AND type=t.id ';
    $rows = $this->dbDriver->GetAll($SQL);

    foreach($rows as & $row)
    {
      $SQL = 'SELECT s.situationId, s.name ';
      $SQL .= 'FROM LEGIS_CustomerSituation cs, LEGIS_PersonSituation s ';
      $SQL .= "WHERE cs.docId='".$row['entityId']."' AND cs.situationId=s.situationId ";
      $row['situationId'] = $this->dbDriver->GetAll($SQL);
    }

    $objectListField = new AV_ModObject('object', $this->pageBuilder);
    $objectListField->attrs = & $fieldDef;
    $objectListField->attrs['items'] = $rows;

    return $objectListField;
  }


  function GetRepresentedList($fieldDef,$values,$keyFields)
  {
    if($keyFields == NULL)
      return NULL;

    $entityId = current($keyFields);

    $SQL = 'SELECT r.docId, np.docId as entityId, np.name, t.name as type, ';
    $SQL .= '(SELECT name FROM LEGIS_Kinship WHERE id=kinship) ';
    $SQL .= 'as kinship ';

    $SQL .= 'FROM LEGIS_Representative r,EM_NaturalPerson np,';
    $SQL .= 'LEGIS_RepresentativeType t ';
    $SQL .= "WHERE representative = '$entityId' ";
    $SQL .= 'AND customer=np.docId ';
    $SQL .= 'AND type=t.id ';
    $rows = $this->dbDriver->GetAll($SQL);

    if(count($rows)==0)
      return NULL;

    foreach($rows as & $row)
    {
      $SQL = 'SELECT s.situationId, s.name ';
      $SQL .= 'FROM LEGIS_CustomerSituation cs, LEGIS_PersonSituation s ';
      $SQL .= "WHERE cs.docId='".$row['entityId']."' AND cs.situationId=s.situationId ";
      $row['situationId'] = $this->dbDriver->GetAll($SQL);
    }

    $objectListField = new AV_ModObject('object', $this->pageBuilder);
    $objectListField->attrs = & $fieldDef;
    $objectListField->attrs['items'] = $rows;

    return $objectListField;
  }

  protected function &GetEntryList($fieldDef,$keyFields,$dateFormat,$numberFormat)
  {
    if($keyFields==NULL)
      return NULL;

    // cria objeto para o campo de contratos
    $field =  new AV_ModObject('contractList',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    $doc = new LIB_Document($this->module);
    $search['path'] = '/entry/';
    $exlusionFilter['fieldName'] = 'customerId';
    $exlusionFilter['value'] = current($keyFields);
    $exlusionFilter['operator'] = '=';
    $search['exclusionFilters']['customerId'][] = $exlusionFilter;
    $search['orderBy'] = 'expiration';
    $search['order'] = '0';
    $search['dateFormat'] = $dateFormat;
    $search['numberFormat'] = $numberFormat;
    $search['tempTable'] = '0';
    $field->attrs['items'] = $doc->Search($search);
    
    return $field;
  }


}

?>