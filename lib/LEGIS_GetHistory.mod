<?php
require_once('LIB_GetDoc.mod');
require_once('LEGIS_History.inc');

class LEGIS_GetHistory extends LIB_GetDoc
{
  protected function LoadFieldsDef()
  {
    parent::LoadFieldsDef();
    $i = count($this->fieldsDef);

    $this->fieldsDef[$i]['fieldName'] = 'processId';
    $this->fieldsDef[$i]['required'] = false;
    $this->fieldsDef[$i]['allowNull'] = true;
    $this->fieldsDef[$i]['consistType'] = 'integer';
  }

  protected function &CreateDocument()
  {
    return new LEGIS_History($this);
  }

  protected function Execute()
  {
    $doc = & $this->CreateDocument();
    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 

    if($this->fields['docId'] == NULL)
      $keyFields = NULL;
    else
      $keyFields = Array('docId' => $this->fields['docId']);

    $doc->GetForm($keyFields,$this->fields['processId'],$this->fields['path'], 
		  $this->fields['fieldName'], $this->fields['sourceId'], 
		  $templates, $formObject,$defValues,
		  $this->fields['dateFormat'],$this->fields['numberFormat'],
		  $this->fields['short']);

    $this->children[] = & $formObject;
  }
}
?>