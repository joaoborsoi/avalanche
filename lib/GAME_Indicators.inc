<?php

require_once('FORM_DynTable.inc');

class GAME_Indicators extends FORM_DynTable
{
  protected $elementId;
  protected $typeId;

  function __construct(AV_Module &$module, $elementId='*',$typeId=NULL)
  {
    parent::__construct('GAME_Indicators',$module);
    $this->elementId = $elementId;
    $this->typeId = $typeId;
  }

  function GetLastId()
  {
    // recupera id dos indicadores no ultimo turno
    $SQL = 'SELECT MAX(i.id) ';
    $SQL .= 'FROM GAME_Indicators i, GAME_PlayUser pu, UM_UserSessions us ';
    $SQL .= "WHERE sessionId='".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $SQL .= 'AND pu.id = playId LOCK IN SHARE MODE ';
    return $this->dbDriver->GetOne($SQL);
  }

  function GetTurn()
  {
    $SQL = 'SELECT MAX(turn) ';
    $SQL .= 'FROM GAME_Indicators i, GAME_PlayUser pu, UM_UserSessions us ';
    $SQL .= "WHERE sessionId='".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $SQL .= 'AND pu.id = playId LOCK IN SHARE MODE ';
    return $this->dbDriver->GetOne($SQL);
  }

  function GetIndicators(FORM_Object &$formObject, & $templates)
  {
    $this->LoadFieldsDef();

    $key = $this->GetLastId();
    if($key==NULL)
      throw new AV_Exception(NOT_FOUND,$this->lang);

    // recupera indicadores
    $keyFields = Array('id' => $key);
    $this->GetForm($keyFields,NULL,NULL, $templates, $formObject,NULL); 
    return $formObject; 
  }

  function GetMaxIndicator($fieldName)
  {
    $SQL = "SELECT MAX($fieldName) as max ";
    $SQL .= 'FROM GAME_Indicators, GAME_PlayUser pu, UM_UserSessions us ';
    $SQL .= "WHERE sessionId='".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $SQL .= 'AND pu.id = playId ';
    $SQL .= "AND $fieldName <> -999999999 ";
    return $this->dbDriver->GetOne($SQL);
  }

  function GetIndicatorSeries($fieldName, $scale, array &$fieldInfo)
  {
    $fieldInfo = $this->GetFieldInfo($fieldName);
    $SQL = 'SELECT turn as x, ';
    if($scale == 0)
      $SQL .= "$fieldName as y ";
    else
      $SQL .= "$fieldName/$scale as y ";
    $SQL .= 'FROM GAME_Indicators, GAME_PlayUser pu, UM_UserSessions us ';
    $SQL .= "WHERE sessionId='".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $SQL .= 'AND pu.id = playId ';
    $SQL .= "AND $fieldName <> -999999999 ";
    return $this->dbDriver->GetAll($SQL);
  }

  function UpdateFormulas($id)
  {
    $this->LoadFormulaDefs();
    foreach($this->formulaFieldsDef as $value)
      $fields[$value['fieldName']] = $value;
    
    // prevents locale problems inserting float fields
     setlocale(LC_ALL, 'en_US');

     foreach($fields as $field => $value)
       $this->formulas[$field] = $this->Decompose($this->formulas, $value,
						  $fields,$parms);
    
    if(count($this->formulas)==0)
      return;

    $SQL = 'UPDATE GAME_Indicators SET ';
    $first = true;
    foreach($this->formulas as $field => $formula)
    {
      if($first)
	$first = false;
      else
	$SQL .= ',';

      $SQL .= $field . "=" . $formula;
    }
    $SQL .= " WHERE id='$id'";
    setlocale(LC_ALL, $this->lang.'.utf8');

    $this->dbDriver->ExecSQL($SQL);
  }

  function ApplyImpact($id, $impact)
  {
    // lock for update
    $SQL = "SELECT id FROM GAME_Indicators WHERE id='$id' FOR UPDATE ";
    $id = $this->dbDriver->GetOne($SQL);

    // apply impact
    $SQL = "UPDATE GAME_Indicators SET $impact " ;
    $SQL .= "WHERE id='$id' ";
    $this->dbDriver->ExecSQL($SQL);

    // correct max and min values restriction
    $this->LoadFieldsDef();
    $SQL = NULL;
    foreach($this->fieldsDef as &$field)
    {
      if($field['accessMode']=='rw' || $field['accessMode']=='w')
      {
	$value = $field['fieldName'];
	if($field['maxValue'] != NULL)
	{
	  $aux = "IF($value>".$field['maxValue'].',';
	  $aux .= $field['maxValue'].",$value)";
	  $value = $aux;
	}
	if($field['minValue'] != NULL)
	{
	  $aux = "IF($value<".$field['minValue'].',';
	  $aux .= $field['minValue'].",$value)";
	  $value = $aux;
	}
	if($value != $field['fieldName'])
	{
	  if($SQL!=NULL)
	    $SQL .= ',';
	  $SQL .= $field['fieldName'] . "=$value";
	}
      }
    }
    if($SQL != NULL)
    {
      $SQL = 'UPDATE GAME_Indicators SET '.$SQL;
      $SQL .= "WHERE id='$id' ";
      $this->dbDriver->ExecSQL($SQL);
    }
  }

  //--Method: Decompose
  protected function Decompose(&$formulas, $value, &$formulaFields, &$params)
  {
    $formula = $value['expression'];
    $lex = new FORM_LexAnalyzer($formula);
    while($lex->NextToken())
    {
      if($lex->token == varTk)
      {
	// check if variable is a formula
	if(isset($formulaFields[$lex->value]))
	{
	  // check if the formula variable has already been decomposed
	  if(isset($formulas[$lex->value]))
	    $out .= '(' . $formulas[$lex->value] . ')';
	  else
	  {
  	    // decompose formula variable
	    $out .= '(' . $this->Decompose($formulas, 
					   $formulaFields[$lex->value], 
					   $formulaFields, $params);
	    $out .= ')';
	  }
	}
	else
	  // prints normal variable
	  $out .= $this->PrintVar($lex->value, $params);
      }
      else if($lex->token == caseTk ||
	      $lex->token == whenTk ||
	      $lex->token == thenTk ||
	      $lex->token == endTk ||
	      $lex->token == andTK ||
	      $lex->token == orTk)
	$out .= ' '.$lex->value . ' ';
      else
	$out .= $lex->value;
    }

    if($value['maxValue']!=NULL)
    {
      $out = "IF((@v:=$out)>".$value['maxValue'].',';
      $out .= $value['maxValue'].',@v)';
    }
    if($value['minValue']!=NULL)
    {
      $out = "IF((@v:=$out)<".$value['minValue'].',';
      $out .= $value['minValue'].',@v)';
    }
    return $out;
  }

  protected function PrintVar($fieldName, & $params)
  {
      return $fieldName;
  }

  protected function GetFieldsDef()
  {
    if($this->elementId == '*')
      return parent::GetFieldsDef();

    $SQL = 'SELECT labelId,f.fieldName,type,template,langTemplate,outputFuncRef,';
    $SQL .= 'size,attributes,consistType,`maxValue`,minValue,allowNull,trimField,required,uniq,';
    $SQL .= 'staticProp,accessMode, keyField, orderby, ';
    $SQL .= '(SELECT value FROM LANG_Text lei WHERE ei.info=lei.labelId ';
    $SQL .= "AND lei.lang='".$this->pageBuilder->lang."' ";
    $SQL .= ') as info ';
    $SQL .= 'FROM FORM_Field f, GAME_ElementIndicator ei ';
    $SQL .= "WHERE f.tableName ='". $this->table . "' ";
    $SQL .= "AND f.tableName=ei.tableName AND f.fieldName=ei.fieldName ";
    if($this->elementId != NULL)
      $SQL .= "AND elementId='".$this->elementId."' ";
    else
      $SQL .= 'AND elementId IS NULL ';

    if($this->typeId != NULL)
      $SQL .= "AND typeId='".$this->typeId."' ";

    $SQL .= "ORDER BY orderby ";
    $SQL .= 'LOCK IN SHARE MODE ';
    // Executes SQL statement
    return $this->dbDriver->GetAll($SQL);
  } 

  protected function &GetField(& $fieldDef, & $values, $keyFields,  
			       & $templates, $sourceId, $dateFormat = NULL)
  {
    switch ($fieldDef['type'])
    {
    case 'currencySlider':
      return $this->GetCurrencySlider($fieldDef, $values);

    case 'calendars':
      return $this->GetCalendars($fieldDef, $values);

    default: 
      return parent::GetField($fieldDef,$values,$keyFields,  
			      $templates,$sourceId,$dateFormat);

    }
  }

  protected function &GetCalendars(& $fieldDef, $values)
  {
    $fieldDef['value'] = $values[$fieldDef['fieldName']];
    $modobj = new AV_ModObject('field', $this->pageBuilder,
				 'timelineAno','timelineAno');
    $modobj->attrs = $fieldDef;
    $modobj->isArray = true;

    
    $anoIni = date('Y');
    $anoFim = $anoIni +(int)($this->pageBuilder->siteConfig->getVar('game','lastTurn')/12)-1;
    $anoAtual = $anoIni + (int)($values[$fieldDef['fieldName']]/12);
    $meses = $values[$fieldDef['fieldName']] % 12;

    for($ano = $anoIni; $ano<=$anoFim; $ano++)
    {
      $field = new AV_ModObject('field', $this->pageBuilder);
      $field->attrs['ano'] = $ano;
      if($ano<$anoAtual)
	$field->attrs['meses'] = 12;
      elseif($ano==$anoAtual)
	$field->attrs['meses'] = $meses;
      $modobj->children[] = $field;
    }
    return $modobj;
  }

  protected function &GetCurrencySlider(& $fieldDef, $values)
  {
    // prevents locale problems becouse the field will be loaded from javascript
    // except for reports
    if($this->typeId!=4 && $this->typeId!=6)
      setlocale(LC_ALL, 'en_US');
    $field = & $this->GetFloatField($fieldDef, $values);
    // reverts default locale
    if($this->typeId!=4 && $this->typeId!=6)
      setlocale(LC_ALL, $this->lang.'.utf8');
    return $field;
  }
 
  protected function &GetPercentField(& $fieldDef, $values)
  {
    if($values[$fieldDef['fieldName']]==-999999999)
      $fieldDef['attributes'] .= ' disabled';
    else
    {
      $values[$fieldDef['fieldName']] *= 100;
      $fieldDef['level'] = round($values[$fieldDef['fieldName']]/10);
      if($fieldDef['level']>10)
	$fieldDef['level'] = 10;
      elseif($fieldDef['level']<=0)
	$fieldDef['level'] = 0;
    }

    // prevents locale problems becouse the field will be loaded from javascript
    // for actions
    if($this->typeId==5)
      setlocale(LC_ALL, 'en_US');

    $locale = localeconv();
    $fieldDef['value'] = number_format($values[$fieldDef['fieldName']],
    				       0,
    				       $locale['mon_decimal_point'],
    				       $locale['mon_thousands_sep']);
    $modobj = new AV_ModObject('field', $this->pageBuilder);
    $modobj->attrs = $fieldDef;
    
    // reverts default locale
    if($this->typeId==5)
      setlocale(LC_ALL, $this->lang.'.utf8');
    return $modobj;
  }

  protected function &GetTextField(& $fieldDef, $values)
  {
    if($fieldDef['consistType'] == 'integer')
    { 
      if($fieldDef['maxValue'] !== NULL &&
	 $fieldDef['minValue'] !== NULL)
      {
	if($values[$fieldDef['fieldName']]==-999999999)
	  $fieldDef['attributes'] .= ' disabled';
	else
	{
	  $fieldDef['level'] = round(($values[$fieldDef['fieldName']]-$fieldDef['minValue'])/($fieldDef['maxValue']-$fieldDef['minValue'])*10);
	  if($fieldDef['level']>10)
	    $fieldDef['level'] = 10;
	  elseif($fieldDef['level']<=0)
	    $fieldDef['level'] = 0;
	}
      }
      $locale = localeconv();
      $fieldDef['value'] = number_format($values[$fieldDef['fieldName']],
					 0,
					 $locale['mon_decimal_point'],
					 $locale['mon_thousands_sep']);

    }
    else
      $fieldDef['value'] = $values[$fieldDef['fieldName']];
    $modobj = new AV_ModObject('field', $this->pageBuilder);
    $modobj->attrs = $fieldDef;
    return $modobj;
  }

  protected function &GetFloatField(& $fieldDef, $values)
  {
    if($fieldDef['maxValue'] != NULL &&
       $fieldDef['minValue'] != NULL)
    {
      if($values[$fieldDef['fieldName']]==-999999999)
	$fieldDef['attributes'] .= ' disabled';
      else
      {
	$fieldDef['level'] = round(($values[$fieldDef['fieldName']]-$fieldDef['minValue'])/($fieldDef['maxValue']-$fieldDef['minValue'])*10);
	if($fieldDef['level']>10)
	  $fieldDef['level'] = 10;
	elseif($fieldDef['level']<=0)
	  $fieldDef['level'] = 0;
      }
    }
    $modobj = & parent::GetFloatField($fieldDef,$values);
    if($fieldDef['type']=='float')
    {
      $fieldDef['value'] = number_format($values[$fieldDef['fieldName']],
					 0,
					 $locale['mon_decimal_point'],
					 $locale['mon_thousands_sep']);
      $modobj->attrs = $fieldDef;
    }
    return $modobj;
  }

}

?>