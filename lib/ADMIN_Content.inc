<?php

require_once('ADMIN_Page.inc');

abstract class ADMIN_Content extends ADMIN_Page
{
  protected $tabs;
  protected $script;
  protected $pageLabels;

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminRoot');
    $this->pageBuilder->rootTemplate->addText($this->pageId,'pageId');
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');
      
    $this->pageLabels = & $this->LoadModule("LANG_GetPageLabels");

    $this->pageLabels->PrintHTML($this->pageBuilder->rootTemplate, NULL, 
				 'htmlentities_utf');

    $sessionH = &$this->pageBuilder->sessionH;

    if($_REQUEST['doLogout'] == '1')
      $sessionH->attemptLogout();

    if($sessionH->isMember())
      $this->LoadContent();
    else if($_REQUEST['doLogin'] == '1')
      $this->Login();
    else
      $this->LoadLogin();

    $this->pageBuilder->rootTemplate->addText((string)$this->script, 'onLoad');
  }

  protected function LoadContent()
  { 
    $contentTpl = $this->pageBuilder->loadTemplate('adminContent');
    $this->pageBuilder->rootTemplate->addTemplate($contentTpl, 'content');

    $this->pageLabels->PrintHTML($contentTpl, NULL, 'htmlentities_utf');

    $this->LoadTabs($contentTpl);

    $this->script .= "Initiate('".$this->pageBuilder->pageId."');";

    $this->LoadTabContent($contentTpl);

    $this->LoadFooter($contentTpl);

    if(isset($this->userSession))
      $this->userSession->PrintHTML($contentTpl);
  }

  protected function LoadTabs(& $contentTpl)
  {
    $adminTabs = & $this->pageBuilder->loadTemplate('adminTabs');
    $base=stripslashes($_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']));
    $adminTabs->addText($base,'websiteAddress');
    $adminTabs->addText($this->pageBuilder->avVersion,'avVersion');
    $adminTabs->addText("class='selected'",$this->pageId.'Selected');
    $this->pageLabels->PrintHTML($adminTabs, NULL, 'htmlentities_utf');
    $contentTpl->addTemplate($adminTabs,'mainTabs');
  }

  abstract protected function LoadTabContent(& $contentTpl);

  protected function LoadLogin($login)
  {
    $loginTpl = & $this->pageBuilder->loadTemplate('adminLogin'); 
    $this->pageLabels->PrintHTML($loginTpl, NULL, 'htmlentities_utf');
    $loginTpl->addText($this->pageBuilder->avVersion,'avVersion');
    $loginTpl->addText($this->pageId, 'pageId');
    $loginTpl->addText((string)$_REQUEST['login'], 'login');
    $this->pageBuilder->rootTemplate->addTemplate($loginTpl, 'content');
    $this->script .= "document.getElementById('login').focus();";
  }

  protected function Login()
  {
    try
    {
      $checkSSL = $this->pageBuilder->siteConfig->getVar('admin','checkSSL');
      if($checkSSL && $_SERVER['HTTPS']=='')
      {
	$_GET['labelId'] = 'onlySSLMsg';
	$_GET['lang'] = $this->lang;
	$getLabel = $this->LoadModule('LANG_GetLabel');
	throw new AV_Exception(PERMISSION_DENIED, $this->lang,
			       array('addMsg'=>' '.$getLabel->attrs['value']));
      }

      if($this->allowedUsers != NULL &&
	 !in_array($_REQUEST['login'],$this->allowedUsers))
	throw new AV_Exception(PERMISSION_DENIED, $this->lang);

      $module = $this->LoadModule('UM_Login');
      $this->UpdateUserSession();
      $this->LoadContent();
    }
    catch(Exception $e)
    {
      $this->LoadLogin();
      $this->script .= "alert('".$e->getMessage()."');";
    }
  }

  // default empty footer
  protected function LoadFooter(& $contentTpl)
  {
  }
}

?>