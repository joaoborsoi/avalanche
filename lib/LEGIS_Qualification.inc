<?php

class LEGIS_Qualification
{
  protected $dbDriver;
  protected $module;

  function __construct(& $module)
  {
    $this->module = & $module;
    $this->dbDriver = & $module->dbDriver;
  }

  function Get($list)
  {
    $SQL = 'SELECT np.name,(YEAR(CURDATE())-YEAR(birthday))-';
    $SQL .= '(RIGHT(CURDATE(),5)<RIGHT(birthday,5)) as age,rg,cpf,logradouro,';
    $SQL .= 'rs,sex,nationality,mother,profession,wageClass,number,numberAux,';
    $SQL .= 'bairro,postalCode,pensionerOfName,pensionerOfProfession,';
    $SQL .= "DATE_FORMAT(retirement,'%d/%m/%Y') as retirement,";
    $SQL .= "DATE_FORMAT(pensionerOfRetirement,'%d/%m/%Y') as ";
    $SQL .= 'pensionerOfRetirement,';

    // subselect para maritalStatus
    $SQL .= '(SELECT IF(np.sex=2,feminineName,name) ';
    $SQL .= 'FROM LEGIS_MaritalStatus s WHERE s.id=np.maritalStatus) ';
    $SQL .= 'as maritalStatus,';

    // subselect para situationSum (1 - aposentado, 4 - pensionista, 5 - ambos,
    // 0 - nda)
    $SQL .= '(SELECT SUM(situationId) FROM LEGIS_CustomerSituation cs ';
    $SQL .= 'WHERE cs.docId=np.docId AND situationId IN (1,4)) ';
    $SQL .= 'as situationSum,';

    // subselect para city
    $SQL .= '(SELECT cidade_descricao FROM cidade c ';
    $SQL .= 'WHERE c.cidade_codigo=np.cidade_codigo) as city, ';

    // subselect para state
    $SQL .= '(SELECT uf_sigla FROM uf ';
    $SQL .= 'WHERE uf.uf_codigo=np.uf_codigo) as state,';

    // subselect para nationality
    $SQL .= '(SELECT IF(np.sex=2,feminineNationality,masculineNationality) ';
    $SQL .= 'FROM EM_Country c WHERE c.docId=np.nationality) ';
    $SQL .= 'as nationality ';

    $SQL .= 'FROM EM_NaturalPerson np ';
    $SQL .= "WHERE np.docId IN ('".implode("','",$list)."') ";
    $rows = $this->dbDriver->GetAll($SQL);

    setlocale(LC_ALL,$this->module->pageBuilder->lang);

    $num = count($rows);

    if($num==0)
      return array();

    $error = array();
    $labels = array();
    foreach($rows as $key => $child)
    {
      // divisão de clientes
      if($key>0)
	$qual .= '; ';

      // verifica se sexo está preenchido, senão considera masculino
      if($child['sex']==NULL)
	$this->AddError($error,$labels,$key,'sex');

      $qual .= '<strong>';

      // numero
      if($num>1)
	$qual .= ($key+1) . ' - ';

      // Nome
      $qual .= '<u>'.htmlentities_utf(mb_strtoupper($child['name'])).'</u></strong>';

      // Data de Nascimento
      if($child['age'] != NULL)
	$qual .= ', <strong><u>'.$child['age'].' anos</u></strong> de idade';
      else
	$this->AddError($error,$labels,$key,'birthday');

      // RS
      if($child['rs'] != NULL)
      {
	$qual .= ', registrado no sistema da <strong><u>FESP</u></strong> ';
	$qual .= 'sob o n&ordm; '.$child['rs'];
      }
      else
	$this->AddError($error,$labels,$key,'rs');

	 // Nacionalidade
      if($child['nationality'] != NULL)
	$qual .= ', '.$child['nationality'];
      else
	$this->AddError($error,$labels,$key,'nationality');
      
      // Filiação Materna
      if($child['mother'] != NULL)
      {
	$qual .= ', ' . (($child['sex']==2)?'filha':'filho').' de ';
	$qual .= htmlentities_utf(mb_ucwords(mb_strtolower($child['mother'])));
      }
      else
	$this->AddError($error,$labels,$key,'mother');

      // Estado Civil
      if($child['maritalStatus'] != NULL)
	$qual .= ', '. htmlentities_utf(mb_strtolower($child['maritalStatus']));
      else
	$this->AddError($error,$labels,$key,'maritalStatus');

      // pensionista
      if($child['situationSum'] >= 4)
      {
	if($child['maritalStatus'] != NULL)
	  $qual .= ' e ';
	else
	  $qual .= ', ';
	$qual .= 'pensionista';

	// Nome
	if($child['pensionerOfName'] != NULL)
	{
	  $qual .= ' de '; 
	  $qual .= htmlentities_utf(mb_ucwords(mb_strtolower($child['pensionerOfName'])));
	}
	else
	  $this->AddError($error,$labels,$key,'pensionerOfName');
	
	$qual .= ' (';

	// Profissão
	if($child['pensionerOfProfession'] != NULL)
	  $qual.=htmlentities_utf(mb_strtolower($child['pensionerOfProfession'])).' ';
	else
	  $this->AddError($error,$labels,$key,'pensionerOfProfession');

	$qual .= (($child['sex']==2)?'aposentada':'aposentado');

	// Data da Aposentadoria
	if($child['pensionerOfRetirement'] != NULL)
	  $qual .= ' em '.$child['pensionerOfRetirement'];
	else
	  $this->AddError($error,$labels,$key,'pensionerOfRetirement');

	// Classe Salarial
	$qual .= ', na classe salarial 605)';	
      } //if($child['situationSum'] >= 4)
      else
      {
	// não pensionista

	// Profissão
	if($child['profession'] != NULL)
	  $qual .= ', '.htmlentities_utf(mb_strtolower($child['profession']));
	else
	  $this->AddError($error,$labels,$key,'profession');

	// Aposentado 
	if($child['situationSum']==1)
	{
	  if($child['profession'] == NULL)
	    $qual .= ',';

	  $qual .= ' '.(($child['sex']==2)?'aposentada':'aposentado');
	  
	  //  Data da Aposentadoria
	  if($child['retirement'] != NULL)
	    $qual .= ' em '.$child['retirement'];
	  else
	    $this->AddError($error,$labels,$key,'retirement');

	  // Classe Salarial
	  if($child['wageClass'] != NULL)
	    $qual .= ', na classe salarial '.$child['wageClass'];
	  else
	    $this->AddError($error,$labels,$key,'wageClass');
	}
      } // if($child['situationSum'] >= 4) else

      // RG
      if($child['rg'] != NULL)
	$qual .= ', portador da cédula de identidade RG n&ordm; '.$child['rg'];
      else
	$this->AddError($error,$labels,$key,'rg');


      // CPF,
      if($child['cpf'] != NULL)
      {
	$qual .= ', inscrito no CPF/MF sob o n&ordm; ';
	$qual .= number_format(substr($child['cpf'],0,-2),0,',','.');
	$qual .= '-'.substr($child['cpf'],-2);
      }
      else
	$this->AddError($error,$labels,$key,'cpf');

      // Logradouro
      $end = '';
      if($child['logradouro'] != NULL)
      {
	$end .= ', residente na ';
	$end .= htmlentities_utf(mb_ucwords(mb_strtolower($child['logradouro'])));

	// Número
	if($child['number'] != NULL)
	  $end .= ', '.htmlentities_utf($child['number']);
	else
	  $this->AddError($error,$labels,$key,'number');
	
	// Complemento
 	if($child['numberAux'] != NULL)
	  $end .= ' '.htmlentities_utf($child['numberAux']);
      }
      else
	$this->AddError($error,$labels,$key,'logradouro');
      
      // Bairro
      if($child['bairro'] != NULL)
      {
	if($end ==NULL)
	  $end = ', residente em ';
	else
	  $end .= ' - ';

	$end .= htmlentities_utf(mb_ucwords(mb_strtolower($child['bairro'])));
      }
      else
	$this->AddError($error,$labels,$key,'bairro');

      // Cidade
      if($child['city'] != NULL)
      {
	if($end==NULL)
	  $end = ', residente em ';
	else
	  $end .= ' - ';

	$end .= htmlentities_utf(mb_ucwords(mb_strtolower($child['city'])));
      }
      else
	$this->AddError($error,$labels,$key,'cidade_codigo');

      // Estado
      if($child['state'] != NULL)
      {
	if($end==NULL)
	  $end = ', residente em ';
	else
	  $end .= ' - ';

	$end .= $child['state'];
      }
      else
	$this->AddError($error,$labels,$key,'uf_codigo');

      // CEP
      if($child['postalCode'] != NULL)
      {
	if($end==NULL)
	  $end = ', residente em ';
	else
	  $end .= ' - ';

	$end .= 'CEP ' .substr($child['postalCode'],0,-3);
	$end .= '-'.substr($child['postalCode'],-3);
      }
      else
	$this->AddError($error,$labels,$key,'postalCode');

      $qual .= $end;


    } // foreach($rows as $key => $child)

    
    // recupera labels dos campos com erro e monta mensagens de erro
    if(count($labels>0))
    {
      $SQL = 'SELECT fieldName, value FROM FORM_Field f,LANG_Label l ';
      $SQL .= "WHERE tableName='EM_NaturalPerson' ";
      $SQL .= "AND fieldName IN ('".implode("','",$labels)."') ";
      $SQL .= 'AND f.labelId=l.labelId ';
      $SQL .= "AND lang='".$this->module->pageBuilder->lang."'";
      $labels = $this->dbDriver->GetAll($SQL);

      foreach($labels as $label)
	$labelList[$label['fieldName']] = $label['value'];

      foreach($error as $key=>$fields)
      {
	$msg = 'O cliente '.$rows[$key]['name'].' não possui os seguintes ';
	$msg .= 'campos: ';
	foreach($fields as $key2=>$field)
	{
	  if($key2>0)
	    $msg .= ', ';
	  if(substr($field,0,11)=='pensionerOf')
	    $msg .= 'Pensionista de - ';
	  $msg .= $labelList[$field];
	}
	$res['warningMessages'][] = $msg;
      }
    }
      
    $res['qualification'] = $qual;
    return $res;
  }


  function AddError(&$error, &$labels, $key, $fieldName)
  {
    $error[$key][]=$fieldName;
    if(!in_array($fieldName,$labels))
      $labels[]=$fieldName;
  }

}

?>