<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


class LIB_TakeFolderOwnership extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'folderId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  function Execute()
  {
    $folder = new LIB_Folder($this);
    $this->attrs['login'] = $folder->TakeOwnership($this->fields['folderId']);
  }
}

?>
