<?php

require_once('UM_User.inc');

//--Class: L2H_Manager
//--Desc: latex2html manager class.
class L2H_Manager
{
  //
  //--Private
  //
  var $dbDriver;                 //--Desc: Database driver handler
  var $module;                   //--Desc: Module's handler

  //
  //--Public
  //

  //--Method: L2H_Manager
  //--Desc: Constructor.
  function L2H_Manager(&$module)
  {
    $this->module = & $module;
    $this->dbDriver = & $module->dbDriver;
  }


  //--Method: CheckAccessRight
  //--Desc: Checks if member can access the document
  function CheckAccessRight($path)
  {
    $SQL = 'SELECT userId, groupId FROM L2H_Path WHERE ';
    $SQL .= "path = '$path' LOCK IN SHARE MODE ";
    $data = $this->dbDriver->GetRow($SQL, $status);
    if($status != SUCCESS)
      return $status;
    if(count($data) == 0)
      return NOT_FOUND;

    if($data['userId'] !== NULL ||
       $data['groupId'] !== NULL)
    {
      $userMan = new UM_User($this->module);
      $memData = $userMan->GetMemberData($status, 'shared', false, false);
      if($status != SUCCESS)
	return $status;

      if($memData['userId'] == $data['userId'])
	return SUCCESS;

      $groups = $userMan->GetMemberGroupsIds($status);
      if($data['groupId'] !== NULL && in_array($data['groupId'], $groups))
	return SUCCESS;

      return PERMISSION_DENIED;
    }
    return SUCCESS;
  }

}