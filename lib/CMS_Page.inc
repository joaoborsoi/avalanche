<?php

require_once('AV_Page.inc');
require_once('CMS_SearchTools.inc');

class CMS_Page extends AV_Page
{
  protected $searchTools;
  public $pageLabels;
  public $memberData;
  public $memberGroups;
  public $menuPath;
  public $subareas;
  public $base;

  protected $public;

  function &LoadLoginForm($reload = true, $location = NULL,$fields = NULL)
  {
    $script = "<script type='text/javascript'>\n";
    $script .= "\$(document).ready(function() {\n";
    $script .= "avForm.OnLoad('loginFormField');\n";
    $script .= "});\n";
    $script .= '</script>';
    $this->pageBuilder->rootTemplate->addText($script,'header');
 
    if($fields == NULL)
    {
      // loads area content
      $loginPath = $this->pageBuilder->siteConfig->getVar('cms','loginPath');
      $_GET['path'] = '/content/Menu/'.$loginPath;
      $getFolder = & $this->LoadModule('LIB_GetFolder');
      $fields = $getFolder->children[0]->attrs;
    }

     // loads document title
    $title = htmlentities_utf($fields['title']);
    $this->pageBuilder->rootTemplate->addText((string)$title,'title');

    // folder content title
    $content = "<h1>$title</h1>\n";

    // folder content
    $content .= $fields['content'];
 
    $login = &$this->pageBuilder->loadTemplate('cmsLoginForm');
    $login->addText($content,'content');

    if($this->pageBuilder->siteConfig->getVar('cms','register'))
    {
      $registerPath = $this->pageBuilder->siteConfig->getVar('cms','registerPath');
      $registerLink = "<p>" . $this->pageLabels->attrs['notRegisteredYetTag'];
      $registerLink .= " <a href='area/$registerPath'>";
      $registerLink .= $this->pageLabels->attrs['registerHereTag'] . "</a></p>";
      $login->addText($registerLink,'registerLink');
    }
    $login->addText((($reload)?'1':'0'),'reload');
    if($location != NULL)
      $login->addText($location,'location');
    $this->pageBuilder->rootTemplate->addTemplate($login,'mainContent');
    $this->pageLabels->PrintHTML($login);

    return $login;
  }

  function LoadSearchInfo(& $search, $offset, $limit)
  {
    return $this->searchTools->LoadSearchInfo($search,$offset,$limit);
  }

  function LoadSearchNav(& $search, $offset, $limit, $link, 
				   $filter=NULL,$data=NULL)
  {
    return $this->searchTools->LoadSearchNav($search,$offset,$limit,$link,$filter,$data);
  }

  function LoadSubareas()
  {
    $this->pageBuilder->rootTemplate->addText((string)$this->subareas,
					      'subareas');    
  }

  function LoadImages($imageList,$thumbSrc='scale')
  {
    if($imageList == NULL)
      return;

    $imageList = explode(',',$imageList);

    foreach($imageList as $imageId)
    {
      $_GET['docId'] = $imageId;
      $_GET['fieldName'] = 'contentType';
      $getImg = $this->LoadModule('LIB_GetDoc');
      $contentType = explode('/',$getImg->children[0]->attrs['contentType']);
      $_GET['fieldName'] = 'title';
      $getImg = $this->LoadModule('LIB_GetDoc');

      switch($contentType[0])
      {
      case 'image':
	$extra .= "<li><a href='images/$imageId' class='highslide' ";
	$extra .= "onclick='return hs.expand(this)'>\n";
	$extra .= "<img src='$thumbSrc/$imageId' alt='";
	$extra .= $getImg->children[0]->attrs['title']."' ";
	$extra .= "title='Click to enlarge' /></a>\n";
	$extra .= '<small>'.$getImg->children[0]->attrs['title'].'</small>';
	$extra .= "</li>";
	break;
      default:
	$extra .= "<li><a class='imageItem ".str_replace('.','-',
							 $contentType[1])."' ";
	$extra .= "href='adminGetFileContent.av?fieldName=content";
	$extra .= "&amp;docId=$imageId'>".$getImg->children[0]->attrs['title'];
	$extra .= "</a></li>";
	break;
      }
    }

    if($extra != NULL)
      $extra = "<ul class='display' id='imageList'>\n$extra\n</ul>";

    return $extra;
  }


  function GetResume($text,$filter)
  {
    // maximum characters per result
    $maxWidth = 150;
    // maximum result lines
    $maxLines = 2;

    $text = trim(strip_tags($text));

    if($filter !== NULL && $filter !== '')
    {
      // prepares filter
      $filter = str_replace("\"", '', $filter);
      while(strpos($filter, '  ') !== false)
	$filter = str_replace('  ', ' ', $filter);
      $filter = explode(' ',$filter);
    

      $lines = explode("\n",$text);
      $text = '';
      foreach($lines as & $line)
      {
	$phraseList = explode('.',$line);
	foreach($phraseList as & $phrase)
	{
	  if($this->FindFilter($phrase, $filter))
	    $text .= $phrase;	
	}
      }
    }

    if(strlen($text)>$maxWidth)
    {
      $text = wordwrap($text,150);
      $text = explode("\n",$text);
      $text = $text[0];
    }

    if($filter !== NULL || $filter !== '')
    {
      foreach($filter as & $word)
	$text = str_replace($word,"<strong>$word</strong>",$text);
    }
    return $text;
  }

  protected function FindFilter(&$phrase, &$filter)
  {
    foreach($filter as & $word)
      if(stripos($phrase, $word)!==false)
	return true;

    return false;
  }

  // loads defaut template and builds main menu
  protected function LoadPage()
  {   
    $this->searchTools = new CMS_SearchTools();

    // root template
    $this->pageBuilder->rootTemplate('cmsMain.tpt');

    $_GET['lang'] = $this->lang;
    $this->pageLabels = & $this->LoadModule('LANG_GetPageLabels');
    $this->pageLabels->PrintHTML($this->pageBuilder->rootTemplate);

    // avalanche version
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');

    // logout if requested
    if($_REQUEST['logout'] == '1')
      $this->pageBuilder->sessionH->attemptLogout();


    // sets site base
    $this->base = dirname($_SERVER['PHP_SELF']);
    if($this->base!='/') 
      $this->base .= '/';
    $this->base = '//'.$_SERVER['HTTP_HOST'].$this->base;
    $this->pageBuilder->rootTemplate->addText($this->base,'base');

    // body id as the pageId
    $bodyAttrs = "id='".$this->pageId."'";
    $this->pageBuilder->rootTemplate->addText($bodyAttrs,'bodyAttrs');
    	
    // if it is a registered user, prints login info
    if($this->pageBuilder->sessionH->isMember())
    {
      $loginInfo = "<p id='userInfo'><span>";
      $loginInfo .= $this->userSession->attrs['login']."</span></p>";
      $this->pageBuilder->rootTemplate->addText($loginInfo,'loginInfo');

      // loads user's groups
      $this->memberData = &$this->userSession->attrs;
      $getMemberGroups = $this->LoadModule('UM_GetMemberGroups');
      foreach($getMemberGroups->children as & $child)
	$this->memberGroups[$child->attrs['groupId']]=
	  $child->attrs['groupName'];

    }
    $this->pageBuilder->rootTemplate->addText('cmsSubscribe.av','rss');

    // show cart
    if($this->pageBuilder->siteConfig->getVar('cms','shopCart'))
    {
      $_GET['labelId'] = 'myCartTag';
      $_GET['lang'] = $this->lang;
      $getLabel = $this->LoadModule('LANG_GetLabel');
      $cartLink = "<p id='cartLink'><a href='shopCart.av'>";
      $cartLink .= $getLabel->attrs['value'];
      $cartLink .= "</a></p>\n";

      $this->pageBuilder->rootTemplate->addText($cartLink,'myCartTag');
    }

    $this->public = $this->pageBuilder->siteConfig->getVar('cms','public');
    
    // load available languages
    $this->LoadLanguages();


    // loads menu, submenu and path
    $this->LoadMenu();

    // loads history
    if($this->pageBuilder->siteConfig->getVar('cms','history'))
      $this->LoadHistory();

    $this->LoadContent();
  }

  protected function LoadLanguages()
  {
    $getLangList = & $this->LoadModule('LANG_GetLangList');

    if(count($getLangList->children)<=1)
      return;

    $link = 'http://'.$_SERVER['HTTP_HOST'];
    $pos = strpos($_SERVER['REQUEST_URI'],'?');
    if($pos===false)
      $link .= $_SERVER['REQUEST_URI']."?";
    else
      $link .= $_SERVER['REQUEST_URI']."&amp;";
    
    $content = "<ul id='switch-lang'>\n";
    foreach($getLangList->children as $child)
    {
      $lang = $child->attrs['lang'];
      $langId = strtolower(str_replace('_','-',$lang));
      $content .= '<li';
      if($child->attrs['lang'] == $this->lang)
	$content .= " class='selected'><span>".$child->attrs['name']."</span>";
      else
      {
	$content .= "><a href='${link}lang=$lang' id='$langId'>";
	$content .= $child->attrs['name']."</a>";
      }
      $content .= "</li>";
    }

    $content .= "</ul>\n";
    $this->pageBuilder->rootTemplate->addText($content,'langList');
  }

  protected function GetMenuPath(& $getFolderList)
  {
    // if there is a document references, searches selected menu from document 
    // links
    if($_REQUEST['docId'] != NULL)
    {
      $docLinks = & $this->LoadModule('LIB_GetDocLinks');
      
      // check for a valid menu path
      foreach($docLinks->children as & $link)
      {
	if(!strncmp('/content/Menu/',$link->attrs['path'],14))
	  return addslashes(mb_substr($link->attrs['path'],14));
      }
    }
    else if($_REQUEST['path'] != NULL)
    {
      if(mb_substr($_REQUEST['path'],-1)!='/')
	return $_REQUEST['path'].'/';
      return $_REQUEST['path'];
    }
    return NULL;
  }

  protected function LoadMenu()
  {
    // sets 

    // loads default menu
    $_GET['path'] = '/content/Menu/';
    $_GET['fixedOrder'] = '1';
    $getFolderList = & $this->LoadModule('LIB_GetFolderList');

    // loads menu path
    $this->menuPath = $this->GetMenuPath($getFolderList);

    $menu = NULL;
    foreach($getFolderList->children as & $folder)
    {
      // anchor link
      $link = "<a href=\"area/".urlencode($folder->attrs['name'])."/\" ";
      $link .= ">";
      $link .= $folder->attrs['menuTitle']."</a>";

      if(count($getFolderList->children)>1)
	$menu .= "<li";

      $currPath = addslashes($folder->attrs['name']).'/';
      if(!strncmp($currPath, $this->menuPath,strlen($folder->attrs['name'])+1))
      {
	if(count($getFolderList->children)>1)
	  $menu .= " class='selected'";

	// initiates topPath
	if($this->pageBuilder->siteConfig->getVar('cms','topPathRoot'))
	{
	  if($this->menuPath == $currPath)
	    $topPath .= "<li class='active'>".$folder->attrs['menuTitle']."</li>\n";
	  else
	  {
	    if($this->pageBuilder->siteConfig->getVar('cms','menuParentPathLink'))
	      $topPath .= '<li>'.$link."</li>\n";
	    else
	      $topPath .= '<li>'.$folder->attrs['menuTitle']."</li>\n";
	  }
	}

	// checks for submenu and completes topPath
	$menuType = $this->pageBuilder->siteConfig->getVar('cms','menuType');
	require_once($menuType.'.inc');
	$submenuObj = new $menuType($this);

	if($submenuObj->LoadMenu($folder->attrs['name'],$submenu,$subareas,
				    $topPath))
	{
	  $this->pageBuilder->rootTemplate->addText($submenu,'submenu');

	  if($subareas != NULL)
	  {
	    $subareasUl = "<ul class='genericList'>$subareas</ul>";
	    $subareas = "<h2 class='subsection'>";
	    $subareas .= $this->pageLabels->attrs['sectionsTag']."</h2>\n";
	    $subareas .= $subareasUl;
	    $this->subareas = $subareas;
	  }

	}

      }
      if(count($getFolderList->children)>1)
	$menu .= ">$link</li>";
    }

    // login / register 
    if($this->pageBuilder->siteConfig->getVar('cms','login'))
    {
      // if it is logged
      if($this->pageBuilder->sessionH->isMember())
      {
	if($this->pageBuilder->siteConfig->getVar('cms','myAccount'))
	{
	  $menu .= "<li ".(($this->pageId=='cmsRegisterForm')?
			   "class='selected'":'').'>';
	  $menu .= "<a href='cmsRegisterForm.av'>";
	  $menu .= $this->pageLabels->attrs['myAccountTag']."</a></li>\n";
	}
	$menu .= "<li><a href='?logout=1'>";
	$menu .= $this->pageLabels->attrs['quitTag']."</a></li>";
      }
      else
      {
	$menu .= "<li ".(($this->pageId=='cmsLoginForm')?
			 "class='selected'":'').'>';
	$menu .="<a href='cmsLoginForm.av'>Login</a></li>";
	if($this->pageBuilder->siteConfig->getVar('cms','register'))
	{
	  $menu .= "<li ".(($this->pageId=='cmsRegisterForm')?
			   "class='selected'":'').'>';
	  $menu .= "<a href='cmsRegisterForm.av'>";
	  $menu .= $this->pageLabels->attrs['registerTag']."</a></li>";
	}
      }
    }

    if($menu != NULL)
    {
      $menu = "<ul id='topNav'>\n$menu</ul>\n";

      // print out result
      $this->pageBuilder->rootTemplate->addText($menu,'menu');
    }
    if($topPath != NULL)
    {
      $topPath = "<ol class='breadcrumb'>\n".$topPath."</ol>\n";
      $this->pageBuilder->rootTemplate->addText($topPath,'topPath');
    }
  }


  protected function LoadHistory()
  {
    $getHistory = & $this->LoadModule('AV_GetHistory');
    if(count($getHistory->children)==0)
      return;

    $history = "<div class='box'>\n";
    $history .= "<h3>Histórico</h3>\n<ul class='inner'>";
    foreach($getHistory->children as &$child)
    {
      $history .= "<li><a href='";
      $history .= str_replace("%2F", "/", urlencode($child->attrs['link']));
      $history .= "'>".$child->attrs['title'] . "</a></li>";
    }
    $history .= "</ul></div>\n";
    $this->pageBuilder->rootTemplate->addText($history,'history');
  }

  protected function LoadContent()
  {
    // loads content
    if(!$this->pageBuilder->sessionH->isMember() && !$this->public)
      $this->LoadLoginForm();
    else
    {
      if($_REQUEST['docId'] == NULL)
	// no document defined, load selected menu area (search)
	$this->LoadArea();
      else
	// load document content
	$this->LoadDocument($_REQUEST['docId']);
    }
  }

  protected function &LoadContentModule($folderTables)
  {
    // loads document data to the page
    $_GET['folderTables'] = $folderTables;
    $getContentModule = $this->LoadModule('LIB_GetContentModule');

    $bodyAttrs = $getContentModule->attrs['contentModule']."'";
    $this->pageBuilder->rootTemplate->addText($bodyAttrs,'bodyAttrs');

    require_once($getContentModule->attrs['contentModule'].'.inc');
    return  new $getContentModule->attrs['contentModule']($this);

  }

  protected function LoadArea()
  {
    try
    {
      // loads area content
      $_GET['path'] = '/content/Menu/'.$this->menuPath;
      $getFolder = & $this->LoadModule('LIB_GetFolder');

      $bodyAttrs = " class='area ";
      $this->pageBuilder->rootTemplate->addText($bodyAttrs,'bodyAttrs');
      
      // load area
      $folderTables = $getFolder->children[0]->attrs['folderTables'];
      $contentModule = &$this->LoadContentModule($folderTables);
      $contentModule->LoadArea($getFolder->children[0]->attrs,$_GET['path']);   
    }
    catch(Exception $e)
    {
      $message = "<p id='errormsg'>".$e->getMessage().'</p>';
      $this->pageBuilder->rootTemplate->addText($message,'mainContent');
    }
  }

  protected function LoadDocument($docId)
  {
    try
    {
      // read document data from database
      $_GET['docId']= $docId;
      $_GET['dateFormat'] = NULL;
      $getDoc = $this->LoadModule('LIB_GetDoc');

      $folderTables = $getDoc->children[0]->attrs['folderTables'];

      $bodyAttrs = " class='content ";
      $this->pageBuilder->rootTemplate->addText($bodyAttrs,'bodyAttrs');

      $contentModule = & $this->LoadContentModule($folderTables);
      $contentModule->LoadDocument($getDoc->children[0]->attrs, $getDoc->children[0]);
    }
    catch(Exception $e)
    {
      $message = "<p id='errormsg'>".$e->getMessage().'</p>';
      $this->pageBuilder->rootTemplate->addText($message,'mainContent');
    }

  }
}

?>
