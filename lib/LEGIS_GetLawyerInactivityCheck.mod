<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Process.inc');

class LEGIS_GetLawyerInactivityCheck extends AV_DBOperationObj
{
  function LoadFieldsDef()
  { 
    $this->fieldsDef[0]['fieldName'] = 'userId';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;
  }

  function Execute()
  { 
    $this->isArray = true;
    $process = new LEGIS_Process($this);
    $result = $process->GetLawyerInactivityCheck($this->fields['userId']);
    $this->LoadChildrenFromRows($result, 'check');
  } 

}

?>
