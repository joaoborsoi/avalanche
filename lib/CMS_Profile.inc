<?php

require_once('CMS_Content.inc');

class CMS_Profile extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    // dummy
  }

  function LoadArea(&$fields,$path)
  {
    parent::LoadArea($fields,$path);

    // search for files at the folder
    $_GET['path'] = $path;
    $_GET['orderBy'] = 'fixedOrder';
    $_GET['order'] = '1';
    $_GET['returnFields'] = 'title,bio,imageList';
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsPerPage');
 
    $search = $this->page->LoadModule('LIB_Search');

    // encoded menu path for the links
    $encMenuPath = str_replace("%2F", "/", 
			       urlencode(stripslashes($this->menuPath)));

    if($search->attrs['totalResults']>0)
    {
      $content .= $this->LoadList($search);
      $content .= $this->page->LoadSearchNav($search,$_REQUEST['offset'],
					     $_GET['limit'],
					     'area/'.$encMenuPath);
    }

    $this->pageBuilder->rootTemplate->addText((string)$content,'mainContent');
  }

  function LoadList($search)
  {
    $content .= "<ul class='profile-list'>\n";
    foreach($search->children as & $child)
    {
      $content .= "<li>\n";
      $content .= $this->LoadListItem($child->attrs);
      $content .= "</li>\n";
    }
    $content .= "</ul>\n";
    return $content;
  }

  function LoadListItem(& $fields)
  {
    $content .= "<h4>";
    if($fields['imageList']!=NULL)
    {
      $content .= "<img src='thumb/".$fields['imageList']."' alt='";
      $content .= $fields['title']."' />\n";
    }
    $content .= $fields['title']."</h4>\n";
    $content .= $fields['bio'];
    return $content;
  }
}

?>
