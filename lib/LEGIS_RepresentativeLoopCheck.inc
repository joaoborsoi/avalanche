<?php

require_once('LEGIS_Customer.inc');

class LEGIS_RepresentativeLoopCheck extends LEGIS_Customer
{
  protected $entityId;

  function Check($customer, $representative)
  {
    $this->entityId = $customer;

    // cria tabela para armazenar clientes representativos recursivos
    $tableName = uniqid('LEGIS_RepresentativeCheck_',false);
    $SQL = "CREATE TEMPORARY TABLE $tableName (";
    $SQL .= 'entityId INTEGER UNSIGNED NOT NULL)';
    $SQL .= "SELECT $representative as entityId, ";
    $SQL .= "$representative as representative  ";
    $this->dbDriver->ExecSQL($SQL);

    // busca clientes representativos recursivos
    try
    {
      $this->GetRecursiveRepresentative($tableName);
      $this->dbDriver->ExecSQL("DROP TABLE $tableName");
    }
    catch(AV_Exception $e)
    {
      $this->dbDriver->ExecSQL("DROP TABLE $tableName");
      throw $e;
    }
  }


  function GetRecursiveRepresentative($tableName, $lock = true)
  {
    $SQL = "SELECT entityId FROM $tableName ";
    $SQL .= "WHERE representative='".$this->entityId."' ";
    $entityId = $this->dbDriver->GetOne($SQL,$status);

    if($entityId != NULL)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg'=>' (Representação em loop detectada'));

    parent::GetRecursiveRepresentative($tableName,$lock);
  }
}

?>