<?php

require_once("AV_DBOperationObj.inc");
require_once("CHAT_Manager.inc");

class CHAT_SendMessage extends AV_DBOperationObj
{
   
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'roomId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
  
    $this->fieldsDef[1]['fieldName'] = 'message';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['stripSlashes'] = false;

    $this->fieldsDef[2]['fieldName'] = 'target';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
  }
  
  
  function Execute()
  {   
     $sessionId = $this->pageBuilder->sessionH->sessionID;
     
     $maganer = new  CHAT_Manager($this);
     $maganer->LoadMessages();
     return $maganer->SendMessage($this->fieldParser->fields['roomId'],
                                  $sessionId,
                                  $this->fieldParser->fields['message'],
                                  $this->fieldParser->fields['target']);
  }
}

?>