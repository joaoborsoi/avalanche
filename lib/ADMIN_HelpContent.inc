<?php

require_once('ADMIN_Content.inc');

class ADMIN_HelpContent extends ADMIN_Content
{
  function LoadTabContent(& $contentTpl)
  {
    $helpContentTpl = $this->pageBuilder->loadTemplate("adminHelpContent");
    $helpContentTpl->addText((string)$_REQUEST['helpId'],'helpId');
    $contentTpl->addTemplate($helpContentTpl, 'tabContent');
    return NULL;
  }
}

?>