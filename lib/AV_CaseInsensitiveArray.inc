<?php

class AV_CaseInsensitiveArray extends ArrayObject 
{
    public function __construct(Array $initial_array = array()) 
    {
      $initial_array = array_change_key_case($initial_array,CASE_LOWER);
      parent::__construct($initial_array);
    }
    
    public function offsetSet($offset, $value) 
    {
      if(is_string($offset))
	$offset = strtolower($offset);
      parent::offsetSet($offset,$value);
    }

    public function offsetExists($offset) 
    {
      if(is_string($offset))
	$offset = strtolower($offset);
      return parent::offsetExists($offset);
    }

    public function offsetUnset($offset) 
    {
      if(is_string($offset))
	$offset = strtolower($offset);
      return parent::offsetUnset($offset);
    }

    public function offsetGet($offset) 
    {
      if(is_string($offset))
	$offset = strtolower($offset);
      return parent::offsetGet($offset);
    }
}

?>