<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_FacebookLogin extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $i = 0;

    $this->fieldsDef[$i]['fieldName'] = 'code';
    $this->fieldsDef[$i]['required'] = false;
    $this->fieldsDef[$i]['allowNull'] = true;
    $this->fieldsDef[$i]['stripSlashes'] = false;
    $i++;

    $this->fieldsDef[$i]['fieldName'] = 'state';
    $this->fieldsDef[$i]['required'] = false;
    $this->fieldsDef[$i]['allowNull'] = true;
    $this->fieldsDef[$i]['stripSlashes'] = false;
    $i++;
  }

  protected function Execute()
  {
    $lang = $this->pageBuilder->lang;
    $app_id = $this->pageBuilder->siteConfig->getVar('facebook','app_id');
    $app_secret = $this->pageBuilder->siteConfig->getVar('facebook','app_secret');
    $site_uri = $this->pageBuilder->siteConfig->getVar('facebook','site_uri');
    $redirect_uri = $this->pageBuilder->siteConfig->getVar('facebook','redirect_uri');
    
    session_save_path($this->pageBuilder->siteConfig->getVar("dirs", "session"));
    session_start();

    $code = $this->fields['code'];

    if(empty($code)) 
    {
      $_SESSION['state'] = md5(uniqid(rand(), true)); // CSRF protection
      $dialog_url = "https://www.facebook.com/dialog/oauth?client_id=" 
	. $app_id . "&redirect_uri=" . urlencode($site_uri) . "&state="
	. $_SESSION['state'];

      $this->attrs['location'] = $dialog_url;
    }
    else
    {
      if($_SESSION['state'] && ($_SESSION['state'] === $this->fields['state'])) 
      {
	$token_url = "https://graph.facebook.com/oauth/access_token?"
	  . "client_id=" . $app_id . "&redirect_uri=" . urlencode($site_uri)
	  . "&client_secret=" . $app_secret . "&code=" .$code;

	$response = file_get_contents($token_url);

	if($response===false)
	  throw new AV_Exception(FAILED, $lang,
				 array('addMsg'=>' (file_get_contents)'));
	  

	$params = null;
	parse_str($response, $params);

	$_SESSION['access_token'] = $params['access_token'];
       
	$graph_url = "https://graph.facebook.com/me?access_token=" 
	  . $params['access_token'];

	$user = json_decode(file_get_contents($graph_url));
      
	if($user->error)
	  throw new AV_Exception(FAILED, $lang,
				array('addMsg'=>' ('.$user->error->message.')'));

	$userFields['login'] = $user->id;
	$userFields['password'] = substr($_SESSION['state'],0,14);
	$userFields['firstName'] = $user->first_name;
	$userFields['lastName'] = $user->last_name;
	$userFields['status'] = 1; // ativo
	$userFields['groups'] = 
	  explode(',', $this->pageBuilder->siteConfig->getVar("facebook", 
							      "memberGroups"));
	foreach($userFields['groups'] as &$groupId)
	  $groupId = trim($groupId);

	$userMan = new UM_User($this);
	
	// loads fields definition for all dynamic tables associated
	$this->fieldsDef = $userMan->LoadFieldsDef();
	
	$userFields['path']='/';
	$userFields['photo'] = $picture->data->url;

	$userId = $userMan->FacebookLogin($userFields);

	if($userId != NULL)
	{
	  $graph_url = "https://graph.facebook.com/me/picture?access_token="
	    . $params['access_token'] . '&type=large&redirect=false';
	  $picture = json_decode(file_get_contents($graph_url));

	  if(!$picture->error)
	  {
	    $tmpImg['title'][0]['lang'] = 'pt_BR';
	    $tmpImg['title'][0]['value'] = 'Foto do Facebook';
	    $tmpImg['path'] = '/userImages/';
	    $tmpImg['content']['tmp_name'] = $picture->data->url;
	    $tmpImg['content']['name'] = 'picture.jpg';
	    $tmpImg['content']['type'] = 'image/jpeg';
	    $doc = new LIB_Document($this);
	    $doc->LoadFieldsDef(NULL,'/userImages/');
	    $docId = $doc->Insert($tmpImg);

	    $userFields2['userId'] = $userId;
	    $userFields2['photo'][0]['value'] = $docId;
	    $userMan->Set($userFields2);
	  }
	}

	$this->attrs['location'] = $redirect_uri;
       
      }
      else 
      {
	$label = new LANG_Label($this);
	$msg = $label->GetLabel('umFcbkCSRFMsg',$lang);
	throw new AV_Exception(FAILED, $lang,array('addMsg'=>" ($msg)"));
      }
    }

  }
}

?>