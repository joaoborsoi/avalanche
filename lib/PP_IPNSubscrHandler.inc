<?php

require_once('PP_IPNHandler.inc');

/**
 * Manipulador de exemplo de Notificação de Pagamento
 * Instantâneo
 */
class PP_IPNSubscrHandler extends PP_IPNHandler 
{
  /**
   * @param   boolean $isVerified
   * @param   array $message
   * @see     PP_IPNHandler::Handle()
   */
  public function Handle(array $message) 
  {

    /**
     * Verificamos se foi realmente o PayPal quem enviou a
     * notificação e se o email contido no campo receiver_email
     * é o nosso email; Essa é uma segunda verificação necessária.
     */
    $receiver_email = $this->pageBuilder->siteConfig->getVar('paypal',
							     'receiver_email');
    if (!$message['isVerified'])
      throw new AV_Exception(FAILED, $this->lang,
			     array('addMsg'=>' (Invalid IPN)'));
    else if ($message[ 'receiver_email' ]!=$receiver_email)
      throw new AV_Exception(FAILED, $this->lang,
			     array('addMsg'=>' (Invalid receiver_email)'));
    
    // está tudo ok, seguimos com nossas regras de negócio
    // podemos enviar um email para alguém, emitir nota fiscal
    // eletrônica se for o caso ou qualquer outra decisão
    // relacionada com as regras de negócio.
    switch($message['txn_type'])
    {
    case 'subscr_signup':
      $this->SubscrSignup($message);
      break;
      
    case 'subscr_payment':
      $this->SubscrPayment($message);
      break;
      
    case 'subscr_failed':
      $this->SubscrFailed($message);
      break;
      
    case 'subscr_cancel':
      $this->SubscrCancel($message);
      break;

    case 'subscr_eot':
      $this->SubscrEOT($message);
      break;
    }
  }

  protected function CheckSubscriptionOption(array $message)
  {
    // search for subscription option
    $SQL = 'SELECT docId FROM PP_SubscriptionOption ';
    $SQL .= "WHERE mc_currency='".$message['mc_currency']."' ";

    if($message['period1'] != NULL)
    {
      $SQL .= "AND period1='".$message['period1']."' ";
      if($message['mc_amount1']==='')
	$message['mc_amount1'] = 0;
      $SQL .= "AND mc_amount1='".$message['mc_amount1']."' ";
    }
    else
      $SQL .= "AND period1='' ";
    
    if($message['period2'] != NULL)
    {
      $SQL .= "AND period2='".$message['period2']."' ";
      if($message['mc_amount2']==='')
	$message['mc_amount2'] = 0;
      $SQL .= "AND mc_amount2='".$message['mc_amount2']."' ";
    }
    else
      $SQL .= "AND period2='' ";

    if($message['period3'] != NULL)
    {
      $SQL .= "AND period3='".$message['period3']."' ";
      if($message['mc_amount3']==='')
	$message['mc_amount3'] = 0;
      $SQL .= "AND mc_amount3='".$message['mc_amount3']."' ";
    }
    else
      $SQL .= "AND period3='' ";

    $subscr_option = $this->dbDriver->GetOne($SQL);
    if($subscr_option == NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang,
			     array('addMsg'=>' (Subscription option not found)'));

    return $subscr_option;
  }

  protected function SubscrSignup(array $message)
  {
    // check for duplicated IPN subscription
    $SQL = 'SELECT subscr_id FROM PP_Subscription ';
    $SQL .= "WHERE subscr_id='".$message['subscr_id']."' ";
    $SQL .= 'FOR UPDATE';
    $subscr_id = $this->dbDriver->GetOne($SQL);
    if($subscr_id != NULL)
    {
      $this->ipnListener->PrintLog("Duplicated IPN subscr_incr");
      return;
    }
    
    // find subscription option
    $subscr_option = $this->CheckSubscriptionOption($message);

    // create subscription
    $SQL = 'INSERT INTO PP_Subscription ';
    $SQL .= '(subscr_id,subscr_date,subscr_option,userId) VALUES ';
    $SQL .= "('".$message['subscr_id']."','";
    $SQL .= date('Y-m-d H:i:s', strtotime($message['subscr_date']))."',";
    $SQL .= "'$subscr_option','".$message['custom']."');";
    $this->dbDriver->ExecSQL($SQL);
  }

  protected function SubscrPayment(array $message)
  {
    // check for duplicated IPN payment
    $SQL = 'SELECT * FROM PP_SubscriptionPayment ';
    $SQL .= "WHERE txn_id='".$message['txn_id']."' ";
    $SQL .= 'FOR UPDATE';
    $payment = $this->dbDriver->GetRow($SQL);
    if(count($payment)==0)
    {
      // register payment
      $SQL = 'INSERT INTO PP_SubscriptionPayment ';
      $SQL .= '(txn_id,subscr_id,payment_status,payment_date,mc_gross) VALUES ';
      $SQL .= "('".$message['txn_id']."','".$message['subscr_id'];
      $SQL .= "','".$message['payment_status']."','";
      $SQL .= date('Y-m-d H:i:s', strtotime($message['payment_date']))."',";
      $SQL .= $message['mc_gross'].");";
      $this->dbDriver->ExecSQL($SQL);
    }
    else
    {
      if($payment_status == $message['payment_status'])
      {
	$this->ipnListener->PrintLog("Duplicated IPN subscr_payment");
	return;
      }

      $SQL = 'UPDATE PP_SubscriptionPayment ';
      $SQL .= "SET payment_status='".$message['payment_status']."' ";
      $SQL .= "WHERE txn_id='".$message['txn_id']."' ";
      $this->dbDriver->ExecSQL($SQL);
    }
  }

  protected function SubscrFailed(array $message)
  {
    // Not implemented
  }

  protected function SubscrCancel(array $message)
  {
    // search for subscription
    $SQL = 'SELECT subscr_id FROM PP_Subscription ';
    $SQL .= "WHERE subscr_id='".$message['subscr_id']."' ";
    $SQL .= 'FOR UPDATE';
    $subscr_id = $this->dbDriver->GetOne($SQL);
    if($subscr_id == NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang,
			     array('addMsg'=>' (Subscription not found)'));

    // update subscription
    $SQL = "UPDATE PP_Subscription SET subscr_cancel_date='";
    $SQL .= date('Y-m-d H:i:s', strtotime($message['subscr_date']))."' ";
    $SQL .= "WHERE subscr_id='".$message['subscr_id']."' ";
    $this->dbDriver->ExecSQL($SQL);
  }

  protected function SubscrEOT(array $message)
  {
    // search for subscription
    $SQL = 'SELECT subscr_id FROM PP_Subscription ';
    $SQL .= "WHERE subscr_id='".$message['subscr_id']."' ";
    $SQL .= 'FOR UPDATE';
    $subscr_id = $this->dbDriver->GetOne($SQL);
    if($subscr_id == NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang,
			     array('addMsg'=>' (Subscription not found)'));

    // update subscription
    $SQL = "UPDATE PP_Subscription SET subscr_eot='1' ";
    $SQL .= "WHERE subscr_id='".$message['subscr_id']."' ";
    $this->dbDriver->ExecSQL($SQL);
  }
}