<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_JSFacebookLogin extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $i = 0;

    $this->fieldsDef[$i]['fieldName'] = 'access_token';
    $this->fieldsDef[$i]['required'] = true;
    $this->fieldsDef[$i]['allowNull'] = false;
    $this->fieldsDef[$i]['stripSlashes'] = false;
    $i++;
  }

  protected function Execute()
  {
    $lang = $this->pageBuilder->lang;

    $graph_url = "https://graph.facebook.com/me?access_token=" 
      . $this->fields['access_token'];

    $user = json_decode(file_get_contents($graph_url));
      
    if($user->error)
      throw new AV_Exception(FAILED, $lang,
			     array('addMsg'=>' ('.$user->error->message.')'));

    $userFields['login'] = $user->id;
    $userFields['password'] = substr($_SESSION['state'],0,14);
    $userFields['firstName'] = $user->first_name;
    $userFields['lastName'] = $user->last_name;
    $userFields['status'] = 1; // ativo
    $userFields['groups'] = 
      explode(',', $this->pageBuilder->siteConfig->getVar("facebook", 
							  "memberGroups"));
    foreach($userFields['groups'] as &$groupId)
      $groupId = trim($groupId);

    $userMan = new UM_User($this);
	
    // loads fields definition for all dynamic tables associated
    $this->fieldsDef = $userMan->LoadFieldsDef();
	
    $userFields['path']='/';
    $userFields['photo'] = $picture->data->url;

    $userId = $userMan->FacebookLogin($userFields);

    if($userId != NULL)
    {
      $graph_url = "https://graph.facebook.com/me/picture?access_token="
	. $this->fields['access_token'] . '&type=large&redirect=false';
      $picture = json_decode(file_get_contents($graph_url));

      if(!$picture->error)
      {
	$tmpImg['title'][0]['lang'] = 'pt_BR';
	$tmpImg['title'][0]['value'] = 'Foto do Facebook';
	$tmpImg['path'] = '/userImages/';
	$tmpImg['content']['tmp_name'] = $picture->data->url;
	$tmpImg['content']['name'] = 'picture.jpg';
	$tmpImg['content']['type'] = 'image/jpeg';
	$doc = new LIB_Document($this);
	$doc->LoadFieldsDef(NULL,'/userImages/');
	$docId = $doc->Insert($tmpImg);

	$userFields2['userId'] = $userId;
	$userFields2['photo'][0]['value'] = $docId;
	$userMan->Set($userFields2);
      }
    }
  }
}

?>