<?php

require_once('ADMIN_Page.inc');


class ADMIN_Files extends ADMIN_Page
{
  protected $rootPath;
  protected $langFolder;
  protected $fixedOrder;

  function __construct(& $pageBuilder, $fixedOrder, $rootPath = '')
  {
    if(mb_substr($rootPath,-1) == '/')
      $rootPath = mb_substr($rootPath,0,-1);
    $this->rootPath = $rootPath;
    $this->fixedOrder = $fixedOrder;
    parent::__construct($pageBuilder);
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminFiles');
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');
    $pageLabels = & $this->LoadModule("LANG_GetPageLabels");
    $pageLabels->PrintHTML($this->pageBuilder->rootTemplate,NULL,
			   'htmlentities_utf');

    $htmlPath = urlencode(stripslashes($_GET['path']));

    if($_REQUEST['type']=='file')
    {
      $currNode = ($_REQUEST['docId']!=NULL)?$_REQUEST['docId']:'null';
      $currType = "'file'";
    }
    else if($_REQUEST['type']=='folder')
    {
      $currNode = ($_REQUEST['folderId']!=NULL)?$_REQUEST['folderId']:'null';
      $currType = "'folder'";
    }
    else
    {
      $currType = "'null'";
      $currNode = 'null';
    }

    $script = "parent.OnLoadFiles(\"$htmlPath\",$currNode,$currType);";
    $script .= $this->LoadFiles($_GET['path']);
    $this->pageBuilder->rootTemplate->addText((string)$script,'onLoad');
  }

  protected function LoadFiles($path)
  {
    $_GET['path'] = $this->rootPath . $path;

    // print folders
    if($_REQUEST['filter'] === NULL)
    {
      $_GET['fixedOrder'] = ($this->fixedOrder)?'1':'0';
      $getFolder = $this->LoadModule('LIB_GetFolderList', 'adminFolder', 
				     'adminFolder');

      $getFolder->PrintHTML($this->pageBuilder->rootTemplate,NULL,
			    'htmlentities_utf');
    }

    // print files
    $_GET['dateFormat']= '%d-%m-%Y';

    if($this->fixedOrder)
    {
      $_GET['orderBy'] = 'fixedOrder';
      $getOrderAsc = $this->LoadModule('LIB_GetFolderOrderAsc');
      $_GET['order'] = $getOrderAsc->attrs['orderAsc'];
    }
    else
      $_GET['orderBy'] = 'title';

    $search = $this->LoadModule('LIB_Search', 'adminFile','adminFile');

    if(count($getFolder->children)+count($search->children)==0)
      $this->pageBuilder->rootTemplate->addText('<p class="vazio">Vazio.</p>',
						'errorMessage');
    else
      $search->PrintHTML($this->pageBuilder->rootTemplate);

    $this->pageBuilder->rootTemplate->addText((string)$_GET['path'],'path');
  }

}
?>