<?php

require_once('AV_Functions.inc');
require_once('LANG_Label.inc');

class AV_FieldParser
{
  public $fields;
  protected $module;
  protected $dbDriver;
  protected $inputSources;
  protected $lang;

  function __construct(&$module, &$inputSources)
  {
    $this->module = & $module;
    $this->lang = $this->module->pageBuilder->lang;
    $this->inputSources = & $inputSources;
    $this->dbDriver = & $module->dbDriver;
  }

  function GetVariable($varName, $trimField = true, $stripSlashes = true)
  {
    $varName = $this->module->varPrefix . $varName;
    reset($this->inputSources);
    $varValue = NULL;
    while($varValue === NULL && list($key, $input) = each($this->inputSources))
      $varValue = $input[$varName];
    if($varValue === NULL)
      return NULL;
    if($trimField)
      $varValue = trim($varValue);
    if(!$stripSlashes)
      return addslashes($varValue);
    return $varValue;
  }

  function Parse(& $fieldsDef)
  {
    foreach($fieldsDef as $field)
    {
      if($field['list'] === NULL)
      	$field['list'] = false;
      
      if($field['list'])
	$this->fields[$field['fieldName']] = $this->ConsistListField($field);
      else
	$this->fields[$field['fieldName']] = $this->ParseField($field);
    }
  }

  function ParseField(& $field)
  {
    // searches for variable in the input sources
    if($field['trimField'] === NULL)
      $field['trimField'] = true;
    
    if($field['stripSlashes'] === NULL)
      $field['stripSlashes'] = true;

    $varValue = $this->GetVariable($field['fieldName'], 
				   $field['trimField'],
				   $field['stripSlashes']);
    // consist NULL value
    if($varValue === '' || $varValue === NULL || 
       is_array($varValue) && count($varValue)==0)
    {
      if($field['default'] !== NULL)
	$varValue = $field['default'];
      else if($field['required']=='1')
	throw new AV_Exception(FIELD_REQUIRED, $this->lang,
			       array('fieldName'=>$field['fieldName'],
				     'addMsg'=>$this->GetFieldLabel($field),
				     'field'=>$field));
    }

    if(($varValue === ''|| is_array($varValue) && count($varValue)==0) 
       && !$field['allowNull'])
      throw new AV_Exception(FIELD_REQUIRED, $this->lang,
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($field)));

    if($varValue !== '' && $varValue !== NULL && $field['consistType'] !== '')
      $this->ConsistType($field, $varValue);
  
    return $varValue;
  }


  function ConsistListField(& $field)
  {
    $size = $this->GetVariable($field['sizeVarName'] , true);    
    if($size != NULL && !str_is_int($size))
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>$field['sizeVarName'],
				   'addMsg'=>' ('.$field['sizeVarName'].')'));

    if($field['required']=='1' && $size == NULL)
      throw new AV_Exception(FIELD_REQUIRED, $this->lang,
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>' ('.$field['fieldName'].')'));
    $i = 1;
    try
    {
      for(; $i <= $size; $i++)
      {
	foreach($field['fieldsDef'] as $subfield)
	{
	  $subFieldName = $subfield['fieldName'];
	  $subfield['fieldName'] = $field['fieldName']."_${i}__$subFieldName";
	  $subfield['labelId'] = $field['labelId'];
	  $varValue[$i - 1][$subFieldName] = $this->ParseField($subfield);
	}
      }
      return $varValue;
    }
    catch(AV_Exception $e)
    {
      if($field['type'] == 'langLabel' || 
	 $field['type'] == 'langTextArea' ||
	 $field['type'] == 'langHtmlArea')
      {
	$lang = $varValue[$i - 1]['lang'];
	if($lang != NULL)
	  $e->setMessage($e->getMessage() . ' ['.mb_substr($lang,0,2).']');
      }
      throw $e;
    }
  }

  function ConsistType(& $field, & $value)
  {
    switch($field['consistType'])
    {
    case 'integer':
      if(!str_is_int($value))
	throw new AV_Exception(INVALID_FIELD, $this->lang,
			       array('fieldName'=>$field['fieldName'],
				     'addMsg'=>$this->GetFieldLabel($field),
				     'value'=>$value));
      break;

    case 'percent':
      $this->ParseFloat($value);
      $value = $value/100;
      break;

    case 'float':
      $this->ParseFloat($value);
      break;

    case 'date':
      $this->ParseDate($field, $value);
      break;

    case 'datetime':
      list($date, $time) = explode(' ', $value, 2);
      $this->ParseDate($field, $date);

      $this->ParseTime($field, $time);
      
      $value = "$date $time";
      break;

    case 'time':
      $this->ParseTime($field, $value);

    case 'e-mail':
      if(filter_var($value, FILTER_VALIDATE_EMAIL) === false)
	throw new AV_Exception(INVALID_FIELD, $this->lang,
			       array('fieldName'=>$field['fieldName'],
				     'addMsg'=>$this->GetFieldLabel($field)));
      break;

    case 'boolean':
      $value = mb_strtolower($value);
      if($value == 'true' || $value == true)
	$value = '1';
      else if($value == 'false' || $value == false)
	$value = '0';
      if($value != '0' && $value != '1')
	throw new AV_Exception(INVALID_FIELD, $this->lang,
			       array('fieldName'=>$field['fieldName'],
				     'addMsg'=>$this->GetFieldLabel($field)));
      break;

    case 'accessRight':
      if($value != 'r' &&
	 $value != 'w' &&
	 $value != 'rw' &&
	 $value != '' &&
	 $value != NULL)
	throw new AV_Exception(INVALID_FIELD, $this->lang,
			       array('fieldName'=>$field['fieldName'],
				     'addMsg'=>$this->GetFieldLabel($field)));
      break;

    case 'file':
      $this->ParseFile($field, $value);
      break;
      
    case 'folder':
      $this->ParseFolder($field, $value);
      break;

    case 'alfanum':
      $this->ParseAlfaNum($field, $value);
      break;

    case 'html':
      $this->ParseHTML($field, $value);
      break;

    case 'objectList':
      $this->ParseObjectList($field,$value);
      break;

    case 'emailList':
      $this->ParseEmailList($field,$value);
      break;

    case 'cnpj':
      $this->ConsistCnpj($field,$value);
      break;

    case 'cpf':
      $this->ConsistCpf($field,$value);
      break;
    }
  }

  function ParseDate(& $field, & $value)
  {
    switch($field['dateFormat'])
    {
    case 'YYYY-MM':
    case 'YY-MM':
      list($year, $month) = explode('-', $value, 2);
      $day = 1;
      break;

    case 'MM-DD':
      list($month, $day) = explode('-', $value, 2);
      $today = getdate(); 
      $year = $today['year']; 
      break;

    case 'YYYY-MM-DD':
    case 'YY-MM-DD':
    default:
      list($year, $month, $day) = explode('-', $value, 3);
      break;
    }

    if(!str_is_int($month) ||
       !str_is_int($day) ||
       !str_is_int($year) ||
       !checkdate($month, $day, $year))
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($field)));

    $value = "$year-$month-$day";
  }

  function ParseTime(& $field,& $value)
  {
    switch($field['timeFormat'])
    {
    case 'HH:MM':
      list($hour, $minute) = explode(':', $value, 2);
      $second = 0;
      break;

    default:
    case 'HH:MM:SS':
      list($hour, $minute, $second) = explode(':', $value, 3);
      break;
    }

    if(!str_is_int($hour) || 
       !str_is_int($minute) || 
       !str_is_int($second) ||
       $hour < 0 || $hour > 23 ||
       $minute < 0 || $minute > 59 ||
       $second < 0 || $second > 59)
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($field)));

    $value = "$hour:$minute:$second";
  }

  function ParseFloat(& $value)
  {
    $locale = localeconv();
    $value = str_replace($locale['mon_thousands_sep'], '', $value);
    if($locale['mon_decimal_point'] != '.')
      $value = str_replace($locale['mon_decimal_point'], '.', $value);
    $value = floatval($value);
  }


  protected function GetFieldLabel($fieldDef)
  {
    if($fieldDef['labelId'] != NULL)
    {
      $langLabel = new LANG_Label($this->module);
      return ' ('.$langLabel->GetLabel($fieldDef['labelId'],$this->lang).')';
    }
    else
      return ' ('.$fieldDef['fieldName'].')';
  }

  function ParseFile(&$fieldDef, &$value)
  {
    switch($value['error'])
    {
    case UPLOAD_ERR_OK:
      // checks source file 
      if(is_uploaded_file($value['tmp_name']))
	return;
      $status = FAILED;
      $errorTag = 'adminUploadErrorTag';
      break;

    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      $status = FAILED;
      $errorTag = 'adminInvalidSizeTag';
      break;

    case UPLOAD_ERR_NO_FILE:
      if($fieldDef['required'] == '0')
	return;
      $status = FIELD_REQUIRED;
      break;

    case UPLOAD_ERR_PARTIAL:
      $status = FAILED;
      $errorTag = 'adminUploadErrorTag';
      break;
    }

    $langLabel = new LANG_Label($this->module);
    if($errorTag != NULL)
      $data['message'] = ' '.$langLabel->GetLabel($errorTag,$this->lang);
    $data['fieldName'] = $fieldDef['fieldName'];
    $data['addMsg'] = $this->GetFieldLabel($fieldDef);
    throw new AV_Exception($status, $this->lang,$data);
  }

  function ParseFolder(&$field,&$value)
  {
    // checks out if the folder name contains '/' which is not allowed
    if(strstr($value, '/') !== false)
      throw new AV_Exception(INVALID_FIELD, $this->lang, 
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($field)));
  }

  function ParseAlfaNum(&$fieldDef,&$value)
  {
    if(!ctype_alnum($value))
      throw new AV_Exception(INVALID_FIELD, $this->lang, 
			     array('fieldName'=>$fieldDef['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($fieldDef)));
    
  }

  function ParseHTML(&$fieldDef,&$value)
  {
    $value = html_entity_decode(str_replace("&#39;",'\\\'',
					    str_replace("&quot;",'\"',
							$value)),
				ENT_NOQUOTES,"UTF-8");
  }

  function ParseObjectList(& $field, & $varValue)
  {
    foreach($varValue as & $value)
    {
      foreach($field['fieldsDef'] as $subfield)
      {
	if($subfield['trimField'] === NULL)
	  $subfield['trimField'] = true;
	
	if($subfield['stripSlashes'] === NULL)
	  $subfield['stripSlashes'] = true;

	if($subfield['trimField'])
	  $value[$subfield['fieldName']] = trim($value[$subfield['fieldName']]);
	if(!$subfield['stripSlashes'])
	  $value[$subfield['fieldName']] = addslashes($value[$subfield['fieldName']]);

	// consist NULL value
	if($value[$subfield['fieldName']] === '' || 
	   $value[$subfield['fieldName']] === NULL || 
	   is_array($value[$subfield['fieldName']]) && 
	   count($value[$subfield['fieldName']])==0)
	{
	  if($subfield['default'] !== NULL)
	    $value[$subfield['fieldName']] = $subfield['default'];
	  else if($subfield['required']=='1')
	    throw new AV_Exception(FIELD_REQUIRED, $this->lang,
				   array('fieldName'=>$subfield['fieldName'],
					 'addMsg'=>$this->GetFieldLabel($subfield)));
	}

	if(($value[$subfield['fieldName']] === ''|| 
	    is_array($value[$subfield['fieldName']]) && 
	    count($value[$subfield['fieldName']])==0) 
	   && !$subfield['allowNull'])
	  throw new AV_Exception(FIELD_REQUIRED, $this->lang,
				 array('fieldName'=>$subfield['fieldName'],
				       'addMsg'=>$this->GetFieldLabel($subfield)));

	if($value[$subfield['fieldName']] !== '' && 
	   $value[$subfield['fieldName']] !== NULL && 
	   $subfield['consistType'] !== '')
	  $this->ConsistType($subfield, $value[$subfield['fieldName']]);
      }
    }
    
  }

  function ParseEmailList(& $field, & $varValue)
  {
    foreach($varValue as & $value)
    {
      if(filter_var($value, FILTER_VALIDATE_EMAIL) === false)
	throw new AV_Exception(INVALID_FIELD, $this->lang,
			       array('fieldName'=>$field['fieldName'],
				     'addMsg'=>$this->GetFieldLabel($field)));
    }
  }

  //-- Method: ConsistCpf
  //-- Desc: Check if CPF's number is valid
  function ConsistCpf(& $field, & $cpf)
  {
    //verifica se oq foi informado eh numero
    if(!is_numeric($cpf))
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($field),
				   'value'=>$cpf));

    if( ($cpf == '11111111111') || ($cpf == '22222222222') ||
	($cpf == '33333333333') || ($cpf == '44444444444') ||
	($cpf == '55555555555') || ($cpf == '66666666666') ||
	($cpf == '77777777777') || ($cpf == '88888888888') ||
	($cpf == '99999999999') || ($cpf == '00000000000') ) 
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($field),
				   'value'=>$cpf));

    $dv_inform = mb_substr($cpf,9,2);
    for ($i=0 ; $i<=8 ; $i++)
    {
      $digit[$i] = mb_substr($cpf,$i,1);
    }
    //calcula o valor do 10 digito de verificacao
    $position = 10;
    $add = 0;
    for ($i=0 ; $i<=8 ; $i++)
    {
      $add = $add + $digit[$i] * $position;
      $position = $position - 1;
    }
    $digit[9] = $add % 11;

    if ($digit[9] < 2)
      $digit[9] = 0;
    else
      $digit[9] = 11 - $digit[9];
    
    //calcula o valor do 11 digito de verificacao
    $position = 11;
    $add = 0;
    for ($i=0 ; $i<=9 ; $i++)
    {
      $add = $add + $digit[$i] * $position;
      $position = $position - 1;
    }
    $digit[10] = $add % 11;
    if ($digit[10] < 2)
      $digit[10] = 0;
    else
      $digit[10] = 11 - $digit[10];

    //verifica se o dv calculado eh igual ao informado
    $dv = $digit[9] * 10 + $digit[10];
    if ($dv != $dv_inform)
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($field),
				   'value'=>$cpf));

  }

      
  //-- Method: ConsistCnpj
  //-- Desc: Check if CNPJ's number is valid
  function ConsistCnpj(& $field, & $cnpj)
  {
    
    // Get only numeric digits
    if (mb_strlen($cnpj) != 14 OR !is_numeric($cnpj) OR 
	$cnpj == "00000000000000" OR $cnpj == "11111111111111" OR
	$cnpj == "22222222222222" OR $cnpj == "33333333333333" OR 
	$cnpj == "44444444444444" OR $cnpj == "55555555555555" OR 
	$cnpj == "66666666666666" OR $cnpj == "77777777777777" OR
	$cnpj == "88888888888888" OR $cnpj == "99999999999999")
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($field),
				   'value'=>$cnpj));
      
    // Inicialize variables.
    // cnpjCalc variable has the first 12 caracters of the CNPJ
    // cnpjDigit variable as the las two caracters of the CNPJ. The digit.
    $cnpjCalc = "";
    $cnpjDigit = "";

    for ( $i = 0 ; $i < 12 ; $i++ )
      $cnpjCalc .= $cnpj[$i];

    for ( $i = 12 ; $i < 14 ; $i++ )
      $cnpjDigit .= $cnpj[$i];
     
    // First part of digit verification
    $cnpjAdd = 0;
    for( $i = 0 ; $i < 4 ; $i++ )
      $cnpjAdd += $cnpjCalc[ $i ] * (5 - $i);
 
    for( $i = 0 ; $i < 8 ; $i++ )
      $cnpjAdd += $cnpjCalc[ $i + 4] * (9 - $i);
     
    // Fisrt digit
    $cnpjDigit = 11 - ($cnpjAdd % 11);
      
    if ( $cnpjDigit == 10 OR $cnpjDigit == 11 )
      $cnpjCalc .= '0';
    else
      $cnpjCalc .= $cnpjDigit;
    
    // Second part of digit verification
    $cnpjAdd = 0;
    for ( $i = 0 ; $i < 5 ; $i++ )
      $cnpjAdd += $cnpjCalc[ $i ] * (6 - $i);
    for ( $i = 0 ; $i < 8 ; $i++ )
      $cnpjAdd += $cnpjCalc[ $i + 5 ] * (9 - $i);
     
    // Second digit
    $cnpjDigit = 11 - ($cnpjAdd % 11);
    if ( $cnpjDigit == 10 OR $cnpjDigit == 11 )
      $cnpjCalc .= '0';
    else
      $cnpjCalc .= $cnpjDigit;
     
    if($cnpj != $cnpjCalc) 
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>$field['fieldName'],
				   'addMsg'=>$this->GetFieldLabel($field),
				   'value'=>$cnpj));

  }
}

?>
