<?php

class SHOP_Cart
{
  protected $module;
  protected $pageBuilder;
  protected $dbDriver;
  protected $lang;
  
  function __construct($module)
  {
    $this->module = & $module;
    $this->dbDriver = & $module->dbDriver;
    $this->pageBuilder = & $this->module->pageBuilder;
    $this->lang = $this->pageBuilder->lang;

    // inicia sessões do php
    global $adminDir;
    session_save_path($adminDir.'/session');
    session_start();
  }

  function AddToCart($docId)
  {
    // check if it's already in the cart
    $SQL = 'SELECT num FROM SHOP_Cart ';
    $SQL .= "WHERE phpSessionId='".session_id()."' ";
    $SQL .= "AND docId='$docId' FOR UPDATE ";
    $num = $this->dbDriver->GetOne($SQL);

    // if already in the cart, append one more
    if($num!=NULL)
    {
      $this->SetQuantity($docId, ++$num);
      return;
    }

    // if not in cart, create new
    $SQL = 'INSERT INTO SHOP_Cart ';
    $SQL .= '(phpSessionId, docId, num) VALUES ';
    $SQL .= "('".session_id()."','$docId',1)";
    $this->dbDriver->ExecSQL($SQL);
  }

  function SetQuantity($docId, $num)
  {
    if($num<=0)
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>'num'));

    $SQL = "UPDATE SHOP_Cart SET num='$num' ";
    $SQL .= "WHERE phpSessionId='".session_id()."' AND docId='$docId' ";
    $this->dbDriver->ExecSQL($SQL);
  }

  function RemoveFromCart($docId)
  {
    $SQL = 'DELETE FROM SHOP_Cart ';
    $SQL .= "WHERE phpSessionId='".session_id()."' AND docId='$docId' ";
    $this->dbDriver->ExecSQL($SQL);
  }

  function FlushCart()
  {
    $SQL = 'DELETE FROM SHOP_Cart ';
    $SQL .= "WHERE phpSessionId='".session_id()."' ";
    $this->dbDriver->ExecSQL($SQL);
  }

  function GetCart()
  {
    $SQL = 'SELECT c.docId, value as title, num, ';
    $SQL .= '(SELECT imageId FROM LIB_ContentFile ci ';
    $SQL .= 'WHERE ci.nodeId=c.docId ORDER BY idx LIMIT 1) as imageId, ';
    $SQL .= 'price, weight, value ';
    $SQL .= 'FROM SHOP_Cart c, SHOP_Item i, LANG_Label l ';
    $SQL .= "WHERE phpSessionId='".session_id()."' AND c.docId=i.docId ";
    $SQL .= "AND i.title=labelId AND lang='".$this->lang."' ";
    $SQL .= 'ORDER BY value ';
    return $this->dbDriver->GetAll($SQL);
  }
}

?>