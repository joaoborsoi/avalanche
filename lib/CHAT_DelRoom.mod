<?php

require_once("AV_DBOperationObj.inc");

require_once("CHAT_Manager.inc");

class CHAT_DelRoom extends AV_DBOperationObj
{
   
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'roomId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
  }
  
  function Execute()
  {
    $maganer = new  CHAT_Manager($this);
    $maganer->LoadMessages();
    return $maganer->DelRoom($this->fieldParser->fields['roomId']);
  }
}

?>