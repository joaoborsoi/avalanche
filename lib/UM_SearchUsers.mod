<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_SearchUsers extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'groupId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;

    $this->fieldsDef[1]['fieldName'] = 'filter';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['stripSlashes'] = false;
  }

  protected function Execute()
  {
    $userMan = new UM_User($this);

    $this->isArray = true;
    // get rows
    $rows = $userMan->SearchUsers($this->fields['groupId'],
				  $this->fields['filter'],$status);

    $this->LoadChildrenFromRows($rows, 'user');
  }
}

?>

