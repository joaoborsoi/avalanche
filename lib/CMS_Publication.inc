<?php

require_once('CMS_Content.inc');

class CMS_Publication extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    // loads document title
    $this->pageBuilder->rootTemplate->addText($fields['title'],'title');
    $scripturi = 'http:'.$this->page->base.'content/'.$fields['docId'];
    $this->pageBuilder->rootTemplate->addText($scripturi,'url');
    if($fields['imageList']!=NULL)
    {
      $imageList = explode(',',$fields['imageList']);
      $img = 'http:'.$this->page->base .'thumb/'.$imageList[0];
      $this->pageBuilder->rootTemplate->addText($img,'image');
    }
    $this->pageBuilder->rootTemplate->addText($fields['descr'],'descr');

    // add to history
    if($this->pageBuilder->siteConfig->getVar('cms','history'))
    {
      if($_REQUEST['docId'] != NULL)
	$_GET['link'] = 'content/'.$_REQUEST['docId'];
      else
	$_GET['link'] = 'area/'.$this->menuPath;
      $_GET['title'] = addslashes($fields['title']);
      $this->page->LoadModule('AV_AddHistory');
    }

    // publicationDate
    $dateFormat = $this->pageBuilder->siteConfig->getVar('cms',
							 'pubDateFormat');
    $publicationDate = strftime($dateFormat,(int)$fields['publicationDate']);
    $content = "<h5 class='date'><small>$publicationDate</small></h5>\n";

    // header
    $content .= '<h1>'.$fields['title']."</h1>\n";

    // load article's images
    $content .= $this->page->LoadImages($fields['imageList']);

    // content
    $content .= "<div id='fileContent'>\n";
    $content .= $fields['content'];
    $content .= "</div>\n";

     // loads main content to the page
    $this->pageBuilder->rootTemplate->addText((string)$content,'mainContent');
  }

  function LoadArea(&$fields,$path)
  {
    parent::LoadArea($fields,$path);

    // search for files at the folder
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
							 'pubDateFormat');
    $_GET['path'] = $path;
    $_GET['orderBy'] = 'publicationDate';
    $_GET['order'] = '0';
    $_GET['returnFields'] = 'title,publicationDate,descr,imageList';
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsPerPage');
    $_GET['exclusionFilters_numOfOptions'] = 1;
    $_GET['exclusionFilters_1__fieldName'] = 'publicationDate';
    $_GET['exclusionFilters_1__value'] = date('Y-m-d H:i:s');
    $_GET['exclusionFilters_1__operator'] = '<=';


    $search = $this->page->LoadModule('LIB_Search');

    // encoded menu path for the links
    $encMenuPath = str_replace("%2F", "/", 
			       urlencode(stripslashes($this->menuPath)));

    if($search->attrs['totalResults']>0)
    {
      $content .= $this->LoadList($search);
      $content .= $this->page->LoadSearchNav($search,$_REQUEST['offset'],
					     $_GET['limit'],
					     'area/'.$encMenuPath);
    }

    $this->pageBuilder->rootTemplate->addText((string)$content,'mainContent');
  }

  function LoadList($search)
  {
    $content .= "<ul class='pub-list'>\n";
    foreach($search->children as & $child)
    {
      $content .= "<li>\n";
      $content .= $this->LoadListItem($child->attrs);
      $content .= "</li>\n";
    }
    $content .= "</ul>\n";
    return $content;
  }

  function LoadListItem(& $fields)
  {
    $content = "<h5>".$fields['publicationDate']."</h5>\n";
    $content .= "<h4><a href='content/".$fields['docId']."'>";
    if($fields['imageList']!=NULL)
    {
      $imageList = explode(',',$fields['imageList']);
      $content .= "<img src='thumb/".$imageList[0]."' alt='";
      $content .= $fields['title']."' />\n";
    }
    $content .= $fields['title']."</a></h4>\n";
    if($fields['descr']!=NULL)
      $content .= "<p>".$fields['descr']."</p>\n";
    return $content;
  }

  function LoadFeedItem(& $fields, $base)
  {
    $content = '<title>'.$fields['title']."</title>\n";
    $content .= '<link>'.$base.'content/'.$fields['docId']."</link>\n";
    $content .= '<guid>'.$base.'content/'.$fields['docId']."</guid>\n";
    $content .= "<description>\n";
    $content .= "<![CDATA[ \n";
    $content .= $fields['content'];
    $content .= "\n]]>\n";
    $content .= "</description>\n";
    $content .= '<pubDate>'.$fields['publicationDate']."</pubDate>\n";  
    return $content;
  }

}

?>
