<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


class LIB_InsFolder extends AV_DBOperationObj
{
  private $folder;

  function __construct(&$pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
		       $jsonInput=false, $transaction = true)
  {
    $this->folder = new LIB_Folder($this);
    parent::__construct($pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
			$jsonInput,$transaction);
  }

  protected function LoadFieldsDef()
  {
    $this->fieldsDef = $this->folder->LoadFieldsDef();
  }

  protected function Execute()
  {
    $this->attrs['folderId'] = $this->folder->Insert($this->fields);
  }
}

?>
