<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

abstract class UM_UserOper extends AV_DBOperationObj
{
  protected $userMan;
  protected $disableRequired = false;

  protected function ParseInput(&$inputSources)
  {
    parent::ParseInput($inputSources);

    $this->userMan = new UM_User($this);

    // loads fields definition for all dynamic tables associated
    $this->fieldsDef = $this->userMan->LoadFieldsDef($this->disableRequired,
						     $this->disableRequired);


    if(is_array($this->fieldParser))
    {
      foreach($this->fieldParser as $key => $parser)
      {
	$this->fieldParser[$key]->Parse($this->fieldsDef);
	$this->fields = array_merge($this->fields, 
				    $this->fieldParser[$key]->fields);
      }
    }
    else
    {
      $this->fieldParser->Parse($this->fieldsDef);
      $this->fields = array_merge($this->fields, $this->fieldParser->fields);
    }

    try
    {
      $this->fieldsDef = $this->userMan->LoadUserTypeFieldsDef($this->fields['typeId']);

      if(is_array($this->fieldParser))
      {
	foreach($this->fieldParser as $key => $parser)
	{
	  $this->fieldParser[$key]->Parse($this->fieldsDef);
	  $this->fields = array_merge($this->fields, 
				      $this->fieldParser[$key]->fields);
	}
      }
      else
      {
	$this->fieldParser->Parse($this->fieldsDef);
	$this->fields = array_merge($this->fields, $this->fieldParser->fields);
      }
    }
    catch(AV_Exception $e)
    {
      if($e->getCode()!=NOT_FOUND)
	throw $e;
    }
  }

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'path';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['default'] = '/';
    $this->fieldsDef[0]['stripSlashes'] = false;
  }

}

?>