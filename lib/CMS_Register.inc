<?php

require_once('CMS_Content.inc');

class CMS_Register extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    // dummy
  }

  function LoadArea(&$fields, $path)
  {
    $registerForm = &$this->pageBuilder->loadTemplate('cmsRegisterForm');
    $this->pageBuilder->rootTemplate->addTemplate($registerForm,'mainContent');

     // loads document title
    $title = htmlentities_utf($fields['title']);
    $this->pageBuilder->rootTemplate->addText((string)$title,'title');

    // folder content title
    $content = "<h1>$title</h1>\n";

    // folder content
    $content .= $fields['content'];

    $registerForm->addText($content,'content');

    $this->pageLabels->PrintHTML($registerForm);  

    $_GET['userId'] = $this->memberData['userId'];

    if($fields['typeId'] != NULL)
      $_GET['typeId'] = $fields['typeId'];
    $getUser = $this->page->LoadModule('ADMIN_GetUserForm');

    if($_GET['userId']!=NULL)
      $registerForm->addText($_GET['userId'],'userId');
    else
    {
      // new users should have password required and captcha
      $getUser->children[0]->children[170]->attrs['required'] = "required";

      $captcha  = $this->pageBuilder->loadTemplate('captchaField');
      $this->pageLabels->PrintHTML($captcha);
      $registerForm->addTemplate($captcha,'captcha');
    }


    // remove some fields
    unset($getUser ->children[0]->children[1000]); // status
    unset($getUser ->children[0]->children[1010]); // groups
    
    // prints article fields to the template
    $getUser->PrintHTML($registerForm);

    $tags['changeTag'] = $this->pageLabels->attrs['changeTag'];

    $script = "<script type='text/javascript'>\n";
    $script .= "\$(document).ready(function() {\n";
    $script .= "cms.OnRegisterFormLoad(".json_encode($tags).");\n";
    $script .= "});\n";
    $script .= '</script>';
    $this->pageBuilder->rootTemplate->addText($script,'header');
   }

  function LoadListItem(& $fields)
  {
    // dummy
  }


}
?>