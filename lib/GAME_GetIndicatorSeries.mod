<?php

require_once('AV_DBOperationObj.inc');
require_once('GAME_Indicators.inc');

class GAME_GetIndicatorSeries extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'fieldName';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'scale';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['consitType'] = 'integer';
    $this->fieldsDef[1]['default'] = '0';
  }

  protected function Execute()
  {
    $this->isArray = true;
    $indicators = new GAME_Indicators($this);
    $fieldInfo = array();
    $series = $indicators->GetIndicatorSeries($this->fields['fieldName'],
					      $this->fields['scale'],
					      $fieldInfo);
    $this->LoadChildrenFromRows($series,'dataItem');
    $this->attrs['label'] = $fieldInfo['descr_'.$this->pageBuilder->lang];
  }
}

?>