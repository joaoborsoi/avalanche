<?php

require_once('L2PDF_Document.inc');
require_once('LIB_Document.inc');

class LEGIS_PDFMail
{
  protected $module;
  protected $dbDriver;

  function __construct(& $module)
  {
    $this->module = &$module;
    $this->dbDriver = & $module->dbDriver;
  }

  function Get($params)
  {
    // cria documento para prestação de contas
    $mail =  new L2PDF_Document($this->module->pageBuilder); 

    $this->Init($mail);

    $mail->Write('\large');

    $mail->Write('\textbf{De:} ',false);
    $mail->Write($mail->Escape($params['fromAddress']));
    $mail->Write('\\\\');
    $mail->Write('');


    $mail->Write('\textbf{Para:} ',false);
    $mail->Write($mail->Escape($params['toAddress']));
    $mail->Write('\\\\');
    $mail->Write('');

    if($params['ccAddress']!='') {
      $mail->Write('\textbf{Cc:} ',false);
      $mail->Write($mail->Escape($params['ccAddress']));
      $mail->Write('\\\\');
      $mail->Write('');
    }

    $date = strftime('%d/%m/%Y - %H:%M',strtotime($params['msgDate']));
    $mail->Write('\textbf{Data:} ',false);
    $mail->Write($mail->Escape($date));
    $mail->Write('\\\\');
    $mail->Write('');

    $mail->Write('\textbf{Assunto:} ',false);
    $mail->Write($mail->Escape($params['subject']));
    $mail->Write('\\\\');
    $mail->Write('');

    $message = str_replace("\r\n","\\\\", 
			   str_replace("\n","\\\\",$params['message']));

    $mail->Write('\textbf{Mensagem:} ');
    $mail->Write('');
    $mail->Write($mail->Escape($message));


    $this->End($mail);

    $tmpDir = $this->module->pageBuilder->siteConfig->getVar("dirs", "tmp");
    $fileInfo = $mail->CreateDoc($tmpDir,true,true);
    try
    {
      $doc = new LIB_Document($this->module);

      $fields['path'] = '/files/';
      $fields['title'][0]['lang'] = 'pt_BR';
      $fields['title'][0]['value'] = 'Mensagem';
      $fields['content']['error'] = UPLOAD_ERR_OK;
      $fields['content']['tmp_name'] = $fileInfo['filePath'].'.pdf';
      $fields['content']['name'] = 'mensagem.pdf';
      $fields['content']['type'] = 'application/pdf';
      $fields['content']['size'] = filesize($fileInfo['filePath'].'.pdf');
      $doc->LoadFieldsDef(NULL,$fields['path']);
      $docId = $doc->Insert($fields);
      $mail->UnlinkGarbage($fileInfo['fileName']);
      return $docId;
    }
    catch(Exception $e)
    {
      $mail->UnlinkGarbage($fileInfo['fileName']);
      throw $e;
    }

  }

  protected function Init(& $mail)
  {
    $mail->Write('\documentclass[a4paper]{article}');
    $mail->Write('\usepackage{fancyhdr}');
    $mail->Write('\usepackage[psamsfonts]{amsfonts}');
    $mail->Write('\usepackage[leqno]{amsmath}');
    $mail->Write('\usepackage[dvips]{graphicx}');
    $mail->Write('\usepackage[portuges]{babel}');
    $mail->Write('\usepackage[utf8]{inputenc}');
    $mail->Write('\usepackage[T1]{fontenc}');
    $mail->Write('\usepackage{ae,aecompl}');
    $mail->Write('\usepackage[usenames]{color}');
    $mail->Write('\usepackage[dvipsnames]{xcolor}');
    $mail->Write('\usepackage{colortbl}');
    $mail->Write('\usepackage{pdfpages}');
    $mail->Write('\definecolor{camara}{rgb}{0.094,0.145,0.455}');
    $mail->Write('\renewcommand{\familydefault}{\sfdefault}');
    $mail->Write('\renewcommand{\doublerulesep}{1pt}');
    $mail->Write('\addtolength{\headheight}{0.5cm}');
    $mail->Write('\addtolength{\hoffset}{-3.5cm}');
    $mail->Write('\addtolength{\textwidth}{7.5cm}');
    $mail->Write('\addtolength{\voffset}{-1cm}');
    $mail->Write('\addtolength{\textheight}{3cm}');
    $mail->Write('\pagestyle{fancy}');
    $mail->Write('\lhead{{\huge\color{camara}Mensagem}}');
    $mail->Write('\chead{}');
    $mail->Write('\rhead{\includegraphics[scale=0.7]{logo}}');
    $mail->Write('\newcommand{\topHeadRule}');
    $mail->Write('{{\color{camara}%');
    $mail->Write('\hrule height 2pt');
    $mail->Write('width\headwidth');
    $mail->Write('\vspace{5pt}}');
    $mail->Write('}');
    $mail->Write('\renewcommand\headrule{');
    $mail->Write('\topHeadRule}');
    $mail->Write('\renewcommand\footrule');
    $mail->Write('{{\color{camara}%');
    $mail->Write('\hrule height 2pt');
    $mail->Write('width\headwidth}}');
    $mail->Write('\lfoot{'.strftime('%c').'}');
    $mail->Write('\cfoot{}');
    $mail->Write('\rfoot{\thepage}');
    $mail->Write('\fancypagestyle{plain}{');
    $mail->Write('  \fancyhf{}');
    $mail->Write('  \rhead{\includegraphics{logo}}');
    $mail->Write('  \lfoot{}');
    $mail->Write('  \rfoot{\thepage}}');
    $mail->Write('\fancypagestyle{title}{');
    $mail->Write('  \fancyhf{}');
    $mail->Write('  \rhead{\includegraphics{logo}}');
    $mail->Write('  \lfoot{}');
    $mail->Write('  \rfoot{}}');
    $mail->Write('\begin{document}');
    $mail->Write('\setlength\parindent{0pt}');
  }

  protected function End(& $mail)
  {
    $mail->Write('\end{document}');
  }

}

?>