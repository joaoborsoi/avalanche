<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Report.inc');
require_once('LEGIS_Process.inc');

class LEGIS_GroupingReport extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'paramList';
    $this->fieldsDef[0]['stripSlashes'] = true; 
    $this->fieldsDef[0]['trimField'] = false; 

    $this->fieldsDef[1]['fieldName'] = 'grouping';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $process = new LEGIS_Process($this);
    $memberAggregator = $process->GetMemberAggregator();

    if(count($memberAggregator) > 0)
    {
      $this->fields['paramList'][] = Array('param'=>'aggregator',
					   'aggregatorId'=>$memberAggregator['docId']);
    }

    $report = new LEGIS_Report($this);
    $rows = $report->GetGroupingReport($this->fields['paramList'],$this->fields['grouping']);
    if(count($rows)==0)
      throw new AV_Exception(NOT_FOUND,$this->pageBuilder->lang);
    $this->LoadChildrenFromRows($rows, 'item');
  }
}

?>