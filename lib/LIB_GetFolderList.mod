<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


class LIB_GetFolderList extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'path';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['default'] = '/';
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[2]['fieldName'] = 'fixedOrder';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'boolean';
    $this->fieldsDef[2]['default'] = '0';
  }

  protected function Execute()
  {
    $this->isArray = true;

    $folder = new LIB_Folder($this);
    $rows = $folder->GetFolderList($this->fieldParser->fields['path'], 
				   $this->fieldParser->fields['fixedOrder']);

    // load children
    $this->LoadChildrenFromRows($rows, 'folder');
  }
}

?>
