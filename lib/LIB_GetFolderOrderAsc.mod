<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


class LIB_GetFolderOrderAsc extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'path';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;
  }

  protected function Execute()
  {
    $folder = new LIB_Folder($this);
    $this->attrs['orderAsc'] = $folder->GetOrderAsc($this->fields['path']);
  }
}

?>
