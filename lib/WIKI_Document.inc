<?php

require_once('LIB_Document.inc');

class WIKI_Document extends LIB_Document
{
  protected function GetSearchResult(& $params)
  {
    if(in_array('r',$params['searchTables']['LIB_Node']['exclusionFilters']
		['docNode.otherRight']))
    {
      // removes comments from articles not published from the result
      $SQL = 'DELETE FROM sr USING ' .$params['tableName'] . ' sr,';
      $SQL .= 'WIKI_Document a,LIB_Node an,LIB_DocumentLink dl,LIB_Node fn ';
      $SQL .= 'WHERE articleId=a.docId ';
      $SQL .= 'AND a.docId=an.nodeId AND a.docId=dl.docId ';
      $SQL .= 'AND dl.folderId=fn.nodeId ';
      $SQL .= "AND (an.otherRight='' OR NOT ( ";

      if(isset($params['memberData']['userId']))
      {
	$SQL .= 'IF(fn.userId='.$params['memberData']['userId'].',';
	$SQL .= "INSTR(fn.userRight,'r')<>0,";
	$SQL .= "IF(fn.groupId IN (";
	foreach($params['memberGroups'] as $key => $group)
	{
	  if($key > 0)
	    $SQL .= ',';
	  $SQL .= $group;
	}
	$SQL .= "),INSTR(fn.groupRight,'r')<>0,";
	$SQL .= "INSTR(fn.otherRight,'r')<>0)))) ";
      }
      else
	$SQL .= "INSTR(fn.otherRight,'r')<>0)) ";
     $this->dbDriver->ExecSQL($SQL);
    }

    // complete comments results with user full name
    $SQL = 'ALTER TABLE '.$params['tableName']. ' ADD name VARCHAR(255) ';
    $this->dbDriver->ExecSQL($SQL);

    $SQL = 'UPDATE '.$params['tableName'].' sr, CMS_DocumentComments c, ';
    $SQL .= 'LIB_Node n, UM_User u ';
    $SQL .= "SET name=CONCAT(firstName,CONCAT(' ',lastName)) ";
    $SQL .= 'WHERE sr.docId=c.docId AND c.docId=n.nodeId ';
    $SQL .= 'AND n.userId=u.userId ';
    $this->dbDriver->ExecSQL($SQL);

    $params['returnFieldsFound'][] = 'name';

    // calls parent class method
    return parent::GetSearchResult($params);
  }
}