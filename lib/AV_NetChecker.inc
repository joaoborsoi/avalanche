<?php

class AV_NetChecker
{
  protected $pageBuilder;

  function __construct(& $pageBuilder)
  {
    $this->pageBuilder = & $pageBuilder;
  }

  function IsValidClient() 
  {
    $hostname = trim($this->pageBuilder->siteConfig->getVar("config", 
							    "allowed_hostname"));
    if($hostname != NULL)
    {
      $hostnameList = explode(',',$hostname);
      foreach($hostnameList as $host)
      {
	$records = dns_get_record($host);
	foreach($records as $record)
	{
	  if($record['type'] == 'A')
	    $ipAllowedList[] = $record['ip'];
	}
      }
      $ipList = explode(', ', $_SERVER['REMOTE_ADDR']);
      if($_SERVER['HTTP_X_FORWARDED_FOR']!=NULL && 
	 $this->pageBuilder->siteConfig->getVar("config",
						"http_x_forwarded_for"))
      {
	$forwardedList = explode(', ',$_SERVER['HTTP_X_FORWARDED_FOR']);
	$ipList[] = array_pop($forwardedList);
      }

      if($ipAllowedList != NULL)
	foreach($ipAllowedList as $ipAllowed)
	  if(in_array($ipAllowed,$ipList))
	    return true;
    }

    $network = trim($this->pageBuilder->siteConfig->getVar("config", 
							   "allowed_network"));
    if($network == NULL)
      return ($hostname==NULL);

    $network = explode('/', $network);

    //doesn't check for the return value of ip2long
    $ip = ip2long($_SERVER['REMOTE_ADDR']);
    $rede = ip2long($network[0]);
    $mask = ip2long($network[1]);

    //AND
    $res = $ip & $mask;
 
    return ($res == $rede);

  }
}
?>