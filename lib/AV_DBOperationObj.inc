<?php

require_once('AV_OperationObj.inc');

define('DEAD_LOCK', -100);

//--Class: AV_DBOperationObj
//--Parent: AV_OperationObj
//--Desc: Represents module's object which executes database operations, such
//--Desc: as query
abstract class AV_DBOperationObj extends AV_OperationObj
{
  //
  //--Private
  //
  protected $transaction;    //--Desc: flag to indicate whether to
                             //--Desc: use transactions
  protected $tableLocks = array(); //--Desc: keep the tables locks


  //
  //--Public
  //

  //--Method: AV_DBOperationObj
  //--Desc: Constructor
  function __construct(&$pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
		       $jsonInput=false, $transaction = true)
  {
    $this->transaction = $transaction;
    parent::__construct($pbuilder, $itemTpl, $itemTplTag, $varPrefix, $jsonInput);
  }

  //--Method: Load
  //--Desc: Implements transaction safe on load
  function Load(&$inputSources)
  {
    // not transaction safe
    if(!$this->transaction)
    {
      parent::Load($inputSources);
      return;
    }

    do
    {
      $ret = SUCCESS;
      $this->dbDriver->ExecSQL('BEGIN');

      try
      {
	parent::Load($inputSources);
      }
      catch(Exception $e)
      {
	$ret = $e->getCode();
	$this->dbDriver->ExecSQL('ROLLBACK');
	if($ret != DEAD_LOCK && $ret != SUCCESS)
	  throw $e;
      }
    } while($ret == DEAD_LOCK);

    $this->dbDriver->ExecSQL('COMMIT');
  }

}

?>