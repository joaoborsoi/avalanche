<?php

require_once('LEGIS_AdvancedSearch.inc');

class LEGIS_AdvancedSearchReceipt extends LEGIS_AdvancedSearch
{
  function SearchReceipt(& $params, $limit, $offset, &$totalResults, &$status)
  {
    if(count($params) == 0)
    {
      $status = INVALID_FIELD;
      return NULL;
    }
    $tables[0] = 'LIB_Document doc';
    $tablesWhere = array();
    $tables[] = 'LEGIS_Receipt r';
    $tablesWhere[] = 'doc.docId = r.docId';
    $where = array();
    foreach($params as $param)
    {
      switch($param['field'])
      {
      case 'folder':
	$this->AddFolder($param, $tables, $tablesWhere, $where);
	break;

      case 'justice':
	$this->AddJustice($param, $tables, $tablesWhere, $where);
	break;

      case 'actionType':
	$this->AddActionType($param, $tables, $tablesWhere, $where);
	break;

      case 'local':
	$this->AddLocal($param, $tables, $tablesWhere, $where);
	break;

      case 'phases':
	$this->AddPhases($param, $tables, $tablesWhere, $where);
	break;

      case 'subject':
	$this->AddSubject($param, $tables, $tablesWhere, $where);
	break;

      case 'situation':
 	$this->AddSituation($param, $tables, $tablesWhere, $where);
	break;

      case 'result':
 	$this->AddResult($param, $tables, $tablesWhere, $where);
	break;

      case 'otherPart':
 	$this->AddOtherPart($param, $tables, $tablesWhere, $where);
	break;

      case 'client':
	$this->AddClient($param, $tables, $tablesWhere, $where);
	break;

      case 'start':
	$this->AddStart($param, $tables, $tablesWhere, $where);
	break;

      case 'city':
	$this->AddProcessCity($param, $tables, $tablesWhere, $where);
	break;

      case 'state':
	$this->AddProcessState($param, $tables, $tablesWhere, $where);
	break;

      case 'clientCity':
	$this->AddClientCity($param, $tables, $tablesWhere, $where);
	break;

      case 'receiptDate':
	$this->AddDate($param, $tables, $tablesWhere, $where);
	break;

      case 'receiptValue':
	$this->AddValue($param, $tables, $tablesWhere, $where);
	break;

      case 'receiptNum':
	$this->AddNumber($param, $tables, $tablesWhere, $where);
	break;
      }
    }

    $SQL = $this->Search('doc.docId, doc.title ',
			 $limit, $offset, $tables, $tablesWhere, $where,
			 $totalResults, $status);
    if($status != SUCCESS)
      return NULL;

    return $this->dbDriver->GetAll($SQL, $status);
  }


  function AddFolder(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'r.processId = p.processId';
    }
    if(!in_array('LEGIS_RootProcess rp', $tables))
    {
      $tables[] = 'LEGIS_RootProcess rp';
      $tablesWhere[] = 'p.rootProcessId = rp.processId';
    }

    $expr = "rp.folder = '" . $param['valueA'] . "'";
    if($param['valueB'] != NULL)
      $expr .= " AND rp.folderYear = '" . $param['valueB'] . "'";

    if($param['not'])
      $expr = "NOT ($expr)";
    $where['folder'][] = $expr;
  }

  function AddJustice(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'r.processId = p.processId';
    }
    if(!in_array('LEGIS_RootProcess rp', $tables))
    {
      $tables[] = 'LEGIS_RootProcess rp';
      $tablesWhere[] = 'p.rootProcessId = rp.processId';
    }

    $where['justice'][] = 
      "rp.justiceId ".($param['not']?'<>':'=')." '" . $param['valueA'] . "'";
  }


  function AddActionType(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'r.processId = p.processId';
    }

    $where['actionTypes'][] = 
      "p.actionTypeId ".($param['not']?'<>':'=')." '" . $param['valueA'] . "'";
  }

  function AddLocal(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!$param['not'])
    {
      if(!in_array('LEGIS_ProcessLocal pl', $tables))
      {
	$tables[] = 'LEGIS_ProcessLocal pl';
	$tablesWhere[] = 'r.processId = pl.processId';
      }

      $expr = "pl.localId = '" . $param['valueA'] . "'";
      if($param['valueB'] !== '' && $param['valueB'] !== NULL)
	$expr .= " AND pl.room = '" . $param['valueB'] . "'";
    }
    else
    {
      $expr = '(SELECT DISTINCT processId FROM LEGIS_ProcessLocal ';
      $expr .= "WHERE localId = '" . $param['valueA'] . "'";
      if($param['valueB'] !== '' && $param['valueB'] !== NULL)
	$expr .= " AND room = '" . $param['valueB'] . "')";

      $expr = 'r.processId NOT IN ' . $expr;
    }
    $where['local'][] = $expr;
  }


  function AddPhases(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'r.processId = p.processId';
    }
    $where['phases'][] = 
      "p.phasesId ".($param['not']?'<>':'=')." '" . $param['valueA'] . "'";
  }

  function AddSubject(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'r.processId = p.processId';
    }
    $where['subject'][] = 
      "p.subjectId ".($param['not']?'<>':'=')." '" . $param['valueA'] . "'";
  }

  function AddSituation(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'r.processId = p.processId';
    }
    $where['situation'][] = 
      "p.situationId ".($param['not']?'<>':'=')." '" . $param['valueA'] . "'";
  }

  function AddResult(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'r.processId = p.processId';
    }
    $where['result'][] = 
      "p.resultId ".($param['not']?'<>':'=')." '" . $param['valueA'] . "'";
  }

  function AddOtherPart(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'r.processId = p.processId';
    }
    $where['otherPart'][] = 
      "p.otherPart ".($param['not']?'NOT ':'')."like '%".$param['valueA']."%'";
  }

  function AddClient(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['client'][] = 
      "r.entityId ".($param['not']?'<>':'=')." '" . $param['valueA'] . "'";
  }

  function AddStart(& $param, & $tables, & $tablesWhere, & $where)
  {
    $SQL = "(SELECT MIN(date) FROM LEGIS_History WHERE processId = doc.docId)";
    $where['start'][] = $this->GetDateParam($SQL, $param);
  }

  function AddProcessCity(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!$param['not'])
    {
      if(!in_array('LEGIS_ProcessLocal pl', $tables))
      {
	$tables[] = 'LEGIS_ProcessLocal pl';
	$tablesWhere[] = 'r.processId = pl.processId';
      }

      $where['city'][] = "pl.cityId = '" . $param['valueA'] . "'";
    }
    else
    {
      $SQL = '(SELECT DISTINCT processId FROM LEGIS_ProcessLocal ';
      $SQL .= "WHERE cityId='".$param['valueA'] . "')";
      $where['city'][] = 'r.processId NOT IN ' . $SQL;
    }
  }

  function AddProcessState(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!$param['not'])
    {
      if(!in_array('LEGIS_ProcessLocal pl', $tables))
      {
	$tables[] = 'LEGIS_ProcessLocal pl';
	$tablesWhere[] = 'r.processId = pl.processId';
      }
      $where['state'][] = "pl.stateId = '" . $param['valueA'] . "'";
    }
    else
    {
      $SQL = '(SELECT DISTINCT processId FROM LEGIS_ProcessLocal ';
      $SQL .= "WHERE stateId='".$param['valueA'] . "')";
      $where['state'][] = 'r.processId NOT IN ' . $SQL;
    }
  }

  function AddClientCity(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('EM_NaturalPerson np', $tables))
    {
      $tables[] = 'EM_NaturalPerson np';
      $tablesWhere[] = 'r.entityId = np.entityId';
    }
    $where['clientCity'][] = 
      "np.cityId ".($param['not']?'<>':'=')." '" . $param['valueA'] . "'";
  }

  function AddDate(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['date'][] = $this->GetDateParam('r.date', $param);
  }


  function AddValue(& $param, & $tables, & $tablesWhere, & $where)
  {
    $expr = '';
    if($param['valueA'] !== '' && $param['valueA'] !== NULL)
      $expr = "'" . $param['valueA'] . "' <= r.value";

    if($param['valueB'] !== '' && $param['valueB'] !== NULL)
    {
      if($expr != '')
	$expr .= ' AND ';
      $expr .= "r.value <= '" . $param['valueB'] . "'";
    }
    $where['value'][] = $expr;
  }

  function AddNumber(& $param, & $tables, & $tablesWhere, & $where)
  {
    $expr = '';
    if($param['valueA'] !== '' && $param['valueA'] !== NULL)
    {
      $valueA = explode('-',$param['valueA']);
      if(count($valueA)==2)
      {
	$expr .= "'" . $valueA[0] . "' <= r.number";
	$expr .= " AND '" . $valueA[1] . "' <= r.year";
      }
    }

    if($param['valueB'] !== '' && $param['valueB'] !== NULL)
    {
      if($expr != '')
	$expr .= ' AND ';
      $valueB = explode('-',$param['valueB']);
      if(count($valueB)==2)
      {
	$expr .= "r.number <= '" . $valueB[0] . "'";
	$expr .= " AND r.year <= '" . $valueB[1] . "'";
      }
    }
    $where['value'][] = $expr;
  }

}

?>