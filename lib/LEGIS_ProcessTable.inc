<?php

require_once('FORM_DynTable.inc');
require_once('LEGIS_ContractDoc.inc');

class LEGIS_ProcessTable extends FORM_DynTable
{
  function Insert(& $fields, $extraFields = array())
  {
    parent::Insert($fields,$extraFields);

    // clientes do processo
    $this->SetProcessClients($extraFields['processId'],$fields['clientList'],true);

    // numeros do processo
    $this->SetProcessNumbers($extraFields['processId'],$fields['processNumbers'],true);
    
    // numeros do processo
    $this->SetProcessLocal($extraFields['processId'],$fields['processLocals']);
  }

  function Set($fields, $extraFields = array())
  {
    $oldValues = parent::Set($fields,$extraFields);

    // clientes do processo
    if(isset($fields['clientList']))
      $this->SetProcessClients($fields['processId'],$fields['clientList'],false);
    
    // numeros do processo
    if(isset($fields['processNumbers']))
      $this->SetProcessNumbers($fields['processId'],$fields['processNumbers'],false);
    
    // numeros do processo
    if(isset($fields['processLocals']))
      $this->SetProcessLocal($fields['processId'],$fields['processLocals']);


    return $oldValues;
  }

  function SetProcessClients($processId, & $processClients, $newProcess)
  {
    if(!$newProcess)
    {
      $SQL = "DELETE FROM LEGIS_ProcessClient WHERE processId=$processId";
      $this->dbDriver->ExecSQL($SQL);
    }
    $SQL = 'INSERT INTO LEGIS_ProcessClient ';
    $SQL .= '(processId, entityId, header, excluded, reason) VALUES ';

    foreach($processClients as $key=>$client)
    {  
      if($key > 0)
	$SQL .= ',';
      $SQL .= "($processId,".$client['entityId'].",'".$client['header']."','";
      $SQL .= $client['excluded']."','".$client['reason']."')";
    }
    $this->dbDriver->ExecSQL($SQL);
  }

  function SetProcessNumbers($processId, & $processNumbers, $newProcess)
  {
    if(!$newProcess)
    {
      $SQL = "DELETE FROM LEGIS_ProcessNumber WHERE processId=$processId";
      $this->dbDriver->ExecSQL($SQL);
    }
    $SQL = 'INSERT INTO LEGIS_ProcessNumber (processId, number) VALUES ';
    foreach($processNumbers as $key=>$number)
    {
      if($key > 0)
	$SQL .= ',';
      $SQL .= "($processId,'$number')";
    }
    $this->dbDriver->ExecSQL($SQL);
  }

  function SetProcessLocal($processId, & $processLocals)
  {
    $table = new FORM_DynTable('LEGIS_ProcessLocal', $this->module);
    $table->LoadFieldsDef();

    foreach($processLocals as &$local)
    {
      if($local['active'])
      {
	if($local['processLocalId']!=NULL)
	  $table->Set($local);
	else
	{
	  $local['processId'] = $processId;
	  $table->Insert($local);
	}
      }
      else
      {
	if($local['processLocalId']!=NULL)
	  $table->Del($local);
      }
    }

  }

  protected function ParseFieldDef(& $fieldDef,$disableFileRequired,$disableRequired)
  {
    parent::ParseFieldDef($fieldDef,$disableFileRequired,$disableRequired);

    switch($fieldDef['type'])
    {
    case 'processNumbers':
      // gambiarra pra evitar addslashes      
      $fieldDef['stripSlashes'] = true; 
      break;
      
    case 'clientList':
      // gambiarra pra evitar addslashes      
      $fieldDef['stripSlashes'] = true; 
      break;

    case 'processLocals':
      // gambiarra pra evitar addslashes      
      $fieldDef['stripSlashes'] = true; 
      break;
    }
  }

  protected function &GetField(& $fieldDef, & $values, $keyFields,  
			       & $templates, $sourceId, $dateFormat = NULL,
			       $numberFormat = true, $fieldName = NULL)
  {
    switch ($fieldDef['type'])
    {
    case 'clientList':
      return $this->GetClientListField($fieldDef,$keyFields);
    case 'processNumbers':
      return $this->GetProcessNumbers($fieldDef,$keyFields);
    case 'processLocals':
      return $this->GetProcessLocals($fieldDef,$keyFields,$sourceId,$values,
				     $dateFormat,$numberFormat,$fieldName);
    case 'processHistory':
      return $this->GetHistoryField($fieldDef,$keyFields);
    case 'processCommitment':
      return $this->GetCommitmentField($fieldDef,$keyFields);
    case 'contractList':
      return $this->GetContractList($fieldDef,$keyFields,$dateFormat,
				    $numberFormat);
    case 'entryList':
      return $this->GetEntryList($fieldDef,$keyFields,$dateFormat,
				 $numberFormat);
    default:
      return parent::GetField($fieldDef,$values,$keyFields,$templates,
			      $sourceId,$dateFormat,$numberFormat);
    }
  }

  protected function &GetHistoryField($fieldDef,$keyFields)
  {
    if($keyFields==NULL)
      return NULL;

    // cria objeto para o campo de historico
    $field =  new AV_ModObject('history',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    // retorna historico
    $SQL = '(SELECT h.docId, date, subject, f.docId as defDocId, ';
    $SQL .= 'f.contentType as defDocContentType, f.title as defDocTitle ';
    $SQL .= 'FROM LEGIS_History h, LIB_FileDocument f ';
    $SQL .= "WHERE h.processId='".$keyFields['processId']."' ";
    $SQL .= 'AND h.defaultDoc=f.docId)';
    $SQL .= ' UNION ';
    $SQL .= '(SELECT h.docId, date, subject, NULL as defDocId, ';
    $SQL .= 'NULL as defDocContentType, NULL as defDocTitle ';
    $SQL .= 'FROM LEGIS_History h ';
    $SQL .= "WHERE h.processId='".$keyFields['processId']."' ";
    $SQL .= 'AND defaultDoc IS NULL ) ';
    $field->attrs['items'] = $this->dbDriver->GetAll($SQL);
    return $field;
  }

  protected function &GetCommitmentField($fieldDef,$keyFields)
  {
    if($keyFields==NULL)
      return NULL;

    // cria objeto para o campo de compromisso
    $field =  new AV_ModObject('history',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    // retorna compromisssos
    $SQL = 'SELECT docId, date, subject ';
    $SQL .= 'FROM LEGIS_Commitment ';
    $SQL .= "WHERE processId='".$keyFields['processId']."' ";
    $field->attrs['items'] = $this->dbDriver->GetAll($SQL);
    return $field;
  }

  protected function &GetContractList($fieldDef,$keyFields,$dateFormat,$numberFormat)
  {
    if($keyFields==NULL)
      return NULL;

    // cria objeto para o campo de contratos
    $field =  new AV_ModObject('contractList',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    $doc = new LEGIS_ContractDoc($this->module);
    $search['path'] = '/contract/';
    $search['orderBy'] = 'expiration';
    $search['order'] = '0';
    $search['dateFormat'] = $dateFormat;
    $search['numberFormat'] = $numberFormat;
    $search['tempTable'] = '0';
    $search['processId'] = $keyFields['processId'];
    $field->attrs['items'] = $doc->Search($search);
    
    return $field;
  }

  protected function &GetEntryList($fieldDef,$keyFields,$dateFormat,$numberFormat)
  {
    if($keyFields==NULL)
      return NULL;

    // cria objeto para o campo de contratos
    $field =  new AV_ModObject('contractList',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    $doc = new LIB_Document($this->module);
    $search['path'] = '/entry/';
    $exlusionFilter['fieldName'] = 'processId';
    $exlusionFilter['value'] = current($keyFields);
    $exlusionFilter['operator'] = '=';
    $search['exclusionFilters']['processId'][] = $exlusionFilter;
    $search['orderBy'] = 'expiration';
    $search['order'] = '0';
    $search['dateFormat'] = $dateFormat;
    $search['numberFormat'] = $numberFormat;
    $search['tempTable'] = '0';
    $field->attrs['items'] = $doc->Search($search);
    
    return $field;
  }

  protected function &GetProcessLocals($fieldDef,$keyFields,$sourceId,&$values,
				       $dateFormat,$numberFormat,$fieldName)
  {
    if($fieldName != NULL)
      $fieldName = explode('.',$fieldName);

    if($fieldName != NULL && count($fieldName)==1)
      $justiceId = $sourceId;
    else
      $justiceId = $values['justiceId'];

    // cria objeto para o campo de lista de locais
    $field =  new AV_ModObject('processLocal',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    // recupera locais segundo justiça
    if($justiceId != NULL)
    {
      if($keyFields != NULL)
	$processId = current($keyFields);

      $SQL = 'SELECT docId as localId,name,cityFlag,auxFieldFlag,';
      $SQL .= '(SELECT processLocalId ';
      $SQL .= "FROM LEGIS_ProcessLocal pl WHERE processId = '$processId' ";
      $SQL .= "AND pl.localId=l.docId) as processLocalId ";
      $SQL .= "FROM LEGIS_Local l WHERE justiceId = '$justiceId' ";
      $SQL .= "ORDER BY orderby ";
      $localList = $this->dbDriver->GetAll($SQL);

      // default São Paulo/SP
      $defValues['uf_codigo'] = "26";
      $defValues['cidade_codigo'] = "9394";

      foreach($localList as $key => &$local)
      {
	if($local['processLocalId']!=NULL)
	  $processLocalKey = array('processLocalId'=>$local['processLocalId']);
	else
	  $processLocalKey = NULL;

	// recupera formulário usado para campos
	$formObject = new FORM_Object('form', $this->pageBuilder); 
	$processLocal = new FORM_DynTable('LEGIS_ProcessLocal', $this->module);
	$processLocal->GetForm($processLocalKey, $localFieldName, $localSourceId, $templates, 
			       $formObject, $defValues,$dateFormat,
			       $numberFormat, $short);
	
	$formObject->attrs = array_merge($formObject->attrs,$local);
	$field->children[] = $formObject;
	
	$field->attrs['value'][] = $formObject->attrs;
      }
      return $field;
    } 
    else if(count($fieldName)==2) 
    {
      $formObject = new FORM_Object('form', $this->pageBuilder); 
      $processLocal = new FORM_DynTable('LEGIS_ProcessLocal', $this->module);
      $processLocal->GetForm(NULL, $fieldName[1], $sourceId, $templates, 
			     $formObject, $defValues,$dateFormat,
			     $numberFormat, $short);
      return current($formObject->children);
    }

    return $field;
  }

  protected function &GetProcessNumbers($fieldDef,$keyFields)
  {
    // cria objeto para o campo
    $field = new AV_ModObject('processNumbers', $this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    if($keyFields != NULL)
    {
      $processId = current($keyFields);

      // recupera numeros do processo
      $SQL = "SELECT number ";  
      $SQL .="FROM LEGIS_ProcessNumber ";
      $SQL .= "WHERE processId = '$processId' ";
      $field->attrs['value'] = $this->dbDriver->GetCol($SQL);
    }
    return $field;
  }

  protected function &GetClientListField($fieldDef,$keyFields)
  {   
    // cria objeto para o campo
    $field = new AV_ModObject('clientList', $this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    if($keyFields != NULL)
    {
      $processId = current($keyFields);

      // retorna clientes pessoa jurídica
      $SQL = "(SELECT pc.entityId, name, path, ";
      $SQL .= "header, excluded, reason, '' as cpf, le.cnpj ";
      $SQL .= "FROM LEGIS_ProcessClient pc, EM_LegalEntity le, ";
      $SQL .= 'LIB_DocumentLink dl, LIB_Folder f ';
      $SQL .= "WHERE processId='$processId' ";
      $SQL .= "AND le.docId=pc.entityId ";
      $SQL .= 'AND le.docId=dl.docId AND dl.folderId=f.folderId) ';
      
      $SQL .= 'UNION ';
      
      // retorna clientes pessoa física
      $SQL .= "(SELECT pc.entityId, name, path,";
      $SQL .= "header, excluded, reason, ";
      $SQL .= "np.cpf, '' as cnpj ";
      $SQL .= "FROM LEGIS_ProcessClient pc,EM_NaturalPerson np,";
      $SQL .= 'LIB_DocumentLink dl, LIB_Folder f ';
      $SQL .= "WHERE processId='$processId' ";
      $SQL .= "AND np.docId=pc.entityId ";
      $SQL .= 'AND np.docId=dl.docId AND dl.folderId=f.folderId) ';
            
      // recupera lista de clientes
      $field->attrs['value'] = $this->dbDriver->GetAll($SQL);

      // se usuário logado for cliente, este precisa estar na lista de clientes
      // do processo consultado para ter permissão
      $user = new UM_User($this->module);
      $groups = $user->GetMemberGroups();
      $customer = false;
      foreach($groups as & $group)
      {
	if($group['groupId']=='5')
	  $customer = true;
      }
      if($customer)
      {
	$memberData = $user->GetMemberData();
	$foundMember = false;
      }

      // ajusta situação dos clientes
      foreach($field->attrs['value'] as & $client)
      {
	if($customer && $client['entityId'] == $memberData['entityId'])
	  $foundMember = true;

	if($client['path']=="/client/naturalPerson/") 
	{
	  $SQL = 'SELECT ps.situationId, ps.name ';
	  $SQL .= 'FROM LEGIS_PersonSituation ps, LEGIS_CustomerSituation cs ';
	  $SQL .= "WHERE cs.docId='".$client['entityId']."' ";
	  $SQL .= "AND ps.situationId=cs.situationId ";
	  $client['situationId'] = $this->dbDriver->GetAll($SQL);
	}
      }

      if($customer && !$foundMember)
	throw new AV_Exception(PERMISSION_DENIED,$this->lang);
    }
    return $field;

  }

      
}
?>