<?php

require_once('AV_DBOperationObj.inc');

class PP_CheckSubscription extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'userId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $SQL = "SELECT IF(COUNT(*)>0,1,0) as permission ";
    $SQL .= 'FROM PP_Subscription s, PP_SubscriptionPayment p ';
    $SQL .= "WHERE userId='".$this->fields['userId']."' ";
    $SQL .= 'AND subscr_cancel_date IS NULL ';
    $SQL .= "AND subscr_eot = '0' ";
    $SQL .= 'AND s.subscr_id = p.subscr_id ';
    $SQL .= "AND payment_status='Completed' ";
    $this->attrs['permission'] = $this->dbDriver->GetOne($SQL);
  }
}

?>