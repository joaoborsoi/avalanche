<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Node.inc');


class LIB_TakeDocOwnership extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  function Execute()
  {
    $doc = new LIB_Document($this);
    $this->attrs['login'] = $doc->TakeOwnership($this->fields['docId']);
  }
}

?>
