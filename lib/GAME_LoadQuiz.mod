<?php

require_once('AV_DBOperationObj.inc');

class GAME_LoadQuiz extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'id';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $this->isArray = true;
    $SQL = 'SELECT * FROM GAME_Quiz  ';
    $SQL .= "WHERE id='".$this->fields['id']."' ";
    $this->attrs = $this->dbDriver->GetRow($SQL);
    if(count($this->attrs)==0)
      throw new AV_Exception(NOT_FOUND,$this->pageBuilder->lang);

    $SQL = 'SELECT * FROM GAME_QuizAnswer q, GAME_PlayUser pu, UM_UserSessions us ';
    $SQL .= "WHERE sessionId='".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $SQL .= "AND playId = pu.id AND quizId='".$this->fields['id']."' ";
    $quizAnswer = $this->dbDriver->GetRow($SQL);

    $SQL = 'SELECT id, text ';
    if(count($quizAnswer) != 0)
    {
      $SQL .= ",IF(id='".$quizAnswer['optionId']."','checked=\"checked\"',NULL) ";
      $SQL .= "as xhtmlChecked, 'disabled=\"disabled\"' as xhtmlDisabled ";
      $this->attrs['answered'] = true;
    }
    $SQL .= 'FROM GAME_QuizOption ';
    $SQL .= "WHERE quizId='".$this->attrs['id']."' ";
    $rows = $this->dbDriver->GetAll($SQL);
    $this->LoadChildrenFromRows($rows,'quizOption');
  }
}