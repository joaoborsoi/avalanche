<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_GetMemberData extends AV_DBOperationObj
{
  protected $userMan;

  function __construct(&$pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
		       $jsonInput=false, $transaction = true)
  {
    $this->userMan = new UM_User($this);
    parent::__construct($pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
			$jsonInput,$transaction);
  }

 
  protected function Execute()
  {
    // get rows
    $rows = $this->userMan->GetMemberData();
    $this->attrs = array_merge((array)$this->attrs,(array)$rows);
  }
}

?>

