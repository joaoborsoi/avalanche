<?php

$tkId = 10000;
define('integerTk', $tkId++);
define('floatTk', $tkId++);
define('varTk', $tkId++);
define('stringTk', $tkId++);
define('unclosedStringTk', $tkId++);
define('invalidTk', $tkId++);
define('eofTk', $tkId++);

class LEX_Analyzer
{
  var $caseSensitive;
  var $symbols;
  var $code;
  var $codeLength;
  var $nextPos;
  var $char;
  var $charKey;
  var $decimalPoint;
  var $multibyte;

  // public readonly
  var $value;
  var $token;
  var $tokenPos;

  function __construct($code, $symbols, $caseSensitive=true, 
		       $decimalPoint = '.', $multibyte = true)
  {
    $this->multibyte = $multbyte;
    $this->symbols = ($caseSensitive?$symbols:array_change_key_case($symbols));
    $this->code = $code;
    $this->caseSensitive = $caseSensitive;
    $this->nextPos = 0;
    $this->tokenPos = 0;
    $this->codeLength = $this->multibyte?mb_strlen($this->code):strlen($this->code);
    $this->decimalPoint = $decimalPoint;
    $this->NextChar();
  }

  function NextChar()
  {
    if($this->nextPos < $this->codeLength)
    {
      $this->char = $this->multibyte ? 
	mb_substr($this->code,$this->nextPos++,1):
	substr($this->code,$this->nextPos++,1);

      if(!$this->caseSensitive)
	$this->charKey = strtolower($this->char);
      else
	$this->charKey = $this->char;
    }
    else
      $this->charKey = $this->char = NULL;
  }

  function NextToken()
  {
    $this->value = NULL;

    // eliminates white space
    while($this->char == ' ' ||
	  $this->char == "\n" ||
	  $this->char == "\t" ||
	  $this->char == "\r")
      $this->NextChar();

    if($this->char === NULL)
    {
      $this->token = eofTk;
      return false;
    }

    // string
    if($this->char=='\'' || $this->char=='"')
      $this->ParseString();
    else
    {
      // symbols
      if(isset($this->symbols[$this->charKey]))
	$this->ParseSymbol();
      // numbers
      elseif(ctype_digit($this->char))
	$this->ParseNumber();
      // variables
      elseif(ctype_alpha($this->char))
	$this->ParseVariables();
      else
      {
	$this->token = invalidTk;
	$this->NextChar();
      }
    }

    return true;
  }

  protected function ParseString()
  {
    $delimiter = $this->char;
    $this->NextChar();
    $this->value = $delimiter;

    while($this->char != NULL && $this->char != $delimiter)
    {
      if($this->char == '\\')
      {
	$this->value .= $this->char;
	$this->NextChar();
	
	if($this->char != NULL)
	{
	  $this->value .= $this->char;
	  $this->NextChar();
	}
      }
      else
      {
	$this->value .= $this->char;
	$this->NextChar();
      }
    }
    if($this->char === NULL)
      $this->token = unclosedStringTk;
    else
    {
      $this->value .= $delimiter;
      $this->token = stringTk;
      $this->NextChar();
    }
  }

  protected function ParseSymbol()
  {
    $this->tokenPos = $this->nextPos - 1;
    $this->token = $this->symbols[$this->charKey];
    $this->value = $this->char;
    $this->NextChar();
  }

  protected function ParseNumber()
  {
    $this->tokenPos = $this->nextPos - 1;
    $this->value = '';
    do
    {
      $this->value .= $this->char;
      $this->NextChar();
    } while(ctype_digit($this->char));

    if($this->char == $this->decimalPoint)
    {
      // eats decimal
      $this->value .= $this->char;
      $this->NextChar(); 

      while(ctype_digit($this->char))
      {
	$this->value .= $this->char;
	$this->NextChar();
      }
	
      $this->token = floatTk;
      $this->value = floatval($this->value);
    }
    else
    {
      $this->token = integerTk;
      $this->value = intval($this->value);
    }
  }

  protected function ParseVariables()
  {
    $this->tokenPos = $this->nextPos - 1;
    $this->value = '';
    $key = '';
    do
    {
      $this->value .= $this->char;
      $key .= $this->charKey;
      $this->NextChar();
    } while(ctype_alnum($this->char) || $this->char == '_');
    
    if(isset($this->symbols[$key]))
      $this->token = $this->symbols[$key];
    else
      $this->token = varTk;
  }


}

?>