<?php

class CMS_Bootstrap
{
  function Carousel($imageList,$size=NULL)
  {
    $imageList = explode(',',$imageList);
    $carousel = (count($imageList)>1);
    $li = NULL;
    foreach($imageList as $key=>$image)
    {
      if($carousel)
      {
	$li .= "<li data-target='#imageList' data-slide-to='$key'";
	if($key==0)
	  $li .= " class='active'";
	$li .="></li>\n";
      }

      $items .= "<div class='item";
      if($key==0)
	$items .= ' active';
      $items .= "'><img class='item' src='thumb/$image";
      if($size != NULL)
	$items .= "?size=$size";
      $items .= "' alt='$imageId' />\n";
      $items .= "</div>\n";
    }
    if($carousel)
    {
      $content = "<div class='carousel slide'>\n";
      $content .= "<ol class='carousel-indicators'>\n$li</ol>\n";
      $content .= "<div class='carousel-inner'>\n$items</div>\n";
      $content .= "<a class='carousel-control left' href='#imageList' ";
      $content .= "data-slide='prev'><i class='icon icon-chevron-left'></i></a>\n";
      $content .= "<a class='carousel-control right' href='#imageList' ";
      $content .= "data-slide='next'><i class='icon icon-chevron-right'></i></a>\n";
      $content .= "</div>\n";
    }
    else
      $content = "<div class='carousel'>\n$items\n</div>";
    return $content;
  }
}

?>