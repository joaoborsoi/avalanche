<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


class LIB_GetFolder extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'folderId';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'path';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['stripSlashes'] = false;

    $this->fieldsDef[2]['fieldName'] = 'fieldName';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;

    $this->fieldsDef[3]['fieldName'] = 'sourceId';
    $this->fieldsDef[3]['required'] = false;
    $this->fieldsDef[3]['allowNull'] = true;
    $this->fieldsDef[3]['consistType'] = 'integer';

    $this->fieldsDef[4]['fieldName'] = 'dateFormat';
    $this->fieldsDef[4]['required'] = false;
    $this->fieldsDef[4]['allowNull'] = true;
  }

  protected function GetTemplates()
  {
    if($this->fields['fieldName'] != '')
    {
      $templates['select']['fileName'] = 'loadSelectField';
      $templates['selectMult']['fileName'] = 'loadSelectMultField';
      $templates['selectOption']['fileName']  = 'loadSelectFieldOption';
      $templates['selectMultOption']['fileName']  = 'loadSelectMultFieldOption';
    }
    else
    {
      $templates['hidden']['fileName'] = 'hiddenField';
      $templates['hidden']['outputFuncRef'] = 'htmlentities_utf';
      $templates['text']['fileName'] = 'textField';
      $templates['text']['outputFuncRef'] = 'htmlentities_utf';
      $templates['textArea']['fileName'] = 'textAreaField';
      $templates['textArea']['outputFuncRef'] = 'htmlentities_utf';
      $templates['htmlArea']['fileName'] = 'htmlAreaField';
      $templates['htmlArea']['outputFuncRef'] = 'htmlentities_utf';
      $templates['cep']['fileName']  = 'cepField';
      $templates['cep']['outputFuncRef'] = 'htmlentities_utf';
      $templates['cnpj']['fileName']  = 'cnpjField';
      $templates['cpf']['fileName']  = 'cpfField';
      $templates['file']['fileName'] = 'fileField';
      $templates['fileList']['fileName'] = 'imageListField';
      $templates['fileListItem']['fileName'] = 'imageListItem';

      if($this->pageBuilder->lang == 'en_US')
	$templates['date']['fileName'] = 'dateField_en_US';
      else
	$templates['date']['fileName'] = 'dateField';

      $templates['select']['fileName'] = 'selectField';
      $templates['selectMult']['fileName'] = 'selectMultField';
      $templates['selectMult']['outputFuncRef'] = 'htmlentities_utf';
      $templates['selectOption']['fileName']  = 'selectFieldOption';
      $templates['selectOption']['outputFuncRef'] = 'htmlentities_utf';
      $templates['selectMultOption']['fileName']  = 'selectMultFieldOption';
      $templates['selectMultOption']['outputFuncRef'] = 'htmlentities_utf';
      $templates['legalEntityRef']['fileName']  = 'legalEntityRefField';
      $templates['legalEntityRef']['outputFuncRef'] = 'htmlentities_utf';
      $templates['naturalPersonRef']['fileName']  = 'naturalPersonRefField';
      $templates['naturalPersonRef']['outputFuncRef'] = 'htmlentities_utf';
      $templates['accessRight']['fileName']  = 'accessRight';
      $templates['accessRight']['outputFuncRef'] = 'htmlentities_utf';
      $templates['langField']['fileName']  = 'langField';
      $templates['langField']['outputFuncRef'] = 'htmlentities_utf';
      $templates['langLabel']['fileName']  = 'langLabelField';
      $templates['langTextArea']['fileName']  = 'langTextAreaField';
      $templates['langHtmlArea']['fileName']  = 'langHtmlAreaField';
      $templates['langTab']['fileName']  = 'langTab';
      $templates['langTabItem']['fileName']  = 'langTabItem';
      $templates['boolean']['fileName']  = 'checkboxField';
    }
    return $templates;
  }

  protected function Execute()
  {
    $templates = $this->GetTemplates();

    $folder = new LIB_Folder($this);

    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 

    if($this->fields['folderId'] == NULL &&
       $this->fields['path'] != NULL)
    {
      $folderData = $folder->GetFolderData($this->fields['path']);
      $this->fields['folderId'] = $folderData['folderId'];
    }

    if($this->fields['folderId'] == NULL)
      $keyFields = NULL;
    else
      $keyFields = Array('folderId' => $this->fields['folderId']);

    $defValues = Array('userRight'=>'rw','groupRight'=>'rw','otherRight'=>'r');
    $folder->GetForm($keyFields, $this->fields['fieldName'],
		     $this->fields['sourceId'], $templates, 
		     $formObject,$defValues,$this->fields['dateFormat']);

    $this->children[] = & $formObject;
  }
}

?>
