<?php

require_once('ADMIN_Popup.inc');

class ADMIN_FileForm extends ADMIN_Popup
{
  protected function LoadPopup()
  {
    $_GET['labelId'] = 'adminPageTag';
    $_GET['lang'] = $this->lang;
    $windowTitleLabel = &$this->LoadModule('LANG_GetLabel');
    $windowTitle = htmlentities_utf($windowTitleLabel->attrs['value']).' '.
      $_REQUEST['docId'];
    $this->pageBuilder->rootTemplate->addText($windowTitle, 'windowTitle');

    $fileFormTpl = $this->pageBuilder->loadTemplate('adminFileForm');
    $path = htmlentities_utf(stripslashes($_REQUEST['path']));
    $fileFormTpl->addText((string)$path, 'path');

    $fileFormTpl->addText((string)$_REQUEST['updateImage'], 'updateImage');
    $fileFormTpl->addText((string)$_REQUEST['imageListField'], 'imageListField');

   $this->pageBuilder->rootTemplate->addTemplate($fileFormTpl, 'content');

    switch($_REQUEST['tab'])
    {
    case 'adminContent':
      $_GET['path'] = '/content' . $_REQUEST['path'];
      break;
    case 'adminLib':
      $_GET['path'] = '/library' . $_REQUEST['path'];
      break;
    }

    try
    {
      $getDoc = $this->LoadModule('LIB_GetDoc');
      $getDoc->PrintHTML($fileFormTpl);
      $this->script .= "InitiateFileContent('".$this->lang."');";
    }
    catch(Exception $e)
    {
      //throw $e;
      $this->script .= "alert('".$e->getMessage()."');";
      $this->script .= "close();";
    }

  }
}

?>
