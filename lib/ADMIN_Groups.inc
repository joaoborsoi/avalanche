<?php

require_once('ADMIN_Page.inc');


class ADMIN_Groups extends ADMIN_Page
{
  protected function LoadPage()
  {    
    $this->pageBuilder->rootTemplate('adminUsers');
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');

    $_GET['labelId'] = 'groupsUMTag';
    $_GET['lang'] = $this->lang;
    $title = &$this->LoadModule('LANG_GetLabel');
    $this->pageBuilder->rootTemplate->addText($title->attrs['value'], 'title');

    $script .= $this->LoadGroups();
    $this->pageBuilder->rootTemplate->addText((string)$script,'onLoad');
  }

  protected function LoadGroups()
  {
    // print folders
    $get = $this->LoadModule('UM_GetAllGroups','adminUserGroup',
			     'adminUserNode');

    $get->PrintHTML($this->pageBuilder->rootTemplate,NULL,
    		    'htmlentities_utf');
    return 'parent.OnLoadGroups('.$get->children[0]->attrs['groupId'].');';
  }

}
?>