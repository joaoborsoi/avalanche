<?php

require_once("AV_DBOperationObj.inc");
require_once("CHAT_Manager.inc");

class CHAT_GetBuffer extends AV_DBOperationObj
{
 
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'roomId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;    
  }

  function Execute()
  {   
     $sessionId = $this->pageBuilder->sessionH->sessionID;
     $this->isArray = true;
     $maganer = new  CHAT_Manager($this); 
     $maganer->LoadMessages();
     $rows = $maganer->GetBuffer($this->fieldParser->fields['roomId'],
                                 $sessionId,
                                 $status);   

   if($status != SUCCESS) 
      return $status;

   $this->sizeVarName = 'numMessages';
   
   $this->LoadChildrenFromRows($rows,"history");
  
   return SUCCESS;
  }
}

?>