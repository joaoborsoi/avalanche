<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Labels.inc');
require_once('LEGIS_Process.inc');

class LEGIS_GenerateLabels extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'params';
    $this->fieldsDef[0]['consistType'] = 'objectList';
    $this->fieldsDef[0]['stripSlashes'] = true; 
    $this->fieldsDef[0]['trimField'] = false; 
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $process = new LEGIS_Process($this);
    $memberAggregator = $process->GetMemberAggregator();

    if(count($memberAggregator) > 0)
    {
      $this->fields['params'][] = Array('param'=>'aggregator',
					'aggregatorId'=>$memberAggregator['docId']);
    }
    $labels = new LEGIS_Labels($this);
    $this->attrs['docId'] = $labels->Generate($this->fields['params']);
  }
}

?>