<?php

require_once('AV_DBDriver.inc');

class AV_ODBCDriver extends AV_DBDriver
{
  protected $sqlserv;

  function __construct($pageBuilder)
  {
    parent::__construct($pageBuilder);

    $dsn = $pageBuilder->siteConfig->getVar('db','dsn');
    $user = $pageBuilder->siteConfig->getVar('db','userName');
    $password = $pageBuilder->siteConfig->getVar('db','passWord');
    $this->sqlserv = odbc_connect($dsn, $user, $password);
    if($this->sqlserv === false)
      throw new AV_Exception(FAILED,$this->lang);

    odbc_close($sqlserv);
  }
 

  function ExecSQL($SQL, $reportError = true)
  {
    if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $res = odbc_exec($this->sqlserv, $SQL);
    if($res === false) 
      throw new AV_Exception(FAILED,$this->lang);

    odbc_free_result($res);
  }

  function GetAll($SQL, $reportError = true)
  {
    if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $res = odbc_exec($this->sqlserv, $SQL);
    if($res === false) 
      throw new AV_Exception(FAILED,$this->lang);

    $rows = array();
    $i = 0;
    $fCount = odbc_num_fields($res);
    while (odbc_fetch_row($res))
    {
      for($col = 1; $col <= $fCount; $col++)
      {
	$fName = odbc_field_name($res, $col);
	$rows[$i][$fName] = odbc_result($res, $col);
      }
      $i++;
    }

    odbc_free_result($res);
    return $rows;
  }


  function GetOne($SQL, & $status, $reportError = true)
  {
    if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $rows = $this->GetAll($SQL, $reportError);

    return each($rows[0])[1];
  }

}

?>

