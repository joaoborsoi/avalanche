<?php

require_once('FORM_DynTable.inc');
require_once('LIB_Folder.inc');
require_once('CMS_RSSEvents.inc');

class CMS_RssPrefs extends FORM_DynTable
{
  function Set($fields)
  {
    parent::Set($fields);

    // erases previous content
    $SQL = "DELETE FROM CMS_RSSFolder WHERE userId='".$fields['userId']."' ";
    $this->dbDriver->ExecSQL($SQL);

    if(count($fields['prefs'])>0) 
    {
      $SQL = 'INSERT INTO CMS_RSSFolder (userId,folderId) VALUES ';
      foreach($fields['prefs'] as $key => & $folder)
      {
	if($key>0)
	  $SQL .= ',';
	$SQL .= "('".$fields['userId']."','".$folder['folderId']."')";
      }

      $this->dbDriver->ExecSQL($SQL);
    }

    return $this->oldValues;    
  }


  function GetNotificationAddresses($docId,$event=RSS_NEW)
  {
    switch($event)
    {
    case RSS_NEW: case RSS_UPDATES: case RSS_COMMENTS:
      break;
    default:
      throw new AV_Exception(INVALID_FIELD, $this->lang, 
			     Array('fieldName'=>'event'));
    }

    $SQL = 'SELECT DISTINCT login ';
    $SQL .= 'FROM LIB_Node n, LIB_DocumentLink dl, LIB_Folder df,';
    $SQL .= 'LIB_Folder f, CMS_RSSFolder rf, CMS_RSSPrefs r';
    if($event != RSS_NEW)
      $SQL .= ', CMS_RSSPrefsEvents re';
    $SQL .= ', UM_User u ';

    $SQL .= "WHERE nodeId='$docId' AND nodeId=dl.docId ";
    $SQL .= 'AND dl.folderId=df.folderId ';
    $SQL .= 'AND SUBSTR(df.path,1,CHAR_LENGTH(f.path))=f.path ';
    $SQL .= 'AND df.level >= f.level AND f.folderId=rf.folderId ';
    $SQL .= "AND rf.userId=r.userId AND r.mail='1' ";
    if($event != RSS_NEW)
      $SQL .= "AND rf.userId=re.userId AND re.id='$event' ";
    $SQL .= "AND rf.userId=u.userId AND status='1' ";
    $SQL .= "AND IF(n.userId=u.userId,INSTR(n.userRight,'r')<>0,";
    $SQL .= "IF(n.groupId IN ";
    $SQL .= '(SELECT groupId FROM UM_UserGroup ug WHERE ug.userId=u.userId),';
    $SQL .= "INSTR(n.groupRight,'r')<>0,";
    $SQL .= "INSTR(n.otherRight,'r')<>0)) ";

    return $this->dbDriver->GetCol($SQL);
  }

  protected function &GetField(& $fieldDef, & $values, $keyFields,  
			       & $templates, $sourceId, $dateFormat = NULL)
  {
    switch ($fieldDef['type'])
    {
    case "rssPrefs":
      return $this->GetRssTreeField($fieldDef, $keyFields, $values);
    default:
      return parent::GetField($fieldDef,$values,$keyFields,  
			      $templates,$sourceId,$dateFormat);
    }
  }
  protected function GetRssTreeField(&$fieldDef,&$keyFields,
				     $values)
  {
    $folderObj = new LIB_Folder($this->module);

    $content = "<p id='".$fieldDef['fieldName']."Field' class='rssTreeField'>";
    $content .= $fieldDef['label'].":</p>\n";

    $SQL = 'SELECT path FROM CMS_RSSFolder r,LIB_Folder f ';
    $SQL .= "WHERE userId='".$keyFields['userId']."' ";
    $SQL .= 'AND r.folderId=f.folderId ';
    $folders = $this->dbDriver->GetCol($SQL);

    // checks for submenu and completes topPath
    $rssPath = $this->pageBuilder->siteConfig->getVar('cms','rssPath');
    if($rssPath[0]=='/')
      $rssPath = substr($rssPath,1);
    $currPath = '/content/Menu/'.$rssPath.'/';
    $checked = in_array($currPath,$folders);
    
    $folder = $folderObj->GetFolderData($currPath);
    $langLabel = new LANG_Label($this->module);
    $folder['menuTitle'] = $langLabel->GetLabel($folder['menuTitle'],
						$this->lang);
    $content .= "<div class='box'>\n";
    $content .= "<ul class='inner' id='tree'>\n";
    $content .= "<li>";
    $content .= "<label for='rssInput".$folder['folderId']."'>";
    $content .= "<input id='rssInput".$folder['folderId']."' ";
    $content .= "type='checkbox' value='".$folder['folderId']."' ";

    // check if necessary
    if($checked)
      $content .= "checked='checked' ";
    $content .= "onclick='SetRSSFolder(".$folder['folderId'].")' />";
    $content .= $folder['menuTitle'] .'</label>';

    if($this->LoadRssTree($folderObj,$rssPath,$tree,$folders,$checked))
    {
      $currPath = '/content/Menu/'. addslashes($rssPath);


      $content .= "<ul id='rssUl".$folder['folderId']."'>$tree</ul>";
    }
    $content .= "</li>\n</ul>\n</div>\n";

    $modobj = new AV_ModObject('field', $this->module->pageBuilder);
    $modobj->attrs = $fieldDef;
    $modobj->attrs['content'] = $content;
    $modobj->attrs['value'] = $folders;    
    return $modobj;
  }

  protected function LoadRssTree(&$folderObj,$nodeItem,&$tree,&$rssPrefs,
				 $pChecked=false)
  {
    $folderList = $folderObj->GetFolderList('/content/Menu/'.
					    addslashes($nodeItem),true);

    if(count($folderList)==0)
      return false;

    foreach($folderList as & $folder)
    {
      $tree .= "<li>";
      $tree .= "<label for='rssInput".$folder['folderId']."'>";
      $tree .= "<input id='rssInput".$folder['folderId']."' ";
      $tree .= "type='checkbox' value='".$folder['folderId']."' ";

      $currPath = '/content/Menu/'.$nodeItem.'/'.$folder['name'].'/';
      // if parent is checked, should disable field
      if($pChecked)
	$tree .= "checked='checked' disabled='disabled' ";
      // check if necessary
      else if(in_array($currPath,$rssPrefs))
      {
	$tree .= "checked='checked' ";
	$checked = true;
      }
      else
	$checked = false;

      $tree .= "onclick='SetRSSFolder(".$folder['folderId'].")' />";
      $tree .= $folder['menuTitle'] .'</label>';
      
      // loads subnodes
      $branch = '';
      if($this->LoadRssTree($folderObj,$nodeItem.'/'.$folder['name'],$branch,
			    $rssPrefs,($checked || $pChecked)))
	$tree .= "<ul id='rssUl".$folder['folderId']."'>$branch</ul>";

      $tree .= '</li>';
    }    
    return true;
  }
}

?>