<?php

require_once('AV_OperationObj.inc');

//--Class: L2H_GetContentsTpl
//--Parent: AV_OperationObj
//--Desc: 
class L2H_GetContentsTpl extends AV_OperationObj
{
  //
  //--Public
  //

  //--Method: LoadFieldsDef
  //--Desc: Redefines parent method to define module's input fields
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'fileName';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $fileName = $this->fieldParser->fields['fileName'];
    $this->template = $fileName . '_ct.html';
    return SUCCESS;
  }
}

?>