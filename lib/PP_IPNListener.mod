<?php

require_once('AV_DBOperationObj.inc');

/**
 * Observador de Notificações de Pagamento Instantâneo
 */
class PP_IPNListener extends AV_DBOperationObj
{
  /**
   * @var string
   */
  private $endpoint;
 
  /**
   * @var IPNHandler
   */
  private $ipnHandler;
 
  /**
   * Constroi o objeto que receberá as notificações de pagamento
   * instantâneas do PayPal..
   * @param   boolean $sandbox Define se será utilizado o Sandbox
   * @throws  InvalidArgumentException
   */
  function __construct(&$pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
		       $jsonInput=false, $transaction = true)
  {
    $this->endpoint = $pbuilder->siteConfig->getVar('paypal','endpoint').
      '?cmd=_notify-validate';


    parent::__construct($pbuilder, $itemTpl, $itemTplTag, $varPrefix, $jsonInput, 
			$transaction);
  }
 
  /**
   * Define o objeto que irá manipular as notificações de pagamento
   * instantâneas enviadas pelo PayPal.
   * @param   IPNHandler $ipnHandler
   */
  public function SetIPNHandler() 
  {
    $handlerName = $this->pageBuilder->siteConfig->getVar('paypal','handler');
    require_once($handlerName.'.inc');
    $this->ipnHandler = new $handlerName($this);
  }
 
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'receiver_email';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'txn_type';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['stripSlashes'] = false;

    $this->fieldsDef[2]['fieldName'] = 'period1';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = false;

    $this->fieldsDef[3]['fieldName'] = 'period2';
    $this->fieldsDef[3]['required'] = false;
    $this->fieldsDef[3]['allowNull'] = true;
    $this->fieldsDef[3]['stripSlashes'] = false;

    $this->fieldsDef[4]['fieldName'] = 'period3';
    $this->fieldsDef[4]['required'] = false;
    $this->fieldsDef[4]['allowNull'] = true;
    $this->fieldsDef[4]['stripSlashes'] = false;

    $this->fieldsDef[5]['fieldName'] = 'mc_currency';
    $this->fieldsDef[5]['required'] = false;
    $this->fieldsDef[5]['allowNull'] = true;
    $this->fieldsDef[5]['stripSlashes'] = false;

    $this->fieldsDef[6]['fieldName'] = 'mc_amount1';
    $this->fieldsDef[6]['required'] = false;
    $this->fieldsDef[6]['allowNull'] = true;
    $this->fieldsDef[6]['consistType'] = 'float';

    $this->fieldsDef[7]['fieldName'] = 'mc_amount2';
    $this->fieldsDef[7]['required'] = false;
    $this->fieldsDef[7]['allowNull'] = true;
    $this->fieldsDef[7]['stripSlashes'] = false;
    $this->fieldsDef[7]['consistType'] = 'float';

    $this->fieldsDef[8]['fieldName'] = 'mc_amount3';
    $this->fieldsDef[8]['required'] = false;
    $this->fieldsDef[8]['allowNull'] = true;
    $this->fieldsDef[8]['stripSlashes'] = false;
    $this->fieldsDef[8]['consistType'] = 'float';

    $this->fieldsDef[9]['fieldName'] = 'subscr_id';
    $this->fieldsDef[9]['required'] = false;
    $this->fieldsDef[9]['allowNull'] = true;
    $this->fieldsDef[9]['stripSlashes'] = false;

    $this->fieldsDef[10]['fieldName'] = 'subscr_date';
    $this->fieldsDef[10]['required'] = false;
    $this->fieldsDef[10]['allowNull'] = true;
    $this->fieldsDef[10]['stripSlashes'] = false;

    $this->fieldsDef[11]['fieldName'] = 'txn_id';
    $this->fieldsDef[11]['required'] = false;
    $this->fieldsDef[11]['allowNull'] = true;
    $this->fieldsDef[11]['stripSlashes'] = false;

    $this->fieldsDef[12]['fieldName'] = 'custom';
    $this->fieldsDef[12]['required'] = false;
    $this->fieldsDef[12]['allowNull'] = true;
    $this->fieldsDef[12]['stripSlashes'] = false;

    $this->fieldsDef[13]['fieldName'] = 'payment_status';
    $this->fieldsDef[13]['required'] = false;
    $this->fieldsDef[13]['allowNull'] = true;
    $this->fieldsDef[13]['stripSlashes'] = false;

    $this->fieldsDef[14]['fieldName'] = 'payment_date';
    $this->fieldsDef[14]['required'] = false;
    $this->fieldsDef[14]['allowNull'] = true;
    $this->fieldsDef[14]['stripSlashes'] = false;

    $this->fieldsDef[15]['fieldName'] = 'mc_gross';
    $this->fieldsDef[15]['required'] = false;
    $this->fieldsDef[15]['allowNull'] = true;
    $this->fieldsDef[15]['stripSlashes'] = false;
  }

  /**
   * Aguarda por notificações de pagamento instantânea; Caso uma nova
   * notificação seja recebida, faz a verificação e notifica um manipulador
   * com o status (verificada ou não) e a mensagem recebida.
   * @see     InstantPaymentNotification::setIPNHandler()
   * @throws  BadMethodCallException Caso o método seja chamado antes
   * de um manipulador ter sido definido ou nenhum email de recebedor
   * tenha sido informado.
   */
  protected function Execute()
  {
    $this->SetIPNHandler();

    try
    {
      if ( $this->ipnHandler !== null ) 
      {
	if ( $_SERVER[ 'REQUEST_METHOD' ] == 'POST' ) 
	{
	  if ( filter_input( INPUT_POST , 'receiver_email' , FILTER_VALIDATE_EMAIL ) ) 
	  {
	    // log every received message
	    $log = "IPN Message:\n".print_r($_POST,true);
	    $this->PrintLog($log);

	    $curl = curl_init();
 
	    curl_setopt( $curl , CURLOPT_URL , $this->endpoint );
	    curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false );
	    curl_setopt( $curl , CURLOPT_RETURNTRANSFER , 1 );
	    curl_setopt( $curl , CURLOPT_POST , 1 );
	    curl_setopt( $curl , CURLOPT_POSTFIELDS , http_build_query( $_POST ) );
	  
	    $response = curl_exec( $curl );
	    $error = curl_error( $curl );
	    $errno = curl_errno( $curl );
 
	    curl_close( $curl );
	    
	    if(!(empty( $error ) && $errno == 0)) 
	      throw new Exception("Falha de conexão com o paypal (error: $error, errno: $errno)");

	    $this->PrintLog("IPN Validate Response: $response");

	    $this->fields['isVerified'] = (trim($response) == 'VERIFIED');
	    //	    $this->fields['response'] = $response;

	    $this->ipnHandler->Handle($this->fields);
	  }
	  else
	    throw new Exception('Invalid receiver_email');
	}
	else
	  throw new Exception('Método POST requerido');
      } 
      else 
      {
	throw new BadMethodCallException( 'Nenhum manipulador de mensagem foi definido' );
      }
    }
    catch(Exception $e)
    {
      $this->PrintLog($e->getMessage(),true);
      throw $e;
    }
  }
}

?>