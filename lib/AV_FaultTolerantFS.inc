<?php

define('ft_tries',3);
define('ft_sleep',3);

function ft_rename($oldname,$newname) 
{
  $i=0;
  while(true)
  {
    if(rename($oldname,$newname))
      return true;

    if($i==ft_tries-1)
      return false;

    sleep(ft_sleep);
    $i++;
  }
}

function ft_file_get_contents($filename)
{
  $i = 0;
  while(true)
  {
    $content = file_get_contents($filename);
    if($content!==false)
      return $content;

    if($i==ft_tries-1)
      return false;

    sleep(ft_sleep);
    $i++;
  }
}

function ft_file_put_contents($filename,$data) 
{
  $i=0;
  while(true)
  {
    if(file_put_contents($filename,$data))
      return true;

    if($i==ft_tries-1)
      return false;

    sleep(ft_sleep);
    $i++;
  }
}

function ft_unlink($filename)
{
  $i=0;
  while(true)
  {
    if(unlink($filename))
      return true;

    if($i==ft_tries-1)
      return false;

    sleep(ft_sleep);
    $i++;
  }
}


?>