<?php

// $SM_rootDir = '/var/www/user/siteManager/';

$SM_rootDir = '';   // when set, make sure it ends with a slash!

// -----------------------------------------------------------------------------
// don't touch below here
// -----------------------------------------------------------------------------

// Version
$SM_versionTag = "2.2.0 production";
define('SM_VERSION', '2.2.0');

// turn on all warnings and notices
error_reporting(E_ALL);

// catch all output
ob_start();

// script start time
$microtime = explode(' ', microtime());
$SM_scriptStartTime = $microtime[1].mb_substr( $microtime[0], 1);

require($SM_rootDir."lib/errors.inc");
require($SM_rootDir."lib/smObject.inc");
require($SM_rootDir."lib/smConfig.inc");
require($SM_rootDir."lib/smRoot.inc");
require("AV_ModObject.inc");
require("AV_Module.inc");
require("AV_DBPageBuilder.inc");
require($SM_rootDir."lib/dataManip.inc");
require($SM_rootDir."lib/support.inc");
require($SM_rootDir."lib/security.inc");
require($SM_rootDir."lib/auth.inc");
require($SM_rootDir."lib/smartForm.inc");
require($SM_rootDir."lib/layoutTemplate.inc");
require($SM_rootDir."lib/modules.inc");
require($SM_rootDir."lib/smMembers.inc");
require($SM_rootDir."lib/sessions.inc");
require($SM_rootDir."lib/codePlate.inc");


// ROOT SITEMANAGER CLASS
$SM_siteManager = new AV_DBPageBuilder();

if ($SM_develState)
    SM_debugLog("Initializing SiteManager...");

?>
