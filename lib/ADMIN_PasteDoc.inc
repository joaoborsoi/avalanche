<?php

require_once('ADMIN_Files.inc');

class ADMIN_PasteDoc extends ADMIN_Files
{

  function LoadFiles($path)
  {
    try
    {
      if($_REQUEST['cutNode'] == '1')
      {
	$_GET['path'] = $this->rootPath . $path;
	$copyModule = $this->LoadModule('LIB_InsDocLink');

	$_GET['path'] = $this->rootPath . $_REQUEST['copyPath'];
	$delModule = $this->LoadModule('LIB_DelDocLink');
      }
      else
      {
	$_GET['path'] = $this->rootPath . $path;
	$copyModule = $this->LoadModule('LIB_CopyDoc');
      }

      $script = 'parent.OnPaste();';
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }    

    $script .= parent::LoadFiles($path);
    return $script;
  }
}

?>