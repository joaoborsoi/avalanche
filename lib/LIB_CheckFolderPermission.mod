<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


class LIB_CheckFolderPermission extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'path';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['default'] = '/';
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'write';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['default'] = false;
    $this->fieldsDef[1]['consistType'] = 'boolean';
  }

  protected function Execute()
  {
    $folder = new LIB_Folder($this);
    $folder->CheckFolderPermission($this->fields['path'],
				   $this->fields['write']);
  }
}

?>
