<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_GetAllGroups extends AV_DBOperationObj
{
  protected function Execute()
  {
    $group = new UM_Node('UM_Group', $this);

    $this->isArray = true;
    // get rows
    $rows = $group->GetAll('groupName');

    $this->LoadChildrenFromRows($rows, 'user');
  }
}

?>

