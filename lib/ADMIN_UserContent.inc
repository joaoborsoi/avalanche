<?php

require_once('ADMIN_Content.inc');

class ADMIN_UserContent extends ADMIN_Content
{
  protected function LoadTabContent(& $contentTpl)
  {
    $userContentTpl = $this->pageBuilder->loadTemplate('adminUserContent');
    $contentTpl->addTemplate($userContentTpl, 'tabContent');
  }

  protected function LoadFooter(& $contentTpl)
  {
    $footerTpl = $this->pageBuilder->loadTemplate('adminUserFooter');
    $this->pageLabels->PrintHTML($footerTpl, NULL, 'htmlentities_utf');
    $contentTpl->addTemplate($footerTpl, 'footer');
  }
}

?>