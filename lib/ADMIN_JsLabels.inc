<?php
require_once('AV_Page.inc');

class ADMIN_JsLabels extends AV_Page
{
  protected function Header()
  {
    // by default, disable cache
    $this->DisableCache();
    header('Content-Type: text/javascript');
  }

  protected function LoadPage()
  {
    $tpl = & $this->pageBuilder->rootTemplate('adminScriptSrc');

    $pageLabels = $this->LoadModule('LANG_GetPageLabels');

    foreach($pageLabels->attrs as $label=>$value)
      $script .= "$label=\"$value\";\n";
    $tpl->addText((string)$script,'script'); 
  }
  
}

?>