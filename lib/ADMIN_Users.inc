<?php

require_once('ADMIN_Page.inc');


class ADMIN_Users extends ADMIN_Page
{
  protected function LoadPage()
  {    
    $this->pageBuilder->rootTemplate('adminUsers');
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');

    $pageLabels = &$this->LoadModule('LANG_GetPageLabels');

    $filterTpl = $this->pageBuilder->loadTemplate('adminUsersFilter');
    $filter = htmlentities_utf(stripslashes($_REQUEST['filter']));
    $filterTpl->addText((string)$filter,'filter');
    $pageLabels->PrintHTML($filterTpl);
    $this->pageBuilder->rootTemplate->addTemplate($filterTpl, 'filter');

    $_GET['labelId'] = 'adminUsersTag';
    $_GET['lang'] = $this->lang;
    $title = &$this->LoadModule('LANG_GetLabel');
    $this->pageBuilder->rootTemplate->addText($title->attrs['value'], 'title');

    $script = "parent.OnLoadUsers();";
    $script .= $this->LoadUsers();
    $this->pageBuilder->rootTemplate->addText((string)$script,'onLoad');
  }

  protected function LoadUsers()
  {
    // print folders
    $get = $this->LoadModule('UM_SearchUsers', 'adminUser', 'adminUserNode');

    $get->PrintHTML($this->pageBuilder->rootTemplate,NULL,'htmlentities_utf');
  }

}
?>