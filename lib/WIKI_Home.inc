<?php

require_once('CMS_Generic.inc');
require_once('WIKI_Content.inc');

class WIKI_Home extends CMS_Generic
{
  protected $wikiContent;

  function LoadArea(&$fields,$path)
  {
    CMS_Content::LoadArea($fields,$path);

    $this->wikiContent = new WIKI_Content($this);
    try
    {
      // for everyone show lates
      $content = $this->LoadLatest();

      // for moderators show pending approval list
      if(in_array('Moderators',$this->memberGroups))
	$content .= $this->LoadPendingApproval();
    
      // for contributors shows my artiels
      if(in_array('Contributors',$this->memberGroups))
	$content .= $this->LoadMyArticles();
    }
    catch(Exception $e)
    {
    }

    // subareas
    $content .= $this->page->LoadSubareas();

    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }

    
  protected function LoadPendingApproval()
  {
    // searchs database for pending approval documents from all users
    $_GET['path'] = '/content/Menu/';
    $_GET['orderBy'] = 'lastChanged';
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
								 'dateFormat');
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = 'authors,lastChanged,title';
    $_GET['exclusionFilters_numOfOptions'] = 3;
    $_GET['exclusionFilters_1__fieldName'] = 'contentType';
    $_GET['exclusionFilters_1__value'] = 'article/html';
    $_GET['exclusionFilters_2__fieldName'] = 'pendingApproval';
    $_GET['exclusionFilters_2__value'] = '1';
    $_GET['exclusionFilters_3__fieldName'] = 'moderator';
    $_GET['exclusionFilters_3__value'] = NULL;
    $_GET['limit'] = NULL;
    $pendingApproval = $this->page->LoadModule('LIB_Search');
    if($pendingApproval->attrs['totalResults'] > 0)
    {
      // loads list to screen
      $content = "<h2 class='subsection'>";
      $content .= $this->pageLabels->attrs['pendingApprovalTag']."</h2>\n";
      $content .= $this->wikiContent->LoadArticleList($pendingApproval);
    }

    $_GET['exclusionFilters_3__fieldName'] = 'moderator';
    $_GET['exclusionFilters_3__value'] = $this->memberData['userId'];
    $myModerations = $this->page->LoadModule('LIB_Search');
    if($myModerations->attrs['totalResults'] > 0)
    {
      // loads list to screen
      $content .= "<h2 class='subsection'>";
      $content .= $this->pageLabels->attrs['myModerationsTag']."</h2>\n";
      $content .= $this->wikiContent->LoadArticleList($myModerations);
    }

    return $content;
  }

  protected function LoadMyArticles()
  {
    // searchs database for pending approval documents from the user
    $_GET['path'] = '/content/Menu/';
    $_GET['orderBy'] = 'lastChanged';
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
								 'dateFormat');
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = 'authors,lastChanged,title';
    $_GET['exclusionFilters_numOfOptions'] = 3;
    $_GET['exclusionFilters_1__fieldName'] = 'contentType';
    $_GET['exclusionFilters_1__value'] = 'article/html';
    $_GET['exclusionFilters_2__fieldName'] = 'pendingApproval';
    $_GET['exclusionFilters_2__value'] = '1';
    $_GET['exclusionFilters_3__fieldName'] = 'userId';
    $_GET['exclusionFilters_3__value'] = $this->memberData['login'];
    $_GET['limit'] = NULL;
    $search = $this->page->LoadModule('LIB_Search');
    if($search->attrs['totalResults'] > 0)
    {
      // loads list to screen
      $content .= "<h3 class='subsubsection'>";
      $content .= $this->pageLabels->attrs['pendingApprovalTag']."</h3>\n";
      $content .= $this->wikiContent->LoadArticleList($search,'lastChanged');
    }

    // searchs database for published articles
    $_GET['orderBy'] = 'lastChanged';
    $_GET['exclusionFilters_numOfOptions'] = 4;
    $_GET['exclusionFilters_2__value'] = '0';
    $_GET['exclusionFilters_4__fieldName'] = 'otherRight';
    $_GET['exclusionFilters_4__value'] = '';
    $search = $this->page->LoadModule('LIB_Search');
    if($search->attrs['totalResults'] > 0)
    {
      // loads list to screen
      $content .= "<h3 class='subsubsection'>";
      $content .= $this->pageLabels->attrs['notPublishedTag']."</h3>\n";
      $content .= $this->wikiContent->LoadArticleList($search);
    }


    // searchs database for unpublished articles
    $_GET['orderBy'] = 'lastChanged';
    $_GET['exclusionFilters_4__fieldName'] = 'otherRight';
    $_GET['exclusionFilters_4__value'] = 'r';
    $search = $this->page->LoadModule('LIB_Search');
    if($search->attrs['totalResults'] > 0)
    {
      // loads list to screen
      $content .= "<h3 class='subsubsection'>";
      $content .= $this->pageLabels->attrs['publishedTag']."</h3>\n";
      $content .= $this->wikiContent->LoadArticleList($search);
    }

    // prints content
    if($content != NULL)
    {
      $content = "<h2 class='subsection'>".
	$this->pageLabels->attrs['myWikiTag']."</h2>\n".$content;
    }
    return $content;
  }

  protected function LoadLatest()
  {
    // searchs database for latest articles
    $_GET['path'] = '/content/Menu/';
    $_GET['orderBy'] = 'lastChanged';
    $_GET['order'] = '0';
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
								 'dateFormat');
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = 'authors,lastChanged,title';
    $_GET['exclusionFilters_numOfOptions'] = 2;
    $_GET['exclusionFilters_1__fieldName'] = 'contentType';
    $_GET['exclusionFilters_1__value'] = 'article/html';
    $_GET['exclusionFilters_2__fieldName'] = 'otherRight';
    $_GET['exclusionFilters_2__value'] = 'r';
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsLatest');
    $search = $this->page->LoadModule('LIB_Search');
    if($search->attrs['totalResults'] > 0)
    {
      // loads list to screen
      $content = "<h2 class='subsection'>";
      $content .= $this->pageLabels->attrs['latestTag']."</h2>\n";
      $content .= $this->wikiContent->LoadArticleList($search);
    }
    return $content;
  }
}

?>