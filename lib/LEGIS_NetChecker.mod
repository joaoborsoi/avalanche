<?php

require_once('AV_DBOperationObj.inc');
require_once('AV_NetChecker.inc');
require_once('UM_User.inc');

class LEGIS_NetChecker extends AV_DBOperationObj
{ 
  function Execute()
  {
    $netChecker = new AV_NetChecker($this->pageBuilder);
    
    if($netChecker->IsValidClient())
      return;

    $userMan = new UM_User($this);
   
    $rows = $userMan->GetMemberGroups();

    foreach($rows as $row)
    {
      if($row['groupId']==23)
	return;
    }

    throw new AV_Exception(PERMISSION_DENIED,$this->lang);
  } 
}

?>