<?php

require_once('AV_DBOperationObj.inc');
require_once('IM_Manager.inc');

class IM_SendMessage extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'talkId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'message';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['stripSlashes'] = false;
  }

  protected function Execute()
  {
    $im = new IM_Manager($this);

    // get rows
    $im->SendMessage($this->fields);
  }
}

?>