<?php

require_once('GAME_Element.inc');

class GAME_Quiz extends GAME_Element
{
  function LoadArea(AV_Module &$loadArea)
  {
    $loadQuiz = $this->page->LoadModule('GAME_LoadQuiz','gameQuizOption','quizOptions');
    
    $this->pageBuilder->rootTemplate($loadArea->attrs['type']);
    $loadArea->PrintHTML($this->pageBuilder->rootTemplate);

    // prevents double printing id
    unset($loadQuiz->attrs['id']);

    $loadQuiz->PrintHTML($this->pageBuilder->rootTemplate);

    if($loadQuiz->attrs['answered'])
    {
      $submit = "<a class='btn btn-large voltar' onclick='game.CloseQuiz($(this));'><strong>Voltar</strong></a>";
      $this->pageBuilder->rootTemplate->addText($submit,'button');
    }
    else
    {
      $submit = "<input class='btn btn-large' type='submit' value='OK' />";
      $this->pageBuilder->rootTemplate->addText($submit,'button');
      $this->pageBuilder->rootTemplate->addText("action='gameQuiz.av'",'action');
    }
  }
}

?>
