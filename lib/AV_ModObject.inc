<?php

//--Class: AV_ModObject
//--Desc: Represents module's output object. The output object is essecially
//--Desc: composed of attributes and children objects. This hierarchy is build
//--Desc: on module's creation to represent output status variables. This class
//--Desc: implements different output format (currently HTML, XML and MIME).
class AV_ModObject
{
  public $name;            //--Desc: Object name
  public $attrs;           //--Desc: Object attributes array
  public $children;        //--Desc: Children objects

  public $pageBuilder;  //--Desc: Module wich contains the object

  public $isArray;            //--Desc: Flag to indicate weather to print index 
                           //--Desc: numbers on children objects (arrays)
  protected $itemTpl;            //--Desc: Item template name
  protected $itemTplTag;         //--Desc: Item template tag
  protected $sizeVarName;        //--Desc: Indicates size variable name (default 
                           //--Desc: is 'size').
  protected $lang;



  //
  //--Public
  //

  //--Method: AV_ModObject
  //--Desc: Constructor
  function __construct($name, & $pageBuilder,  
		       $itemTpl = '', $itemTplTag = '')
  {
    $this->attrs = array();
    $this->children = array();
    $this->name = $name;
    $this->isArray = false;
    $this->pageBuilder = & $pageBuilder;
    $this->itemTpl = $itemTpl;
    $this->itemTplTag = $itemTplTag;
    $this->sizeVarName = 'size';
    $this->lang = $this->pageBuilder->lang;
  }

  function copy()
  {
    $serialized_contents = serialize($this);
    return unserialize($serialized_contents);
  }

  //--Method: LoadChildrenFromRows
  //--Desc: Loads object children based on bidimensional array.
  function LoadChildrenFromRows(& $rows, $rowName, $sizeVarName = NULL)
  {
    $i = count($this->children);

    foreach($rows as $key => $row)
    {
      $this->children[$i] = new AV_ModObject($rowName, $this->pageBuilder);
      $this->children[$i]->attrs = &$rows[$key];
      $this->children[$i]->attrs['fieldIndex'] = $i+1;
      $this->children[$i]->attrs['lang'] = $this->pageBuilder->lang;
      $this->children[$i]->attrs['avVersion'] = $this->pageBuilder->avVersion;
      $i++;
    }

    if($sizeVarName !== NULL)
      $this->attrs[$sizeVarName] = $i;
  }

  //--Method: PrintHTML
  //--Desc: Prints object in HTML format
  function PrintHTML(&$template, $prefix = '', $outputFuncsRef = NULL)
  {
    if($this->isArray)
      $this->attrs[$this->sizeVarName] = count($this->children);
    $this->PrintHTMLAttrs($template, $prefix, $outputFuncsRef);
    $this->PrintHTMLChildren($template, $prefix, $outputFuncsRef);
  }

  //--Method: PrintMIME
  //--Desc: Prints object in MIME standard format
  function PrintMIME($prefix = '')
  {
    if($this->isArray)
      $this->attrs[$this->sizeVarName] = count($this->children);

    $output = $this->PrintMIMEAttrs($prefix);
    $aux = $this->PrintMIMEChildren($prefix);

    // joins attributes and children output
    if($output != '')
    {
      if($aux != '')
	$output .= '&'. $aux;
    }
    else
      $output = $aux;
	

    return $output;
  }

  //--Method: PrintXML
  //--Desc: Prints object in XML format
  function PrintXML($tabs = 0)
  {
    if($this->isArray)
      $this->attrs[$this->sizeVarName] = count($this->children);

    // gets children output first
    $children = $this->PrintXMLChildren($tabs + 1);

    // starts printing output
    $output = str_repeat("\t", $tabs);
    $output .= '<' . $this->name . $this->PrintXMLAttrs();

    if($children == '')
      // if there was no child, closes tag
      $output .= "/>\n";
    else
    {
      // if there are children, print them and closes tags after
      $output .= ">\n" . $children;
      $output .= str_repeat("\t", $tabs) . '</' . $this->name . ">\n";
    }

    return $output;
  }




  //--Method: PrintHTMLAttrs
  //--Desc: Prints object's attributes in HTML format
  protected function PrintHTMLAttrs(&$template, $prefix = '', $outputFuncsRef)
  {
    if($outputFuncsRef!=NULL)
    {
      $outputFuncsRef = explode(',',$outputFuncsRef);
      $out1 = implode('(',$outputFuncsRef) . '(';
      $out2 = str_repeat(')', count($outputFuncsRef)).';';
    }

    foreach($this->attrs as $key => $attr)
    {
      if($outputFuncsRef != NULL)
	eval('$attr='.$out1.'$attr'.$out2);

      $template->addText((string)$attr, $prefix . $key);
    }
  }


  //--Method: PrintHTMLChildren
  //--Desc: Prints object's children in HTML format
  protected function PrintHTMLChildren(&$template, $prefix = '', $outputFuncsRef)
  {
    if(count($this->children) == 0)
      return;

    if($this->isArray && $this->itemTpl !== NULL && $this->itemTpl !== '')
    {
      $prevTpl = & $template;
      $itemTpl = & $this->pageBuilder->loadTemplate($this->itemTpl);
    }

    foreach($this->children as $key => $child)
    {
      $newPrefix = $prefix . $child->name;
      if($this->isArray)
      {
	if($this->itemTpl === NULL || $this->itemTpl === '')
	{
	  if(!isset($counter[$child->name]))
	    $counter[$child->name] = 1;
	  $newPrefix .= '_' . $counter[$child->name]++ . '__';
	  $child->PrintHTML($template, $newPrefix, $outputFuncsRef);
	}
	else
	{
	  //	  $newPrefix .= '__';

	  // creates a copy of the item template
	  $tpl[$key] = clone($itemTpl);
	  $child->PrintHTML($tpl[$key], $prefix, $outputFuncsRef);//$newPrefix);
	  $prevTpl->addTemplate($tpl[$key], $this->itemTplTag);
	  $prevTpl = & $tpl[$key];
	}
      }
      else
    	$child->PrintHTML($template, $prefix, $outputFuncsRef); 
    }
  }

  //--Method: PrintMIMEAttrs
  //--Desc: Prints object's attributes in MIME format
  protected function PrintMIMEAttrs($prefix = '')
  {
    $first = true;

    reset($this->attrs);
    foreach($this->attrs as $key => $attr)
    {
      if($first)
	$first = false;
      else
	$output .= '&';

      $output .= $prefix . $key . '=';
      $output .= htmlentities_utf(urlencode($attr));
    }

    return $output;
  }

  //--Method: PrintMIMEChildren
  //--Desc: Prints object's children in MIME format
  protected function PrintMIMEChildren($prefix = '')
  {
    $first = true;
    reset($this->children);
    $counter = array();
    foreach($this->children as $child)
    {
      if($first)
	$first = false;
      else
	$output .= '&';

      $newPrefix = $prefix . $child->name;
      if($this->isArray)
      {
	if(!isset($counter[$child->name]))
	  $counter[$child->name] = 1;
	$newPrefix .= '_' . $counter[$child->name]++;
      }
      $newPrefix .= '__';
     $output .= $child->PrintMIME($newPrefix);
    }

    return $output;
  }

  //--Method: PrintXMLAttrs
  //--Desc: Prints object's attributes in XML format
  protected function PrintXMLAttrs()
  {
    reset($this->attrs);
    foreach($this->attrs as $key => $attr)
      $output .= " $key=\"$attr\"";

    return $output;
  }

  //--Method: PrintXMLChildren
  //--Desc: Prints object's children in XML format
  protected function PrintXMLChildren($tabs)
  {
    reset($this->children);
    foreach($this->children as $child)
      $output .= $child->PrintXML($tabs);

    return $output;
  }

}

?>