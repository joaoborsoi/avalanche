<?php

require_once('CMS_RSSEvents.inc');

class CMS_Notification
{
  protected $page;
  protected $pageBuilder;

  function __construct($page)
  {
    $this->page = & $page;
    $this->pageBuilder = & $page->pageBuilder;
  }

  function SendNotification($docId)
  {
    if(!$this->pageBuilder->siteConfig->getVar("cms", "rssSendEmail"))
      return;

    // retrieves articles information
    $_GET['docId'] = $docId;
    $getDoc = $this->page->LoadModule('LIB_GetDoc');
    $fields = & $getDoc->children[0]->attrs;

    // only send messages for public articles
    if($fields['otherRight'] != 'r')
      return;


    $pageLabels = & $this->page->LoadModule('LANG_GetPageLabels');
    $subjectPrefix = $this->pageBuilder->siteConfig->getVar("cms", 
							    "subjectPrefix");

    $subject = $subjectPrefix.' '.$fields['title'];
    if($fields['publicationDate'] != NULL &&
       $fields['lastChanged'] != $fields['publicationDate'])
    {
      $subject .= ' ('.$pageLabels->attrs['changedTag'].')';
      $_GET['event'] = RSS_UPDATES;
    }
    else     
      $_GET['event'] = RSS_NEW;

    // get destinantions
    $getNotAddrs = $this->page->LoadModule('CMS_GetNotificationAddresses');
    $mailTo = implode(',',(array)$getNotAddrs->attrs['addresses']);

    // link for the document
    $link = dirname($_SERVER['PHP_SELF']);
    if($link!='/') 
      $link .= '/';
    $link = 'http://'.$_SERVER['HTTP_HOST'].$link;
    $link .= 'content/'.$docId;

    $mailFrom = $this->pageBuilder->siteConfig->getVar("cms", "mailFrom");
    $headers = "From: $mailFrom\r\n";
    $headers .= "Bcc: $mailTo\r\n";
    $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";
    $headers .= "Content-Transfer-Encoding: quoted-printable \r\n";


    $message = "<!DOCTYPE html PUBLIC \"-";
    $message .= "//W3C//DTD XHTML 1.0 Strict//EN\" ";
    $message .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
    $message .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>";

    // header
    $message .= "<h1>".$fields['title']."</h1>\n";
    $message .= "<p></small><a href='$link'>$link</a></small></p>\n";

    // authors, article reference and date
    $message .= '<cite>'.$fields['authors'].', #' . $fields['docId'];
    $dateFormat = $this->pageBuilder->siteConfig->getVar('cms','dateFormat');
    $lastChanged = strftime($dateFormat,(int)$fields['lastChanged']);
    $message .= ", <span id='lastChanged'>$lastChanged</span></cite>\n";

    // keywords
    $message .= "<p class='keywords'><strong>";
    $message .= $pageLabels->attrs['keywordsTag'].":</strong> ";
    $message .= $fields['keywords']."</p>\n";

    // content
    $message .= $fields['content'];

    $message .= '</body></html>';
    if(!mail(NULL,$subject,$message,$headers))
    {
      $msg = " " . $pageLabels->attrs['notificationErrorTag'];
      throw new AV_Exception(WARNING,$this->lang,array('addMsg'=>$msg));
    }
  }
}
?>