<?php

require_once('FORM_DynTable.inc');
require_once('AV_ModObject.inc');

//--Class: Form_Object
//--Desc: Implements Form Object operations.

class FORM_Object extends AV_ModObject
{
  public $printItems;                  // may specify array with the keys of 
                                        // items to be printed

  protected $templates;    //--Desc: Dynamic table array

  //--Method: FORM_Object
  //--Desc: Constructor.
  function __construct($name, $pageBuilder, & $templates, 
		       $nextField = 'nextField')
  {
    parent::__construct($name, $pageBuilder); //chama construtor da classe pai
    $this->templates = & $templates;
    $this->nextField = $nextField;
    $this->printItems = NULL;
  }

  function PrintHTMLChildren (& $template, $prefix, $outputFuncRef)
  {
    $prev = & $template;

    if($this->printItems == NULL)
    {
      foreach($this->children as & $child)
	$this->PrintHTMLChild($template,$prefix,$outputFuncRef,$prev,$child);
    }
    else
      foreach($this->printItems as $key)
	$this->PrintHTMLChild($template,$prefix,$outputFuncRef,$prev,
			      $this->children[$key]);
  }
    


  function PrintHTMLChild (& $template, $prefix, $outputFuncRef, &$prev,&$child)
  {
    // check if template definition comes from the database
    if($child->attrs['template'])
    {
      // check for lang tamplate 
      if($child->attrs['langTemplate'] == '1')
	$tplName = $child->attrs['template'].'_'.$this->pageBuilder->lang;
      else
	$tplName = $child->attrs['template'];

      // check for output function defined from the database
      if($child->attrs['outputFuncRef'])
      {
	if($outputFuncRef!=NULL)
	  $outputFuncRef = ','.$outputFuncRef;
	$outputFuncRef = $child->attrs['outputFuncRef'].$outputFuncRef;
      }
    }
    else
    {
      // handles formula fields
      $type = $child->attrs['type'];
      if($type =='formula')
	$type = $child->attrs['formulaType'];

      // template definitions comes from the templates array
      if(is_array($this->templates[$type]))
      {
	// new templates array format
	$tplName = $this->templates[$type]['fileName'];

	// check for output function defined from templates array
	if($this->templates[$type]['outputFuncRef'])
	{
	  if($outputFuncRef!=NULL)
	    $outputFuncRef = ','.$outputFuncRef;
	  $outputFuncRef = 
	    $this->templates[$type]['outputFuncRef'].
	    $outputFuncRef;
	}
      }
      else
	// old templates array format - kept for compatibiliy
	$tplName = $this->templates[$type];
    }

    if($tplName != NULL)
    {
      $tpl = & $this->pageBuilder->loadTemplate($tplName); 
      $prev->addTemplate($tpl, $this->nextField);
      $child->PrintHTML($tpl, $prefix, $outputFuncRef);
      $prev = & $tpl;
    }
  }
}
?>