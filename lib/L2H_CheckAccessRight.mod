<?php

require_once('AV_OperationObj.inc');
require_once('L2H_Manager.inc');

//--Class: L2H_CheckAccessRight
//--Parent: AV_OperationObj
//--Desc: 
class L2H_CheckAccessRight extends AV_OperationObj
{
  //
  //--Public
  //

  //--Method: LoadFieldsDef
  //--Desc: Redefines parent method to define module's input fields
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'path';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $path = $this->fieldParser->fields['path'];
    $man = new L2H_Manager($this);
    $status = $man->CheckAccessRight($path);
    if($status != SUCCESS)
      return $status;

    $pBuilder = & $this->pageBuilder;
    $templates = $pBuilder->siteConfig->getVar("dirs", "templates");
    if(mb_substr($templates,- 1) != '/')
      $templates .= '/';
    $pBuilder->siteConfig->setVar("dirs", "templates", 
				  $templates . $path);
    return SUCCESS;
  }
}


?>