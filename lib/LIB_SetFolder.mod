<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');

class LIB_SetFolder extends AV_DBOperationObj
{
  private $folder;

  function __construct(&$pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
		       $jsonInput=false, $transaction = true)
  {
    parent::__construct($pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
			$jsonInput,$transaction);
  }

  protected function LoadFieldsDef()
  {
    $this->folder = new LIB_Folder($this);
    $this->fieldsDef = $this->folder->LoadFieldsDef();
  }

  protected function Execute()
  {
    $this->folder->Set($this->fields);
  }
}

?>
