<?php

require_once('GAME_Element.inc');

class GAME_Message extends GAME_Element
{
  function LoadArea(AV_Module &$loadArea)
  {
    $loadMessage = $this->page->LoadModule('GAME_LoadMessage');
    
    $this->pageBuilder->rootTemplate($loadArea->attrs['type']);
    $loadArea->PrintHTML($this->pageBuilder->rootTemplate);
    $loadMessage->PrintHTML($this->pageBuilder->rootTemplate);
  }
}

?>