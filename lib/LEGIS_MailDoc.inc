<?php

require_once('LIB_Document.inc');

class LEGIS_MailDoc extends LIB_Document
{
  protected $entityId;

  function __construct(&$module,$entityId)
  {
    parent::__construct($module);
    $this->entityId = $entityId;
  }

  protected function LoadSearchDefs(& $params)
  {
    parent::LoadSearchDefs($params);

    $params['returnFieldsFound']['fromAddress'] = 'fromAddress';
    $params['searchObjs'][100009]['returnFields'][] = 'fromAddress';
    $params['searchObjs'][100009]['returnFieldsRefs']['fromAddress'] = "fromAddress";

    $params['returnFieldsFound']['toAddress'] = 'toAddress';
    $params['searchObjs'][100009]['returnFields'][] = 'toAddress';
    $params['searchObjs'][100009]['returnFieldsRefs']['toAddress'] = "toAddress";

    $params['returnFieldsFound']['ccAddress'] = 'ccAddress';
    $params['searchObjs'][100009]['returnFields'][] = 'ccAddress';
    $params['searchObjs'][100009]['returnFieldsRefs']['ccAddress'] = "ccAddress";

    $params['searchObjs'][100009]['tables'][] = 'LEGIS_MailEntity me';
    $params['searchObjs'][100009]['tablesWhere'][] = 'doc.docId=me.docId';
    $params['searchObjs'][100009]['tablesWhere'][] = "me.entityId='".$this->entityId."'";
  }
}

?>