<?php

require_once('AV_DBOperationObj.inc');
require_once('GAME_Engine.inc');

class GAME_Play extends AV_DBOperationObj
{
  protected function Execute()
  {
    $game = new GAME_Engine($this);
    $game->Play();
  }
}

?>