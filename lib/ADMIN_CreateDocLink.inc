<?php

require_once('ADMIN_Files.inc');

class ADMIN_CreateDocLink extends ADMIN_Files
{

  function LoadFiles($path)
  {
    try
    {
      $_GET['path'] = $this->rootPath . $path;
      $copyModule = $this->LoadModule('LIB_InsDocLink');

      $script = 'parent.OnCreateLink();';
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }    

    $script .= parent::LoadFiles($path);
    return $script;
  }
}

?>