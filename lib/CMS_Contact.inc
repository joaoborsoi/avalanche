<?php

require_once('CMS_Content.inc');

class CMS_Contact extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    // dummy
  }

  function LoadArea(&$fields, $path)
  {
    parent::LoadArea($fields, $path);

    $tpl = & $this->pageBuilder->loadTemplate('cmsContactForm');

    $getForm = $this->page->LoadModule('CMS_GetContactForm');
    $getForm->PrintHTML($tpl);

    $this->pageLabels->PrintHTML($tpl);

    $captcha  = $this->pageBuilder->loadTemplate('captchaField');
    $this->pageLabels->PrintHTML($captcha);
    $tpl->addTemplate($captcha,'captcha');

    // avalanche version
    $tpl->addText($this->pageBuilder->avVersion, 'avVersion');

    $script .= "<script type='text/javascript'>\n";
    $_GET['labelId']='fieldRequired';
    $_GET['lang'] = $this->pageBuilder->lang;
    $getLabel = $this->page->LoadModule('LANG_GetLabel');
    $script .= "$(document).ready(function(){cms.OnContactPageLoad(";
    $script .= json_encode(array('fieldRequired'=>$getLabel->attrs['value']));
    $script .= ");});\n</script>\n";
    $tpl->addText((string)$script,'script');

    $this->pageBuilder->rootTemplate->addTemplate($tpl,'mainContent');
  }

  function LoadListItem(& $fields)
  {
    // dummy
  }


}
?>