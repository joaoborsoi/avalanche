<?php 

require_once("AV_DBOperationObj.inc");
require_once("LANG_Label.inc");

class LANG_GetLangList extends AV_DBOperationObj
{
  
  function Execute()
  {
    $this->isArray = true;
    $aux = new LANG_Label ($this);
    $rows = $aux->GetLangList();


    // here we don't use AV_ModObject::LoadChildrenFromRows
    // beacuse it erases lang variable from lang list
    $i = 0;
    foreach($rows as $key => $row)
    {
      $this->children[$i] = new AV_ModObject('lang', $this->pageBuilder);
      $this->children[$i]->attrs = &$rows[$key];
      $this->children[$i]->attrs['fieldIndex'] = $i+1;
      $this->children[$i]->attrs['avVersion'] = $this->pageBuilder->avVersion;
      $i++;
    }
  }
}

?>