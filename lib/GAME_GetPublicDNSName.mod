<?php

require_once('AV_DBOperationObj.inc');
require_once('GAME_Application.inc');

class GAME_GetPublicDNSName extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

  }

  function Execute()
  {
    $app = new GAME_Application($this);
    $this->attrs['url'] = $app->GetPublicDNSName($this->fields['docId']);
  }
}

?>