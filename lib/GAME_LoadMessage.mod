<?php

require_once('AV_DBOperationObj.inc');

class GAME_LoadMessage extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'id';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $SQL = 'SELECT m.*, value as message FROM GAME_Message m, LANG_Text l ';
    $SQL .= "WHERE id='".$this->fields['id']."' ";
    $SQL .= 'AND message = labelId ';
    $SQL .= "AND lang='".$this->pageBuilder->lang."' ";
    $this->attrs = $this->dbDriver->GetRow($SQL);
    if(count($this->attrs)==0)
      throw new AV_Exception(NOT_FOUND,$this->pageBuilder->lang);
  }
}