<?php

class WIKI_Moderation extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'moderator';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['consistType'] = 'integer';

    $this->fieldsDef[2]['fieldName'] = 'otherRight';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'accessRight';

    $this->fieldsDef[3]['fieldName'] = 'pendingApproval';
    $this->fieldsDef[3]['required'] = false;
    $this->fieldsDef[3]['allowNull'] = true;
    $this->fieldsDef[3]['consistType'] = 'boolean';
  }

  protected function Execute()
  {
    $doc = new LIB_Document($this);
    $doc->LoadFieldsDef($this->fields['docId'],NULL);
    $doc->Set($this->fields);

    $nodeData = $doc->GetNodeData($this->fields['docId']);
    $this->attrs['lastChanged'] = $nodeData['lastChanged'];
  }
  

}