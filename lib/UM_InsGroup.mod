<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_InsGroup extends AV_DBOperationObj
{
  protected $groupMan;

  function __construct(&$pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
		       $jsonInput=false, $transaction = true)
  {
    $this->groupMan = new UM_Node('UM_Group',$this);
    parent::__construct($pbuilder, $itemTpl, $itemTplTag, $varPrefix, 
			$jsonInput,$transaction);
  }

  protected function LoadFieldsDef()
  {
    // loads fields definition for all dynamic tables associated
    $this->fieldsDef = $this->groupMan->LoadFieldsDef();

    $i = count($this->fieldsDef);
    $this->fieldsDef[$i]['fieldName'] = 'path';
    $this->fieldsDef[$i]['required'] = true;
    $this->fieldsDef[$i]['allowNull'] = false;
    $this->fieldsDef[$i]['default'] = '/';
    $this->fieldsDef[$i]['stripSlashes'] = false;
  }

  protected function Execute()
  {
    $this->groupMan->Insert($this->fields);
  }
}

?>