<?php

class L2PDF_Document
{
  protected $pageBuilder;
  protected $texContent;
  protected $lang;

  function __construct(& $pageBuilder)
  {
    $this->pageBuilder = & $pageBuilder;
    $this->lang = $pageBuilder->lang;
  }

  function Write($text,$lineBreak=true,$pos=-1)
  {
    if($lineBreak)
      $text .= "\n";
    if($pos >= 0)
    {
      $this->texContent = substr_replace($this->texContent,$text,$pos,0);
      return $pos+strlen($text);
    }
    else
      $this->texContent .= $text;
  }

  function GetLength()
  {
    return strlen($this->texContent);
  }

  function Escape($string)
  {
    $string = str_replace('#', '\#', $string);
    $string = str_replace('$', '\$', $string);
    $string = str_replace('%', '\%', $string);
    $string = str_replace('^', '\^{}', $string);
    $string = str_replace('&', '\&', $string);
    $string = str_replace('_', '\_', $string);
    $string = str_replace('{', '\{', $string);
    $string = str_replace('}', '\}', $string);
    $string = str_replace('²', '$^{2}$', $string);
    $string = str_replace('³', '$^{3}$', $string);
    $string = str_replace('°', '$^{\circ}$', $string);
    $string = str_replace('´','$\acute{}$',$string);
    $string = str_replace('´','$\acute{}$',$string);
    $string = str_replace(' ',' ',$string);
    $string = str_replace('',' ',$string);
    return trim(str_replace('~', '$\sim$', $string));
  }

  function CreateDoc($tmpDir, $unlink = true, $pdfLatex = false)
  {
    if(!is_dir($tmpDir))
      throw new AV_Exception(INVALID_FIELD,$this->lang,
			     Array('addMsg'=>' (tmpDir)'));

    if($tmpDir[strlen($tmpDir)-1] != '/')
      $tmpDir .= '/';

    $filePath = tempnam($tmpDir, "");
    $fileName = substr($filePath, strlen($tmpDir));

    $handle = fopen($filePath . ".tex", "w");
    fwrite($handle, $this->texContent);
    fclose($handle);
    
    chdir($tmpDir);
    
    // runs latex 3 times to generate table of content
    try
    {
      if(!$pdfLatex)
      {
	exec("latex $fileName.tex");
	if(strpos($this->texContent,'\tableofcontents')!==false)
	{
	  exec("latex $fileName.tex");
	  exec("latex $fileName.tex");
	}
	if(!is_file($filePath.'.dvi'))
	  throw new AV_Exception(FAILED, $this->lang);

	exec("dvipdf -dPDFSETTINGS=/prepress $fileName.dvi");
      }
      else
      {
	exec("pdflatex $fileName.tex");
	exec("pdflatex $fileName.tex");
      }

      if(!is_file($filePath.'.pdf'))
	throw new AV_Exception(FAILED, $this->lang);

      $fileInfo['fileName'] = $fileName;
      $fileInfo['filePath'] = $filePath;
      return $fileInfo;
    }
    catch(Exception $e)
    {
      if($unlink)
	$this->UnlinkGarbage($fileName);
      throw $e;
    }
  }

  function ReadFile($fileInfo, $unlink = true)
  {
      header('Cache-Control: maxage=3600'); //Adjust maxage appropriately
      header('Pragma: public');    
      header("Content-type: application/pdf");
      header("Content-Length: ".filesize($fileInfo['filePath'].'.pdf'));
      header("Content-Disposition: attachment; filename=report.pdf");
      
      readfile($fileInfo['filePath'].'.pdf');
      
      if($unlink)
	$this->UnlinkGarbage($fileInfo['fileName']);
  }

  function UnlinkGarbage($fileName)
  {
    unlink($fileName);

    $files = shell_exec("find -name \"$fileName.*\" -printf \"%f\n\" ");

    foreach(explode("\n",$files) as $file)
    {
      if($file != NULL)
	unlink($file);
    }
  }

}

?>