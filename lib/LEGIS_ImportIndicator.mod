<?php

require_once('AV_DBOperationObj.inc');
require_once('simpletest/browser.php');
include('phpQuery-onefile.php');

class LEGIS_ImportIndicator extends AV_DBOperationObj
{

  function Execute()
  {
    if($_REQUEST['date']!=NULL)
      $date = strtotime($_REQUEST['date']);
    else
      $date = strtotime("first day of previous month");

    $month = date('m/Y',$date);
    $date = date('Y-m-d',$date);

    $SQL = 'SELECT value FROM LEGIS_Indicator ';
    $SQL .= "WHERE refDate='$date' ";
    $value = $this->dbDriver->GetOne($SQL);
    if($value!==NULL)
      return;

    $value = $this->GetIndicator($month,$month);
    if($value!=NULL)
    {
      $SQL = 'INSERT INTO LEGIS_Indicator (refDate,value) VALUES ';
      $SQL .= "('$date',$value)";
      $this->dbDriver->ExecSQL($SQL);
    }
  }

  function GetIndicator($dataInicial,$dataFinal)
  {
    $url = "https://www3.bcb.gov.br/CALCIDADAO/publico/exibirFormCorrecaoValores.do?method=exibirFormCorrecaoValores";
    $browser = new SimpleBrowser();
    $browser->get($url);
    $browser->setFieldByName('dataInicial', $dataInicial);
    $browser->setFieldByName('dataFinal', $dataFinal);
    $browser->setFieldByName('valorCorrecao', '1');
    $browser->submitFormById('corrigirPorIndiceForm');

    $html = phpQuery::newDocumentHTML($browser->getContent());
    $found = false;
    foreach(pq('td',$html) as $td)
    {
      $td = pq($td);
      if($found)
      {
	// aplica correção de inflação (IGP-M)
	return
	  str_replace(',','.',
		      str_replace('.','',
				  preg_replace("/[^0-9,.]/","",
					       $td->text())));
      }

      if(preg_replace('/\s+/', ' ',
		      strtolower(trim($td->text())))=='valor percentual correspondente')
	$found = true;
    }

    return NULL;
  }
}

?>