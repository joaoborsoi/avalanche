<?php

require_once('LEGIS_AdvancedSearch.inc');

class LEGIS_AdvancedSearchProcess extends LEGIS_AdvancedSearch
{
  function SearchProcess(& $params, $limit, $offset, $sub, &$totalResults)
  {
    if(count($params) == 0)
      throw new AV_Exception(INVALID_FIELD,$this->lang);

    $this->ProcessSearchParams($params, $tables, $tablesWhere, $where);

    if(!$sub)
      $tablesWhere[] = 'p.processId=p.rootProcessId';

    //doc.title, doc.authors, doc.descr, subjectId 
    $SQL = 'p.processId,p.rootProcessId,rp.folder,rp.folderYear,';
    $SQL .= "(SELECT GROUP_CONCAT(number) FROM LEGIS_ProcessNumber pn ";
    $SQL .= "WHERE pn.processId=p.processId) AS number, ";
    $SQL .= "(SELECT s.name FROM LEGIS_Situation s ";
    $SQL .= "WHERE s.docId=p.situationId) AS situation, ";
    $SQL .= "(SELECT s.name FROM LEGIS_Subject s ";
    $SQL .= "WHERE s.docId=p.subjectId) AS subject ";

    $SQL = $this->Search($SQL,$limit, $offset, $tables, $tablesWhere, $where,
			 $totalResults);

    $rows = $this->dbDriver->GetAll($SQL);
    foreach($rows as &$row)
      $row['number'] = explode(',',$row['number']);
    return $rows;
  }

  protected function ProcessSearchParams(& $params, & $tables, & $tablesWhere, & $where)
  {
    $tables[] = 'LIB_Document doc';
    $tablesWhere = array();

    $tables[] = 'LEGIS_Process p';
    $tablesWhere[] = 'p.processId = doc.docId';


    $tables[] = 'LEGIS_RootProcess rp';
    $tablesWhere[] = 'p.rootProcessId = rp.processId';

    $where = array();
    foreach($params as $param)
    {
      switch($param['param'])
      {
      case 'folder':
	$this->AddFolder($param, $tables, $tablesWhere, $where);
	break;

      case 'commitment':
	$this->AddCommitment($param, $tables, $tablesWhere, $where);
	break;

      case 'actionType':
	$this->AddActionTypes($param, $tables, $tablesWhere, $where);
	break;

      case 'local':
	$this->AddLocal($param, $tables, $tablesWhere, $where);
	break;

      case 'lawyer':
	$this->AddLawyer($param, $tables, $tablesWhere, $where);
	break;

      case 'phases':
	$this->AddPhases($param, $tables, $tablesWhere, $where);
	break;

      case 'subject':
	$this->AddSubject($param, $tables, $tablesWhere, $where);
	break;

      case 'situation':
 	$this->AddSituation($param, $tables, $tablesWhere, $where);
	break;

      case 'result':
 	$this->AddResult($param, $tables, $tablesWhere, $where);
	break;

      case 'otherPart':
 	$this->AddOtherPart($param, $tables, $tablesWhere, $where);
	break;

      case 'customer':
	$this->AddCustomer($param, $tables, $tablesWhere, $where);
	break;

      case 'aggregator':
	$this->AddAggregator($param, $tables, $tablesWhere, $where);
	break;

      case 'start':
	$this->AddStart($param, $tables, $tablesWhere, $where);
	break;

      case 'city':
	$this->AddProcessCity($param, $tables, $tablesWhere, $where);
	break;

      case 'state':
	$this->AddProcessState($param, $tables, $tablesWhere, $where);
	break;

      case 'clientCity':
	$this->AddProcessClientCity($param, $tables, $tablesWhere, $where);
	break;

      case 'value':
	$this->AddValue($param, $tables, $tablesWhere, $where);
	break;

      case 'justice':
	$this->AddJustice($param, $tables, $tablesWhere, $where);
	break;

      default:
	throw new AV_Exception(INVALID_FIELD,$this->lang,$param);
      }
    }
  }

  protected function AddFolder(& $param, & $tables, & $tablesWhere, & $where)
  {
    $expr = "rp.folder = '" . $param['folder'] . "'";
    if($param['folderYear'] != NULL)
      $expr .= " AND rp.folderYear = '" . $param['folderYear'] . "'";

    if($param['not'])
      $expr = "NOT ($expr)";
    $where['folder'][] = $expr;
  }

  protected function AddCommitment(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_Commitment cm', $tables))
    {
      $tables[] = 'LEGIS_Commitment cm';
      $tablesWhere[] = 'p.processId = cm.processId';
    }

    $where['commitment'][] = $this->GetDateParam('cm.date', $param['commitment']);
  }

  protected function AddActionTypes(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['actionTypes'][] = 
      "p.actionTypeId ".($param['not']?'<>':'=')." '" . $param['actionTypeId'] . "'";
  }

  protected function AddLocal(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!$param['not'])
    {
      if(!in_array('LEGIS_ProcessLocal pl', $tables))
      {
	$tables[] = 'LEGIS_ProcessLocal pl';
	$tablesWhere[] = 'p.processId = pl.processId';
      }

      $expr = "pl.localId = '" . $param['localId'] . "'";
      if($param['room'] !== '' && $param['room'] !== NULL)
	$expr .= " AND pl.room = '" . $param['room'] . "'";
    }
    else
    {
      $expr = '(SELECT DISTINCT processId FROM LEGIS_ProcessLocal ';
      $expr .= "WHERE localId = '" . $param['localId'] . "'";
      if($param['room'] !== '' && $param['room'] !== NULL)
	$expr .= " AND room = '" . $param['room'] . "')";

      $expr = 'p.processId NOT IN ' . $expr;
    }
    $where['local'][] = $expr;
  }


  protected function AddLawyer(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_ProcessLawyer plo', $tables))
    {
      $tables[] = 'LEGIS_ProcessLawyer plo';
      $tablesWhere[] = 'p.processId = plo.processId';
    }

    $where['lawyer'][] = 
      "plo.entityId ".($param['not']?'<>':'=')." '" . $param['lawyer'] . "'";
  }


  protected function AddPhases(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['phases'][] = 
      "p.phasesId ".($param['not']?'<>':'=')." '" . $param['phasesId'] . "'";
  }

  protected function AddSubject(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['subject'][] = 
      "p.subjectId ".($param['not']?'<>':'=')." '" . $param['subjectId'] . "'";
  }

  protected function AddSituation(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['situation'][] = 
      "p.situationId ".($param['not']?'<>':'=')." '" . $param['situationId'] . "'";
  }

  protected function AddResult(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['result'][] = 
      "p.resultId ".($param['not']?'<>':'=')." '" . $param['resultId'] . "'";
  }

  protected function AddOtherPart(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['otherPart'][] = 
      "p.otherPart ".($param['not']?'NOT ':'')."like '%".$param['otherPart']."%'";
  }

  protected function AddCustomer(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!$param['not'])
    {
      if(!in_array('LEGIS_ProcessClient pc', $tables))
      {
	$tables[] = 'LEGIS_ProcessClient pc';
	$tablesWhere[] = 'p.processId = pc.processId';
      }
      $where['client'][] = "pc.entityId = '" . $param['customer']['docId'] . "'";
    }
    else
    {
      $SQL = '(SELECT DISTINCT processId FROM LEGIS_ProcessClient ';
      $SQL .= "WHERE entityId='".$param['customer']['docId'] . "')";
      $where['client'][] = 'p.processId NOT IN ' . $SQL;
    }
  }

  protected function AddAggregator(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!in_array('LEGIS_ProcessClient agr', $tables))
    {
      $tables[] = 'LEGIS_ProcessClient agr';
      $tablesWhere[] = 'p.processId = agr.processId';
    }
    $where['aggregator'][] = "agr.entityId = '" . $param['aggregatorId'] . "'";
  }

  protected function AddStart(& $param, & $tables, & $tablesWhere, & $where)
  {
    $SQL = "(SELECT MIN(date) FROM LEGIS_History WHERE processId = p.processId)";
    $where['start'][] = $this->GetDateParam($SQL, $param['start']);
  }

  protected function AddProcessCity(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!$param['not'])
    {
      if(!in_array('LEGIS_ProcessLocal pl', $tables))
      {
	$tables[] = 'LEGIS_ProcessLocal pl';
	$tablesWhere[] = 'p.processId = pl.processId';
      }

      $where['city'][] = "pl.cidade_codigo = '" . $param['cidade_codigo'] . "'";
    }
    else
    {
      $SQL = '(SELECT DISTINCT processId FROM LEGIS_ProcessLocal ';
      $SQL .= "WHERE cidade_codigo='".$param['cidade_codigo'] . "')";
      $where['city'][] = 'p.processId NOT IN ' . $SQL;
    }
  }

  protected function AddProcessState(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!$param['not'])
    {
      if(!in_array('LEGIS_ProcessLocal pl', $tables))
      {
	$tables[] = 'LEGIS_ProcessLocal pl';
	$tablesWhere[] = 'p.processId = pl.processId';
      }

      $where['state'][] = "pl.uf_codigo = '" . $param['uf_codigo'] . "'";
    }
    else
    {
      $SQL = '(SELECT DISTINCT processId FROM LEGIS_ProcessLocal ';
      $SQL .= "WHERE uf_codigo='".$param['uf_codigo'] . "')";
      $where['state'][] = 'p.processId NOT IN ' . $SQL;
    }

  }

  protected function AddProcessClientCity(& $param, & $tables, & $tablesWhere, & $where)
  {
    if(!$param['not'])
    {
      if(!in_array('LEGIS_ProcessClient pcc, EM_NaturalPerson np', $tables))
      {
	$tables[] = 'LEGIS_ProcessClient pcc, EM_NaturalPerson np';
	$tablesWhere[] = 'p.processId = pcc.processId';
	$tablesWhere[] = 'pcc.entityId = np.docId';
      }
      $where['clientCity'][] = "np.cidade_codigo = '" . $param['cidade_codigo'] . "'";
    }
    else
    {
      $SQL = '(SELECT DISTINCT processId ';
      $SQL .= 'FROM LEGIS_ProcessClient pcc, EM_NaturalPerson np ';
      $SQL .= 'WHERE pcc.entityId = np.docId ';
      $SQL .= "AND cidade_codigo='".$param['cidade_codigo'] . "')";
      $where['clientCity'][] = 'p.processId NOT IN ' . $SQL;
    }

  }

  protected function AddValue(& $param, & $tables, & $tablesWhere, & $where)
  {
    $expr = '';
    if($param['value']['lower'] !== '' && $param['value']['lower'] !== NULL)
      $expr = "'" . $param['value']['lower'] . "' >= p.cost";

    if($param['value']['greater'] !== '' && $param['value']['greater'] !== NULL)
    {
      if($expr != '')
	$expr .= ' AND ';
      $expr .= "p.cost >= '" . $param['value']['greater'] . "'";
    }
    $where['cost'][] = $expr;

  }

  protected function AddJustice(& $param, & $tables, & $tablesWhere, & $where)
  {
    $where['justice'][] = 
      "rp.justiceId ".($param['not']?'<>':'=')." '" . $param['justiceId'] . "'";
  }

}