<?php

function ArrayCommand($cmd,$args)
{
  $cmd = '$result = '.$cmd.'(';
  $first = true;
  $arrayObject = NULL;
  foreach($args as $key=>$arg)
  {
    if($first)
      $first = false;
    else
      $cmd .= ',';

    switch(gettype($arg))
    {
    case 'array':
      $cmd .= '$args['.$key.']';
      break;
    case 'object':
      if(!is_subclass_of($arg,'ArrayObject'))
	throw new BadMethodCallException( 'Tipo inválido' );
      $cmd .= '$args['.$key."]->getArrayCopy()";

      $arrayObject = get_class($arg);
      break;
    case 'NULL':
      $cmd .= 'array()';
      break;
    default:
      throw new BadMethodCallException( 'Tipo inválido' );
    }
  }
  $cmd .= ');';
  eval($cmd);
  if($arrayObject!=NULL)
    return new $arrayObject($result);
  return $result;
}

function ArrayObjectMerge()
{
  $args = func_get_args();
  return ArrayCommand('array_merge',$args);
}

function ArrayObjectDiff()
{
  $args = func_get_args();
  return ArrayCommand('array_diff',$args);
}

function ArrayObjectKeys($input)
{
  switch(gettype($input))
  {
  case 'array':
    return array_keys($input);
  case 'object':
    if(!is_subclass_of($input,'ArrayObject'))
      throw new BadMethodCallException( 'Tipo inválido' );
    return array_keys($input->getArrayCopy(),$search_value,$strict);
  }
}

function ArrayObjectKeyExists(mixed $key, $search)
{
  switch(gettype($search))
  {
  case 'array':
    return array_key_exists($key,$search);
  case 'object':
    if(!is_subclass_of($search,'ArrayObject'))
      throw new BadMethodCallException( 'Tipo inválido' );
    return $search->offsetExists($key);
  }
}
 
?>