<?php

require_once('ADMIN_Page.inc');

abstract class ADMIN_Popup extends ADMIN_Page
{
  protected $pageLabels;
  protected $script;

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminPopup');
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');

    $this->pageLabels = & $this->LoadModule("LANG_GetPageLabels");
    $this->pageLabels->PrintHTML($this->pageBuilder->rootTemplate, NULL,
				 'htmlentities_utf');

    $this->pageBuilder->rootTemplate->addText((string)$_REQUEST['tab'], 
					      'tab');

    if($this->pageBuilder->siteConfig->getVar("debug", "frameDebug"))
      $this->pageBuilder->rootTemplate->addText("width='700'", 'frameParams');
    else
      $this->pageBuilder->rootTemplate->addText("style='display:none'", 
						'frameParams');



    $this->LoadPopup();
    $this->pageBuilder->rootTemplate->addText((string)$this->script, 'onLoad');
  }

  abstract protected function LoadPopup();
}

?>