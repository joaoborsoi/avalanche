<?php
require_once('CMS_Menu.inc');

class CMS_TreeMenu extends CMS_Menu
{
  function LoadMenu($menuItem,&$submenu,&$subareas,&$topPath)
  {
    if($this->LoadSubmenu($menuItem,$submenu,$subareas,$topPath))
    {
      $submenu = "<div class='box'>\n".
	"<h3>".$this->page->pageLabels->attrs['navTag']."</h3>\n".
	"<ul class='inner tree' id='tree'>\n".
	$submenu."</ul>\n</div>";
      return true;
    }
    return false;
  }

  protected function LoadSubmenu($menuItem,&$submenu,&$subareas,&$topPath)
  {
    $menuItemAux = addslashes($menuItem);

    $_GET['path'] = '/content/Menu/'.$menuItemAux;
    $_GET['fixedOrder'] = '1';
    $getFolderList = & $this->page->LoadModule('LIB_GetFolderList');

    if(count($getFolderList->children)==0)
      return false;

    foreach($getFolderList->children as & $folder)
    {
      $currPath = $menuItem.'/'.$folder->attrs['name'].'/';

      // anchor link
      $link = "<a href=\"area/".str_replace("%2F", "/", urlencode($currPath));
      $link .= "\">" . $folder->attrs['menuTitle'] ."</a>";

      // if we reach the selected menu item, build subarea for content
      if($menuItemAux.'/' == $this->page->menuPath && $_REQUEST['docId'] == NULL)
	$subareas .= "<li>$link</li>";

      // build submenu item
      $submenu .= "<li id='menu_".$folder->attrs['folderId']."' ";
      $currPath = addslashes($currPath);
      if(!strncmp($currPath,$this->page->menuPath,strlen($currPath)))
      {
	if($currPath == $this->page->menuPath)
	{
	  // updates topPath
	  if($_REQUEST['docId'] == NULL)
	    $topPath .= ' <strong>'.$folder->attrs['name'].'</strong>';	
	  else
	  {
	    if($this->pageBuilder->siteConfig->getVar('cms',
						      'menuParentPathLink'))
	      $topPath .= ' ' . $link . ' #'.$_REQUEST['docId'];
	    else
	      $topPath .= ' '.$folder->attrs['name'];
	  }

	  $submenu .= " class='selected'";

	  // saves currently selected folder tables
	  $this->page->folderTables = $folder->attrs['folderTables'];
	}
	else
	{
	  // updates topPath
	  if($this->pageBuilder->siteConfig->getVar('cms',
						    'menuParentPathLink'))
	    $topPath .= ' ' . $link;
	  else
	    $topPath .= ' '.$folder->attrs['menuTitle'];
	}
      }
      else
	  $submenu .= " class='closed'";
	
      $submenu .= ">$link";

      // loads subsubmenu
      $subsubmenu = '';
      if($this->LoadSubmenu($menuItem.'/'.$folder->attrs['name'],$subsubmenu,
			    $subareas,$topPath))
	$submenu .= "<ul>$subsubmenu</ul>";

      $submenu .= "</li>";
    }
    return true;
  }

}

?>