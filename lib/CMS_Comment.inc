<?php

require_once('CMS_Content.inc');

class LIB_Comment extends CMS_Content
{
  function LoadDocument(& $fields)
  {
  }

  function LoadArea(&$fields,$path)
  {
  }

  function LoadListItem(& $fields)
  {
    // comment
    $content = '<h4>'.$fields['name'];
    $content .= ' <em>'.$fields['lastChanged']."</em></h4>\n";
    $content .= "<p><a href='content/".$fields['articleId'];
    $content .= '#comment'.$fields['docId']."'>";
    $content .= '<small>'.$this->pageLabels->attrs['commentInTag'].'</small> ';
    $content .= $fields['articleIdText']."</a></p>\n";
    $content .= '<p>'.$this->page->GetResume($fields['comment'],
					     $_REQUEST['filter']);
    $content .= "</p>\n";
    return $content;
  }

  function LoadFeedItem(& $fields, $base)
  {
    $link = $base.'content/'.$fields['articleId'].'#comment'.$fields['docId'];
    $content = '<title>'.$this->pageLabels->attrs['commentInTag'];
    $content .= ' ' . $fields['articleIdText']."</title>\n";
    $content .= "<link>$link</link>\n";
    $content .= "<guid>$link</guid>\n";
    $content .= "<description>\n";
    $content .= "<![CDATA[ \n";
    $content .= $this->page->GetResume($fields['comment']);
    $content .= "\n]]>\n";
    $content .= "</description>\n";
    $content .= '<pubDate>'.$fields['lastChanged']."</pubDate>\n";  
    return $content;
  }


}
    

?>