<?php

require_once('AV_DBOperationObj.inc');

class GAME_LoadIcon extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'id';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $SQL = 'SELECT * ';
    $SQL .= 'FROM GAME_Icon ';
    $SQL .= "WHERE id='".$this->fields['id']."'";
    $this->attrs = $this->dbDriver->GetRow($SQL);

  }
}

?>