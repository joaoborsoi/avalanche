<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Process.inc');

class LEGIS_GenerateReceipt extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'processId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'entityId';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['consistType'] = 'integer';

    $this->fieldsDef[2]['fieldName'] = 'processNumber';
    $this->fieldsDef[2]['required'] = true;
    $this->fieldsDef[2]['allowNull'] = false;
    $this->fieldsDef[2]['stripSlashes'] = false;

    $this->fieldsDef[3]['fieldName'] = 'value';
    $this->fieldsDef[3]['required'] = true;
    $this->fieldsDef[3]['allowNull'] = false;

    $this->fieldsDef[4]['fieldName'] = 'date';
    $this->fieldsDef[4]['required'] = true;
    $this->fieldsDef[4]['allowNull'] = false;
  }
  
  function Execute()
  {
    $process =  new LEGIS_Process($this);
    $receipt = 
      $process->GenerateReceipt($this->fields['processId'],
				$this->fields['entityId'],
				$this->fields['processNumber'],
				$this->fields['value'],
				$this->fields['date']/*,false*/);
    $this->attrs = array_merge($this->fields,$receipt);
  } 
}

?>