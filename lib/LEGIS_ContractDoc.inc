<?php

require_once('LIB_Document.inc');

class LEGIS_ContractDoc extends LIB_Document
{
  protected function LoadSearchDefs(& $params)
  {
    parent::LoadSearchDefs($params);

    if($params['entityId'])
    {
      $params['searchObjs'][100010]['tables'][] = 'LEGIS_ContractEntity ce';
      $params['searchObjs'][100010]['tablesWhere'][] = 'doc.docId=ce.docId';
      $params['searchObjs'][100010]['tablesWhere'][] = "ce.entityId='".$params['entityId']."'";
    }

    if($params['processId'])
    {
      $params['searchObjs'][100010]['tables'][] = 'LEGIS_ContractProcess cp';
      $params['searchObjs'][100010]['tablesWhere'][] = 'doc.docId=cp.docId';
      $params['searchObjs'][100010]['tablesWhere'][] = "cp.processId='".$params['processId']."'";
    }


  }
}

?>