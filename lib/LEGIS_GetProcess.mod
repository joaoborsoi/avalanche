<?php
require_once('LIB_GetDoc.mod');
require_once('LEGIS_Process.inc');

class LEGIS_GetProcess extends LIB_GetDoc
{
  protected function LoadFieldsDef()
  {
    parent::LoadFieldsDef();
    $i = count($this->fieldsDef);

    $this->fieldsDef[$i]['fieldName'] = 'parentId';
    $this->fieldsDef[$i]['required'] = false;
    $this->fieldsDef[$i]['allowNull'] = true;
    $this->fieldsDef[$i]['consistType'] = 'integer';
  }

  protected function &CreateDocument()
  {
    return new LEGIS_Process($this);
  }

  protected function Execute()
  {
    $templates = $this->GetTemplates();

    $doc = & $this->CreateDocument();
    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 

    if($this->fields['docId'] == NULL)
      $keyFields = NULL;
    else
      $keyFields = Array('docId' => $this->fields['docId']);

    $doc->GetForm($keyFields,$this->fields['parentId'],$this->fields['path'], 
		  $this->fields['fieldName'], $this->fields['sourceId'], 
		  $templates, $formObject,$defValues,
		  $this->fields['dateFormat'],$this->fields['numberFormat'],
		  $this->fields['short']);

    $this->children[] = & $formObject;
  }

}
?>