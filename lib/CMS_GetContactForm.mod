<?php

require_once('AV_OperationObj.inc');

class CMS_GetContactForm extends AV_OperationObj
{
  protected function GetTemplates()
  {
    if($this->fields['fieldName'] != '')
    {
      $templates['select']['fileName'] = 'loadSelectField';
      $templates['selectMult']['fileName'] = 'loadSelectMultField';
      $templates['selectOption']['fileName']  = 'loadSelectFieldOption';
      $templates['selectMultOption']['fileName']  = 'loadSelectMultFieldOption';
      $templates['radio']['fileName'] = 'radioField';
      $templates['radioOption']['fileName']  = 'radioFieldOption';
    }
    else
    {
      $templates['hidden']['fileName'] = 'hiddenField';
      $templates['hidden']['outputFuncRef'] = 'htmlentities_utf';
      $templates['text']['fileName'] = 'textField';
      $templates['text']['outputFuncRef'] = 'htmlentities_utf';
      $templates['textArea']['fileName'] = 'textAreaField';
      $templates['textArea']['outputFuncRef'] = 'htmlentities_utf';
      $templates['htmlArea']['fileName'] = 'htmlAreaField';
      $templates['htmlArea']['outputFuncRef'] = 'htmlentities_utf';
      $templates['cep']['fileName']  = 'cepField';
      $templates['cep']['outputFuncRef'] = 'htmlentities_utf';
      $templates['cnpj']['fileName']  = 'cnpjField';
      $templates['cpf']['fileName']  = 'cpfField';
      $templates['file']['fileName'] = 'fileField';
      $templates['fileList']['fileName'] = 'imageListField';
      $templates['fileListItem']['fileName'] = 'imageListItem';

      if($this->pageBuilder->lang == 'en_US')
	$templates['date']['fileName'] = 'dateField_en_US';
      else
	$templates['date']['fileName'] = 'dateField';

      if($this->pageBuilder->lang == 'en_US')
	$templates['datetime']['fileName'] = 'dateTimeField_en_US';
      else
	$templates['datetime']['fileName'] = 'dateTimeField';

      $templates['schedule']['fileName'] = 'scheduleField';
      $templates['select']['fileName'] = 'selectField';
      $templates['select']['outputFuncRef'] = 'htmlentities_utf';
      $templates['readOnlySelect']['fileName'] = 'readOnlySelect';
      $templates['selectMult']['fileName'] = 'selectMultField';
      $templates['selectMult']['outputFuncRef'] = 'htmlentities_utf';
      $templates['selectOption']['fileName']  = 'selectFieldOption';
      $templates['selectMultOption']['fileName']  = 'selectMultFieldOption';
      $templates['selectMultOption']['outputFuncRef'] = 'htmlentities_utf';
      $templates['legalEntityRef']['fileName']  = 'legalEntityRefField';
      $templates['legalEntityRef']['outputFuncRef'] = 'htmlentities_utf';
      $templates['naturalPersonRef']['fileName']  = 'naturalPersonRefField';
      $templates['naturalPersonRef']['outputFuncRef'] = 'htmlentities_utf';
      $templates['accessRight']['fileName']  = 'accessRight';
      $templates['accessRight']['outputFuncRef'] = 'htmlentities_utf';
      $templates['langField']['fileName']  = 'langField';
      $templates['langField']['outputFuncRef'] = 'htmlentities_utf';
      $templates['langLabel']['fileName']  = 'langLabelField';
      $templates['langTextArea']['fileName']  = 'langTextAreaField';
      $templates['langHtmlArea']['fileName']  = 'langHtmlAreaField';
      $templates['langTab']['fileName']  = 'langTab';
      $templates['langTabItem']['fileName']  = 'langTabItem';
      $templates['boolean']['fileName']  = 'checkboxField';
      $templates['float']['fileName'] = 'floatField';
      $templates['percent']['fileName'] = 'percentField';
      $templates['radio']['fileName'] = 'radioField';
      $templates['radioOption']['fileName']  = 'radioFieldOption';
    }
    return $templates;
  }


  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  protected function Execute()
  {
    $templates = $this->GetTemplates();

    $form = new FORM_DynTable('CMS_Contact', $this);
    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 

    $form->GetForm(NULL,NULL,NULL, $templates,$formObject,NULL,NULL);

    $this->children[] = & $formObject;
  }
}

?>