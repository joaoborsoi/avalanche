<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Document.inc');

class LIB_GetDocLinks extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }


  protected function Execute()
  {
    $this->isArray = true;

    $doc = new LIB_Document($this);
    $result = $doc->GetDocLinks($this->fields['docId'], $status);

    $this->LoadChildrenFromRows($result, 'path');
  }
}

?>