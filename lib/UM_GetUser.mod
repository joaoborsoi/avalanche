<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_GetUser extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'userId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
  }

  protected function Execute()
  {
    $userMan = new UM_User($this);
    $userMan->LoadFieldsDef();

    // get rows
    $row = $userMan->Get(Array('userId'=>$this->fields['userId']),$status);
    $this->attrs = $row;
  }
}

?>

