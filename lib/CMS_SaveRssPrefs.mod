<?php

require_once('AV_DBOperationObj.inc');
require_once('CMS_RssPrefs.inc');

class CMS_SaveRssPrefs extends AV_DBOperationObj
{
  protected $table;

  protected function LoadFieldsDef()
  {
    $this->table = & new CMS_RssPrefs('CMS_RSSPrefs',$this);
    $this->fieldsDef = $this->table->LoadFieldsDef();

    $i = count($this->fieldsDef);  
    $this->fieldsDef[$i]['fieldName'] = 'prefs';
    $this->fieldsDef[$i]['list'] = true;
    $this->fieldsDef[$i]['sizeVarName'] = 'prefs_numOfOptions';
    $subfieldDef = array();
    $subfieldDef[0]['fieldName'] = 'folderId';
    $subfieldDef[0]['required'] = true;
    $subfieldDef[0]['allowNull'] = false;
    $subfieldDef[0]['consistType'] = 'integer';
    $this->fieldsDef[$i]['fieldsDef'] = $subfieldDef;
  }


  protected function Execute()
  {
    $this->table->Set($this->fields);
  }
}
?>