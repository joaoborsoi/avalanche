<?php

require_once('AV_DBOperationObj.inc');
require_once('GAME_Application.inc');

class GAME_CheckCancelRequested extends AV_DBOperationObj
{
  function Execute()
  {
    $app = new GAME_Application($this);
    $app->CheckCancelRequested();
  }
}

?>