<?php

require_once('FORM_DynTable.inc');
require_once('LIB_Folder.inc');
require_once('LEGIS_RepresentativeLoopCheck.inc');
require_once('LEGIS_Customer.inc');

class LEGIS_Representative extends FORM_DynTable
{
  function Insert(& $fields, $extraFields = array())
  {
    if($fields['representative']['docId']==NULL)
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>'representative',
				   'addMsg'=>' (Representante)'));
    $fields['representative'] = $fields['representative']['docId'];

    // verifica se existe loop representativo
    $check =  new LEGIS_RepresentativeLoopCheck('EM_NaturalPerson',$this->module);
    $check->Check($fields['customer'], $fields['representative']);

    $this->dbDriver->ExecSQL('BEGIN');

    parent::Insert($fields, $extraFields);

    // recupera id para gerar representante de
    $fields['docId'] = $this->dbDriver->GetOne("SELECT LAST_INSERT_ID()");
    if($fields['type'] != 1)
      return;

    $this->SetInheritance($fields);
  }

  function Set($fields, $extraFields = array())
  {
    if($fields['representative']['docId']==NULL)
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>'representative',
				   'addMsg'=>' (Representante)'));
    $fields['representative'] = $fields['representative']['docId'];

    $oldFields = parent::Set($fields, $extraFields);

    // remove eventuais processos habilitados
    $SQL = "DELETE FROM LEGIS_Inheritance WHERE docId='".$fields['docId']."'";
    $this->dbDriver->ExecSQL($SQL);
    
    if($fields['type'] != 1)
      return $oldFields;

    $this->SetInheritance($fields);

    return $oldFields;
  }

  protected function ParseFieldDef(& $fieldDef,$disableFileRequired,$disableRequired)
  {
    parent::ParseFieldDef($fieldDef,$disableFileRequired,$disableRequired);

    switch($fieldDef['type'])
    {
    case 'license':
      // gambiarra pra evitar addslashes      
      $fieldDef['stripSlashes'] = true; 
      break;

    case 'naturalPersonCustomerRef':
      // gambiarra pra evitar addslashes      
      $fieldDef['stripSlashes'] = true; 
      break;
    }
  }

  protected function SetInheritance($fields)
  {
    $SQL = "INSERT INTO LEGIS_Inheritance ";
    $SQL .= "(docId, processId, enabledDate) VALUES ";
    $first = true;
    foreach($fields['license'] as $lItem)
    {
      if($lItem['enabledDate'] != NULL)
      {
	if($first)
	  $first = false;
	else
	  $SQL .= ', ';

	$SQL .= "('".$fields['docId']."','".$lItem['processId'];
	$SQL .= "','".$lItem['enabledDate']."')";
      }
    }

    if($first)
      return;

    $this->dbDriver->ExecSQL($SQL);      
  }


  protected function &GetField(& $fieldDef, $values, $keyFields,  
			       $templates, $sourceId)
  {
    switch ($fieldDef['type'])
    {
    case "license":
      return $this->GetLicense($fieldDef,$values,$keyFields,$sourceId);
    case "naturalPersonCustomerRef":
      return $this->GetCustomerRef($fieldDef,$values,$keyFields,$sourceId);
    default:
      return parent::GetField($fieldDef, $values, $keyFields,  
			      $templates, $sourceId);
    }
  }

  protected function &GetLicense($fieldDef,$values,$keyFields,$sourceId)
  {
    $modobj =  new AV_ModObject('field', $this->module->pageBuilder);
    $modobj->attrs = $fieldDef;
    $modobj->isArray = true;

    $pList = $this->GetCustomerFullProcessList($values['customer']);
    if($keyFields['docId']!=NULL)
    {
      $SQL = "SELECT docId, processId, enabledDate ";
      $SQL .= ' FROM LEGIS_Inheritance';
      $SQL .= " WHERE docId='".$keyFields['docId']."'";
      $inhtList = $this->dbDriver->GetAll($SQL);
    }

    foreach($pList as &$pItem)
    {
      foreach($inhtList as $inhKey=>$inhItem)
      {
	if($pItem['processId']==$inhItem['processId'])
	  $pItem['enabledDate'] = $inhItem['enabledDate'].' 00:00:00';
      }
      $pItem['number'] = explode(',',$pItem['number']);
    }

    $modobj->attrs['value'] = $pList;

    return $modobj;
  }

  function GetCustomerFullProcessList($entityId)
  {
    $customer =  new LEGIS_Customer('',$this->module);

    $customerRespTable = uniqid('LEGIS_CustomerRepresented_',false);
    $SQL = "CREATE TEMPORARY TABLE $customerRespTable ";
    $SQL .= '(docId INTEGER UNSIGNED NOT NULL) ';
    $SQL .= "SELECT $entityId as entityId ";
    $this->dbDriver->ExecSQL($SQL);

    try
    {
      $aggregator = $customer->GetMemberAggregator();

      $customer->GetRecursiveRepresented($customerRespTable);

      $SQL = 'SELECT p.processId,p.rootProcessId,rp.folder,rp.folderYear,';
      $SQL .= "(SELECT GROUP_CONCAT(number) FROM LEGIS_ProcessNumber pn ";
      $SQL .= "WHERE pn.processId=p.processId) AS number ";

      $SQL .= "FROM LEGIS_ProcessClient pc,$customerRespTable crt,";
      $SQL .= 'LEGIS_Process p, LEGIS_RootProcess rp ';
      if($aggregator['docId'] != NULL)
	$SQL .= ', LEGIS_ProcessClient pc2 ';

      $SQL .= "WHERE pc.entityId = crt.entityId ";
      $SQL .= 'AND pc.processId=p.processId ';
      $SQL .= 'AND p.rootProcessId=rp.processId ';
      if($aggregator['docId'] != NULL)
	$SQL.="AND pc.processId=pc2.processId AND pc2.entityId='$aggregatorId' ";
      $SQL .= 'ORDER BY processId DESC ';
      $pList = $this->dbDriver->GetAll($SQL);

      $this->dbDriver->ExecSQL("DROP TABLE $customerRespTable");
      return $pList;
    }
    catch(AV_Exception $e)
    {
      $this->dbDriver->ExecSQL("DROP TABLE $customerRespTable");
      throw $e;
    }
  }

  protected function &GetCustomerRef($fieldDef,$values,$keyFields,$sourceId)
  {
    $modobj =  new AV_ModObject('field', $this->module->pageBuilder);
    $modobj->attrs = $fieldDef;

    // retorna clientes pessoa física
    if($values[$fieldDef['fieldName']]!=NULL)
    {
      $docId = $values[$fieldDef['fieldName']];
      $SQL .= "SELECT name ";
      $SQL .= "FROM EM_NaturalPerson np ";
      $SQL .= "WHERE docId='$docId' ";
      
      $value['docId'] = $docId;
      $value['name'] = $this->dbDriver->GetOne($SQL);

      $SQL = 'SELECT s.situationId, s.name ';
      $SQL .= 'FROM LEGIS_CustomerSituation cs, LEGIS_PersonSituation s ';
      $SQL .= "WHERE cs.docId='$docId' AND cs.situationId=s.situationId ";
      $value['situationId'] = $this->dbDriver->GetAll($SQL);

      $modobj->attrs['value'] = $value;
    }
    return $modobj;
  }

  
}

?>