<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


//--Class: LIB_SetFolderOrder
//--Parent: AV_DBOperationObj
//--Desc: Represents module's object which implements get folder operation.
class LIB_SetFolderOrder extends AV_DBOperationObj
{
  //
  //--Public
  //

  //--Method: LoadFieldsDef
  //--Desc: Redefines parent method to define module's input fields
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'folderPath';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['stripSlashes'] = false;
    
    $this->fieldsDef[1]['fieldName'] = 'up';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['consistType'] = 'boolean';
    $this->fieldsDef[1]['default'] = '0';
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $folder = new LIB_Folder($this);
    $folder->SetOrder($this->fieldParser->fields['folderPath'], 
		      $this->fieldParser->fields['up']);
  }
}
?>
