<?php

require_once('UM_Node.inc');

//--Class: UM_User
//--Desc: Implements user operations.
class UM_User extends UM_Node
{
  protected $userType;

  //--Method: UM_User
  //--Desc: Constructor.
  function __construct(& $module)
  {
     parent::__construct('UM_User', $module);
     $this->userType = NULL;
  }

  function LoadUserTypeFieldsDef($typeId)
  {
    $this->userType = $this->CreateUserTypeTable($typeId);
    return $this->userType->LoadFieldsDef();
  }

  //--Method: Insert
  //--Desc: Inserts a new user. Returns operation status.
  function Insert(& $fields)
  {
    $extraFields = array('regDate' => 'NOW()',
			 'disabledDate' => 'NOW()');

    if($fields['password'] == NULL)
    {
      throw new AV_Exception(FIELD_REQUIRED, $this->lang,
			     array('fieldName'=>'password'));
    }

    parent::Insert($fields,$extraFields);

    if($this->userType != NULL)
      $this->userType->Insert($fields,array('userId'=>'LAST_INSERT_ID()'));
  }

  function Set($fields, $extraFields = array())
  {
    // if the logged user is trying to set his register without status and 
    // groups, then bypass permission check
    if(!isset($fields['status']) &&
       !isset($fields['groups']))
    {
      $memberData = $this->GetMemberData();
      if($memberData['userId'] == $fields['userId'])
	$oldValues = FORM_DynTable::Set($fields, $extraFields);
      else
	$oldValues = parent::Set($fields,$extraFields); 
    }
    else
      $oldValues = parent::Set($fields,$extraFields); 

    if($this->userType != NULL)
      $this->userType->Set($fields);
    return $oldValues;
  }

  function GetForm($keyFields, $fieldName, $sourceId, $templates,
  		   & $formObject, $defValues = array(),$dateFormat = NULL)
  {
    parent::GetForm($keyFields,$fieldName,$sourceId,$templates,
  		    $formObject,$defValues,$dateFormat);

    if($formObject->attrs['typeId'] != NULL)
    {
      $this->userType = $this->CreateUserTypeTable($formObject->attrs['typeId']);
      $this->userType->GetForm($keyFields,$fieldName,$sourceId,$templates,
			       $formObject,$defValues,$dateFormat);
    }

    ksort($formObject->children);
    return $formObject;
  }

  function RecoverPassword($emailFieldName,$login)
  {
    $SQL = "SELECT $emailFieldName FROM UM_User ";
    $SQL .= "WHERE login='$login' FOR UPDATE ";
    $res['email'] = $this->dbDriver->GetOne($SQL);
    if($res['email']==NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $res['password'] = mb_substr(md5(mt_rand()),0,8);
    $SQL = "UPDATE UM_User SET password='".$res['password']."' ";
    $SQL .= "WHERE login='$login' ";
    $this->dbDriver->ExecSQL($SQL);

    return $res;
  }

  function ChangeMemberEmail($email)
  {
    $memberData = $this->GetMemberData();

    // remove any older change not finalized
    $SQL = 'DELETE FROM UM_ValidateNewEmail ';
    $SQL .= "WHERE userId='".$memberData['userId']."'";
    $this->dbDriver->ExecSQL($SQL);

    $id = md5(mt_rand());
    $SQL ='INSERT INTO UM_ValidateNewEmail ';
    $SQL .= '(userId,keyValidate,email) VALUES ';
    $SQL .= "('".$memberData['userId']."','$id','$email');";
    $this->dbDriver->ExecSQL($SQL);

    return $id;
  }

  function ValidateNewEmail($emailFieldName,$id)
  {
    $memberData = $this->GetMemberData();

    $SQL = 'SELECT email FROM UM_ValidateNewEmail ';
    $SQL .= "WHERE userId='".$memberData['userId']."' ";
    $SQL .= "AND keyValidate='$id'";
    $email = $this->dbDriver->GetOne($SQL);
    if($email==NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $SQL = 'DELETE FROM UM_ValidateNewEmail ';
    $SQL .= "WHERE userId='".$memberData['userId']."'";
    $this->dbDriver->ExecSQL($SQL);

    $SQL = 'SELECT userId FROM UM_User ';
    $SQL .= "WHERE userId='".$memberData['userId']."' FOR UPDATE";
    $userId = $this->dbDriver->GetOne($SQL);
    if($userId==NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $SQL = "UPDATE UM_User SET $emailFieldName='$email' ";
    $SQL .= "WHERE userId='".$memberData['userId']."' ";
    $this->dbDriver->ExecSQL($SQL);

    return $email;
  }

  //--Method: GetMemberData
  //--Desc: Returns logged user data.
  function GetMemberData($lockMode = NULL)
  {
    $sessionH = & $this->module->pageBuilder->sessionH;

    // checks if user is logged
    if($sessionH->isGuest())
       throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    $returnFields = $this->GetReadableFields();

    $memberData = $sessionH->getMemberData();

    foreach($returnFields as $field)
      $returnData[$field] = $memberData[$field];

    return $returnData;
  }

  //--Method: GetMemberGroups
  //--Desc: Returns logged user groups.
  function GetMemberGroups()
  {
    $sessionH = & $this->module->pageBuilder->sessionH;

    // checks if user is logged
    if($sessionH->isGuest())
       throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    $SQL = 'SELECT UM_Group.* '; 
    $SQL .= 'FROM UM_UserGroup, UM_UserSessions, UM_Group WHERE ';
    $SQL .= "UM_UserSessions.sessionId = '" . $sessionH->sessionID . "' AND ";
    $SQL .= 'UM_UserSessions.userId = UM_UserGroup.userId AND ';
    $SQL .= 'UM_UserGroup.groupId = UM_Group.groupId  ';

    $groups = $this->dbDriver->GetAll($SQL);
    foreach($groups as &$group)
      if($group['permissions'] != NULL)
	$group['permissions'] = explode(',',$group['permissions']);

    return $groups;
  }



  //--Method: GetMemberGroupsIds
  //--Desc: Returns
  function GetMemberGroupsIds($lockMode)
  {
    $sessionH = & $this->module->pageBuilder->sessionH;

    // checks if user is logged
    if($sessionH->isGuest())
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    $SQL = 'SELECT UM_UserGroup.groupId FROM UM_UserGroup, UM_UserSessions ';
    $SQL .= 'WHERE UM_UserGroup.userId = UM_UserSessions.userId AND ';
    $SQL .= "UM_UserSessions.sessionId = '" . $sessionH->sessionID . "' ";
    $SQL .= 'ORDER BY UM_UserGroup.groupId ';
    switch($lockMode)
    {
    case 'share':
      $SQL .= ' LOCK IN SHARE MODE';
      break;
    case 'update':
      $SQL .= ' FOR UPDATE';
      break;
    }

    return $this->dbDriver->GetCol($SQL);
  }

  //--Method: UpdateUserSession()
  //--Desc: Updates user session login
  function UpdateUserSession()
  {
    $sessionH = & $this->module->pageBuilder->sessionH;

    $memberData = $this->GetMemberData('true');

    // updates user session date
    $SQL = 'UPDATE UM_UserSessions SET dateCreated=NOW() WHERE ';
    $SQL .= "sessionId = '" . $sessionH->sessionID . "'";
    $this->dbDriver->ExecSQL($SQL);

    return $memberData;
  }

  function FlushUserSessions()
  {
    $this->dbDriver->ExecSQL('DELETE FROM UM_UserSessions');
  }

  // returns NULL for old users and userId for new users
  function FacebookLogin($fields)
  {
    if($fields['login']==NULL)
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>'login'));
    $userId = NULL;
    $SQL = 'SELECT u.userId, password ';
    $SQL .= 'FROM UM_User u, UM_UserGroup ug ';
    $SQL .= "WHERE login='".$fields['login']."' AND u.userId=ug.userId ";
    $SQL .= "AND ug.groupId IN (".implode(',',$fields['groups']).") ";
    $row = $this->dbDriver->GetRow($SQL);

    if(count($row)>0)
      $fields['password'] = $row['password'];
    else
    {
      FORM_DynTable::Insert($fields);
      $userId = $this->dbDriver->GetOne('SELECT LAST_INSERT_ID()');
    }
    $this->Login($fields['login'],$fields['password']);
    return $userId;
  }

  function Logout()
  {
    $this->pageBuilder->sessionH->attemptLogout();
  }

  //--Method: Login
  //--Desc: Login an user 
  function Login($login, $password, $authCode=NULL)
  {
    $maintenance=$this->module->pageBuilder->siteConfig->getVar("flags",
								"maintenance");
    if($maintenance)
      throw new AV_Exception(MAINTENANCE, $this->lang);

    try
    {
      $this->dbDriver->ResumeTransaction();
    }
    catch(Exception $e)
    {
      throw new AV_Exception(MAINTENANCE, $this->lang);
    }


    $sessionH = &$this->module->pageBuilder->sessionH;
    if($sessionH->isMember())
      $sessionH->attemptLogout();

    $twoFactorAuth=$this->module->pageBuilder->siteConfig->getVar("config",
								  "twoFactorAuth");

    $SQL = 'SELECT userId, status ';
    if($twoFactorAuth)
      $SQL .= ',authSecret ';
    $SQL .= 'FROM UM_User WHERE ';
    $SQL .= "login = '" . $login . "'";
    $user = $this->dbDriver->GetRow($SQL);

    if($user['status'] == 0)
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);
    
    // check auth secret
    if($twoFactorAuth && $user['authSecret'] != NULL)
    {
      if($authCode == NULL)
	throw new AV_Exception(FIELD_REQUIRED, $this->lang,
			       array('fieldName'=>'authCode'));
      $ga = new PHPGangsta_GoogleAuthenticator();
      $checkResult = $ga->verifyCode($user['authSecret'], $authCode, 2);    // 2 = 2*30sec clock tolerance

      if(!$checkResult)
	throw new AV_Exception(PERMISSION_DENIED, $this->lang);

      $SQL = 'SELECT authCode FROM UM_UsedAuthSecret ';
      $SQL .= "WHERE userId='".$user['userId']."' ";
      $SQL .= "AND authCode='".$authCode."' ";
      $regCode = $this->dbDriver->GetOne($SQL);
      if($regCode != NULL)
	throw new AV_Exception(PERMISSION_DENIED, $this->lang);
    }

    if(!$sessionH->attemptLogin($login,$password))
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    if($twoFactorAuth && $user['authSecret'] != NULL)
    {
      $SQL = 'INSERT INTO UM_UsedAuthSecret ';
      $SQL .= '(userId, authCode) VALUES ';
      $SQL .= "('".$user['userId']."','".$authCode."');";
      $this->dbDriver->ExecSQL($SQL);

      $SQL = 'DELETE FROM UM_UsedAuthSecret WHERE id NOT IN (';
      $SQL .= 'SELECT id FROM (';
      $SQL .= 'SELECT id FROM UM_UsedAuthSecret ';
      $SQL .= "WHERE userId='".$user['userId']."' ";
      $SQL .= 'ORDER BY id DESC LIMIT 50) foo)';
      $this->dbDriver->ExecSQL($SQL);
    }

    return $user['userId'];
  }

  function InsTempUser(& $fields)
  {
    $registerCheckEmail = $this->module->pageBuilder->siteConfig->getVar("cms",
									"registerCheckEmail");
    $registerNotification = $this->module->pageBuilder->siteConfig->getVar("cms",
									   "registerNotification");
    // create user
    $extraFields = array('regDate' => 'NOW()',
			 'disabledDate' => 'NOW()');
    if($fields['password'] == NULL)
    {
      $addMsg = $this->GetFieldLabel(array('labelId'=>'passwordUMTag'));
      throw new AV_Exception(FIELD_REQUIRED, $this->lang,
			     array('fieldName'=>'password',
				   'addMsg'=>$addMsg));
    }
    $fields['status'] = $registerCheckEmail?'0':'1';
    FORM_DynTable::Insert($fields);

    // create user type
    if($this->userType != NULL)
      $this->userType->Insert($fields,array('userId'=>'LAST_INSERT_ID()'));

    if($registerNotification || $registerCheckEmail)
    {
      if($registerCheckEmail)
	{
	  // register temp user info
	  $userId = $this->dbDriver->GetOne('SELECT LAST_INSERT_ID()');
	  $key = md5($userId);
	  $SQL = 'INSERT INTO UM_TempUser (userId,keyValidate) VALUES ';
	  $SQL .= "(LAST_INSERT_ID(),'$key')";
	  $this->dbDriver->ExecSQL($SQL);

	  $link = dirname($_SERVER['PHP_SELF']);
	  if($link!='/') 
	    $link .= '/';
	  $link = 'http://'.$_SERVER['HTTP_HOST'].$link;
	  $link .= $fields['site']."?id=".$key;
	}

      // send email
      $email = $fields['email'];
      $subject = $fields['subject'];
 
      $message = "<!DOCTYPE html PUBLIC \"-";
      $message .= "//W3C//DTD XHTML 1.0 Strict//EN\" ";
      $message .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
      $message .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>";
      $message .= $fields['message'];
      if($registerCheckEmail)
	$message .= "<p><a href='$link'>$link</a></p>";
      $message .= '</body></html>';
      $headers .= "From: ".$fields['mailFrom']."\r\n";
      $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";
 
      if(!mail($email, $subject, $message, $headers ,"-r".$fields['mailFrom']))
      { 
	// Se "não for Postfix"
	$headers .= "Return-Path: " . $fields['mailFrom'] . "\r\n"; 
	if(!mail($email, $subject, $message, $headers))
	  throw new AV_Exception(FAILED, $this->lang);
      }
    }
 }


  function ValidateUser($id)
  {
    $SQL = "SELECT userId FROM UM_TempUser WHERE keyValidate = '$id' ";
    $SQL .= "AND keyModeration IS NULL FOR UPDATE";
    $row = $this->dbDriver->GetOne($SQL);
    if($row == NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $registerModerator = $this->module->pageBuilder->siteConfig->getVar("cms",
									"registerModerator");
    if($registerModerator == NULL)
    {
      $SQL = 'SELECT * FROM UM_User WHERE userId = ';
      $SQL .= $row.' FOR UPDATE';
      $temp = $this->dbDriver->GetRow($SQL);
    
      $SQL = 'UPDATE UM_User SET status = 1 WHERE userId = '.$row;
      $this->dbDriver->ExecSQL($SQL);

      $SQL = 'DELETE FROM UM_TempUser WHERE userId = ';
      $SQL .= $row." AND keyValidate = '$id'";
      $this->dbDriver->ExecSQL($SQL);

      return $temp;
    }

    $key = md5($id);
    $SQL = "UPDATE UM_TempUser SET keyModeration='$key' ";
    $SQL .= "WHERE userId='$row' AND keyValidate = '$id' ";
    $this->dbDriver->ExecSQL($SQL);

    // send email
    $mailTo = $registerModerator;
    $mailFrom = $this->pageBuilder->siteConfig->getVar("cms", "mailFrom");
    $langLabel = new LANG_Label($this->module);
    $labels = $langLabel->GetLabels(array('cmsUserModSubject',
					  'registerTag',
					  'cmsUserModMsg',
					  'yesTag','noTag'),$this->lang);

    $subject = $labels['cmsUserModSubject'];
 
    $link = dirname($_SERVER['PHP_SELF']);
    if($link!='/') 
      $link .= '/';
    $link = 'http://'.$_SERVER['HTTP_HOST'].$link;
    $link .= "cmsModerateUser.av?id=".$key;

    $keyFields['userId'] = $row;
    $formObject = new FORM_Object('form', $this->pageBuilder);
    $this->GetForm($keyFields, NULL,NULL,NULL, $formObject, $defValues);
    $cadastro = '<h2>'.$labels['registerTag'].'</h2>';
    foreach($formObject->children as & $child)
    {
      switch($child->attrs['type'])
      {
      case 'text':
	if($child->attrs['template']!='hiddenField')
	  $cadastro .= '<p><b>'.$child->attrs['label'].':</b> '.$child->attrs['value']."</p>\n";
	break;
      case 'boolean':
	if($child->attrs['template']!='hiddenField' &&
	   $child->attrs['fieldName']!='status')
	{

	  if(isset($child->attrs['value']))
	  {
	    if($child->attrs['value']=='1')
	      $aux = $labels['yesTag'];
	    else
	      $aux = $labels['noTag'];
	  }
	  $cadastro .= '<p><b>'.$child->attrs['label'].":</b> $aux</p>\n";
	}
	break;
      case 'select':
      case 'selectMult':
	$cadastro .='<p><b>'.$child->attrs['label'].':</b> '.$child->attrs['text']."</p>\n";
	break;
      default:
      }
    }

    $message = "<!DOCTYPE html PUBLIC \"-";
    $message .= "//W3C//DTD XHTML 1.0 Strict//EN\" ";
    $message .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
    $message .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>";
    $message .= $cadastro;
    $message .= '<p>'.$labels['cmsUserModMsg'].'</p>';
    $message .= "<p><a href='$link'>$link</a></p>";
    $message .= '</body></html>';
    $headers .= "From: $mailFrom\r\n";
    $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";
 
    if(!mail($mailTo, $subject, $message, $headers ,"-r".$fields['mailFrom']))
    { 
      // Se "não for Postfix"
      $headers .= "Return-Path: " . $fields['mailFrom'] . "\r\n"; 
      if(!mail($mailTo, $subject, $message, $headers))
	throw new AV_Exception(FAILED, $this->lang);
    }


  }

  function ModerateUser($id)
  {
    $SQL = "SELECT userId FROM UM_TempUser WHERE keyModeration = '$id' FOR UPDATE";
    $row = $this->dbDriver->GetOne($SQL);
    if($row == NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $SQL = 'SELECT * FROM UM_User WHERE userId = ';
    $SQL .= $row.' FOR UPDATE';
    $temp = $this->dbDriver->GetRow($SQL);
    
    $SQL = 'UPDATE UM_User SET status = 1 WHERE userId = '.$row;
    $this->dbDriver->ExecSQL($SQL);

    $SQL = 'DELETE FROM UM_TempUser WHERE userId = ';
    $SQL .= $row." AND keyModeration = '$id'";
    $this->dbDriver->ExecSQL($SQL);
    
    // send email
    $mailTo = $temp['login'];
    $mailFrom = $this->pageBuilder->siteConfig->getVar("cms", "mailFrom");
    $langLabel = new LANG_Label($this->module);
    $labels = $langLabel->GetLabels(array('activatedAccountTag',
					  'accessTag'),$this->lang);

    $subject = $labels['activatedAccountTag'];
 
    $loginPath = $this->pageBuilder->siteConfig->getVar("cms", "loginPath");
    $link = dirname($_SERVER['PHP_SELF']);
    if($link!='/') 
      $link .= '/';
    $link = 'http://'.$_SERVER['HTTP_HOST'].$link;
    $link .= "area/".$loginPath;

    $message = "<!DOCTYPE html PUBLIC \"-";
    $message .= "//W3C//DTD XHTML 1.0 Strict//EN\" ";
    $message .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
    $message .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>";
    $message .= $cadastro;
    $message .= '<p>'.$labels['accessTag'].'</p>';
    $message .= "<p><a href='$link'>$link</a></p>";
    $message .= '</body></html>';
    $headers .= "From: $mailFrom\r\n";
    $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";
 
    if(!mail($mailTo, $subject, $message, $headers ,"-r".$mailFrom))
    { 
      // Se "não for Postfix"
      $headers .= "Return-Path: " . $mailFrom . "\r\n"; 
      if(!mail($mailTo, $subject, $message, $headers))
      {
	$msg = " ".$pageLabels->attrs['sendEmailErrorTag'];
	throw new AV_Exception(FAILED,$this->lang);
      }
    }
  }

  public function InsAndroidDevice($regId)
  {
    require_once('PN_Server.inc');
    $pnServer = new PN_Server($this->pageBuilder);

    // search for notification_key
    $sessionId = $this->pageBuilder->sessionH->sessionID;
    $SQL = 'SELECT userId, gcm_notification_key ';
    $SQL .= 'FROM UM_User ';
    $SQL .= 'WHERE userId=(SELECT userId FROM UM_UserSessions ';
    $SQL .= "WHERE sessionId='$sessionId') ";
    $SQL .= 'LOCK IN SHARE MODE ';
    $userData = $this->dbDriver->GetRow($SQL);
    if(count($userData)==0)
      throw new AV_Exception(NOT_FOUND,$this->lang);

    $reCreate = false;

    // notification_key already exists
    // tries to add a new registration_id
    if($userData['gcm_notification_key'] != NULL)
    {
      try
      {
	$request = array( "operation" => "add",
			  "notification_key_name" => $userData['userId'],
			  "notification_key" => $userData['gcm_notification_key'],
			  "registration_ids" => array($regId) );
	$result = $pnServer->PostToGoogle('notification',$request);
      }
      catch(AV_Exception $e)
      {
	$data = $e->getData();
	if($data['httpStatus']!=400)
	  throw $e;

	$reCreate = true;
      }
    }


    // if notification_key does not exists or if need to re-create
    if($userData['gcm_notification_key'] == NULL || $reCreate)
    {
      // lock for update - only one may create notification at time
      $SQL = 'SELECT userId FROM UM_User ';
      $SQL .= "WHERE userId='".$userData['userId']."' ";
      $SQL .= 'FOR UPDATE ';
      $userId = $this->dbDriver->GetOne($SQL);
      if($userId==NULL)
	throw new AV_Exception(FAILED,$this->lang);

      // creates notification_key
      $request = array('operation' => 'create',
		       'notification_key_name' => $userData['userId'],
		       'registration_ids'=> array($regId) );
      $result = $pnServer->PostToGoogle('notification',$request);
      if($result['notification_key']==NULL)
	throw new AV_Exception(FAILED,$this->lang);
      // stores notification_key for sendind notifications
      $SQL = 'UPDATE UM_User ';
      $SQL .= "SET gcm_notification_key='".$result['notification_key']."' ";
      $SQL .= "WHERE userId='".$userData['userId']."' ";
      $this->dbDriver->ExecSQL($SQL);
    }
  }

  public function DelAndroidDevice($regId)
  {
    require_once('PN_Server.inc');
    $pnServer = new PN_Server($this->pageBuilder);

    // search for notification_key
    $sessionId = $this->pageBuilder->sessionH->sessionID;
    $SQL = 'SELECT userId, gcm_notification_key ';
    $SQL .= 'FROM UM_User ';
    $SQL .= 'WHERE userId=(SELECT userId FROM UM_UserSessions ';
    $SQL .= "WHERE sessionId='$sessionId') ";
    $SQL .= 'LOCK IN SHARE MODE ';
    $userData = $this->dbDriver->GetRow($SQL);
    if(count($userData)==0 || $userData['gcm_notification_key']==NULL)
      throw new AV_Exception(NOT_FOUND,$this->lang);

    // tries to add a new registration_id
    $request = array( "operation" => "remove",
		      "notification_key_name" => $userData['userId'],
		      "notification_key" => $userData['gcm_notification_key'],
		      "registration_ids" => array($regId) );
    $result = $pnServer->PostToGoogle('notification',$request);
  }

  function GetUserLogin($userId)
  {
    $SQL = "SELECT login FROM UM_User WHERE userId='$userId' ";
    return $this->dbDriver->GetOne($SQL);
  }

  function GetGroupName($groupId)
  {
    $SQL = "SELECT groupName FROM UM_Group WHERE groupId='$groupId' ";
    return $this->dbDriver->GetOne($SQL);
  }

  function SearchUsers($groupId, $filter)
  {
    $returnFields = $this->GetReadableFields();
    if(count($returnFields)==0)
      throw new AV_Exception(FAILED, $this->lang);

    $returnFields = $this->table.'.'.implode(','.$this->table.'.',
					     $returnFields);

    $SQL = "SELECT $returnFields FROM ".$this->table.',UM_UserGroup WHERE ';
    $SQL .= "UM_User.userId=UM_UserGroup.userId AND groupId=$groupId ";
    if($filter !== '' && $filter !== NULL)
      $SQL .= "AND login like '%$filter%'";
    $SQL .= ' ORDER BY login ';
    $SQL .= ' LIMIT 500 ';
    return $this->dbDriver->GetAll($SQL);    
  }


  protected function &CreateUserTypeTable($typeId)
  {
    $SQL = "SELECT tableName FROM UM_UserType WHERE typeId='$typeId' ";
    $tableName = $this->dbDriver->GetOne($SQL);
    if($tableName==NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang);
    return new FORM_DynTable($tableName,$this->module);
  }
}

?>
