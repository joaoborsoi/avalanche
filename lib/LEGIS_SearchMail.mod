<?php

require_once('LIB_Search.mod');
require_once('LEGIS_MailDoc.inc');

class LEGIS_SearchMail extends LIB_Search
{
  protected function LoadFieldsDef()
  {
    parent::LoadFieldsDef();
    
    $i = count($this->fieldsDef);

    $this->fieldsDef[$i]['fieldName'] = 'entityId';
    $this->fieldsDef[$i]['required'] = true;
    $this->fieldsDef[$i]['allowNull'] = false;
    $this->fieldsDef[$i]['consistType'] = 'integer';
  }

  protected function &CreateDocument()
  {
    return new LEGIS_MailDoc($this,$this->fields['entityId']);
  }
}

?>
