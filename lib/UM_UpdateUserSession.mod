<?php

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_UpdateUserSession extends AV_DBOperationObj
{
  protected function Execute()
  {
    $userMan = new UM_User($this);
    $this->attrs = $userMan->UpdateUserSession();
  }
}

?>