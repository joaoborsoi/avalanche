<?php


class PN_Server
{
  protected $pageBuilder;
    
  protected $googleApiKey;
  protected $googleApiUrl;

  protected $appleApiUrl;
  protected $privateKey;
  protected $privateKeyPassPhrase;

  public function __construct(& $pageBuilder)
  {
    $this->pageBuilder = & $pageBuilder;
    $this->lang = $this->pageBuilder->lang;

    $this->googleProjectId = $this->pageBuilder->siteConfig->getVar("pushNotification",
								    "googleProjectId");
    $this->googleApiKey = $this->pageBuilder->siteConfig->getVar("pushNotification",
								 "googleApiKey");
    $this->googleApiUrl = $this->pageBuilder->siteConfig->getVar("pushNotification",
								 "googleApiUrl");
  
    $this->appleApiUrl = $this->pageBuilder->siteConfig->getVar("pushNotification",
								"appleApiUrl");
    $this->privateKey = $this->pageBuilder->siteConfig->getVar("pushNotification",
							       "privateKey");
    $this->privateKeyPassPhrase = $this->pageBuilder->siteConfig->getVar("pushNotification",
									 "privateKeyPassPhrase");
  }

  
  public function PostToGoogle($resource, $request, $headers)
  {
    $headers = 
      array(
	    'project_id: '.$this->googleProjectId,
	    'Authorization: key=' . $this->googleApiKey,
	    'Content-Type: application/json'
	    );


    /* $this->pageBuilder->PrintLog('url:'.$this->googleApiUrl.'/'.$resource,false); */
    /* $this->pageBuilder->PrintLog('headers:'.json_encode($headers),false); */
    $this->pageBuilder->PrintLog('request:'.json_encode($request),false);

    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $this->googleApiUrl.'/'.$resource);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
    curl_setopt($ch, CURLOPT_FAILONERROR, true);

    // Execute post
    $result = curl_exec($ch);
    if(!$result)
    {
      $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      // Close connection
      curl_close($ch);
      throw new AV_Exception(FAILED,$this->lang,
			     array('httpStatus' => $status,
				   'addMsg' => '(httpStatus:'.$status.')'));
    }
   

    // Close connection
    curl_close($ch);

    $this->pageBuilder->PrintLog('result:'.$result,false);

    return json_decode($result,true);
  }

  public function PushToApple($deviceToken, $message)
  {
    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', $this->privateKey);
    stream_context_set_option($ctx, 'ssl', 'passphrase', $this->privateKeyPassPhrase);
    
    // Open a connection to the APNS server
    $fp = stream_socket_client(
			       $this->appleApiUrl,
			       $err,
			       $errstr,
			       60,
			       STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT,
			       $ctx);

    if (!$fp)
      exit("Failed to connect: $err $errstr" . PHP_EOL);

    echo 'Connected to APNS' . PHP_EOL;

    // Create the payload body
    $body['aps'] = array(
			 'alert' => $message,
			 'sound' => 'default',
			 'badge' => +1
			 );

    // Encode the payload as JSON
    $payload = json_encode($body);

    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
    
    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));

    // Close the connection to the server
    fclose($fp);

    return $result;
  }
}

?>