<?php

require_once('ADMIN_Popup.inc');

class ADMIN_GroupForm extends ADMIN_Popup
{
  function LoadPopup()
  {
    $this->pageBuilder->rootTemplate->addText('Grupo : Propriedades', 
					      'windowTitle');

    $formTpl = $this->pageBuilder->loadTemplate('adminGroupForm');
    $this->pageBuilder->rootTemplate->addTemplate($formTpl, 'content');

    $getGroup = $this->LoadModule('ADMIN_GetGroupForm');
    if($getGroup->status != SUCCESS)
      return "alert('".$getGroup->attrs['message']."');";
    $getGroup->PrintHTML($formTpl,NULL,'htmlentities_utf');
  }
}

?>