<?php

require_once('ADMIN_Users.inc');

class ADMIN_DelUser extends ADMIN_Users
{
  protected function LoadUsers()
  {
    try
    {
      if($_REQUEST['userId'] == '1')
	throw new AV_Exception(PERMISSION_DENIED, $this->lang);

      $delModule = $this->LoadModule('UM_DelUser');
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }    
    
    $script .= parent::LoadUsers();

    return $script;
  }

}

?>