<?php

require_once('AV_DBOperationObj.inc');

class PP_GetSubscription extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'userId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'dateFormat';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['default'] = '%a %e %b %Y %T';
    $this->fieldsDef[1]['stripSlashes'] = true;
  }

  protected function Execute()
  {
    // load subscription
    $SQL = 'SELECT subscr_id, value as subscr_option, ';
    $SQL .= "DATE_FORMAT(subscr_date,'".$this->fields['dateFormat'];
    $SQL .= "') as subscr_date ";
    $SQL .= 'FROM PP_Subscription s, PP_SubscriptionOption po, LANG_Label pol ';
    $SQL .= "WHERE userId='".$this->fields['userId']."' ";
    $SQL .= 'AND subscr_cancel_date IS NULL ';
    $SQL .= "AND subscr_eot = '0' ";
    $SQL .= 'AND subscr_option=docId ';
    $SQL .= 'AND title=labelId ';
    $SQL .= "AND lang='".$this->pageBuilder->lang."' ";
    $SQL .= 'ORDER BY s.subscr_date DESC LIMIT 1';
    $this->attrs = $this->dbDriver->GetRow($SQL);
    if(count($this->attrs)==0)
      throw new AV_Exception(NOT_FOUND, $this->pageBuilder->lang);
    
    // load last payment
    $SQL = 'SELECT value as payment_status, ';
    $SQL .= "DATE_FORMAT(payment_date,'".$this->fields['dateFormat'];
    $SQL .= "') as payment_date ";
    $SQL .= 'FROM PP_SubscriptionPayment sp, PP_PaymentStatus, LANG_Label ';
    $SQL .= "WHERE subscr_id='".$this->attrs['subscr_id']."' ";
    $SQL .= 'AND payment_status = status ';
    $SQL .= 'AND title=labelId ';
    $SQL .= "AND lang='".$this->pageBuilder->lang."' ";
    $SQL .= 'ORDER BY sp.payment_date DESC LIMIT 1 ';
    $this->attrs = array_merge($this->attrs, (array)$this->dbDriver->GetRow($SQL));
  }
}

?>