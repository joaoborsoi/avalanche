<?php

require_once('LIB_InsDoc.mod');
require_once('LEGIS_History.inc');

class LEGIS_InsHistory extends LIB_InsDoc
{
  protected function &CreateDocument()
  {
    return new LEGIS_History($this);
  }
}

?>