<?php

require_once('CMS_Content.inc');

class CMS_Event extends CMS_Content
{
  function LoadDocument(& $fields)
  {
    // loads document title
    $this->pageBuilder->rootTemplate->addText($fields['title'],'title');

    // add to history
    if($this->pageBuilder->siteConfig->getVar('cms','history'))
    {
      if($_REQUEST['docId'] != NULL)
	$_GET['link'] = 'content/'.$_REQUEST['docId'];
      else
	$_GET['link'] = 'area/'.$this->menuPath;
      $_GET['title'] = addslashes($fields['title']);
      $this->page->LoadModule('AV_AddHistory');
    }

    // event date
    $dateFormat = $this->pageBuilder->siteConfig->getVar('cms',
							 'eventDateFormat');
    $eventDate = strftime($dateFormat,(int)$fields['eventDate']);
    $content = "<h5 class='date'><small>$eventDate</small></h5>\n";

    // header
    $content .= '<h1>'.$fields['title']."</h1>\n";

    // load article's images
    $content .= $this->page->LoadImages($fields['imageList']);

    // content
    $content .= "<div id='fileContent'>\n";
    $content .= $fields['content'];
    $content .= "</div>\n";

     // loads main content to the page
    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }

  function LoadArea(&$fields, $path)
  {
    parent::LoadArea($fields,$path);

    // search for files at the folder
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
							 'eventDateFormat');
    $_GET['path'] = $path;
    $_GET['orderBy'] = 'eventDate';
    $_GET['order'] = '1';
    $_GET['returnFields'] = 'title,eventDate,descr';
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsPerPage');

    $_GET['exclusionFilters_numOfOptions'] = 1;
    $_GET['exclusionFilters_1__fieldName'] = 'eventDate';
    $_GET['exclusionFilters_1__value'] = date('Y-m-d H:i:s');
    if($_REQUEST['old'])
    {
      $_GET['exclusionFilters_1__operator'] = '<';
      $data = 'old=1';
    }
    else
      $_GET['exclusionFilters_1__operator'] = '>=';

    $search = $this->page->LoadModule('LIB_Search');

    // encoded menu path for the links
    $encMenuPath = str_replace("%2F", "/", 
			       urlencode(stripslashes($this->menuPath)));

    if($search->attrs['totalResults']>0)
    {
      $content .= $this->LoadList($search);
      $content .= $this->page->LoadSearchNav($search,$_REQUEST['offset'],
					     $_GET['limit'],
					     'area/'.$encMenuPath,NULL,$data);
    }

    if($_REQUEST['old'])
    {
      $content .= "<p><a href='area/$encMenuPath'>";
      $content .= $this->pageLabels->attrs['upcomingEventsTag'].'</a></p>';
    }
    else
    {
      $content .= "<p><a href='area/$encMenuPath?old=1'>";
      $content .= $this->pageLabels->attrs['oldEventsTag'].'</a></p>';
    }

    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }

  function LoadList($search)
  {
    $content .= "<ul class='news-list'>\n";
    foreach($search->children as & $child)
    {
      $content .= "<li>\n";
      $content .= $this->LoadListItem($child->attrs);
      $content .= "</li>\n";
    }
    $content .= "</ul>\n";
    return $content;
  }

  function LoadListItem(& $fields)
  {
    $content = "<h5>".$fields['eventDate']."</h5>\n";
    $content .= "<h4><a href='content/".$fields['docId']."'>";
    $content .= $fields['title']."</a></h4>\n";
    if($fields['descr']!=NULL)
      $content .= "<p>".$fields['descr']."</p>\n";
    return $content;
  }
}

?>
