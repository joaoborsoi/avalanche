<?php

//--Class: UM_GetUserForm
//--Parent: AV_DBOperationObj
//--Desc: Responsible for retrieving users data

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class ADMIN_GetUserForm extends AV_DBOperationObj
{
  //--Method: LoadFieldsDef
  //--Desc: Redefines parent method to define module's input fields
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'userId';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;

    $this->fieldsDef[1]['fieldName'] = 'fieldName';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;

    $this->fieldsDef[2]['fieldName'] = 'sourceId';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'integer';

    $this->fieldsDef[3]['fieldName'] = 'typeId';
    $this->fieldsDef[3]['required'] = false;
    $this->fieldsDef[3]['allowNull'] = true;
    $this->fieldsDef[3]['consistType'] = 'integer';
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    if($this->fields['fieldName'] != '')
    {
      $templates['select']['fileName'] = 'loadSelectField';
      $templates['selectOption']['fileName']  = 'loadSelectFieldOption';
      $templates['selectMult'] = 'adminUserEmptySelectMult';
      $templates['selectMultOption']  = 'adminUserSelectMultArray';
    }
    else
    {
      $templates['hidden']['fileName'] = 'hiddenField';
      $templates['hidden']['outputFuncRef'] = 'htmlentities_utf';
      $templates['password']['fileName']  = 'adminUserPassword';
      $templates['text']['fileName'] = 'textField';
      $templates['text']['outputFuncRef'] = 'htmlentities_utf';
      $templates['integer']['fileName'] = 'textField';
      $templates['integer']['outputFuncRef'] = 'htmlentities_utf';
      $templates['textArea']['fileName'] = 'textAreaField';
      $templates['textArea']['outputFuncRef'] = 'htmlentities_utf';
      $templates['boolean']['fileName']  = 'checkboxField';
      $templates['usageTerms']['fileName']  = 'usageTermsField';
      $templates['select']['fileName'] = 'selectField';
      $templates['selectMult']['fileName'] = 'selectMultField';
      $templates['selectMult']['outputFuncRef'] = 'htmlentities_utf';
      $templates['selectOption']['fileName']  = 'selectFieldOption';
      $templates['selectOption']['outputFuncRef'] = 'htmlentities_utf';
      $templates['selectMultOption']['fileName']  = 'selectMultFieldOption';
      $templates['float']['fileName'] = 'floatField';
      $templates['cnpj']['fileName']  = 'cnpjField';
      $templates['cpf']['fileName']  = 'cpfField';
      $templates['cep']['fileName']  = 'cepField';
      $templates['usageTerms']['fileName'] = 'usageTermsField';
      $templates['fileList']['fileName'] = 'imageListField';
      $templates['fileListItem']['fileName'] = 'imageListItem';
      $templates['percent']['fileName'] = 'percentField';

      if($this->pageBuilder->lang == 'en_US')
	$templates['date']['fileName'] = 'dateField_en_US';
      else
	$templates['date']['fileName'] = 'dateField';

      if($this->pageBuilder->lang == 'en_US')
	$templates['datetime']['fileName'] = 'dateTimeField_en_US';
      else
	$templates['datetime']['fileName'] = 'dateTimeField';
    }

    $userMan = new UM_User($this);

    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 

    if($this->fields['userId'] != NULL)
      $keyFields = Array('userId'=>$this->fields['userId']);
    else
      $keyFields = NULL;

    $defValues['status'] = '1';
    if($this->fields['typeId']!=NULL)
      $defValues['typeId'] = $this->fields['typeId'];
    $result = $userMan->GetForm($keyFields, $this->fields['fieldName'],
				$this->fields['sourceId'], $templates, 
				$formObject, $defValues);
    $this->children[] = $result;
  }
}

?>

