<?php

require_once('AV_DBOperationObj.inc');

class WIKI_Check extends AV_DBOperationObj
{
  protected function Execute()
  {
    // unset unlogged moderators from articles pending approval
   $loginTTL = $this->pageBuilder->siteConfig->getVar('members','loginTTL');
   $SQL = 'UPDATE WIKI_Document SET moderator=NULL ';
   $SQL .= "WHERE pendingApproval='1' AND moderator NOT IN ";
   $SQL .= '(SELECT userId FROM UM_UserSessions ';
   $SQL .= "WHERE UNIX_TIMESTAMP()-UNIX_TIMESTAMP(dateCreated)<=$loginTTL)";
   $this->dbDriver->ExecSQL($SQL);
  }
}

?>