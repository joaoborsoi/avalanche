<?php

// some usefull function
function str_is_int($str) 
{
  $var=intval($str);
  return ("$str"=="$var");
}

function FloatToStr($num, $fracDigits, $decimalSep, $thousandSep)
{
  $result = '';
  if($fracDigits > 0)
  {
    $numInt = (string)(int)$num;
    $numDec = (string)round(abs($num - $numInt)*pow(10,$fracDigits));
    while(mb_strlen($numDec) < $fracDigits)
      $numDec .= '0'; 
  }
  else
    $numInt = (string)round($num);
  $numChar = 0;
  for($i=mb_strlen($numInt)-1; $i >= 0;$i--)
  {
    if($numChar>0 && $numChar%3 == 0)
      $result = $thousandSep . $result;
    $result = $numInt[$i] . $result;
    $numChar++;
  }
  if($fracDigits > 0)
    $result = $result . $decimalSep . $numDec;
  return $result;
}

?>