<?php

require_once('CMS_Profile.inc');

class CMS_ProfileGroup extends CMS_Profile
{
  function LoadArea(&$fields,$path)
  {
    CMS_Content::LoadArea($fields,$path);

    $_GET['path'] = $path;
    $getFolderList = $this->page->LoadModule('LIB_GetFolderList');

    $_GET['orderBy'] = 'fixedOrder';
    $_GET['order'] = '1';
    $_GET['returnFields'] = 'title,bio,imageList';

    foreach($getFolderList->children as &$child)
    {
      // only profile folders
      if($child->attrs['folderTables'] != 18)
	continue;

      $content .= '<h3>' . $child->attrs['menuTitle'] . "</h3>\n";

      // search for files at the folder
      $_GET['path'] = $path . $child->attrs['name'];
 
      $search = $this->page->LoadModule('LIB_Search');
      if($search->attrs['totalResults']>0)
	$content .= $this->LoadList($search);
    }

    $this->pageBuilder->rootTemplate->addText((string)$content,'mainContent');
  }
}

?>
