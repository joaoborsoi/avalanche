<?php

class CMS_SearchTools
{
  function LoadSearchInfo(& $search, $offset, $limit)
  {
    $start = ($search->attrs['totalResults']>0)?$offset + 1:0;
    $end = $offset + $limit;
    
    if($end > $search->attrs['totalResults'])
      $end = $search->attrs['totalResults'];

    $content = "<p id='searchInfo'>$start - $end de ";
    $content .= $search->attrs['totalResults'];
    $content .= $this->pageLabels->attrs['resultsFoundTag']."</p>\n";
    return $content;
  }

  function LoadSearchNav(& $search, $offset, $limit, $link, 
				   $filter=NULL,$data=NULL)
  {
    if($search->attrs['totalResults']<=$limit)
      return;

    // prevents inifinte loop
    if($limit<=0)
      return;

    $nav = "<p class='pagination'>";
    for($i = 0; $i <= $limit*9 && $i < $search->attrs['totalResults'];
	$i += $limit)
    {
      if($offset == NULL && $i == 0 || $i == $offset)
	$nav .= '<strong>'.($i/$limit+1).'</strong>';
      else
      {
	$nav .= "<a href='$link?offset=$i";
	if($filter != NULL)
	  $nav .= '&amp;filter='.urlencode(stripslashes($filter));
	if($data != NULL)
	  $nav .= "&amp;$data";
	$nav .= "'>".($i/$limit+1).'</a>';
      }
    }
    if($offset < $limit*9 && 
       ($search->attrs['totalResults'] - $offset) > $limit)
    {
      $nav .= "<a href='$link?offset=".($offset + $limit);
      if($filter != NULL)
	$nav .= '&amp;filter='.urlencode(stripslashes($filter));
      if($data != NULL)
	$nav .= "&amp;$data";
      $nav .= "'>".$this->pageLabels->attrs['nextTag']." &raquo;</a>";
    }
    $nav .= "</p>\n";
    return $nav;
  }
}
?>