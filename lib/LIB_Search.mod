<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Document.inc');


//--Class: LIB_Search
//--Parent: AV_DBOperationObj
//--Desc: Represents module's object which implements library search operation.
class LIB_Search extends AV_DBOperationObj
{

  //--Method: LoadFieldsDef
  //--Desc: Redefines parent method to define module's input fields
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'path';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['default'] = '/';
    $this->fieldsDef[0]['stripSlashes'] = false;

    $this->fieldsDef[1]['fieldName'] = 'filter';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['stripSlashes'] = true;

    $this->fieldsDef[2]['fieldName'] = 'returnFields';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;

    $this->fieldsDef[3]['fieldName'] = 'searchCriteria';
    $this->fieldsDef[3]['required'] = false;
    $this->fieldsDef[3]['allowNull'] = true;

    $this->fieldsDef[4]['fieldName'] = 'exclusionFilters';
    $this->fieldsDef[4]['list'] = true;
    $this->fieldsDef[4]['sizeVarName'] = 'exclusionFilters_numOfOptions';
    $subfieldDef = array();
    $subfieldDef[0]['fieldName'] = 'fieldName';
    $subfieldDef[0]['required'] = true;
    $subfieldDef[0]['allowNull'] = false;
    $subfieldDef[1]['fieldName'] = 'value';
    $subfieldDef[1]['required'] = false;
    $subfieldDef[1]['allowNull'] = true;
    $subfieldDef[2]['fieldName'] = 'operator';
    $subfieldDef[2]['required'] = false;
    $subfieldDef[2]['allowNull'] = true;
    $subfieldDef[2]['default'] = '=';
    $this->fieldsDef[4]['fieldsDef'] = $subfieldDef;

    $this->fieldsDef[5]['fieldName'] = 'orderBy';
    $this->fieldsDef[5]['required'] = false;
    $this->fieldsDef[5]['allowNull'] = true;
    $this->fieldsDef[5]['default'] = 'matches';

    $this->fieldsDef[6]['fieldName'] = 'order';
    $this->fieldsDef[6]['required'] = false;
    $this->fieldsDef[6]['allowNull'] = true;
    $this->fieldsDef[6]['consistType'] = 'boolean';
    $this->fieldsDef[6]['default'] = true;

    $this->fieldsDef[7]['fieldName'] = 'recursiveSearch';
    $this->fieldsDef[7]['required'] = false;
    $this->fieldsDef[7]['allowNull'] = true;
    $this->fieldsDef[7]['default'] = false;

    $this->fieldsDef[8]['fieldName'] = 'dateFormat';
    $this->fieldsDef[8]['required'] = false;
    $this->fieldsDef[8]['allowNull'] = true;
    $this->fieldsDef[8]['default'] = '%c';

    $this->fieldsDef[9]['fieldName'] = 'limit';
    $this->fieldsDef[9]['required'] = false;
    $this->fieldsDef[9]['allowNull'] = true;
    $this->fieldsDef[9]['consistType'] = 'integer';

    $this->fieldsDef[10]['fieldName'] = 'offset';
    $this->fieldsDef[10]['required'] = false;
    $this->fieldsDef[10]['allowNull'] = true;
    $this->fieldsDef[10]['consistType'] = 'integer';

    $this->fieldsDef[11]['fieldName'] = 'tempTable';
    $this->fieldsDef[11]['required'] = false;
    $this->fieldsDef[11]['allowNull'] = true;
    $this->fieldsDef[11]['consistType'] = 'boolean';

    $this->fieldsDef[12]['fieldName'] = 'numberFormat';
    $this->fieldsDef[12]['required'] = false;
    $this->fieldsDef[12]['allowNull'] = true;
    $this->fieldsDef[12]['consistType'] = 'boolean';
    $this->fieldsDef[12]['default'] = true;

    $this->fieldsDef[13]['fieldName'] = 'detailedFileList';
    $this->fieldsDef[13]['required'] = false;
    $this->fieldsDef[13]['allowNull'] = true;
    $this->fieldsDef[13]['consistType'] = 'boolean';
    $this->fieldsDef[13]['default'] = false;

    $this->fieldsDef[14]['fieldName'] = 'totalResults';
    $this->fieldsDef[14]['required'] = false;
    $this->fieldsDef[14]['allowNull'] = true;
    $this->fieldsDef[14]['consistType'] = 'boolean';
    $this->fieldsDef[14]['default'] = true;

  }

  protected function &CreateDocument()
  {
    return new LIB_Document($this);
  }

  protected function Execute()
  {
    $this->isArray = true;

    if($this->fields['returnFields'] != NULL)
      $this->fields['returnFields'] = explode(',',$this->fields['returnFields']);
    if($this->fields['searchCriteria'] != NULL && 
       $this->fields['searchCriteria'] != '*')
      $this->fields['searchCriteria'] = 
	explode(',',$this->fields['searchCriteria']);

    $exclusionFilters = NULL;
    foreach($this->fields['exclusionFilters'] as &$field)
      $exclusionFilters[$field['fieldName']][] = $field;
    $this->fields['exclusionFilters'] = $exclusionFilters;

    $doc = & $this->CreateDocument();
    $rows = & $doc->Search($this->fields);

    // load children
    $this->LoadChildrenFromRows($rows, 'doc');
    if($this->fields['totalResults'])
      $this->attrs['totalResults'] = $this->fields['totalResults'];
  }
}

?>
