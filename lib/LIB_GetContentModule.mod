<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Folder.inc');


class LIB_GetContentModule extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'folderTables';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $folder = new LIB_Folder($this);
    $this->attrs['contentModule'] = 
      $folder->GetContentModule($this->fields['folderTables']);
  }
}

?>
