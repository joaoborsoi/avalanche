<?php

require_once('CMS_Generic.inc');

class CMS_Gallery extends CMS_Generic
{
  function LoadDocument(& $fields)
  {
    // loads document title
    $title = htmlentities_utf($fields['title']);
    $this->pageBuilder->rootTemplate->addText($title,'title');

    // saves history
    if($this->pageBuilder->siteConfig->getVar('cms','history'))
    {
      if($_REQUEST['docId'] != NULL)
	$_GET['link'] = 'content/'.$_REQUEST['docId'];
      else
	$_GET['link'] = 'area/'.$this->menuPath;
    
      $_GET['title'] = addslashes($fields['title']);
      $this->page->LoadModule('AV_AddHistory');
    }

    // header
    $content = "<h1>$title</h1>\n";

    // content
    $content .= $fields['content'];

    // loads image list
    $content .= $this->page->LoadImages($fields['imageList'],'thumb');

    // loads to the page template
    $this->pageBuilder->rootTemplate->addText($content,'mainContent');

  }

  function LoadArea(&$fields, $path)
  {
    // loads document title
    $title = htmlentities_utf($fields['title']);
    $this->pageBuilder->rootTemplate->addText((string)$title,'title');

    // folder content title
    $content = "<h1>$title</h1>\n";

    // folder content
    $content .= $fields['content'];

    // loads image list
    $content .= $this->page->LoadImages($fields['imageList'],'thumb');

    // prints area generic information
    $this->pageBuilder->rootTemplate->addText($content,'mainContent');

    // subareas
    $this->page->LoadSubareas();
  }

}
?>