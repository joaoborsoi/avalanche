<?php

//--Class: UM_GetMemberGroups
//--Parent: AV_DBOperationObj
//--Desc: Responsible for retrieving users data

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class UM_GetMemberGroups extends AV_DBOperationObj
{
  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $this->isArray = true;

    // get rows
    $userMan = new UM_User($this);
    
    $rows = $userMan->GetMemberGroups($status);

    if($status != SUCCESS)
      return $status;
    
    $this->LoadChildrenFromRows($rows, 'group', 'numOfGroups');

    return SUCCESS;
  }
}

?>

