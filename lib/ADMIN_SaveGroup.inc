<?php

require_once('ADMIN_Page.inc');

class ADMIN_SaveGroup extends ADMIN_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminScript');

    try
    {
      if($_REQUEST['groupId'] == NULL)
	$saveFile = $this->LoadModule('UM_InsGroup');
      else
	$saveFile = $this->LoadModule('UM_SetGroup');

      $script .= 'if(parent.opener) ';
      $script .= "parent.opener.igroups.location = 'index.php?pageId=adminGroups';";
      $script .= 'parent.close();';
    }
    catch(Exception $e)
    {
      $script .= "alert('".$e->getMessage()."');";
    }    

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }
}

?>