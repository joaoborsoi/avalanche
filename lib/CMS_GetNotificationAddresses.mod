<?php

require_once('CMS_RssPrefs.inc');

class CMS_GetNotificationAddresses extends AV_DBOperationObj
{
  // disable transaction - only consulting
  function __construct(&$pbuilder, $itemTpl, $itemTplTag, $varPrefix, $jsonInput=false)
  {
    parent::__construct($pbuilder, $itemTpl, $itemTplTag, $varPrefix, $jsonInput,false);
  }

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'event';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;
    $this->fieldsDef[1]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $table = new CMS_RssPrefs('CMS_RSSPrefs',$this);

    $this->attrs['addresses'] = 
      $table->GetNotificationAddresses($this->fields['docId'],
				       $this->fields['event']);
  }
}
?>