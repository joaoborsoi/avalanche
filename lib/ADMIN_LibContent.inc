<?php

require_once('ADMIN_Content.inc');

class ADMIN_LibContent extends ADMIN_Content
{
  protected function LoadTabContent(& $contentTpl)
  {
    $libContentTpl = $this->pageBuilder->loadTemplate('adminLib');
    $this->pageLabels->PrintHTML($libContentTpl, NULL, 'htmlentities_utf');
    $libContentTpl->addText($this->pageBuilder->pageId, 'pageId');
    $contentTpl->addTemplate($libContentTpl, 'tabContent');
  }

  protected function LoadFooter(& $contentTpl)
  {
    $libFooterTpl = $this->pageBuilder->loadTemplate('adminLibFooter');
    $this->pageLabels->PrintHTML($libFooterTpl, NULL, 'htmlentities_utf');
    $contentTpl->addTemplate($libFooterTpl, 'footer');

    switch($this->pageBuilder->pageId)
    {
    case 'adminContent':
      $orderFooterTpl=$this->pageBuilder->loadTemplate('adminLibOrderFooter');
      $this->pageLabels->PrintHTML($orderFooterTpl, NULL, 'htmlentities_utf');
      $libFooterTpl->addTemplate($orderFooterTpl, 'orderFooter');
      break;

    case 'adminLib':
      $openBtn=$this->pageBuilder->loadTemplate('adminLibOpenBtn');
      $this->pageLabels->PrintHTML($openBtn, NULL, 'htmlentities_utf');
      $libFooterTpl->addTemplate($openBtn, 'adminLibOpenBtn');
      break;

    }
  }
}

?>