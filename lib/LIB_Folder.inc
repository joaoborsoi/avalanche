<?php

require_once('LIB_Node.inc');
require_once('LIB_Document.inc');

//--Class: LIB_Folder
//--Desc: Library folder class.
class LIB_Folder extends LIB_Node
{
  protected $folderTable;              //--Desc: Folder database table

  //
  //--Public
  //

  //--Method: __constructor
  //--Desc: Constructor.
  function __construct(&$module)
  {
    parent::__construct($module);
    $this->folderTable = new FORM_DynTable("LIB_Folder", $module);
  }

  function LoadFieldsDef()
  {
    $fieldsDef[0]['fieldName'] = 'folderPath';
    $fieldsDef[0]['required'] = true;
    $fieldsDef[0]['allowNull'] = false;
    $fieldsDef[0]['default'] = '/';
    $fieldsDef[0]['stripSlashes'] = false;

    return array_merge($fieldsDef,
		       (array)$this->folderTable->LoadFieldsDef(), 
		       (array)$this->nodeTable->LoadFieldsDef());

  }

  function GetContentModule($folderTables)
  {
    $SQL = 'SELECT contentModule FROM LIB_FolderType ';
    $SQL .= "WHERE id='$folderTables' ";
    return $this->dbDriver->GetOne($SQL);
  }

  function GetOrderAsc($path)
  {
    $path = $this->PreparePath($path);
    $SQL = 'SELECT orderAsc FROM LIB_Folder f, LIB_FolderType ft ';
    $SQL .= "WHERE path='$path' AND folderTables=id ";
    return $this->dbDriver->GetOne($SQL);
  }

  function &GetForm($keyFields, $fieldName, $sourceId, $templates, 
		    & $formObject, $defValues = array(),$dateFormat = NULL)
  {
    $this->LoadFieldsDef();
 
    if($keyFields != NULL)
      $keyFields['nodeId'] = $keyFields['folderId'];
    parent::GetForm($keyFields,$fieldName,$sourceId,$templates,$formObject,
		    $defValues,$dateFormat);

    $this->folderTable->GetForm($keyFields,$fieldName,$sourceId,$templates, 
				$formObject,$defValues,$dateFormat);

    // language tab
    $langList = $this->label->GetLangList();
    $langTab = new FORM_Object('field', $this->module->pageBuilder, 
				 $templates, 'langTabItem');
    $langTab->attrs['fieldName'] = 'langTab';
    $langTab->attrs['type'] = 'langTab';
    $langTab->attrs['adminLangTitle']=$this->label->GetLabel('adminLangTitle',
							     $this->lang);

    // creates tab items
    foreach($langList as $key => $lang)
    {
      $field = new AV_ModObject('tab', $this->module->pageBuilder);
      $field->attrs['type'] = 'langTabItem';
      $field->attrs['lang'] = $lang['lang'];
      $field->attrs['label'] = mb_substr($lang['lang'],0,2);
      $langTab->children[] = $field;
    }
    $formObject->children[500] = & $langTab;

    ksort($formObject->children);

    return $formObject;
  }


  //--Method: Insert
  //--Desc: Inserts new folder to the library
  function Insert($fields)
  {
    $folderName = $fields['path'];

    // insere node
    $fields['folderPath'] = $this->PreparePath($fields['folderPath']);
    $fields['path'] = $fields['folderPath'];
    $folderData = parent::Insert($fields);
      
    // Retrieves folderId to be returned
    $folderId = $this->dbDriver->GetOne('SELECT LAST_INSERT_ID() FROM DUAL');
    if($folderId === NULL)
      throw new AV_Exception(FAILED, $this->lang);

    // sets new folder name and calcultes level
    $fields['path'] .= "$folderName/";
    $level = count_chars($fields['path']);
    $level = $level[ord('/')];

    $extraFields['folderId'] = $folderId;
    $extraFields['level'] = $level;
    $extraFields['fixedOrder'] = $folderId;

    // if not coming from interface, set as the parent folder
    if($fields['folderTables'] == NULL)
      $extraFields['folderTables'] = $folderData['folderTables'];
    
    $this->folderTable->Insert($fields, $extraFields);

    $this->NotifyAdmin('adminInsFolderLogTag',$fields);

    return $folderId;
  }

  function CheckFolderPermission($path, $write = false)
  {
    // checks out for write permission
    $this->CheckNodePermission($this->GetFolderData($path, 'update'),
			       $write);
  }

  //--Method: Set
  //--Desc: Renames the folder
  function Set($fields)
  {
    $fields['nodeId'] = $fields['folderId'];
    parent::Set($fields);

    // locks LIB_Folder tables
    $SQL = 'SELECT folder.path FROM LIB_Folder folder, LIB_Folder subFolders ';
    $SQL .= "WHERE folder.folderId = '".$fields['folderId']."' AND ";
    $SQL .= "subFolders.path LIKE CONCAT(folder.path,'%') FOR UPDATE";
    $folderList = $this->dbDriver->GetCol($SQL);
    if(count($folderList) == 0)
      throw new AV_Exception(INVALID_FIELD, $this->lang, 
			     array('fieldName'=>'folderId'));

    $folderPath = addslashes($folderList[0]);
    if(mb_substr($folderPath,- 1) == '/')
      $folderPath = mb_substr($folderPath, 0, mb_strlen($folderPath) - 1);
    $path = mb_substr($folderPath, 0, mb_strrpos($folderPath, '/') + 1);
    $newFolderPath = $path . $fields['path'];
    $folderPathLen = mb_strlen(stripslashes($folderPath)) + 1;

    // checks parent folder permission
    $this->CheckFolderPermission($path,true);

    // updates path from LIB_Folder
    $SQL = 'UPDATE LIB_Folder ';
    $SQL .= "SET path = IF(path = '$folderPath', '$newFolderPath', ";
    $SQL .= "CONCAT('$newFolderPath', SUBSTR(path, $folderPathLen))) ";
    $SQL .= "WHERE path LIKE '$folderPath%'";
    $this->dbDriver->ExecSQL($SQL);

    // removes temporarily path field beacuse it contains only the path name
    // and doesn't need to be set here
    $aux = $fields['path'];
    unset($fields['path']);
    // save other fields
    $this->folderTable->Set($fields);
    // reset path field
    $fields['path'] = $aux;

    $this->NotifyAdmin('adminSetFolderLogTag',$fields);
  }


  //--Method: DelFolder
  //--Desc: Deletes a library folder
  function Del($fields)
  {
    // searchs for folder full path
    $SQL = "SELECT path FROM LIB_Folder WHERE ";
    $SQL .= "folderId = '".$fields['folderId']."' FOR UPDATE";
    $folderPath = $this->dbDriver->GetOne($SQL);
    if($folderPath == '/' || $folderPath == '' || $folderPath === NULL)
      throw new AV_Exception(INVALID_FIELD, $this->lang, 
			     array('fieldName'=>'folderId'));

    $folderPath = addslashes($folderPath);
    // check for subfolders
    $SQL = "SELECT COUNT(*) FROM LIB_Folder WHERE path LIKE '$folderPath%'";
    $subFolders = $this->dbDriver->GetOne($SQL);
    if($subFolders > 1)
      throw new AV_Exception(FOLDER_NOT_EMPTY, $this->lang);

    // check for folder documents
    $SQL = 'SELECT COUNT(*) FROM LIB_DocumentLink WHERE ';
    $SQL .= "folderId = '".$fields['folderId']."' ";
    $docs = $this->dbDriver->GetOne($SQL);
    if($docs > 0)
      throw new AV_Exception(FOLDER_NOT_EMPTY, $this->lang);


    // finds parent folder
    $folderPath = mb_substr($folderPath, 0, mb_strrpos($folderPath, '/'));
    $fields['path'] = mb_substr($folderPath, 0, mb_strrpos($folderPath, '/') + 1);
    $fields['nodeId'] = $fields['folderId'];

     // checks out for write permission
    $folderData = $this->GetFolderData($fields['path'], 'share');
    $data = $this->CheckNodePermission($folderData, true);

    // checks restricted deletion flag
    $this->CheckRestrictedDeletion($folderData, $data['memberData'], 
				   $fields['folderId']);


    parent::Del($fields);

    $this->NotifyAdmin('adminDelFolderLogTag',$fields);
  }

  //--Method: DelFolder
  //--Desc: Deletes a library folder
  function Move($fields)
  {
    // searchs for folder full path
    $SQL = "SELECT path, level, fixedOrder FROM LIB_Folder WHERE ";
    $SQL .= "folderId = '".$fields['folderId']."' FOR UPDATE";
    $folder = $this->dbDriver->GetRow($SQL);
    if($folder == NULL || count($folder)==0 || $folder['path'] == '/')
      throw new AV_Exception(INVALID_FIELD, $this->lang, 
			     array('fieldName'=>'folderId'));
    $folder['path'] = addslashes($folder['path']);
    $folderPathLen = mb_strlen(stripslashes($folder['path']));

    // finds parent folder
    $parentFolder = mb_substr($folder['path'], 0, mb_strrpos($folder['path'], '/'));
    $parentFolder = mb_substr($parentFolder, 0, mb_strrpos($parentFolder, '/') + 1);

     // checks parent folder for write permission
    $parentFolderData = $this->GetFolderData($parentFolder, 'share');
    $parentData = $this->CheckNodePermission($parentFolderData, true);
    $parentFolderPathLen = mb_strlen(stripslashes($parentFolder));

    // checks restricted deletion flag
    $this->CheckRestrictedDeletion($parentFolderData, $parentData['memberData'], 
				   $fields['folderId']);


    // checks target 
    $targetFolderData = $this->GetFolderData($fields['path'], 'share');
    $targetFolderPathLen = mb_strlen(stripslashes($fields['path']));

    // checks if target is not a source children folder
    if(mb_substr($fields['path'],0,mb_strlen($folder['path'])) == $folder['path'])
    {
      $impMoveFolder = $this->label->GetLabel('impMoveFolder', $this->lang);
      throw new AV_Exception(FAILED, $this->lang,array('addMsg'=>" ($impMoveFolder)"));
    }

    // checks target path permission
    $targetData = $this->CheckNodePermission($targetFolderData, true);

    // locks source folders
    $SQL = "SELECT * FROM LIB_Node n, LIB_Folder f ";
    $SQL .= "WHERE SUBSTR(path,1,$folderPathLen) = '".$folder['path']."' ";
    $SQL .= 'AND nodeId=folderId FOR UPDATE';
    $this->dbDriver->GetAll($SQL);

    // find  index order (MAX(fixedOrder))
    $order = NULL;
    if($this->dbDriver->type=='mysql')
    {
      $SQL = 'SELECT MAX(fixedOrder) as fixedOrder FROM LIB_Folder ';
      $SQL .= "WHERE level = ".($targetFolderData['level']+1).' ';
      $SQL .= "AND SUBSTR(path,1,$targetFolderPathLen) = '".$fields['path']."' ";
      $SQL .= 'FOR UPDATE ';
      $order = $this->dbDriver->GetOne($SQL);
      $order = $order + 1;
    }

    // move folder and subfolders
    $levelDiff = $targetFolderData['level']-$parentFolderData['level'];
    $SQL = "UPDATE LIB_Folder ";
    $SQL .= "SET path=CONCAT('".$fields['path']."',SUBSTR(path,$parentFolderPathLen+1)),";
    $SQL .= 'level=level'.(($levelDiff>=0)?('+'.$levelDiff):$levelDiff).' ';
    if($order!=NULL)
      $SQL .= ',fixedOrder = ' . $order . ' ';
    $SQL .= "WHERE SUBSTR(path,1,$folderPathLen) = '".$folder['path']."' ";
    $this->dbDriver->ExecSQL($SQL);

    $this->NotifyAdmin('adminMoveFolderLogTag',$fields);
  }

  //--Method: Get
  //--Desc: Gets folders
  function GetFolderList($path, $fixedOrder = false)
  {
    $folderData = $this->GetFolderData($path, 'share');

    $level = count_chars($path);
    $level = $level[ord('/')] + 1;

    // checks out for read permission
    $data = $this->CheckNodePermission($folderData, false);
    $groups = implode(',',$data['memberGroups']);

    $pathLen = mb_strlen(stripslashes($path)) + 1;
    $SQL .= "SELECT DISTINCT SUBSTR(SUBSTR(path, $pathLen), 1, ";
    $SQL .= "INSTR(SUBSTR(path, $pathLen),'/')-1) as name, ";
    $SQL .= 'folderId,folderTables,content,LIB_Node.userId,';
    $SQL .= 'LIB_Node.groupId, userRight,groupRight,otherRight, ';
    $SQL .= '(SELECT value FROM LANG_Label WHERE menuTitle=labelId AND ';
    $SQL .= "lang='" . $this->module->pageBuilder->lang . "') as menuTitle, ";
    $SQL .= 'fixedOrder, '; // compatibilização com oracle
    $SQL .= '(SELECT views FROM LIB_FolderType where id=folderTables) as views ';
    $SQL .= 'FROM LIB_Node, LIB_Folder ';
    $SQL .= 'WHERE '; 
    $SQL .=       'nodeId = folderId AND ';
    $SQL .=       "level = $level AND ";
    $SQL .=       "path LIKE '$path%' AND ";
    $SQL .=       "SUBSTR(path, $pathLen) <> '' ";

    if(isset($data['memberData']['userId']))
    {
      // check node permission
      $SQL .= ' AND ';
      $SQL .= 'IF(LIB_Node.userId='.$data['memberData']['userId'].',';
      $SQL .= "INSTR(LIB_Node.userRight,'r'),";
    
      $SQL .= "IF(LIB_Node.groupId IN ($groups),";
      $SQL .= "INSTR(LIB_Node.groupRight,'r'),";
      
      $SQL .= "IF((SELECT SUM(IF(ngr.groupId IN ($groups),";
      $SQL .= "INSTR(ngr.groupRight,'r'),0)) FROM LIB_NodeGroupRights ngr ";
      $SQL .= "WHERE ngr.nodeId=LIB_Node.nodeId)<>0,1, ";
      
      $SQL .= "INSTR(LIB_Node.otherRight,'r'))))<>0 ";
    }
    else
    {
      $SQL .= " AND INSTR(LIB_Node.otherRight,'r')<>0 ";
    }

    if($fixedOrder)
	$SQL .= 'ORDER BY fixedOrder ';
      else
	$SQL .= 'ORDER BY name ';

    $rows = $this->dbDriver->GetAll($SQL);

    return $rows;
  }

  function GetFolderName($folderId)
  {
    $SQL = "SELECT path FROM LIB_Folder WHERE folderId = '$folderId'";
    $path_tmp = $this->dbDriver->GetOne($SQL);
                  
    if($path_tmp == '/') 
       return ''; 
   
    if($path_tmp === NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang);
                   
    $array = explode('/', $path_tmp); 
    $name = $array[count($array) - 2];
    
    return $name;    
  }

  function GetFolderPath($folderId)
  {
    $SQL = "SELECT path FROM LIB_Folder WHERE folderId = '$folderId'";
    $path = $this->dbDriver->GetOne($SQL);
                  
    if($path === NULL)
      throw new AV_Exception(NOT_FOUND, $this->lang);
                   
    return $path;    
  }


  //--Method: SetOrder
  function SetOrder($folderPath, $up)
  {
    if($folderPath == '/' || $folderPath == '' || $folderPath === NULL)
      throw new AV_Exception(INVALID_PATH, $this->lang);

    // finds parent folder
    if($folderPath[0] != '/')
      $folderPath = '/' . $folderPath;
    if(mb_substr($folderPath,- 1) == '/')
      $folderPath = mb_substr($folderPath,0,mb_strrpos($folderPath, '/'));

    $parentFolderPath=mb_substr($folderPath,0,mb_strrpos($folderPath, '/')+1);
    $parentFolderData = $this->GetFolderData($parentFolderPath, 'share');

    // checks out for write permission
    $this->CheckNodePermission($parentFolderData, true);

    $folderData = $this->GetFolderData($folderPath, 'update');
    
    $folderId = $folderData['folderId'];
    $currPos = $folderData['fixedOrder'];
    $level = $folderData['level'];

    if($currPos == NULL)
      throw new AV_Exception(FAILED, $this->lang);
    

    if($this->dbDriver->type=='oracle')
    {
      // for oracle, lock all subfolders
      $SQL = 'SELECT fixedOrder FROM LIB_Folder ';
      $SQL .= "WHERE level=$level AND path like '$parentFolderPath%' ";
      $SQL .= 'FOR UPDATE';
      $this->dbDriver->ExecSQL($SQL);
    }

    // finds and locks next doc by position
    $SQL = 'SELECT folderId, fixedOrder FROM LIB_Folder ';
    $SQL .= "WHERE level=$level AND path like '$parentFolderPath%' AND ";
    if($up == 1)
    {
      $SQL .= "fixedOrder<$currPos ";
      $SQL .= "ORDER BY fixedOrder DESC ";
    }
    else
    {
      $SQL .= "fixedOrder>$currPos ";
      $SQL .= "ORDER BY fixedOrder ASC ";
    }
    $SQL .= "LIMIT 1 ";
    if($this->dbDriver->type!='oracle')
      $SQL .= 'FOR UPDATE';
    $next = $this->dbDriver->GetRow($SQL);
    if(count($next) == NULL)
      return;
    $nextPos = $next['fixedOrder'];
    $nextId = $next['folderId'];

    $SQL = "UPDATE LIB_Folder SET fixedOrder=$nextPos WHERE ";
    $SQL .= "folderId=$folderId ";
    $this->dbDriver->ExecSQL($SQL);
    
    $SQL = "UPDATE LIB_Folder SET fixedOrder = $currPos WHERE ";
    $SQL .= "folderId=$nextId";
    $this->dbDriver->ExecSQL($SQL);
  }

  function TakeFolderOwnership($folderId)
  {
    $fields['path'] = $this->GetFolderPath($folderId);
    $login = $this->TakeOwnership($folderId);
    $this->NotifyAdmin('adminTakeFolderOwnershipLogTag',$fields);
    return $login;
  }

}

?>
