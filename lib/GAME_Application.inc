<?php
define('requested',1);
define('scheduled',2);
define('notified',3);
define('executing',4);
define('finished',5);
define('cancelled',6);
define('started',7);
define('registered',8);
define('stopped',9);
define('terminated',10);
define('cancelRequested',11);

class GAME_Application
{
  //
  //--Private
  //
  protected $dbDriver;
  protected $pageBuilder;
  protected $module;
  protected $lang;

  //
  //--Public
  //

  function __construct(&$module)
  {
    $this->module = & $module;
    $this->pageBuilder = & $module->pageBuilder;
    $this->dbDriver = & $module->dbDriver;
    $this->lang = $module->pageBuilder->lang;
  }

  function CheckRequested()
  {
    $durationUnity = $this->pageBuilder->siteConfig->getVar("gamecloud",
							    "durationUnity");
    $SQL = 'SELECT * ';
    $SQL .= 'FROM GAME_Application a, GAME_ApplicationPlace ap ';
    $SQL .= 'WHERE status='.requested.' AND a.placeId=ap.id ';
    $SQL .= 'AND (SELECT COUNT(*) ';
    $SQL .= 'FROM AV_Schedule s WHERE a.docId=s.docId ';
    $SQL .= 'AND NOW() <= DATE_ADD(CONVERT_TZ(start,ap.timezone,@@time_zone) ';
    $SQL .= ", INTERVAL numOfHours $durationUnity)) >=1 ";
    $SQL .= "LIMIT 1 FOR UPDATE ";
    $app = $this->dbDriver->GetRow($SQL);

    if(count($app)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    // cria registro do DNS para acesso
    $this->RegisterDNS($app['docId'],"gamecloud.oktala.com.br.");
    $this->ChangeStatus($app, scheduled);
  }

  function CheckScheduled()
  {
    $notifyBefore = $this->pageBuilder->siteConfig->getVar("gamecloud",
							   "notifyBefore");
    $SQL = 'SELECT * ';
    $SQL .= 'FROM GAME_Application a, GAME_ApplicationPlace ap ';
    $SQL .= 'WHERE status='.scheduled.' AND a.placeId=ap.id ';
    $SQL .= 'AND (SELECT CONVERT_TZ(start,ap.timezone,@@time_zone) ';
    $SQL .= 'FROM AV_Schedule s WHERE a.docId=s.docId ORDER BY start LIMIT 1) ';
    $SQL .= '<= DATE_ADD(NOW(), INTERVAL '.$notifyBefore.') ';
    $SQL .= "LIMIT 1 FOR UPDATE ";
    $app = $this->dbDriver->GetRow($SQL);

    if(count($app)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $this->ChangeStatus($app, notified);
  }

  function CheckReadyToRun()
  {
    $startBefore = $this->pageBuilder->siteConfig->getVar("gamecloud",
							  "startBefore");
    $stopAfter = $this->pageBuilder->siteConfig->getVar("gamecloud",
							"stopAfter");
    $durationUnity = $this->pageBuilder->siteConfig->getVar("gamecloud",
							    "durationUnity");
    $SQL = 'SELECT * ';
    $SQL .= 'FROM GAME_Application a, GAME_ApplicationPlace ap ';
    $SQL .= 'WHERE (status='.notified.' OR status='.stopped.') ';
    $SQL .= 'AND a.placeId=ap.id ';
    $SQL .= 'AND (SELECT COUNT(*) ';
    $SQL .= 'FROM AV_Schedule s WHERE a.docId=s.docId ';
    $SQL .= 'AND NOW() >= DATE_SUB(CONVERT_TZ(start,ap.timezone,@@time_zone) ';
    $SQL .= ", INTERVAL $startBefore) ";
    $SQL .= 'AND NOW() <= DATE_ADD(DATE_ADD(';
    $SQL .= 'CONVERT_TZ(start,ap.timezone,@@time_zone) ';
    $SQL .= ", INTERVAL numOfHours $durationUnity), INTERVAL $stopAfter)) >=1 ";
    $SQL .= "LIMIT 1 FOR UPDATE ";
    $app = $this->dbDriver->GetRow($SQL);

    if(count($app)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    try
    {
      if($app['status']==notified)
      {
	$this->Run($app);
	$node = new LIB_Node($this->module);
	$node->ChMod($app['docId'],'r','r','r');
      }
      else
	$this->Start($app);

      $this->ChangeStatus($app, started);
    }
    catch(AV_Exception $e)
    {
      $this->Terminate($app);
      throw $e;
    }
  }

  function CheckStarted()
  {
    $maxRegisterTime = $this->pageBuilder->siteConfig->getVar("gamecloud",
							      "maxRegisterTime");

    $SQL = 'SELECT *, NOW() > DATE_ADD(startTime, ';
    $SQL .= "INTERVAL $maxRegisterTime) as timeOut ";
    $SQL .= 'FROM GAME_Application a, GAME_ApplicationPlace ap ';
    $SQL .= 'WHERE status='.started.' AND a.placeId=ap.id ';
    $SQL .= "LIMIT 1 FOR UPDATE ";
    $app = $this->dbDriver->GetRow($SQL);

    if(count($app)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    if($app['instanceId']==NULL)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', ' - Aplicação iniciada mas sem instanceId'));

    // run/create instance
    $cmd = 'ec2-describe-instances'.$this->EC2Credentials();
    $cmd .= "--region " . $app['region'].' '.$app['instanceId'];
    exec($cmd,$output,$retVal);
    if($retVal!=0)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', implode("\r\n",$output)));

    $publicDNSName = NULL;
    foreach($output as $item)
    {
      $params = explode("\t",$item);
      if($params[0]=='INSTANCE')
      {
	$publicDNSName = $params[3];
	break;
      }
    }
    if($publicDNSName!=NULL)
    {
      // cria registro do DNS para acesso
      $this->RegisterDNS($app['docId'], $publicDNSName);
      try
      {
	$this->ChangeStatus($app, registered);
      }
      catch(AV_Exception $e)
      {
	$this->RegisterDNS($app['docId'],"gamecloud.oktala.com.br.");
	throw $e;
      }
    }
    elseif($app['timeOut'])
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', ' - Timeout atingido para registro'));
  }

  function CheckRegistered()
  {
    $maxStartTime = $this->pageBuilder->siteConfig->getVar("gamecloud",
							   "maxStartTime");

    $SQL = 'SELECT *, NOW() > DATE_ADD(startTime, ';
    $SQL .= "INTERVAL $maxStartTime) as timeOut ";
    $SQL .= 'FROM GAME_Application a, GAME_ApplicationPlace ap ';
    $SQL .= 'WHERE status='.registered.' AND a.placeId=ap.id ';
    $SQL .= "LIMIT 1 FOR UPDATE ";
    $app = $this->dbDriver->GetRow($SQL);

    if(count($app)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    // check app status
    $ch = curl_init($this->GameURL($app)."/gameImpactValidation.av");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $output = explode("\n",trim(strip_tags(curl_exec($ch))));
    curl_close($ch);

    if(trim($output[count($output)-1])=='Nenhum erro encontrado')
      $this->ChangeStatus($app, executing);
    elseif($app['timeOut'])
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', ' - Timeout atingido para iniciar aplicação'));
  }

  function CheckExecuting()
  {
    $startBefore = $this->pageBuilder->siteConfig->getVar("gamecloud",
							  "startBefore");
    $stopAfter = $this->pageBuilder->siteConfig->getVar("gamecloud",
							"stopAfter");
    $durationUnity = $this->pageBuilder->siteConfig->getVar("gamecloud",
							    "durationUnity");
    $SQL = 'SELECT *, (SELECT COUNT(*) ';
    $SQL .= 'FROM AV_Schedule s WHERE a.docId=s.docId ';
    $SQL .= 'AND NOW() < DATE_SUB(CONVERT_TZ(start,ap.timezone,@@time_zone) ';
    $SQL .= ", INTERVAL $startBefore)) as scheduleCount ";
    $SQL .= 'FROM GAME_Application a, GAME_ApplicationPlace ap ';
    $SQL .= 'WHERE status='.executing.' ';
    $SQL .= 'AND a.placeId=ap.id ';
    $SQL .= 'AND (SELECT COUNT(*) ';
    $SQL .= 'FROM AV_Schedule s WHERE a.docId=s.docId ';
    $SQL .= 'AND (NOW() >= DATE_SUB(CONVERT_TZ(start,ap.timezone,@@time_zone) ';
    $SQL .= ", INTERVAL $startBefore) ";
    $SQL .= 'AND NOW() <= DATE_ADD(DATE_ADD(';
    $SQL .= 'CONVERT_TZ(start,ap.timezone,@@time_zone) ';
    $SQL .= ", INTERVAL numOfHours $durationUnity), INTERVAL $stopAfter))) = 0 ";
    $SQL .= "LIMIT 1 FOR UPDATE ";
    $app = $this->dbDriver->GetRow($SQL);

    if(count($app)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    // Dump da base logo que jogo finaliza
    if($app['scheduleCount']==0)
    {
      $dumpPath = $this->pageBuilder->siteConfig->getVar("gamecloud",
							 "dumpPath");
      $cmd = 'mysqldump -u avalanche -pavalanche -h ' . $this->GameHost($app);
      $cmd .= ' sidergame_website > '.$dumpPath.'game'.$app['docId'].'.sql';

      exec($cmd,$output,$retVal);
      if($retVal!=0)
	throw new AV_Exception(FAILED,$this->lang,
			       array('addMsg', implode("\r\n",$output)));
    }

    $this->Stop($app);

    $this->RegisterDNS($app['docId'],"gamecloud.oktala.com.br.");

    if($app['scheduleCount']>0)
      $this->ChangeStatus($app, stopped);
    else
      $this->ChangeStatus($app, finished);
  }

  function CheckFinished()
  {
    $durationUnity = $this->pageBuilder->siteConfig->getVar("gamecloud",
							    "durationUnity");
    $terminateAfter = $this->pageBuilder->siteConfig->getVar("gamecloud",
							     "terminateAfter");
    $SQL = 'SELECT * ';
    $SQL .= 'FROM GAME_Application a, GAME_ApplicationPlace ap ';
    $SQL .= 'WHERE status='.finished.' ';
    $SQL .= 'AND a.placeId=ap.id ';
    $SQL .= 'AND NOW() > (SELECT DATE_ADD(DATE_ADD(';
    $SQL .= 'CONVERT_TZ(start, ap.timezone,@@time_zone), ';
    $SQL .= "INTERVAL numOfHours $durationUnity), INTERVAL $terminateAfter) ";
    $SQL .= 'FROM AV_Schedule s WHERE a.docId=s.docId ORDER BY start DESC LIMIT 1) ';
    $SQL .= "LIMIT 1 FOR UPDATE ";
    $app = $this->dbDriver->GetRow($SQL);

    if(count($app)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $this->UnregisterDNS($app['docId']);
    $this->Terminate($app);
    $this->ChangeStatus($app, terminated);
  }

  function CheckCancelRequested()
  {
    $SQL = 'SELECT * ';
    $SQL .= 'FROM GAME_Application a, GAME_ApplicationPlace ap ';
    $SQL .= 'WHERE status='.cancelRequested.' AND a.placeId=ap.id ';
    $SQL .= "LIMIT 1 FOR UPDATE ";
    $app = $this->dbDriver->GetRow($SQL);

    if(count($app)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $this->UnregisterDNS($app['docId']);
    if($app['instanceId']!=NULL)
      $this->Terminate($app);

    $this->ChangeStatus($app, cancelled);
  }

  function GetPublicDNSName($docId)
  {
    $SQL = 'SELECT * FROM GAME_Application a, GAME_ApplicationPlace ap ';
    $SQL .= "WHERE docId='$docId' AND status=".executing.' AND a.placeId=ap.id ';
    $app = $this->dbDriver->GetRow($SQL);

    if(count($app)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    if($app['instanceId']==NULL)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', ' - Aplicação iniciada mas sem instanceId'));

    // describe instance
    $cmd = "export EC2_HOME=\"/opt/ec2-api-tools-1.6.11.0/\" && ";
    $cmd .= "export PATH=/opt/ec2-api-tools-1.6.11.0/bin/:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin && ";
    $cmd .= "export JAVA_HOME=\"/usr/java/latest/\" && ";
    $cmd .= 'ec2-describe-instances'.$this->EC2Credentials();
    $cmd .= "--region " . $app['region'].' '.$app['instanceId'];
    exec($cmd,$output,$retVal);
    if($retVal!=0)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg'=> implode("\r\n",$output)));

    $publicDNSName = NULL;
    foreach($output as $item)
    {
      $params = explode("\t",$item);
      if($params[0]=='INSTANCE')
      {
	$publicDNSName = $params[3];
	break;
      }
    }

    if($publicDNSName==NULL)
      throw new AV_Exception(FAILED,$this->lang);
      
    return $publicDNSName;
  }


  protected function ChangeStatus($app, $status)
  {
    $SQL = "UPDATE GAME_Application SET status = '$status'";
    $SQL .= "WHERE docId='".$app['docId']."' ";
    $this->dbDriver->ExecSQL($SQL);

    switch($status)
    {
    case scheduled:
      $subject = 'gameSchdSubject';
      $statusText = 'scheduled';
      $obs = 'obsScheduled';
      break;
    case notified:
      $subject = 'gameNotSubject';
      $statusText = 'notified';
      $obs = 'obsScheduled';
      break;
    case executing:
      $subject = 'gameExeSubject';
      $statusText = 'executing';
      $obs = 'obsExecuting';
      break;
    case finished:
      $subject = 'gameFinSubject';
      $statusText = 'finished';
      $obs = 'obsFinished';
      break;
    case cancelled:
      $subject = 'gameCanSubject';
      $statusText = 'cancelled';
      $obs = 'obsCancelled';
      break;
    case stopped:
      $subject = 'gameStoppedSubject';
      $statusText = 'stopped';
      $obs = 'obsStopped';
      break;
    default:
      return;
    }

    $label = new LANG_Label($this->module);
    $labels = $label->GetLabels(array(

				      $subject,
				      $statusText,
				      'responsibleTag',
				      'scheduleTag',
				      'dateTimeTag',
				      'durationTag',
				      'statusTag',
				      'placeTag',
				      'gameLinkTag',
				      'usersTag',
				      'usersInfo',
				      $obs,
				      'obsTag'), $app['lang']);

    $subject = $labels[$subject];
    $statusText = $labels[$statusText];
    $obs = $labels[$obs];
    
    // retrieves schedule
    $SQL = 'SELECT id, DATE_FORMAT(start,';
    if($app['lang'] == 'en_US')
      $SQL .= "'%m/%d/%Y - %H:%i'";
    else
      $SQL .= "'%d/%m/%Y - %H:%i'";
    $SQL .= ') as dateTime, numOfHours ';
    $SQL .= 'FROM AV_Schedule ';
    $SQL .= "WHERE docId='".$app['docId']."' ";
    $SQL .= 'ORDER BY start ';
    $schedule = $this->dbDriver->GetAll($SQL);

    // send email
    $to = $this->pageBuilder->siteConfig->getVar("gamecloud","mailTo");
    if($to!=NULL)
      $to .= ',';
    $to .= $app['email'];

    $templatesDir = $this->pageBuilder->siteConfig->getVar("dirs","templates");
    foreach($templatesDir as $dir)
    {
      $msg = file_get_contents($dir."pmgMsg.tpt");
      if($msg!==false)
	break;
    }
    if($msg===false)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg'=>' - Template não encontrado - '.
				   $dir.'pmgMsg.tpt'));

    $tags[] = '%responsibleTag%'; $replace[] = $labels['responsibleTag'];
    $tags[] = '%responsible%'; $replace[] = $app['responsible'];
    $tags[] = '%email%'; $replace[] = $app['email'];
    $tags[] = '%scheduleTag%'; $replace[] = $labels['scheduleTag'];
    $tags[] = '%dateTimeTag%'; $replace[] = $labels['dateTimeTag'];
    $tags[] = '%durationTag%'; $replace[] = $labels['durationTag'];
    foreach($schedule as $item)
      $scheduleContent  .= $item['dateTime']. ' -  '. $item['numOfHours']. "<br />\n";
    $tags[] = '%schedule%'; $replace[] = $scheduleContent;
    $tags[] = '%statusTag%'; $replace[] = $labels['statusTag'];
    $tags[] = '%statusText%'; $replace[] = $statusText;
    $tags[] = '%placeTag%'; $replace[] = $labels['placeTag'];
    $tags[] = '%place%'; $replace[] = $app['name'];
    $tags[] = '%usersTag%'; $replace[] =  $labels['usersTag'];
    $tags[] = '%usersInfo%'; $replace[] =  $labels['usersInfo'];
    $tags[] = '%obsTag%'; $replace[] =  $labels['obsTag'];
    $tags[] = '%obs%'; $replace[] =  $obs;
    $tags[] = '%gameLink%'; $replace[] = $this->GameURL($app);
    $tags[] = '%base%'; $replace[] = 
			  $this->pageBuilder->siteConfig->getVar("gamecloud",
								 "base");
    $msg = str_replace($tags, $replace, $msg);

    $from = $this->pageBuilder->siteConfig->getVar("gamecloud","mailFrom");

    $headers .= "From: $from\r\n";
    $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";
 
    if(!mail($to, $subject, $msg, $headers ,"-r$from"))
    { 
      // Se "não for Postfix"
      $headers .= "Return-Path: $from\r\n"; 
      if(!mail($to, $subject, $msg, $headers))
	throw new AV_Exception(FAILED, $this->lang);
    }
  }

  protected function GameURL($app)
  {
    return 'http://'.$this->GameHost($app);
  }

  protected function GameHost($app)
  {
    return 'game'.$app['docId'].'.gamecloud.oktala.com.br';
  }

  protected function EC2Credentials()
  {
    $access_key_id = $this->pageBuilder->siteConfig->getVar("gamecloud",
							    "access_key_id");
    $secret_access_key = $this->pageBuilder->siteConfig->getVar("gamecloud",
								"secret_access_key");
    return " -O $access_key_id -W $secret_access_key ";
  }

  protected function Run(&$app)
  {
    $instance_type = $this->pageBuilder->siteConfig->getVar("gamecloud",
							    "instance_type");
    $gamecloud_init = $this->pageBuilder->siteConfig->getVar("gamecloud",
							     "init");

    if($app['instanceId']!=NULL)
	throw new AV_Exception(FAILED,$this->lang,
			       array('addMsg', 
				     ' - Não pode criar - instanceId NOT NULL - '.
				     $app['instanceId']));

    // run/create instance
    $cmd = 'ec2-run-instances'.$this->EC2Credentials();
    $cmd .= "-k gamecloud -g gamecloud -f ".$gamecloud_init.'.'.$app['tag'];
    $cmd .= " -t $instance_type --region ";
    $cmd .= $app['region'].' '.$app['ami'];
    exec($cmd,$output,$retVal);
    if($retVal!=0)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', $cmd."\r\n".implode("\r\n",$output)));

    $instanceId = NULL;
    foreach($output as $item)
    {
      $params = explode("\t",$item);
      if($params[0]=='INSTANCE')
      {
	$instanceId = $params[1];
	break;
      }
    }
    if($instanceId==NULL)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', $cmd."\r\n".implode("\r\n",$output)));

    $app['instanceId'] = $instanceId;

    $SQL = "UPDATE GAME_Application SET instanceId='$instanceId', startTime=NOW() ";
    $SQL .= "WHERE docId='".$app['docId']."' ";
    $this->dbDriver->ExecSQL($SQL);
  }

  protected function Start($app)
  {
    if($app['instanceId']==NULL)
	throw new AV_Exception(FAILED,$this->lang,
			       array('addMsg', ' Não pode iniciar - instanceId NULL '));

    // start instance
    $cmd = 'ec2-start-instances'.$this->EC2Credentials();
    $cmd .= "--region " .$app['region'].' '.$app['instanceId'];
    exec($cmd,$output,$retVal);
    if($retVal!=0)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', implode("\r\n",$output)));
  }

  protected function Stop($app)
  {
    if($app['instanceId']==NULL)
	throw new AV_Exception(FAILED,$this->lang,
			       array('addMsg', ' Não pode parar instancia - instanceId NULL '));

    // stop instance
    $cmd = 'ec2-stop-instances'.$this->EC2Credentials();
    $cmd .= "--region " .$app['region'].' '.$app['instanceId'];
    exec($cmd,$output,$retVal);
    if($retVal!=0)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', implode("\r\n",$output)));
  }

  protected function Terminate($app)
  {
    if($app['instanceId']==NULL)
	throw new AV_Exception(FAILED,$this->lang,
			       array('addMsg', ' Não pode parar instancia - instanceId NULL '));

    // terminate instance
    $cmd = 'ec2-terminate-instances'.$this->EC2Credentials();
    $cmd .= "--region " .$app['region'].' '.$app['instanceId'];
    exec($cmd,$output,$retVal);
    if($retVal!=0)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', implode("\r\n",$output)));
  }

  protected function RegisterDNS($docId,$url)
  {
    $access_key_id = $this->pageBuilder->siteConfig->getVar("gamecloud",
							    "access_key_id");
    $secret_access_key = $this->pageBuilder->siteConfig->getVar("gamecloud",
								"secret_access_key");
    $cmd = "export AWS_ACCESS_KEY_ID=$access_key_id ";
    $cmd .= "&& export AWS_SECRET_ACCESS_KEY=$secret_access_key ";
    $cmd .= "&& cli53 rrcreate -r -x 30 oktala.com.br ";
    $cmd .= "game".$docId.".gamecloud CNAME $url ";
    exec($cmd,$output,$retVal);
    if($retVal!=0)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', implode("\r\n",$output)));
  }

  protected function UnregisterDNS($docId)
  {
    $access_key_id = $this->pageBuilder->siteConfig->getVar("gamecloud",
							    "access_key_id");
    $secret_access_key = $this->pageBuilder->siteConfig->getVar("gamecloud",
								"secret_access_key");
    $cmd = "export AWS_ACCESS_KEY_ID=$access_key_id ";
    $cmd .= "&& export AWS_SECRET_ACCESS_KEY=$secret_access_key ";
    $cmd .= "&& cli53 rrdelete oktala.com.br ";
    $cmd .= "game".$docId.".gamecloud CNAME ";
    exec($cmd,$output,$retVal);
    if($retVal!=0)
      throw new AV_Exception(FAILED,$this->lang,
			     array('addMsg', implode("\r\n",$output)));
  }
}

?>