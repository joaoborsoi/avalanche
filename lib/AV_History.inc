<?php

class AV_History
{
  protected $dbDriver;
  protected $module;

  function __construct(&$module)
  {
    $this->module = & $module;
    $this->dbDriver = & $module->dbDriver;
    $this->lang = $module->pageBuilder->lang;
  }

  function AddHistory($title,$link)
  {
    $sessionH = & $this->module->pageBuilder->sessionH;

    // removes old link if exists
    $SQL = 'DELETE FROM AV_History WHERE ';
    if($sessionH->isGuest())
      $SQL .= "sessionId='".$sessionH->sessionID."' ";
    else
    {
      $SQL .= 'userId=(SELECT userId ';
      $SQL .= 'FROM UM_UserSessions ';
      $SQL .= "WHERE sessionId='".$sessionH->sessionID."') ";
    }
    $SQL .= "AND link='$link' ";
    $this->dbDriver->ExecSQL($SQL);

    $SQL = 'INSERT INTO AV_History ';
    if($sessionH->isGuest())
    {
      $SQL .= "(sessionId,title,link) VALUES ";
      $SQL .= "('".$sessionH->sessionID."','$title','$link')";
    }
    else
    {
      $SQL .= '(userId, title, link) ';
      $SQL .= "SELECT userId, '$title' as title, '$link' as link ";
      $SQL .= 'FROM UM_UserSessions ';
      $SQL .= "WHERE sessionId='".$sessionH->sessionID."' ";
    }
    $this->dbDriver->ExecSQL($SQL);
  }

  function GetHistory()
  {
    $limit = $this->module->pageBuilder->siteConfig->getVar('cms',
							    'resultsHistory');
    $sessionH = & $this->module->pageBuilder->sessionH;

    $SQL = 'SELECT id, title, link FROM AV_History h ';
    if($sessionH->isGuest())
      $SQL .= "WHERE sessionId='".$sessionH->sessionID."' ";

    else
    {
      $SQL .= ', UM_UserSessions u ';
      $SQL .= "WHERE u.sessionId='".$sessionH->sessionID."' ";
      $SQL .= 'AND u.userId=h.userId ';
    }

    $SQL .= " ORDER BY id DESC LIMIT $limit";
    $result = $this->dbDriver->GetAll($SQL);
    if(count($result)==0)
      return NULL;

    $SQL = 'DELETE FROM AV_History WHERE ';
    if($sessionH->isGuest())
      $SQL .= "sessionId='".$sessionH->sessionID."' ";
    else
    {
      $SQL .= 'userId=(SELECT userId ';
      $SQL .= 'FROM UM_UserSessions ';
      $SQL .= "WHERE sessionId='".$sessionH->sessionID."') ";
    }
    $SQL .= 'AND id < '.$result[count($result)-1]['id'];
    $this->dbDriver->ExecSQL($SQL);

    return $result;
  }
}

?>