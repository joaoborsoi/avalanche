<?php

require_once('AV_DBOperationObj.inc');
require_once('GAME_Indicators.inc');

class GAME_LoadQuizOption extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'id';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';

    $this->fieldsDef[1]['fieldName'] = 'quizOption';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    // retrieve quiz div to return to interface
    $SQL = 'SELECT targetDiv FROM GAME_ElementType et, GAME_Element e ';
    $SQL .= "WHERE e.id='".$this->fields['id']."' AND e.typeId=et.id ";
    $this->attrs['targetDiv'] = $this->dbDriver->GetOne($SQL);

    // do impact
    $SQL = 'SELECT impact FROM GAME_QuizOption ';
    $SQL .= "WHERE id='".$this->fields['quizOption']."' ";
    $impact = $this->dbDriver->GetOne($SQL);
    if($impact!=NULL)
    {
      $indicators = new GAME_Indicators($this);
      $id = $indicators->GetLastId();
      $indicators->ApplyImpact($id,$impact);
    }

    // register answer
    $SQL = 'INSERT INTO GAME_QuizAnswer ';
    $SQL .= '(playId,optionId,quizId)  ';
    $SQL .= 'SELECT pu.id as playId, ';
    $SQL .= $this->fields['quizOption'] . ' as optionId, ';
    $SQL .= $this->fields['id'] . ' as quizId ';
    $SQL .= 'FROM GAME_PlayUser pu, UM_UserSessions us ';
    $SQL .= "WHERE sessionId='".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $this->dbDriver->ExecSQL($SQL);
  }
}