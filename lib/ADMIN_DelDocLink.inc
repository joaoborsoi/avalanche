<?php

require_once('ADMIN_Files.inc');

class ADMIN_DelDocLink extends ADMIN_Files
{

  protected function LoadFiles($path)
  {
    try
    {
      $_GET['path'] = $this->rootPath . $path;
      $delModule = $this->LoadModule('LIB_DelDocLink');
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }    

    $script .= parent::LoadFiles($path);
    return $script;
  }
}

?>