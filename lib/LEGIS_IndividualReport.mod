<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Report.inc');
require_once('LEGIS_Process.inc');

class LEGIS_IndividualReport extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'paramList';
    $this->fieldsDef[0]['stripSlashes'] = true; 
    $this->fieldsDef[0]['trimField'] = false; 
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    $process = new LEGIS_Process($this);
    $memberAggregator = $process->GetMemberAggregator();

    if(count($memberAggregator) > 0)
    {
      $this->fields['paramList'][] = Array('param'=>'aggregator',
    					   'aggregatorId'=>$memberAggregator['docId']);
    }

    $report = new LEGIS_Report($this);
    $this->attrs['docId'] = $report->DoIndividual($this->fields['paramList']);
  }
}

?>