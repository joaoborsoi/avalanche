<?php

require_once('AV_DBOperationObj.inc');
require_once('FORM_DynTable.inc');

class FORM_SetOrder extends AV_DBOperationObj
{
  //--Method: LoadFieldsDef
  //--Desc: Redefines parent method to define module's input fields
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'tableName';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    
    $this->fieldsDef[1]['fieldName'] = 'fieldName';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;

    $this->fieldsDef[2]['fieldName'] = 'up';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'boolean';
    $this->fieldsDef[2]['default'] = false;
  }

  function Execute()
  {
    $table = new FORM_DynTable($this->fields['tableName']);

    return $table->SetOrder($this->fields['fieldName'],$this->fields['up']);
  }
}

?>