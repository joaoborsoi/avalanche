<?php 

class LANG_Label 
{
  var $module;
  var $dbDriver;

  //--Method: LANG_Label
  //--Desc: 
  function __construct(&$module)
  {
    $this->dbDriver =  & $module->dbDriver;
    $this->module = & $module;
  }

  //--Method: GetPageLabels
  //--Desc: Get information about Label from the database
  function GetPageLabels ($pageId, $lang)
  {
    $SQL = 'SELECT LANG_Label.labelId,value ';
    $SQL .= 'FROM LANG_PageLabels,LANG_Label WHERE ';
    $SQL .= "(pageId = '$pageId' OR pageId='*') AND lang = '$lang' AND ";
    $SQL .= "LANG_PageLabels.labelId = LANG_Label.labelId";

    return $this->dbDriver->GetAll($SQL);
  }

  function GetPartLabels ($part, $lang)
  {
    $SQL = 'SELECT LANG_Label.labelId,value ';
    $SQL .= 'FROM LANG_PartLabels,LANG_Label WHERE ';
    $SQL .= "part = '$part' AND lang = '$lang' AND ";
    $SQL .= "LANG_PartLabels.labelId = LANG_Label.labelId";

    return $this->dbDriver->GetAll($SQL);
  }

  
  //--Method: SetLabel
  //--Desc: Set label
  function SetLabel ($labelId, $labels, $textLabel = false)
  {
    $tableName = $textLabel ? 'LANG_Text':'LANG_Label';
    $SQL = "DELETE FROM $tableName WHERE labelId='$labelId'";
    $this->dbDriver->ExecSQL($SQL);

    $control = true;
    $SQL ="INSERT INTO $tableName (labelId,lang, value) VALUES ";
    foreach ($labels as $stateIndex => $value)
    {
      if($control)
	$control = false;
      else
	$SQL .= ',';
      $SQL .= "('$labelId','".$value['lang']."','".$value['value']."') ";
    }
    $this->dbDriver->ExecSQL($SQL);
  }

  //--Method: GetLabel
  //--Desc: Get label
  function GetLabel($labelId, $lang)
  {
    $SQL = "SELECT value FROM LANG_Label WHERE labelId='$labelId' ";
    $SQL .= "AND lang = '$lang'";
    return $this->dbDriver->GetOne($SQL);
  }

  function GetLabels($labels, $lang)
  {
    $SQL = 'SELECT labelId, value FROM LANG_Label WHERE ';
    $SQL .= "(labelId='".implode("' OR labelId='", $labels)."') ";
    $SQL .= "AND lang = '$lang'";
    $rows = $this->dbDriver->GetAll($SQL);

    $result = Array();
    foreach($rows as $row)
      $result[$row['labelId']]=$row['value'];
    return $result;
  }

  function GetLabelList($labelId, $textLabel = false)
  {
    $tableName = $textLabel ? 'LANG_Text':'LANG_Label';
    $SQL = "SELECT lang, value FROM $tableName WHERE labelId='$labelId' ";
    return $this->dbDriver->GetAll($SQL);
  }

  function GetLangList()
  {
    $SQL = 'SELECT lang, name FROM LANG_Language ';
    $SQL .= "WHERE enabled='1' ORDER BY indexOrder";
    return $this->dbDriver->GetAll($SQL);
  }

  function DuplicateLabel($sourceId,$targetId,$textLabel = false)
  {
    $tableName = $textLabel ? 'LANG_Text':'LANG_Label';

    $SQL = "INSERT INTO $tableName (labelId,lang,value) ";
    $SQL .= "SELECT '$targetId', lang, value ";
    $SQL .= "FROM $tableName WHERE labelId='$sourceId'";
    $this->dbDriver->ExecSQL($SQL);
  }
}

?>