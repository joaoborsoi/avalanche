<?php

require_once('ADMIN_Page.inc');

class ADMIN_SaveFolder extends ADMIN_Page
{
  var $rootPath;

  function __construct(& $pageBuilder, $rootPath = '')
  {
    if(mb_substr($rootPath,-1) == '/')
      $rootPath = mb_substr($rootPath,0,-1);
    $this->rootPath = $rootPath;
    parent::__construct($pageBuilder);    
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminScript');

    try
    {
      if($_REQUEST['folderId'] == NULL)
      {
	$_GET['folderPath'] = $this->rootPath . $_REQUEST['folderPath'];
	$saveFolder = $this->LoadModule('LIB_InsFolder');
      }
      else
	$saveFolder = $this->LoadModule('LIB_SetFolder');

      $script .= 'if(parent.opener) ';
      $script .= 'parent.opener.Reload();';
      $script .= 'parent.close();';
    }
    catch(Exception $e)
    {
      $script .= "alert('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }
}

?>