<?php

require_once('AV_DBOperationObj.inc');

class GAME_LoadIcons extends AV_DBOperationObj
{
  protected function Execute()
  {
    $this->isArray = true;
    $SQL = 'SELECT e.*, el.value as title, i.*, elementClass, targetDiv, ';
    $SQL .= 'onLoadSuccess, name as type ';
    $SQL .= 'FROM GAME_Element e, LANG_Label el, GAME_Icon i, GAME_ElementType it ';
    $SQL .= 'WHERE e.id = i.id AND typeId = it.id ';
    $SQL .= 'AND e.title=el.labelId ';
    $SQL .= "AND el.lang='".$this->pageBuilder->lang."' ";
    $SQL .= 'ORDER BY e.id ';
    $iconList = $this->dbDriver->GetAll($SQL);
    $this->LoadChildrenFromRows($iconList,'icon');
  }
}

?>