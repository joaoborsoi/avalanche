<?php

require_once('LEX_Analyzer.inc');

define('openParenthesisTk', 1);
define('closeParenthesisTk', 2);
define('ifTk', 3);
define('semicolonTk', 4);
define('equalTk', 5);
define('greaterThenTk', 6);
define('greaterEqualThenTk', 7);
define('lowerThenTk', 8);
define('lowerEqualThenTk', 9);
define('diffTk', 10);
define('plusTk', 11);
define('minusTk', 12);
define('multTk', 13);
define('divTk', 14);
define('powTk', 15);
define('logTk', 16);
define('caseTk', 17);
define('thenTk', 18);
define('whenTk', 19);
define('endTk', 20);
define('colonTk', 21);
define('andTk', 22);
define('orTk', 23);


class FORM_LexAnalyzer extends LEX_Analyzer
{

  function __construct(& $code, $decimalPoint = '.')
  {
    $symbols['('] = openParenthesisTk;
    $symbols[')'] = closeParenthesisTk;
    $symbols['IF'] = ifTk;
    $symbols['if'] = ifTk;
    $symbols['If'] = ifTk;
    $symbols['iF'] = ifTk;
    $symbols[';'] = semicolonTk;
    $symbols[','] = colonTk;
    $symbols['='] = equalTk;
    $symbols['>'] = greaterThenTk;
    $symbols['>='] = greaterEqualThenTk;
    $symbols['<'] = lowerThenTk;
    $symbols['<='] = lowerEqualThenTk;
    $symbols['<>'] = diffTk;
    $symbols['+'] = plusTk;
    $symbols['-'] = minusTk;
    $symbols['*'] = multTk;
    $symbols['/'] = divTk;
    $symbols['POW'] = powTk;
    $symbols['POw'] = powTk;
    $symbols['PoW'] = powTk;
    $symbols['Pow'] = powTk;
    $symbols['pOW'] = powTk;
    $symbols['pOw'] = powTk;
    $symbols['poW'] = powTk;
    $symbols['pow'] = powTk;
    $symbols['LOG'] = logTk;
    $symbols['LOg'] = logTk;
    $symbols['LoG'] = logTk;
    $symbols['Log'] = logTk;
    $symbols['lOG'] = logTk;
    $symbols['lOg'] = logTk;
    $symbols['loG'] = logTk;
    $symbols['log'] = logTk;
    $symbols['case'] = caseTk;
    $symbols['Case'] = caseTk;
    $symbols['CASE'] = caseTk;
    $symbols['then'] = thenTk;
    $symbols['Then'] = thenTk;
    $symbols['THEN'] = thenTk;
    $symbols['when'] = whenTk;
    $symbols['When'] = whenTk;
    $symbols['WHEN'] = whenTk;
    $symbols['end'] = endTk;
    $symbols['End'] = endTk;
    $symbols['END'] = endTk;
    $symbols['And'] = andTk;
    $symbols['and'] = andTk;
    $symbols['AND'] = andTk;
    $symbols['or'] = orTk;
    $symbols['Or'] = orTk;
    $symbols['OR'] = orTk;
    parent::__construct($code, $symbols, false, $decimalPoint);
  }
}

?>