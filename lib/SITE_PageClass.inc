<?php

class page{
/*
Abre um arquivo de texto e suas respectivas
imagens e imprime no tag $contentTag dentro 
do root template.
*/

  var $pageBuilder;
  var $page;
  // construtor  
  function page(&$page, $docId, $date=false, $setTitle=true, $contentTag='content'){
        $this->pageBuilder = &$page->pageBuilder ;
        $this->page=&$page;    
    	$galleryMinNum = $this->pageBuilder->siteConfig->getVar("gallery", "galleryMinNum");
    	
    	
        $_GET['docId']= $docId;
	if($this->lang=='en_US') $_GET['dateFormat']="%B %d, %Y";
	else $_GET['dateFormat']="%d/%B/%Y";
	
	$tpl=$page->wrap;
	  
        $details = $this->page->LoadModule('LIB_GetDoc');
       // print_r($details->children[0]->attrs);exit;
	
	
	$titulo = $details->children[0]->attrs['title'];
	$content = $details->children[0]->attrs['content'];
	$descr = $details->children[0]->attrs['descr'];
	$authors = $details->children[0]->attrs['authors'];
	$h1=h1($titulo, 'class=title');
	if($setTitle)$this->page->setPageTitle($titulo);
	$this->page->setPageDescr($descr);
	$this->page->setPageAuthor($authors);
        $keywords = $details->children[0]->attrs['imageList'];  //print_r($details->attrs['keywords']);exit;
        $docIdList=explode(",",$keywords);
        $numImages=count($docIdList);
        if($numImages>=$galleryMinNum) $gallery=true;
        else $gallery=false;
        $images = "";
        for($k=0;$k<$numImages;$k++){
            if(mb_strlen($docIdList[$k])>0 && is_numeric($docIdList[$k])){
                $_GET['docId']= trim($docIdList[$k]);
                $imDetails = $this->page->LoadModule('LIB_GetDoc'); //print_r($imDetails->attrs);exit;
                $imDescr = $imDetails->children[0]->attrs['descr'];
                if($gallery)$imageSize='thumb';
                else $imageSize='scale';
                
                $thumb = a('<img src="'.$imageSize.'/'.$_GET['docId'].'" alt="'.$imDescr.'" />', 'class=highslide&href=images/'.$_GET['docId'].'&onclick=return hs.expand(this)');
        	
        	$caption=new div('class=highslide-caption');
        	$hsdir='avalanche-'.$this->pageBuilder->avVersion.'/highslide/graphics/';
        	$caption->add(a(img('src='.$hsdir.'go-prev.gif&alt='.$this->page->labels->attrs['prevTag']), 'class=hs-prev&onclick=return hs.previous(this)&title='.$this->page->labels->attrs['prevTipTag']));
        	$caption->add(a(img('src='.$hsdir.'go-next.gif&alt='.$this->page->labels->attrs['nextTag']), 'class=hs-next&onclick=return hs.next(this)&title='.$this->page->labels->attrs['nextTipTag']));
        	if($imDescr)$caption->add($imDescr);
        	else $caption->add('&nbsp;');
        	
        	if($gallery)$images .= span($thumb.$caption->get_html());
        	else $images .= $thumb;
            }    
        }
        
	if($date)$tpl->addText('<h5 class="date"><small >'.$details->children[0]->attrs['publicationDate'].'</small></h5>',$contentTag);
        
        
        // PCDTR
        if($this->pageBuilder->siteConfig->getVar("pcdtr", "enabled")){
        	//session_save_path(realpath('../admin/session'));
		$pcdtrDir=$this->pageBuilder->siteConfig->getVar("pcdtr", "scriptDir"); 
		
		require_once($pcdtrDir.'class.php');     
		$DTR = new PCDTR();
		$DTR->php_dir = ""; 
		$DTR->heading_css = $this->pageBuilder->siteConfig->getVar("pcdtr", "headingCSS");  
		$DTR->cache_dir = $this->pageBuilder->siteConfig->getVar("pcdtr", "cacheDir");  
		$DTR->image_dir= "sitePCDTR.av";
		$h1 = $DTR->getHeadings($h1);
		//$content = $DTR->getHeadings($content);
		$this->pageBuilder->rootTemplate->addText('<link rel="stylesheet" href="'.$DTR->heading_css.'" media="all" type="text/css" />', 'css');
	}
	// end PCDTR

	if($contentTag!='homeContent') $tpl->addText((string) $h1 ,$contentTag);	
        if($gallery){
        	$tpl->addText((string) $content,$contentTag);
        	$tpl->addText((string) div($images, 'class=gallery'),$contentTag);
        	$js=script("hs.align = 'center';");

        }else{
        	if($images)$tpl->addText((string) div($images, 'class=images'),$contentTag);
        	$tpl->addText((string) $content,$contentTag);
        	$js=script("hs.captionEval='this.thumb.alt';");
        	
        }
        $this->pageBuilder->rootTemplate->addText($js, 'js');
        
        $_GET['docId']= $docId;
	
	//$this->page->LibOpenDoc($tpl);	      		
	
  }
  
}

?>
