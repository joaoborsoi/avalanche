<?php 

require_once("AV_DBOperationObj.inc");
require_once("CHAT_Manager.inc");

class CHAT_InsMember extends AV_DBOperationObj
{
 
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'nick';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;

    $this->fieldsDef[1]['fieldName'] = 'roomId';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;

    $this->fieldsDef[2]['fieldName'] = 'memberColor';
    $this->fieldsDef[2]['required'] = true;
    $this->fieldsDef[2]['allowNull'] = false;

  }

  function Execute()
  {
      
    $sessionId = $this->pageBuilder->sessionH->sessionID;
  
    $maganer = new  CHAT_Manager($this);   
    $maganer->LoadMessages();
    return $maganer->InsMember($this->fieldParser->fields['nick'],
			       $this->fieldParser->fields['roomId'],
                               $this->fieldParser->fields['memberColor'],
                               $sessionId);
  }
}

?>