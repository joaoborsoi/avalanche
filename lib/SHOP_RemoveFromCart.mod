<?php

require_once('AV_DBOperationObj.inc');
require_once('SHOP_Cart.inc');


class SHOP_RemoveFromCart extends AV_DBOperationObj
{

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $cart = new SHOP_Cart($this);
    $cart->RemoveFromCart($this->fields['docId']);
  }
}

?>
