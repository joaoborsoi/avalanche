<?php

require_once('AV_OperationObj.inc');

class CMS_SendContact extends AV_OperationObj
{
  protected $form;

  protected function ParseInput(&$inputSources)
  {
    $this->form = new FORM_DynTable('CMS_Contact', $this);
    parent::ParseInput($inputSources);

    $this->fieldsDef = $this->form->LoadFieldsDef();
    if(is_array($this->fieldParser))
    {
      foreach($this->fieldParser as $key => $parser)
      {
	$this->fieldParser[$key]->Parse($this->fieldsDef);
	$this->fields = array_merge($this->fields, 
				    $this->fieldParser[$key]->fields);
      }
    }
    else
    {
      $this->fieldParser->Parse($this->fieldsDef);
      $this->fields = array_merge($this->fields, $this->fieldParser->fields);
    }
  }

  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'mailTo';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'e-mail';
  }

  protected function Execute()
  {
    foreach($this->fieldsDef as &$fieldDef)
    {
      if($fieldDef['labelId']!=NULL)
      {
	$labelsList[] = $fieldDef['labelId'];
	$fieldLabels[$fieldDef['fieldName']]=$fieldDef['labelId'];
      }
    }
    $labelsList[] = 'cmsSiteContactTag';
    $langLabel = new LANG_Label($this);
    $defLang = $this->pageBuilder->siteConfig->getVar("defaults",'lang');
    $labels = $langLabel->GetLabels($labelsList,$defLang);

    $mailTo = $this->fields['mailTo'];
    $mailFrom = $this->pageBuilder->siteConfig->getVar("cms", "mailFrom");
    $subject = $this->pageBuilder->siteConfig->getVar("cms", "subjectPrefix");
    $subject .= ' ' . $labels['cmsSiteContactTag'];

	
    $body = "<!DOCTYPE html PUBLIC \"-";
    $body .= "//W3C//DTD XHTML 1.0 Strict//EN\" ";
    $body .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
    $body .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>";
    $body = '';
    foreach($this->fieldsDef as &$fieldDef)
    {
      if($fieldDef['fieldName']=='mailTo')
	continue;

      // max number of bytes per variable
      if(strlen($this->fields[$fieldDef['fieldName']])>500)
	throw new AV_Exception(INVALID_FIELD, $this->lang,
			       array('fieldName'=>$fieldDef['fieldName'],
				     'addMsg'=>" (".$fieldDef['fieldName'].")"));

      $body .= '<p><strong>'.$labels[$fieldLabels[$fieldDef['fieldName']]].':</strong> ';
      switch($fieldDef['type'])
      {
      case 'select':
	$modobj = $this->form->GetSelectField($fieldDef,$this->fields);
	$body .= $modobj->attrs['text'];
	break;
      case 'textArea':
	$body .= '</br>';
	$body .= nl2br(stripSlashes($this->fields[$fieldDef['fieldName']]));
	break;
      default:
	$body .= stripSlashes($this->fields[$fieldDef['fieldName']]);
	break;
      }
      $body .= "</p>\r\n";
    }
    $body .= '</body></html>';
    	 	
    $headers .= "From: $mailFrom\r\n";
    $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";

    // NOW SEND
    if(!mail($mailTo, $subject, $body, $headers ,"-r".$mailFrom))
    { 
      // Se "não for Postfix"
      $headers .= "Return-Path: " . $mailFrom . "\n"; 
      if(!mail($mailTo, $subject, $body, $headers))
	throw new AV_Exception(FAILED, $this->lang);
    }
  }
}

?>