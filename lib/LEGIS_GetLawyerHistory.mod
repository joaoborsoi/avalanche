<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Process.inc');

class LEGIS_GetLawyerHistory extends AV_DBOperationObj
{
  function LoadFieldsDef()
  { 
    $this->fieldsDef[0]['fieldName'] = 'userId';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;
  }

  function Execute()
  { 
    $this->isArray = true;
    $fields = & $this->fieldParser->fields;
    $sumary = new LEGIS_Process($this);
    $result = $sumary->GetLawyerHistory($fields['userId']);
    $this->LoadChildrenFromRows($result, 'name');
  } 

}

?>
