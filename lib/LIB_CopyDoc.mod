<?php

require_once('AV_DBOperationObj.inc');
require_once('LIB_Document.inc');

class LIB_CopyDoc extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;

    $this->fieldsDef[1]['fieldName'] = 'path';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
    $this->fieldsDef[1]['default'] = '/';
    $this->fieldsDef[1]['stripSlashes'] = false;

    $this->fieldsDef[2]['fieldName'] = 'beforeDocId';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $doc = new LIB_Document($this);
    $doc->LoadFieldsDef($this->fields['docId']);
    return $doc->Duplicate($this->fieldParser->fields['path'],
			   $this->fieldParser->fields['docId'],
			   $this->fieldParser->fields['beforeDocId']);
  }
}

?>