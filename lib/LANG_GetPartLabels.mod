<?php 

require_once("AV_DBOperationObj.inc");
require_once("LANG_Label.inc");

class LANG_GetPartLabels extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'part';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;

    $this->fieldsDef[1]['fieldName'] = 'lang';
    $this->fieldsDef[1]['required'] = true;
    $this->fieldsDef[1]['allowNull'] = false;
  }

  function Execute()
  {
    $aux = new LANG_Label ($this);
    $res = $aux->GetPartLabels($this->fields['part'],
			       $this->fields['lang']);
    foreach($res as $value)
      $this->attrs[$value['labelId']] = $value['value'];

  }
}

?>