<?php

require_once('AV_Page.inc');

class CMS_Ajax extends AV_Page
{
  protected $memberGroups;

  protected function Header()
  {
    // by default, disable cache
    $this->DisableCache();
    header('Content-Type: text/javascript');
  }

  protected function LoadPage()
  {
    // if logged, loads user's groups
    if($this->pageBuilder->sessionH->isMember())
    {
      $getMemberGroups = $this->LoadModule('UM_GetMemberGroups');
      foreach($getMemberGroups->children as & $child)
	$this->memberGroups[$child->attrs['groupId']]=
	  $child->attrs['groupName'];
    }

    $this->pageBuilder->rootTemplate('adminScriptSrc');
  }
}
