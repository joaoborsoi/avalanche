<?php

require_once('AV_DBOperationObj.inc');
require_once('SHOP_Cart.inc');


class SHOP_GetCart extends AV_DBOperationObj
{
  protected function Execute()
  {
    $this->isArray = true;

    $cart = new SHOP_Cart($this);
    $rows = $cart->GetCart();

    // load children
    $this->LoadChildrenFromRows($rows, 'cart');
  }
}

?>
