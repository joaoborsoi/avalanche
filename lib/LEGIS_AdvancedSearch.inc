<?php

class LEGIS_AdvancedSearch
{
  protected $module;
  protected $dbDriver;

  function __construct(&$module)
  {
    $this->module = & $module;
    $this->dbDriver = & $module->dbDriver;
    $this->lang = $this->module->pageBuilder->lang;
  }

  protected function Search($returnFields, $limit, $offset, & $tables, 
			    & $tablesWhere, & $where, &$totalResults)
  {
    $COUNT = 'SELECT COUNT(DISTINCT doc.docId) ';
    $FIELDS = 'SELECT DISTINCT ' . $returnFields;

    $SQL = $this->BuildFromAndWhere($tables, $tablesWhere, $where);

    if($limit != NULL)
    {
      $SQLLIMIT = ' LIMIT ';
      if(isset($offset))
	$SQLLIMIT .= "$offset,";
      $SQLLIMIT .= $limit;
    }

    $totalResults = $this->dbDriver->GetOne($COUNT . $SQL);

    return $FIELDS . $SQL. $SQLLIMIT;
  }

  protected function BuildFromAndWhere(& $tables, & $tablesWhere, & $where)
  {
    $SQL = ' FROM ' . implode(',', $tables);
    $SQL .= ' WHERE ' . implode(' AND ', $tablesWhere);

    $first = true;
    foreach($where as $field)
    {
      if($first)
      {
	$first = false;
	$SQL .= ' AND (';
      }
      else
	$SQL .= ') AND (';

      foreach($field as $key=>$expr)
      {
	if($key > 0)
	  $SQL .= ' OR ';
	$SQL .= $expr;
      }
    }
    if(!$first)
      $SQL .= ') ';
    return $SQL;
  }

  protected function GetDateParam($field, & $param)
  {
    $expr = "$field IS NOT NULL AND $field <> '0000-00-00'";
    if($param['after'] !== '' && $param['after'] !== NULL)
      $expr .= " AND '" . $param['after'] . "' <= $field";

    if($param['before'] !== '' && $param['before'] !== NULL)
      $expr .= " AND $field <= '" . $param['before'] . "'";
    return $expr;
  }
}
				
?>