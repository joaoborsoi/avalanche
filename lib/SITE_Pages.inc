<?php

require_once('AV_Page.inc');
require_once('webElements.inc');

class SITE_Pages extends AV_Page
{
 
  var $wrap;
  var $path; 
  var $labels;
  var $base;
  var $sitePages;
  var $docId;
  var $siteName;
  
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  protected function LoadPage()
  {
    	//header('Content-Type: text/html; charset=UTF-8');
    	 
    	
    	$this->labels = &$this->LoadModule('LANG_GetPageLabels');
        $this->base = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
    	if(dirname($_SERVER['PHP_SELF'])!='/') $this->base .= '/';
    	$apath=explode('?',$_SERVER['REQUEST_URI']);
    	$this->path = $apath[0];
    	$this->pageBuilder->rootTemplate('siteMain');
    	$this->pageBuilder->rootTemplate->addText((string)$this->lang, 'lang');
    	$this->pageBuilder->rootTemplate->addText((string)$this->base, 'base');
	$this->pageBuilder->rootTemplate->addText((string)$this->path, 'path');	
	if(!isset($_GET['pageId']))$_GET['pageId']='siteHome';
	$this->pageBuilder->rootTemplate->addText((string)$_GET['pageId'], 'pageId');	
	$this->pageBuilder->rootTemplate->addText((string)$this->pageBuilder->avVersion, 'avVersion');	
	$this->pageBuilder->rootTemplate->addText((string)$this->pageBuilder->siteConfig->getVar("gallery", "outlineType"), 'outlineType');	
	$this->pageBuilder->rootTemplate->addText((string)$this->labels->attrs['loadingTag'], 'loadingTag');	
	$this->pageBuilder->rootTemplate->addText((string)$this->labels->attrs['imageTipTag'], 'imageTipTag');	
	
	// IDS
	$this->sitePages = $this->pageBuilder->siteConfig->getSection('pages');
	//$this->knownIds= array_flip($sitePages);
	
	if(!isset($_GET['docId']) && isset($this->sitePages[$_GET['pageId']])){ 
		$_GET['docId']= $this->sitePages[$_GET['pageId']];
	}
	if(isset($_GET['docId'])){		
		$this->docId=$_GET['docId'];
		$this->setPageId($this->docId);
	}
    	// SUBTEMPLATE 
    	$this->wrap = $this->pageBuilder->loadTemplate('siteWrap');
	$this->wrap->addText((string)$this->lang, 'lang');
	$this->wrap->addText((string)$this->labels->attrs['siteSearchTag'], 'siteSearchTag');
	$footer = $this->pageBuilder->loadTemplate('siteFooter-'.$this->lang);
    	$this->wrap->addTemplate($footer, 'footer');
    	$this->pageBuilder->rootTemplate->addTemplate($this->wrap, 'wrap');
    	$bottom = $this->pageBuilder->loadTemplate('siteBottom');
    	$this->pageBuilder->rootTemplate->addTemplate($bottom, 'bottom');
	
	// LOGO
	$_GET['docId']=24;
	$module = $this->LoadModule('LIB_GetDoc');
	$this->siteName=$module->children[0]->attrs['title'];
	$gis=getimagesize($this->base.'/images/24');
	$logo=img('src=original/24&alt='.$this->siteName.'&width='.$gis[0].'&height='.$gis[1]);
	$this->wrap->addText($logo, 'logo');
	
	// LANG
	$switchLang = $this->pageBuilder->loadTemplate('siteLang');
	$switchLang->addText((string)$this->path, 'path');
	$this->wrap->addTemplate($switchLang, 'langLinks');
	
	// MENU
	require_once("SITE_Menu.inc");
        $menu = new menu($this,'/Menu/'); 
        $nav = $menu->get_html(); 
        //print_r($nav);exit;
        $this->wrap->addText((string) $nav, 'nav');
        $navBottom = $this->pageBuilder->loadTemplate('siteNavBottom');
        $this->wrap->addTemplate($navBottom, 'nav');
        
        // RSS LINK
        try{
        	$_GET['docId']= $this->sitePages['siteNews'] ;
		$module = $this->LoadModule('LIB_GetDoc');
		$meta='<link rel="alternate" type="application/rss+xml" title="'.$module->children[0]->attrs['title'].'" href="siteFeed.av" />'."\n";
		$this->pageBuilder->rootTemplate->addText((string)$meta, 'meta');   
	}
	catch(Exception $e){
		if($e->getCode()!=INVALID_FIELD)
			throw $e;
	}
	
	// DOC ID
	if(isset($this->docId)) $_GET['docId']=$this->docId;
        else unset($_GET['docId']);
    			
  }

  function getFolders($path){
        $_GET['path']='/content'.$path;
        $getDoc = $this->LoadModule('LIB_GetFolderList');
         return $getDoc->children; 
    }
    function getFiles($path, $recurse=0){
        $_GET['recursiveSearch']=$recurse;
        $_GET['path']= '/content'.$path;
        $getDoc = $this->LoadModule('LIB_Search');
        return $getDoc; //->children[0]; 
    }
    
  function LibOpenDoc(&$template)
  {
    $getDoc = $this->LoadModule('LIB_GetDoc');
    $getDoc->PrintHTML($template);  
    // escreve no tag fileContent 
  }

  function setPageTitle ($txt,$full=false){      
  	  $title=htmlentities_utf(strip_tags($txt));
  	  if(!$full)$title.=' - '.$this->siteName;
        $this->pageBuilder->rootTemplate->addText((string)$title, 'title');  
  }
  function setPageDescr ($txt){  
  	if($txt){
  		$meta='<meta name="description" content="'.htmlentities_utf($txt).'" />'; 
        	$this->pageBuilder->rootTemplate->addText((string)$meta, 'meta');
        }
  }
  function setPageAuthor ($txt){  
  	if($txt){
  		$meta="\n".'<meta name="author" content="'.htmlentities_utf($txt).'" />'; 
        	$this->pageBuilder->rootTemplate->addText((string)$meta, 'meta');
        	$by='id="by-'.$this->toSlug($txt).'"';
        	$this->pageBuilder->rootTemplate->addText((string)$by, 'byTag');
        }
  }
  function setPageId($txt){  
  	if($txt){
  		$idstr='id="doc'.$txt.'"';
  		$this->pageBuilder->rootTemplate->addText($idstr, 'docId');
        }
  }
  function toSlug($string,$space="-") {
	if (function_exists('iconv')) {
	$string = @iconv('UTF-8', 'ASCII//TRANSLIT', $string);
	}
	$string = preg_replace("/[^a-zA-Z0-9 -]/", "", trim($string));
	$string = mb_strtolower($string);
	$string = str_replace(" ", $space, $string);
	return $string;
  }
}

?>
