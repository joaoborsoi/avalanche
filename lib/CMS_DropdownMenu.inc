<?php
require_once('CMS_Menu.inc');

class CMS_DropdownMenu extends CMS_Menu
{

  function LoadMenu($menuItem,&$submenu,&$subareas,&$topPath)
  {
    if($this->LoadSubmenu($menuItem,$submenu,$subareas,$topPath))
    {
      $submenu = "<ul class='nav navbar-nav navbar-right'>\n".
	$submenu."</ul>\n";
      return true;
    }
    return false;
  }


  protected function LoadSubmenu($menuItem,&$submenu,&$subareas,&$topPath)
  {
    $menuItemAux = addslashes($menuItem);

    $_GET['path'] = '/content/Menu/'.$menuItemAux;
    $_GET['fixedOrder'] = '1';
    $getFolderList = & $this->page->LoadModule('LIB_GetFolderList');

    if(count($getFolderList->children)==0)
      return false;

    foreach($getFolderList->children as & $folder)
    {
      $currPath = $menuItem.'/'.$folder->attrs['name'].'/';

      // anchor link
      $linkData['href'] = "area/".str_replace("%2F", "/", urlencode($currPath));
      $linkData['text'] = $folder->attrs['menuTitle'];
      $link = "<a href=\"".$linkData['href']. "\">";
      $link .= $folder->attrs['menuTitle'] ."</a>";

      // if we reach the selected menu item, build subarea for content
      if($menuItemAux.'/' == $this->page->menuPath && $_REQUEST['docId'] == NULL)
	$subareas .= "<li>$link</li>";

      // build submenu item
      $submenu .= "<li id='menu_".$folder->attrs['folderId']."' ";
      $class = array();
      $currPath = addslashes($currPath);
      if(!strncmp($currPath,$this->page->menuPath,strlen($currPath)))
      {
	if($currPath == $this->page->menuPath)
	{
	  // updates topPath
	  if($_REQUEST['docId'] == NULL)
	    $topPath .= "<li class='active'>".$folder->attrs['menuTitle']."</li>\n";
	  else
	  {
	    if($this->pageBuilder->siteConfig->getVar('cms','menuParentPathLink'))
	      $topPath .= '<li>'.$link."</li>\n";
	    else
	      $topPath .= '<li>'.$folder->attrs['menuTitle']."</li>\n";
	  }

	  $class[] = "active";
	}
	else
	  // updates topPath
	  if($this->pageBuilder->siteConfig->getVar('cms','menuParentPathLink'))
	    $topPath .= '<li>'.$link."</li>\n";
	  else
	    $topPath .= '<li>'.$folder->attrs['menuTitle']."</li>\n";
      }
	
      // loads subsubmenu
      $subsubmenu = '';
      if($this->LoadSubmenu($menuItem.'/'.$folder->attrs['name'],$subsubmenu,
			 $subareas,$topPath))
      {
	$class[] = "dropdown";
	$subsubmenu = "<ul class='dropdown-menu'>$subsubmenu</ul>";
	$link = "<a class='dropdown-toggle' data-toggle='dropdown' ";
	$link .= "href=\"".$linkData['href']. "\">";
	$link .= $folder->attrs['menuTitle'];
	$link .= "<b class='caret'></b></a>";
      }
	
      if(count($class)>0)
	$submenu .= " class='".implode(' ',$class)."' ";
      $submenu .= ">$link";
      $submenu .= "$subsubmenu</li>";
    }
    return true;
  }

}
?>