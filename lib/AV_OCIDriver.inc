<?php

require_once('AV_DBDriver.inc');
require_once('AV_OCILexAnalyzer.inc');
require_once('AV_OCISQLAnalyzer.inc');
require_once('AV_OCIArray.inc');

class AV_OCIDriver extends AV_DBDriver
{
  protected $conn;
  protected $blobs = array();

  function __construct(& $pageBuilder)
  {
    parent::__construct($pageBuilder);

    $username = $pageBuilder->siteConfig->getVar('db','userName');
    $password = $pageBuilder->siteConfig->getVar('db','passWord');
    $connection_string = $pageBuilder->siteConfig->getVar('db',
							  'connection_string');
    $this->conn = oci_pconnect($username, $password, $connection_string,'UTF8');
    if(!$this->conn)
    {
      $e = oci_error();
      throw new AV_Exception(FAILED,$this->pageBuilder->lang,array('addMsg'=>$e['message']));
    }

    $SQL = "ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'";
    $this->ExecSQL($SQL);
  }
 
  function BindBlob($var,&$blob)
  {
    $this->blobs[$var]['data'] = &$blob;
  }

  function ExecSQL($SQL, $reportError = true)
  {
    try
    {
      $SQL = $this->PrepareSQL($SQL);
      if($SQL == NULL)
	return;

      if($this->logDBOper)
	$this->pageBuilder->PrintLog($SQL, false);

      $stid = oci_parse($this->conn, $SQL);
      if(!$stid)
      {
	$e = oci_error();
	throw new AV_Exception(FAILED,$this->pageBuilder->lang,array('addMsg'=>$e['message']));
      }

      // bind blobs
      if(count($this->blobs)>0)
      {
	foreach($this->blobs as $fieldName=>&$blob)
	{
	  $blob['descriptor'] = oci_new_descriptor($this->conn, OCI_D_LOB);
	  oci_bind_by_name($stid, $fieldName, $blob['descriptor'], 
			   -1, OCI_B_BLOB);

	  
	}
      }

      if(!oci_execute($stid, OCI_NO_AUTO_COMMIT))
      {
	$e = oci_error();
	throw new AV_Exception(FAILED,$this->pageBuilder->lang,array('addMsg'=>
							$e['message']));
      }

      foreach($this->blobs as $fieldName=>&$blob)
      {
	if(!$blob['descriptor']->save($blob['data']))
	{
	  throw new AV_Exception(FAILED,$this->pageBuilder->lang,
				 array('addMsg' => 
				       ' Could not save the blob'));
	}
	$blob['descriptor']->free();
      }

      // free resources
      $this->blobs = array();
      oci_free_statement($stdi);
    }
    catch(Exception $e)
    {
      // empty blobs;
      $this->blobs = array();
      throw $e;
    }

  }

  function GetAll($SQL, $reportError = true)
  {
    $SQL = $this->PrepareSQL($SQL);
    if($SQL == NULL)
      return;

    if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $stid = oci_parse($this->conn, $SQL);
    if(!$stid)
    {
      $e = oci_error();
      throw new AV_Exception(FAILED,$this->pageBuilder->lang,array('addMsg'=>$e['message']));
    }

    if(!oci_execute($stid, OCI_NO_AUTO_COMMIT))
    {
      $e = oci_error();
      throw new AV_Exception(FAILED,$this->pageBuilder->lang,array('addMsg'=>$e['message']));
    }

    //oci_fetch_all($stid, $rows, 0,-1,OCI_FETCHSTATEMENT_BY_ROW);
    while (($row = oci_fetch_assoc($stid)) != false)
      $rows[] = new AV_OCIArray($row);

    oci_free_statement($stdi);

    return $rows;
  }


  function GetRow($SQL,$reportError = true)
  {
    $SQL = $this->PrepareSQL($SQL);
    if($SQL == NULL)
      return;

     if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $stid = oci_parse($this->conn, $SQL);
    if(!$stid)
    {
      $e = oci_error();
      throw new AV_Exception(FAILED,$this->pageBuilder->lang,array('addMsg'=>$e['message']));
    }

    if(!oci_execute($stid, OCI_NO_AUTO_COMMIT))
    {
      $e = oci_error();
      throw new AV_Exception(FAILED,$this->pageBuilder->lang,array('addMsg'=>$e['message']));
    }

    $row = oci_fetch_assoc($stid);

    oci_free_statement($stdi);

    if($row!=NULL)
      return new AV_OCIArray($row);
    else
      return NULL;
  }

  function GetCol($SQL,$reportError = true)
  {
    $SQL = $this->PrepareSQL($SQL);
    if($SQL == NULL)
      return;

     if($this->logDBOper)
      $this->pageBuilder->PrintLog($SQL, false);

    $stid = oci_parse($this->conn, $SQL);
    if(!$stid)
    {
      $e = oci_error();
      throw new AV_Exception(FAILED,$this->pageBuilder->lang,array('addMsg'=>$e['message']));
    }

    if(!oci_execute($stid, OCI_NO_AUTO_COMMIT))
    {
      $e = oci_error();
      throw new AV_Exception(FAILED,$this->pageBuilder->lang,array('addMsg'=>$e['message']));
    }

    oci_fetch_all($stid, $rows, 0,-1,OCI_NUM);

    oci_free_statement($stdi);

    return $rows[0];
  }

  function GetOne($SQL, $reportError = true)
  {
    $row = $this->GetRow($SQL, $reportError);

    $col = each($row);
    return $col[1];
  }


  protected function PrepareSQL($SQL)
  {
    $sqlAnalyzer = new AV_OCISQLAnalyzer($this->pageBuilder, $this->logDBOper);
    return $sqlAnalyzer->AnalizeSQL($SQL);
  }
}

?>