<?php 
function htmlentities_utf($text)
{
  return htmlentities($text, ENT_COMPAT, 'utf-8');
}

function mb_ucwords($str)
{
  return mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
}

?>