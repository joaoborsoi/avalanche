<?php

require_once('AV_Page.inc');

class JSON_Page extends AV_Page
{

  protected function Header()
  {
    header('Content-Type: application/json; charset=utf-8');
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('json');
  }
}
