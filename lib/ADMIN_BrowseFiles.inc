<?php

require_once('ADMIN_Page.inc');

class ADMIN_BrowseFiles extends ADMIN_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminRoot');
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');
    $this->pageBuilder->rootTemplate->addText($_REQUEST['tab'],
					      'pageId');

    $css = '@import "avalanche-';
    $css .= $this->pageBuilder->avVersion.'/browseFiles.css";';
    $this->pageBuilder->rootTemplate->addText($css,'css');

    $pageLabels = & $this->LoadModule("LANG_GetPageLabels");

    $pageLabels->PrintHTML($this->pageBuilder->rootTemplate, NULL, 
			   'htmlentities_utf');

    $contentTpl = $this->pageBuilder->loadTemplate('adminContent');
    $pageLabels->PrintHTML($contentTpl, NULL, 'htmlentities_utf');
    $this->pageBuilder->rootTemplate->addTemplate($contentTpl, 'content');

    $libContentTpl = $this->pageBuilder->loadTemplate('adminLib');
    $libContentTpl->addText($_REQUEST['tab'], 'pageId');
    $contentTpl->addTemplate($libContentTpl, 'tabContent');

    $libFooterTpl = $this->pageBuilder->loadTemplate('adminBrowseFilesFooter');
    $pageLabels->PrintHTML($libFooterTpl, NULL, 'htmlentities_utf');
    $libFooterTpl->addtext((string)$_REQUEST['returnFunction'],
			   'returnFunction');
    $libFooterTpl->addtext((string)$_REQUEST['fieldName'],'fieldName');
    $contentTpl->addTemplate($libFooterTpl, 'footer');

    $script = "Initiate('".$_REQUEST['tab']."');";
    $this->pageBuilder->rootTemplate->addText((string)$script, 'onLoad');

  }

}

?>