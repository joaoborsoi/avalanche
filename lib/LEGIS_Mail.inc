<?php

require_once('FORM_DynTable.inc');

class LEGIS_Mail extends FORM_DynTable
{
  function Insert(& $fields, $extraFields = array())
  {
    if(count($fields['entityList'])==0)
      throw new AV_Exception(FAILED,$this->lang);

    parent::Insert($fields,$extraFields);

    $SQL = 'INSERT INTO LEGIS_MailEntity (docId, entityId) VALUES ';
    $SQL .= "(LAST_INSERT_ID(),'".implode("'),(LAST_INSERT_ID(),'",
					  $fields['entityList']);
    $SQL .= "')";
    $this->dbDriver->ExecSQL($SQL);
  }

  protected function ParseFieldDef(& $fieldDef,$disableFileRequired,$disableRequired)
  {
    parent::ParseFieldDef($fieldDef,$disableFileRequired,$disableRequired);

    switch($fieldDef['type'])
    {
    case 'emailAddress':
    case 'emailAddressList':
    case 'entityList':
      // gambiarra pra evitar addslashes      
      $fieldDef['stripSlashes'] = true; 
    break;
    }
  }

  protected function &GetField(& $fieldDef, & $values, $keyFields,  
			       & $templates, $sourceId, $dateFormat = NULL,
			       $numberFormat = true, $fieldName = NULL)
  {
    switch ($fieldDef['type'])
    {
    case 'emailAddress':
    case 'emailAddressList':
      $values[$fieldDef['fieldName']] = json_decode($values[$fieldDef['fieldName']]);
      return $this->GetTextField($fieldDef, $values);

    default:
      return parent::GetField($fieldDef,$values,$keyFields,$templates,
			      $sourceId,$dateFormat,$numberFormat);
     }
  }
}

?>