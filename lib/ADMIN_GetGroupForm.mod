<?php

//--Class: UM_GetGroupForm
//--Parent: AV_DBOperationObj
//--Desc: Responsible for retrieving groups data

require_once('AV_DBOperationObj.inc');
require_once('UM_User.inc');

class ADMIN_GetGroupForm extends AV_DBOperationObj
{
  //--Method: LoadFieldsDef
  //--Desc: Redefines parent method to define module's input fields
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'groupId';
    $this->fieldsDef[0]['required'] = false;
    $this->fieldsDef[0]['allowNull'] = true;

    $this->fieldsDef[1]['fieldName'] = 'fieldName';
    $this->fieldsDef[1]['required'] = false;
    $this->fieldsDef[1]['allowNull'] = true;

    $this->fieldsDef[2]['fieldName'] = 'sourceId';
    $this->fieldsDef[2]['required'] = false;
    $this->fieldsDef[2]['allowNull'] = true;
    $this->fieldsDef[2]['consistType'] = 'integer';
    return SUCCESS;
  }

  //--Method: Execute
  //--Desc: Redefines parent method to implement module's execution
  function Execute()
  {
    if($this->fields['fieldName'] != '')
    {
      $templates['selectMult'] = 'adminUserEmptySelectMult';
      $templates['selectMultArray']  = 'adminUserSelectMultArray';
    }
    else
    {
      $templates['dummy'] = 'adminUserText';
      $templates['text'] = 'adminUserText';
      $templates['selectMult'] = 'selectMultField';
      $templates['selectMultOption']  = 'selectMultFieldOption';
      $templates['boolean']  = 'adminUserBoolean';
      $templates['password']  = 'adminUserPassword';
    }

    $groupMan = new UM_Node('UM_Group',$this);

    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 

    if($this->fields['groupId'] != NULL)
      $keyFields = Array('groupId'=>$this->fields['groupId']);
    else
      $keyFields = NULL;

    $result = $groupMan->GetForm($keyFields, $this->fields['fieldName'],
				$this->fields['sourceId'], $templates, 
				$formObject, $status);
    if($status != SUCCESS)
      return $status;

    $this->children[] = $result;
    return SUCCESS;
  }
}

?>

