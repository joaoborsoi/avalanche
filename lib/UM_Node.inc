<?php

require_once('FORM_DynTable.inc');
require_once('LIB_Folder.inc');

//--Class: UM_Node
//--Desc: Implements user and group generic operations.
class UM_Node extends FORM_DynTable
{
  //--Method: Insert
  //--Desc: Inserts a new user. Returns operation status.
  function Insert(& $fields, $extraFields)
  {
    $folder = new LIB_Folder($this->module);
    $folder->CheckFolderPermission($fields['path'], true);
 
    parent::Insert($fields,$extraFields); 
  }

  //--Method: Set
  //--Desc: Updates user. Returns operation status.
  function Set($fields, $extraFields = array())
  {
    $folder = new LIB_Folder($this->module);
    $folder->CheckFolderPermission($fields['path'], true);

    return parent::Set($fields,$extraFields); 
  }

  //--Method: Del(& $keyFields)
  //--Desc: Removes user. Returns operation status.
  function Del($fields)
  {
    $folder = new LIB_Folder($this->module);
    $folder->CheckFolderPermission($fields['path'], true);
    parent::Del($fields);
  }

}

?>
