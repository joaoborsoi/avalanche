<?php

define("NOT_FOUND_ROOM",100);
define("NOT_FOUND_NICK",101);
define("INVALID_NICK",102);
define("ROOM_FULL",103);

//--Class: CHAT_Manager
//--Parent: 
//--Desc: Responsavel por realisar toda a gerencia do Chat.

class CHAT_Manager {

  var $dbDriver;
  var $module;

  //--Method: LoadMessages
  //--Desc: Define as mensagens de retorno.
 
  function LoadMessages() 
  {           
     switch($this->module->pageBuilder->lang) 
     { 
       case 'en_US': // US english messages 
	 $this->module->messages[NOT_FOUND_ROOM] = "Room not found.";
         $this->module->messages[NOT_FOUND_NICK] = "Nick not found.";
         $this->module->messages[INVALID_NICK]  = "Invalid nick.";
         $this->module->messages[ROOM_FULL] = "Room Full.";
	 break; 
 
       case 'pt_BR': // Brazillian portuguese messages 
	 $this->module->messages[NOT_FOUND_ROOM] = "Sala não encontrada.";
         $this->module->messages[NOT_FOUND_NICK] = "Nick não encontrado.";
         $this->module->messages[INVALID_NICK]  = "Nick invalido.";
         $this->module->messages[ROOM_FULL] = "Sala Cheia.";
	 break;
     }  
  }


  //--Method: MergeResults
  //--Desc: Faz o marge entre as tabelas de salas e membros para poder 
  //--      retornar o numero de membros,mesmo que seja zero.

  function MergeResults($res1, $res2) 
  { 
    foreach($res2 as $r2) 
    { 
      $found = false; 
      reset($res1);
      while((list($key, $r1) = each($res1)) && !$found) 
      { 
        if($r1['roomId'] == $r2['roomId']) 
           $found = true; 
      } 
      if(!$found) 
      { 
        $r2['numMembers'] = 0;  
        $res1[] = $r2; 
      }    
    } 
    return $res1; 
  } 


  function CHAT_Manager(&$module)
  {
     $this->dbDriver =  & $module->dbDriver;
     $this->module = & $module;
  }

     
  //--Method: SetRoom
  //--Desc: Cria uma sala nova se nao existir ou apenas atualiza a descrição
  //--      se ela ja existir.

  function SetRoom($name,$maxMembers,$descr)
  {
     
     $SQL = "SELECT name FROM CHAT_Room WHERE name = '$name' FOR UPDATE";  
     $name_tmp = $this->dbDriver->GetOne($SQL, $status); 
     
     if($status != SUCCESS)
	return $status;
      
     if($name_tmp === NULL) 
     { 
	$SQL = "INSERT INTO CHAT_Room (roomId,name,maxMembers,descr) 
                VALUES (NULL,'$name','$maxMembers','$descr')";
        return $this->dbDriver->ExecSQL($SQL);
     }                                                         
     
     $SQL = "UPDATE CHAT_Room SET descr = '$descr', maxMembers = '$maxMembers' WHERE name = '$name'"; 
     return $this->dbDriver->ExecSQL($SQL);
  }

  
  //--Method: DelRoom
  //--Desc: Deleta uma sala criada.

  function DelRoom($roomId)
  {
     $SQL = "SELECT roomId FROM CHAT_Room WHERE roomId = '$roomId' FOR UPDATE";  
     $roomId_tmp = $this->dbDriver->GetOne($SQL, $status); 
     
     if($status != SUCCESS) 
       return $status;

     if($roomId_tmp === NULL)
       return NOT_FOUND_ROOM;  

     $SQL = "DELETE from CHAT_Room WHERE roomId = '$roomId'";
     return $this->dbDriver->ExecSQL($SQL);     
  }


  //--Method: GetRoom
  //--Desc: Recupera todas as informações referentes a todas as salas criadas.

  function GetRoom(&$status)
  {
    $SQL1 = "SELECT roomId, name, maxMembers, descr FROM CHAT_Room";

    $SQL2 = "SELECT CHAT_Room.roomId, name, maxMembers, descr,count(*) as numMembers 
             FROM CHAT_Room, CHAT_RoomMember 
             WHERE CHAT_Room.roomId = CHAT_RoomMember.roomId  
             GROUP BY CHAT_Room.roomId";
     
    $res1 = $this->dbDriver->GetAll($SQL1, $status1);
    $res2 = $this->dbDriver->GetAll($SQL2, $status2);

    if($status1 != SUCCESS && $status2 != SUCCESS) 
       return $status;
     
    return $this->MergeResults($res2,$res1);  
  }


  //--Method: InsMember
  //--Desc: Responsavel por inserir um membro em uma sala especifica de Chat.
  //--      Verifica se a sala existe e se o usuario tambem, se o usuario existir
  //--      ele remove o antingo tigo usuario e insere o novo, caso a sala de chat
  //--      nao exista retorna uma mensagem de erro. Incrementa o numero de usuarios
  //--      na sala.
  
  function InsMember($nick,$roomId,$memberColor,$sessionId)
  {
     $SQL = "SELECT roomId FROM CHAT_Room WHERE roomId = '$roomId' LOCK IN SHARE MODE";  
     $roomId_tmp = $this->dbDriver->GetOne($SQL, $status); 
     
     if($status != SUCCESS)
	return $status;
    
     if($roomId_tmp === NULL) 
       return NOT_FOUND_ROOM;   
      
     $SQL = "SELECT COUNT(*) FROM CHAT_RoomMember where roomId = '$roomId' FOR UPDATE";
     $numMembers = $this->dbDriver->GetOne($SQL, $status);
    
     if($status != SUCCESS)
	return $status;
   
     $SQL = "SELECT maxMembers FROM CHAT_Room WHERE roomId = '$roomId'";
     $maxMembers_tmp = $this->dbDriver->GetOne($SQL, $status);
     
     if($status != SUCCESS)
	return $status;
     
     if($numMembers >= $maxMembers_tmp)
        return ROOM_FULL;

     $SQL = "SELECT sessionId FROM CHAT_RoomMember 
             WHERE sessionId = '$sessionId' AND roomId = '$roomId'";
     $sessionId_tmp = $this->dbDriver->GetOne($SQL, $status);

     if($status != SUCCESS)
	return $status;
    
     if($sessionId_tmp === NULL)
     {
	$SQL = "INSERT INTO CHAT_RoomMember (roomId,sessionId,nick,lastReadMessage,insMemberDate,memberColor) 
                VALUES ('$roomId','$sessionId','$nick',NOW(),NOW(),'$memberColor')";

	return $this->dbDriver->ExecSQL($SQL);        
     } 
     else 
     {
	$status = $this->DelMember($sessionId,$roomId);
    
	if($status != SUCCESS)
	   return $status;

        $SQL = "SELECT COUNT(*) FROM CHAT_RoomMember where roomId = '$roomId' FOR UPDATE";
        $numMembers = $this->dbDriver->GetOne($SQL, $status);
    
        if($status != SUCCESS)
	   return $status;
   
	$SQL = "SELECT maxMembers FROM CHAT_Room WHERE roomId = '$roomId'";
	$maxMembers_tmp = $this->dbDriver->GetOne($SQL, $status);
     
	if($status != SUCCESS)
	   return $status;
     
	if($numMembers >= $maxMembers_tmp)
	   return ROOM_FULL;
   
	$SQL = "INSERT INTO CHAT_RoomMember (roomId,sessionId,nick,lastReadMessage,insMemberDate,memberColor) 
                VALUES ('$roomId','$sessionId','$nick',NOW(),NOW(),'$memberColor')";         

	return $this->dbDriver->ExecSQL($SQL);      
     }              
  }


  //--Method: DelMember
  //--Desc: Deleta um membro de uma determinada sala de chat, decrementa o numero de usuarios
  //--      que estao na sala.

  function DelMember($sessionId,$roomId)
  {
     $SQL = "SELECT sessionId FROM CHAT_RoomMember 
             WHERE sessionId = '$sessionId' AND roomId = '$roomId' FOR UPDATE";
  
     $sessionId_tmp = $this->dbDriver->GetOne($SQL, $status); 

     if($status != SUCCESS)
       return $status;
        
     $SQL = "DELETE FROM CHAT_RoomMember WHERE sessionId = '$sessionId' AND roomId = '$roomId'"; 

     return $this->dbDriver->ExecSQL($SQL);
  } 



  //--Method: GetMember
  //--Desc: Recupera todas as informações referentes ao membro de uma sala de chat.

  function GetMember($roomId,&$status)
  {
     $SQL = "SELECT roomId FROM CHAT_Room WHERE roomId = '$roomId' FOR UPDATE";  
     $roomId_tmp = $this->dbDriver->GetOne($SQL, $status); 
     
     if($status != SUCCESS)
        return NULL;

     if($roomId_tmp === NULL)
     {
        $status = NOT_FOUND_ROOM;
        return NULL;     
     }

     $SQL = "SELECT nick,lastReadMessage,insMemberDate  FROM CHAT_RoomMember WHERE roomId = '$roomId'";
   
     return $this->dbDriver->GetAll($SQL, $status);
  }



  //--Method: SendMessage
  //--Desc: Insere uma messagem de um usuario no buffer de messagens informando a sala para qual
  //--      foi destinada a mensagem e se foi para um determinado usuario ou nao. Realiza consis-
  //--      tencias para verificar a existencia da sala e do usuario destinado a mensagem.

  function SendMessage($roomId,$sessionId,$message,$target)
  {
     $SQL = "SELECT roomId FROM CHAT_Room WHERE roomId = '$roomId' LOCK IN SHARE MODE";  
     $roomId_tmp = $this->dbDriver->GetOne($SQL, $status); 
     
     if($status != SUCCESS)
	return $status;
    
     if($roomId_tmp === NULL) 
        return NOT_FOUND_ROOM;
    
     $SQL = "SELECT sessionId FROM CHAT_RoomMember 
             WHERE nick = '$target' AND roomId = '$roomId' LOCK IN SHARE MODE";
  
     $sessionId_tmp = $this->dbDriver->GetOne($SQL, $status);

     if($status != SUCCESS)
       return $status;       

     if($sessionId_tmp != $sessionId) 
     {
        $SQL = "INSERT INTO CHAT_Buffer (roomId,sessionId,date,message,target) 
                VALUES('$roomId','$sessionId',NOW(),'$message',IF('$sessionId_tmp' = '',NULL,'$sessionId_tmp'))";

	$control = 1;
	$status = $this->dbDriver->ExecSQL($SQL); 
	do{
	  if (($status == INDEX_VIOLATED)||($status == UNKNOWN_ERROR)){
	    srand ((double) microtime() * 1000000);
	    $time = rand(1,3);
	    sleep($time);
	    $status = $this->dbDriver->ExecSQL($SQL); 
	  }else
	    return $status; 
	}while($control == 1);
	
     }
     else
        return INVALID_NICK;
  }
  


  //--Method: GetBuffer
  //--Desc: Recupera todas as informações referentes as messagens enviada e recebidas
  //--      por um determinado usuario apartir de uma data espeficifica ou desde quando
  //--      ele entrou na sala. Faz todas as consistencias de validacao da sala.

  function GetBuffer($roomId,$sessionId,&$status)
  {  
     $SQL = "SELECT roomId FROM CHAT_Room WHERE roomId = '$roomId' LOCK IN SHARE MODE";  
     $roomId_tmp = $this->dbDriver->GetOne($SQL, $status); 
     
     if($status != SUCCESS)
	return $status;
    
     if($roomId_tmp === NULL) 
       return NOT_FOUND_ROOM;      
         
      $SQL =  "SELECT rm1.nick, rm1.memberColor, message, FROM_UNIXTIME(UNIX_TIMESTAMP(date),'%H:%i:%s') as date, 
                      date as unixDate, IF (target IS NULL, 0,1) AS 'restrict' 
	       FROM CHAT_Buffer, CHAT_RoomMember rm1, CHAT_RoomMember rm2 
               WHERE
                     CHAT_Buffer.roomId = rm1.roomId AND 
                     CHAT_Buffer.sessionId = rm1.sessionId AND 
                     (CHAT_Buffer.target = '$sessionId' OR 
                      CHAT_Buffer.target IS NULL OR 
                      CHAT_Buffer.sessionId = '$sessionId') AND 
                     CHAT_Buffer.roomId = '$roomId' AND 
                     CHAT_Buffer.roomId = rm2.roomId AND
                     rm2.sessionId = '$sessionId' AND
                     CHAT_Buffer.date > rm2.lastReadMessage ORDER BY date";

      if($status != SUCCESS)
	 return NULL;
      
      $status_tmp =  $this->dbDriver->GetAll($SQL, $status);

      $count = count($status_tmp);     
         
      $SQL = "UPDATE CHAT_RoomMember SET lastRefresh = NOW() ";      

      if($count > 0)
      {
        $lastDate = $status_tmp[$count - 1]['unixDate'];
	$SQL .= ",lastReadMessage = '$lastDate' ";       
      }
      else
	$SQL .= ",lastReadMessage =  lastReadMessage ";
               
      $SQL .= "WHERE sessionId = '$sessionId' AND roomId = '$roomId'";    
      
      $status = $this->dbDriver->ExecSQL($SQL);

      if($status != SUCCESS)
	 return NULL;

      return $status_tmp;               
  }   
     
}

?>
