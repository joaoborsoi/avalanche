<?php

require_once('UM_UserOper.inc');


class UM_InsTempUser extends UM_UserOper
{
  protected function LoadFieldsDef()
  {
    parent::LoadFieldsDef();

    $i = count($this->fieldsDef);
    $this->fieldsDef[$i]['fieldName'] = 'email';
    $this->fieldsDef[$i]['required'] = true;
    $this->fieldsDef[$i]['allowNull'] = false;

    $i ++;
    $this->fieldsDef[$i]['fieldName'] = 'site';
    $this->fieldsDef[$i]['required'] = true;
    $this->fieldsDef[$i]['allowNull'] = false;

    $i ++;
    $this->fieldsDef[$i]['fieldName'] = 'subject';
    $this->fieldsDef[$i]['required'] = true;
    $this->fieldsDef[$i]['allowNull'] = false;

    $i ++;
    $this->fieldsDef[$i]['fieldName'] = 'mailFrom';
    $this->fieldsDef[$i]['required'] = true;
    $this->fieldsDef[$i]['allowNull'] = false;

    $i ++;
    $this->fieldsDef[$i]['fieldName'] = 'message';
    $this->fieldsDef[$i]['required'] = true;
    $this->fieldsDef[$i]['allowNull'] = false;
  }

  protected function Execute()
  {
    // the user will be initially 
    // disabled until confirmation
    $this->fields['status'] = '0';
    $this->userMan->InsTempUser($this->fields);
  }
}

?>