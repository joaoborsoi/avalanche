<?php

abstract class GAME_Element
{
  protected $page;
  protected $pageBuilder;

  function __construct(AV_Page &$page)
  {
    $this->page = &$page;
    $this->pageBuilder = &$page->pageBuilder;
  }

  abstract function LoadArea(AV_Module &$loadArea);
}

?>