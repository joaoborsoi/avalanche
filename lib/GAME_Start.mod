<?php

require_once('AV_DBOperationObj.inc');
require_once('GAME_Engine.inc');

class GAME_Start extends AV_DBOperationObj
{
  protected function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'restart';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistType'] = 'boolean';
    $this->fieldsDef[0]['default'] = false;
  }

  protected function Execute()
  {
    $game = new GAME_Engine($this);
    $game->Start($this->fields['restart']);
  }
}

?>