<?php

require_once('AV_DBOperationObj.inc');

class LEGIS_GetExpenses extends AV_DBOperationObj
{
  function LoadFieldsDef()
  {
    $this->fieldsDef[0]['fieldName'] = 'docId';
    $this->fieldsDef[0]['required'] = true;
    $this->fieldsDef[0]['allowNull'] = false;
    $this->fieldsDef[0]['consistTyoe'] = 'integer';
  }

  protected function Execute()
  { 
    // verifica existencia de alvará anterior
    $SQL = 'SELECT last.paymentDate FROM LEGIS_Entry this, LEGIS_Entry last';
    $SQL .= " WHERE this.docId='".$this->fields['docId']."'";
    $SQL .= ' AND last.paymentDate IS NOT NULL';
    $SQL .= ' AND this.paymentDate IS NOT NULL';
    $SQL .= ' AND last.paymentDate < this.paymentDate';
    $SQL .= ' AND last.processId=this.processId';
    $SQL .= ' AND last.processId IS NOT NULL';
    $SQL .= ' AND last.classId=1 AND this.classId=last.classId';
    $SQL .= ' ORDER BY last.paymentDate DESC LIMIT 1';
    $SQL .= ' LOCK IN SHARE MODE';
    $last = $this->dbDriver->GetOne($SQL);


    // recuperar despesas
    $SQL = 'SELECT expList.value, expList.classId,expList.paymentDate';
    $SQL .= ' FROM LEGIS_Entry expList, LEGIS_Entry thisCharter';
    $SQL .= " WHERE thisCharter.docId='".$this->fields['docId']."'";
    $SQL .= ' AND thisCharter.paymentDate IS NOT NULL';
    $SQL .= ' AND expList.paymentDate IS NOT NULL';
    $SQL .= ' AND expList.paymentDate<thisCharter.paymentDate';
    $SQL .= ' AND expList.processId=thisCharter.processId';
    $SQL .= ' AND expList.processId IS NOT NULL';
    if($last!=NULL)
      $SQL .= " AND '".$last."' < expList.paymentDate";

    // despesa processual inclusos na prestação de contas
    $SQL .= " AND (expList.classId=4 AND expList.accountability='1'";
    // lançamentos de obrigação a fazer com desconto do alvará
    $SQL .= " OR expList.classId=3 AND expList.discountOfCharter='1'";
    // sucumbencias
    $SQL .= ' OR expList.classId=2)';

    $expList = $this->dbDriver->GetAll($SQL);

    // efetua correção monetária e calculo do total
    $total['expenses'] = 0;
    $total['succumbing'] = 0;
    foreach($expList as $expense)
    {
      if($expense['classId']==2)
      {
	// sucumbencia
	$total['succumbing'] += floatval($expense['value']);
      }
      else
      {
	$this->DoIndexation($expense);

	// despesas
	$total['expenses'] += $expense['value'];
      }
    }

    $this->attrs = $total;
  } 

  function DoIndexation(& $expense)
  {
    $expense['value'] = floatval($expense['value']);

    $paymentDate = strtotime($expense['paymentDate']);
    $paymentDate = strtotime("first day of this month",$paymentDate);
    $paymentDate = date('Y-m-d',$paymentDate);
    $lastMonth = strtotime("first day of previous month");
    $lastMonth = date('Y-m-d',$lastMonth);

    $SQL = 'SELECT EXP(SUM(LOG(1+value/100+0.01))) as value ';
    $SQL .= 'FROM LEGIS_Indicator ';
    $SQL .= "WHERE refDate>='$paymentDate' AND refDate<='$lastMonth' ";
    $value = $this->dbDriver->GetOne($SQL);
    if($value!==NULL)
      $expense['value'] = floatval($value)*$expense['value'];
  }
}

?>