<?php

require_once('LIB_DocumentOper.inc');

class LIB_InsDoc extends LIB_DocumentOper
{
  protected function LoadFieldsDef()
  {
    parent::LoadFieldsDef();

    $i=count($this->fieldsDef);
    $this->fieldsDef[$i]['fieldName'] = 'beforeDocId';
    $this->fieldsDef[$i]['required'] = false;
    $this->fieldsDef[$i]['allowNull'] = true;
    $this->fieldsDef[$i]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    $docId = $this->doc->Insert($this->fields);
    $this->attrs['docId'] = $docId;
  }
}

?>