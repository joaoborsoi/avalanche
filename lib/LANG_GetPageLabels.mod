<?php 

require_once("AV_DBOperationObj.inc");
require_once("LANG_Label.inc");

class LANG_GetPageLabels extends AV_DBOperationObj
{
  
  function Execute()
  {
    $aux = new LANG_Label ($this);
    $res = $aux->GetPageLabels($this->pageBuilder->pageId,
			       $this->pageBuilder->lang);
    foreach($res as $value)
      $this->attrs[$value['labelId']] = $value['value'];

    $this->attrs['lang'] = $this->pageBuilder->lang;
  }
}

?>