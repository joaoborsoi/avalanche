<?php

require_once('LEGIS_AdvancedSearchProcess.inc');
require_once('L2PDF_Document.inc');
require_once('LIB_Document.inc');

class LEGIS_Report extends LEGIS_AdvancedSearchProcess
{
  function GetGroupingReport(& $params,$grouping)
  {
    if($grouping == NULL)
      throw new AV_Exception(INVALID_FIELD,$this->lang);

    $this->ProcessSearchParams($params, $tables, $tablesWhere, $where);

    switch($grouping)
    {
    case 'justice':
      $this->AddGroupByJustice($tables, $tablesWhere, $groupBy, $title);
      break;

    case 'actionType':
      $this->AddGroupByActionType($tables, $tablesWhere, $groupBy, $title);
      break;
      
    case 'local':
      $this->AddGroupByLocal($tables, $tablesWhere, $groupBy, $title);
      break;
      
    case 'phases':
      $this->AddGroupByPhases($tables, $tablesWhere, $groupBy, $title);
      break;
      
    case 'subject':
      $this->AddGroupBySubject($tables, $tablesWhere, $groupBy, $title);
      break;
      
    case 'situation':
      $this->AddGroupBySituation($tables, $tablesWhere, $groupBy, $title);
      break;
      
    case 'result':
      $this->AddGroupByResult($tables, $tablesWhere, $groupBy, $title);
      break;

    default:
      throw new AV_Exception(INVALID_FIELD,$this->lang);
    }

    $WHERE = $this->BuildFromAndWhere($tables, $tablesWhere, $where);

    $SQL = 'SET @total := (SELECT count(*) ' . $WHERE . ')';
    $this->dbDriver->ExecSQL($SQL);

    $SQL = "SELECT $title, count(*) as value, ";
    $SQL .= 'count(*)/@total as percent ';
    $SQL .= $WHERE;
    $SQL .= " GROUP BY $groupBy ORDER BY count(*) DESC ";
    return $this->dbDriver->GetAll($SQL);
  }

  function DoIndividual($params)
  {
    if(count($params) == 0)
      throw new AV_Exception(INVALID_FIELD,$this->lang);

    $this->ProcessSearchParams($params, $tables, $tablesWhere, $where);

    // nro do processo - title
    // situação - descr
    // assunto - subject

    // nome do cabeça,
    // quantidade de autores (ex: + 10 fulanos),

    // resultado
    //$returnFields = 'doc.docId, doc.title, doc.authors, doc.descr, subjectId, ';
    $returnFields = 'p.processId,p.rootProcessId,';
    $returnFields .= "GROUP_CONCAT(CONCAT('\\\\mbox{',pn.number,'}') SEPARATOR ', ') ";
    $returnFields .= 'as processNumbers,situationId,subjectId,resultId ';

    /*$returnFields .= "(SELECT s.name FROM LEGIS_Situation s ";
    $returnFields .= "WHERE s.docId=p.situationId) AS situation, ";
    $returnFields .= "(SELECT s.name FROM LEGIS_Subject s ";
    $returnFields .= "WHERE s.docId=p.subjectId) AS subject ";*/

    $tables[] = 'LEGIS_ProcessNumber pn';
    $tablesWhere[] = 'doc.docId = pn.processId';
    $SQL = $this->Search($returnFields,
			 $limit, $offset, $tables, $tablesWhere, $where,
			 $totalResults);


    $tableName = uniqid('LEGIS_AdvSearch_',false);
    $SQL = "CREATE TEMPORARY TABLE $tableName $SQL GROUP BY docId LIMIT 600 ";
    $this->dbDriver->ExecSQL($SQL);

    try
    {
      // cria campos adicionais
      $SQL = "ALTER TABLE $tableName ADD subject VARCHAR(255)";
      $SQL .= ',ADD result VARCHAR(255), ADD header VARCHAR(255)';
      $SQL .= ',ADD otherClients INTEGER, ADD situation VARCHAR(255)';
      $this->dbDriver->ExecSQL($SQL);


      // atualiza tabela e ajusta assunto quando estiver preenchido
      $SQL = "UPDATE $tableName t, LEGIS_Subject s ";
      $SQL .= 'SET subject=s.name ';
      $SQL .= 'WHERE t.subjectId=s.docId ';
      $this->dbDriver->ExecSQL($SQL);

      // atualiza tabela e ajusta situação quando estiver preenchido
      $SQL = "UPDATE $tableName t, LEGIS_Situation s ";
      $SQL .= 'SET situation=s.name ';
      $SQL .= 'WHERE t.situationId=s.docId ';
      $this->dbDriver->ExecSQL($SQL);

      // atualiza tabela e ajusta resultado quando estiver preenchido
      $SQL = "UPDATE $tableName t, LEGIS_Result r ";
      $SQL .= 'SET result=r.name ';
      $SQL .= 'WHERE t.resultId=r.docId ';
      $this->dbDriver->ExecSQL($SQL);

      // atualiza informações dos clientes
      $SQL = "UPDATE $tableName t ";
      $SQL .= 'SET header=(SELECT GROUP_CONCAT(name) as name ';
      $SQL .= 'FROM LEGIS_ProcessClient pc, ';
      $SQL .= '((SELECT name,docId FROM EM_NaturalPerson np) UNION ';
      $SQL .= '(SELECT name,docId FROM EM_LegalEntity le)) cli ';
      $SQL .= 'WHERE pc.processId=t.processId and pc.entityId=cli.docId ';
      $SQL .= "AND header='1'),";
      $SQL .= 'otherClients=(SELECT COUNT(*) ';
      $SQL .= 'FROM LEGIS_ProcessClient pc ';
      $SQL .= 'WHERE pc.processId=t.processId ';
      $SQL .= "AND header='0')";
      $this->dbDriver->ExecSQL($SQL);

      $SQL = "SELECT * FROM $tableName ";
      $rows = $this->dbDriver->GetAll($SQL);
      $this->dbDriver->ExecSQL("DROP TABLE $tableName");
    }
    catch(Exception $e)
    {
      $this->dbDriver->ExecSQL("DROP TABLE $tableName");
      throw $e;
    }


    // cria documento pdf para relatorio
    $report =  new L2PDF_Document($this->pageBuilder);

    $this->InitIndividualReport($report);

    foreach($rows as & $process)
    {
      // numeros do processo
      $report->Write($process['processNumbers'].'&',false);

      // cabeça + num clientes
      if($process['header'] == NULL)
	$report->Write('Sem cabeça',false);
      else
	$report->Write($report->Escape($process['header']),false);
      if($process['otherClients'] != NULL)
	$report->Write(' + '.$process['otherClients'],false);
      $report->Write('&',false);

      // assunto
      $report->Write($report->Escape($process['subject']).'&',false);

      // resultado
      $report->Write($report->Escape($process['result']).'&',false);

      // situação
      $report->Write($report->Escape($process['situation']).'\\\\');
      $report->Write('\hline');
    }

    $this->EndIndividualReport($report);
  
    $tmpDir = $this->module->pageBuilder->siteConfig->getVar("dirs", "tmp");
    $fileInfo = $report->CreateDoc($tmpDir,true,true);
    try
    {
      $doc = new LIB_Document($this->module);

      $fields['path'] = '/files/';
      $fields['title'][0]['lang'] = 'pt_BR';
      $fields['title'][0]['value'] = 'Relatório individual';
      $fields['content']['error'] = UPLOAD_ERR_OK;
      $fields['content']['tmp_name'] = $fileInfo['filePath'].'.pdf';
      $fields['content']['name'] = 'relatorio_individual.pdf';
      $fields['content']['type'] = 'application/pdf';
      $fields['content']['size'] = filesize($fileInfo['filePath'].'.pdf');
      $fields['groupRight'] = '';
      $fields['otherRight'] = '';
      $doc->LoadFieldsDef(NULL,$fields['path']);
      $docId = $doc->Insert($fields);
      $report->UnlinkGarbage($fileInfo['fileName']);
      return $docId;
    }
    catch(Exception $e)
    {
      $report->UnlinkGarbage($fileInfo['fileName']);
      throw $e;
    }
  }

  protected function AddGroupByJustice(& $tables, & $tablesWhere, &$groupBy, &$title)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'doc.docId = p.processId';
    }
    if(!in_array('LEGIS_RootProcess rp', $tables))
    {
      $tables[] = 'LEGIS_RootProcess rp';
      $tablesWhere[] = 'p.rootProcessId = rp.processId';
    }

    $tables[] = 'LEGIS_Justice j';
    $tablesWhere[] = 'rp.justiceId=j.docId';
    $groupBy = 'rp.justiceId';
    $title = 'j.name as title';
  }

  protected function AddGroupByActionType(& $tables, & $tablesWhere, &$groupBy, &$title)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'doc.docId = p.processId';
    }

    $tables[] = 'LEGIS_ActionType at';
    $tablesWhere[] = 'p.actionTypeId=at.docId';
    $groupBy = 'p.actionTypeId';
    $title = 'at.name as title';
  }

  protected function AddGroupByLocal(& $tables, & $tablesWhere, &$groupBy, &$title)
  {
    $tables[] = 'LEGIS_ProcessLocal pl';
    $tablesWhere[] = 'doc.docId = pl.processId';

    $tables[] = 'LEGIS_Local lc';
    $tablesWhere[] = 'pl.localId=lc.docId';
    $groupBy = 'pl.localId';
    $title = 'lc.name as title';
  }

  protected function AddGroupByPhases(& $tables, & $tablesWhere, &$groupBy, &$title)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'doc.docId = p.processId';
    }

    $tables[] = 'LEGIS_Phases ph';
    $tablesWhere[] = 'p.phasesId=ph.docId';
    $groupBy = 'p.phasesId';
    $title = 'ph.name as title';
  }

  protected function AddGroupBySubject(& $tables, & $tablesWhere, &$groupBy, &$title)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'doc.docId = p.processId';
    }

    $tables[] = 'LEGIS_Subject sbj';
    $tablesWhere[] = 'p.subjectId=sbj.docId';
    $groupBy = 'p.subjectId';
    $title = 'sbj.name as title';
  }

  protected function AddGroupBySituation(& $tables, & $tablesWhere, &$groupBy, &$title)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'doc.docId = p.processId';
    }

    $tables[] = 'LEGIS_Situation sit';
    $tablesWhere[] = 'p.situationId=sit.docId';
    $groupBy = 'p.situationId';
    $title = 'sit.name as title';
  }

  protected function AddGroupByResult(& $tables, & $tablesWhere, &$groupBy, &$title)
  {
    if(!in_array('LEGIS_Process p', $tables))
    {
      $tables[] = 'LEGIS_Process p';
      $tablesWhere[] = 'doc.docId = p.processId';
    }

    $tables[] = 'LEGIS_Result rslt';
    $tablesWhere[] = 'p.resultId=rslt.docId';
    $groupBy = 'p.resultId';
    $title = 'rslt.name as title';
  }

  protected function InitIndividualReport(& $report)
  {
    $report->Write('\documentclass[a4paper]{article}');
    $report->Write('\usepackage{fancyhdr}');
    $report->Write('\usepackage[psamsfonts]{amsfonts}');
    $report->Write('\usepackage[leqno]{amsmath}');
    $report->Write('\usepackage[dvips]{graphicx}');
    $report->Write('\usepackage[portuges]{babel}');
    $report->Write('\usepackage[utf8]{inputenc}');
    $report->Write('\usepackage[T1]{fontenc}');
    $report->Write('\usepackage{ae,aecompl}');
    $report->Write('\usepackage{ltablex}');
    $report->Write('\usepackage[usenames]{color}');
    $report->Write('\usepackage[dvipsnames]{xcolor}');
    $report->Write('\usepackage{colortbl}');
    $report->Write('\usepackage[landscape]{geometry}');
//    $report->Write('\usepackage{lscape}');
    $report->Write('\usepackage{pdfpages}');
    $report->Write('\definecolor{camara}{rgb}{0.094,0.145,0.455}');
    $report->Write('\setcounter{LTchunksize}{50}');
    $report->Write('\renewcommand{\familydefault}{\sfdefault}');
    $report->Write('\renewcommand{\doublerulesep}{1pt}');
    $report->Write('\addtolength{\headheight}{0.5cm}');
    $report->Write('\addtolength{\hoffset}{-3.5cm}');
    $report->Write('\addtolength{\textwidth}{7.5cm}');
    $report->Write('\addtolength{\voffset}{-1cm}');
    $report->Write('\addtolength{\textheight}{3cm}');
    $report->Write('\pagestyle{fancy}');
    $report->Write('\lhead{{\huge\color{camara}Relatório de Processos}}');
    $report->Write('\chead{}');
    $report->Write('\rhead{\includegraphics[scale=0.7]{logo}}');
    $report->Write('\newcommand{\topHeadRule}');
    $report->Write('{{\color{camara}%');
    $report->Write('\hrule height 2pt');
    $report->Write('width\headwidth');
    $report->Write('\vspace{5pt}}');
    $report->Write('}');
    $report->Write('\renewcommand\headrule{');
    $report->Write('\topHeadRule}');
    $report->Write('\renewcommand\footrule');
    $report->Write('{{\color{camara}%');
    $report->Write('\hrule height 2pt');
    $report->Write('width\headwidth}}');
    $report->Write('\lfoot{'.strftime('%c').'}');
    $report->Write('\cfoot{}');
    $report->Write('\rfoot{\thepage}');
    $report->Write('\fancypagestyle{plain}{');
    $report->Write('  \fancyhf{}');
    $report->Write('  \rhead{\includegraphics{logo}}');
    $report->Write('  \lfoot{}');
    $report->Write('  \rfoot{\thepage}}');
    $report->Write('\fancypagestyle{title}{');
    $report->Write('  \fancyhf{}');
    $report->Write('  \rhead{\includegraphics{logo}}');
    $report->Write('  \lfoot{}');
    $report->Write('  \rfoot{}}');
    $report->Write('\begin{document}');
    //    $report->Write('\begin{landscape}');
    $report->Write('\newlength{\topTableCellWidth}');
    $report->Write('\setlength{\topTableCellWidth}{0.25\textwidth}');
    $report->Write('\addtolength{\topTableCellWidth}{-1.7\tabcolsep}');
    $report->Write('\setlength\LTleft{0pt}');
    $report->Write('\setlength\LTright{0pt}');
    $report->Write('\begin{tabularx}{\linewidth}{@{} X X X X X @{}}');
    $report->Write('\textbf{Processo}&\textbf{Autores}&\textbf{Assunto}&\textbf{Resultado}&\textbf{Situação}\\\\');
    $report->Write('\arrayrulecolor{camara}');
    $report->Write('\hline\hline\hline\hline');
    $report->Write('\arrayrulecolor{black}');
    $report->Write('\endhead');
  }

  protected function EndIndividualReport(& $report)
  {
    $report->Write('\end{tabularx}');
    //    $report->Write('\end{landscape}');
    $report->Write('\end{document}');
  }

}

?>