<?php

require_once('AV_DBOperationObj.inc');

class GAME_LoadQuizList extends AV_DBOperationObj
{
  protected function Execute()
  {
    $this->isArray = true;
    $SQL = 'SELECT *, el.value as title FROM GAME_Element e, LANG_Label el, ';
    $SQL .= 'GAME_ElementType et, GAME_Quiz q, GAME_QuizAnswer qa, ';
    $SQL .= 'GAME_PlayUser pu, UM_UserSessions us ';
    $SQL .= "WHERE sessionId='".$this->pageBuilder->sessionH->sessionID."' ";
    $SQL .= "AND us.userId = pu.userId AND currGame = '1' ";
    $SQL .= 'AND playId = pu.id AND q.id=quizId ';
    $SQL .= 'AND q.id = e.id AND typeId=et.id ';
    $SQL .= 'AND e.title=el.labelId ';
    $SQL .= "AND el.lang='".$this->pageBuilder->lang."' ";
    $quizList = $this->dbDriver->GetAll($SQL);
    $this->LoadChildrenFromRows($quizList,'quizItem');
  }
}