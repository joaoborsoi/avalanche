<?php

require_once('AV_DBOperationObj.inc');
require_once('LEGIS_Process.inc');

class LEGIS_GetAggregatorHistory extends AV_DBOperationObj
{
  function Execute()
  { 
    $this->isArray = true;
    $sumary = new LEGIS_Process($this);
    $result = $sumary->GetAggregatorHistory();
    $this->LoadChildrenFromRows($result, 'name');
  } 

}

?>
