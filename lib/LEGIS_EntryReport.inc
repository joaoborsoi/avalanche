<?php

require_once('L2PDF_Document.inc');
require_once('LIB_Document.inc');

class LEGIS_EntryReport
{
  protected $dbDriver;
  protected $lang;
  protected $module;

  function __construct(&$module)
  {
    $this->module = &$module;
    $this->dbDriver = & $module->dbDriver;
    $this->lang = $module->pageBuilder->lang;
  }

  function Get($fromDate, $toDate)
  {
    $locale = localeconv();

    $data = $this->GetData($fromDate, $toDate);
    if(count($data)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    // cria documento pdf para relatorio
    $report =  new L2PDF_Document($this->pageBuilder);
    
    $this->InitReport($report);

    $lastType = 0;
    $totalExpenses = 0;
    $totalIncoming = 0;
    $totalCustomerPayment = 0;
    foreach($data as $entry)
    {
      if($lastType!=$entry['type'])
      {
	if($lastType>0)
	  $report->Write('\end{tabularx}');

	switch($entry['type'])
	{
	case 1:
	  $report->Write('\section{Receitas}');
	  break;
	case 2:
	  $report->Write('\section{Pagamento de Cliente}');
	  break;
	case 3:
	  $report->Write('\section{Despesas}');
	  break;
	}

	$report->Write('\begin{tabularx}{\linewidth}{@{} l l l X r @{}}');
	$report->Write('\textbf{ID}&\textbf{Pagamento}&\textbf{Classe}&\textbf{Histórico}&\textbf{Valor}\\\\');
	$report->Write('\arrayrulecolor{camara}');
	$report->Write('\hline\hline\hline\hline');
	$report->Write('\arrayrulecolor{black}');
	$report->Write('\endhead');

	$lastType = $entry['type'];
      }

      //ID
      $report->Write($entry['entryId'].'/'.$entry['entryYear'].'&',false);

      // pagamento
      $timestamp = strtotime($entry['paymentDate']);
      $report->Write($report->Escape(strftime('%d/%m/%Y',$timestamp)).'&',false);

      // classe
      $report->Write($report->Escape($entry['classIdText']).'&',false);

      // historico
      $report->Write($report->Escape($entry['history']).'&',false);

      // valor
      $report->Write(number_format($entry['value'],
				   $locale['frac_digits'],
				   $locale['mon_decimal_point'],
				   $locale['mon_thousands_sep']).'\\\\');
      

      switch($entry['type'])
      {
      case 1:
	$totalIncoming += $entry['value'];
	break;
      case 2:
	$totalCustomerPayment += $entry['value'];
	break;
      case 3:
	$totalExpenses += $entry['value'];
	break;
      }
    }
    $report->Write('\end{tabularx}');


    $report->Write('\section{Totais}');
    $report->Write('\begin{tabularx}{\linewidth}{@{} X r @{}}');
    $report->Write('\textbf{Tipo de entrada}&\textbf{Valor}\\\\');
    $report->Write('\arrayrulecolor{camara}');
    $report->Write('\hline\hline\hline\hline');
    $report->Write('\arrayrulecolor{black}');
    $report->Write('\endhead');

    $report->Write('Receitas&',false);
    $report->Write(number_format($totalIncoming,
				 $locale['frac_digits'],
				 $locale['mon_decimal_point'],
				 $locale['mon_thousands_sep']).'\\\\');

    $report->Write('Pagamento de Cliente&',false);
    $report->Write(number_format($totalCustomerPayment,
				 $locale['frac_digits'],
				 $locale['mon_decimal_point'],
				 $locale['mon_thousands_sep']).'\\\\');

    $report->Write('Despesas&',false);
    $report->Write(number_format($totalExpenses,
				 $locale['frac_digits'],
				 $locale['mon_decimal_point'],
				 $locale['mon_thousands_sep']).'\\\\');
    
    $report->Write('\end{tabularx}');

    $this->EndReport($report);
  
    $tmpDir = $this->module->pageBuilder->siteConfig->getVar("dirs", "tmp");
    $fileInfo = $report->CreateDoc($tmpDir,true,true);
    try
    {
      $doc = new LIB_Document($this->module);

      $fields['path'] = '/files/';
      $fields['title'][0]['lang'] = 'pt_BR';
      $fields['title'][0]['value'] = 'Relatório de lançamentos';
      $fields['content']['error'] = UPLOAD_ERR_OK;
      $fields['content']['tmp_name'] = $fileInfo['filePath'].'.pdf';
      $fields['content']['name'] = 'relatorio_lancamentos.pdf';
      $fields['content']['type'] = 'application/pdf';
      $fields['content']['size'] = filesize($fileInfo['filePath'].'.pdf');
      $fields['groupRight'] = '';
      $fields['otherRight'] = '';
      $doc->LoadFieldsDef(NULL,$fields['path']);
      $docId = $doc->Insert($fields);
      $report->UnlinkGarbage($fileInfo['fileName']);
      return $docId;
    }
    catch(Exception $e)
    {
      $report->UnlinkGarbage($fileInfo['fileName']);
      throw $e;
    }

  }

  protected function GetData($fromDate, $toDate)
  {
    $SQL = 'SELECT entryId, entryYear, expiration, paymentDate, ';
    $SQL .= 'history, e.typeId, e.classId, e.value, ';
    $SQL .= 'ec.value as classIdText, ';

    // receitas (1)
    $SQL .= 'CASE WHEN e.classId=1 OR e.classId=2 ';
    $SQL .= "OR e.classId=3 AND discountOfCharter='0' ";
    $SQL .= "OR e.classId=4 AND accountability='0' THEN 1 ";

    // pagamento de cliente (2)
    $SQL .= 'WHEN e.classId=6 THEN 2 ';

    // despesas (3)
    $SQL .= 'WHEN e.classId=5 THEN 3 ';

    // outros (0)
    $SQL .= 'ELSE 0 END as type ';

    $SQL .= 'FROM LEGIS_Entry e, LEGIS_EntryClass ec ';

    $SQL .= 'WHERE e.classId=ec.classId ';
    $SQL .= 'AND paymentDate IS NOT NULL ';
    $SQL .= "AND paymentDate >= '$fromDate' ";
    $SQL .= "AND paymentDate <= '$toDate' ";
    $SQL .= 'AND (';

    // receitas
    $SQL .= '(e.classId=1 OR e.classId=2 ';
    $SQL .= "OR e.classId=3 AND discountOfCharter='0' ";
    $SQL .= "OR e.classId=4 AND accountability='0') ";
    // pagamento de cliente
    $SQL .= 'OR e.classId=6 ';
    // despesas
    $SQL .= 'OR e.classId=5) ';


    $SQL .= 'ORDER BY type, paymentDate ';
    
    return $this->dbDriver->GetAll($SQL);
  }

  protected function InitReport(& $report)
  {
    $report->Write('\documentclass[a4paper]{article}');
    $report->Write('\usepackage{fancyhdr}');
    $report->Write('\usepackage[psamsfonts]{amsfonts}');
    $report->Write('\usepackage[leqno]{amsmath}');
    $report->Write('\usepackage[dvips]{graphicx}');
    $report->Write('\usepackage[portuges]{babel}');
    $report->Write('\usepackage[utf8]{inputenc}');
    $report->Write('\usepackage[T1]{fontenc}');
    $report->Write('\usepackage{ae,aecompl}');
    $report->Write('\usepackage{ltablex}');
    $report->Write('\usepackage[usenames]{color}');
    $report->Write('\usepackage[dvipsnames]{xcolor}');
    $report->Write('\usepackage{colortbl}');
    $report->Write('\usepackage{pdfpages}');
    $report->Write('\definecolor{camara}{rgb}{0.094,0.145,0.455}');
    $report->Write('\setcounter{LTchunksize}{50}');
    $report->Write('\renewcommand{\familydefault}{\sfdefault}');
    $report->Write('\renewcommand{\doublerulesep}{1pt}');
    $report->Write('\addtolength{\headheight}{0.5cm}');
    $report->Write('\addtolength{\hoffset}{-3.5cm}');
    $report->Write('\addtolength{\textwidth}{7.5cm}');
    $report->Write('\addtolength{\voffset}{-1cm}');
    $report->Write('\addtolength{\textheight}{3cm}');
    $report->Write('\pagestyle{fancy}');
    $report->Write('\lhead{{\huge\color{camara}Relatório de Processos}}');
    $report->Write('\chead{}');
    $report->Write('\rhead{\includegraphics[scale=0.7]{logo}}');
    $report->Write('\newcommand{\topHeadRule}');
    $report->Write('{{\color{camara}%');
    $report->Write('\hrule height 2pt');
    $report->Write('width\headwidth');
    $report->Write('\vspace{5pt}}');
    $report->Write('}');
    $report->Write('\renewcommand\headrule{');
    $report->Write('\topHeadRule}');
    $report->Write('\renewcommand\footrule');
    $report->Write('{{\color{camara}%');
    $report->Write('\hrule height 2pt');
    $report->Write('width\headwidth}}');
    $report->Write('\lfoot{'.strftime('%c').'}');
    $report->Write('\cfoot{}');
    $report->Write('\rfoot{\thepage}');
    $report->Write('\fancypagestyle{plain}{');
    $report->Write('  \fancyhf{}');
    $report->Write('  \rhead{\includegraphics{logo}}');
    $report->Write('  \lfoot{}');
    $report->Write('  \rfoot{\thepage}}');
    $report->Write('\fancypagestyle{title}{');
    $report->Write('  \fancyhf{}');
    $report->Write('  \rhead{\includegraphics{logo}}');
    $report->Write('  \lfoot{}');
    $report->Write('  \rfoot{}}');
    $report->Write('\begin{document}');
    $report->Write('\newlength{\topTableCellWidth}');
    $report->Write('\setlength{\topTableCellWidth}{0.25\textwidth}');
    $report->Write('\addtolength{\topTableCellWidth}{-1.7\tabcolsep}');
    $report->Write('\setlength\LTleft{0pt}');
    $report->Write('\setlength\LTright{0pt}');
  }

  protected function EndReport(& $report)
  {
    $report->Write('\end{document}');
  }
}
?>