<?php

require_once('FORM_DynTable.inc');
require_once('LEGIS_ContractDoc.inc');

class LEGIS_User extends FORM_DynTable
{
  function Insert(& $fields, $extraFields = array())
  {
    parent::Insert($fields,$extraFields);

    if($fields['userRef']['login'] == NULL)
      throw new AV_Exception(FIELD_REQUIRED,$this->lang,
			     array('fieldName'=>'login',
				   'addMsg'=>' (login)'));

    if($fields['userRef']['password']==NULL)
      throw new AV_Exception(FIELD_REQUIRED,$this->lang,
			     array('fieldName'=>'password',
				   'addMsg'=>' (senha)'));

    if($fields['userRef']['groups']==NULL ||
       count($fields['userRef']['groups'])==0)
      throw new AV_Exception(FIELD_REQUIRED,$this->lang,
			     array('fieldName'=>'groups',
				   'addMsg'=>' (Permissões)'));

    // não podem ser ajustados os grupos
    // 1 - Administradores
    // 5 - Clientes
    // 24 - Agregadores
    if(in_array(1,$fields['userRef']['groups']) |
       in_array(5,$fields['userRef']['groups']) ||
       in_array(24,$fields['userRef']['groups']))
      throw new AV_Exception(INVALID_FIELD,$this->lang,
			     array('fieldName'=>'groups',
				   'addMsg'=>' (Permissões)'));
			     

    $entityId = $this->dbDriver->GetOne('SELECT LAST_INSERT_ID()');
    $fields['userRef']['entityId'] = $entityId;

    $user = new UM_User($this->module);
    $user->LoadFieldsDef();
    $fields['userRef']['path'] = '/user/';
    $user->Insert($fields['userRef']);
  }

  function Set($fields, $extraFields = array())
  {
    if($fields['userRef']['login'] != NULL)
    {
      $user = new UM_User($this->module);

      if($fields['userRef']['userId']==NULL)
      {
	$fields['userRef']['path'] = '/user/';
	$fields['userRef']['entityId'] = $fields['docId'];
	$fields['userRef']['groups'][] = array('value'=>5);
	$user->LoadFieldsDef();
	$user->Insert($fields['userRef']);
      }
      else
      {
	$user->LoadFieldsDef(true,true);
	if($fields['userRef']['password']==='')
	  unset($fields['userRef']['password']);
	$fields['userRef']['path'] = '/user/';
	$user->Set($fields['userRef']);
      }
    }
    
    return parent::Set($fields,$extraFields);
  }

  protected function ParseFieldDef(& $fieldDef,$disableFileRequired,$disableRequired)
  {
    parent::ParseFieldDef($fieldDef,$disableFileRequired,$disableRequired);

    switch($fieldDef['type'])
    {
    case 'userRef':
      // gambiarra pra evitar addslashes      
      $fieldDef['stripSlashes'] = true; 
      break;
    }
  }

  protected function &GetField(& $fieldDef, & $values, $keyFields,  
			       & $templates, $sourceId, $dateFormat = NULL,
			       $numberFormat = true, $fieldName = NULL)
  {
    switch ($fieldDef['type'])
    {
    case 'userRef':
      return $this->GetUserRef($fieldDef,$values,$keyFields);
    case 'contractList':
      return $this->GetContractList($fieldDef,$keyFields,$dateFormat,
				    $numberFormat);
    default:
      return parent::GetField($fieldDef,$values,$keyFields,$templates,
			      $sourceId,$dateFormat,$numberFormat);
    }
  }

  protected function GetUserRef($fieldDef,&$values,$keyFields)
  {
    $field =  new AV_ModObject('userRef',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;
    $field->isArray = true;

    if($keyFields!=NULL)
    {
      $entityId = $keyFields['docId'];

      $SQL = 'SELECT userId FROM UM_User ';
      $SQL .= "WHERE entityId='$entityId' ";
      $userKey = $this->dbDriver->GetRow($SQL);
    }
    else
      $userKey = NULL;

    $user = new UM_User($this->module);
    $user->LoadFieldsDef();
    $formObject = new FORM_Object('form', $this->pageBuilder, $templates);
    $user->GetForm($userKey,NULL,NULL,$templates,$formObject);

    if($userKey==NULL)
    {
      foreach($formObject->children as &$child)
      {
	if($child->attrs['fieldName']=='password')
	{
	  $child->attrs['required'] = 'required';
	  break;
	}
      }
    }

    $field->attrs['value'] = $formObject->attrs;
    $field->children = $formObject->children;

    return $field;
  }


  protected function &GetContractList($fieldDef,$keyFields,$dateFormat,$numberFormat)
  {
    if($keyFields==NULL)
      return NULL;

    // cria objeto para o campo de contratos
    $field =  new AV_ModObject('contractList',$this->module->pageBuilder);
    $field->attrs = & $fieldDef;

    $doc = new LEGIS_ContractDoc($this->module);
    $search['path'] = '/contract/';
    $search['orderBy'] = 'expiration';
    $search['order'] = '0';
    $search['dateFormat'] = $dateFormat;
    $search['numberFormat'] = $numberFormat;
    $search['tempTable'] = '0';
    $search['entityId'] = $keyFields['docId'];
    $field->attrs['items'] = $doc->Search($search);
    
    return $field;
  }
}

?>