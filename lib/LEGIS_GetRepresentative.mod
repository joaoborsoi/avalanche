<?php

require_once('LIB_GetDoc.mod');


class LEGIS_GetRepresentative extends LIB_GetDoc
{
  protected function LoadFieldsDef()
  {
    parent::LoadFieldsDef();

    $i = count($this->fieldsDef);

    $this->fieldsDef[$i]['fieldName'] = 'customer';
    $this->fieldsDef[$i]['required'] = false;
    $this->fieldsDef[$i]['allowNull'] = true;
    $this->fieldsDef[$i]['consistType'] = 'integer';
  }

  protected function Execute()
  {
    if($this->fields['docId']==NULL && $this->fields['customer']==NULL)
      throw new AV_Exception(INVALID_FIELD,$this->lang);
    $defValues['customer'] = $this->fields['customer'];

    $doc = & $this->CreateDocument();
    $formObject = new FORM_Object('form', $this->pageBuilder, $templates); 

    if($this->fields['docId'] == NULL)
      $keyFields = NULL;
    else
      $keyFields = Array('docId' => $this->fields['docId']);

    $doc->GetForm($keyFields,$this->fields['path'], $this->fields['fieldName'],
		  $this->fields['sourceId'], $templates, 
		  $formObject,$defValues,$this->fields['dateFormat'],
		  $this->fields['numberFormat'], $this->fields['short']);

    $this->children[] = & $formObject;
  }
}

?>