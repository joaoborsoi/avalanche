<?php

require_once('ADMIN_Files.inc');

class ADMIN_PasteFolder extends ADMIN_Files
{

  function LoadFiles($path)
  {
    try
    {
      if($_REQUEST['cutNode'] == '1')
      {
	$_GET['path'] = $this->rootPath . $path;
	$moveFolder = $this->LoadModule('LIB_MoveFolder');
	$script = 'parent.OnPaste();';
	$script .= 'parent.ReloadFolders();';
      }
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }    

    $script .= parent::LoadFiles($path);
    return $script;
  }
}

?>