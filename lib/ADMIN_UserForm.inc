<?php

require_once('ADMIN_Popup.inc');

class ADMIN_UserForm extends ADMIN_Popup
{
  protected function LoadPopup()
  {
    $_GET['labelId'] = 'adminUserTag';
    $_GET['lang'] = $this->lang;
    $windowTitleLabel = &$this->LoadModule('LANG_GetLabel');
    $windowTitle = htmlentities_utf($windowTitleLabel->attrs['value']);
    $this->pageBuilder->rootTemplate->addText($windowTitle, 'windowTitle');

    $formTpl = $this->pageBuilder->loadTemplate('adminUserForm');
    $this->pageBuilder->rootTemplate->addTemplate($formTpl, 'content');

    $formTpl->addText($windowTitle, 'adminUserTag');

    $getUser = $this->LoadModule('ADMIN_GetUserForm');

    $getUser->PrintHTML($formTpl);
    $this->script .= "InitiateUserContent('".$this->lang."');";
  }
}

?>