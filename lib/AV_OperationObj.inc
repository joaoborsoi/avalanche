<?php

require_once("AV_Module.inc");
require_once("AV_FieldParser.inc");

//--Class: AV_OperationObj
//--Parent: AV_ModObject
//--Desc: Represents module's object which executes operations when loading.
//--Desc: Implements input variables parsing.
abstract class AV_OperationObj extends AV_Module
{
  //--Attributes
  protected $fieldParser;         //--Desc: Field parser handler
  protected $fieldsDef = array(); //--Desc: Object's input fields definition
  protected $fields;

  //--Method: Load
  //--Desc: Loads object, which means to parse input fields (variables)
  //--Desc: and then execute module
  protected function Load(&$inputSources)
  {
    $this->ParseInput($inputSources);
    $this->Execute();
  }

  //--Private
  //

  //--Method: CreateParser
  //--Desc: Creates field parser object (AV_FieldParser), which is responsible
  //--Desc: for parsing object's input fields. AV_FieldParse parses basic
  //--Desc: types such as integer, boolean, e-mail, etc. To extend the parser
  //--Desc: capabilities, this method should be reimplemented by subclasses
  //--Desc: in order to create AV_FieldParser extensions instead of the default
  //--Desc: object.
  protected function CreateParser(&$inputSources)
  {
    $this->fieldParser = new AV_FieldParser($this,
					    $inputSources);
  }

  //--Method: ParseInput
  //--Desc: Parses input variables
  protected function ParseInput(&$inputSources)
  {
    // Parses input
    $this->CreateParser($inputSources);
    $this->LoadFieldsDef();

    if(is_array($this->fieldParser))
    {
      foreach($this->fieldParser as $key => $parser)
      {
	$this->fieldParser[$key]->Parse($this->fieldsDef);
	$this->fields = array_merge($this->fields, 
				    $this->fieldParser[$key]->fields);
      }
    }
    else
    {
      $this->fieldParser->Parse($this->fieldsDef);
      $this->fields = & $this->fieldParser->fields;
    }
  }

  protected function LoadFieldsDef()
  {
  }

  abstract protected function Execute();
}

?>
