<?php
/******************************************************************************/
/********************************** WebElements *******************************/
/******************** por João Makray <www.joaomak.net> ******************/
/********************************** Versão 2.5 ********************************/

// ATTRIBUTES
function parseAttributes($params){
    if(is_array($params))return $params;
    $params=str_replace('&amp;','<Eamp;>',$params);
    $params=str_replace('\=','<Eq;>',$params);
    $out=array();
    $sep=explode('&',$params);
    foreach($sep as $attr){
        $attr=str_replace('<Eamp;>','&amp;',$attr);    
        $kv=explode('=',$attr);
	  if(count($kv)==1 && count($sep)==1 && $kv[0]!='')
		$out['id']=$kv[0];
        if(count($kv)==2)
            $out[$kv[0]]= str_replace('<Eq;>','=',$kv[1]);
    }
    return $out;
}

///////////////////////////////// BASIC ////////////////////////////////////////

class div{
    var $str;
    function div($params=''){$params = parseAttributes($params);
        $this->str = "\n".'<div ';
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>';
    } 
    function add($txt){
    	  $this->str .= $txt;
    }
    function get_html(){
        return $this->str . "\n".'</div>';      
    }
}
function div($txt, $params=''){$params = parseAttributes($params);
    $str="\n<div ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';    
    $str .= '>'.$txt."\n".'</div>';
    return $str;
}
class span{
    var $str;
    function span($params=''){$params = parseAttributes($params);
        $this->str = '<span ';
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>';
    } 
    function add($txt){
    	  $this->str .= $txt;
    }
    function get_html(){
        return $this->str . '</span>';      
    }
}
function span($txt, $params=''){$params = parseAttributes($params);
    $str="<span ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</span>';
    return $str;
}
function small($txt, $params=''){$params = parseAttributes($params);
    $str="<small ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</small>';
    return $str;
}
function big($txt, $params=''){$params = parseAttributes($params);
    $str="<big ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</big>';
    return $str;
}
function a($txt, $params=''){$params = parseAttributes($params);
    $str="<a ";
    if(array_key_exists('href',$params)){
    		$tmp=str_replace('&amp;','&',$params['href']);
    		$params['href']=str_replace('&','&amp;',$tmp);
    }		
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</a>';
    return $str;
}
function img($params=''){$params = parseAttributes($params);
    $str="<img ";
    if(!array_key_exists('alt',$params)) $params['alt']='';
    if($params['alt']!='' && !array_key_exists('title',$params)) $params['title']=''; //correct ie 
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    return $str.'/>';
} 
function p($txt, $params=''){$params = parseAttributes($params);
    $str="\n<p ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</p>';
    return $str;
}
function br($params=''){$params = parseAttributes($params);
    $str="<br ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    return $str.'/>'."\n";
}                  
function hr($params=''){$params = parseAttributes($params);
    $str="\n<hr ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    return $str.'/>'."\n";
}
function script($script, $params=''){  $params = parseAttributes($params); 
	//<![CDATA[
    if(!array_key_exists('type',$params)) $params['type']='text/javascript';
    $str= '<script ';  
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';   
    return $str.'>'.$script.'</script>'."\n";  //]]
}
function del($txt, $params=''){$params = parseAttributes($params);
    $str="<del ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</del>';
    return $str;
}
function blockquote($txt, $params=''){$params = parseAttributes($params);
    $str="\n<blockquote ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt."\n</blockquote>";
    return $str;
}
function cite($txt, $params=''){$params = parseAttributes($params);
    $str="<cite ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</cite>';
    return $str;
}
function b($txt){
    return '<b>'.$txt.'</b>';
} 
function i($txt){
    return '<i>'.$txt.'</i>';
}
function strong($txt, $params=''){$params = parseAttributes($params);
	$str="<strong ";
	foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    	$str .= '>'.$txt.'</strong>';
    	return $str;
}
function em($txt, $params=''){$params = parseAttributes($params);
	$str="<em ";
	foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    	$str .= '>'.$txt.'</em>';
    	return $str;
}
function iframe($params=''){$params = parseAttributes($params);
    $str="\n<iframe ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    return $str.'></iframe>';
}                  
function address($txt, $params=''){$params = parseAttributes($params);
    $str="\n<address ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';    
    $str .= '>'.$txt."\n".'</address>';
    return $str;
}
function comment($txt){
    return "\n<!-- ".$txt." -->\n";
}
///////////////////////////////// HEADINGS /////////////////////////////////////
function h1($txt, $params=''){$params = parseAttributes($params);
    $str="\n<h1";
    foreach($params as $key=>$value)
        $str .= ' '.$key.'="'.$value.'"';
    $str .= '>'.$txt.'</h1>';
    return $str;
}
function h2($txt, $params=''){$params = parseAttributes($params);
    $str="\n<h2";
    foreach($params as $key=>$value)
        $str .= ' '.$key.'="'.$value.'" ';
    $str .= '>'.$txt.'</h2>';
    return $str;
}
function h3($txt, $params=''){$params = parseAttributes($params);
    $str="\n<h3";
    foreach($params as $key=>$value)
        $str .= ' '.$key.'="'.$value.'" ';
    $str .= '>'.$txt.'</h3>';
    return $str;
}
function h4($txt, $params=''){$params = parseAttributes($params);
    $str="\n<h4";
    foreach($params as $key=>$value)
        $str .= ' '.$key.'="'.$value.'" ';
    $str .= '>'.$txt.'</h4>';
    return $str;
}
function h5($txt, $params=''){$params = parseAttributes($params);
    $str="\n<h5";
    foreach($params as $key=>$value)
        $str .= ' '.$key.'="'.$value.'" ';
    $str .= '>'.$txt.'</h5>';
    return $str;
}
function h6($txt, $params=''){$params = parseAttributes($params);
    $str="\n<h6";
    foreach($params as $key=>$value)
        $str .= ' '.$key.'="'.$value.'" ';
    $str .= '>'.$txt.'</h6>';
    return $str;
}
///////////////////////////////// FORMS //////////////////////////////////////// 
function form($txt, $params=''){$params = parseAttributes($params);
    $str="\n<form ";
    if(!array_key_exists('method',$params)) $params['method']='post';
    if(array_key_exists('action',$params)) $params['action']=str_replace('&','&amp;',$params['action']);
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt."\n".'</form>';
    return $str;
}
class form{
    var $str;
    function form($params=''){$params = parseAttributes($params);
        $this->str = "\n".'<form ';
        if(!array_key_exists('method',$params)) $params['method']='post';
        if(array_key_exists('action',$params)) $params['action']=str_replace('&','&amp;',$params['action']);
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>';
    } 
    function add($txt){
    	  $this->str .= $txt;
    }
    function get_html(){
        return $this->str . "\n".'</form>';      
    }
}
class fieldset{
    var $str;
    function fieldset($params=''){$params = parseAttributes($params);
        $this->str = "\n".'<fieldset ';
        foreach($params as $key=>$value)
            $this->str .= $key.'="'.$value.'" ';
        $this->str .= '>';    
    } 
    function add_legend($txt, $params=''){$params = parseAttributes($params);
        $this->str .= "\n".'<legend ';
        foreach($params as $key=>$value)
            $this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'.$txt.'</legend>'."\n";        
    } 
    function add($txt){
    	  $this->str .= $txt;
    }    
    function get_html(){
        return $this->str . "\n".'</fieldset>';      
    }
}//end fieldset class

function fieldset($txt, $params=''){$params = parseAttributes($params);
    $str="<fieldset ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</fieldset>';
    return $str."\n";
}
function legend($txt, $params=''){$params = parseAttributes($params);
    $str="<legend ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</legend>';
    return $str."\n";
}
function button($txt, $params=''){$params = parseAttributes($params);
    $str="<button ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</button>';
    return $str;
}
function label($txt, $params=''){$params = parseAttributes($params);
    $str="<label ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</label>';
    return $str;
}
function input($params=''){$params = parseAttributes($params);
    $str="<input ";
    if(!array_key_exists('type',$params)) $params['type']='text';
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    return $str.'/>';
}
function textarea($txt, $params=''){$params = parseAttributes($params);
    $str="<textarea ";
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '>'.$txt.'</textarea>';
    return $str."\n";
}   
class select{
    var $str ;
    var $empty;
    /*
    Select
    */
    function select($params=''){$params = parseAttributes($params);
        $this->str = "\n<select "; 
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'."\n";
        $this->empty = true;
    }
    function add_option($txt, $params=''){$params = parseAttributes($params);
   	  $this->str.="<option ";
    	  foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
	  $this->str .= '>'.$txt.'</option>'."\n";
	  $this->empty = false;
    }    
    function get_html(){    
        if(!$this->empty) return $this->str .= '</select>'."\n";      
        else return '<!-- select empty -->'."\n";
    }
}
///////////////////////////////// LISTS ////////////////////////////////////////   
 
class ul{
    var $str ;
    var $empty;
    /*
    Unordered List
    */
    function ul($params=''){$params = parseAttributes($params);
        $this->str = "\n<ul "; 
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '><!--'."\n";
        $this->empty = true;
    }
    function add_li($txt, $params=''){
        $this->str .= li($txt, $params);
        $this->empty = false;
    }    
    function get_html(){    
        if(!$this->empty) return $this->str .= '--></ul>'."\n\n";      
        else return '<!-- ul empty -->'."\n";
    }
}
function get_ul($items, $params=''){
    $ul=new ul($params);
    foreach($items as $value){
        $ul->add_li($value);
    }
    return $ul->get_html();          
}

// LI
function li($txt, $params=''){$params = parseAttributes($params);
    $str = '--><li';
    foreach($params as $key=>$value)
        	$str .= ' '.$key.'="'.$value.'" ';
    $str .= '>';
    $str .= $txt;               
    return $str ."</li><!--\n"; 
} 
class ol{
    var $str ;
    var $empty;
    /*
    Unordered List
    */
    function ol($params=''){$params = parseAttributes($params);
        $this->str = "\n<ol ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
	  $this->str .= '><!--'."\n";
        $this->empty = true;
    }
    function add_li($txt, $params=''){
        $this->str .= li($txt, $params);
        $this->empty = false;
    }    
    function get_html(){ 
    	  if(!$this->empty) return $this->str .= '--></ol>'."\n\n";  
    	  else return '<!-- ol empty -->'."\n";    
    }
}
function get_ol($items, $params=''){
    $ol=new ol($id,$params);
    foreach($items as $value){
        $ol->add_li($value);
    }
    return $ol->get_html();          
}

class dl{
    var $str ;
    var $empty;
    /*
    Definition List
    */
    function dl($params=''){$params = parseAttributes($params);
        $this->str = "\n<dl ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'."\n";
        $this->empty = true;
    }
    
    function add_dt($txt, $params=''){$params = parseAttributes($params);
        // dt
        $this->str .= "<dt ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'.$txt.'</dt>'."\n";
        $this->empty = false;
    }        
    function add_dd($txt, $params=''){$params = parseAttributes($params);
        // dd
        $this->str .= "<dd ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'.$txt.'</dd>'."\n";
        $this->empty = false;
    }
    function get_html(){    
    	  if(!$this->empty) return $this->str .= '</dl>'."\n\n";  
        else return $this->str .= '<!-- dl empty -->'."\n";      
    }           
}
function get_dl ($items, $params=''){
    /*
    Definition List
    - entra array tipo 'titulo' => 'descricao',
    */ 
    $dl=new dl($params='');         
    foreach($items as $key => $value){
        $dl->add_dt($key);
        $dl->add_dd($value);
    }
    return $dl->get_html();
}
///////////////////////////////// TABLE ////////////////////////////////////////   
class table{
    var $str ;
    var $is_first_tr = true;
    /*
    Table
    */
    function table($params=''){$params = parseAttributes($params);
        $this->str = "\n<table ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'."\n";
    }
    function add_caption($txt, $params=''){$params = parseAttributes($params);
        // caption
        $this->str .= "<caption ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'.$txt.'</caption>';
    }
    function new_tr($params=''){$params = parseAttributes($params);
        // tr 
        if(!$this->is_first_tr)$this->str .= "</tr>\n";
        $this->str .= "<tr ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'."\n";
        $this->is_first_tr = false;
    }
    function add_th($txt, $params=''){$params = parseAttributes($params);
        // td
        $this->str .= "<th ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'.$txt.'</th>';
    }        
    function add_td($txt, $params=''){$params = parseAttributes($params);
        // td
        $this->str .= "<td ";
     	  foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'.$txt.'</td>';
    }
    function get_html(){    
        if(!$this->is_first_tr)$this->str .= "</tr>\n";
        return $this->str .= "</table>\n\n";      
    }           
}
function get_table($items, $params=''){
    $table = new table($params);
    foreach($items as $row){
        $table->new_tr();
        foreach($row as $cell){
            $table->add_td($cell);
        }
    }
    return $table->get_html();
}
///////////////////////////////// Compostos //////////////////////////////

function flash($src, $params=''){$params = parseAttributes($params);
	$alt='<small><a href="http://adobe.com/go/getflash">get Flash</a></small>';
	$flash=array('src'=>$src,'id'=>'we_flash','alt'=>$alt,'width'=>'100%','height'=>'100%','version'=>6,'bgcolor'=>'#ffffff');
	foreach($flash as $attr=>$value){
		if(array_key_exists($attr,$params)) $flash[$attr]=$params[$attr];
	}
	//$w='100%', $h='100%', $bgcolor='#ffffff', $version = 6){ 
	$str = '<div id="'.$flash['id'].'" class="flash">'.$flash['alt'].'</div>';
	$str .= script('var we_flash=new SWFObject("'.$flash['src'].'", "'.$flash['id'].'", "'.$flash['width'].
	'", "'.$flash['height'].'", '.$flash['version'].', "'.$flash['bgcolor'].'");we_flash.addParam("wmode","transparent");we_flash.write("'.$flash['id'].'");');
	return $str;  
}
function swf($txt='', $params=''){$params = parseAttributes($params);
    $str="<object ";
    if(!array_key_exists('type',$params)) $params['type']='application/x-shockwave-flash';
    if(!array_key_exists('data',$params)) $params['data']='';
    // data, width, height
    foreach($params as $key=>$value)
        $str .= $key.'="'.$value.'" ';
    $str .= '><param name="movie" value="'.$params['data'].'" />';
    $str .= $txt.'</object>';
    return $str;
}


///////////////////////////////// RSS ///////////////////////////////////

class item{
    var $str ;
    var $empty;
    /*
    RSS Item
    */
    function item($params=''){$params = parseAttributes($params);
        $this->str = "\n<item ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'."\n";
        $this->empty = true;
    }
    
    function add_title($txt, $params=''){$params = parseAttributes($params);
        $this->str .= "<title ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'.$txt.'</title>'."\n";
        $this->empty = false;
    }        
    function add_link($txt, $params=''){$params = parseAttributes($params);
        $this->str .= "<link ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '>'.$txt.'</link>'."\n";
        $this->empty = false;
    }
    function add_description($txt, $params=''){$params = parseAttributes($params);
        $this->str .= "<description ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        $this->str .= '><![CDATA['.$txt.']]></description>'."\n";
        $this->empty = false;
    }
    function add_pubDate($txt, $params=''){$params = parseAttributes($params);
        $this->str .= "<pubDate ";
        foreach($params as $key=>$value)
        	$this->str .= $key.'="'.$value.'" ';
        	
        if(is_numeric($txt))$txt=gmdate("D, d M Y H:i:s T", $txt);	
        $this->str .= '>'.$txt.'</pubDate>'."\n";
        $this->empty = false;
    }
    function get_html(){    
    	  if(!$this->empty) return $this->str .= '</item>'."\n\n";  
        else return $this->str .= '<!-- item empty -->'."\n";      
    }           
}

?>
