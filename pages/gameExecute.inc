<?php

require_once('CMS_Ajax.inc');

class gameExecute extends CMS_Ajax
{
  protected function LoadPage()
  {
    if($_REQUEST['logout'] == '1')
      $this->pageBuilder->sessionH->attemptLogout();
      
    parent::LoadPage();

    if(!$this->pageBuilder->sessionH->isMember())
      $script = 'game.LoadLoginForm();';
    else
      $script = "game.Start();";

    $this->pageBuilder->rootTemplate->addText($script,'script');

  }
  
}

?>