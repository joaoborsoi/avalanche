<?php

require_once('AV_Page.inc');

class gameArea extends AV_Page
{
  protected function LoadPage()
  {
    $loadArea = $this->LoadModule('GAME_LoadArea');

    require_once($loadArea->attrs['elementClass'].'.inc');
    $elementClass = new $loadArea->attrs['elementClass']($this);

    $elementClass->LoadArea($loadArea);
  }
  
}


?>