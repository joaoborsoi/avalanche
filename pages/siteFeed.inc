<?php

require_once('SITE_Pages.inc');

class siteFeed extends SITE_Pages
{ 
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { 	//parent::LoadPage();
  	
  	header('Content-type: text/xml');
	header("Pragma: no-cache");
	$this->pageBuilder->rootTemplate('siteFeed');
	$lang=str_replace('_','-',mb_strtolower($this->lang)); 
	$this->pageBuilder->rootTemplate->addText((string)$lang, 'lang');
	$base = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
    	if(dirname($_SERVER['PHP_SELF'])!='/') $base .= '/';
    	$this->pageBuilder->rootTemplate->addText((string)$base, 'base');
    	
    	// FILE
    	$_GET['docId']= $this->sitePages['siteFeed'];
    	$doc = $this->LoadModule('LIB_GetDoc');
    	$title = $doc->children[0]->attrs['title'];
	$description = $doc->children[0]->attrs['content'];
    	$this->pageBuilder->rootTemplate->addText((string)strip_tags($description), 'description');
    	$this->pageBuilder->rootTemplate->addText((string)($title), 'title');
	
	// LIST
	$maxItems= $this->pageBuilder->siteConfig->getVar("news", "feedMaxItems");
  	$_GET['orderBy']='publicationDate';
	$_GET['returnFields']='publicationDate,title,content';
	$_GET['dateFormat']='%Y-%m-%d';
	$dateFormat='%U';
		
	$files = $this->getFiles('/Notícias/');
	$i=0;
	foreach($files->children as $file){
		// don't show items in the future
		$now=time();
		$eventTimestamp=strtotime($file->attrs['publicationDate']);
		if($eventTimestamp>$now)continue;
		
		$name = $file->attrs['title'];
		$date = $file->attrs['publicationDate'];
		//$date = strftime($dateFormat, $eventTimestamp);
		$date =gmdate("D, d M Y H:i:s T", $eventTimestamp);
		
		$cont = $file->attrs['content'];
		$docId = $file->attrs['docId'];
		$item=new item();
		$item->add_title($name);
		$item->add_link('http://'.$_SERVER['SERVER_NAME'].'/siteNews/'.$docId.'.av');
		$item->add_description($cont);
		$item->add_pubDate(($date));

		$i++;
		if($i>$maxItems)break;
	} 
	
	$this->pageBuilder->rootTemplate->addText($item->get_html(), 'content');
		

  	
	
	
  }
 
}

?>
