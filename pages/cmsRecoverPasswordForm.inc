<?php

require_once('CMS_Page.inc');

class cmsRecoverPasswordForm extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    // first area should be the home
    if(count($getFolderList->children)==1)
      return $getFolderList->children[0]->attrs['name'].'/';
    else
      return parent::GetMenuPath($getFolderList);
  }

  protected function LoadContent()
  {
    $content = & $this->pageBuilder->loadTemplate('cmsRecoverPasswordForm');

    $this->pageLabels->PrintHTML($content);  
    
    $this->pageBuilder->rootTemplate->addTemplate($content,'mainContent');

    $recoverPasswordTag = $this->pageLabels->attrs['recoverPasswordTag'];
    $this->pageBuilder->rootTemplate->addText($recoverPasswordTag,'title');

    $script = "<script type='text/javascript'>\n";
    $script .= "\$(document).ready(function() {\n";
    $script .= "avForm.OnLoad('recoverPasswordFormField');\n";
    $script .= "});\n";
    $script .= '</script>';
    $this->pageBuilder->rootTemplate->addText($script,'header');
  }
  

}


?>
