<?php

require_once('SITE_Pages.inc');


class siteEvents extends SITE_Pages
{ 
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { 	
  	
  	
  	if(isset($_GET['docId'])){
  		 
  		parent::LoadPage(); 
  		  		
  		require_once("SITE_PageClass.inc");
    		new page($this, $_GET['docId'], true);
  		//$this->wrap->addText(a('Todas', 'href=siteNews.av'), 'content');
  	}else{
  		  			
	     	$_GET['docId']= $this->sitePages['siteEvents'];
	    	parent::LoadPage();	    	
	    	
	    	require_once("SITE_PageClass.inc");
	    	new page($this, $_GET['docId']);

		// LIST
		$_GET['orderBy']='eventDate';
		$_GET['limit']=50;
		$_GET['returnFields']='eventDate,title,descr';
		$_GET['dateFormat']='%Y-%m-%d';
		if($this->lang=='en_US')$dateFormat='%m/%d/%y';
		else $dateFormat='%d/%m/%y';
		
		$files = $this->getFiles('/Eventos/');//print_r($files->children[0]->attrs);exit;
		$nav=new ul('class=date-list');
		$pastEvents=array();
		foreach($files->children as $k=>$file){ 
			
			// don't show items in the past
			$now=strtotime("-24 hours");
			$eventTimestamp=strtotime($file->attrs['eventDate']);
			if($eventTimestamp<$now){
				$pastEvents[]=$file;
				continue;
			}
			$name = $file->attrs['title'];
			$date = strftime($dateFormat, $eventTimestamp);
			//$author = $file->attrs['authors'];
			$descr='';
			if($file->attrs['descr'])$descr = p(nl2br($file->attrs['descr']));
			$docId = $file->attrs['docId'];
		
			$link = 'siteEvents/'.$docId.'.av';
			$nav->add_li(h5($date,'class=date').' '.h4(a($name, 'href='.$link)). $descr );
		}
		
		// past events
		$past=new ul('class=date-list past');
		$pastEvents=array_reverse($pastEvents);
		foreach($pastEvents as $event){
			$name = $event->attrs['title'];
			$eventTimestamp=strtotime($event->attrs['eventDate']);
			$date = strftime($dateFormat, $eventTimestamp);
			$descr='';
			if($event->attrs['descr'])$descr = p(nl2br($event->attrs['descr']));
			$docId = $event->attrs['docId'];
		
			$link = 'siteEvents/'.$docId.'.av';
			$past->add_li(h5($date,'class=date').' '.h4(a($name, 'href='.$link)). $descr );
		}
		
		if(count($files->children)==0)$this->wrap->addText(p($this->labels->attrs['noEventsTag'],'class=message'), 'content');
		else $this->wrap->addText($nav->get_html(), 'content');
		
		if(!$past->empty)$this->wrap->addText(hr().$past->get_html(), 'content');
		
	}
	
  }
 
}

?>
