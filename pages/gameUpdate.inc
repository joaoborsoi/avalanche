<?php

require_once('CMS_Ajax.inc');

class gameUpdate extends CMS_Ajax
{
  protected $logFile;

  protected function Exec($cmd, & $output)
  {
    $msg = date("Y-m-d H:i:s")." exec: $cmd\n";
    error_log($msg, 3, $this->logFile);
    exec($cmd, $output, $retValue);
    $msg = date("Y-m-d H:i:s")." return_value: $retValue\n";
    error_log($msg, 3, $this->logFile);
    foreach($output as $line)
      error_log($line."\n", 3, $this->logFile);
    return $retValue;
  }

  protected function LoadPage()
  {
    parent::LoadPage();

    $this->logFile = $this->pageBuilder->siteConfig->getVar("dirs", "log").
      'update.log';

    try
    {
      // verifica se usuário logado é administrador
      if(!in_array('Administrators',$this->memberGroups))
	throw new AV_Exception(PERMISSION_DENIED,$this->pageBuilder->lang);

      // atualizar arquivos - git
      $this->Pull();

      // salvar backup da base
      $this->BackupDB();

      // reseta banco de dados
      $this->CreateDB();

      $pageLabels = $this->LoadModule('LANG_GetPageLabels');
      $script = "game.Alert('".$pageLabels->attrs['gameUpdateMsg']."');";
      $script .= "window.location.reload();";

    }
    catch(Exception $e)
    {
      $script = "game.Alert('".$e->getMessage()."');";
    }
    $this->pageBuilder->rootTemplate->addText($script,'script');
  }

  protected function BackupDB()
  {
    $binDir = $this->pageBuilder->siteConfig->getVar('dirs','bin');
    $retValue = $this->Exec("cd $binDir && ./AV_BackupDB",$output);
    if($retValue!=0)
      throw new AV_Exception(FAILED, $this->pageBuilder->lang,
			     array('addMsg'=>' Erro ao salvar base de dados.'));
  }

  protected function CreateDB()
  {
    $binDir = $this->pageBuilder->siteConfig->getVar('dirs','bin');
    $place = $this->pageBuilder->siteConfig->getVar('defaults','place');
    $retValue = $this->Exec("cd $binDir && ./AV_CreateDB $place",$output);
    if($retValue!=0)
      throw new AV_Exception(FAILED, $this->pageBuilder->lang,
			     array('addMsg'=>' Erro ao criar base de dados.'));
  }

  protected function Pull()
  {
    $updated = false;
    $siteDir = $this->pageBuilder->siteConfig->getVar('dirs','site');
    $retValue = $this->Exec("cd $siteDir && git reset --hard", $output1);
    if($retValue!=0)
      throw new AV_Exception(FAILED, $this->pageBuilder->lang,
    			     array('addMsg'=>' Erro ao atualizar jogo.'));

    $retValue = $this->Exec("cd $siteDir && git pull", $output2);
    if($retValue!=0)
      throw new AV_Exception(FAILED, $this->pageBuilder->lang,
			     array('addMsg'=>' Erro ao atualizar jogo.'));

    if($output2[0]!="Already up-to-date.")
      $updated = true;

    $avDir = $this->pageBuilder->siteConfig->getVar('dirs','avalanche');
    $retValue = $this->Exec("cd $avDir && git reset --hard",$output3);
    if($retValue!=0)
      throw new AV_Exception(FAILED, $this->pageBuilder->lang,
			     array('addMsg'=>' Erro ao atualizar avalanche.'));

    $retValue = $this->Exec("cd $avDir && git pull",$output4);
    if($retValue!=0)
      throw new AV_Exception(FAILED, $this->pageBuilder->lang,
			     array('addMsg'=>' Erro ao atualizar avalanche.'));
    if($output4[0]!="Already up-to-date.")
      $updated = true;

    if(!$updated)
      throw new AV_Exception(FAILED, $this->pageBuilder->lang,
			     array('message'=>'Jogo já atualizado'));
   }

}

?>