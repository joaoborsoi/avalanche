<?php

require_once('CMS_Ajax.inc');

class gameLogin extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      try
      {
	$login = $this->LoadModule('UM_Login');
	$this->LoadModule('GAME_Start');

	$script .= "game.OnLoginSuccess();";
      }
      catch(AV_Exception $e)
      {
	if($e->getCode()==FIELD_REQUIRED)
	{
	  $data = $e->getData();
	  if(!$data['fieldName'])
	    throw $e;
	  $script = "game.SetRequiredError('".$data['fieldName']."');";
	}
	$script .= "game.OnFormSubmitError('".$e->getMessage()."');";
      }
    }
    catch(Exception $e)
    {
      $script = "game.OnFormSubmitError('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');

  }
  
}

?>