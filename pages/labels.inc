<?php

require_once('REST_Page.inc');

class labels extends REST_Page
{
  protected function Get()
  {
    $labels = $this->LoadModule('LANG_GetPartLabels');
    $labels = $this->EncodeModObject($labels);
    return $labels['attrs'];
  }
}