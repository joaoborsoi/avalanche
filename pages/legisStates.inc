<?php

require_once('states.inc');

class legisStates extends states
{
  protected function Get()
  {
    $states1 = json_decode(file_get_contents('json/states.json'),true);

    $states2 = $this->GetStates('/content/Menu','/');

    try
    {
      $states3 = $this->GetStates('/wikiDocuments/','/docs/');
    }
    catch(AV_Exception $e)
    {
      if($e->getCode()==PERMISSION_DENIED)
	return array_merge($states1,$states2);
    }
    return array_merge($states1,$states2,$states3);

  }


}

?>