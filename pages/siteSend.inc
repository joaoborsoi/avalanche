<?php

require_once('SITE_Pages.inc');


class siteSend extends SITE_Pages
{
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { 	
  	
  	parent::LoadPage();
    
 	$from = $_POST['name']. '<'.$_POST['email']. '>';
	$mail_to = $this->pageBuilder->siteConfig->getVar("admin", "emailTo");
	$subject = '['.$_SERVER['HTTP_HOST'].'] '.$_POST['name'];
	
	$body = '';
	foreach($_POST as $varName=>$var){
	      // max number of bytes per variable
	      if(strlen($var)>500){
		$this->ReturnMessage('Invalid: '.$var);
		return;
	      }
	      $body .= str_replace('_',' ',$varName).': '.stripslashes($var);
	      $body .= "\n. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n";

	 }
    	
  
    	/* SECURITY */
    	
    	if(!$this->is_valid_email($_POST['email']))$this->ReturnMessage('Invalid Email');
    	
    	// max number of variables in the form
	if(count($_POST)>20){
	      $this->ReturnMessage('Invalid Form! Too long');
	      return;
	}
	// spambots
    	if (eregi("\r",$from) 
  	 || eregi("\n",$from) 
  	 || eregi("Content-Type: text/plain;",$body) 
  	)$this->ReturnMessage("Invalid: ".$from);
  	
  	// NOW SEND
   	$go=mail($mail_to, $subject, utf8_decode($body) );
  	if($go){
  		//$this->ReturnMessage($this->labels->attrs['successTag']);
  		require_once("SITE_PageClass.inc");
    		new page($this, $_GET['docId']);
  	}else $this->ReturnMessage("Error");
  	// "From: ".$from."\r\n"
    
  }
  function ReturnMessage($txt){
  	die($txt);
  }
  function is_valid_email($email){
  	
      return preg_match("/^([a-z0-9]([a-z0-9_-]*\.?[a-z0-9])*)(\+[a-z0-9]+)?@([a-z0-9]([a-z0-9-]*[a-z0-9])*\.)*([a-z0-9]([a-z0-9-]*[a-z0-9]+)*)\.[a-z]{2,6}$/", $email) > 0;
  }

}

?>
