<?php

require_once('folders.inc');

class legisFolders extends folders
{
  protected function Post()
  {
    $this->LoadModule('LEGIS_NetChecker');
    return parent::Post();
  }

  protected function Delete()
  {
    $this->LoadModule('LEGIS_NetChecker');
    return parent::Delete();
  }
}