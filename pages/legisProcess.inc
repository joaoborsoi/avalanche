<?php

require_once('REST_Page.inc');

class legisProcess extends REST_Page
{
  private $expiration = NULL;

  protected function Header()
  {
    if($this->expiration !=NULL)
      header("Expires: ".gmdate("D, d M Y H:i:s",strtotime($this->expiration))." GMT");
    parent::Header();
  }

  protected function Get()
  {
    $_GET['dateFormat'] = '%Y-%m-%d %H:%M:%S';
    $_GET['numberFormat'] = '0';
    $_GET['short'] = '1';
    $getDoc = $this->LoadModule('LEGIS_GetProcess');

    if($_REQUEST['docId']==NULL && $_REQUEST['parentId']==NULL)
      $this->expiration = '+1 year';

    return $this->EncodeModObject($getDoc->children[0]);
  }

  protected function Post()
  {
    $this->LoadModule('LEGIS_NetChecker');
    if($_GET['docId']==NULL)
    {
      $mod = $this->LoadModule('LEGIS_InsProcess');
      $_GET['docId'] = $mod->attrs['docId'];
    }
    else
      $mod = $this->LoadModule('LEGIS_SetProcess');
    return $this->Get();
  }

  protected function Delete()
  {
    $this->LoadModule('LEGIS_NetChecker');
    $insDoc = $this->LoadModule('LIB_DelDocLink');
    return "";

  }
}