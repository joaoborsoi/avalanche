<?php

require_once('CMS_Page.inc');

class cmsLoginForm extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    // first area should be the home
    if(count($getFolderList->children)==1)
      return $getFolderList->children[0]->attrs['name'].'/';
    else
      return parent::GetMenuPath($getFolderList);
  }

  protected function LoadContent()
  {
    // calls parent function
    $this->LoadLoginForm(false,$_REQUEST['location']);
  }
  

}


?>
