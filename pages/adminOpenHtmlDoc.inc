<?php

require_once('ADMIN_OpenHtmlDoc.inc');

class adminOpenHtmlDoc extends ADMIN_OpenHtmlDoc
{
  function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminHtmlDoc');
    
    $module = $this->LoadModule('LIB_GetDoc');
    if($module->status != SUCCESS)
    {
      $script = "alert('".$module->attrs['message'] . "');";
      $this->pageBuilder->rootTemplate->addText($script, 'onLoad');
    }
    else
      $module->PrintHTML($this->pageBuilder->rootTemplate);
  }
}

?>