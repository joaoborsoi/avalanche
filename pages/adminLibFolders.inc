<?php

require_once('ADMIN_Folders.inc');

class adminLibFolders extends ADMIN_Folders
{
  function __construct(& $pageBuilder)
  {
    parent::__construct($pageBuilder,'0','?pageId=adminLibFiles',
			'/library/');
    $this->pageBuilder->rootTemplate->addText('adminLibBody','bodyId');
    $this->pageBuilder->rootTemplate->addText('adminLibFiles','searchTarget');
  }
}
?>
