<?php

require_once('AV_Page.inc');

class shopCheckout extends AV_Page
{
  protected function Header()
  {
    // by default, disable cache
    $this->DisableCache();
    header('Content-Type: text/javascript');
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('shopAjaxScript');

    try
    {
      $setQuantity = & $this->LoadModule('SHOP_FlushCart');
      $script = "SubmitPagseguro()";
      $this->pageBuilder->rootTemplate->addText($script,'script');
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
      $this->pageBuilder->rootTemplate->addText($script,'script');
    }
  }
}

?>

