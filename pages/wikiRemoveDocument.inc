<?php

require_once('CMS_Ajax.inc');

class wikiRemoveDocument extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $_GET['path'] = '/content/Menu/'.$_REQUEST['path'];
      $delDoc = $this->LoadModule('WIKI_DelDocument');
      $script = "alert('Article has been removed!');";
      $script = "window.location='index.php'";
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }

}

?>