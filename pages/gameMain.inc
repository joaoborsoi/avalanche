<?php

require_once('AV_Page.inc');

class gameMain extends AV_Page
{
  protected function LoadPage()
  {   
    // root template
    $this->pageBuilder->rootTemplate('gameMain.tpt');
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');

    $pageLabels = $this->LoadModule('LANG_GetPageLabels');
    $pageLabels->PrintHTML($this->pageBuilder->rootTemplate);


    $customerLogo = $this->pageBuilder->siteConfig->getVar('game',
							   'customerLogo');
    $this->pageBuilder->rootTemplate->addText((string)$customerLogo,
					      'customerLogo');
  }
}

?>