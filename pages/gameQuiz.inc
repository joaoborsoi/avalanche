<?php

require_once('CMS_Ajax.inc');

class gameQuiz extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();
    try
    {
      $loadArea = $this->LoadModule('GAME_LoadQuizOption');
      $script = "game.OnQuizSubmitted('".$loadArea->attrs['targetDiv']."');";
    }    
    catch(AV_Exception $e)
    {
      if($e->getCode()==FIELD_REQUIRED)
      {
	$pageLabels = $this->LoadModule('LANG_GetPageLabels');
	$script .= "game.OnFormSubmitError('";
	$script .= $pageLabels->attrs['quizRequiredMsg']."');";
      }
      else
	$script .= "game.OnFormSubmitError('".$e->getMessage()."');";
    }
    $this->pageBuilder->rootTemplate->addText($script,'script');
  }
  
}

?>