<?php

require_once('CMS_Ajax.inc');

class gamePlay extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $this->LoadModule('GAME_Play');
      $script .= "game.Load();";
    }
    catch(AV_Exception $e)
    {
      $script = "game.Alert('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');

  }
  
}

?>