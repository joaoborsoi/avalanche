<?php

require_once('ADMIN_CreateDocLink.inc');

class adminCreateDocLink extends ADMIN_CreateDocLink
{
  function __construct(& $pageBuilder)
  {
    switch($_REQUEST['tab'])
    {
    case 'adminContent':
      parent::__construct($pageBuilder,1,'/content/');
      break;
    case 'adminLib':
      parent::__construct($pageBuilder,0,'/library/');
      break;
    }
    $this->pageBuilder->rootTemplate->addText($_REQUEST['tab'].'Body',
					      'bodyId');

  }
}

?>