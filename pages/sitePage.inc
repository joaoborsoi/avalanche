<?php

require_once('SITE_Pages.inc');


class sitePage extends SITE_Pages
{ 
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { 	parent::LoadPage();
  
  	
    	require_once("SITE_PageClass.inc");
    	new page($this, $_GET['docId']);

	
  }
 
}

?>
