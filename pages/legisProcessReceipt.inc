<?php

require_once('REST_Page.inc');

class legisProcessReceipt extends REST_Page
{
  protected function Get()
  {
    $memberGroups = $this->LoadModule('UM_GetMemberGroups');
    $process = false;
    foreach($memberGroups->children as $group)
    {
      if(in_array('app.process',$group->attrs['permissions']))
	$process = true;
    }

    if(!$process)
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    $get = $this->LoadModule('LEGIS_GetReceipts');
    return $get->attrs['value'];
  }

  protected function Post()
  {
    $this->LoadModule('LEGIS_NetChecker');
    $receipt = $this->LoadModule('LEGIS_GenerateReceipt');
    return $receipt->attrs;
  }

  protected function Delete()
  {
    $this->LoadModule('LEGIS_NetChecker');
    $memberGroups = $this->LoadModule('UM_GetMemberGroups');
    $permission = false;
    foreach($memberGroups->children as $group)
    {
      if(in_array('cancel.receipt',$group->attrs['permissions']))
	$permission = true;
    }

    if(!$permission)
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    $receipt = $this->LoadModule('LEGIS_CancelReceipt');
    return $receipt->attrs;
  }

}