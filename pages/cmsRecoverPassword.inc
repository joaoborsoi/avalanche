<?php

require_once('CMS_Ajax.inc');

class cmsRecoverPassword extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $this->pageLabels = & $this->LoadModule('LANG_GetPageLabels');
      try
      {
	$_GET['emailFieldName'] = 'login';
	$recoverPassword = $this->LoadModule('UM_RecoverPassword');
	$this->SendPassword($recoverPassword->attrs['email'],
			    $recoverPassword->attrs['password']);
	$script = "cms.OnRecoverPasswordSuccess('".$this->pageLabels->attrs['passwordSentTag']."');";
      }
      catch(AV_Exception $e)
      {
	if($e->getCode()==FIELD_REQUIRED)
	{
	  $data = $e->getData();
	  if(!$data['fieldName'])
	    throw $e;
	  $script = "avForm.OnFieldError('".$data['fieldName']."','";
	  $script .= $e->getMessage()."');";
	}
	else
	  $script .= "avForm.OnSubmitError('".$e->getMessage()."');";
      }
    }
    catch(Exception $e)
    {
      $script = "avForm.OnSubmitError('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');

  }

  protected function SendPassword($mailTo,$password)
  {
    $mailFrom = $this->pageBuilder->siteConfig->getVar("cms", "mailFrom");
    $headers = "From: $mailFrom\r\n";
    $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";

    $pageLabels = & $this->LoadModule('LANG_GetPageLabels');

    $subjectPrefix = $this->pageBuilder->siteConfig->getVar("cms", 
							    "subjectPrefix");
    $subject = $subjectPrefix." ".$pageLabels->attrs['newPasswordRequestTag'];
    $message = "<!DOCTYPE html PUBLIC \"-";
    $message .= "//W3C//DTD XHTML 1.0 Strict//EN\" ";
    $message .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
    $message .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>";
    $message .= "<p>".$pageLabels->attrs['newPasswordTag'];
    $message .= " <strong>$password</strong></p>";
    $message .= '</body></html>';

    if(!mail($mailTo, $subject, $message, $headers ,"-r".$mailFrom))
    { 
      // Se "não for Postfix"
      $headers .= "Return-Path: " . $mailFrom . "\r\n"; 
      if(!mail($mailTo, $subject, $message, $headers))
      {
	$msg = " ".$pageLabels->attrs['sendEmailErrorTag'];
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>$msg));
      }
    }
  }

}

?>