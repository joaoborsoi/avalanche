<?php

require_once('CMS_Page.inc');

class cmsModerateUser extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    // first area should be the home
    if(count($getFolderList->children)==1)
      return $getFolderList->children[0]->attrs['name'].'/';
    else
      return parent::GetMenuPath($getFolderList);
  }

  protected function LoadContent()
  {
    try
    {
      $moderateUser = $this->LoadModule('UM_ModerateUser');
      $message = $this->pageLabels->attrs['accountModActivatedTag'];
      $content = "<p class='alert'>$message</p>.";
      $this->pageBuilder->rootTemplate->addText($content,'mainContent');
    }
    catch(Exception $e)
    {
      $content = "<p class='alert'>".$e->getMessage()."</p>";
      $this->pageBuilder->rootTemplate->addText($content,'mainContent');
    }

  }
}

?>
