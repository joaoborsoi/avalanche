<?php

require_once('AV_Page.inc');

class shopSetQuantity extends AV_Page
{
  protected function Header()
  {
    // by default, disable cache
    $this->DisableCache();
    header('Content-Type: text/javascript');
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('shopAjaxScript');

    try
    {
      $setQuantity = & $this->LoadModule('SHOP_SetQuantity');
      $script = "UnsetError(".$_REQUEST['docId'].");";
      $this->pageBuilder->rootTemplate->addText($script,'script');
    }
    catch(Exception $e)
    {
      $script = "SetError(".$_REQUEST['docId'].",'".$e->getMessage()."');";
      $this->pageBuilder->rootTemplate->addText($script,'script');
    }
  }
}

?>

