<?php

require_once('CMS_Page.inc');

class cmsSubscribe extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    // first area should be the home
    if(count($getFolderList->children)==1)
      return $getFolderList->children[0]->attrs['name'].'/';
    else
      return parent::GetMenuPath($getFolderList);
  }

  protected function LoadContent()
  {
    // public access not allowed
    if($this->pageBuilder->sessionH->isGuest())
    {
	$this->LoadLoginForm();
	return;
    }

    try
    {
      $this->LoadRssPrefs();
    }
    catch(Exception $e)
    {
      $message = "<p id='errormsg'>".$e->getMessage().'</p>';
      $this->pageBuilder->rootTemplate->addText($message,'mainContent');
      return;
    }

  }
  
  protected function LoadRssPrefs()
  {
    if($this->userSession==NULL)
      throw new AV_Exception(PERMISSION_DENIED, $this->pageBuilder->lang);

    $_GET['userId'] = $this->userSession->attrs['userId'];
    $getRssPrefs = $this->LoadModule('CMS_GetRssPrefs');

    $rssPrefs = $this->pageBuilder->loadTemplate('cmsRSSPrefs');

    // avalanche version
    $rssPrefs->addText($this->pageBuilder->avVersion,'avVersion');

    // print labels and form
    $this->pageLabels->PrintHTML($rssPrefs);
    $getRssPrefs->PrintHTML($rssPrefs);


    $freeMyFeedLink = $this->pageBuilder->siteConfig->getVar('cms',
							     'freeMyFeedLink');
    if($freeMyFeedLink != NULL)
    {
      $content = "<p>";
      $content .= "<label>".$this->pageLabels->attrs['subscribeByTag'];
      $content .= " FreeMyFeed (Google Reader)</label>";
      $content .= "<a href='$freeMyFeedLink' >\n";
      $content .= "<img src='";
      $content .= "http://freemyfeed.com/images/fmf_chicklet_small.gif";
      $content .= "' alt='FreeMyFeed' />\n</a></p>\n";
    }
    $content .= "<p id='subscribeLink' class='rss2'>";
    $content .= "<label>".$this->pageLabels->attrs['subscribeDirectlyTag'];
    $content .= "</label>";
    $content .= "<a href='cmsFeed.av'>RSS</a></p>\n";

    $rssPrefs->addText($content,'extra');

    $this->pageBuilder->rootTemplate->addTemplate($rssPrefs,'mainContent');
  }

}


?>
