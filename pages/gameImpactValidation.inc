<?php

require_once('AV_Page.inc');

class gameImpactValidation extends AV_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('gameImpactValidation');

    try
    {
      $_GET['login'] = 'validator';
      $_GET['password'] = 'v4l1d4t0r';
      $this->LoadModule('UM_Login');

      $impactValidation = $this->LoadModule('GAME_ImpactValidation');
      $content = $impactValidation->attrs['log'];
      $this->pageBuilder->sessionH->attemptLogout();
    }
    catch(AV_Exception $e)
    {
      $content = '<h3>'.$e->getMessage().'</h3>';
    }

    $this->pageBuilder->rootTemplate->addText($content,'content');

  }
  
}

?>