<?php

require_once('AV_Page.inc');

class adminUploadFiles extends AV_Page
{
  protected function Header()
  {
    // by default, disable cache
    $this->DisableCache();
    header('Content-Type: text/javascript');
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminScriptSrc');

    try
    {
      // retrieve languages used by the system
      $getLangList = $this->LoadModule('LANG_GetLangList');
   
      $numFiles = count($_FILES['files']['name']);

      for ($i=0; $i < $numFiles; $i++) 
      {
	$uploadFile = basename($_FILES['files']['name'][$i]);

	// set new file document titles
	$_GET['title_numOfOptions'] = count($getLangList->children);
	$_GET['descr_numOfOptions'] = count($getLangList->children);
	foreach($getLangList->children as $child)
	{
	  $_GET['title_'.$child->attrs['fieldIndex'].'__lang'] = $child->attrs['lang'];
	  $_GET['title_'.$child->attrs['fieldIndex'].'__value'] = $_FILES['files']['name'][$i];
	  $_GET['descr_'.$child->attrs['fieldIndex'].'__lang'] = $child->attrs['lang'];
	  $_GET['descr_'.$child->attrs['fieldIndex'].'__value'] = '';
	}
	foreach($_FILES['files'] as $key=>$value)
	  $_GET['content'][$key] = $value[$i];
	$insDoc = $this->LoadModule('LIB_InsDoc');
	$docList[$i]['docId'] = $insDoc->attrs['docId'];
	$docList[$i]['title'] = $_FILES['files']['name'][$i];
	$docList[$i]['type'] = $_FILES['files']['type'][$i];
      }
      $result = array("status"=>SUCCESS,"message"=>"","numFiles"=>$numFiles,
		      "docList"=>$docList);

    }
    catch(Exception $e)
    {
      $result = array("status"=>$e->getCode(),
		      "message"=>$e->getMessage());
    }

    $this->pageBuilder->rootTemplate->addText(json_encode($result),
					      'script');
  }
}