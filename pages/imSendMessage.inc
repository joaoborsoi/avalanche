<?php 
require_once('AV_Page.inc');

class imSendMessage extends AV_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('json');
    try
    {
      $this->LoadModule('IM_SendMessage');
    }
    catch(AV_Exception $e)
    {
      $content = json_encode(array('exception'=>$e->getMessage()));
      $this->pageBuilder->rootTemplate->addText($content,'content');
    }
  }
}

?>