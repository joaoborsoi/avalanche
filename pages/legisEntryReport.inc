<?php

require_once('REST_Page.inc');

class legisEntryReport extends REST_Page
{
  protected function Post()
  {
    $module = $this->LoadModule('LEGIS_GetEntryReport'); 
    return $module->attrs;
  }
}

?>