<?php

require_once('REST_Page.inc');

class documents extends REST_Page
{
  private $expiration = NULL;

  protected function Header()
  {
    if($this->expiration !=NULL)
      header("Expires: ".gmdate("D, d M Y H:i:s",strtotime($this->expiration))." GMT");
    parent::Header();
  }

  protected function Get()
  {
    $_GET['dateFormat'] = '%Y-%m-%d %H:%M:%S';
    $_GET['numberFormat'] = '0';
    $_GET['short'] = '1';
    $getDoc = $this->LoadModule('LIB_GetDoc');

    if($_REQUEST['docId']==NULL)
      $this->expiration = '+1 day';

    $results = $this->EncodeModObject($getDoc->children[0]);

    if($_REQUEST['docId']!=NULL && $_REQUEST['docLinks'])
    {
      $getDocLinks = $this->LoadModule('LIB_GetDocLinks');
      $results['docLinks'] = $this->EncodeModObjectChildren($getDocLinks);
    }

    return $results;
  }

  protected function Post()
  {
    if($_GET['docId']==NULL)
    {
      $mod = $this->LoadModule('LIB_InsDoc');
      $_GET['docId'] = $mod->attrs['docId'];
    }
    else
      $mod = $this->LoadModule('LIB_SetDoc');
    return $this->Get();
  }

  protected function Delete()
  {
    $insDoc = $this->LoadModule('LIB_DelDocLink');
    return "";

  }
}

?>