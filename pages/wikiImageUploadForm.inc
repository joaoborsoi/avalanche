<?php

require_once('AV_Page.inc');

class wikiImageUploadForm extends AV_Page
{
  protected function LoadPage()
  {
    $max = ini_get("upload_max_filesize");
    switch($max[strlen($max)-1])
    {
    case 'K':
      $tag = $max . 'B';
      $max = substr($max,0,-1);
      $max = $max * 1024;
      break;
    case 'M':
      $tag = $max . 'B';
      $max = substr($max,0,-1);
      $max = $max * 1024 * 1024;
      break;
    case 'G':
      $tag = $max . 'B';
      $max = substr($max,0,-1);
      $max = $max * 1024 * 1024 * 1024;
      break;
    }

    $this->pageBuilder->rootTemplate('wikiImageUploadForm');
    $this->pageBuilder->rootTemplate->addText((string)$max, 'upload_max_filesize');
    $this->pageBuilder->rootTemplate->addText($tag, 'maxFilesizeTag');

    // avalanche version
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');

    $this->pageBuilder->rootTemplate->addText($_REQUEST['fieldName'],
					      'fieldName');

    $pageLabels = $this->LoadModule('LANG_GetPageLabels');
    $pageLabels->PrintHTML($this->pageBuilder->rootTemplate);  
  }

}