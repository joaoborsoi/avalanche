<?php

require_once('AV_Page.inc');

class cmsCommentForm extends AV_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('cmsCommentForm');
    $this->pageLabels = & $this->LoadModule('LANG_GetPageLabels');
    $this->pageLabels->PrintHTML($this->pageBuilder->rootTemplate);
    
    $_GET['path'] = '/comments/';
    $getDoc = $this->LoadModule('LIB_GetDoc');

    // specifies items to be printed
    $printItems = & $getDoc->children[0]->printItems;
    $printItems[] = 10; // docId
    $printItems[] = 20; // articleId
    $printItems[] = 4150; // comment

    // prints article fields to the template
    $getDoc->PrintHTML($this->pageBuilder->rootTemplate);
  }
}