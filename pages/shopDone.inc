<?php

require_once('CMS_Page.inc');

class shopDone extends CMS_Page
{

  protected function LoadContent()
  {
    if(count($_POST)>0)
    {
      $this->pageBuilder->rootTemplate('shopAjaxScript');
      return;
    }

    $_GET['labelId'] = 'shopDoneTag';
    $_GET['lang'] = $this->lang;
    $getLabel = $this->LoadModule('LANG_GetLabel');

    $message = "<h2>".$getLabel->attrs['value']."</h2>\n";
    $this->pageBuilder->rootTemplate->addText($message,'mainContent');
  }


}

?>