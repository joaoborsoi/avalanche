<?php

require_once('AV_Page.inc');

class gamePanel extends AV_Page
{
  protected function LoadPage()
  {
    // root template
    $this->pageBuilder->rootTemplate('gamePanel');

    // labels
    $pageLabels = & $this->LoadModule('LANG_GetPageLabels');
    $pageLabels->PrintHTML($this->pageBuilder->rootTemplate);

    $_GET['typeId'] = 1;
    $getIndicators = & $this->LoadModule('GAME_GetIndicators');
    $getIndicators->PrintHTML($this->pageBuilder->rootTemplate);
  }
  

}


?>
