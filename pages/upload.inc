<?php

require_once('REST_Page.inc');

class upload extends REST_Page
{
  protected function Post()
  {
    // retrieve languages used by the system
    $getLangList = $this->LoadModule('LANG_GetLangList');
    
    $numFiles = count($_FILES['files']['name']);
    
    for ($i=0; $i < $numFiles; $i++) 
    {
      $uploadFile = basename($_FILES['files']['name'][$i]);
      
      // set new file document titles
      $_GET['title_numOfOptions'] = count($getLangList->children);
      $_GET['descr_numOfOptions'] = count($getLangList->children);
      foreach($getLangList->children as $child)
      {
	$_GET['title_'.$child->attrs['fieldIndex'].'__lang'] = $child->attrs['lang'];
	$_GET['title_'.$child->attrs['fieldIndex'].'__value'] = $_FILES['files']['name'][$i];
	$_GET['descr_'.$child->attrs['fieldIndex'].'__lang'] = $child->attrs['lang'];
	$_GET['descr_'.$child->attrs['fieldIndex'].'__value'] = '';
      }
      foreach($_FILES['files'] as $key=>$value)
	$_GET['content'][$key] = $value[$i];
      $insDoc = $this->LoadModule('LIB_InsDoc');
      $docList[$i]['docId'] = $insDoc->attrs['docId'];
    }


    foreach($docList as $key=>$doc)
    {
      $_GET['docId'] = $doc['docId'];
      $getDoc = $this->LoadModule('LIB_GetDoc');
      $docList[$key] = $getDoc->children[0]->attrs;
    }

    return $docList;
  }
}