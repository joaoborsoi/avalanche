<?php

require_once('SITE_Pages.inc');


class sitePCDTR extends SITE_Pages
{ 
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { 	
	$pcdtrDir=$this->pageBuilder->siteConfig->getVar("pcdtr", "scriptDir"); 
    	
    	if(isset($_GET['text'])){
    		require_once($pcdtrDir."png.php");
	}else {
		require_once($pcdtrDir."css.php");
		$this->Header();
	}	
  }
  protected function Header()
  {
    //$this->DisableCache();
    header('Content-type: text/css');
  }
 
}

?>
