<?php

require_once('AV_Page.inc');

class adminGetFileContent extends AV_Page
{
  // prevents default header
  protected function Header()
  {
  }

  protected function LoadPage()
  {
    $openDoc = $this->LoadModule('LIB_GetFileContent');
    $data = & $openDoc->attrs;
    $fileName = $data[$_REQUEST['fieldName'].'FileName'];
    if($_REQUEST['forceDownload'])
    {
      header("Content-type: application/force-download");
      header("Content-Disposition: attachment; filename=\"$fileName\"");
    }
    else
    {
      // checking if the client is validating his cache and if it is current.
      if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) &&
	 strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$data['lastChanged'])
      {
	// Client's cache IS current, so we just respond '304 Not Modified'.
	header("Last-Modified: " . gmdate("D, d M Y H:i:s",$data['lastChanged'])
	       . " GMT", true, 304);
	return;
      }
      header("Content-Disposition: filename=\"$fileName\"");
    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s",$data['lastChanged'])
	   . " GMT", true, 200); 
    header("Content-type: " . $data[$_REQUEST['fieldName']."Type"]);
    header("Content-Length: ".strlen($data[$_REQUEST['fieldName']]));
    echo $data[$_REQUEST['fieldName']];
  }
}
?>