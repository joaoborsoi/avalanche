<?php

require_once('CMS_Page.inc');

class shopCart extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    // first area should be the home
    if(count($getFolderList->children)==1)
      return $getFolderList->children[0]->attrs['name'].'/';
    else
      return parent::GetMenuPath($getFolderList);
  }

  protected function LoadContent()
  {
    switch($_REQUEST['action'])
    {
    case 'addToCart':
      $addToCart = & $this->LoadModule('SHOP_AddToCart');
      break;
     
    case 'removeFromCart':
      $removeFromCart = & $this->LoadModule('SHOP_RemoveFromCart');
      break;

    default:
      break;
    }

    $this->LoadCart();
  }

  protected function LoadCart()
  {
    // loads cart
    $getCart = & $this->LoadModule('SHOP_GetCart');

    // load page labels
    $pageLabels = & $this->LoadModule('LANG_GetPageLabels');

    // check if there are products in the cart
    if(count($getCart->children)==0)
    {
      $content = '<h1>'.$pageLabels->attrs['cartTag']."</h1>\n";
      $content .= "<p id='message'>".$pageLabels->attrs['emptyCartTag'];
      $content .= "</p>\n";
      $this->pageBuilder->rootTemplate->addText($content,'mainContent');
      return;
    }

    // load cart form template
    $cartForm = & $this->pageBuilder->loadTemplate('shopCartForm');
    $pageLabels->PrintHTML($cartForm,NULL,'htmlentities_utf');
    $this->pageBuilder->rootTemplate->addTemplate($cartForm,'mainContent');

    $email_cobranca = $this->pageBuilder->siteConfig->getVar("pagseguro", 
							     "email_cobranca");
    $frete = $this->pageBuilder->siteConfig->getVar("pagseguro", "frete");

    $cartForm->addText((string)$email_cobranca,'email_cobranca');

    $cartForm->addText($this->pageBuilder->avVersion,'avVersion');

    $locale = localeconv();
    $cartForm->addText((string)$locale['frac_digits'],'fracDigits');
    $cartForm->addText((string)$locale['mon_decimal_point'],'decimalSep');
    $cartForm->addText((string)$locale['mon_thousands_sep'],'thousandSep'); 

    $i = 1;
    foreach($getCart->children as $child)
    {
      $imageId = $child->attrs['imageId'];
      $_GET['docId'] = $imageId;
      $getImg = $this->LoadModule('LIB_GetDoc');

      $title = htmlentities_utf($child->attrs['title']);
      $titleLatin1 = htmlentities(mb_convert_encoding($child->attrs['title'],
 						      'ISO-8859-1','UTF-8'));
  
      // product column - icon + title
      $content .= "<tr id='row_".$child->attrs['docId']."'>\n";
      $content .= "<td class='productName'><a href='content/";
      $content .= $child->attrs['docId']."'> ";
      $content .= "<img src='cartThumb/$imageId' alt='";
      $content .= htmlentities_utf($getImg->children[0]->attrs['title'])."' ";
      $content .= " /> ";
      $content .= "<big>$title</big></a>\n";
      // hidden fields
      $content .= "<input type='hidden' id='item_id_$i' name='item_id_$i' ";
      $content .= "value='".$child->attrs['docId']."' />\n";
      $content .= "<input type='hidden' name='item_descr_$i' ";
      $content .= "value='$titleLatin1'/>\n";
      $content .= "<input type='hidden' id='item_valor_unit_$i' value='";
      $content .= str_replace(array(',','.'),'',$child->attrs['price']);
      $content .= "' />\n";
      $content .= "<input type='hidden' name='item_valor_$i' ";
      $content .= "id='item_valor_$i' />\n";
      $content .= "<input type='hidden' name='item_frete_$i' ";
      $content .= "value='$frete' />\n";
      $content .= "<input type='hidden' name='item_peso_$i' value='";
      $content .= $child->attrs['weight']."' />\n";
      $content .= "</td>\n";

      // value column
      $content .= "<td id='value_$i'>&nbsp;</td>\n";

      // quantity column
      $content .= "<td><input type='text' name='item_quant_$i' ";
      $content .= "size='2' id='item_quant_$i' value='";
      $content .= $child->attrs['num']."' ";
      $content .= "onchange='OnCartItemChange($i);' /></td>\n";

      // subtotal
      $content .= "<td id='subtotal_$i'>&nbsp;</td>\n";

      // remove item button
      $content .= "<td><a class='removeBtn' title='";
      $content .= $pageLabels->attrs['removeTag'];
      $content .= "' onclick='OnRemoveFromCartClick(".$child->attrs['docId'];
      $content .= ");'><em>".$pageLabels->attrs['removeTag'].'</em></a>';
      $content .= "</td>\n";

      $content .= "</tr>\n";

      $i++;
    }

    $cartForm->addText($content,'form');
  }
}


?>