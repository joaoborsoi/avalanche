<?php

require_once('REST_Page.inc');

class session extends REST_Page
{
  protected function Get()
  {
    $this->userSession->attrs['sessionId'] = $this->pageBuilder->sessionH->sessionID;    
    $memberGroups = $this->LoadModule('UM_GetMemberGroups');
    $groups = $this->EncodeModObjectChildren($memberGroups);
    $this->userSession->attrs['groups'] = $groups;

    return $this->userSession->attrs;
  }
}

?>