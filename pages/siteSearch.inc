<?php

require_once('SITE_Pages.inc');


class siteSearch extends SITE_Pages
{
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { parent::LoadPage();
    
    	
      $_GET['filter']= ($_GET['q']);
      //die($_GET['filter']);
      // acento nao funciona!!
    	
      $_GET['limit']= 30;
      $_GET['searchCriteria']= '*';
      $_GET['recursiveSearch']=1;
      $_GET['path']= '/content/';
      $_GET['returnFields']='title,descr';
      $module = $this->LoadModule('LIB_Search');
    	
      $this->setPageTitle($this->labels->attrs['siteSearchTag'].': '.$_GET['q']);
      
      $total=$module->attrs['totalResults'];
      $out.= p(strong($total).' '.$this->labels->attrs['siteResultsTag'].' &ldquo;'. b($_GET['q']). '&rdquo;.').br();
      
      $list=new ol('search-results');
      foreach($module->children as $child){
      	$di=$child->attrs['docId'];
      	$descr=$child->attrs['descr'];
      	if(trim($descr))$descr=p(nl2br($descr));
      	
      	$path=substr($child->attrs['path'], 9);
      	switch($path){
      	case 'Eventos/':
      		$link='siteEvents/'.$di.'.av';
      	break;
      	case 'Notícias/':
      		$link='siteNews/'.$di.'.av';
      	break; 
      	
      	default:
      		$link='sitePage/'.$di.'.av';
      	}   
      	
      	$knownIds=array_flip($this->sitePages);
      	if(isset($knownIds[$di])) $link=$knownIds[$di] .'.av'; 
      	
      	// skip send page
      	if($link=='siteSend.av')$link='siteContact.av';	
      	
      	$list->add_li( big(a($child->attrs['title'], 'href='.$link)). $descr);
      
      }
      $out.= $list->get_html();


/* NAV 

     $start = $_GET['offset'] + 1;
     $end = $start + 9;
     if($end > $module->attrs['totalResults'])
   $end = $module->attrs['totalResults'];
     $searchList->addText((string)$start, 'start');
     $searchList->addText((string)$end, 'end');
     for($i = 0; $i <= 90 && $i < $module->attrs['totalResults']; $i += 10)
     {
   if($i > 0)
     $nav .= ' - ';
   if($_GET['offset'] == NULL && $i == 0 || $i == $_GET['offset'])
     $nav .= '<b> ' . ($i/10+1) . ' </b>';
   else
   {
     $nav .= "<a href='index.php?pageId=searchClient&amp;offset=$i";
     $nav .= '&amp;lang=' . $this->pageBuilder->lang . '&amp;filter=';
     $nav .= htmlentities_utf(urlencode(stripslashes($_REQUEST['filter'])));
     $nav .= "'>" .($i/10+1) . '</a>';
   }
     }
     if($_GET['offset'] < 90 &&
    ($module->attrs['totalResults'] - $_GET['offset']) > 10)
     {
   $nav .= "&nbsp; <a class='next' href='index.php?pageId=";
   $nav .= 'searchClient&amp;offset=' .($_GET['offset'] + 10);
   $nav .= '&amp;lang=' . $this->pageBuilder->lang . '&amp;filter=';
   $nav .= htmlentities_utf(urlencode(stripslashes($_REQUEST['filter'])));
   $nav .= "'><b>&nbsp;&raquo;&nbsp;</b></a>";
     }
     $searchList->addText((string)$nav, 'nav'); 
     
     */
     
      $this->wrap->addText((string)$out, 'content');
      $this->wrap->addText((string)$_GET['q'], 'siteSearchFilter');
      
      // more
      $more = $this->pageBuilder->loadTemplate('siteSearchMore');
      $more->addText((string)$_GET['q'], 'siteSearchFilter');
      $this->wrap->addTemplate($more, 'include');
   }   
}

?>
