<?php

require_once('AV_Page.inc');

class gameUserInfo extends AV_Page
{
  protected $memberGroups;

  protected function LoadPage()
  {
    // only for logged users
    if($this->pageBuilder->sessionH->isMember())
    {
      $pageLabels = $this->LoadModule('LANG_GetPageLabels');

      // root template
      $this->pageBuilder->rootTemplate('gameUserInfo');
      $pageLabels->PrintHTML($this->pageBuilder->rootTemplate);
      $this->pageBuilder->rootTemplate->addText($this->userSession->attrs['login'],
						'login');
      // loads user's groups
      $getMemberGroups = $this->LoadModule('UM_GetMemberGroups');
      foreach($getMemberGroups->children as & $child)
	$this->memberGroups[$child->attrs['groupId']]=
	  $child->attrs['groupName'];

      // for admin, show update game option
      if(in_array('Administrators',$this->memberGroups))
      {
	$gameUpdate = $this->pageBuilder->loadTemplate('gameUpdate');
	$pageLabels->PrintHTML($gameUpdate);
	$this->pageBuilder->rootTemplate->addTemplate($gameUpdate,'update');
      }
    }
  }
  

}


?>
