<?php

require_once('ADMIN_Folders.inc');

class adminContentFolders extends ADMIN_Folders
{
  function __construct(& $pageBuilder)
  {
    parent::__construct($pageBuilder,'1','?pageId=adminContentFiles',
			'/content/');
    $this->pageBuilder->rootTemplate->addText('adminContentBody','bodyId');
    $this->pageBuilder->rootTemplate->addText('adminContentFiles','searchTarget');
  }
}
?>
