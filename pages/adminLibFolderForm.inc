<?php

require_once('ADMIN_FolderForm.inc');

class adminLibFolderForm extends ADMIN_FolderForm
{
  function adminLibFolderForm(& $pageBuilder)
  {
    parent::__construct($pageBuilder, '/library/');
  }
}