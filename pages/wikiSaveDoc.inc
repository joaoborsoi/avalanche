<?php

require_once('CMS_Ajax.inc');
require_once('CMS_Notification.inc');

class wikiSaveDoc extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    $_GET['path'] = '/content/Menu/'.$_REQUEST['path'];
    $_GET['userRight'] = 'rw';
    $_GET['groupRight'] = 'rw';
    $_GET['groupId'] = 2;  // Moderators
    $_GET['pendingApproval'] = '0';
    switch($_REQUEST['publish'])
    {
    case 'publish':
      $_GET['otherRight'] = 'r';
      break;
    case 'unpublish':
      $_GET['otherRight'] = '';
      break;
    }

    try
    {
      try
      {
	// for articles, keywords should be required
	if($_REQUEST['keywords'] == NULL)
	  throw new AV_Exception(FIELD_REQUIRED, $this->lang,
				 array('fieldName'=>'keywords',
				       'addMsg'=>' (keywords)'));

	if($_REQUEST['docId'] == NULL)
	{
	  $saveDoc = $this->LoadModule('LIB_InsDoc');
	  $_REQUEST['docId'] = $saveDoc->attrs['docId'];
	}
	else
	  $saveDoc = $this->LoadModule('LIB_SetDoc');
	$not = & new CMS_Notification($this);
	$not->SendNotification($_REQUEST['docId']);

	if($_GET['pendingApproval'])
	{
	  $pageLabels = & $this->LoadModule('LANG_GetPageLabels');
	  $script = "alert('".$pageLabels->attrs['wikiSaveDoc']."');";
	}

	$script .= "window.location='content/".$_REQUEST['docId']."';";
      }
      catch(AV_Exception $e)
      {
	if($e->getCode()==FIELD_REQUIRED)
	{
	  $data = $e->getData();
	  if(!$data['fieldName'])
	    throw $e;
	  $script = "avForm.OnFieldError('".$data['fieldName']."','";
	  $script .= $e->getMessage()."');";
	}
	else
	  $script .= "avForm.OnSubmitError(".json_encode($e->getMessage()).");";
      }
    }
    catch(Exception $e)
    {
      $script .= "avForm.OnSubmitError(".json_encode($e->getMessage()).");";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }

}

?>