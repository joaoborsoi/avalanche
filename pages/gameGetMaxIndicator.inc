<?php

require_once('AV_Page.inc');

class gameGetMaxIndicator extends AV_Page
{
  protected function Header()
  {
    // by default, disable cache
    $this->DisableCache();
    header('Content-Type: text/javascript');
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminScriptSrc');

    $getMaxIndicator = & $this->LoadModule('GAME_GetMaxIndicator');
    $script = "game.yMaxValue='".$getMaxIndicator->attrs['value']."';";
    $this->pageBuilder->rootTemplate->addText($script,'script');
  }
}
