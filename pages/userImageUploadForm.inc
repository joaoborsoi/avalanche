<?php

require_once('AV_Page.inc');

class userImageUploadForm extends AV_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('userImageUploadForm');

    // avalanche version
    $this->pageBuilder->rootTemplate->addText($this->pageBuilder->avVersion,
					      'avVersion');

    $this->pageBuilder->rootTemplate->addText($_REQUEST['fieldName'],
					      'fieldName');

    $pageLabels = $this->LoadModule('LANG_GetPageLabels');
    $pageLabels->PrintHTML($this->pageBuilder->rootTemplate);  
  }
}