<?php

require_once('AV_Page.inc');

class gameChart extends AV_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('chart.tpt');

    $pageLabels = $this->LoadModule('LANG_GetPageLabels');
    $pageLabels->PrintHTML($this->pageBuilder->rootTemplate);
  }
  
}


?>