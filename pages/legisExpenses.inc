<?php

require_once('REST_Page.inc');

class legisExpenses extends REST_Page
{
  protected function Get()
  {
    $getExpenses = $this->LoadModule('LEGIS_GetExpenses');
    return $getExpenses->attrs;
  }
}

?>