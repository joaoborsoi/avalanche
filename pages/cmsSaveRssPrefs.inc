<?php

require_once('CMS_Ajax.inc');

class cmsSaveRssPrefs extends CMS_Ajax
{
  protected function LoadPage()
  {
    try
    {
      // only logged users
      if(!$this->pageBuilder->sessionH->isMember())
	throw new AV_Exception(PERMISSION_DENIED, $this->lang);

      // call parent class
      parent::LoadPage();

      // saves RSS prefs      
      $_GET['userId'] = $this->userSession->attrs['userId'];
      $this->LoadModule('CMS_SaveRssPrefs');
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }

    if($script != NULL)
      $this->pageBuilder->rootTemplate->addText($script,'script');
  }
}

?>