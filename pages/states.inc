<?php

require_once('REST_Page.inc');

class states extends REST_Page
{
  protected function Get()
  {
    return $this->GetStates('/content/Menu');
  }

  protected function GetStates($path, $urlBase = '/area/')
  {
    if($path[mb_strlen($path)-1]!='/')
      $path .= '/';

    $_GET['path'] = $path;
    $_GET['fixedOrder'] = '1';
    $getFolderList = $this->LoadModule('LIB_GetFolderList');

    $results = array();
    foreach($getFolderList->children as & $child)
    {
      $row  = array();
      $name = iconv('utf-8','ascii//TRANSLIT',
		    strtolower($child->attrs['name']));
      $row['name'] = "site.$name";
      $row['url'] = $urlBase.htmlentities(urlencode($name));
      $row['views'] = json_decode($child->attrs['views'],true);
      $row['menuTitle'] = $child->attrs['menuTitle'];
      $row['folderId'] = $child->attrs['folderId'];
      $row['path'] = $path.$child->attrs['name'];
      $results[] = $row;

      $subresults = $this->GetStates($path.$child->attrs['name'],
				     $row['url'].'/');
      if(count($subresults)>0)
	$results = array_merge($results,$subresults);
    }
    return $results;


  }

}

?>