<?php

require_once('REST_Page.inc');

class legisProcessHistory extends REST_Page
{
  protected function Get()
  {
    $_GET['dateFormat'] = '%Y-%m-%d %H:%M:%S';
    $_GET['numberFormat'] = '0';
    $_GET['short'] = '1';
    $getDoc = $this->LoadModule('LEGIS_GetHistory');
    return $this->EncodeModObject($getDoc->children[0]);
  }

  protected function Post()
  {
    $this->LoadModule('LEGIS_NetChecker');
    if($_GET['docId']==NULL)
    {
      $mod = $this->LoadModule('LEGIS_InsHistory');
      $_GET['docId'] = $mod->attrs['docId'];
    }
    else
      $mod = $this->LoadModule('LEGIS_SetHistory');
    $get = $this->Get();

    $get['attrs']['defDocId'] = $get['attrs']['defaultDoc'];
    if($get['attrs']['defaultDoc']!=NULL)
    {
      $_GET['docId'] = $get['attrs']['defaultDoc'];
      $getDoc = $this->LoadModule('LIB_GetDoc');
      $get['attrs']['defDocContentType'] = $getDoc->children[0]->attrs['contentType'];
      $get['attrs']['defDocTitle'] = $getDoc->children[0]->attrs['title'];
    }
    return $get;
  }

  protected function Delete()
  {
    $this->LoadModule('LEGIS_NetChecker');
    $insDoc = $this->LoadModule('LIB_DelDocLink');
    return "";
  }
}