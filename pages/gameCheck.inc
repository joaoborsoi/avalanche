<?php
require_once('AV_Page.inc');

class gameCheck extends AV_Page
{
  protected function LoadPage()
  {
    $_GET['login'] = $this->pageBuilder->siteConfig->getVar("gamecloud",
							    "login");

    $_GET['password'] = $this->pageBuilder->siteConfig->getVar("gamecloud",
							       "password");
    $this->LoadModule('UM_Login');

    $this->Check('GAME_CheckRequested');
    $this->Check('GAME_CheckScheduled');
    $this->Check('GAME_CheckRegistered');
    $this->Check('GAME_CheckStarted');
    $this->Check('GAME_CheckReadyToRun');
    $this->Check('GAME_CheckCancelRequested');
    $this->Check('GAME_CheckFinished');
    $this->Check('GAME_CheckExecuting');
  }

  protected function Check($module)
  {
    $found = true;
    do
    {
      try
      {
	$gameCheck = $this->LoadModule($module);
      }
      catch(AV_Exception $e)
      {
	if($e->getCode() != NOT_FOUND)
	  throw $e;
	$found = false;
      }
    } while($found);
  }

}


?>