<?php

require_once('CMS_Ajax.inc');

class cmsRegister extends CMS_Ajax
{
  protected $pageLabels;

  protected function LoadPage()
  {
    parent::LoadPage();

    session_save_path($this->pageBuilder->siteConfig->getVar("dirs", "session"));
    session_start();
      
    try
    {
     $this->pageLabels = & $this->LoadModule('LANG_GetPageLabels');
     
     $script = $this->Register();
    }
    catch(AV_Exception $e)
    {
      if($e->getCode()==FIELD_REQUIRED ||
      	 $e->getCode()==INVALID_FIELD)
      {
      	$data = $e->getData();
      	if($data['fieldName'])
	{
	  if($data['fieldName']=='email')
	    $data['fieldName']='login';
	  $script = "avForm.OnFieldError('".$data['fieldName']."','";
	  $script .= $e->getMessage()."');";
	}
	else
	  $script .= "avForm.OnSubmitError(".json_encode($e->getMessage()).");";
      }
      else
	$script .= "avForm.OnSubmitError(".json_encode($e->getMessage()).");";
    }
    $this->pageBuilder->rootTemplate->addText($script,'script');
  }



  protected function Register()
  {    
    if($_REQUEST['userId'] == NULL)
    {
      $registerCheckEmail = 
	$this->pageBuilder->siteConfig->getVar("cms",
					       "registerCheckEmail");

      if(empty($_REQUEST['validator']) || $_REQUEST['validator'] != $_SESSION['rand_code']) 
	throw new AV_Exception(INVALID_FIELD, $this->lang,
			       array('fieldName'=>'validator'));

      $_GET['email'] = $_REQUEST['login'];
      $_GET['site'] = 'cmsValidateUser.av';
      $_GET['subject'] = $this->pageLabels->attrs['cmsRegisterSubjectTag'];
      $_GET['mailFrom'] = $this->pageBuilder->siteConfig->getVar("cms", "mailFrom");
      
      $message = '<p>'.$this->pageLabels->attrs['cmsRegisterThankTag'];
      if($registerCheckEmail)
	$message .= ' '.$this->pageLabels->attrs['cmsRegisterMsgTag'];
      $message .= '</p>';

      $_GET['message'] = $message;

      $memberGroups = 
	explode(',', $this->pageBuilder->siteConfig->getVar("cms", "memberGroups"));
      $_GET['groups_numOfOptions'] = count($memberGroups);
      foreach($memberGroups as $key=>$groupId)
	$_GET['groups_'.($key+1).'__value'] = trim($groupId);
      $_GET['status'] = 1; // ativo
      $insTempUser = $this->LoadModule('UM_InsTempUser');
      return 'cms.OnRegisterSuccess("'.$this->pageLabels->attrs['registerSuccessTag'].'");';
    }
    else
    {
      $setUser = $this->LoadModule('UM_SetUser');
      return 'cms.OnRegisterSuccess("'.$this->pageLabels->attrs['accountUpdatedTag'].'");';
    }
 
  }

}

?>