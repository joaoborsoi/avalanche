<?php

require_once('REST_Page.inc');

class legisSearch extends REST_Page
{
  protected function Post()
  {
    $json = $this->pageBuilder->GetJsonInput();

    if($_REQUEST['path']==NULL)
      $_REQUEST['path'] = $json['path'];

    $path = explode('/',$_REQUEST['path']);

    $_GET['dateFormat'] = '%Y-%m-%d %H:%M:%S';
    $_GET['numberFormat'] = '0';
    $_GET['tempTable'] = '0';
    $_GET['detailedFileList'] = '1';
    switch($path[1])
    {
    case 'entry':
      $search = $this->LoadModule('LEGIS_SearchEntry');
      $result = $search->attrs;
      $result['children'] = $this->EncodeModObjectChildren($search);
      break;

    case 'mail':
      $search = $this->LoadModule('LEGIS_SearchMail');
      $result = $search->attrs;
      $result['children'] = $this->EncodeModObjectChildren($search,NULL,function(& $child) {
	  $child->attrs['fromAddress'] = json_decode($child->attrs['fromAddress']);
	  $child->attrs['toAddress'] = json_decode($child->attrs['toAddress']);
	  $child->attrs['ccAddress'] = json_decode($child->attrs['ccAddress']);
	});
      break;

    case 'process':
      $search = $this->LoadModule('LEGIS_SearchProcess');
      $result = $search->attrs;
      $result['children'] = $this->EncodeModObjectChildren($search,NULL,function(& $child) {
	  $child->attrs['number'] = explode(',', $child->attrs['number']);
	});
      break;

    case 'client':
      $search = $this->LoadModule('LEGIS_SearchCustomer');
      $result = $search->attrs;
      $result['children'] = $this->EncodeModObjectChildren($search);
      break;

    default:
      $search = $this->LoadModule('LIB_Search');
      $result = $search->attrs;
      $result['children'] = $this->EncodeModObjectChildren($search);
    }
    return $result;
  }
}