<?php

require_once('SITE_Pages.inc');

class siteLogin extends SITE_Pages
{

  protected function LoadPage()
  {
    parent::LoadPage();
    $this->LoadLogin();
  }

  protected function LoadLogin($message=NULL)
  {
    require_once("SITE_PageClass.inc");
    new page($this, $_GET['docId']);
    
    $js=script('','src=avalanche-'.$this->pageBuilder->avVersion.'/formValidator.js');
    $js.=script('errorMsg="'.$this->labels->attrs['fillTag'].'";errorAlt="'.$this->labels->attrs['errorTag'].'";errorTitle="'.$this->labels->attrs['fieldErrTag'].'";');
    $this->pageBuilder->rootTemplate->addText($js, 'js');
	
    $form = $this->pageBuilder->loadTemplate('siteLoginForm');
    if($_REQUEST['login'] != NULL)
      $form->addText((string)$_REQUEST['login'],
		     'login');
    $this->labels->PrintHTML($form);
    if($message) 
      $form->addText(p($message, 'id=errormsg'), 
		     'message');
    
    $this->wrap->addTemplate($form, 'include');
    
  }

}

?>
