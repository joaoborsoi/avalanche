<?php

require_once('REST_Page.inc');

class legisSearchProcess extends REST_Page
{
  protected function Post()
  {
    $_GET['dateFormat'] = '%Y-%m-%d %H:%M:%S';
    $_GET['numberFormat'] = '0';
    $_GET['tempTable'] = '0';
    $_GET['detailedFileList'] = '1';
    $search = $this->LoadModule('LEGIS_SearchProcess');
    $result = $search->attrs;
    $result['children'] = $this->EncodeModObjectChildren($search,NULL,function(& $child) {
	$child->attrs['number'] = explode(',', $child->attrs['number']);
      });
    return $result;
  }
}