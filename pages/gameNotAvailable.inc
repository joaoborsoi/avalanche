<?php

require_once('AV_Page.inc');

class gameNotAvailable extends AV_Page
{
  protected function LoadPage()
  {
    $serverName = explode('.',$_SERVER['SERVER_NAME']);
    $_GET['docId'] = substr($serverName[0],strlen('game'));
    try
    {
      $getPublicDNSName = $this->LoadModule('GAME_GetPublicDNSName');
      $this->pageBuilder->rootTemplate('gameRedirect');
      $getPublicDNSName->PrintHTML($this->pageBuilder->rootTemplate);
    }
    catch(Exception $e)
    {
      $this->pageBuilder->rootTemplate('gameNotAvailable');
    }
  }

}

?>