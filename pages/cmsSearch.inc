<?php

require_once('CMS_Page.inc');

class cmsSearch extends CMS_Page
{
  protected function LoadContent()
  {
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
								 'dateFormat');
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = $this->pageBuilder->siteConfig->getVar('cms',
								   'searchReturnFields');
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsPerPage');
    $_GET['searchCriteria'] = 'title,content,comment,keywords';

    $_GET['path'] = $this->pageBuilder->siteConfig->getVar('cms','searchPath');

    // keep filter on the search field
    $filter = htmlentities_utf(stripslashes($_REQUEST['filter']));
    $this->pageBuilder->rootTemplate->addText((string)$filter, 'filter');


    // administrators or moderators see all 
    if(!in_array('Moderators',$this->memberGroups) &&
       !in_array('Administrators',$this->memberGroups))
    {
      $_GET['exclusionFilters_numOfOptions'] = 1;
      $_GET['exclusionFilters_1__fieldName'] = 'otherRight';
      $_GET['exclusionFilters_1__value'] = 'r';
    }
    $search = $this->LoadModule('WIKI_Search');

    $content = "<h2 class='subsection'>";
    $content .= $this->pageLabels->attrs['searchResultsTag']."</h2>\n";
    $content .= $this->LoadSearchInfo($search,$_REQUEST['offset'],
				      $_GET['limit']);
    if($search->attrs['totalResults'] > 0)
    {
      $contentModules = array();
      $content .= "<ol class='pair'>";
      foreach($search->children as & $child)
      {
	$content .= "<li>\n";

	// load content module if not loaded yet
	$folderTables = $child->attrs['folderTables'];
	if(!isset($contentModules[$folderTables]))
	{
	  $contentModules[$folderTables] = 
	    & $this->LoadContentModule($folderTables);
	}

	// load list item
	$content .=$contentModules[$folderTables]->LoadListItem($child->attrs);

	$content .= "</li>\n";
      }
      $content .= "</ol>\n";
      $content .= $this->LoadSearchNav($search,$_REQUEST['offset'],
				       $_GET['limit'],'cmsSearch.av',
				       $_REQUEST['filter'],
				       'search='.$_REQUEST['search']);
    }

    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }  

}
