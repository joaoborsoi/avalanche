<?php

require_once('ADMIN_FolderForm.inc');

class adminContentFolderForm extends ADMIN_FolderForm
{
  function adminContentFolderForm(& $pageBuilder)
  {
    parent::__construct($pageBuilder, '/content/');
  }
}