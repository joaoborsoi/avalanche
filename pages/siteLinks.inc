<?php

require_once('SITE_Pages.inc');


class siteLinks extends SITE_Pages
{ 
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { 	
  
     	//$_GET['docId']= $this->sitePages['siteLinks'];
     	parent::LoadPage();
     	
    	
    	require_once("SITE_PageClass.inc");
    	new page($this, $_GET['docId']);
	
	
	// LIST
	$path='/Links/';
	$links=$this->getLinks($path);
	
	$subfolder=$this->getFolders($path);
	//print_r($subfolder->attrs);exit;
	foreach($subfolder as $folder){
		$folderName=$folder->attrs['name'];
		$links.= h3($folderName);
		$links.=$this->getLinks($path.$folderName);
	}
	
	$this->wrap->addText($links, 'content');

  }
  function getLinks($path){
  	// LIST
	$_GET['orderBy']='fixedOrder';
	$_GET['returnFields']='content,title,link';
	$files = $this->getFiles($path);
	$nav=new ul('class=link-list');
	foreach($files->children as $k=>$file){ 
		$name = $file->attrs['title'];
		//$date = $file->attrs['creationDate'];
		$url = trim($file->attrs['link']);
		$descr = $file->attrs['content'];
		$docId = $file->attrs['docId'];
		$http=$url;
		if(mb_substr($url,0,7)!='http://') $http='http://'.$url;
		
		$nav->add_li(a($name, 'href='.$http) .p($descr).small($url));
	}
	return $nav->get_html();
  }
 
}

?>
