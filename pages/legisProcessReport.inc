<?php

require_once('REST_Page.inc');

class legisProcessReport extends REST_Page
{
  protected function Post()
  {
    $data = $this->pageBuilder->GetJsonInput();
    switch($data['type'])
    {
    case 'screen':
      $search = $this->LoadModule('LEGIS_AdvancedProcessSearch');
      $result = $search->attrs;
      $result['children'] = $this->EncodeModObjectChildren($search);
      return $result;
    case 'groupingReport':
      $module = $this->LoadModule('LEGIS_GroupingReport');

      $others = 0;
      $showAll = false;
      $totalResults = count($module->children);
      $maxPercent = 0.01;
      $result = array();
      foreach($module->children as $key=>& $child)
      {
	if($key==0 && $child->attrs['percent'] <= $maxPercent)
	  $showAll = true;
	if(($showAll && $key < 3 || $child->attrs['percent'] > $maxPercent || 
	    $key>=$totalResults-3)  && $key < 10)
	{
	  $result[] = array($child->attrs['title'],(integer)$child->attrs['value']);
	}
	else
	  $others +=  $child->attrs['value'];
      }
      if($others > 0)
	$result[] = array('Outros', $others);

      $ret->attrs = $module->attrs;
      $ret->children = $result;
      return $ret;

    case 'individualReport':
      $module = $this->LoadModule('LEGIS_IndividualReport'); 
      $ret->attrs = $module->attrs;
      return $ret;

    default:
      throw new AV_Exception(INVALID_FIELD,$this->lang);
    }
  }
}