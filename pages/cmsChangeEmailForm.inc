<?php

require_once('CMS_Page.inc');

class cmsChangeEmailForm extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    // first area should be the home
    if(count($getFolderList->children)==1)
      return $getFolderList->children[0]->attrs['name'].'/';
    else
      return parent::GetMenuPath($getFolderList);
  }

  protected function LoadContent()
  {
    $changeEmailForm = &$this->pageBuilder->loadTemplate('cmsChangeEmailForm');

    $this->pageLabels->PrintHTML($changeEmailForm);  

    $this->pageBuilder->rootTemplate->addTemplate($changeEmailForm,
						  'mainContent');

    $changeEmailTag = $this->pageLabels->attrs['changeEmailTag'];
    $this->pageBuilder->rootTemplate->addText($changeEmailTag,'title');

  }
  

}


?>
