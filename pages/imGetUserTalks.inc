<?php 
require_once('AV_Page.inc');

class imGetUserTalks extends AV_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('json');
    try
    {
      $getTalks = $this->LoadModule('IM_GetUserTalks');
      $content = json_encode($getTalks->attrs);
    }
    catch(AV_Exception $e)
    {
      $content = json_encode(array('exception'=>$e->getMessage()));
    }
    $this->pageBuilder->rootTemplate->addText($content,'content');
  }
}

?>