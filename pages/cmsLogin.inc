<?php

require_once('CMS_Ajax.inc');

class cmsLogin extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      try
      {
	$login = $this->LoadModule('UM_Login');
	if($_REQUEST['reload'])
	  $script .= "window.location.reload();";
	else
	  $script .= "window.location = '".$_REQUEST['location']."';";
      }
      catch(AV_Exception $e)
      {
	if($e->getCode()==FIELD_REQUIRED)
	{
	  $data = $e->getData();
	  if(!$data['fieldName'])
	    throw $e;
	  $script = "avForm.OnFieldError('".$data['fieldName']."','";
	  $script .= $e->getMessage()."');";
	}
	else
	  $script .= "avForm.OnSubmitError('".$e->getMessage()."');";
      }
    }
    catch(Exception $e)
    {
      $script = "avForm.OnSubmitError('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }

}

?>