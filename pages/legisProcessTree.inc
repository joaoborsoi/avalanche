<?php

require_once('REST_Page.inc');

class legisProcessTree extends REST_Page
{
  protected function Get()
  {
    $memberGroups = $this->LoadModule('UM_GetMemberGroups');
    $process = $customerSummary = false;
    foreach($memberGroups->children as $group)
    {
      if(in_array('app.process',$group->attrs['permissions']))
	$process = true;
      if(in_array('app.customerSummary',$group->attrs['permissions']))
	$customerSummary = true;
    }

    if(!$process && !$customerSummary)
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    $_GET['customer'] = $process ? '0' : '1';
    $getTree = $this->LoadModule('LEGIS_GetProcessTree');
    return $getTree->attrs['value'];
  }

}