<?php

require_once('CMS_Page.inc');

class cmsPage extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    $menuPath = parent::GetMenuPath($getFolderList);
    if($menuPath == NULL)
    {
      // home page should be public
      $this->public = true;

      // first area should be the home
      return $getFolderList->children[0]->attrs['name'].'/';
    }
    return $menuPath;
  }
}

?>
