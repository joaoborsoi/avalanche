<?php

require_once('CMS_Ajax.inc');
require_once('CMS_Notification.inc');

class wikiPublish extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      if($_REQUEST['flag'])
      {
	$_GET['otherRight'] = 'r';
	$_GET['pendingApproval'] = '0';
      }
      else
	$_GET['otherRight'] = '';

      $moderation = $this->LoadModule('WIKI_Moderation');
      $dateFormat = $this->pageBuilder->siteConfig->getVar('cms','dateFormat');
      $lastChanged = strftime($dateFormat,$moderation->attrs['lastChanged']);
      if($_REQUEST['flag'])
      {
	$script = 'SetPublishBtn('.$_REQUEST['docId'].",'$lastChanged');";
 	$not = & new CMS_Notification($this);
	$not->SendNotification($_REQUEST['docId']);
      }
      else
	$script = 'SetUnpublishBtn('.$_REQUEST['docId'].",'$lastChanged');";
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }

}

?>