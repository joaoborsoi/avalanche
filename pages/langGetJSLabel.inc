<?php

require_once('AV_Page.inc');

class langGetJSLabel extends AV_Page
{
  protected function Header()
  {
    // by default, disable cache
    $this->DisableCache();
    header('Content-Type: text/javascript');
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('adminScriptSrc');

    $_GET['lang'] = $this->lang;
    $getLabel = & $this->LoadModule('LANG_GetLabel');
    $script = 'getLabelRet="'.$getLabel->attrs['value'].'";';
    $this->pageBuilder->rootTemplate->addText($script,'script');
  }
}
