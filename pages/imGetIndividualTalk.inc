<?php 
require_once('AV_Page.inc');

class imGetIndividualTalk extends AV_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('json');
    try
    {
      $getIndividualTalk = $this->LoadModule('IM_GetIndividualTalk');
      $content = json_encode($getIndividualTalk->attrs);
    }
    catch(AV_Exception $e)
    {
      $content = json_encode(array('exception'=>$e->getMessage()));
    }
    $this->pageBuilder->rootTemplate->addText($content,'content');
  }
}

?>