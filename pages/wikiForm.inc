<?php

require_once('CMS_Page.inc');

class wikiForm extends CMS_Page
{
  protected function LoadContent()
  {
    $mainScripts = $this->pageBuilder->loadTemplate('wikiScripts');
    $this->pageBuilder->rootTemplate->addTemplate($mainScripts,'header');

    $articleFormHeader = $this->pageBuilder->loadTemplate('wikiFormHeader');
    $articleFormHeader->addText($this->pageBuilder->avVersion,'avVersion');
    $this->pageBuilder->rootTemplate->addTemplate($articleFormHeader,'header');

    // loads article form and content
    $_GET['path'] = '/content/Menu/'.$this->menuPath;
    $getDoc = $this->LoadModule('LIB_GetDoc');
    
    // specifies items to be printed
    $printItems = & $getDoc->children[0]->printItems;
    $printItems[] = 10; // id
    $printItems[] = 4110; // title
    $printItems[] = 4120; // authors
    $printItems[] = 2080; // keywords
    $printItems[] = 4150; // content
    $printItems[] = 5010; // attach/image manager

    // unset attributes for title and authors
    unset($getDoc->children[0]->children[4110]->attrs['attributes']);
    unset($getDoc->children[0]->children[4120]->attrs['attributes']);

    // for articles, keywords should be required
    $getDoc->children[0]->children[2080]->attrs['xhtmlRequired'] = 
      "class='required'";

    // build html form on root template
    $formHeader = "<form id='articleFormField' action='wikiSaveDoc.av' ";
    $formHeader .= "method='post' accept-charset='UTF-8'>";
    $this->pageBuilder->rootTemplate->addText($formHeader,'formHeader');
    $this->pageBuilder->rootTemplate->addText('</form>','formFooter');

    // loads article form fields template
    $articleForm = & $this->pageBuilder->loadTemplate('wikiForm');
    $this->pageLabels->PrintHTML($articleForm);

    $submit = "<input type='button' value='";
    $status = "<p ><strong>";
    $status .= $this->pageLabels->attrs['statusTag'] . ':</strong> ';
    if($_REQUEST['docId']==NULL ||
       $getDoc->children[0]->attrs['otherRight']=='')
    {
      $submit .= $this->pageLabels->attrs['saveAndPublishTag']."' ";
      $submit .= "onclick='SubmitArticleAndPublish(true);'";
      $status .= "<span id='pubTxt".$fields['docId']."' class='red'>";
      $status .= $this->pageLabels->attrs['notPublishedTag']."</span> ";
    }
    else
    {
      $submit .= $this->pageLabels->attrs['saveAndUnpublishTag']."' ";
      $submit .= "onclick='SubmitArticleAndPublish(false);'";
      $status .= "<span id='pubTxt".$fields['docId']."' class='green'>";
      $status .= $this->pageLabels->attrs['publishedTag']."</span> ";
    }
    $submit .= '/>';
    $status .= '</p>';

    $articleForm->addText($submit,'submitExtra');
    if($_REQUEST['docId'] != NULL)
      $articleForm->addText($status,'status');

    // prints article path
    $articleForm->addText(htmlentities_utf(stripslashes($this->menuPath)),
			  'path');

    // prints article fields to the template
    $getDoc->PrintHTML($articleForm);

    // adds template to the main content
    $this->pageBuilder->rootTemplate->addTemplate($articleForm,'mainContent');
  }
}


?>