<?php

require_once('REST_Page.inc');

class legisCustomerReport extends REST_Page
{
  protected function Post()
  {
    $data = $this->pageBuilder->GetJsonInput();
    switch($data['type'])
    {
    case 'e-mail':
      $_GET['email'] = '1';
    case 'screen':
      $search = $this->LoadModule('LEGIS_AdvancedClientSearch');
      $result = $search->attrs;
      $result['children'] = $this->EncodeModObjectChildren($search);
      return $result;

    default:
      throw new AV_Exception(INVALID_FIELD,$this->lang);
    }
  }
}