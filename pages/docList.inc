<?php

require_once('REST_Page.inc');

class docList extends REST_Page
{
  protected function Post()
  {
    $search = $this->LoadModule('LIB_List');
    return $this->EncodeModObjectChildren($search);
  }
}