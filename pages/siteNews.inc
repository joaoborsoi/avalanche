<?php

require_once('SITE_Pages.inc');


class siteNews extends SITE_Pages
{ 
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { 	
  	
  	
  	if(isset($_GET['docId'])){
  		 
  		parent::LoadPage(); 
  		  		
  		require_once("SITE_PageClass.inc");
    		new page($this, $_GET['docId'], true);
  		//$this->wrap->addText(a('Todas', 'href=siteNews.av'), 'content');
  	}else{
  		  			
	     	$_GET['docId']= $this->sitePages['siteNews'];
	    	parent::LoadPage();
	    	
	    	$this->wrap->addText(a(span('RSS'), 'class=rss&href=siteFeed.av'), 'content');
	    	require_once("SITE_PageClass.inc");
	    	new page($this, $_GET['docId']);

		// LIST
		$_GET['orderBy']='publicationDate';
		$_GET['order']='0';
		$_GET['returnFields']='publicationDate,title,descr';
		$_GET['dateFormat']='%Y-%m-%d';
		if($this->lang=='en_US')$dateFormat='%m/%d/%y';
		else $dateFormat='%d/%m/%y';
		
		$files = $this->getFiles('/Notícias/');//print_r($files->children[0]->attrs);exit;
		$nav=new ul('class=date-list');
		foreach($files->children as $k=>$file){ 
		
			// don't show items in the future
			$now=time();
			$eventTimestamp=strtotime($file->attrs['publicationDate']);
			if($eventTimestamp>$now)continue;
			
			$name = $file->attrs['title'];
			$date = strftime($dateFormat, $eventTimestamp);
			//$author = $file->attrs['authors'];
			if($file->attrs['descr'])$descr = p(nl2br($file->attrs['descr']));
			$docId = $file->attrs['docId'];
		
			$link = 'siteNews/'.$docId.'.av';
			$nav->add_li(h5($date,'class=date').' '.h4(a($name, 'href='.$link)). $descr );
		}
		$this->wrap->addText($nav->get_html(), 'content');
		
	}
	
  }
 
}

?>
