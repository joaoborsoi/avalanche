<?php

require_once('REST_Page.inc');

class languages extends REST_Page
{
  protected function Header()
  {
    header("Expires: ".gmdate("D, d M Y H:i:s",strtotime('+1 day'))." GMT");
    parent::Header();
  }

  protected function Get()
  {
    $getLangList = $this->LoadModule('LANG_GetLangList');
    return $this->EncodeModObjectChildren($getLangList);
  }
}

?>