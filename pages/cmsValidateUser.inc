<?php

require_once('CMS_Page.inc');

class cmsValidateUser extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    // first area should be the home
    if(count($getFolderList->children)==1)
      return $getFolderList->children[0]->attrs['name'].'/';
    else
      return parent::GetMenuPath($getFolderList);
  }

  protected function LoadContent()
  {
    try
    {
      $registerModerator = $this->pageBuilder->siteConfig->getVar("cms",
								  "registerModerator");

      $validateUser = $this->LoadModule('UM_ValidateUser');
      if($registerModerator == NULL)
      {
	$message = $this->pageLabels->attrs['activatedAccountTag'];
	$content = "<p class='alert'>$message</p>.";

	// calls parent function
	$location = $this->pageBuilder->siteConfig->getVar('cms','firstLoginLocation');
	$login = $this->LoadLoginForm(false,$location);
	$login->addText($content,'message');
      }
      else
      {
	$message = $this->pageLabels->attrs['accountValidatedTag'];
	$content = "<p class='alert'>$message</p>.";
	$this->pageBuilder->rootTemplate->addText($content,'mainContent');
      }
    }
    catch(Exception $e)
    {
      $content = "<p class='alert'>".$e->getMessage()."</p>";
      $this->pageBuilder->rootTemplate->addText($content,'mainContent');
    }

  }
}

?>
