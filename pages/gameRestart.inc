<?php

require_once('CMS_Ajax.inc');

class gameRestart extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $_GET['restart'] = '1';
      $this->LoadModule('GAME_Start');
      $script .= "game.Load();";
    }
    catch(AV_Exception $e)
    {
      $script = "game.Alert('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');

  }
  
}

?>