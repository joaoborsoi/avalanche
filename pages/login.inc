<?php

require_once('REST_Page.inc');

class login extends REST_Page
{
  protected function Post()
  {
    $insDoc = $this->LoadModule('UM_Login');

    $memberData = $this->LoadModule('UM_GetMemberData');
    $memberData->attrs['sessionId'] = $this->pageBuilder->sessionH->sessionID;

    $memberGroups = $this->LoadModule('UM_GetMemberGroups');
    $groups = $this->EncodeModObjectChildren($memberGroups);
    $memberData->attrs['groups'] = $groups;

    return $memberData->attrs;
  }
}