<?php

require_once('REST_Page.inc');

class legisLawyerCommitments extends REST_Page
{
  protected function Header()
  {
    header("Expires: ".gmdate("D, d M Y H:i:s",strtotime('+10 minutes'))." GMT");
    parent::Header();
  }

  protected function Get()
  {
    if($this->pageBuilder->sessionH->isGuest())
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    $_GET['userId'] = $this->userSession->attrs['userId'];
    $commitments = $this->LoadModule('LEGIS_GetLawyerCommitments');
    $result = $this->EncodeModObjectChildren($commitments,NULL,function(& $child) {
	$child->attrs['number'] = explode(',', $child->attrs['number']);
      });
    return $result;
  }
}

?>