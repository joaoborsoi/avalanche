<?php

require_once('REST_Page.inc');

class user extends REST_Page
{
  protected function Get()
  {
    $_GET['dateFormat'] = '%Y-%m-%d %H:%M:%S';
    $_GET['numberFormat'] = '0';
    $_GET['short'] = '1';
    $getUser = $this->LoadModule('ADMIN_GetUserForm');
    return $this->EncodeModObject($getUser->children[0]);
  }

  protected function Post()
  {
    if($_GET['userId']==NULL)
    {
      $mod = $this->LoadModule('UM_InsUser');
      $_GET['userId'] = $mod->attrs['userId'];
    }
    else
      $mod = $this->LoadModule('UM_SetUser');
    return $this->Get();
  }

  protected function Delete()
  {
    $insDoc = $this->LoadModule('UM_DelUser');
    return "";

  }

}