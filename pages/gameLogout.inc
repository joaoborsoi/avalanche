<?php

require_once('CMS_Ajax.inc');

class gameLogout extends CMS_Ajax
{
  protected function LoadPage()
  {
    $this->pageBuilder->sessionH->attemptLogout();
    parent::LoadPage();
    $script = "location.reload();";
    $this->pageBuilder->rootTemplate->addText($script,'script');

  }
  
}

?>