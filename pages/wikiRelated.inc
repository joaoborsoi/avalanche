<?php

require_once('CMS_Page.inc');
require_once('WIKI_Content.inc');

class wikiRelated extends CMS_Page
{
  // redefine default search load
  protected function LoadContent()
  {
    // search for files at the folder
    $_GET['dateFormat'] = $this->pageBuilder->siteConfig->getVar('cms',
								 'dateFormat');
    $_GET['path'] = '/content/Menu/Wiki';
    $_GET['orderBy'] = 'matches';
    $_GET['filter'] = $_REQUEST['filter'];
    $_GET['searchCriteria'] = 'keywords';
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = 'authors,lastChanged,title';
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'resultsPerPage');

    $_GET['exclusionFilters_numOfOptions'] = 2;
    $_GET['exclusionFilters_1__fieldName'] = 'otherRight';
    $_GET['exclusionFilters_1__value'] = 'r';
    $_GET['exclusionFilters_2__fieldName'] = 'docId';
    $_GET['exclusionFilters_2__value'] = '!'.$_REQUEST['sourceId'];

    $content = "<h2 class='subsection'>";
    $content .= $this->pageLabels->attrs['relatedDocumentsTag']."</h2>\n";

    $search = & $this->LoadModule('LIB_Search');
    $content .= $this->LoadSearchInfo($search,$_REQUEST['offset'],
				      $_GET['limit']);
    if($search->attrs['totalResults']>0)
    {
      $data = 'sourceId='.$_REQUEST['sourceId'];

      $wikiContent = & new WIKI_Content($this);

      $content .= $wikiContent->LoadArticleList($search);
      $content .= $this->LoadSearchNav($search,$_REQUEST['offset'],
				       $_GET['limit'],"wikiRelated.av",
				       $_REQUEST['filter'],$data);
    }
     // loads content to the page
    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }
}
