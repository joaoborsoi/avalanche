<?php

require_once('CMS_Ajax.inc');

class gameLoadIcons extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();
    $loadIcons = $this->LoadModule('GAME_LoadIcons');

    $divList = array();
    $typeList = array();

    foreach($loadIcons->children as & $icon)
    {
      if(!isset($typeList[$icon->attrs['elementClass']]))
      {
	require_once($icon->attrs['elementClass'].'.inc');
	$typeList[$icon->attrs['elementClass']] = 
	  new $icon->attrs['elementClass']($this);
      }

      $divList[$icon->attrs['divId']] .= 
	$typeList[$icon->attrs['elementClass']]->LoadIcon($icon->attrs);
    }

    foreach($divList as $divId=> & $html)
      $script .= "$(\"#$divId\").html(\"$html\");";

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }
  
}

?>