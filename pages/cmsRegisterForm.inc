<?php

require_once('CMS_Page.inc');

class cmsRegisterForm extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    // first area should be the home
    if(count($getFolderList->children)==1)
      return $getFolderList->children[0]->attrs['name'].'/';
    else
      return parent::GetMenuPath($getFolderList);
  }

  protected function LoadContent()
  {
    $registerForm = &$this->pageBuilder->loadTemplate('cmsRegisterForm');
    $this->pageBuilder->rootTemplate->addTemplate($registerForm,'mainContent');

    $registerTag = $this->pageLabels->attrs['registerTag'];
    $this->pageBuilder->rootTemplate->addText($registerTag,'title');

    $this->pageLabels->PrintHTML($registerForm);  

    $_GET['userId'] = $this->userSession->attrs['userId'];
    if($_GET['userId']!=NULL)
      $registerForm->addText($_GET['userId'],'userId');
    $getUser = $this->LoadModule('ADMIN_GetUserForm');

    // remove some fields
    unset($getUser ->children[0]->children[1000]); // status
    unset($getUser ->children[0]->children[1010]); // groups
    
    // prints article fields to the template
    $getUser->PrintHTML($registerForm);
  }
  

}


?>
