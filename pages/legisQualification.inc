<?php

require_once('REST_Page.inc');

class legisQualification extends REST_Page
{
  protected function Post()
  {
    $getQual = $this->LoadModule('LEGIS_GetQualification');
    return $getQual->attrs;
  }
}

?>