<?php

require_once('AV_Page.inc');

class gameChartData extends AV_Page
{
  protected function Header()
  {
    // by default, disable cache
    $this->DisableCache();
    header('Content-type: text/csv');
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('chartData');
    $indSeries = $this->LoadModule('GAME_GetIndicatorSeries','chartDataItem',
				   'chartDataItem');
    $indSeries->PrintHTML($this->pageBuilder->rootTemplate);
  }
}
