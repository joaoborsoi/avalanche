<?php

require_once('REST_Page.inc');

class cmsContact extends REST_Page
{
  protected function Post()
  {
    session_save_path($this->pageBuilder->siteConfig->getVar("dirs","session"));
    session_start();

    $jsonInput = $this->pageBuilder->GetJsonInput();

    // check captcha
    if(empty($jsonInput['validator']) || 
       $jsonInput['validator'] != $_SESSION['rand_code'])
      throw new AV_Exception(INVALID_FIELD, $this->lang,
			     array('fieldName'=>'validator'));

    // search for files at the folder
    $_GET['path'] = '/content/';
    $_GET['returnFields'] = 'contactMailTo';
    $_GET['limit'] = 1;
    $search = $this->LoadModule('LIB_Search');
      
    if(count($search->children)==0)
      throw new AV_Exception(NOT_FOUND, $this->lang);

    $_GET['mailTo'] = $search->children[0]->attrs['contactMailTo'];
    $this->LoadModule('CMS_SendContact');
  }
}

?>
