<?php

require_once('SITE_Pages.inc');


class siteForm extends SITE_Pages
{ 
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { 	

  	//$_GET['docId']= $this->sitePages['siteForm'];
  	parent::LoadPage();
    	
    	require_once("SITE_PageClass.inc");
    	new page($this, $_GET['docId']);

	// FORM
	$js=script('','src=avalanche-'.$this->pageBuilder->avVersion.'/formValidator.js');
	$js.=script('errorMsg="'.$this->labels->attrs['fillTag'].'";errorAlt="'.$this->labels->attrs['errorTag'].'";errorTitle="'.$this->labels->attrs['fieldErrTag'].'";');
	$this->pageBuilder->rootTemplate->addText($js, 'js');	
	$form = $this->pageBuilder->loadTemplate('siteRegisterForm-'.$this->lang);
	//$this->labels->PrintHTML($form);
	$this->wrap->addTemplate($form, 'include');
	
  }
 
}

?>
