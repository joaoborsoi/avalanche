<?php

require_once('AV_Page.inc');
require_once('CMS_RSSEvents.inc');

class cmsFeed extends AV_Page
{
  protected function Header()
  {
    $this->DisableCache();
    header('Content-type: text/xml; charset=utf-8');
  }

  protected function &LoadContentModule($folderTables)
  {
    // loads document data to the page
    $_GET['folderTables'] = $folderTables;
    $getContentModule = $this->LoadModule('LIB_GetContentModule');

    require_once($getContentModule->attrs['contentModule'].'.inc');
    return  new $getContentModule->attrs['contentModule']($this);
  }

  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('cmsFeed.tpt');     

    $public = $this->pageBuilder->siteConfig->getVar('cms','public');


    $_GET['lang'] = $this->lang;
    $this->pageLabels = & $this->LoadModule('LANG_GetPageLabels');
    $this->pageLabels->PrintHTML($this->pageBuilder->rootTemplate);

    // tries to login
    try
    {
      if(isset($_SERVER['PHP_AUTH_USER']))
      {
	$_GET['login'] = $_SERVER['PHP_AUTH_USER'];
	$_GET['password'] = $_SERVER['PHP_AUTH_PW'];
	$login = $this->LoadModule('UM_Login');
	$_GET['userId'] = $login->attrs['userId'];
      }
      else
	$_GET['userId'] = $this->userSession->attrs['userId'];

      // not allowed public access
      if($this->pageBuilder->sessionH->isGuest() && !$public)
      {
	header('WWW-Authenticate: Basic realm="RSS Restrito"');
	header('HTTP/1.0 401 Unauthorized');
	throw new AV_Exception(PERMISSION_DENIED, $this->lang);
      }
    }
    catch(AV_Exception $e)
    {
      $content = "<item>\n";
      $content .= "<title>".$e->getMessage()."</title>\n";
      $content .= "</item>\n";
      $this->pageBuilder->rootTemplate->addText($content,'feedItem');
      return;
    }


    // sets site's base
    $base = dirname($_SERVER['PHP_SELF']);
    if($base!='/') 
      $base .= '/';
    $base = 'http://'.$_SERVER['HTTP_HOST'].$base;
    $this->pageBuilder->rootTemplate->addText($base,'link');

    // feed language
    $feedLang = strtolower(str_replace('_','-',$this->pageBuilder->lang));
    $this->pageBuilder->rootTemplate->addText($feedLang,'feedLang');

    // load preferences
    $this->pageBuilder->SetLang('en_US');
    if(!$this->pageBuilder->sessionH->isGuest())
    {
      $rssPrefs = $this->LoadModule('CMS_GetRssPrefs');
      $events = explode(',',$rssPrefs->children[0]->attrs['events']);
      foreach($events as $event)
      {
	switch($event)
	{
	case RSS_UPDATES:
	  $updates = true;
	  break;
	case RSS_COMMENTS:
	  $comments = true;
	  break;
	}
      }
    }

    // sets search parameters
    $_GET['path'] = addslashes(implode(',',
			       $rssPrefs->children[0]->attrs['folderList']));
    
    $_GET['recursiveSearch'] = '1';
    $_GET['returnFields'] = 'lastChanged,contentType';
    $_GET['returnFields'] .= ',title,content';

    if($comments)
    {
      $_GET['path'] .= ',/comments/';
      $_GET['returnFields'] .= ',comment,articleId';
    }

    if($updates)
    {
      $_GET['returnFields'] .= ',publicationDate';
      $_GET['orderBy'] = 'lastChanged';
    }
    else
      $_GET['orderBy'] = 'publicationDate';

    $_GET['order'] = 0;
    $_GET['limit'] = $this->pageBuilder->siteConfig->getVar('cms',
							    'feedMaxItems');
    $_GET['dateFormat'] = '%a, %d %b %Y %H:%M:%S %z';
    $_GET['exclusionFilters_numOfOptions'] = 1;
    $_GET['exclusionFilters_1__fieldName'] = 'otherRight';
    $_GET['exclusionFilters_1__value'] = 'r';

    if($comments)
      $search = $this->LoadModule('WIKI_Search');
    else
      $search = $this->LoadModule('LIB_Search');

    $contentModules = array();
    
    foreach($search->children as & $child)
    {
      $content .= "<item>\n";

      // load content module if not loaded yet
      $folderTables = $child->attrs['folderTables'];
      if(!isset($contentModules[$folderTables]))
      {
	$contentModules[$folderTables] = 
	  & $this->LoadContentModule($folderTables);
      }

      // load feed item
      $content .=$contentModules[$folderTables]->LoadFeedItem($child->attrs,
							      $base);

      $content .= "</item>\n";
    }
    
    if($content != NULL)
      $this->pageBuilder->rootTemplate->addText($content,'feedItem');
  }
}
?>