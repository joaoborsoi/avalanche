<?php

require_once('AV_Page.inc');

class cmsCaptcha extends AV_Page
{
  // prevents default header
  protected function Header()
  {
  }

  protected function LoadPage()
  {
    session_save_path($this->pageBuilder->siteConfig->getVar("dirs", "session"));
    session_start();

    unset($_SESSION['rand_code']);
    if (empty($_SESSION['rand_code']))
    {
      $str = "";
      $length = 0;
      for ($i = 0; $i < 4; $i++)
      {
        // this numbers refer to numbers of the ascii table (small-caps)
        $str .= chr(rand(97, 122));
      }
      $_SESSION['rand_code'] = $str;
    }

    $imgX = 60;
    $imgY = 19;
    $image = imagecreatetruecolor(60, 26);

    $backgr_col = imagecolorallocate($image, 238,238,238);
    $border_col = imagecolorallocate($image, 238,238,238);
    $text_col = imagecolorallocate($image, 100,100,100);

    imagefilledrectangle($image, 0, 0, 60, 25, $backgr_col);
    imagerectangle($image, 0, 0, 59, 25, $border_col);

    // it's a Bitstream font check www.gnome.org for more
    $font = $this->pageBuilder->siteConfig->getVar("dirs", "home") . "captcha.ttf";

    $font_size = 19;
    $angle = 0;
    $box = imagettfbbox($font_size, $angle, $font, $_SESSION['rand_code']);
    $x = (int)($imgX - $box[4]) / 2;
    $y = (int)($imgY - $box[5]) / 2;
    imagettftext($image, $font_size, $angle, $x, $y, $text_col, $font, $_SESSION['rand_code']);
    //imageline ($image,0,rand(0,$imgY),$imgX,rand(0,$imgY), $border_col);

    header("Content-type: image/png");
    imagepng($image);
    imagedestroy ($image);
  }
}
?>
