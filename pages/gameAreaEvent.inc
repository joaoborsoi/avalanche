<?php

require_once('CMS_Ajax.inc');

class gameAreaEvent extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $loadArea = $this->LoadModule('GAME_LoadAreaEvent');
      if($loadArea->attrs['areaElement'] != NULL)
      {
	$script = "popup.Open('gameArea.av?id=".$loadArea->attrs['areaElement'];
	$script .= "','".$loadArea->attrs['targetDiv']."',null,'";
	$script .= $loadArea->attrs['type']."',";
	$script .= ($loadArea->attrs['onLoadSuccess']!=NULL)?
	  $loadArea->attrs['onLoadSuccess']:'null';
	$script .= ',game.NextAreaEvent)';
      }
      else
	$script .= "game.EnableControls();";
    }
    catch(AV_Exception $e)
    {
      if($e->getCode()!=NOT_FOUND)
	$script .= "game.Alert('".$e->getMessage()."');";
      else
	$script .= "game.EnableControls();";
    }
    if($script!=NULL)
      $this->pageBuilder->rootTemplate->addText($script,'script');
  }
  
}

?>