<?php

require_once('ADMIN_Files.inc');

class adminContentFiles extends ADMIN_Files
{
  function __construct(& $pageBuilder)
  {
    parent::__construct($pageBuilder,1,'/content/');
    $this->pageBuilder->rootTemplate->addText('adminContentBody','bodyId');
  }
}
?>
