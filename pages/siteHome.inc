<?php

require_once('SITE_Pages.inc');


class siteHome extends SITE_Pages
{ 
  //
  //--Private
  //

  //--Method: LoadPage
  //--Desc: Redefines parent method to specific template managment.
  function LoadPage()
  { 	
  	
  	parent::LoadPage();
  
  	// common
  	if($this->lang=='en_US')$dateFormat="%b %d";
  	else $dateFormat="%d/%m/%y";
	$_GET['dateFormat']='%Y-%m-%d';
	$sBox=new div('class=sbox');
	
	// EVENTS
	
	$_GET['orderBy']='eventDate';
	$_GET['returnFields']='eventDate,title';
	$_GET['limit']= $this->pageBuilder->siteConfig->getVar("events", "homeMaxItems");
	$files = $this->getFiles('/Eventos/');
	$control=0;
	$ul=new ul('id=events-box&class=date-list');
	foreach($files->children as $k=>$file){ 
		// don't show items in the past
		$now=strtotime("-24 hours");
		$eventTimestamp=strtotime($file->attrs['eventDate']);
		if($eventTimestamp<$now)continue;
	
		$name = $file->attrs['title'];
		$date = strftime($dateFormat, $eventTimestamp);
		$docId = $file->attrs['docId'];
		$newsImage='';
		$link = 'siteEvents/'.$docId.'.av';
		$ul->add_li($newsImage.' '.h5($date,'class=date').' '.h4(a($name, 'href='.$link)));
		$control++;
		if($control>=$limit)break;
	}
	if(!$ul->empty){
		$sBox->add(h3($this->labels->attrs['homeEventsTag']));
		$sBox->add($ul->get_html());
	}
	
	
	// NEWS

	$showImage=$this->pageBuilder->siteConfig->getVar("news", "homeShowImage");
	$_GET['orderBy']='publicationDate';
	$_GET['order']='0';
	$_GET['limit']= $this->pageBuilder->siteConfig->getVar("news", "homeMaxItems");
	$_GET['returnFields']='publicationDate,title';

	$files = $this->getFiles('/Notícias/');
	array_reverse($files);
	$ul=new ul('id=news-box&class=date-list');
	foreach($files->children as $k=>$file){ 
		// don't show items in the future
		$now=time();
		$eventTimestamp=strtotime($file->attrs['publicationDate']);
		if($eventTimestamp>$now)continue;
	
		$name = $file->attrs['title'];
		$date = strftime($dateFormat, $eventTimestamp);
		$docId = $file->attrs['docId'];
		$newsImage='';
		/*if($showImage){
			$docIdList=explode(",",$file->attrs['imageList']);
			if($docIdList[0]) $newsImage=img('src=thumb/'.$docIdList[0]);
		}*/
		$link = 'siteNews/'.$docId.'.av';
		$ul->add_li($newsImage.' '.h5($date,'class=date').' '.h4(a($name, 'href='.$link)));
	}
	if(!$ul->empty){
		$sBox->add(a(span('RSS'), 'class=rss&href=siteFeed.av'));
		$sBox->add(h3($this->labels->attrs['homeNewsTag']));
		$sBox->add($ul->get_html());
	}
	
	
	if(!$sBox->empty)$this->wrap->addText($sBox->get_html(), 'include');
	
	
	
	// TEXT
		
    	require_once("SITE_PageClass.inc");
    	new page($this, $_GET['docId'], false, false, 'homeContent');
    	
    	$this->setPageTitle ($this->siteName,true);
    	
  }
 
}

?>
