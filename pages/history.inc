<?php

require_once('REST_Page.inc');

class history extends REST_Page
{
  protected function Get()
  {
    $search = $this->LoadModule('LIB_History');
    return $this->EncodeModObjectChildren($search);
  }
}