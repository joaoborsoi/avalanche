<?php

require_once('CMS_Ajax.inc');

class cmsRegisterFormField extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $getUser = $this->LoadModule('ADMIN_GetUserForm');
      $loadField = & $this->pageBuilder->loadTemplate('loadField');
      $getUser->PrintHTML($loadField);
      $this->pageBuilder->rootTemplate->addTemplate($loadField,'script');
    }
    catch(Exception $e)
    {
      $script = "avForm.Alert('".$e->getMessage()."');";
      $this->pageBuilder->rootTemplate->addText($script,'script');
    }
  }
}

?>