<?php 
require_once('AV_Page.inc');

class imGetTalkMessages extends AV_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('json');
    try
    {
      $getTalk = $this->LoadModule('IM_GetTalkMessages');
      $content = json_encode($getTalk->attrs);
    }
    catch(AV_Exception $e)
    {
      $content = json_encode(array('exception'=>$e->getMessage()));
    }
    $this->pageBuilder->rootTemplate->addText($content,'content');
  }
}

?>