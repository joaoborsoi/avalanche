<?php

require_once('REST_Page.inc');

class folders extends REST_Page
{
  private $expiration = NULL;

  protected function Header()
  {
    if($this->expiration !=NULL)
      header("Expires: ".gmdate("D, d M Y H:i:s",strtotime($this->expiration))." GMT");
    parent::Header();
  }

  protected function Get()
  {
    if($_REQUEST['folderId']=='*')
    {
      $getFolderList = $this->LoadModule('LIB_GetFolderList');
      return $this->EncodeModObjectChildren($getFolderList,NULL,function(&$child){
	  $child->attrs['views'] = json_decode($child->attrs['views']);
	});
    }

    $_GET['dateFormat'] = '%Y-%m-%d %H:%M:%S';
    $_GET['numberFormat'] = '0';
    $_GET['short'] = '1';
    $getFolder = $this->LoadModule('LIB_GetFolder');

    if($_REQUEST['folderId']==NULL)
      $this->expiration = '+1 year';

    return $this->EncodeModObject($getFolder->children[0]);
  }

  protected function Post()
  {
    if($_GET['folderId']==NULL)
    {
      $mod = $this->LoadModule('LIB_InsFolder');
      $_GET['folderId'] = $mod->attrs['folderId'];
    }
    else
      $mod = $this->LoadModule('LIB_SetFolder');
    return $this->Get();
  }

  protected function Delete()
  {
    $this->LoadModule('LIB_DelFolder');
    return "";

  }
}