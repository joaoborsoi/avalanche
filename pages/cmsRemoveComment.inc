<?php

require_once('CMS_Ajax.inc');

class cmsRemoveComment extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    if(!in_array('Administrators',$this->memberGroups))
      throw new AV_Exception(PERMISSION_DENIED, $this->lang);

    try
    {
      $_GET['path'] = '/comments/';
      $delDoc = $this->LoadModule('LIB_DelDocLink');
      $script = "alert('Comment has been removed!');";
      $script .= "window.location.reload()";
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }

}

?>