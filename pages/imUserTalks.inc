<?php 
require_once('AV_Page.inc');

class imUserTalks extends AV_Page
{
  protected function LoadPage()
  {
    $this->pageBuilder->rootTemplate('json');
    $this->LoadModule('IM_GetUserTalks');
  }
}

?>