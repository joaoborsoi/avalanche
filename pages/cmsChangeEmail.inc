<?php

require_once('CMS_Ajax.inc');

class cmsChangeEmail extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $this->pageLabels = & $this->LoadModule('LANG_GetPageLabels');

      try
      {
	$changeEmail = $this->LoadModule('UM_ChangeMemberEmail');
	$this->CheckEmail($_REQUEST['email'],$changeEmail->attrs['id']);
	$script = "cms.OnChangeEmailSuccess('".$this->pageLabels->attrs['checkEmailSentTag']."');";
      }
      catch(AV_Exception $e)
      {
	if($e->getCode()==FIELD_REQUIRED)
	{
	  $data = $e->getData();
	  if(!$data['fieldName'])
	    throw $e;
	  $script = "avForm.OnFieldError('".$data['fieldName']."','";
	  $script .= $e->getMessage()."');";
	}
	else
	  $script .= "avForm.OnSubmitError('".$e->getMessage()."');";
      }
    }
    catch(Exception $e)
    {
      $script = "avForm.OnSubmitError('".$e->getMessage()."');";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');

  }

  protected function CheckEmail($mailTo,$id)
  {
    $mailFrom = $this->pageBuilder->siteConfig->getVar("cms", "mailFrom");
    $headers = "From: $mailFrom\r\n";
    $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";

    $link = dirname($_SERVER['PHP_SELF']);
    if($link!='/') 
      $link .= '/';
    $link = 'http://'.$_SERVER['HTTP_HOST'].$link;
    $link .= 'cmsValidateEmail.av?id='.$id;

    $pageLabels = & $this->LoadModule('LANG_GetPageLabels');

    $subjectPrefix = $this->pageBuilder->siteConfig->getVar("cms", 
							    "subjectPrefix");
    $subject = $subjectPrefix . ' ' . $pageLabels->attrs['emailValidationTag'];
    $message = "<!DOCTYPE html PUBLIC \"-";
    $message .= "//W3C//DTD XHTML 1.0 Strict//EN\" ";
    $message .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
    $message .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>";
    $message .= '<p>'.$pageLabels->attrs['emailValidationTxtTag'].'</p>';
    $message .= "<p><a href='$link'>$link</a></p>";
    $message .= '</body></html>';

    if(!mail($mailTo, $subject, $message, $headers ,"-r".$mailFrom))
    { 
      // Se "não for Postfix"
      $headers .= "Return-Path: " . $mailFrom . "\r\n"; 
      if(!mail($mailTo, $subject, $message, $headers))
      {
	$msg = " ".$pageLabels->attrs['sendEmailErrorTag'];
	throw new AV_Exception(FAILED,$this->lang,array('addMsg'=>$msg));
      }
    }
  }

}

?>