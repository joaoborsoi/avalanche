<?php

require_once('REST_Page.inc');

class legisProcessReportForm extends REST_Page
{
  protected function Get()
  {
    $_GET['dateFormat'] = '%Y-%m-%d %H:%M:%S';
    $_GET['numberFormat'] = '0';
    $_GET['short'] = '1';
    $getDoc = $this->LoadModule('LIB_GetDoc');

    if($_REQUEST['docId']==NULL)
      $this->expiration = '+1 year';

    return $this->EncodeModObject($getDoc->children[0]);
  }

}