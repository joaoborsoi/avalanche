<?php

require_once('legisDocuments.inc');

class legisRepresentative extends legisDocuments
{
  protected function Get()
  {
    $_GET['dateFormat'] = '%Y-%m-%d %H:%M:%S';
    $_GET['numberFormat'] = '0';
    $_GET['short'] = '1';
    $getDoc = $this->LoadModule('LEGIS_GetRepresentative');

    if($_REQUEST['docId']==NULL)
      $this->expiration = '+1 year';

    return $this->EncodeModObject($getDoc->children[0]);
  }

}

?>