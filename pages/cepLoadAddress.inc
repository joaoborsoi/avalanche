<?php

require_once('CMS_Ajax.inc');

class cepLoadAddress extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $loadAddress = $this->LoadModule('CEP_LoadAddress');
      $script = 'avForm.LoadAddress('.$loadAddress->attrs['uf_codigo'].',';
      $script .= $loadAddress->attrs['cidade_codigo'].",\"";
      $script .= $loadAddress->attrs['bairro_descricao']."\",\"";
      $script .= $loadAddress->attrs['endereco_logradouro']."\");";
    }
    catch(Exception $e)
    {
      $script = 'avForm.EnableAddress();';
      $script .= "avForm.Alert('".$e->getMessage()."');";
    }
    $this->pageBuilder->rootTemplate->addText($script,'script');
  }

}

?>