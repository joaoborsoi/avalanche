<?php

require_once('REST_Page.inc');

class cep extends REST_Page
{
  protected function Get()
  {
    $cepLoad = $this->LoadModule('CEP_LoadAddress');
    return $cepLoad->attrs;
  }
}