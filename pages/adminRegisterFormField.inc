<?php

require_once('CMS_Ajax.inc');

class adminRegisterFormField extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    try
    {
      $getUser = $this->LoadModule('ADMIN_GetUserForm');
      $loadField = & $this->pageBuilder->loadTemplate('loadField');
      $getUser->PrintHTML($loadField);
      $this->pageBuilder->rootTemplate->addTemplate($loadField,'script');
    }
    catch(Exception $e)
    {
      $script = "alert('".$e->getMessage()."');";
      $this->pageBuilder->rootTemplate->addText($script,'script');
    }

  }

}

?>