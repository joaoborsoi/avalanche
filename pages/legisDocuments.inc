<?php

require_once('documents.inc');

class legisDocuments extends documents
{
  protected function Post()
  {
    $this->LoadModule('LEGIS_NetChecker');
    return parent::Post();
  }

  protected function Delete()
  {
    $this->LoadModule('LEGIS_NetChecker');
    return parent::Delete();
  }
}

?>