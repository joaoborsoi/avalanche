<?php

require_once('ADMIN_Files.inc');

class adminLibFiles extends ADMIN_Files
{
  function __construct(& $pageBuilder)
  {
    parent::__construct($pageBuilder,0,'/library/');
    $this->pageBuilder->rootTemplate->addText('adminLibBody','bodyId');
  }
}

?>
