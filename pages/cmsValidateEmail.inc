<?php

require_once('CMS_Page.inc');

class cmsValidateEmail extends CMS_Page
{
  protected function GetMenuPath(& $getFolderList)
  {
    // first area should be the home
    if(count($getFolderList->children)==1)
      return $getFolderList->children[0]->attrs['name'].'/';
    else
      return parent::GetMenuPath($getFolderList);
  }

  protected function LoadContent()
  {
    if($this->pageBuilder->sessionH->isGuest())
    {
      $this->LoadLoginForm();
      return;
    }
   
    $content .= "<p class='alert'>";
    try
    {
      $_GET['emailFieldName'] = 'login';
      $validateEmail = $this->LoadModule('UM_ValidateNewEmail');
      $content .= $this->pageLabels->attrs['emailChangedMsgTag'].' <strong>';
      $content .= $validateEmail->attrs['email'].'</strong>';
    }
    catch(Exception $e)
    {
      $content .= $e->getMessage();
    }
    $content .= '</p>';
    $this->pageBuilder->rootTemplate->addText($content,'mainContent');
  }
  

}


?>
