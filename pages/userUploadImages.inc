<?php

require_once('AV_Page.inc');

class userUploadImages extends AV_Page
{
  protected function LoadPage()
  {

    $this->pageBuilder->rootTemplate('adminScript');
    
    // ajusta variaveis de entrada
    $_GET['path'] = '/tmpUserImages/';
    $_GET['title_numOfOptions'] = 1;
    $_GET['descr_numOfOptions'] = 1;
    $_GET['title_1__lang'] = 'pt_BR';
    $_GET['descr_1__lang'] = 'pt_BR';
    $error = false;
    $sourceFileVar = 'sourceFileName';
    try
    {
      if(isset($_FILES[$sourceFileVar]) &&
	 $_FILES[$sourceFileVar]['name'] != NULL)
      {
	$_GET['title_1__value'] = $sourceFileVar['name'];
	$_GET['descr_1__value'] = '';
	$_GET['content'] = $_FILES[$sourceFileVar];
	$module = $this->LoadModule('LIB_InsDoc');
	$data['fieldName'] = $_REQUEST['fieldName'];
	$data['docId'] = $module->attrs['docId'];
	$data['title'] = $_REQUEST['title'];
	$data['type'] = $_FILES[$sourceFileVar]['type'];
	$script = 'parent.SetUploadSuccess('.json_encode($data).');';
      }
      else
	throw new AV_Exception(FIELD_REQUIRED,$this->lang,
			       array('fieldName'=>'sourceFileName'));
    }
    catch(Exception $e)
    {
      $script .= "parent.SetUploadError(".json_encode($e->getMessage()).");";
    }
    $this->pageBuilder->rootTemplate->addText($script, 'script');
  }
}