<?php

require_once('REST_Page.inc');

class logout extends REST_Page
{
  protected function Post()
  {
    $this->LoadModule('UM_Logout');
    return "";
  }
}