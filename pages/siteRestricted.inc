<?php

require_once('siteLogin.inc');

class siteRestricted extends siteLogin
{
  // sobrescreve ação padrão siteLogin e evita
  // de carregar o formulário de login antes de 
  // tentar efetuar as operações
  protected function LoadPage()
  {
    // carrega LoadPage do SITE_Pages
    SITE_Pages::LoadPage();

    try
    {
      $this->LoadModule('UM_Login');
    }
    catch(Exception $e)
    {
      if($e->getCode()!=PERMISSION_DENIED)
	$this->LoadLogin($e->getMessage());
      else
	$this->LoadLogin($this->labels->attrs['siteLoginErrorTag']);
      return;
    }

    $this->UpdateUserSession();
    if($this->userSession == NULL)
    {
      $this->LoadLogin($this->labels->attrs['siteLoginErrorTag']);
      return;
    }

    try
    {
      $_GET['docId']=$this->GetClientPage();
      require_once("SITE_PageClass.inc");
      new page($this, $_GET['docId']);
    }
    catch(Exception $e)
    {
      $this->LoadLogin($e->getMessage());
    }
  }
  
  function GetClientPage()
  {
    // client page success
    $userRootPath = '/Clientes/';
    $userpath = $userRootPath . 
      $this->userSession->attrs['login'];
    try
    {
      $files = $this->getFiles($userpath);
      if(isset($files->children[0]))
	return $files->children[0]->attrs['docId'];
    }
    catch(Exception $e)
    {
      switch($e->getCode())
      {
      case INVALID_FIELD: case INVALID_PATH:
	  break;
      default:
	throw $e;
      }
    }

    $files = $this->getFiles($userRootPath);
    if(isset($files->children[0]))
      return $files->children[0]->attrs['docId'];

    throw new AV_Exception(NOT_FOUND,
			   $this->pageBuilder->lang);
  }

  protected function LoadLogin($message=NULL)
  {
    $_GET['docId']= $this->sitePages['siteLogin'];
    parent::LoadLogin($message);
  }
}

?>
