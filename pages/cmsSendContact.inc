<?php

require_once('CMS_Ajax.inc');

class cmsSendContact extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    session_save_path($this->pageBuilder->siteConfig->getVar("dirs","session"));
    session_start();

    try
    {
      // check captcha
      if(empty($_REQUEST['validator']) || 
	 $_REQUEST['validator'] != $_SESSION['rand_code'])
	throw new AV_Exception(INVALID_FIELD, $this->lang,
			       array('fieldName'=>'validator'));

      // search for files at the folder
      $_GET['path'] = '/content/';
      $_GET['returnFields'] = 'contactMailTo';
      $_GET['limit'] = 1;
      $search = $this->LoadModule('LIB_Search');
      
      if(count($search->children)==0)
	throw new AV_Exception(NOT_FOUND, $this->lang);

      $_GET['mailTo'] = $search->children[0]->attrs['contactMailTo'];
      $this->LoadModule('CMS_SendContact');

      $_GET['labelId'] = 'cmsContactSentTag';
      $_GET['lang'] = $this->pageBuilder->lang;
      $getLabel = $this->LoadModule('LANG_GetLabel');
      $script .= 'cms.OnContactSent('.json_encode($getLabel->attrs['value']).');';
    }
    catch(AV_Exception $e)
    {
      $data = $e->getData();
      if($data['fieldName'] != NULL)
      {
	$script .= "avForm.OnFieldError('".$data['fieldName']."','";
	$script .= $e->getMessage()."');";
      }
      else
	$script .= "avForm.OnSubmitError('".$e->getMessage()."');";
    }
    $this->pageBuilder->rootTemplate->addText($script,'script');
  }
}

?>
