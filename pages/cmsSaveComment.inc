<?php

require_once('CMS_Ajax.inc');
require_once('CMS_RSSEvents.inc');

class cmsSaveComment extends CMS_Ajax
{
  protected function LoadPage()
  {
    parent::LoadPage();

    $_GET['path'] = '/comments/';
    $_GET['userRight'] = 'rw';
    $_GET['groupRight'] = 'rw';
    $_GET['otherRight'] = 'r';
    try
    {
      try
      {
	$saveComment = $this->LoadModule('LIB_InsDoc');
	$script .= 'window.location.reload();';
	$this->SendMessage($saveComment->attrs['docId']);

      }
      catch(AV_Exception $e)
      {
	if($e->getCode()==FIELD_REQUIRED)
	{
	  $data = $e->getData();
	  if(!$data['fieldName'])
	    throw $e;
	  $script = "avForm.OnFieldError('".$data['fieldName']."','";
	  $script .= $e->getMessage()."');";
	}
	else
	  $script .= "avForm.OnSubmitError(".json_encode($e->getMessage()).");";
      }
    }
    catch(Exception $e)
    {
      $script .= "avForm.OnSubmitError(".json_encode($e->getMessage()).");";
    }

    $this->pageBuilder->rootTemplate->addText($script,'script');
  }

  protected function SendMessage($commentId)
  {
    // retrieves articles owner information
    $_GET['docId'] = $_REQUEST['articleId'];
    $getDoc = $this->LoadModule('LIB_GetDoc');

    $pageLabels = & $this->LoadModule('LANG_GetPageLabels');
    $subjectPrefix = $this->pageBuilder->siteConfig->getVar("cms", 
							    "subjectPrefix");

    // if the article is on moderation, and the comment is from the author,
    // send message to the moderator
    if($getDoc->children[0]->attrs['otherRight'] == '' &&
       $getDoc->children[0]->attrs['moderator'] != NULL &&
       $getDoc->children[0]->attrs['userId'] ==
       $this->userSession->attrs['userId'])
    {
      $subject = $subjectPrefix.' '.$pageLabels->attrs['newCommentOnModTag'];
      $_GET['userId'] = $getDoc->children[0]->attrs['moderator'];
    }
    else
    {
      $subject = $subjectPrefix.' '.$pageLabels->attrs['newCommentOnDocTag'];
      $_GET['userId'] = $getDoc->children[0]->attrs['userId'];
    }

    $getUser = $this->LoadModule('UM_GetUser');
    $mailTo = $getUser->attrs['login'];


    // if it's an answer, sends notification to the comment author
    if($_REQUEST['quoteId'] != NULL)
    {
      $_GET['docId'] = $_REQUEST['quoteId'];
      $getComment = $this->LoadModule('LIB_GetDoc');
      if($getComment->children[0]->attrs['userId'] != $_GET['userId'])
      {
	$_GET['userId'] = $getComment->children[0]->attrs['userId'];
	$getUser2 = $this->LoadModule('UM_GetUser');

	$mailTo2 = $getUser2->attrs['login'];
	$subject2 = $subjectPrefix.' ';
	$subject2 .= $pageLabels->attrs['newAnswerOnCommentTag'];
      }
    }
     
     
    // link for the comments
    $link = dirname($_SERVER['PHP_SELF']);
    if($link!='/') 
      $link .= '/';
    $link = 'http://'.$_SERVER['HTTP_HOST'].$link;
    $link .= 'content/'.$_REQUEST['articleId'].'#comment'.$commentId;


    $mailFrom = $this->pageBuilder->siteConfig->getVar("cms", "mailFrom");
    $headers = "From: $mailFrom\r\n";
    $headers .= "Content-type: text/html; charset=\"utf-8\" \r\n";
    $headers .= "Content-Transfer-Encoding: quoted-printable \r\n";

    // send's notification to general users
    if($getDoc->children[0]->attrs['otherRight'] == 'r')
    {
      $_GET['event'] = RSS_COMMENTS;
      $_GET['docId'] = $_REQUEST['articleId'];
      $getNotAddrs = $this->LoadModule('CMS_GetNotificationAddresses');
      $addresses = $getNotAddrs->attrs['addresses'];
      if(count($addresses)>0)
      {
	// remove users who already are receiving notification
	$key = array_search($mailTo,$addresses);
	if($key !== false)
	  unset($addresses[$key]);

	$key = array_search($mailTo2,$addresses);
	if($key !== false)
	  unset($addresses[$key]);

	$mailTo3 = implode(',',$addresses);
	$subject3 = $subjectPrefix.' ';
	$subject3 .= $pageLabels->attrs['commentInTag'];
	$subject3 .= ' '.$getDoc->children[0]->attrs['title'];  
	$headers3 = "From: $mailFrom\r\n";
	$headers3 .= "Bcc: $mailTo3\r\n";
	$headers3 .= "Content-type: text/html; charset=\"utf-8\" \r\n";
	$headers3 .= "Content-Transfer-Encoding: quoted-printable \r\n";
      }    
    }


    $message = "<!DOCTYPE html PUBLIC \"-";
    $message .= "//W3C//DTD XHTML 1.0 Strict//EN\" ";
    $message .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
    $message .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>";
    $message .= "<p><b>" . $pageLabels->attrs['documentTag'];
    $message .= ":</b> <a href='$link'>$link</a></p>";
    $message .= '<p><b>' . $pageLabels->attrs['adminUserTag'];
    $name = $this->userSession->attrs['firstName'];
    if($this->userSession->attrs['lastName']!= '')
      $name .= ' '.$this->userSession->attrs['lastName'];
    $message .= ':</b> '.$name.'</p>';
    $message .= '<p><b>' . $pageLabels->attrs['commentTag'];
    $message .= ':</b></p>' .stripslashes($_REQUEST['comment']);
    $message .= '</body></html>';
    if(!mail($mailTo,$subject,$message,$headers))
    {
      $msg = " " . $pageLabels->attrs['authorNotErrorTag'];
      throw new AV_Exception(WARNING,$this->lang,array('addMsg'=>$msg));
    }
    if($mailTo2 != NULL)
    {
      if(!mail($mailTo2,$subject2,$message,$headers))
      {
	$msg = " " . $pageLabels->attrs['commAuthorNotErrorTag'];
	throw new AV_Exception(WARNING,$this->lang,array('addMsg'=>$msg));
      }
    }
    if($mailTo3 != NULL)
    {
      if(!mail(NULL,$subject3,$message,$headers3))
      {
	$msg = " " . $pageLabels->attrs['notificationErrorTag'];
	throw new AV_Exception(WARNING,$this->lang,array('addMsg'=>$msg));
      }
    }
  }

}

?>