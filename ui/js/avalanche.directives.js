var mod = angular.module('avalanche.directives', []);

mod.directive('avForm', function(Parser,avalancheConfig,$http,$templateCache,$compile) {
    return {
	restrict: 'E',
	scope: {
	    node: '=',
	    translations: '=',
	    avForm: '=name',
	    submit: '&',
	    templateUrl: '@',
	    params: '=',
	    taToolbar: '='
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);

	    scope.datepickerFormat = avalancheConfig.datepickerFormat;
	    scope.timepickerFormat = avalancheConfig.timepickerFormat;

            $http.get(scope.templateUrl || 'avForm.html', { 
		cache: $templateCache 
	    }).success(function(response) {
		var contents = element.html(response).contents();
		$compile(contents)(scope);
             
		scope.avForm.submit = function() {
		    if(scope.avForm.$valid) {
			// commit normally
			if(attr.submit!=undefined)
			    scope.submit();
		    } else {
			scope.avForm.$setSubmitted();
		    }
		}
	    });
	}	
    }
});

mod.directive('avField', function(Parser) {
    return {
	restrict: 'E',
	templateUrl: 'avField.html',
	link: function(scope,element,attr) {
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    }
});

mod.directive('avText',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: 'avTextField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avTextArea',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: 'avTextAreaField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avHtmlArea',function(Parser,$filter) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '=',
	    p_taToolbar: '=taToolbar'
	},
	templateUrl: 'avHtmlAreaField.html',
	controller: function($scope,avalancheConfig) {
	    if($scope.p_taToolbar)
		$scope.taToolbar = $scope.p_taToolbar;
	    else
		$scope.taToolbar = avalancheConfig.taToolbar;
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);

	    scope.onPaste = function ($html) {
		return $filter('htmlToPlaintext')($html);
	    };
	}
    };
});

mod.directive('avLangField',function(Parser,$filter,Languages,Loading,$http,$templateCache,$compile) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '=',
	    taToolbar: '='
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);

	    Loading.beginTasks();
	    var load = Loading.pushTask();
	    Loading.watchTasks();

	    var loadTpl = function() {
		$http.get('avLangField.html', { 
		    cache: $templateCache 
		}).success(function(response) {
		    var contents = element.html(response).contents();
		    $compile(contents)(scope);
		    load.resolve();
		}).error(function(){
		    load.reject();
		});
	    }

	    if(scope.values[scope.field.attrs.fieldName+'_numOfOptions']) {
		loadTpl();
		return;
	    }

	    var langList = {};
	    Languages.query(function(data){
		for(var i=0; i<data.length; i++) {
		    langList[data[i].lang] = data[i].name;
		}

		scope.values[scope.field.attrs.fieldName+'_numOfOptions'] = data.length;

		for(var i=0; i< scope.field.children.length; i++) {
		    var subField = scope.field.children[i];

		    if(scope.field.children.length>1)
			subField.attrs.label += ' ('+langList[subField.attrs.lang]+')';
		    subField.attrs.fieldName += '_'+(i+1)+'__value';
		    scope.values[scope.field.attrs.fieldName+'_'+(i+1)+'__lang'] = subField.attrs.lang;
		    scope.values[scope.field.attrs.fieldName+'_'+(i+1)+'__value'] = subField.attrs.value;	
		}
		
		loadTpl();
	    }, function() {
		load.reject();
	    });
	}
    };
});

mod.directive('avInteger',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: 'avIntegerField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avCurrency',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: 'avCurrencyField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avPercent',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: 'avPercentField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avCurrencySymbol',function($locale) {
    return {
	restrict: "EA",
        template: $locale.NUMBER_FORMATS.CURRENCY_SYM
    };
});

mod.directive('avBooleanInput', function() {
    return {
	require: 'ngModel',
	link: function(scope, element, attrs, ngModel) {
	    ngModel.$formatters.push(function(value) {
		return (value==1)?true:false;
	    });

            ngModel.$parsers.push(function (viewValue) {
		return viewValue?'1':'0'
            });

	}
    };
});

mod.directive('avIntegerInput', function() {
    return {
	require: 'ngModel',
	link: function(scope, element, attrs, ngModel) {
	    ngModel.$formatters.push(function(value) {
		return parseInt(value);
	    });
	}
    };
});

mod.directive('avFloatInput', ['$filter', '$locale', function ($filter, $locale) {
    return {
        require: 'ngModel',
        scope: {
            min: '=min',
            max: '=max',
	    fracDigits: '@'
        },
        link: function (scope, element, attrs, ngModel) {
	    var fracDigits = 2;
	    if(scope.fracDigits)
		fracDigits = scope.fracDigits;

            function decimalRex(dChar) {
                return RegExp("\\d|\\-|\\" + dChar, 'g');
            }

            function clearRex(dChar) {
                return RegExp("\\-{0,1}((\\" + dChar + ")|([0-9]{1,}\\" + dChar + "?))&?[0-9]{0,"+fracDigits+"}", 'g');
            }

            function clearValue(value) {
                value = String(value);
                var dSeparator = $locale.NUMBER_FORMATS.DECIMAL_SEP;
                var cleared = null;

                if(RegExp("^-[\\s]*$", 'g').test(value)) {
                    value = "-0";
                }

                if(decimalRex(dSeparator).test(value))
                {
                    cleared = value.match(decimalRex(dSeparator))
                        .join("").match(clearRex(dSeparator));
                    cleared = cleared ? cleared[0].replace(dSeparator, ".") : null;
                }

                return cleared;
            }

            ngModel.$parsers.push(function (viewValue) {
                return clearValue(viewValue);
            });

            element.on("blur", function () {
                element.val($filter('number')(ngModel.$modelValue, fracDigits));
            });

            ngModel.$formatters.unshift(function (value) {
		if(value != undefined && value !== null && value !== '')
                    return $filter('number')(value, fracDigits);
            });

            scope.$watch(function () {
                return ngModel.$modelValue;
            }, function (newValue, oldValue) {
                runValidations(newValue);
            })

            function runValidations(cVal) {
                if (isNaN(cVal)) {
                    return
                }
                if (typeof scope.min !== 'undefined') {
                    var min = parseFloat(scope.min)
                    ngModel.$setValidity('min', cVal >= min)
                }
                if (typeof scope.max !== 'undefined') {
                    var max = parseFloat(scope.max)
                    ngModel.$setValidity('max', cVal <= max)
                }
            }
        }
    }
}]);


mod.directive('avSelect',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    fieldList: '=',
	    avForm: '=',
	    fieldIndex: '=',
	    parentField: '@',
	    nodeService: '=',
	    path: '='
	},
	templateUrl: 'avSelectField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avSelectField',function(){
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
	    ngModel.$parsers.push(function(val) {
		if(val==null)
		    return '';
		return val;
	    });
	}
    };
});

mod.directive('avRadio',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: 'avRadioField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avSelectMultiple',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    fieldList: '=',
	    avForm: '=',
	    fieldIndex: '=',
	    nodeService: '=',
	    path: '='
	},
	templateUrl: 'avSelectMultField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avTargetField', function(Documents,Loading) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
	    scope.firstBind = true;
	    if(attrs.avTargetField!="") {
		var targetField = attrs.avTargetField.split(',');
		scope.$watch(function () {
                    return ngModel.$modelValue
		}, function (newValue, oldValue) {
		    if(!scope.fieldList) {
			return;
		    }
		    if(scope.firstBind) {
			scope.firstBind = false;
			return;
		    }
		    for(var i=0; i<scope.fieldList.length; i++) {
			if(targetField.indexOf(scope.fieldList[i].attrs.fieldName)!=-1) {
			    var fieldName = '';
			    if(attrs.parentField!="")
				fieldName += attrs.parentField + '.';
			    fieldName += scope.fieldList[i].attrs.fieldName;

			    if(!scope.nodeService)
				scope.nodeService = Documents;

			    scope.fieldList[i].children = [];
			    scope.values[scope.fieldList[i].attrs.fieldName] = undefined;
			    (function(i){
				Loading.beginTasks();
				var load = Loading.pushTask();
				Loading.watchTasks();
				
				var getOptions = {
				    fieldName: fieldName,
				    sourceId: newValue
				}

				if(scope.values.docId)
				    getOptions.docId = scope.values.docId;
				else if(scope.values.folderId)
				    getOptions.folderId = scope.values.folderId;
				else
				    getOptions.path = scope.path;
				
				scope.nodeService.get(getOptions,function(data) {
				    scope.fieldList[i].children = data.children[0].children;
				    scope.values[scope.fieldList[i].attrs.fieldName] = data.attrs[scope.fieldList[i].attrs.fieldName];
				    load.resolve();
				}, function() {
				    load.reject();
				})
			    })(i);
			}
		    }

		});
	    }
	}
    };
});

mod.directive('avObjectList',function(Parser) {
    return {
	restrict: "E",
	templateUrl: 'avObjectListField.html',
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    dateFormat: '@',
	    timeFormat: '@',
	    aSortableListeners: '=',
	    path: '='
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	    scope.insert = function($event) {
		$event.preventDefault();
		eval('scope.values.'+scope.field.attrs.fieldName+'.push({})');
	    }
	    scope.remove = function($index) {
		eval('scope.values.'+scope.field.attrs.fieldName+'.splice($index,1)');
		scope.avForm.$setDirty();
	    }

	    if(scope.aSortableListeners==undefined)
		scope.aSortableListeners = {};
	    scope.aSortableListeners.accept = function (sourceItemHandleScope, destSortableScope) {
		return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
	    }
	    scope.aSortableListeners.orderChanged = function(event) {
		scope.avForm.$setDirty();
	    }  
	    
	}
    };
});

mod.directive('avDateTime',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    dateFormat: '@',
	    timeFormat: '@',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: 'avDateTimeField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avDate',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    dateFormat: '@',
	    avForm: '=',
	    fieldIndex: '=',
	    alignment: '@'
	},
	templateUrl: 'avDateField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avDatepickerCtrls', function($filter,$timeout) {
    return {
	restrict: 'A',
	link: function(scope,element,attr) {
	    scope.today = function() {
		var today = new Date();
		scope.values[scope.field.attrs.fieldName] = $filter('date')(today, attr['modelDateFormat']);
		$timeout(function() {
		    element[0].blur();
		},0,false);
	    }

	    scope.clear = function() {
		scope.values[scope.field.attrs.fieldName] = undefined;
		$timeout(function() {
		    element[0].blur();
		},0,false);
	    }
	}
    }
});

mod.directive('avFileList',function(FileList,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    translations: '=',
	    avForm: '='
	},
	templateUrl: function(elem,attrs) {
	    switch(attrs.type || avalancheConfig.fileListType){
	    case 'detailed':
		return 'avDetailedFileListField.html';

	    case 'icon':
	    default:
		return 'avFileListField.html';
	    };
	},
	link: function(scope,element,attr) {
	    FileList(scope);
	}
    };
});

mod.directive('avComposedField', function() {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
	    var scopeField = scope[attrs.avComposedField];
	    if(scopeField==undefined)
	    {
		scopeField = { fields: [], maxlength: 0};
		scope[attrs.avComposedField]=scopeField;
	    }

	    var id = scopeField.fields.length;
	    scopeField.fields.push("");
	    var strPos = scopeField.maxlength;
	    scopeField.maxlength += parseInt(attrs.maxlength);

            ngModel.$parsers.push(function (viewValue) {
		scopeField.fields[id] = viewValue;
		return scopeField.fields.join("");
            });

            ngModel.$formatters.unshift(function (value) {
		if(typeof(value) != "string" || value.length==0) {
		    scopeField.fields[id] = "";
		    return scopeField.fields[id];
		}

		if(value.length==scopeField.maxlength)
		    scopeField.fields[id] = value.substr(strPos,attrs.maxlength);

		return scopeField.fields[id];
            });

	    element.bind('keyup',function(){
		if(element.val().length==attrs.maxlength && id < scopeField.fields.length-1)
		    element.next()[0].select();
	    });

	}
    };
});


mod.directive('avCnpjField', function(CalcDigitosPosicoes) {

    var valida_cnpj = function(valor) {
	// Garante que o valor é uma string
	valor = valor.toString();

	// Remove caracteres inválidos do valor
	valor = valor.replace(/[^0-9]/g, '');

	// O valor original
	var cnpj_original = valor;

	// Captura os primeiros 12 números do CNPJ
	var primeiros_numeros_cnpj = valor.substr( 0, 12 );

	// Faz o primeiro cálculo
	var primeiro_calculo = CalcDigitosPosicoes( primeiros_numeros_cnpj, 5 );

	// O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
	var segundo_calculo = CalcDigitosPosicoes( primeiro_calculo, 6 );

	// Concatena o segundo dígito ao CNPJ
	var cnpj = segundo_calculo;

	// Verifica se o CNPJ gerado é idêntico ao enviado
	if ( cnpj === cnpj_original ) {
            return true;
	}

	// Retorna falso por padrão
	return false;

    } // valida_cnpj

    return {
        require: 'ngModel',
	priority: 1, // deve rodar antes do avComposedField
        link: function (scope, element, attrs, ngModel) {
          //For DOM -> model validation
            ngModel.$parsers.push(function(value) {
		ngModel.$setValidity('invalidCnpj', valida_cnpj(value));
		return value;
            });

            //For model -> DOM validation
            ngModel.$formatters.push(function(value) {
		if(value)
		    ngModel.$setValidity('invalidCnpj', valida_cnpj(value));
		return value;
            });

	}
    }
});

mod.directive('avCnpj', function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '='
	},
	templateUrl: 'avCnpjField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avCpfField', function(CalcDigitosPosicoes) {

    var valida_cpf = function(valor) {
	// Garante que o valor é uma string
	valor = valor.toString();
    
	// Remove caracteres inválidos do valor
	valor = valor.replace(/[^0-9]/g, '');


	// Captura os 9 primeiros dígitos do CPF
	// Ex.: 02546288423 = 025462884
	var digitos = valor.substr(0, 9);

	// Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
	var novo_cpf = CalcDigitosPosicoes(digitos);

	// Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
	var novo_cpf = CalcDigitosPosicoes(novo_cpf,11);

	// Verifica se o novo CPF gerado é idêntico ao CPF enviado
	if ( novo_cpf === valor ) {
            // CPF válido
            return true;
	} else {
            // CPF inválido
            return false;
	}
	
    } // valida_cpf

    return {
        require: 'ngModel',
	priority: 1, // deve rodar antes do avComposedField
        link: function (scope, element, attrs, ngModel) {
          //For DOM -> model validation
            ngModel.$parsers.push(function(value) {
		ngModel.$setValidity('invalidCpf', valida_cpf(value));
		return value;
            });

            //For model -> DOM validation
            ngModel.$formatters.push(function(value) {
		if(value)
		    ngModel.$setValidity('invalidCpf', valida_cpf(value));
		return value;
            });

	}
    }
});

mod.directive('avCpf', function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '='
	},
	templateUrl: 'avCpfField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avCep', function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    fieldList: '=',
	    avForm: '='
	},
	templateUrl: 'avCepField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avCepField', function(Cep, Loading, avalancheConfig) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
	    scope.firstBind = true;
            scope.$watch(function () {
                return ngModel.$modelValue
            }, function (newValue, oldValue) {
		if(scope.firstBind)
		{
		    scope.firstBind = false;
		    return;
		}

		if(typeof(newValue)!="string" || newValue.length!=8)
		    return;

		Loading.beginTasks();
		var load = Loading.pushTask();
		Loading.watchTasks();
		var data = Cep.get({cep:newValue},function(){

		    if(scope.values[avalancheConfig.cepConfig.estado] != data.uf_codigo) {
			var expr = 'values.'+avalancheConfig.cepConfig.cidade;
			var unreg = scope.$watch(expr,function(newValue, oldValue){
			    if(newValue !== undefined && oldValue === undefined) {
				unreg();
				scope.values[avalancheConfig.cepConfig.cidade] = data.cidade_codigo;
			    }
			});
			scope.values[avalancheConfig.cepConfig.estado] = data.uf_codigo;
		    } else {
			scope.values[avalancheConfig.cepConfig.cidade] = data.cidade_codigo;
		    }

		    scope.values[avalancheConfig.cepConfig.logradouro] = data.endereco_logradouro;
		    scope.values[avalancheConfig.cepConfig.bairro] = data.bairro_descricao;

		    load.resolve();
		}, function() {
		    load.reject();
		});

            })
	}
    };
});

mod.directive('avEmail', function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '='
	},
	templateUrl: 'avEmailField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avCheckbox', function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '='
	},
	templateUrl: 'avCheckboxField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	}
    };
});

mod.directive('avSelectAllCheckbox', function () {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            checkboxes: '=',
            allSelected: '=allSelected',
            allClear: '=allClear'
        },
        template: '<input type="checkbox" ng-model="master" ng-change="masterChange()" av-no-dirty-check>',
        controller: function ($scope, $element) {
            $scope.masterChange = function () {
                if ($scope.master) {
                    angular.forEach($scope.checkboxes, function (cb, index) {
                        cb.checked = true;
                    });
                } else {
                    angular.forEach($scope.checkboxes, function (cb, index) {
                        cb.checked = false;
                    });
                }
            };
	    
            $scope.$watch('checkboxes', function () {
                var allSet = true, allClear = true;
                angular.forEach($scope.checkboxes, function (cb, index) {
                    if (cb.checked) {
                        allClear = false;
                    } else {
                        allSet = false;
                    }
                });

                if ($scope.allSelected !== undefined) {
                    $scope.allSelected = allSet;
                }
                if ($scope.allClear !== undefined) {
                    $scope.allClear = allClear;
                }

                $element.prop('indeterminate', false);
                if (allSet) {
                    $scope.master = true;
                } else if (allClear) {
                    $scope.master = false;
                } else {
                    $scope.master = false;
                    $element.prop('indeterminate', true);
                }
		
            }, true);
        }
    };
});

mod.directive('avLoginForm',function() {
    return {
	restrict: 'E',
	templateUrl: 'avLogin.html',
	link: function(scope,elem,attrs) {
	}
    }
});

mod.directive('avUploader', function () {
    return {
	restrict: 'AE',
	transclude: true,
	template: "<input type='file' name='content' files-model='eventoArquivo.content' style='display:none'><span ng-transclude></span>",
	link: function(scope, element, attrs) {
	    element.ready(function(){
		// recupera input
		var input = element.find("input");

		// onchange do input deve submiter o arquivo
		input.bind('change',function(){
		    scope.EnviaEvento(scope.eventoArquivo);
		});

	        element.bind("click", function(){
		    input[0].click();
		});
            });
	}
    }
});

mod.directive('avTooltip', function($tooltip) {
    return {
	restrict: 'A',
	link: function(scope,element,attr) {
	    var options = {};
	    var prefix = 'avTooltip';
            angular.forEach([ 'template', 'contentTemplate', 'placement', 'container', 'delay', 'trigger', 'html', 'animation', 'backdropAnimation', 'type', 'customClass', 'id' ], function(key) {
		var cKey = prefix+key.charAt(0).toUpperCase()+key.substring(1);
		if (angular.isDefined(attr[cKey]))
		    options[key] = attr[cKey];
            });
            var falseValueRegExp = /^(false|0|)$/i;
            angular.forEach([ 'html', 'container' ], function(key) {
		var cKey = prefix+key.charAt(0).toUpperCase()+key.substring(1);
		if (angular.isDefined(attr[cKey]) && falseValueRegExp.test(attr[cKey]))
		    options[key] = false;
            });
            var dataTarget = element.attr('av-tooltip-target');
            if (angular.isDefined(dataTarget)) {
		if (falseValueRegExp.test(dataTarget))
		    options.target = false;
		else
		    options.target = dataTarget;
            }
	    options.title = attr.avTooltip;

	    var tooltip = $tooltip(element, options);

	    scope.$on('tooltip.show',  function(event, elem){
		tooltip.hide();
		tooltip.setEnabled(false);
	    });

	    scope.$on('tooltip.hide',  function(event, elem){
		tooltip.setEnabled(true);
	    });

	}
    }
});

mod.directive('avPagination', function($anchorScroll) {
    return {
	restrict: "E",
	templateUrl: "avPagination.html",
        link: function (scope, element, attrs) {
	    scope.setOffset = function(offset) {
		$anchorScroll.yOffset = 0;
		$anchorScroll();
		scope.$emit('av.pagination.offset',offset);
	    }

            attrs.source && scope.$watch(attrs.source, function(newValue, oldValue) {
		if(newValue==undefined || newValue.totalResults==undefined)
		{
		    scope.showPagination = false;
		    return;
		}

		scope.pages = [];

		var navStart = 0;
		var offset = parseInt(eval('scope.'+attrs.offset));
		var limit = parseInt(eval('scope.'+attrs.limit));
		var totalResults = newValue.totalResults;
		scope.nextOffset=offset+limit;
		scope.prevOffset=offset-limit;
		scope.disableNext = (totalResults-offset<=limit);
		scope.disablePrev = (offset<limit);
		scope.showPagination = (totalResults>limit);

		if(limit<=0 || totalResults < limit)
		    return;

		if(offset>=limit*6)
		    navStart = offset - limit*5;

		for(var i=navStart;
		    i <= navStart+limit*9 && i < totalResults;
		    i += limit)
		{
		    var page = { value: (i/limit+1), selected: false};
		    if(i == offset)
			page.selected = true;
		    page.offset = i;

		    scope.pages.push(page);
 		}
	    }, true);
	}
    };
});

mod.directive('avFileViewer', function(FileViewer) {
    return {
	restrict: "A",
	scope: { doc: '=' },
        link: function (scope, element, attrs) {
	    var options = {
		doc: scope.doc,
		show: false
	    }		
	    if(attrs.contentTemplate)
		options.contentTemplate = attrs.contentTemplate;
	    if(attrs.container)
		options.container = attrs.container;
	    var fileViewer = FileViewer(options);
	    element.bind('click',function(){
		fileViewer.show();
	    });

            // Garbage collection
            scope.$on('$destroy', function() {
		if (fileViewer) fileViewer.destroy();
		options = null;
		fileViewer = null;
            });
	    
	}
    };
});

mod.directive('avMdiFileIcon', function() {
    return {
	restrict: "E",
	scope: { contentType: '@' },
	template: "<i class='mdi file-icon' ng-class='getIconClass()'>",
        link: function (scope, element, attrs) {
	    scope.getIconClass = function () {
		switch(scope.contentType)
		{
		    // documents
		case 'text/plain':
		    return 'mdi-file-document';

		    // spreadsheet
		case 'application/vnd.ms-excel':
		    return 'mdi-file-excel';

		    // word
		case 'application/msword':
		    return 'mdi-file-word';

		    // images
		case 'image/gif':
		case 'image/png':
		case 'image/jpeg':
		case 'image/pjpeg':
		    return 'mdi-file-image';

		    // music
		case 'audio/mpeg':
		case 'audio/x-pn-realaudio':
		case 'audio/x-wav':
		    return 'mdi-file-music';

		    // pdf
		case 'application/pdf':
		    return 'mdi-file-pdf';
		default:
		    return 'mdi-file';
		}
	    }
	}
    };
});

mod.directive('avConfirmPopover', function($popover) {
    return {
	restrict: 'A',
	scope: {
	    action: '&'
	},
	link: function(scope,element,attrs) {
	    scope.confirm = function($event,$hide) {
		$event.preventDefault();
		$hide();
		scope.action();
	    }
	    var options = {
		content: attrs.avConfirmPopover,
		template: 'avConfirmPopover.html',
		autoClose: 1,
		scope: scope
	    }
	    if(attrs.placement)
		options.placement = attrs.placement;
	    var popover = $popover(element, options);

            // Garbage collection
            scope.$on('$destroy', function() {
		if (popover) {
		    popover.destroy();
		}
            });
	    
	}
    }

});

mod.directive('avBodyLogin', function() {
    return {
	restrict: 'A',
	link: function(scope,element,attrs) {
	    scope.$watch('session.userId',function(newValue,oldValue){
		if(newValue)
		    element.removeClass('login');
		else
		    element.addClass('login');
	    });
	}
    };
});


mod.directive('avBody', function() {
    return {
	restrict: 'A',
	controller: function($scope){
	    $scope.$on('$stateChangeSuccess',
		       function(event, toState, toParams, fromState, fromParams ){
			   $scope.stateClass = toState.name.replace(/\./g,'-');
		       });
	},
	link: function(scope,element,attrs) {
	    scope.$watch('stateClass',function(newValue,oldValue) {
		if(oldValue)
		    element.removeClass(oldValue);
		if(newValue)
		    element.addClass(newValue);
	    });

	    scope.$watch('loading',function(newValue,oldValue){
		if(newValue)
		    element.addClass('loading');
		else
		    element.removeClass('loading');
	    });

	    scope.$watch('block',function(newValue,oldValue){
		if(newValue)
		    element.addClass('block');
		else
		    element.removeClass('block');
	    });


	}
    };
});

mod.directive('avVisited', [function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, ctrl) {
	element.bind('blur', function(evt) {
            scope.$apply(function() {ctrl.$visited = true;});
	});
	scope.$watch('avForm.$pristine',function(newValue,oldValue){
	    if(newValue) {
		ctrl.$visited = false;
	    }
	});
    }
  }
}]);

mod.directive('avPassword',function(Parser,$modal) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: function(elem,attrs) {
	    return attrs.templateUrl || 'avPasswordField.html';
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);

	    scope.generatePassword = function() {
		scope.genPassword = Math.random().toString(36).slice(-8);
	    }

	    scope.usePassword = function($hide) {
		scope.values[scope.field.attrs.fieldName] = scope.genPassword;
		scope.values[scope.field.attrs.fieldName+'2'] = scope.genPassword;
		$hide();
	    }

	    scope.generatePasswordModal = function() {
		scope.generatePassword();
		var modal = $modal({
		    template: 'avGeneratedPassword.html',
		    animation: 'am-fade-and-scale',
		    scope: scope
		});
	    }
	}
    };
});

mod.directive('avPasswordCheck', [function() {
    return {
	restrict: 'A',
	require: 'ngModel',
	scope: {
	    password: '='
	},
	link: function(scope, element, attrs, ctrl) {
	    ctrl.$validators.avPasswordCheck = function(modelValue,viewValue) {
		return modelValue==scope.password;
	    }

	    scope.$watch("password", function() {
		ctrl.$validate();
	    });
	}
    }
}]);

mod.directive('avAuthSecret',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '='
	},
	templateUrl: 'avAuthSecret.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);

	}
    };
});

mod.directive('avTabs', function() {
    return {
	templateUrl: function(element, attr) {
	    return attr.templateUrl || "avTabs.html";
	}
    };
});

mod.directive('avPane', function($window, $animate, $sce) {
    return {
	require: ['^?ngModel', '^bsTabs'],
	scope: true,
	link: function postLink(scope, element, attrs, controllers) {

            var ngModelCtrl = controllers[0];
            var bsTabsCtrl = controllers[1];
	    
            // Add base class
            element.addClass('tab-pane');
	    
            // Observe title attribute for change
            attrs.$observe('title', function(newValue, oldValue) {
		scope.title = $sce.trustAsHtml(newValue);
            });
	    
            // Save tab name into scope
            scope.name = attrs.name;
	    
            // Save tab removeBtn into scope
            scope.removeBtn = attrs.removeBtn;

	    scope.close = function() {
		scope.$parent.$emit('avPaneClose',scope.$tabs[scope.$index]);
		scope.$tabs.splice(scope.$index,1);
	    }

            // Add animation class
            if(bsTabsCtrl.$options.animation) {
		element.addClass(bsTabsCtrl.$options.animation);
            }
	    
            attrs.$observe('disabled', function(newValue, oldValue) {
		scope.disabled = scope.$eval(newValue);
            });
	    
            // Push pane to parent bsTabs controller
            bsTabsCtrl.$push(scope);
	    
            function render() {
		var index = bsTabsCtrl.$panes.indexOf(scope);
		$animate[bsTabsCtrl.$isActive(scope, index) ? 'addClass' : 'removeClass'](element, bsTabsCtrl.$options.activeClass);
            }
	    
            bsTabsCtrl.$activePaneChangeListeners.push(render);
            render();	    

            // remove pane from tab controller when pane is destroyed
            scope.$on('$destroy', function() {
		bsTabsCtrl.$activePaneChangeListeners.splice(bsTabsCtrl.$activePaneChangeListeners.indexOf(render),1);
		bsTabsCtrl.$remove(scope);
            });

	}
    };
    
});

mod.directive('avNoDirtyCheck', function() {
    // Interacting with input elements having this directive won't cause the
    // form to be marked dirty.
    return {
	restrict: 'A',
	require: 'ngModel',
	link: function(scope, elm, attrs, ctrl) {
	    ctrl.$pristine = false;
	}
    }
});

mod.directive('avObjModelCtrl',function(){
    return {
	restrict: 'E',
	require: 'ngModel',
	link: function(scope, element, attrs, ctrl) {
            // Watch model for changes
	    var first = true;
            scope.$watch(attrs.ngModel, function(newValue, oldValue) {
		if(first) {
		    first = false;
		    return;
		}
		if(newValue!=oldValue) {
		    ctrl.$setDirty();
		    ctrl.$validate();
		}
            });

	}
    }
});

mod.directive('avArrayModelCtrl',function(){
    return {
	restrict: 'E',
	require: 'ngModel',
	link: function(scope, element, attrs, ctrl) {
            // Watch model for changes
	    var first = true;
            scope.$watch(attrs.ngModel, function(newValue, oldValue) {
		if(first) {
		    first = false;
		    return;
		}
		if(newValue!=oldValue) {
		    ctrl.$setDirty();
		    ctrl.$validate();
		}
            }, true);

	    
            ctrl.$isEmpty = function(value){
		return !value || value.length === 0;
            };
	}
    }
});

mod.directive('avEmailList',function($alert) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    translations: '='
	},
	templateUrl: 'avEmailListField.html',
	link: function(scope,element,attr) {
	    scope.email = '';

	    scope.add = function() {
		var email = scope.email.trim();
		if(email == '')
		    return;

		var values = scope.values[scope.field.attrs.fieldName];
		if(values) {
		    if(values.indexOf(email)!=-1) {
			var error = $alert({
			    animation: 'am-slide-top',
			    placement: 'top-right',
			    content: scope.translations['addressAlreadyRegTag'],
			    template: 'avAlert.html',
			    type: 'danger',
			    show: true,
			    container: 'body',
			    duration: 5,
			    dismissable: true
			});
			return;
		    }
		} else
		    values = scope.values[scope.field.attrs.fieldName] = [];
		values.push(email);
		scope.email = '';
	    }

	    scope.remove = function(email) {
		var values = scope.values[scope.field.attrs.fieldName];
		values.splice(values.indexOf(email),1);
	    }
	}
    };
});

mod.directive('avCaptcha',function(Parser,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: 'avCaptchaField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	    scope.captchaUrl = avalancheConfig.backendAddress+'/cmsCaptcha.av';
	}
    };
});

mod.directive('avScrollByDragging', function($interval) {
    return {
	restrict: 'A',
	link: function(scope,element,attrs) {
	    var options = {
		offset: 20,
		boundary: 50,
		top: 30,
		interval: 100
	    }

	    var scrollingUp = undefined;
	    var startScrollUp = function() {
		if(scrollingUp !=undefined)
		    return;
		scrollingUp = $interval(function() {
		    var newValue = element[0].scrollTop - options.offset;
		    if(newValue<0)
			newValue = 0;
		    element[0].scrollTop = newValue;
		},options.interval);
	    }
	    var stopScrollUp = function() {
		if(scrollingUp != undefined) {
		    $interval.cancel(scrollingUp);
		    scrollingUp = undefined;
		}
	    }

	    var scrollingDown = undefined;
	    var startScrollDown = function() {
		if(scrollingDown !=undefined)
		    return;
		scrollingDown = $interval(function() {
		    var max = element[0].scrollHeight - element[0].clientHeight;
		    var newValue = element[0].scrollTop + options.offset;
		    if(newValue>max)
			newValue = max;
		    element[0].scrollTop = newValue;
		},options.interval);
	    }
	    var stopScrollDown = function() {
		if(scrollingDown != undefined) {
		    $interval.cancel(scrollingDown);
		    scrollingDown = undefined;
		}
	    }

	    scope.dragMove = function(event) {
		if(event.lastY < options.top + options.boundary)
		    startScrollUp();
		else 
		    stopScrollUp();

		if(event.lastY > element[0].clientHeight + options.top - options.boundary)
		    startScrollDown();
		else
		    stopScrollDown();
		
	    }

	    scope.dragEnd = function() {
		stopScrollUp();
		stopScrollDown();
	    }

	}
    };
});

mod.directive('avSearchFields', function(Parser,Loading,Form,avalancheConfig) {
    return {
	restrict: 'E',
	scope: {
	    path: '@',
	    translations: '=',
	    avForm: '=',
	    values: '=',
	    fieldList: '=',
	    excludeFields: '='
	},
	templateUrl: function(elem,attrs) {
	    return attrs.templateUrl || 'avSearchFields.html';
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);

	    scope.datepickerFormat = avalancheConfig.datepickerFormat;
	    scope.timepickerFormat = avalancheConfig.timepickerFormat;

	    var loadFields = function(path) {
		scope.node = undefined;
		if(!path)
		    return;

		Loading.beginTasks();
		var loadSubject = Loading.pushTask();
		Loading.watchTasks();
		Form.get({
		    path: path,
		    excludeFields: scope.excludeFields ? scope.excludeFields:
			['userId','groupId','userRight','groupRight',
			 'otherRight','creationDate','lastChanged',
			 'lastChangedUserId','keywords']
		},function(node) {
		    scope.node = node;
		    scope.values[attr.name] = scope.node.attrs;
		    scope.fieldList[attr.name] = scope.node.fields;
		    loadSubject.resolve();
		}, function(){
		    loadSubject.reject();
		});
	    }

	    scope.$watch('path',function(newValue,oldValue) {
		loadFields(newValue);
	    });

	    if(attr.reloadEvent) {
		scope.$on(attr.reloadEvent, function() {
		    loadFields(scope.path);
		});
	    }
	}	
    }
});

mod.directive('avDateFilter',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    dateFormat: '@',
	    avForm: '='
	},
	templateUrl: 'avDateFilter.html',
	controller: function($scope,$translate) {
	    $scope.fields = {
		type: {
		    attrs: {
			fieldName:"type",
			label: $scope.field.attrs.label
		    },
		    children: []
		},		    
		before: {
		    attrs: {
			fieldName: 'before'
		    },
		    children: []
		},		    
		after: {
		    attrs: {
			fieldName: 'after'
		    },
		    children: []
		}
	    }
	    $translate(['beforeThanTag','betweenTag','afterThanTag']).then(function (data) {
		$scope.fields.type.children = [
		    { attrs: {id: "before", value: data.beforeThanTag} },
		    { attrs: {id: "between", value: data.betweenTag} },
		    { attrs: {id: "after", value: data.afterThanTag} }
		];
		$scope.fields.after.attrs.label = data.afterThanTag;
		$scope.fields.before.attrs.label = data.beforeThanTag;
	    });
		
	    if(!$scope.values[$scope.field.attrs.fieldName])
		$scope.values[$scope.field.attrs.fieldName] = {};
	    $scope.$watch('values.'+$scope.field.attrs.fieldName+'.type',function(newValue,oldValue) {
		if($scope.values && $scope.values[$scope.field.attrs.fieldName]) {
		    delete $scope.values[$scope.field.attrs.fieldName].before;
		    delete $scope.values[$scope.field.attrs.fieldName].after;
		}
	    });
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avCurrencyFilter',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '='
	},
	templateUrl: 'avCurrencyFilter.html',
	controller: function($scope,$translate) {
	    $scope.fields = {
		type: {
		    attrs: {
			fieldName:"type",
			label: $scope.field.attrs.label
		    },
		    children: []
		},		    
		lower: {
		    attrs: {
			fieldName: 'lower'
		    },
		    children: []
		},		    
		greater: {
		    attrs: {
			fieldName: 'greater'
		    },
		    children: []
		}
	    }
	    $translate(['lowerThanTag','betweenTag','greaterThanTag']).then(function (data) {
		$scope.fields.type.children = [
		    { attrs: {id: "lower", value: data.lowerThanTag} },
		    { attrs: {id: "between", value: data.betweenTag} },
		    { attrs: {id: "greater", value: data.greaterThanTag} }
		];
		$scope.fields.greater.attrs.label = data.greaterThanTag;
		$scope.fields.lower.attrs.label = data.lowerThanTag;
	    });
		
	    if(!$scope.values[$scope.field.attrs.fieldName])
		$scope.values[$scope.field.attrs.fieldName] = {};
	    $scope.$watch('values.'+$scope.field.attrs.fieldName+'.type',function(newValue,oldValue) {
		if($scope.values && $scope.values[$scope.field.attrs.fieldName]) {
		    delete $scope.values[$scope.field.attrs.fieldName].greater;
		    delete $scope.values[$scope.field.attrs.fieldName].lower;
		}
	    });
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avIntegerFilter',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '='
	},
	templateUrl: 'avIntegerFilter.html',
	controller: function($scope,$translate) {
	    $scope.fields = {
		type: {
		    attrs: {
			fieldName:"type",
			label: $scope.field.attrs.label
		    },
		    children: []
		},		    
		lower: {
		    attrs: {
			fieldName: 'lower'
		    },
		    children: []
		},		    
		greater: {
		    attrs: {
			fieldName: 'greater'
		    },
		    children: []
		}
	    }
	    $translate(['lowerThanTag','betweenTag','greaterThanTag']).then(function (data) {
		$scope.fields.type.children = [
		    { attrs: {id: "lower", value: data.lowerThanTag} },
		    { attrs: {id: "between", value: data.betweenTag} },
		    { attrs: {id: "greater", value: data.greaterThanTag} }
		];
		$scope.fields.greater.attrs.label = data.greaterThanTag;
		$scope.fields.lower.attrs.label = data.lowerThanTag;
	    });
		
	    if(!$scope.values[$scope.field.attrs.fieldName])
		$scope.values[$scope.field.attrs.fieldName] = {};
	    $scope.$watch('values.'+$scope.field.attrs.fieldName+'.type',function(newValue,oldValue) {
		if($scope.values && $scope.values[$scope.field.attrs.fieldName]) {
		    delete $scope.values[$scope.field.attrs.fieldName].greater;
		    delete $scope.values[$scope.field.attrs.fieldName].lower;
		}
	    });
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('avRelinkEvent', function($rootScope) {
    return {
        transclude: 'element',
        restrict: 'A',
        link: function(scope, element, attr, ctrl, transclude) {
            var previousContent = null;

            var triggerRelink = function() {
                if (previousContent) {
                    previousContent.remove();
                    previousContent = null;
                }

                transclude(function (clone) {
                    element.parent().append(clone);
                    previousContent = clone;
                });

            };

            triggerRelink();                
            $rootScope.$on(attr.avRelinkEvent, triggerRelink);

        }
    };

});
