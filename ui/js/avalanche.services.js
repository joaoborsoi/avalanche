var mod = angular.module('avalanche.services', ['ngResource','avalanche.config']);

mod.factory('Menu', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/menu/');
});

mod.factory('States', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/states/',null);
});

mod.factory('Languages', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/languages/',null);
});

mod.factory('User', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/user/:userId?fieldName=:fieldName&sourceId=:sourceId');
});

mod.factory('Documents', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/documents/:docId?path=:path&fieldName=:fieldName&sourceId=:sourceId&docLinks=:docLinks');
});

mod.factory('DocOrder', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/doc-order/:docId');
});

mod.factory('Search', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/search',null,
		     { search:  {method:'POST'}});
});

mod.factory('Folders', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/folders/:folderId?fieldName=:fieldName&sourceId=:sourceId');
});

mod.factory('FolderOrder', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/folder-order/');
});

mod.factory('List', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/list',null,
		     { search:  {method:'POST'}});
});

mod.factory('Login', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/login/');
});

mod.factory('Logout', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/logout/');
});

mod.factory('Session', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/session/');
});

mod.factory('Cep',function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/cep/:cep');
});

mod.factory('History',function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/history/:docId');
});

mod.factory('Parser',function() {
    var obj = {};
    
    obj.bool = function(x,def) {
	if(!angular.isDefined(x))
	    return def;
	
        var falseValueRegExp = /^(false|0|)$/i;
	return !falseValueRegExp.test(x);
    }

    return obj;
});

mod.factory('HistoryFormatter', function($filter,$locale,avalancheConfig) {
    var obj = {};

    obj.formatCnpj = function(value) {
	return $filter('cnpj')(value);
    }

    obj.formatCpf = function(value) {
	return $filter('cpf')(value);
    }

    obj.formatCep = function(value) {
	return $filter('cep')(value);
    }

    obj.formatCep = function(field, value) {
	for(var i=0;i<field.children.length;i++)
	    if(field.children[i].attrs.id==value)
		return field.children[i].attrs.value;
    }

    obj.formatDatetime = function (value) {
	return $filter('date')($filter('mysqlDatetimeToISO')(value),
			       avalancheConfig.datetimeFormat);
    }

    obj.formatDate = function(value) {
	return $filter('date')($filter('mysqlDatetimeToISO')(value),
			       avalancheConfig.dateFormat);
    }

    obj.formatCurrency = function(value) {
	return $filter('currency')(value);
    }

    obj.formatPercent = function(value) {
	return $filter('number')(value, 2)+'%';
    }

    obj.formatBoolean = function(value,translations) {
	return translations[value==1?'trueTag':'falseTag'];
    }

    obj.formatObjectList = function(field,value) {
	var newValue = '"';
	for(var j=0;j<value.length;j++)
	{
	    if(j>0)
		newValue += '; "';
	    
	    var first = true;
	    for(var i=0;i<field.children.length;i++)
	    {
		var fieldValue = eval('obj.format(field.children[i],value[j].'+
				      field.children[i].attrs['fieldName']+')');
		if(fieldValue)
		{
		    if(first)
			first = false;
		    else
			newValue += ', ';
		    newValue += fieldValue;
		}
	    }
	    newValue += '"';
	}
	return newValue;
    }

    obj.format = function (field,value,translations)  {

	switch(field.attrs.type)
	{
	case 'cep':
	    return obj.formatCep(value);

	case 'cnpj':
	    return obj.formatCnpj(value);

	case 'select':
	    return obj.formatSelect(field,value);

	case 'datetime':
	    return obj.formatDatetime(value);

	case 'date':
	    return obj.formatDate(value);

	case 'currency':
	    return obj.formatCurrency(value);

	case 'percent':
	    return obj.formatPercent(value);

	case 'text': case 'integer':
	    return value;

	case 'boolean':
	    return obj.formatBoolean(value,translations);

	case 'objectList':
	    return obj.formatObjectList(field,value);
	}
    }

    return obj;
});

mod.factory('CalcDigitosPosicoes', function() {

    /*
      calc_digitos_posicoes
      
      Multiplica dígitos vezes posições
      
      @param string digitos Os digitos desejados
      @param string posicoes A posição que vai iniciar a regressão
      @param string soma_digitos A soma das multiplicações entre posições e dígitos
      @return string Os dígitos enviados concatenados com o último dígito
    */
    var calc_digitos_posicoes = function( digitos, posicoes, soma_digitos) {
	if(posicoes==undefined)
	    posicoes=10;
	if(soma_digitos==undefined)
	    soma_digitos=0;
	
	// Garante que o valor é uma string
	digitos = digitos.toString();
	
	// Faz a soma dos dígitos com a posição
	// Ex. para 10 posições:
	//   0    2    5    4    6    2    8    8   4
	// x10   x9   x8   x7   x6   x5   x4   x3  x2
	//   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
	for ( var i = 0; i < digitos.length; i++  ) {
            // Preenche a soma com o dígito vezes a posição
            soma_digitos = soma_digitos + ( digitos[i] * posicoes );
	    
            // Subtrai 1 da posição
            posicoes--;
	    
            // Parte específica para CNPJ
            // Ex.: 5-4-3-2-9-8-7-6-5-4-3-2
            if ( posicoes < 2 ) {
		// Retorno a posição para 9
		posicoes = 9;
            }
	}
	
	// Captura o resto da divisão entre soma_digitos dividido por 11
	// Ex.: 196 % 11 = 9
	soma_digitos = soma_digitos % 11;
	
	// Verifica se soma_digitos é menor que 2
	if ( soma_digitos < 2 ) {
            // soma_digitos agora será zero
            soma_digitos = 0;
	} else {
            // Se for maior que 2, o resultado é 11 menos soma_digitos
            // Ex.: 11 - 9 = 2
            // Nosso dígito procurado é 2
            soma_digitos = 11 - soma_digitos;
	}
	
	// Concatena mais um dígito aos primeiro nove dígitos
	// Ex.: 025462884 + 2 = 0254628842
	var cpf = digitos + soma_digitos;
	
	// Retorna
	return cpf;
    
    } // calc_digitos_posicoes
    return calc_digitos_posicoes;
});

mod.factory('Config', function(Search,avalancheConfig) {
    return {
	search: function(success,error) {
	    if(success==undefined)
		success = function(){};
	    if(error==undefined)
		error = function(){};

	    return Search.search({ 
		path: avalancheConfig.path.config,
		limit: 1
	    },success,error);
	}
    }
});

mod.factory('Form', function($q, Documents, Folders) {
    var form = {};

    var processForm = function(params, formData, data, success) {
	formData.attrs = data.attrs;
	formData.children = data.children;
	formData.nodeService = params.nodeService;
	formData.path = params.path;

	formData.fields = {};
	for(var i=formData.children.length-1; i>=0;i--) {
	    // field by name refs
	    var fieldName = formData.children[i].attrs.fieldName;
	    formData.fields[fieldName] = formData.children[i];
	    if(formData.children[i].attrs.type=='objectList') {
		formData.fields[fieldName].fields = {};
		for(var j=0; j<formData.fields[fieldName].children.length;j++) {
		    var subFieldName = formData.fields[fieldName].children[j].attrs.fieldName;
		    formData.fields[fieldName].fields[subFieldName]=formData.fields[fieldName].children[j];
		}
	    }
	    
	    // exclude some fields from children
	    if(params.excludeFields && 
	       params.excludeFields.indexOf(formData.children[i].attrs.fieldName)!=-1)
		formData.children.splice(i,1);
	}
	
	// exclude some fields - values
	if(params.excludeFields)
	    for(var i=0;i<params.excludeFields.length;i++)
		delete formData.attrs[params.excludeFields[i]];
	formData.$promise.resolve();
	if(typeof(success)=='function')
	    success(formData);
    }

    form.get = function(params,success,error) {

	var formData = {
	    $promise: $q.defer()
	}

	if(typeof(params)==undefined)
	    params = {};

	if(params.node==undefined) {
	    if(params.nodeService==undefined) {
		if(params.folder || params.folderId)
		    params.nodeService = Folders;
		else
		    params.nodeService = Documents;
	    }

	    var getParams = {
		docId: params.docId,
		folderId: params.folderId,
		path: params.path,
		docLinks: params.docLinks
	    }
	    if(params.extraParams!=undefined) {
		angular.extend(getParams, params.extraParams);
	    }

	    var data = params.nodeService.get(getParams,function() {
		processForm(params, formData, data, success);

		if(params.folder) {
		    if(formData.attrs.folderId) 
			formData.attrs.folderPath = formData.fields.path.attrs.folderPath;
		    else
			formData.attrs.folderPath = params.folderPath;
		}
	    },function(){
		formData.$promise.reject();
		if(typeof(error)=='function')
		    error();
	    });
	} else {
	    processForm(params, formData, params.node, success);
	}
	return formData;
    }

    return form;
});

mod.factory('ChangePasswordModal', function($modal) {
    var ChangePasswordModal = function(options) {
	if(options==undefined)
	    options = {};
	if(options.template == undefined)
	    options.template = 'avChangePasswordModal.html';
	if(options.animation == undefined)
	    options.animation = 'am-fade-and-scale';

	var modal = $modal(options);
    }
    return ChangePasswordModal;
});

mod.factory('ConfirmModal', function($modal,$rootScope,$timeout) {
    var ConfirmModal = function(options) {
	if(options==undefined)
	    options = {};
	if(options.template == undefined)
	    options.template = 'avConfirmModal.html';
	if(options.animation == undefined)
	    options.animation = 'am-fade-and-scale';
	options.backdrop='static';
	options.keyboard=false;

	var modal;
	var scope = $rootScope.$new();
	scope.yes = function($hide) {
	    if(typeof(options.yes)=='function')
		options.yes();
	    $hide();
	    $timeout(function() {
		modal.destroy();
		scope.$destroy();
	    },0,false);
	}
	scope.no = function($hide) {
	    if(typeof(options.no)=='function')
		options.no();
	    $hide();
	    $timeout(function() {
		modal.destroy();
		scope.$destroy;
	    },0,false);
	}

	options.scope = scope;

	modal = $modal(options);
    }
    return ConfirmModal;
});

mod.factory('Loading', function($rootScope,$q) {
    var taskList = [];
    $rootScope.loading = false;
    $rootScope.block = false;

    var Loading = {};

    Loading.beginTasks = function(block) {
	if(!$rootScope.loading)
	{
	    taskList = [];
	    if(block==undefined || block)
		$rootScope.block = true;
	    $rootScope.loading = true;
	}
    }

    Loading.pushTask = function() {
	var task = $q.defer();
	taskList.push(task.promise);
	return task;
    }

    Loading.watchTasks = function(onSuccess,onError) {
	$q.all(taskList).then(function(result) {
	    $rootScope.loading = false;
	    $rootScope.block = false;
	    if(angular.isDefined(onSuccess))
		onSuccess(result);
	}, function() {
	    $rootScope.loading = false;
	    $rootScope.block = false;
	    if(angular.isDefined(onError))
		onError();
	});
    };

    return Loading;
});

mod.factory('FileViewer', function($modal,$window,$rootScope,avalancheConfig) {

    var factory = function(options) {
	var FileViewer = {};
    
	var fileSrc = avalancheConfig.backendAddress+'/file/'+options.doc.docId;

	if((options.doc.contentType=='application/pdf' ||
	    options.doc.contentType=='text/html' ||
	    options.doc.contentType.indexOf('image')==0) &&
	   !bowser.msie) {

	    var scope = $rootScope.$new();
	    scope.fileSrc = fileSrc;
	    scope.doc = options.doc;
	    // open viewer only for pdf and images
	    var modalOptions = {
		contentTemplate: 'avFileViewer.html',
		template: 'avFileViewerModal.html',
		scope: scope
		//animation: 'am-fade-and-scale' //iframe resizing
	    }
	    if(options.contentTemplate)
		modalOptions.contentTemplate = options.contentTemplate;
	    if(options.container)
		modalOptions.container = options.container;
	    if(options.show != undefined)
		modalOptions.show = options.show;
	    modalOptions.title = scope.doc.title;

	    FileViewer.modalInstance = $modal(modalOptions);

	    FileViewer.show = function() {
		FileViewer.modalInstance.show();
	    }

	    FileViewer.destroy = function() {
		FileViewer.modalInstance.destroy();
		scope.$destroy();
	    }
	} else {
	    FileViewer.show = function() {
		$window.open(scope.fileSrc,'_blank');
	    }

	    FileViewer.destroy = function() {
	    }
	}
	return FileViewer;
    }

    return factory;
});

mod.factory('NodeModal', function($rootScope,$modal,$interval,$translate,Form,History,Loading,avalancheConfig,$alert) {
    var translations;
    $translate(['alertErrorTag','alertWarningTag','savingTag','uploadingTag','trueTag','falseTag','adminCancelTag']).then(function (data) {
	translations = data;
    });

    var NodeModal = function (params) {

	Loading.beginTasks();
	var load = Loading.pushTask();
        Loading.watchTasks();
	var nodeModal = undefined;
	var data = Form.get(params, function() {
	    var scope = $rootScope.$new();
	    scope.translations = translations;
	    scope.node = data;

	    scope.nodeModalParams = params;

	    // on open
	    if(typeof(params.onOpen)=='function')
		params.onOpen(scope);

	    // dirty control
	    scope.nodeModalParams.dirty = false;
	    scope.nodeModalParams.formTemplate=params.formTemplate ||
		'avForm.html';
	    scope.saving = false;

	    if(params.buttons == undefined) {
		scope.buttons = {};
	    } else {
		scope.buttons = params.buttons;
	    }
	    if(!angular.isDefined(scope.buttons.history))
		scope.buttons.history = true;
	    if(!angular.isDefined(scope.buttons.remove))
		scope.buttons.remove = true;
	    if(!angular.isDefined(scope.buttons.ok))
		scope.buttons.ok = true;

	    if(params.labels == undefined) {
		scope.labels = {};
	    } else {
		scope.labels = params.labels;
	    }
	    if(!angular.isDefined(scope.labels.cancelTag))
		scope.labels.cancelTag = translations['adminCancelTag'];

	    // show modal
	    var show = function() {
		return $modal({
		    contentTemplate: params.contentTemplate || 
			'avNodeModal.html',
		    scope: scope,
		    animation: 'am-fade-and-scale',
		    prefixEvent: 'av.node.modal'
		});
	    }

	    nodeModal = show();

	    scope.save = function() {
		// don't save if it isn't needed
		if(!scope.nodeModalParams.dirty || scope.saving)
		{
		    nodeModal.hide();
		    return;
		}

		scope.saving = true;
		nodeModal.hide();

		var fields = (typeof(scope.nodeModalParams.extraFields)=='object') ? 
		    angular.extend({}, scope.node.attrs, scope.nodeModalParams.extraFields) : scope.node.attrs;

		var savingAlert = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    content: translations['savingTag']+'...',
		    template: 'avAlert.html',
		    type: 'info',
		    show: true,
		    container: 'body',
		    dismissable: true
		});

		var saveParams = {
		    docId: fields.docId,
		    folderId: fields.folderId,
		};

		if(!scope.nodeModalParams.folder && !fields.docId)
		    saveParams.path = scope.nodeModalParams.path;


		if(scope.nodeModalParams.extraParams!=undefined) {
		    angular.extend(saveParams, scope.nodeModalParams.extraParams);
		}
		params.nodeService.save(saveParams,fields,function(node){
		    // documento salvo
		    savingAlert.hide();
		    scope.$destroy();
		    if(typeof(params.onSave)=='function')
			params.onSave(node);
		}, function(resp) {
		    scope.saving = false;
		    savingAlert.hide();
		    nodeModal = show();
		    var error = $alert({
			animation: 'am-slide-top',
			placement: 'top-right',
			content: resp.data.statusMessage,
			template: 'avAlert.html',
			type: 'danger',
			show: true,
			container: 'body',
			duration: 5,
			dismissable: true
		    });
		});
	    }

	    scope.delete = function() {
		scope.saving = true;
		nodeModal.hide();

		var undoData;
		if(typeof(params.onBeforeDelete)=='function')
		    undoData = params.onBeforeDelete();

		var deletingAlert = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    template: 'avAlertDelete.html',
		    type: 'warning',
		    show: true,
		    container: 'body',
		    dismissable: true,
		    scope: scope
		});
		var deleteInterval = $interval(function() {
		    deletingAlert.hide();
		    params.nodeService.delete({
			docId: scope.node.attrs.docId,
			folderId: scope.node.attrs.folderId,
			path: scope.nodeModalParams.path 
		    },function(){
			// documento removido
			scope.$destroy();
			if(typeof(params.onDelete)=='function')
			    params.onDelete();
		    }, function(resp) {
			scope.saving = false;
			nodeModal = show();
			if(typeof(params.onUndoDelete)=='function')
			    params.onUndoDelete(undoData);
			var error = $alert({
			    animation: 'am-slide-top',
			    placement: 'top-right',
			    content: resp.data.statusMessage,
			    template: 'avAlert.html',
			    type: 'danger',
			    show: true,
			    container: 'body',
			    duration: 5,
			    dismissable: true
			});
		    });
		},avalancheConfig.deleteTimeout,1);

		scope.undoDelete = function($event) {
		    $event.preventDefault();
		    $interval.cancel(deleteInterval);
		    scope.saving = false;
		    deletingAlert.hide();
		    nodeModal = show();
		    if(typeof(params.onUndoDelete)=='function')
			params.onUndoDelete(undoData);
		}

	    }

	    scope.$on('av.node.modal.hide',function(){
		nodeModal.destroy();
		nodeModal = null;
		if(!scope.saving)
		{
		    if(scope.nodeModalParams.dirty)
		    {
			var error = $alert({
			    animation: 'am-slide-top',
			    placement: 'top-right',
			    template: 'avDiscard.html',
			    type: 'warning',
			    show: true,
			    container: 'body',
			    dismissable: true,
			    scope: scope
			});
			var discartInterval = $interval(function() {
			    error.hide();
			    scope.$destroy();
			},avalancheConfig.discardTimeout,1);
			scope.undoDiscard = function() {
			    $interval.cancel(discartInterval);
			    error.hide();
			    nodeModal = show();
			    
			}

		    }
		    else
			scope.$destroy();
		}
	    });

	    if(params.docId && scope.buttons.history)
	    {
		var history = History.query({ 
		    docId: params.docId
		}, function() {
		    scope.history = history;
		});
	    }

	    load.resolve();

	},function() {
	    load.reject();
	});

	var DestroyNodeModal = function() {
	    if(nodeModal)
		nodeModal.hide();
	}

	return DestroyNodeModal;
    }

    return NodeModal;
});

mod.factory('FileList', function(Upload,avalancheConfig,$alert,$rootScope) {

    var FileList = function(scope) {
	scope.thumbUrl = avalancheConfig.backendAddress+'/thumb/';
	scope.removeSelectedDisabled = true;

	var checkNoneSelected = function() {
	    var values = eval('scope.values.'+scope.field.attrs.fieldName);
	    for(var i=0; i<values.length; i++)
		if(values[i].active)
		    return false;
	    return true;
	}

	scope.$watch('values.'+scope.field.attrs.fieldName,function(newValue,oldValue){
 	    if(oldValue==newValue || oldValue && newValue && oldValue.length == newValue.length)
		return;

	    scope.avForm.$setDirty();
	},true);

	scope.remove = function($index,checkSelected) {
	    var values = scope.values[scope.field.attrs.fieldName];
	    
	    if(checkSelected==undefined || checkSelected)
		scope.removeSelectedDisabled = checkNoneSelected();
	    values.splice($index,1);			  
	}
	
	scope.removeSelected = function() {
	    var values = eval('scope.values.'+scope.field.attrs.fieldName);
	    for(var i=values.length-1; i >= 0; i--)
		if(values[i].active)
		    scope.remove(i,false);
	    scope.removeSelectedDisabled = true;
	}

	scope.selectAll = function() {
	    var values = eval('scope.values.'+scope.field.attrs.fieldName);

	    // verifica se todos estão selecionados
	    var allSelected = true;
	    for(var i=0; i<values.length && allSelected; i++)
		if(!values[i].active)
		    allSelected = false;

	    // se todos estiverem selecionados remove seleção
	    // senão, seleciona todos
	    for(var i=0; i<values.length; i++)
		values[i].active = (allSelected)?false:true;
	    
	    scope.removeSelectedDisabled = (allSelected)?true:false;
	}

	scope.onSelectedChange = function(file) {
	    scope.removeSelectedDisabled = (file.active)?false:checkNoneSelected();
	}

	scope.select = function(file,$event) {
	    $event.preventDefault();
	    file.active = !file.active;
	    scope.onSelectedChange(file);
	}


	scope.upload = function(files) {
	    var maxFiles = scope.field.attrs.maxFiles;
	    var values = scope.values[scope.field.attrs.fieldName];
	    if(values==null)
		values = [];
	    if(maxFiles==undefined)
		maxFiles = 0;
	    if (files && files.length) {
		if(maxFiles>0)
		{
		    if(values.length >= maxFiles)
			return;
		    
		    if(files.length+values.length>maxFiles)
			files.splice(maxFiles-values.length,files.length+values.length-maxFiles);
		}
		alertScope = scope.$new();
		alertScope.content = scope.translations['uploadingTag']+'...';
		var uploadingAlert = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    template: 'avAlert.html',
		    type: 'info',
		    show: true,
		    container: 'body',
		    dismissable: true,
		    scope: alertScope
		});
		Upload.upload({
		    url: avalancheConfig.backendAddress+'/upload',
		    fields: {
			'path': avalancheConfig.path.files
		    },
		    file: files,
		    fileFormDataName: 'files[]'
		}).progress(function (evt) {
		    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		    alertScope.content = scope.translations['uploadingTag']+'... '+progressPercentage + '%';
		    //console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
		}).success(function (data, status, headers, config) {
		    uploadingAlert.hide();
		    alertScope.$destroy();
		    scope.values[scope.field.attrs.fieldName]=values.concat(data);
		}).error(function (data, status, headers, config) {
		    //console.log('error status: ' + status);
		    uploadingAlert.hide();
		    alertScope.$destroy();
		    var error = $alert({
			animation: 'am-slide-top',
			placement: 'top-right',
			content: data.statusMessage,
			template: 'avAlert.html',
			type: 'danger',
			show: true,
			container: 'body',
			duration: 5,
			dismissable: true
		    });
		})
	    }
	}
    }

    return FileList;
});

mod.factory('SelectField', function() {
    var field = {};

    field.getTextFromField = function(field,value) {
	for(var i=0; i<field.children.length; i++) {
	    if(field.children[i].attrs.id==value) 
		return field.children[i].attrs.value;
	}
    }

    field.getTextFromChildren = function(fieldName,children,values) {
	if(typeof(fieldName)!='object') {
	    fieldName = [fieldName];
	}

	var textList = {};
	var fieldNameFound = [];

	// search for fields on children
	for(var i=0; i<children.length && fieldName.length>0 ; i++) {
	    
	    for(var j=0; j<fieldName.length; j++) {
		
		if(children[i].attrs.fieldName == fieldName[j]) {
		    textList[fieldName[j]] = field.getTextFromField(children[i],values[fieldName[j]]);

		    // once the field was found, doesn't need to search for it anymore
		    fieldNameFound.concat(fieldName.splice(j,1));
		    break;
		}
	    }
	}

	if(fieldNameFound.length==1)
	    return textList[fieldNameFound[0]];
	return textList;
    }


    return field;
});

mod.provider('RuntimeStates', function RuntimeStates($stateProvider,$urlRouterProvider) {
    this.stateList = [];
    var provider = this;
    this.$get = function() {
	return {
	    addState: function(name, state){
		if(provider.stateList.indexOf(name)==-1)
		{
		    provider.stateList.push(name);
		    $stateProvider.state(name,state);
		}
	    },
	    otherwise: function(url){
		$urlRouterProvider.otherwise(url);		
	    }
	}
    }
});
