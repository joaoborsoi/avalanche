var mod = angular.module('avalanche.filters', []);

mod.filter('mysqlDatetimeToISO', function() {
  return function(badTime) {
    var goodTime = badTime.replace(/(.+) (.+)/, "$1T$2");
    return goodTime;
  };
});

mod.filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';
	
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;
	
        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }
	
        return value + (tail || ' …');
    };
});

mod.filter('typeaheadHighlight', function() {
    function escapeRegexp(queryToEscape) {
	return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
    }
    
    return function(matchItem, query) {
	return query && typeof(query)=='string' ? ('' + matchItem).replace(new RegExp(escapeRegexp(query), 'gi'), '<strong>$&</strong>') : matchItem;
    };
});

mod.filter('cnpj', function() {
    return function(value) {
        return value.substr(0,2) + '.' + value.substr(2,3) + '.' + 
	    value.substr(5,3) + '/' + value.substr(8,4) + '-'+
            value.substr(12,14);
    }
});

mod.filter('cpf', function() {
    return function(value) {
        return value.substr(0,3) + '.' + value.substr(3,3) + '.' + 
	    value.substr(6,3) + '-' + value.substr(9,2);
    }
});

mod.filter('cep',function() {
    return function(value) {
	return value.substr(0,5)+'-'+value.substr(5,3);
    }
});

mod.filter('currency',function($locale,$filter) {
    return function(value, decimals) {
	if(decimals==undefined)
	    decimals = 2;
	return $locale.NUMBER_FORMATS.CURRENCY_SYM+$filter('number')(value, decimals);
    }
});

mod.filter('htmlToPlaintext', function() {
    return function(text) {
	return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});
