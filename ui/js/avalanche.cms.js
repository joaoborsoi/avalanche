var mod = angular.module('avalanche.cms', ['avalanche.services']);

mod.config(function($translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('cms');
});

mod.factory('Contact', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/contact/',null);
});

mod.controller('CMS_Area',function($scope,$state,Loading,Folders){
    Loading.beginTasks();
    var load = Loading.pushTask();
    Loading.watchTasks();

    var folder = Folders.get({
	folderId: $state.current.folderId
    }, function() {
	$scope.folder = folder.attrs;
	load.resolve();
    }, function() {
	load.reject();
    });
});

mod.controller('CMS_Generic',function($scope,$controller,$state,Loading,Search,avalancheConfig){
    $controller('CMS_Area',{$scope: $scope});
    
    $scope.thumbUrl = avalancheConfig.backendAddress+'/thumb/';

    Loading.beginTasks();
    var load = Loading.pushTask();
    Loading.watchTasks();

    $scope.docList = Search.search({
	path: $state.current.path,
	orderBy: 'fixedOrder',
    }, function() {
	load.resolve();
    }, function() {
	load.reject();
    });
});

mod.controller('CMS_Contact',function($scope,$controller,$alert,$translate,Form,Contact,Loading,avalancheConfig,INVALID_FIELD) {
    $controller('CMS_Area',{$scope: $scope});

    var translations;
    $translate(['cmsContactSentTag']).then(function (data) {
	translations = data;
    });

    Loading.beginTasks();
    var load = Loading.pushTask();
    Loading.watchTasks();

    var origNode = Form.get({
	path: avalancheConfig.path.contactForm,
	excludeFields: ['userId','groupId','userRight','groupRight',
			'otherRight','creationDate','lastChanged',
			'lastChangedUserId','keywords']
    }, function() {
	$scope.node = angular.copy(origNode);
	load.resolve();
    }, function() {
	load.reject();
    });

    $scope.send = function() {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();

	var res = Contact.save($scope.node.attrs, function() {
	    $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: translations['cmsContactSentTag'],
		template: 'avAlert.html',
		type: 'info',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	    $scope.node = angular.copy(origNode);
	    $scope.avForm.$setPristine();
	    load.resolve();
	}, function(resp) {
	    if(resp.data.statusCode==INVALID_FIELD &&
	       resp.data.data.fieldName=='validator')
		$scope.avForm.validator.$invalid = true;
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	    load.reject();
	});
    }
});

mod.controller('CMS_Publication',function($scope,$controller,$state,Loading,Search){
    $controller('CMS_Area',{$scope: $scope});
    
    Loading.beginTasks();
    var load = Loading.pushTask();
    Loading.watchTasks();

    $scope.docList = Search.search({
	path: $state.current.path,
	orderBy: 'publicationDate',
	order: '0'
    }, function() {
	load.resolve();
    }, function() {
	load.reject();
    });
});

mod.controller('CMS_Profile',function($controller,$scope){
    $controller('CMS_Generic',{$scope:$scope});
});

