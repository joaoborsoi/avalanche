var mod = angular.module('avalanche.ctrls', ['avalanche.services','ngFileUpload']);

mod.config(function($translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('form');
});

mod.controller('LoginFormCtrl', function ($rootScope,$scope,$alert,Login,Loading,FIELD_REQUIRED){
    $scope.twoFactorAuth = false;
    $scope.loginData = {};

    // Perform the login action when the user submits the login form
    $scope.submit = function() {
	if(!$scope.loginForm.$valid)
	{
	    $scope.loginForm.$setSubmitted();
	    return;
	}

	// login code
	Loading.beginTasks();
	var load = Loading.pushTask();
        Loading.watchTasks();
	Login.save($scope.loginData, function(value, responseHeaders){
	    load.resolve();
	    $rootScope.session = value;
	},function(httpResponse){
	    if(httpResponse.data.statusCode==FIELD_REQUIRED &&
	       httpResponse.data.data.fieldName=='authCode') {
		$scope.twoFactorAuth = true;
		var unreg = $scope.$watch('loginData.login',function(newValue,oldValue){
		    if(newValue==oldValue)
			return;

		    $scope.twoFactorAuth = false;
		    delete $scope.loginData.authCode;
		    unreg();
		});
	    } else {
		var error = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    content: httpResponse.data.statusMessage,
		    template: 'avAlert.html',
		    type: 'danger',
		    show: true,
		    container: 'body',
		    duration: 5,
		    dismissable: true
		});	    
	    }
	    load.reject();
	});
    };
});

mod.controller('LibraryCtrl', function($scope,$translate){
    $translate(['alertErrorTag','alertWarningTag','savingTag','uploadingTag','trueTag','falseTag']).then(function (translations) {
	$scope.translations = translations;
    });
});

mod.controller('NodeModalCtrl',function($scope,$controller){
    $scope.showHistory=false;

    if($scope.nodeModalParams.controller)
	$controller($scope.nodeModalParams.controller, {$scope: $scope});

    $scope.$watch('avForm.$dirty', function (newValue, oldValue) {
	if(!$scope.nodeModalParams.dirty && newValue==true)
	    $scope.nodeModalParams.dirty = true;
    });

    $scope.aSortableListeners = {
	dragMove: function(event) {
	    $scope.dragMove(event);
	},
	dragEnd: function(event) {
	    $scope.dragEnd(event);
	}
    }
});

mod.controller('HistoryCtrl',function($scope,HistoryFormatter){
    var field;
    var parentField = null;
    if($scope.item.parentField)
    {
	parentField = eval('$scope.node.fields.'+$scope.item.parentField);
	field = eval('parentField.fields.'+$scope.item.fieldName);
	$scope.label = parentField.attrs.label+' > '+field.attrs.label;
    }
    else
    {
	field = eval('$scope.node.fields.'+$scope.item.fieldName);
	$scope.label = field.attrs.label;
    }
    $scope.value = HistoryFormatter.format(field,$scope.item.value,$scope.translations);
    $scope.field = field;
});
	

mod.controller('ChangePasswordFormCtrl', function($scope,$state,$alert,User,Loading) {
    // Perform the login action when the user submits the login form
    $scope.avFormSubmit = function() {
	if(!$scope.changePasswordForm.$valid) {
	    $scope.changePasswordForm.$setSubmitted();
	    return;
	}
	// login code
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	var savingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: $scope.translations['savingTag']+'...',
	    template: 'avAlert.html',
	    type: 'info',
	    show: true,
	    container: 'body',
	    dismissable: true
	});
	User.save({
	    userId: $scope.session.userId
	}, $scope.formData, function(value, responseHeaders){
	    load.resolve();
	    savingAlert.hide();
	    $scope.$hide();
	},function(httpResponse){
	    load.reject();
	    savingAlert.hide();
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: httpResponse.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });	    
	});
    };
});
