var mod = angular.module('avalanche.legis',['avalanche','avalanche.cms','avalanche.collapse'])

mod.config(function($translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('legis');
});

//
// Services
//

mod.factory('SearchProcess', function(Search,avalancheConfig,$alert) {
    return {
	search: function(params,success,error) {
	    var newParams;
	    if(typeof(params)!='object')
		newParams = {};
	    else
		newParams = angular.copy(params);

	    newParams.path = avalancheConfig.path.process;
	    if(!newParams.searchCriteria)
		newParams.searchCriteria = avalancheConfig.searchCriteria.processos;

	    var data = Search.search(newParams, function() {
		if(success!=undefined)
		    success();

		if(data.children.length==0) {
		    var error = $alert({
			animation: 'am-slide-top',
			placement: 'top-right',
			content: 'Nenhum processo encontrado',
			template: 'avAlert.html',
			type: 'warning',
			show: true,
			container: 'body',
			duration: 5,
			dismissable: true
		    });
		}
	    },error);
	    return data;
	}
    }
    
});

mod.factory('ProcessTree', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/process-tree/:docId');
});

mod.factory('Process', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/process/:docId?path=:path&parentId=:parentId&fieldName=:fieldName&sourceId=:sourceId');
});

mod.factory('Receipt', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/process-receipt/:processId/:entityId');
});

mod.factory('History', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/process-history/:docId?processId=:processId');
});

mod.factory('LawyerCommitments', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/lawyer-commitments/');
});

mod.factory('LawyerHistory', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/lawyer-history/');
});

mod.factory('LawyerInactivityCheck', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/lawyer-inactivity-check/');
});

mod.factory('CustomerSummary', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/customer-summary/');
});

mod.factory('AggregatorHistory', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/aggregator-history/');
});

mod.factory('Customer', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/customer/:docId?path=:path&fieldName=:fieldName&sourceId=:sourceId');
});

mod.factory('SearchCustomer', function(Search,avalancheConfig,$alert) {
    return {
	search: function(params,success,error) {
	    var newParams;
	    if(typeof(params)!='object')
		newParams = {};
	    else
		newParams = angular.copy(params);
	    newParams.path = avalancheConfig.path.customer;
	    if(!newParams.searchCriteria)
		newParams.searchCriteria = avalancheConfig.searchCriteria.customer;
	    newParams.recursiveSearch = '1';
	    if(error==undefined)
		error = function(){};

	    var data = Search.search(newParams, function() {
		if(success!=undefined)
		    success();

		if(data.children.length==0) {
		    var error = $alert({
			animation: 'am-slide-top',
			placement: 'top-right',
			content: 'Nenhum cliente encontrado',
			template: 'avAlert.html',
			type: 'warning',
			show: true,
			container: 'body',
			duration: 5,
			dismissable: true
		    });
		}
	    },error);
	    return data;
	}
    }
});

mod.factory('CustomerLabels', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/customer-labels/');
});

mod.factory('Representative', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/representative/:docId');
});

mod.factory('SearchProvider', function(Search,avalancheConfig,$alert) {
    return {
	search: function(params,success,error) {
	    var newParams;
	    if(typeof(params)!='object')
		newParams = {};
	    else
		newParams = angular.copy(params);
	    newParams.path = avalancheConfig.path.provider;
	    if(!newParams.searchCriteria)
		newParams.searchCriteria = avalancheConfig.searchCriteria.provider;
	    newParams.recursiveSearch = '1';
	    if(success==undefined)
		success = function(){};

	    var data = Search.search(newParams, function() {
		if(success!=undefined)
		    success();

		if(data.children.length==0) {
		    var error = $alert({
			animation: 'am-slide-top',
			placement: 'top-right',
			content: 'Nenhum fornecedor encontrado',
			template: 'avAlert.html',
			type: 'warning',
			show: true,
			container: 'body',
			duration: 5,
			dismissable: true
		    });
		}
	    },error);
	    return data;
	}
    }
});

mod.factory('SearchUser', function(Search,avalancheConfig,$alert) {
    return {
	search: function(params,success,error) {
	    var newParams;
	    if(typeof(params)!='object')
		newParams = {};
	    else
		newParams = angular.copy(params);
	    newParams.path = avalancheConfig.path.user;
	    if(!newParams.searchCriteria)
		newParams.searchCriteria = avalancheConfig.searchCriteria.user;
	    newParams.recursiveSearch = '1';
	    if(success==undefined)
		success = function(){};

	    var data = Search.search(newParams, function() {
		if(success!=undefined)
		    success();

		if(data.children.length==0) {
		    var error = $alert({
			animation: 'am-slide-top',
			placement: 'top-right',
			content: 'Nenhum usuário encontrado',
			template: 'avAlert.html',
			type: 'warning',
			show: true,
			container: 'body',
			duration: 5,
			dismissable: true
		    });
		}
	    },error);
	    return data;
	}
    }
});

mod.factory('SearchMail', function(Search,avalancheConfig) {
    return {
	search: function(params,success,error) {
	    var newParams;
	    if(typeof(params)!='object')
		newParams = {};
	    else
		newParams = angular.copy(params);
	    newParams.path = avalancheConfig.path.mail;

	    if(!newParams.searchCriteria)
		newParams.searchCriteria = avalancheConfig.searchCriteria.mail;

	    if(newParams.filter && newParams.filter.trim()!='') {
		newParams.orderBy = 'matches';
		newParams.order = '1';
	    }
	    else {
		newParams.orderBy = 'msgDate';
		newParams.order = '0';
	    }
	    newParams.limit = 50;

	    var data = Search.search(newParams, function() {
		if(success!=undefined)
		    success(data);
	    },error);
	    return data;
	}
    }    
});

mod.factory('SearchEntry', function(Search,avalancheConfig,$alert) {
    return {
	search: function(params,success,error) {
	    var newParams;
	    if(typeof(params)!='object')
		newParams = {};
	    else
		newParams = angular.copy(params);
	    newParams.path = avalancheConfig.path.entry;
	    if(success==undefined)
		success = function(){};

	    var data = Search.search(newParams, function() {
		if(success!=undefined)
		    success();

		if(data.children.length==0) {
		    var error = $alert({
			animation: 'am-slide-top',
			placement: 'top-right',
			content: 'Nenhum lançamento encontrado',
			template: 'avAlert.html',
			type: 'warning',
			show: true,
			container: 'body',
			duration: 5,
			dismissable: true
		    });
		}
	    },error);
	    return data;
	}
    }
});

mod.factory('Qualification', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/qualification/');
});

mod.factory('Expenses', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/expenses/:docId');
});

mod.factory('Accountability', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/accountability/:docId');
});

mod.factory('PDFMail', function($resource,avalancheConfig) {
    return $resource(avalancheConfig.backendAddress+'/pdf-mail');
});

mod.factory('ProcessReport', function($resource,avalancheConfig,$alert) {
    return $resource(avalancheConfig.backendAddress+'/process-report',null,
		     { search:  {method:'POST'}});  
});

mod.factory('CustomerReport', function($resource,avalancheConfig,$alert) {
    return $resource(avalancheConfig.backendAddress+'/customer-report',null,
		     { search:  {method:'POST'}});  
});

mod.factory('EntryReport', function($resource,avalancheConfig,$alert) {
    return $resource(avalancheConfig.backendAddress+'/entry-report',null,
		     { search:  {method:'POST'}});  
});



//
// Controllers
//
mod.controller('MainCtrl', function($scope, $rootScope, $controller, $state, $urlRouter,$window,Config,States,Session,RuntimeStates,Loading,avalancheConfig){
    $controller('LibraryCtrl', {$scope: $scope});

    $scope.config = Config.search();
    $scope.$state = $state;
    $scope.dateFormat = avalancheConfig.dateFormat;
    $scope.datetimeFormat = avalancheConfig.datetimeFormat;

    $scope.permission = function(state) {
	if(typeof(state)!='object')
	    state = [state];

	var ret = [];
	for(var k=0; k<state.length; k++)
	{
	    var notFound = true;
	    for(var i=0; i<$rootScope.session.groups.length && notFound; i++)
	    {
		for (var j=0; $rootScope.session.groups[i].permissions &&
		     j<$rootScope.session.groups[i].permissions.length && notFound; j++)
		{
		    if($rootScope.session.groups[i].permissions[j].indexOf(state[k])==0 &&
		       (state[k].length==$rootScope.session.groups[i].permissions[j].length ||
		        $rootScope.session.groups[i].permissions[j][state[k].length]=='.'))
		    {
			ret.push(state[k]);
			notFound = false;
		    }
		}
	    }
	}

	if(state.length==1)
	    return (ret.length==1)
	return ret;
    }

    $scope.menuClick = function() {
	$scope.menuOpen = !$scope.menuOpen;
    }

    $scope.menuItemClick = function(item) {
	if(!$state.includes(item.name))	{
	    $state.go(item.name);
	    $scope.menuOpen = false;
	}
    }

    var loadMenu = function(states) {
	$scope.menu = [];
	if($scope.session.userId) {
	    var menuPerms = $scope.permission(['app.summary','app.process',
					       'app.customer','app.provider',
					       'app.user','app.entry','app.docs',
					       'app.configs','app.customer-summary']);

	    for(var i=0; i<states.length; i++) {
		var item = states[i];
		if(item.menuTitle && menuPerms.indexOf(item.name)!=-1)
		    $scope.menu.push(item);
	    }
	} else {
	    for(var i=0; i<states.length; i++) {
		var item = states[i];
		if(item.menuTitle && item.name.indexOf('app')==-1)
		    $scope.menu.push(item);
	    }
	}
    }


    // carrega estados
    Loading.beginTasks();
    var load = Loading.pushTask();
    Loading.watchTasks();
    var states = States.query(function() {
	for(var i=0; i<states.length; i++) {
	    RuntimeStates.addState(states[i].name, states[i]);
	}

	$rootScope.session = {};
	$rootScope.$watch('session',function(newValue,oldValue){
	    if(!oldValue.userId)
	    {
		if(newValue.userId)
		{
		    loadMenu(states);
		    $state.go($scope.menu[0].name);
		}
	    }
	    else
	    {
		if(!newValue.userId)
		{
		    loadMenu(states);
		    $state.go('site.home');
		}
	    }
	});

	$rootScope.session = Session.get(function(){
	    loadMenu(states);
	    RuntimeStates.otherwise($state.href($scope.menu[0].name).substr(1));
	    $urlRouter.sync();
	    $urlRouter.listen();
	}, function() {
	    loadMenu(states);
	    RuntimeStates.otherwise('/home');
	    $urlRouter.sync();
	    $urlRouter.listen();
	});

	load.resolve();
    }, function() {
	load.reject();
    });

    $scope.scrollToTop = function() {
	$window.scrollTo(0,0);
    }

    $scope.scrollToBottom = function() {
	$window.scrollTo(0,$window.document.body.scrollHeight);
    }

});

mod.controller('TopBarCtrl',function($scope,$state){
    $scope.$on('$stateChangeSuccess',
	       function(event, toState, toParams, fromState, fromParams ){
		   var path = toState.name.split('.');
		   if(path.length==2) {
		       if(toState.menuTitle)
			   $scope.menuTitle = toState.menuTitle;
		       else
			   $scope.menuTitle = "";
		   } else if(path.length>2 && fromState.name=="") {
		       var parent = $state.get(path[0]+'.'+path[1]);
		       if(parent.menuTitle) 
			   $scope.menuTitle = parent.menuTitle;
		       else
			   $scope.menuTitle = "";
		   }
	       });

});

mod.controller('AppCtrl', function($scope,$rootScope,$state,Config,ConfirmModal,Logout,Loading){
    $scope.aggregator = $scope.permission('app.aggregator');
    $scope.undoData = {};
    $scope.config = Config.search();

    var checkDirty;
    $scope.setCheckDirty = function(checkDirtyRef) {
	if(checkDirtyRef)
	    checkDirty = checkDirtyRef;
	else
	    checkDirty = function() {
		return false;
	    }
    }
    $scope.setCheckDirty(null);

    $scope.logginOut = false;

    var doLogout = function() {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	$scope.logginOut = true;
	Logout.save(function(){
	    load.resolve();
	    $rootScope.session = {};
	    $state.go('site.home');
	},function(){
	    load.reject();
	});
    }

    $scope.logout = function() {
	if(!checkDirty()) {
	    doLogout();
	    return;
	}

	ConfirmModal({
	    title: 'Atenção',
	    content: 'Existem alterações não salvas. Sair mesmo assim?',
	    yes: function() {
		doLogout();
	    }
	});
    }

});

mod.controller('AppTopBarCtrl', function($scope,ChangePasswordModal){
    $scope.dropdown = [{
	"text": "<i class=\"mdi mdi-key-change\"></i> Trocar senha",
	"click": "changePassword()"
    },{
	"text": "<i class=\"mdi mdi-logout\"></i> Sair",
	"click": "logout()"
    }];

    $scope.changePassword = function() {
	ChangePasswordModal({scope:$scope});
    }
});

mod.controller('SummaryCtrl', function($scope,LawyerCommitments,LawyerHistory,LawyerInactivityCheck,Loading){
    Loading.beginTasks(false);
    var loadCommitments = Loading.pushTask();
    var loadHistory = Loading.pushTask();
    var loadInactivityCheck = Loading.pushTask();
    Loading.watchTasks();
    $scope.commitments = LawyerCommitments.query(function(){
	loadCommitments.resolve();
    },function(){
	loadCommitments.reject();
    });
    $scope.history = LawyerHistory.query(function(){
	loadHistory.resolve();
    },function(){
	loadHistory.reject();
    });
    $scope.inactivityCheck = LawyerInactivityCheck.query(function(){
	loadInactivityCheck.resolve();
    },function(){
	loadInactivityCheck.reject();
    });
});

mod.controller('CustomerSummaryCtrl', function($scope,AggregatorHistory,CustomerSummary,Loading){

    Loading.beginTasks(false);
    var load = Loading.pushTask();
    Loading.watchTasks();


    if($scope.aggregator) {
	$scope.history = AggregatorHistory.query(function(){
	    load.resolve();
	},function(){
	    load.reject();
	});
    } else {
	$scope.summary = CustomerSummary.query(function(){
	    load.resolve();
	},function(){
	    load.reject();
	});
    }
});

mod.controller('SearchCtrl',function ($scope,avalancheConfig) {

    // variáveis rootScope da busca
    $scope.search = {
	limit: avalancheConfig.search.limit,
	offset: 0
    };   

    var isSearchClear = function() {
	return !$scope.search.filter || $scope.search.filter.trim()=='';
    };

    $scope.setSearch = function(loadSearch,clearSearch,p_isSearchClear) {
	if(!angular.isDefined(clearSearch)) {
	    clearSearch = function() {
		$scope.search.filter = undefined;		
	    };
	}

	if(angular.isDefined(p_isSearchClear)) {
	    isSearchClear = p_isSearchClear;
	}

	// limpa busca
	$scope.docList = undefined;
	if($scope.clearSearch)
	{
	    clearSearch();
	    $scope.search.offset = 0;
	}
	else
	    $scope.clearSearch = true;

	$scope.loadSearch = loadSearch;
    }

    $scope.clearSearch = true;

    var paginationListnerUnreg = $scope.$on('av.pagination.offset', function(event,offset){
	$scope.search.offset=offset;
	$scope.docList = undefined;
	$scope.loadSearch();
    });

    $scope.$on('$destroy', function() {
	paginationListnerUnreg();
    });

    $scope.doSearch = function() {
	if(isSearchClear())
	    return;
	$scope.clearSearch = false;
	$scope.search.offset = 0;
	$scope.loadSearch();
    }

});

mod.controller('ProcessCtrl', function($scope,$state,$q,$alert,$interval,Form,ProcessTree,Process,Loading,avalancheConfig){
    $scope.register = $scope.permission('register.process');
    $scope.params = {
	collapse: [{
	    title: 'Geral',
	    min: 0,
	    max: 119
	},{
	    title: 'Clientes',
	    required: true,
	    min: 120,
	    max: 120
	},{
	    title: 'Locais',
	    min: 140,
	    max: 140,
	    condition: 'node && node.attrs.processLocals && node.attrs.processLocals.length>0'
	},{
	    title: 'Histórico',
	    min: 150,
	    max: 150,
	    condition: 'node && node.attrs.docId'
	},{
	    title: 'Compromissos',
	    min: 160,
	    max: 160,
	    condition: 'node && node.attrs.docId'
	},{
	    title: 'Documentos',
	    min: 170,
	    max: 170,
	    condition: 'node && node.attrs.docId'
	},{
	    title: 'Contratos',
	    min: 180,
	    max: 180,
	    condition: 'node && node.attrs.docId'
	},{
	    title: 'Lançamentos',
	    min: 190,
	    max: 190,
	    condition: 'node && node.attrs.docId'
	}],
	collapseDef: {
	    'new': 0,
	    old: undefined
	},
	aggregator: $scope.aggregator,
	register: $scope.register
    }


    $scope.$on('avPaneClose',function(event,tab) {
	if(tab.node.dirty && !tab.saving) {
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		template: 'avDiscard.html',
		type: 'warning',
		show: true,
		container: 'body',
		dismissable: true,
		scope: $scope
	    });
	    var discartInterval = $interval(function() {
		error.hide();
	    },avalancheConfig.discardTimeout,1);
	    $scope.undoDiscard = function() {
		$interval.cancel(discartInterval);
		error.hide();
		$scope.$tabs.push(tab);
		if(tab.node.attrs.docId) {
		    $state.go('app.process.item',{
			rootProcessId: tab.rootProcessId,
			docId: tab.node.attrs.docId
		    });
		} else {
		    $state.go('app.process.item',{
			rootProcessId: tab.rootProcessId,
			docId: tab.node.attrs.parentId,
			'new': true
		    });
		}

	    }
	}
    });

    $scope.setTabTitle = function(tab) {
	var title;
	var node = tab.node;
	if(!node.attrs.docId && !node.attrs.parentId) {
	    tab.titleText = "Novo";
	}
	else {
	    if(node.attrs.folder != null) {
		title = node.attrs.folder;
		if(node.attrs.folderYear != null)
		    title += '/' + node.attrs.folderYear;
	    } else {
		title = node.attrs.docId
	    }
	    tab.titleText = title;
	}
	$scope.setTabTitleStatus(tab);
    }
    
    $scope.setTabTitleStatus = function(tab) {
	var title = tab.titleText;
	if(tab.node.dirty)
	    title = '<em>'+title+'</em>';
	if(!tab.node.attrs.docId && !tab.node.attrs.parentId) {
	    title = '<i class="mdi mdi-plus-circle"></i> '+ title;
	}
	else {
	    title = '<i class="mdi mdi-folder"></i> ' + title;
	}
	tab.title = title;
    }

    var checkDirty = function() {
	var dirtyTab = false;
	for(var i=0; !dirtyTab && i<$scope.$tabs.length; i++) {
	    if($scope.$tabs[i].node && $scope.$tabs[i].node.dirty)
		dirtyTab = $scope.$tabs[i];
	}
	return dirtyTab;
    }

    $scope.setCheckDirty(checkDirty);

    $scope.$on('$destroy',function(){
	$scope.setCheckDirty(null);
	
	if($scope.logginOut)
	    return;

	var dirtyTab = checkDirty();
	if(!dirtyTab)
	    return;

	var parent = $scope.$parent;
	parent.undoData['process'] = $scope.$tabs;
	var stateParams = {};
	var stateName = 'app.process';
	if(!dirtyTab.node.attrs.docId && !dirtyTab.node.attrs.rootProcessId) {
	    stateName += '.new';
	} else {
	    stateName += '.item';

	    stateParams.rootProcessId = dirtyTab.node.attrs.rootProcessId;

	    if(dirtyTab.node.attrs.docId)
		stateParams.docId = dirtyTab.node.attrs.docId;
	    else {
		stateParams.docId = dirtyTab.node.attrs.parentId;
		stateParams.new = true;
	    }
	    
	}

	var error = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    template: 'avDiscard.html',
	    type: 'warning',
	    show: true,
	    container: 'body',
	    dismissable: true,
	    scope: parent
	});
	var discartInterval = $interval(function() {
	    error.hide();
	    stateParams = null;
	    parent.undoData['process'] = null;
	    parent.undoDiscard = null;
	},avalancheConfig.discardTimeout,1);
	parent.undoDiscard = function() {
	    $interval.cancel(discartInterval);
	    error.hide();
	    $state.go(stateName,stateParams);
	    parent.undoDiscard = null;
	}
    });

    if($scope.undoData['process']) {
	$scope.$tabs = $scope.undoData['process'];
	$scope.undoData['process'] = null;
    } else {
	$scope.$tabs = [{ 
	    title:'<i class="mdi mdi-magnify"></i> Busca', 
	    name: "search", 
	    template: 'legisSearchProcess.html'
	}];

	if($scope.permission('app.reports')) {
	    $scope.$tabs.push({
		title:'<i class="mdi mdi-chart-bar"></i> Relatório',
		name: "report",
		template: 'legisProcessReport.html'
	    });
	}

	if($scope.register) {
	    // push new tab
	    Loading.beginTasks();
	    var loadProcess = Loading.pushTask();
	    Loading.watchTasks();

	    Form.get({
		path: avalancheConfig.path.process,
		nodeService: Process,
		excludeFields: avalancheConfig.excludeFields.process
	    },function(origNewNode) {
		var tab = {
		    name: "new", 
		    template: 'legisProcessItem.html',
		    node: angular.copy(origNewNode),
		    origNewNode: origNewNode
		};
		$scope.setTabTitle(tab);
		$scope.$tabs.push(tab);
		loadProcess.resolve();
	    }, function(){
		loadProcess.reject();
	    });
	}
    }

    $scope.openProcess = function(params) {
	var found = false;
	var tabName = 'process_'+params.rootProcessId;
	for(var i=0; i<$scope.$tabs.length && !found; i++) {
	    if($scope.$tabs[i].name==tabName)
		found = i;
	}
	
	if(!found)  {
	    // aba/pasta não encontrada - abre nova aba
	    Loading.beginTasks();

	    var loadTree = Loading.pushTask();
	    var loadProcess = Loading.pushTask();
	    Loading.watchTasks();
	    
	    if($scope.register) {
		var treeData = ProcessTree.get({
		    docId: params.docId
		}, function(){
		    loadTree.resolve();
		},function(){
		    loadTree.reject();
		});
	    } else {
		loadTree.resolve();
	    }
	    
	    var node;
	    if(params.node == undefined) {
		node = Form.get({
		    docId: params['new']?null:params.docId,
		    extraParams: { parentId: !params['new']?null:params.docId },
		    path: avalancheConfig.path.process,
		    nodeService: Process,
		    excludeFields: avalancheConfig.excludeFields.process
		},function() {
		    loadProcess.resolve();
		}, function(){
		    loadProcess.reject();
		});
	    } else {
		node = params.node;
		loadProcess.resolve();
	    }
	    
	    $q.all([loadTree.promise,loadProcess.promise]).then(function(result) {
		var tab = {
		    name: tabName,
		    template: 'legisProcessItem.html',
		    removeBtn: true,
		    rootProcessId: params.rootProcessId,
		    treeData: [treeData],
		    node: node
		}
		
		$scope.setTabTitle(tab);
		$scope.$tabs.push(tab);
		$scope.$tabs.activeTab = tabName;
	    });
	    
	} else {
	    // aba do processo encontrada - já estava aberta
	    if(!params['new'] && params.docId != $scope.$tabs[found].node.attrs.docId || 
	       params['new'] && params.docId != $scope.$tabs[found].node.attrs.parentId) {
		// subprocesso diferente do atual selecionado
		var oldNode = $scope.$tabs[found].node;
		$scope.$tabs[found].node = undefined;
		
		// opção de undo caso documento antigo esteja alterado e não salvo
		if(oldNode.dirty) {
		    var error = $alert({
			animation: 'am-slide-top',
			placement: 'top-right',
			template: 'avDiscard.html',
			type: 'warning',
			show: true,
			container: 'body',
			dismissable: true,
			scope: $scope
		    });
		    var discartInterval = $interval(function() {
			error.hide();
		    },avalancheConfig.discardTimeout,1);
		    $scope.undoDiscard = function() {
			$scope.$tabs[found].node = oldNode;
			$interval.cancel(discartInterval);
			error.hide();
		    }
		}
		
		Loading.beginTasks();
		$scope.$tabs.activeTab = tabName;
		var loadProcess = Loading.pushTask();
		Loading.watchTasks();
		var node;
		if(params.node == undefined) {
		    node = Form.get({
			docId: params['new']?null:params.docId,
			extraParams: { parentId: !params['new']?null:params.docId },
			path: avalancheConfig.path.process,
			nodeService: Process,
			excludeFields: avalancheConfig.excludeFields.process
		    },function() {
			if($scope.$tabs[found].node==undefined)
			    $scope.$tabs[found].node = node;
			loadProcess.resolve();
		    },function(){
			loadProcess.reject();
		    });
		} else {
		    $scope.$tabs[found].node = params.node;
		    loadProcess.resolve();
		}
	    } else
		$scope.$tabs.activeTab = tabName;
	}
    }

    $scope.closeProcess = function(tab) {
	$scope.$tabs.splice($scope.$tabs.indexOf(tab),1);
	$scope.scrollToTop();
    }

    $scope.cancelNewProcess = function(tab) {
	if(!tab.node.dirty || tab.saving)
	    return;

	var node = tab.node;

	if(node.attrs.parentId) {
	    tab.tree.select_parent_branch();
	    return;
	}

	var error = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    template: 'avDiscard.html',
	    type: 'warning',
	    show: true,
	    container: 'body',
	    dismissable: true,
	    scope: $scope
	});


	var discartInterval = $interval(function() {
	    error.hide();
	},avalancheConfig.discardTimeout,1);
	$scope.undoDiscard = function() {
	    $interval.cancel(discartInterval);
	    error.hide();
	    tab.node = node
	}

	tab.node = angular.copy(tab.origNewNode);
	$scope.scrollToTop();
    }

    $scope.delProcess = function(tab) {
	var node = tab.node;

	tab.saving = true;

	var deletingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    template: 'avAlertDelete.html',
	    type: 'warning',
	    show: true,
	    container: 'body',
	    dismissable: true,
	    scope: $scope
	});

	var currBranch;
	if(!node.attrs.parentId) {
	    // se é processo raiz, fecha aba
	    $scope.closeProcess();
	} else {
	    // se é subprocesso, remove branch dos dados e limpa documento
	    currBranch = {
		siblings: tab.tree.get_siblings(),
		branch: tab.tree.get_selected_branch(),
		parent: tab.tree.select_parent_branch()
	    }
	    currBranch.siblings.splice(currBranch.siblings.indexOf(currBranch.branch),1);
	}

	var restoreNode = function() {
	    restoreNode = undefined;
	    if($scope.$tabs.indexOf(tab)==-1) {
		$scope.$tabs.push(tab);
	    }
	    $scope.$tabs.activeTab = tab.name;

	    if(node.attrs.parentId) {	
		tab.tree.add_branch(currBranch.parent,currBranch.branch);
		currBranch.siblings.push(currBranch.branch);
		tab.node = node
	    } 
	}

	var deleteInterval = $interval(function() {
	    deletingAlert.hide();
	    Process.delete({
		docId: node.attrs.docId,
		path: avalancheConfig.path.process
	    },function(){
		// documento removido - não precisa fazer nada
		tab.saving = false;
	    }, function(resp) {
		tab.saving = false;
		if(restoreNode)
		    restoreNode();
		var error = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    content: resp.data.statusMessage,
		    template: 'avAlert.html',
		    type: 'danger',
		    show: true,
		    container: 'body',
		    duration: 5,
		    dismissable: true
		});
	    });
	},avalancheConfig.deleteTimeout,1);

	$scope.undoDelete = function($event) {
	    $event.preventDefault();
	    $interval.cancel(deleteInterval);
	    tab.saving = false;
	    deletingAlert.hide();
	    if(restoreNode)
		restoreNode();
	}
    }


    $scope.$watch('$tabs.activeTab',function(newValue,oldValue){
	if(oldValue != newValue && newValue != undefined && oldValue != undefined)
	{
	    switch(newValue)
	    {
	    case 'report':
	    case 'search':
	    case 'new':
		$state.go('app.process.'+newValue);
		break;
	    default:
		for(var i=0; i<$scope.$tabs.length; i++) {
		    var tab = $scope.$tabs[i];
		    if(tab.name==newValue) {
			if(tab.node) {
			    if(tab.node.attrs.docId) {
				$state.go('app.process.item',{ 
				    rootProcessId: tab.rootProcessId,
				    docId: tab.node.attrs.docId
				});
			    } else {
				$state.go('app.process.item',{
				    rootProcessId: tab.rootProcessId,
				    docId: tab.node.attrs.parentId,
				    'new': true
				});
			    }
			}
			break;
		    }
		}
		break;
	    }
	}
    });

    switch($state.current.name)
    {
    case 'app.process.new':
	$scope.$tabs.activeTab = 'new';
	break;
    case 'app.process.search':
	$scope.$tabs.activeTab = 'search';
	break;
    case 'app.process.report':
	$scope.$tabs.activeTab = 'report';
	break;
    case 'app.process':
	$state.go('app.process.search');
	$scope.$tabs.activeTab = 'search';
	break;
    }

});

mod.controller('ReportCtrl', function($scope,$controller,$filter,SelectField,avalancheConfig){
    $controller('SearchCtrl',{$scope:$scope});

    $scope.isSendDisabled = function() {
	return $scope.search.paramList.length==0;
    }

    $scope.isAddParamDisabled = function() {
	switch($scope.values.params) {
	case "folder":
	    return !$scope.values.extra || !$scope.values.extra.folder || 
		$scope.values.extra.folder.trim()=='';
	case "justice":
	    return !$scope.values.extra || !$scope.values.extra.justiceId;
	case "commitment":
	    return !$scope.values.extra || !$scope.values.extra.commitment ||
		!$scope.values.extra.commitment.type ||
		$scope.values.extra.commitment.type=='before' && !$scope.values.extra.commitment.before ||
		$scope.values.extra.commitment.type=='after' && !$scope.values.extra.commitment.after ||
		$scope.values.extra.commitment.type=='between' && (!$scope.values.extra.commitment.before || 
								   !$scope.values.extra.commitment.after);
	case "actionType":
	    return !$scope.values.extra || !$scope.values.extra.actionTypeId;
	case "local":
	    return !$scope.values.extra || !$scope.values.extra.localId;
	case "lawyer":
	    return !$scope.values.extra || !$scope.values.extra.lawyer;
	case "phases":
	    return !$scope.values.extra || !$scope.values.extra.phasesId;
	case "subject":
	    return !$scope.values.extra || !$scope.values.extra.subjectId;
	case "situation":
	    return !$scope.values.extra || !$scope.values.extra.situationId;
	case "result":
	    return !$scope.values.extra || !$scope.values.extra.resultId;
	case "otherPart":
	    return !$scope.values.extra || !$scope.values.extra.otherPart || 
		$scope.values.extra.otherPart.trim()=='';
	case "customer":
	    return !$scope.values.extra || !$scope.values.extra.customer ||
		!$scope.values.extra.customer.docId;
	case "start":
	    return !$scope.values.extra || !$scope.values.extra.start ||
		!$scope.values.extra.start.type ||
		$scope.values.extra.start.type=='before' && !$scope.values.extra.start.before ||
		$scope.values.extra.start.type=='after' && !$scope.values.extra.start.after ||
		$scope.values.extra.start.type=='between' && (!$scope.values.extra.start.before || 
							      !$scope.values.extra.start.after);
	case "city":
	case "clientCity":
	    return !$scope.values.extra || !$scope.values.extra.cidade_codigo;
	case "state":
	    return !$scope.values.extra || !$scope.values.extra.uf_codigo;
	case "value":
	    return !$scope.values.extra || !$scope.values.extra.value ||
		!$scope.values.extra.value.type ||
		$scope.values.extra.value.type=='lower' && !$scope.values.extra.value.lower ||
		$scope.values.extra.value.type=='greater' && !$scope.values.extra.value.greater ||
		$scope.values.extra.value.type=='between' && (!$scope.values.extra.value.lower || 
							      !$scope.values.extra.value.greater);
	case "age":
	    return !$scope.values.extra || !$scope.values.extra.age ||
		!$scope.values.extra.age.type ||
		$scope.values.extra.age.type=='lower' && !$scope.values.extra.age.lower ||
		$scope.values.extra.age.type=='greater' && !$scope.values.extra.age.greater ||
		$scope.values.extra.age.type=='between' && (!$scope.values.extra.age.lower || 
							      !$scope.values.extra.age.greater);
	case "func":
	    return !$scope.values.extra || !$scope.values.extra.func || 
		$scope.values.extra.func.trim()=='';
	case "profession":
	    return !$scope.values.extra || !$scope.values.extra.profession || 
		$scope.values.extra.profession.trim()=='';
	default:
	    return true;
	}
    }

    $scope.removeParam = function(param) {
	$scope.search.paramList.splice($scope.search.paramList.indexOf(param),1);
    }

    $scope.addParam = function() {
	var not = $scope.values.not=='1'?true:false;
	var label;
	switch($scope.values.params) {
	case "folder":
	    var folder = $scope.values.extra.folder;
	    if($scope.values.extra.folderYear && 
	       $scope.values.extra.folderYear.trim() != '')
		folder += '/'+$scope.values.extra.folderYear;

	    label = "Pasta "+(not?"diferente de":"igual a")+" '"+folder+"'";
	    break;

	case "justice":
	    label = "Justiça "+(not?"diferente de":"igual a")+" '"+
		SelectField.getTextFromField($scope.fields.extra.justiceId,
					     $scope.values.extra.justiceId)+"'";
	    break;
	case "commitment":
	    switch($scope.values.extra.commitment.type) {
	    case 'before':
		label = "Compromisso antes de '"+
		    $filter('date')($filter('mysqlDatetimeToISO')($scope.values.extra.commitment.before),
				    avalancheConfig.dateFormat) + "'";
		break;
	    case 'after':
		label = "Compromisso depois de '"+
		    $filter('date')($filter('mysqlDatetimeToISO')($scope.values.extra.commitment.after),
				    avalancheConfig.dateFormat) + "'";
		break;
	    case 'between':
		label = "Compromisso entre '"+
		    $filter('date')($filter('mysqlDatetimeToISO')($scope.values.extra.commitment.after),
				    avalancheConfig.dateFormat) + "' e '"+
		    $filter('date')($filter('mysqlDatetimeToISO')($scope.values.extra.commitment.before),
				    avalancheConfig.dateFormat) + "'";
		break;
	    }
	    break;

	case "actionType":
	    label = "Tipo da Ação "+(not?"diferente de":"igual a")+" '"+
		SelectField.getTextFromField($scope.fields.extra.actionTypeId,
					     $scope.values.extra.actionTypeId)+"'";
	    break;

	case "local":
	    label = (not?"Não pertence ":"Pertence ")+
		($scope.values.extra.room && 
		 $scope.values.extra.room.trim()!=""?"a Vara/Turma "+ $scope.values.extra.room+' do':'ao')+
		" Local '"+ SelectField.getTextFromField($scope.fields.extra.localId,
							 $scope.values.extra.localId)+"'" +
		" da Justiça '" + SelectField.getTextFromField($scope.fields.extra.justiceId,
							       $scope.values.extra.justiceId)+"'";
	    break;

	case "lawyer":
	    label = "'"+ SelectField.getTextFromField($scope.fields.extra.lawyer,
						      $scope.values.extra.lawyer)+
		"' "+ (not?"não é ":"é")+" Advogado";
	    break;

	case "phases":
	    label = "Fase "+(not?"diferente de":"igual a")+" '"+
		SelectField.getTextFromField($scope.fields.extra.phasesId,
					     $scope.values.extra.phasesId)+"'";
	    break;

	case "subject":
	    label = "O assunto "+(not?"não é":"é")+" '"+
		SelectField.getTextFromField($scope.fields.extra.subjectId,
					     $scope.values.extra.subjectId)+"'";
	    break;

	case "situation":
	    label = "Situação "+(not?"diferente de":"igual a")+" '"+
		SelectField.getTextFromField($scope.fields.extra.situationId,
					     $scope.values.extra.situationId)+"'";
	    break;

	case "result":
	    label = "Resultado "+(not?"diferente de":"igual a")+" '"+
		SelectField.getTextFromField($scope.fields.extra.resultId,
					     $scope.values.extra.resultId)+"'";
	    break;

	case "otherPart":
	    label = "Outra parte "+(not?"diferente de":"igual a")+" '"+
		$scope.values.extra.otherPart+"'";
	    break;

	case "customer":
	    label = "'"+$scope.values.extra.customer.name+"' "+(not?"não é ":"é")+" Cliente";
	    break;

	case "start":
	    switch($scope.values.extra.start.type) {
	    case 'before':
		label = "Início antes de '" +
		    $filter('date')($filter('mysqlDatetimeToISO')($scope.values.extra.start.before),
				    avalancheConfig.dateFormat) + "'";
		break;
	    case 'after':
		label = "Início depois de '"+
		    $filter('date')($filter('mysqlDatetimeToISO')($scope.values.extra.start.after),
				    avalancheConfig.dateFormat) + "'";
		break;
	    case 'between':
		label = "Início entre '"+
		    $filter('date')($filter('mysqlDatetimeToISO')($scope.values.extra.start.after),
				    avalancheConfig.dateFormat) + "' e '"+
		    $filter('date')($filter('mysqlDatetimeToISO')($scope.values.extra.start.before),
				    avalancheConfig.dateFormat) + "'";
		break;
	    }
	    break;

	case "city":
	case "clientCity":
	    label = "Cidade ";
	    if($scope.values.params=="clientCity")
		label += "do cliente ";
	    label += (not?"não é":"é")+" '"+
		SelectField.getTextFromField($scope.fields.extra.cidade_codigo,
					     $scope.values.extra.cidade_codigo);
	    label += "' do estado '"+
		SelectField.getTextFromField($scope.fields.extra.uf_codigo,
					     $scope.values.extra.uf_codigo) + "'";
	    break;

	case "state":
	    label = "Estado "+(not?"diferente de":"igual a")+" '"+
		SelectField.getTextFromField($scope.fields.extra.uf_codigo,
					     $scope.values.extra.uf_codigo) + "'";
	    break;
    
	case "value":
	    switch($scope.values.extra.value.type) {
	    case 'lower':
		label = "Valor menor que '" +
		    $filter('number')($scope.values.extra.value.lower,2) + "'";
		break;
	    case 'greater':
		label = "Valor maior que '"+
		    $filter('number')($scope.values.extra.value.greater,2) + "'";
		break;
	    case 'between':
		label = "Valor entre '"+
		    $filter('number')($scope.values.extra.value.greater,2) + "' e '" +
		    $filter('number')($scope.values.extra.value.lower,2) + "'";
		break;
	    }
	    break;

	case "age":
	    switch($scope.values.extra.age.type) {
	    case 'lower':
		label = "Idade menor que '" + $scope.values.extra.age.lower + "'";
		break;
	    case 'greater':
		label = "Idade maior que '"+ $scope.values.extra.age.greater + "'";
		break;
	    case 'between':
		label = "Idade entre '"+ $scope.values.extra.age.greater + "' e '" +
		    $scope.values.extra.age.lower + "'";
		break;
	    }
	    break;
	case "func":
	    label = "Função "+(not?"diferente de":"igual a")+" '"+
		$scope.values.extra.func+"'";
	    break;

	case "profession":
	    label = "Profissão "+(not?"diferente de":"igual a")+" '"+
		$scope.values.extra.profession+"'";
	    break;
	}

	var newP = {
	    param: $scope.values.params,
	    label: label, 
	    not: $scope.values.not
	}
	angular.extend(newP,$scope.values.extra);
	$scope.search.paramList.push(newP);

    }
});

mod.controller('ProcessReportCtrl', function($scope,$controller,ProcessReport,Loading,FileViewer){
    $controller('ReportCtrl',{$scope:$scope});

    var initForm = function() {
	$scope.search.type = 'screen';
	$scope.search.sub = '1';
	$scope.search.grouping = undefined;
	$scope.values = {};
	$scope.search.paramList = [];
	$scope.docList = undefined;
	$scope.chartData = undefined;
    }
    initForm();
    $scope.cancel = function() {
	initForm();
	$scope.processReportForm.$setPristine();
    }

    $scope.setSearch(function() {
	$scope.docList = undefined;
	$scope.chartData = undefined;

	// loadSearch
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	var data = ProcessReport.search($scope.search,function() {
	    switch($scope.search.type) {
	    case 'screen':
		$scope.docList = data;
		break;
	    case 'groupingReport':
		$scope.chartData = data.children;
		break;
	    case 'individualReport':
		var fileViewer = FileViewer({
		    doc: {
			docId: data.attrs.docId,
			contentType: 'application/pdf'
		    },
		    container: '#file-viewer'
		});		
	    }
	    load.resolve();
	}, function() {
	    load.reject();
	});
    },function(){
	// clearSearch
	initForm();
    },function(){
	// isSearchClear
	return $scope.search.paramList.length==0;
    });

    $scope.fields = {
	type: {
	    attrs: {
		fieldName: 'type',
		label: 'Tipo',
		required: 'required'
	    },
	    children: [{ attrs: {id: "screen", value: 'Consulta na tela'} },
		       { attrs: {id: "groupingReport", value: 'Relatório agrupamento'} },
		       { attrs: {id: "individualReport", value: 'Relatório individual'} }]
	},
	sub: {
	    attrs: {
		fieldName: 'sub',
		label: 'Incluir sub-processos',
		required: 'required'
	    },
	    children: []
	},
	grouping: {
	    attrs: {
		fieldName: 'grouping',
		label: 'Parâmetro de agrupamento',
		required: 'required'
	    },
	    children: [{ attrs: {id: "justice", value: 'Justiça'} },
		       { attrs: {id: "actionType", value: 'Tipos de Ação'} },
		       { attrs: {id: "local", value: 'Local'} },
		       { attrs: {id: "phases", value: 'Fase Atual'} },
		       { attrs: {id: "subject", value: 'Assunto'} },
		       { attrs: {id: "situation", value: 'Situação'} },
		       { attrs: {id: "result", value: 'Resultado'} } ]
	},
	params: {
	    attrs: {
		fieldName: 'params',
		label: 'Parâmetros'
	    },
	    children: [{ attrs: {id: "folder", value: 'Pasta'} },
		       { attrs: {id: "justice", value: 'Justiça'} },
		       { attrs: {id: "commitment", value: 'Compromisso'} },
		       { attrs: {id: "actionType", value: 'Tipos de Ação'} },
		       { attrs: {id: "local", value: 'Local'} },
		       { attrs: {id: "lawyer", value: 'Advogado'} },
		       { attrs: {id: "phases", value: 'Fase Atual'} },
		       { attrs: {id: "subject", value: 'Assunto'} },
		       { attrs: {id: "situation", value: 'Situação'} },
		       { attrs: {id: "result", value: 'Resultado'} },
		       { attrs: {id: "otherPart", value: 'Outra parte'} },
		       { attrs: {id: "customer", value: 'Cliente'} },
		       { attrs: {id: "start", value: 'Início'} },
		       { attrs: {id: "city", value: 'Cidade'} },
		       { attrs: {id: "state", value: 'Estado'} },
		       { attrs: {id: "value", value: 'Valor do Processo'} },
		       { attrs: {id: "clientCity", value: 'Cidade do Cliente'} }]
	},
	not: {
	    attrs: {
		fieldName: 'not',
		label: 'Sentido Inverso/Negado'
	    },
	    children: []
	}
    };
});

mod.controller('ProcessItemCtrl', function($scope,$stateParams){
    $scope.openProcess($stateParams);
});

mod.controller('ProcessDocCtrl', function($scope,$timeout,$alert,$state,Process,Form,avalancheConfig) {

    $scope.saveAndClose = function() {
	if($scope.tab.name=='new') {
	    $scope.tab.openAfterSave = false;
	    $scope.avForm.submit();
	}
	else {
	    $scope.avForm.submit();
	    $scope.closeProcess($scope.tab);
	}
    }

    $scope.save = function() {
	var tab = $scope.tab;
	var node = tab.node;

	// clear new tab before finish saving
	if(tab.name=='new') {
	    tab.node = angular.copy(tab.origNewNode);
	    $scope.scrollToTop();
	}
	else {
	    tab.saving = true;
	    $scope.avForm.$setPristine();
	}


	var savingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: $scope.translations['savingTag']+'...',
	    template: 'avAlert.html',
	    type: 'info',
	    show: true,
	    container: 'body',
	    dismissable: true
	});

	
	Process.save({
	    docId: node.attrs.docId,
	    path: avalancheConfig.path.process,
	    parentId: node.attrs.parentId
	},node.attrs, function(node) {
	    if(tab.name=='new') {
		if(tab.openAfterSave) {
		    $scope.openProcess({
			docId: node.attrs.docId, 
			rootProcessId: node.attrs.rootProcessId,
			node: node
		    });
		} else {
		    tab.openAfterSave = true;
		}
	    } else {
		tab.saving = false;
		if(!tab.node.attrs.docId)
		    node.newSub = true;

		// utiliza serviço do Form para tratar campos do documento
		Form.get({
		    node: node,
		    path: avalancheConfig.path.process,
		    nodeService: Process,
		    excludeFields: avalancheConfig.excludeFields.process
		},function(node) {
		    // confirma se documento ainda está aberto na aba antes 
		    // de atualizar
		    if(tab.node && (!tab.node.attrs.docId || 
				   tab.node.attrs.docId==node.attrs.docId)) {
			tab.node = node;
			
			if($scope.$tabs.indexOf(tab)!=-1 && node.newSub) {
			    // se for um novo sub-processo atualiza state
			    $state.go('app.process.item',{
				rootProcessId: tab.rootProcessId,
				docId: tab.node.attrs.docId
			    },{	inherit: false });
			}
		    }
		});

		if(!tab.node.attrs.parentId)
		    $scope.setTabTitle(tab);
	    }
	    savingAlert.hide();
	},function(resp) {
	    savingAlert.hide();
	    if(tab.name=='new') {
		tab.openAfterSave = true;
		tab.node = node;
	    } else {
		if($scope.$tabs.indexOf(tab)==-1) {
		    $scope.$tabs.push(tab);
		} else {
		    $scope.avForm.$setDirty();
		}
		$scope.$tabs.activeTab = tab.name;
		tab.saving = false;
	    }
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }
    
    $scope.delete = function() {
	$scope.delProcess($scope.tab);
    }


    $timeout(function(){
	$scope.tab.startCollapsed = '0';
	if($scope.tab.name=='new')
	    $scope.tab.openAfterSave = true;
	else
	    $scope.tab.saving = false;

	if($scope.tab.node.dirty) {
	    $scope.avForm.$setDirty();
	    $scope.setTabTitleStatus($scope.tab);
	} 

	// when the document is changed, the form should be set to pristine
	var unregDirtyWatch = null;

	var regDirtyWatch = function() {
	    unregDirtyWatch = $scope.$watch('avForm.$dirty',function(newValue,oldValue) {
		$scope.tab.node.dirty = newValue;
		$scope.setTabTitleStatus($scope.tab);
	    });
	}

	regDirtyWatch();

	$scope.$watch('tab.node', function(newValue,oldValue) {
	    if(newValue != oldValue && unregDirtyWatch) {
		unregDirtyWatch();
		unregDirtyWatch = null;
	    }

	    if(newValue && !unregDirtyWatch) {
		if(newValue.dirty) {
		    $scope.avForm.$setDirty();
		} else {
		    $timeout(function(){
			$scope.avForm.$setPristine();
			$scope.avForm.$setUntouched();
			$scope.$apply();
		    },0,false);
		}

		regDirtyWatch();

	    }
	});
    },0,false);

});

mod.controller('FormCollapseCtrl',function($scope,$timeout) {

    $scope.node.setActiveTab = function(active) {
	$scope.active = active;
    } 

    var setDefActive = function() {
	if($scope.node.attrs.docId)
	    $scope.active = $scope.params.collapseDef.old;
	else
	    $scope.active = $scope.params.collapseDef.new;
    };

    $scope.$watch('avForm.$pristine', function(newValue,oldValue) {
	if(newValue && $scope.node && $scope.node.attrs) {
	    setDefActive();
	}
    });

    $scope.$watch('node',function(newValue,oldValue) {
	$scope.collapseCtrl = [];
	if(!newValue)
	    return;

	for(var i=0; i<$scope.params.collapse.length; i++) {
	    var colItem = {
		collapse: $scope.params.collapse[i],
		children: []
	    }
	    for(var j=0; j<$scope.node.children.length; j++) {
		var field = $scope.node.children[j];
		if(field.attrs.orderby>=colItem.collapse.min && 
		   field.attrs.orderby<=colItem.collapse.max) {
		    colItem.children.push(field);
		}
	    }
	    if(colItem.children.length>0)
		$scope.collapseCtrl.push(colItem);
	}
	$timeout(function() {
	    setDefActive();
	},0,false);
    });

    // redefine avForm.submit para abrir collapse correto em caso de erro antes de chamar
    // a função original
    $timeout(function(){
	// Existe um caso onde o avForm pode ser destruido antes de chegar aqui,
	// quando estiver saindo deste estado antes dele terminar de carregar.
	// Este ajuste foi feito para evitar erro de js neste caso
	if(!$scope.avForm)
	    return;

	var submit = $scope.avForm.submit;

	$scope.avForm.checkCollapse = function() {
	    var found = false;
	    var collapseIdx = 0;
	    for(var j=0; !found && j < $scope.collapseCtrl.length; j++) {
		if($scope.collapseCtrl[j].collapse.condition &&
		   !$scope.$eval($scope.collapseCtrl[j].collapse.condition))
		    continue;
		
		for(var i=0; i<$scope.collapseCtrl[j].children.length; i++) {
		    var field = $scope.avForm[$scope.collapseCtrl[j].children[i].attrs.fieldName];
		    if(field && field.$invalid) {
			$scope.active = collapseIdx;
			found = true;
		    }
		}
		collapseIdx++;
	    }
	    if(!found)
		$scope.active = 0;
	}


	$scope.avForm.submit = function() {

	    // se formulário foi submetido e não está válido,
	    // encontra collapse que deve estar ativo
	    if(!$scope.avForm.$valid) {
		$scope.avForm.checkCollapse();
	    }
	    submit();
	}
    },0,false);
});

mod.controller('ProcessTreeCtrl',function($scope,$timeout,$state) {
    $scope.treeData = $scope.tab.treeData;
    $scope.tree = {};
    $scope.ready = false;

    var newBranchParent = false;
    $scope.tab.tree = $scope.tree;

    var Select = function(siblings, node) {
	for(var i = 0; i < siblings.length; i++) {
	    if(siblings[i].data.processId==node.attrs.docId) {
		// encontrou processo
		siblings[i].label = node.attrs.processNumbers.join(', ');
		$scope.tree.select_branch(siblings[i]);
		return true;
	    } else if(!node.attrs.docId && siblings[i].data.processId==node.attrs.parentId) {
		// novo subprocesso
		var newBranch;
		if(!siblings[i].children || siblings[i].children.length==0 
		   || siblings[i].children[siblings[i].children.length-1].data.processId != 'new') {
		    newBranch = {
			label: "Novo",
			data: { processId: 'new' }
		    };
		    $scope.tree.add_branch(siblings[i],newBranch);
		} else {
		    newBranch = siblings[i].children[siblings[i].children.length-1];
		}
		$scope.tree.select_branch(newBranch);
		newBranchParent = siblings[i];
		return true;
	    } else {
		if(siblings[i].children &&
		   Select(siblings[i].children, node))
		    return true;
	    }   
	}
	return false;
    }

    $timeout(function(){
	Select($scope.treeData,$scope.tab.node);
	$timeout(function(){
	    $scope.ready = true;
	},0,false);
    },0,false);

    $scope.$watch('tab.node',function(newValue,oldValue){
	// atualiza arvore se existe refencia para um novo branch
	if(newBranchParent) {
	    if(newValue && newValue.newSub) {
		// atualiza branch
		var branch = newBranchParent.children[newBranchParent.children.length-1];
		branch.label = newValue.attrs.processNumbers.join(', ');
		branch.data = {
		    processId: newValue.attrs.docId,
		    parentId: newValue.attrs.parentId
		};
		newBranchParent = false;
	    } else {
		// se não estiver salvando um novo branch, remove
		newBranchParent.children.splice(-1,1);
		newBranchParent = false;
	    }
	}

	if(newValue != undefined && newValue!=oldValue) {
	    Select($scope.treeData,$scope.tab.node);
	}
    });
	    
    	    
    $scope.onSelect = function(branch) {
	if($scope.ready) {
	    if(branch.data.processId != 'new') {
		$state.go('app.process.item',{
		    rootProcessId: $scope.tab.rootProcessId,
		    docId: branch.data.processId
		},{ inherit: false });
	    } else {
		$state.go('app.process.item',{
		    rootProcessId: $scope.tab.rootProcessId,
		    docId: $scope.tab.node.attrs.parentId,
		    'new': true
		});
	    }
	}
    }

    $scope.addSubProcess = function() {
	var selected = $scope.tree.get_selected_branch();
	if(selected.data.processId != 'new') {
	    $state.go('app.process.item',{
		rootProcessId: $scope.tab.rootProcessId,
		docId: selected.data.processId,
		'new': true
	    });
	}
    }
});

mod.controller('ProcessSearchCtrl', function($scope,$controller,SearchProcess,Loading){
    $controller('SearchCtrl', {$scope: $scope});

    $scope.setSearch(function() {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	var data = SearchProcess.search($scope.search,function() {
	    $scope.docList = data;
	    load.resolve();
	}, function() {
	    load.reject();
	});
    });

});

mod.controller('EntityCtrl', function($scope,$state,$q,$alert,$interval,Form,Customer,Loading,avalancheConfig){

    // default params
    if(!$scope.params.entityItemTemplate)
	$scope.params.entityItemTemplate = 'legisEntityItem.html';

    $scope.$on('avPaneClose',function(event,tab) {
	if(tab.node.dirty && !tab.saving) {
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		template: 'avDiscard.html',
		type: 'warning',
		show: true,
		container: 'body',
		dismissable: true,
		scope: $scope
	    });
	    var discartInterval = $interval(function() {
		error.hide();
	    },avalancheConfig.discardTimeout,1);
	    $scope.undoDiscard = function() {
		$interval.cancel(discartInterval);
		error.hide();
		$scope.$tabs.push(tab);
		$state.go('app.'+$scope.params.tag+'.item',{
		    docId: tab.node.attrs.docId
		});
	    }
	}
    });

    $scope.setTabTitle = function(tab) {
	if(angular.isDefined($scope.params.setTabTitle)) {
	    $scope.params.setTabTitle(tab);
	} else {
	    var node = tab.node;
	    if(node.attrs.name) {
		var title = node.attrs.name.split(" ");
		tab.titleText = title[0];
		tab.titleTip = node.attrs.name;
	    } else {
		tab.titleText = node.attrs.docId;
	    }
	}
	$scope.setTabTitleStatus(tab);
    }
    
    $scope.setTabTitleStatus = function(tab) {
	var title = tab.titleText;

	if(tab.node.dirty) {
	    title = '<em>'+title+'</em>';
	}
	if(!tab.node.attrs.docId) {
	    title = '<i class="mdi mdi-plus-circle"></i> '+ title;
	}
	else {
	    title = '<i class="mdi '+$scope.params.icon+'"></i> ' + title;
	}

	if(tab.titleTip) {
	    title = '<span title="'+tab.titleTip+'">'+title+'</span>';
	}
	tab.title = title;
    }

    var checkDirty = function() {
	var dirtyTab = false;
	for(var i=0; !dirtyTab && i<$scope.$tabs.length; i++) {
	    if($scope.$tabs[i].node && $scope.$tabs[i].node.dirty)
		dirtyTab = $scope.$tabs[i];
	}
	return dirtyTab;
    }

    $scope.setCheckDirty(checkDirty);

    $scope.$on('$destroy',function(){
	$scope.setCheckDirty(null);

	if($scope.logginOut)
	    return;

	var dirtyTab = checkDirty();
	if(!dirtyTab)
	    return;

	var parent = $scope.$parent;
	var tag = $scope.params.tag;
	parent.undoData[tag] = $scope.$tabs;
	var stateParams = {};
	var stateName = 'app.'+tag;
	if(!dirtyTab.node.attrs.docId) {
	    stateName += '.'+dirtyTab.name.toLowerCase();
	} else {
	    stateName += '.item';
	    stateParams.docId = dirtyTab.node.attrs.docId;
	}

	var error = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    template: 'avDiscard.html',
	    type: 'warning',
	    show: true,
	    container: 'body',
	    dismissable: true,
	    scope: parent
	});
	var discartInterval = $interval(function() {
	    error.hide();
	    stateParams = null;
	    parent.undoData[tag] = null;
	    parent.undoDiscard = null;
	},avalancheConfig.discardTimeout,1);
	parent.undoDiscard = function() {
	    $interval.cancel(discartInterval);
	    error.hide();
	    $state.go(stateName,stateParams);
	    parent.undoDiscard = null;
	}
    });

    if($scope.undoData[$scope.params.tag]) {
	$scope.$tabs = $scope.undoData[$scope.params.tag];
	$scope.undoData[$scope.params.tag] = null;
    } else {
	$scope.$tabs = [{ 
	    title:'<i class="mdi mdi-magnify"></i> Busca', 
	    name: "search", 
	    template: $scope.params.searchTemplate || 'legisSearchEntity.html'
	}];

	if($scope.params.reportTemplate && $scope.permission('app.reports')) {
	    $scope.$tabs.push({
		title:'<i class="mdi mdi-chart-bar"></i> Relatório',
		name: "report",
		template: $scope.params.reportTemplate
	    });
	}

	var loadEntity = [];
	for(var i=0; i<$scope.params.types.length; i++) {
	    // push new tab
	    Loading.beginTasks();
	    loadEntity[i] = Loading.pushTask();
	    
	    (function(i) { 
		
		var pathPrefix = '';
		if($scope.params.types.length>1)
		    pathPrefix = $scope.params.types[i];
		
		Form.get({
		    path: avalancheConfig.path[$scope.params.tag]+pathPrefix,
		    nodeService: $scope.params.nodeService,
		    excludeFields: ['userId','groupId','userRight','groupRight',
				    'otherRight','creationDate','lastChanged',
				    'lastChangedUserId','keywords']
		},function(origNewNode) {
		    var tab = {
			name: $scope.params.types[i],
			template: $scope.params.entityItemTemplate,
			node: angular.copy(origNewNode),
			origNewNode: origNewNode
		    };
		    switch(pathPrefix) {
		    case 'legalEntity':
			tab.titleText = 'Pessoa Jurídica';
			break;
		    case 'naturalPerson':
			tab.titleText = 'Pessoa Física';
			break;
		    default:
			tab.titleText = 'Novo';
			break;
		    }
		    $scope.setTabTitleStatus(tab);
		    
		    if(i>0) {
			loadEntity[i-1].promise.then(function() {
			    $scope.$tabs.push(tab);
			});
		    } else {
			$scope.$tabs.push(tab);
		    }
		    
		    loadEntity[i].resolve();
		}, function(){
		    loadEntity[i].reject();
		});
	    })(i);
	}
	Loading.watchTasks();
    }

    $scope.openEntity = function(params) {
	var found = false;
	var tabName = 'entity_'+params.docId;
	for(var i=0; i<$scope.$tabs.length && !found; i++) {
	    if($scope.$tabs[i].name==tabName)
		found = i;
	}

	if(!found) {
	    // aba/pasta não encontrada - abre nova aba
	    Loading.beginTasks();
	    var loadEntity = Loading.pushTask();
	    Loading.watchTasks();

	    var node = Form.get({
		docId: params.docId,
		nodeService: $scope.params.nodeService,
		node: params.node,
		excludeFields: ['userId','groupId','userRight','groupRight',
				'otherRight','creationDate','lastChanged',
				'lastChangedUserId','keywords']
	    },function() {
		loadEntity.resolve();

		if($scope.aggregator) {
		    angular.forEach(node.fields,function(field,index) {
			switch(field.attrs.type) {
			case 'fileList':
			case 'selectMultiple':
			    field.attrs.readOnly = true;
			    break;
			default:
			    field.attrs.accessMode = 'r';
			}
		    });
		}
	    }, function(){
		loadEntity.reject();
	    });

	    $q.all([loadEntity.promise]).then(function(result) {
		var tab = {
		    name: tabName,
		    template: $scope.params.entityItemTemplate,
		    removeBtn: true,
		    node: node
		}

		$scope.setTabTitle(tab);
		$scope.$tabs.push(tab);
		$scope.$tabs.activeTab = tabName;
	    });
	    
	} else {
	    // aba do cliente encontrada - já estava aberta
	    $scope.$tabs.activeTab = tabName;
	}

    }

    $scope.closeEntity = function(tab) {
	$scope.$tabs.splice($scope.$tabs.indexOf(tab),1);
	$scope.scrollToTop();
    }

    $scope.cancelNewEntity = function(tab) {
	if(tab.node.dirty && !tab.saving) {
	    var node = tab.node;
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		template: 'avDiscard.html',
		type: 'warning',
		show: true,
		container: 'body',
		dismissable: true,
		scope: $scope
	    });
	    var discartInterval = $interval(function() {
		error.hide();
	    },avalancheConfig.discardTimeout,1);
	    $scope.undoDiscard = function() {
		$interval.cancel(discartInterval);
		error.hide();
		tab.node = node;
	    }
	    tab.node = angular.copy(tab.origNewNode);
	    $scope.scrollToTop();
	}
    }

    $scope.delEntity = function(tab) {
	var node = tab.node;
	tab.saving = true;

	var deletingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    template: 'avAlertDelete.html',
	    type: 'warning',
	    show: true,
	    container: 'body',
	    dismissable: true,
	    scope: $scope
	});

	$scope.closeEntity();

	var restoreNode = function() {
	    restoreNode = undefined;
	    if($scope.$tabs.indexOf(tab)==-1) {
		$scope.$tabs.push(tab);
	    }
	    $scope.$tabs.activeTab = tab.name;
	}

	var deleteInterval = $interval(function() {
	    deletingAlert.hide();
	    $scope.params.nodeService.delete({
		docId: node.attrs.docId,
		path: avalancheConfig.path[avalancheConfig.folderTables[node.attrs.folderTables]]
	    },function(){
		// documento removido - não precisa fazer nada
		tab.saving = false;
	    }, function(resp) {
		tab.saving = false;
		if(restoreNode)
		    restoreNode();
		var error = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    content: resp.data.statusMessage,
		    template: 'avAlert.html',
		    type: 'danger',
		    show: true,
		    container: 'body',
		    duration: 5,
		    dismissable: true
		});
	    });
	},avalancheConfig.deleteTimeout,1);

	$scope.undoDelete = function($event) {
	    $event.preventDefault();
	    $interval.cancel(deleteInterval);
	    tab.saving = false;
	    deletingAlert.hide();
	    if(restoreNode)
		restoreNode();
	}
    }

    $scope.$watch('$tabs.activeTab',function(newValue,oldValue){
	if(oldValue != newValue && newValue != undefined && oldValue != undefined)
	{
	    switch(newValue)
	    {
	    case 'report':
	    case 'search':
	    case 'new':
	    case 'legalEntity':
	    case 'naturalPerson':
		$state.go('app.'+$scope.params.tag+'.'+newValue.toLowerCase());
		break;
	    default:
		for(var i=0; i<$scope.$tabs.length; i++) {
		    var tab = $scope.$tabs[i];
		    if(tab.name==newValue) {
			$state.go('app.'+$scope.params.tag+'.item',{ 
			    docId: tab.node.attrs.docId
			});
			break;
		    }
		}
		break;
	    }
	}
    });

    switch($state.current.name)
    {
    case 'app.'+$scope.params.tag+'.naturalperson':
	$scope.$tabs.activeTab = 'naturalPerson';
	break;
    case 'app.'+$scope.params.tag+'.legalentity':
	$scope.$tabs.activeTab = 'legalEntity';
	break;
    case 'app.'+$scope.params.tag+'.new':
	$scope.$tabs.activeTab = 'new';
	break;
    case 'app.'+$scope.params.tag+'.search':
	$scope.$tabs.activeTab = 'search';
	break;
    case 'app.'+$scope.params.tag+'.report':
	$scope.$tabs.activeTab = 'report';
	break;
    case 'app.'+$scope.params.tag:
	$state.go('app.'+$scope.params.tag+'.search');
	$scope.$tabs.activeTab = 'search';
	break;
    }
});

mod.controller('EntityItemCtrl', function($scope,$stateParams){
    $scope.openEntity($stateParams);
});

mod.controller('EntityDocCtrl', function($scope,$timeout,$alert,$state,Documents,Form,avalancheConfig) {

    $scope.saveAndClose = function() {
	if($scope.tab.name=='new' ||
	   $scope.tab.name=='legalEntity' ||
	   $scope.tab.name=='naturalPerson') {
	    $scope.tab.openAfterSave = false;
	    $scope.avForm.submit();
	}
	else {
	    $scope.avForm.submit();
	    $scope.closeEntity($scope.tab);
	}
    }

    $scope.save = function() {
	var tab = $scope.tab;
	var node = tab.node;

	// clear new tab before finish saving
	if(tab.name=='new' ||
	   tab.name=='legalEntity' ||
	   tab.name=='naturalPerson') {
	    tab.node = angular.copy(tab.origNewNode);
	    $scope.scrollToTop();
	}
	else {
	    tab.saving = true;
	    $scope.avForm.$setPristine();
	}

	var savingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: $scope.translations['savingTag']+'...',
	    template: 'avAlert.html',
	    type: 'info',
	    show: true,
	    container: 'body',
	    dismissable: true
	});

	Documents.save({
	    docId: node.attrs.docId,
	    path: node.path
	},node.attrs, function(node) {
	    if(tab.name=='new' ||
	       tab.name=='legalEntity' ||
	       tab.name=='naturalPerson') {
		if(tab.openAfterSave) {
		    $scope.openEntity({
			docId: node.attrs.docId, 
			node: node
		    });
		} else {
		    tab.openAfterSave = true;
		}
	    } else {
		tab.saving = false;

		// utiliza serviço do Form para tratar campos do documento
		Form.get({
		    node: node,
		    nodeServie: $scope.params.nodeService,
		    excludeFields: ['userId','groupId','userRight','groupRight',
				    'otherRight','creationDate','lastChanged',
				    'lastChangedUserId','keywords']
		},function(node) {
		    // confirma se documento ainda está aberto na aba antes 
		    // de atualizar
		    if(tab.node && (!tab.node.attrs.docId || 
				   tab.node.attrs.docId==node.attrs.docId)) {
			tab.node = node;			
		    }
		});

		$scope.setTabTitle(tab);
	    }
	    savingAlert.hide();
	},function(resp) {
	    savingAlert.hide();
	    if(tab.name=='new' ||
	       tab.name=='legalEntity' ||
	       tab.name=='naturalPerson') {
		tab.openAfterSave = true;
		tab.node = node;
	    } else {
		if($scope.$tabs.indexOf(tab)==-1) {
		    $scope.$tabs.push(tab);
		} else {
		    $scope.avForm.$setDirty();
		}
		$scope.$tabs.activeTab = tab.name;
		tab.saving = false;
	    }
	    if(resp.data.data && resp.data.data.fieldName) {
		if($scope.avForm[resp.data.data.fieldName]) {
		    $timeout(function(){
			$scope.avForm[resp.data.data.fieldName].$invalid = true;
			$scope.avForm[resp.data.data.fieldName].$visited = true;
			$scope.avForm.checkCollapse();
			$scope.$apply();
		    },0,false);
		}
	    }
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }
    
    $scope.delete = function() {
	$scope.delEntity($scope.tab);
    }

    $timeout(function(){
	$scope.tab.startCollapsed = '0';
	if($scope.tab.name=='new' ||
	   $scope.tab.name=='legalEntity' ||
	   $scope.tab.name=='naturalPerson')
	    $scope.tab.openAfterSave = true;
	else
	    $scope.tab.saving = false;

	if($scope.tab.node.dirty) {
	    $scope.avForm.$setDirty();
	    $scope.setTabTitleStatus($scope.tab);
	} 

	// when the document is changed, the form should be set to pristine
	var unregDirtyWatch = null;

	var regDirtyWatch = function() {
	    unregDirtyWatch = $scope.$watch('avForm.$dirty',function(newValue,oldValue) {
		$scope.tab.node.dirty = newValue;
		$scope.setTabTitleStatus($scope.tab);
	    });
	}

	regDirtyWatch();

	$scope.$watch('tab.node', function(newValue,oldValue) {

	    if(newValue != oldValue && unregDirtyWatch) {
		unregDirtyWatch();
		unregDirtyWatch = null;
	    }

	    if(newValue && !unregDirtyWatch) {
		
		if(newValue.dirty) {
		    $scope.avForm.$setDirty();
		} else {
		    $scope.avForm.$setPristine();
		    $scope.avForm.$setUntouched();
		}

		regDirtyWatch();
	    }
	});
    },0,false);

});

mod.controller('EntitySearchCtrl', function($scope,$controller,Loading){
    $controller('SearchCtrl', {$scope: $scope});

    if(!angular.isDefined($scope.params.searchPlaceholder))
	$scope.params.searchPlaceholder = 'Digite o cnpj, cpf, rg ou nome';
    $scope.setSearch(function() {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	var data = $scope.params.Search.search($scope.search,function() {
	    $scope.docList = data;
	    load.resolve();
	}, function() {
	    load.reject();
	});
    });

});

mod.controller('CustomerSearchCtrl',function($scope,$controller,$state,$modal,$alert,$interval,Loading,Qualification) {
    $controller('EntitySearchCtrl', {$scope: $scope});
    
    $scope.active = -1;

    $scope.qualList = {};

    $scope.open = function(docId) {
	if($scope.active==-1)
	    $state.go('app.'+$scope.params.tag+'.item',{docId:docId});
    }

    $scope.add = function(item) {
	$scope.qualList[item.docId] = item;
    }
    
    $scope.remove = function(item) {
	delete $scope.qualList[item.docId];
    }

    var unselect = function() {
	if (document.selection)
	    document.selection.empty();
	else if (window.getSelection)
	    window.getSelection().removeAllRanges();
    }

    var select = function(objId) {
	unselect();
	if(document.selection) {
	    var range = document.body.createTextRange();
	    range.moveToElementText(document.getElementById(objId));
	    range.select();
	}
	else if (window.getSelection) {
	    var range = document.createRange();
	    range.selectNode(document.getElementById(objId));
	    window.getSelection().addRange(range);
	}
    }

    $scope.select = function() {
	select('qualContent');
    }

    $scope.$on('modal.show', function(event, modal) {
	select('qualContent');
    });

    $scope.$on('modal.hide', function(event, modal) {
	modal.destroy();
    });

    $scope.generate = function() {
	var savingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: 'Gerando...',
	    template: 'avAlert.html',
	    type: 'info',
	    show: true,
	    container: 'body',
	    dismissable: true
	});

	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();

	var list = [];
	angular.forEach($scope.qualList,function(value,key) {
	    this.push(key);
	},list);
	Qualification.save({list:list},function(result) {
	    var modal = $modal({ 
		title: 'Qualificação',
		template: 'legisQualification.html',
		html: true,
		content: result.qualification,
		scope: $scope
	    });
	    load.resolve();
	    savingAlert.hide();
	    
	    var alert = '';
	    for(var i=0; i<result.warningMessages.length; i++) {
		if(i>0)
		    alert += ' <br /><i class="mdi mdi-alert-circle"></i> ';
		alert += result.warningMessages[i];
	    }
	    if(alert != '') {
		$alert({
		    template: 'avAlert.html',
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    content: alert,
		    type: 'warning',
		    show: true,
		    container: 'body',
		    dismissable: true
		});
	    }

	},function() {
	    load.reject();
	    savingAlert.hide();
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }

});


mod.controller('CustomerCtrl',function($scope,$controller,Documents,SearchCustomer) {
    $scope.params = {
	tag: 'customer',
	nodeService: Documents,
	Search: SearchCustomer,
	types: $scope.aggregator?[]:['naturalPerson','legalEntity'],
	icon: ' mdi-account-card-details',
	searchTemplate: 'legisSearchClient.html',
	formTemplate: 'legisEntityForm.html',
	reportTemplate: 'legisCustomerReport.html',
	aggregator: $scope.aggregator,
	collapse: [{
	    title: 'Processos do Cliente',
	    min: 4000,
	    max: 4000
	},{
	    title: 'Processos Representados pelo Cliente',
	    min: 4001,
	    max: 4001
	},{
	    title: 'Geral',
	    min: 4005,
	    max: 4340
	},{
	    title: 'Documentos',
	    min: 4350,
	    max: 4350
	},{
	    title: 'Contrato de honorários',
	    min: 4355,
	    max: 4355
	},{
	    title: 'Lançamentos',
	    min: 4357,
	    max: 4357,
	    condition: 'node && node.attrs.docId && !params.aggregator'
	},{
	    title: 'Representantes do Cliente',
	    min: 4360,
	    max: 4360
	},{
	    title: 'Representados pelo Cliente',
	    min: 4370,
	    max: 4370
	},{
	    title: 'Mensagens',
	    min: 4375,
	    max: 4375,
	    condition: 'node && node.attrs.docId && !params.aggregator && (node.fields.relatedMail.attrs.itemList==undefined || node.fields.relatedMail.attrs.itemList.length>0)'
	},{
	    title: 'Usuário',
	    min: 4380,
	    max: 4380,
	    condition: '!params.aggregator'
	}],
	collapseDef: {
	    'new': 0,
	    old: 0
	}
    }
    $controller('EntityCtrl',{$scope: $scope});
});

mod.controller('CustomerReportCtrl', function($scope,$controller,$alert,CustomerReport,CustomerLabels,Mailto,Loading,FileViewer){
    $controller('ReportCtrl',{$scope:$scope});

    var initForm = function() {
	$scope.search.type = 'screen';
	$scope.values = {};
	$scope.search.paramList = [];
	$scope.docList = undefined;
    }
    initForm();
    $scope.cancel = function() {
	initForm();
	$scope.customerReportForm.$setPristine();
    }

    $scope.$watch('search.type',function(newValue,oldValue) {
	$scope.docList = undefined;
    });

    $scope.setSearch(function() {
	$scope.docList = undefined;

	// loadSearch
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	
	if($scope.search.type=='label') {
	    var doc = CustomerLabels.save({
		params: $scope.search.paramList
	    }, function() {
		var fileViewer = FileViewer({
		    doc: {
			docId: doc.docId,
			contentType: 'application/pdf'
		    },
		    container: '#file-viewer'
		});
		load.resolve();
	    }, function() {
		load.reject();
	    });
	} else {
	    var searchParams = angular.copy($scope.search);
	    if($scope.search.type=='e-mail')
		delete searchParams.limit;

	    var data = CustomerReport.search(searchParams,function() {
		switch($scope.search.type) {
		case 'screen':
		    $scope.docList = data;
		    if(data.children.length==0) {
			var error = $alert({
			    animation: 'am-slide-top',
			    placement: 'top-right',
			    content: 'Nenhum cliente encontrado',
			    template: 'avAlert.html',
			    type: 'warning',
			    show: true,
			    container: 'body',
			    duration: 5,
			    dismissable: true
			});
		    }
		    break;
		case 'e-mail':
		    if(data.totalResults>200) {
			var error = $alert({
			    animation: 'am-slide-top',
			    placement: 'top-right',
			    content: 'Número de e-mails encontrados superior ao máximo de 200',
			    template: 'avAlert.html',
			    type: 'warning',
			    show: true,
			    container: 'body',
			    duration: 5,
			    dismissable: true
			});
		    } else {
		  	window.open(Mailto.url(data.children),'_blank');
		    }
		    break;
		}
		load.resolve();
	    }, function() {
		load.reject();
	    });
	}
    },function(){
	// clearSearch
	initForm();
    },function(){
	// isSearchClear
	return $scope.search.paramList.length==0;
    });

    $scope.fields = {
	type: {
	    attrs: {
		fieldName: 'type',
		label: 'Tipo',
		required: 'required'
	    },
	    children: [{ attrs: {id: "screen", value: 'Consulta na tela'} },
		       { attrs: {id: "e-mail", value: 'E-mail'} },
		       { attrs: {id: "label", value: 'Etiquetas'} }]
	},
	params: {
	    attrs: {
		fieldName: 'params',
		label: 'Parâmetros'
	    },
	    children: [{ attrs: {id: "city", value: 'Cidade'} },
		       { attrs: {id: "state", value: 'Estado'} },
		       { attrs: {id: "age", value: 'Idade do Cliente'} },
		       { attrs: {id: "func", value: 'Função'} },
		       { attrs: {id: "profession", value: 'Profissão'} },
		       { attrs: {id: "folder", value: 'Pasta'} },
		       { attrs: {id: "justice", value: 'Justiça'} },
		       { attrs: {id: "commitment", value: 'Compromisso'} },
		       { attrs: {id: "actionType", value: 'Tipos de Ação'} },
		       { attrs: {id: "local", value: 'Local'} },
		       { attrs: {id: "lawyer", value: 'Advogado'} },
		       { attrs: {id: "phases", value: 'Fase Atual'} },
		       { attrs: {id: "subject", value: 'Assunto'} },
		       { attrs: {id: "situation", value: 'Situação'} },
		       { attrs: {id: "result", value: 'Resultado'} },
		       { attrs: {id: "otherPart", value: 'Outra parte'} },
		       { attrs: {id: "start", value: 'Início'} },
		       { attrs: {id: "value", value: 'Valor do Processo'} }]
	},
	not: {
	    attrs: {
		fieldName: 'not',
		label: 'Sentido Inverso/Negado'
	    },
	    children: []
	}
    };
});

mod.controller('ProviderCtrl', function($scope,$controller,Documents,SearchProvider){
    $scope.params = {
	tag: 'provider',
	nodeService: Documents,
	Search: SearchProvider,
	types: ['legalEntity','naturalPerson'],
	icon: ' mdi-account-card-details',
	formTemplate: 'legisEntityForm.html',
	collapse: [{
	    title: 'Geral',
	    min: 4000,
	    max: 4190
	},{
	    title: 'Documentos',
	    min: 4200,
	    max: 4200
	},{
	    title: 'Contratos',
	    min: 4208,
	    max: 4208,
	    condition: 'node && node.attrs.docId'
	},{
	    title: 'Lançamentos',
	    min: 4209,
	    max: 4209,
	    condition: 'node && node.attrs.docId'
	},{
	    title: 'Mensagens',
	    min: 4210,
	    max: 4210,
	    condition: 'node && node.attrs.docId && (node.fields.relatedMail.attrs.itemList==undefined || node.fields.relatedMail.attrs.itemList.length>0)'
	}],
	collapseDef: {
	    'new': 0,
	    old: 0
	}
    }
    $controller('EntityCtrl',{$scope: $scope});
});

mod.controller('UserCtrl', function($scope,$controller,Documents,SearchUser){
    $scope.params = {
	tag: 'user',
	nodeService: Documents,
	Search: SearchUser,
	types: ['new'],
	icon: ' mdi-account-card-details',
	formTemplate: 'legisEntityForm.html',
	searchPlaceholder: 'Digite o cpf, rg ou nome',
	collapse: [{
	    title: 'Geral',
	    min: 4000,
	    max: 4340
	},{
	    title: 'Documentos',
	    min: 4350,
	    max: 4350
	},{
	    title: 'Contratos',
	    min: 4355,
	    max: 4355,
	    condition: 'node && node.attrs.docId'
	},{
	    title: 'Lançamentos',
	    min: 4357,
	    max: 4357,
	    condition: 'node && node.attrs.docId'
	},{
	    title: 'Mensagens',
	    min: 4360,
	    max: 4360,
	    condition: 'node && node.attrs.docId && (node.fields.relatedMail.attrs.itemList==undefined || node.fields.relatedMail.attrs.itemList.length>0)'
	},{
	    title: 'Usuário',
	    min: 4380,
	    max: 4380
	}],
	collapseDef: {
	    'new': 0,
	    old: 0
	}
    }
    $controller('EntityCtrl',{$scope: $scope});
});

mod.controller('EntryCtrl', function($controller,$scope,Documents,SearchEntry){
    $scope.params = {
	tag: 'entry',
	nodeService: Documents,
	Search: SearchEntry,
	types: ['new'],
	icon: ' mdi-account-card-details',
	entityItemTemplate: 'legisEntryItem.html',
	formTemplate: 'legisEntryForm.html',
	searchTemplate: 'legisSearchEntry.html',
	searchPlaceholder: 'Digite o ID do lançamento, pasta do processo, cnpj, cpf, rg ou nome',
	setTabTitle: function(tab) {
	    tab.titleText = tab.node.attrs.entryId+'/'+tab.node.attrs.entryYear;
	},
	reportTemplate: 'legisEntryReport.html',
	collapse: [{
	    title: 'Geral',
	    min: 4000,
	    max: 4077,
	},{
	    title: 'Documentos',
	    min: 4150,
	    max: 4150
	},{
	    title: 'Fornecedor',
	    min: 4080,
	    max: 4080,
	    condition: 'node && !node.fields.providerId.attrs.hidden'	    
	},{
	    title: 'Processo',
	    min: 4100,
	    max: 4100,
	    condition: 'node && !node.fields.processId.attrs.hidden',
	    required: true
	},{
	    title: 'Cliente',
	    min: 4110,
	    max: 4110,
	    condition: 'node && !node.fields.customerId.attrs.hidden && node.attrs.processId && node.attrs.processId.clientList',
	    required: true
	}],
	collapseDef: {
	    'new': 0,
	    old: 0
	}
    }
    $controller('EntityCtrl',{$scope: $scope});
});

mod.controller('EntryReportCtrl', function($scope,$controller,$alert,EntryReport,Loading,FileViewer){
    var initForm = function() {
	$scope.values = {};
    }
    initForm();
    $scope.cancel = function() {
	initForm();
	$scope.entryReportForm.$setPristine();
    }

    $scope.getReport = function() {
	// loadSearch
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	
	var data = EntryReport.search($scope.values,function() {
	    var fileViewer = FileViewer({
		doc: {
		    docId: data.docId,
		    contentType: 'application/pdf'
		},
		container: '#file-viewer'
	    });		
	    load.resolve();
	}, function(resp) {
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	    load.reject();
	});
    }

    $scope.fields = {
	fromDate: {
	    attrs: {
		fieldName: 'fromDate',
		label: 'De',
		required: 'required'
	    },
	    children: []
	},
	toDate: {
	    attrs: {
		fieldName: 'toDate',
		label: 'Até',
		required: 'required'
	    },
	    children: []
	}
    };
});


mod.controller('EntryDocCtrl', function($controller,$timeout,$scope,$modal,$alert,Process,Expenses,Accountability,FileViewer,Loading) {

    $scope.generate = function($hide) {
	var savingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: 'Gerando prestação de contas...',
	    template: 'avAlert.html',
	    type: 'info',
	    show: true,
	    container: 'body',
	    dismissable: true
	});
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();

	Accountability.save({
	    docId: $scope.tab.node.attrs.docId
	}, { 
	    params: $scope.accData
	} ,function(result) {
	    load.resolve();
	    savingAlert.hide();
	    $hide();
	    $scope.tab.node.attrs.docs.push(result.attrs);
	    $scope.tab.node.setActiveTab(1);
	    FileViewer({
		doc: result.attrs,
		container: '#file-viewer'
	    });

	},function(resp) {
	    load.reject();
	    savingAlert.hide();
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }

    $scope.accountability = function() {
	Loading.beginTasks();
	var loadClientList = Loading.pushTask();
	var loadExpenses = Loading.pushTask();

	Loading.watchTasks(function(){
	    modal = $modal({ 
		template: 'legisAccountability.html',
		html: true,
		scope: $scope
	    });
	});

	$scope.accData = {
	    title: 'Processo '+ $scope.tab.node.attrs.processId.number.join(', '),
	    fees: $scope.config.children[0].defFees,
	    clientList: [],
	    totalInss: 0,
	    totalIr: 0, 
	    totalFgts: 0,
	    extraCols: [],
	    totalGross: 0,
	    totalFees: 0,
	    expenses: 0,
	    totalExpensesPercent: 0,
	    totalNetvalue: 0,
	    succumbing: parseFloat($scope.tab.node.attrs.succumbing)
	};

	Process.get({ 
	    docId: $scope.tab.node.attrs.processId.processId,
	    fieldName: 'clientList'
	}, function(data) {
	    if(data.attrs.clientList.length>0) {
		var clientList = data.attrs.clientList.filter(function(child) {
		    return child.excluded != '1'
		});
		var expensesPercent = 100/clientList.length;
		$scope.accData.clientList = clientList.map(function(child) {
		    child.accepted = 0;
		    child.inss = 0;
		    child.ir = 0;
		    child.fgts = 0;
		    child.expensesPercent = expensesPercent;
		    return child;
		});
	    }
	    loadClientList.resolve();
	}, function() {
	    loadClientList.reject();
	});

	Expenses.get({
	    docId: $scope.tab.node.attrs.docId
	}, function(data) {
	    $scope.expenses = data.expenses;
	    $scope.accData.succumbing += parseFloat(data.succumbing);
	    loadExpenses.resolve();
	}, function() {
	    loadExpenses.reject();
	});
	
    };

    $scope.$on('modal.show', function(event, modal) {
	var input = modal.$element.find('input');
	if(input.length>1)
	    input[1].select();
    });

    $scope.$on('modal.hide', function(event, modal) {
	modal.destroy();
    });

    $controller('EntityDocCtrl',{$scope: $scope});
});

mod.controller('AccountabilityCtrl', function($scope,$alert,$interval,avalancheConfig){

    $scope.removeExtraCol = function(col) {
	var list = $scope.accData.extraCols;
	var undoData  = list.splice(list.indexOf(col),1);
	$scope.doAccountability();
	
	var deletingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    template: 'avAlertDelete.html',
	    type: 'warning',
	    show: true,
	    container: 'body',
	    dismissable: true,
	    scope: $scope
	});
	var deleteInterval = $interval(function() {
	    deletingAlert.hide();
	},avalancheConfig.deleteTimeout,1);
	
	$scope.undoDelete = function($event) {
	    deletingAlert.hide();
	    $event.preventDefault();
	    $interval.cancel(deleteInterval);
	    list.push(undoData[0]);
	    $scope.doAccountability();
	}

    }

    $scope.doAccountability = function() {
	var totalApproved = 0;
	$scope.accData.totalExpensesPercent = 0;
	for(var i=0; i<$scope.accData.clientList.length; i++) {
	    totalApproved += parseFloat($scope.accData.clientList[i].accepted);
	    $scope.accData.clientList[i].percent = 0;

	    $scope.accData.totalExpensesPercent += parseFloat($scope.accData.clientList[i].expensesPercent);
	    for(var j=0; j<$scope.accData.extraCols.length; j++) {
		$scope.accData.extraCols[j].total = 0;
	    }
	}

	$scope.accData.totalInss = 0;
	$scope.accData.totalIr = 0;
	$scope.accData.totalFgts = 0;
	$scope.accData.totalGross = 0;
	$scope.accData.totalNetValue = 0;
	for(var i=0; i<$scope.accData.clientList.length; i++) {
	    var client = $scope.accData.clientList[i];

	    // percentagem do valor homologado
	    if(totalApproved>0) {
		client.percent = 100*parseFloat(client.accepted)/totalApproved;

		// alvará
		client.charter = client.percent/100*($scope.tab.node.attrs.value-
						     $scope.tab.node.attrs.succumbing);

		// valor bruto
		client.gross = 
		    client.charter+
		    parseFloat(client.inss)+
		    parseFloat(client.ir)+
		    parseFloat(client.fgts);
		$scope.accData.totalInss += parseFloat(client.inss);
		$scope.accData.totalIr += parseFloat(client.ir);
		$scope.accData.totalFgts += parseFloat(client.fgts);
		for(var j=0; j<$scope.accData.extraCols.length; j++) {
		    var col = $scope.accData.extraCols[j];
		    client.gross += parseFloat(col.values[i]);
		    col.total += parseFloat(col.values[i]);
		}
		$scope.accData.totalGross += client.gross;

		// honorários
		client.fees = client.gross*parseFloat($scope.accData.fees)/100;

		// liquido
		client.netValue = client.charter-client.fees-client.expenses;
		$scope.accData.totalNetValue += client.netValue;
	    }

	    // despesas
	    client.expenses = parseFloat(client.expensesPercent)/100*$scope.expenses;
	}   
    }

    $scope.doAccountability();

});

mod.controller('DocsCtrl', function($scope,$state,NodeModal,Loading, Documents,$filter){
    $scope.registerPerm = $scope.permission('register.docs');

    $scope.$tabs = [{ 
	title:'<i class="mdi mdi-folder-multiple"></i> Pastas', 
	name: "folders", 
	template: 'legisNavDocs.html'
    },{ 
	title:'<i class="mdi mdi-magnify"></i> Busca', 
	name: "search", 
	template: 'legisSearchDocs.html'
    }];

    $scope.$tabs.activeTab = 'folders';

    $scope.openDocTab = function(params) {
	var found = false;
	var tabName = 'doc_'+params.docId;
	for(var i=0; i<$scope.$tabs.length && !found; i++) {
	    if($scope.$tabs[i].name==tabName)
		found = i;
	}

	if(!found) {
	    // aba não encontrada - abre nova aba
	    Loading.beginTasks();
	    var load = Loading.pushTask();
	    Loading.watchTasks();

	    var node = Documents.get({
		docId: params.docId,
		docLinks: '1'
	    },function() {
		var tab = {
		    name: tabName,
		    template: 'legisDocsItem.html',
		    removeBtn: true,
		    node: node,
		    title: '<span title="'+node.attrs.title+'">'+
			'<i class="mdi mdi-file-document"></i> ' + 
			$filter('cut')(node.attrs.title,true,20) + '</span>'
		}
		$scope.$tabs.push(tab);
		$scope.$tabs.activeTab = tabName;
		load.resolve();
	    }, function(){
		load.reject();
	    });
	    
	} else {
	    // aba do cliente encontrada - já estava aberta
	    $scope.$tabs.activeTab = tabName;
	}

    }

    $scope.closeDocTab = function(tab) {
	$scope.$tabs.splice($scope.$tabs.indexOf(tab),1);
	$scope.scrollToTop();
    }

    $scope.openDoc = function(path, item) {
	var params = {
	    title: 'Documento',
	    path: path,
	    buttons: { history: false, remove: true },
	    excludeFields:  ['userId','groupId','userRight','groupRight',
			     'otherRight','creationDate','lastChanged',
			     'lastChangedUserId','keywords','contentType',
			     'authors','descr','publicationDate',
			     'pendingApproval'],
	    taToolbar: [
		['bold', 'italics', 'underline', 'undo', 'redo', 'clear'],
		['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
		['insertImage','insertLink','insertVideo']
	    ],
	    onSave: function(node) {
		$scope.$broadcast('save',item,node.attrs);
	    },
	    onBeforeDelete: function() {
		$scope.$broadcast('before.delete',item);
		return [item];
	    },
	    onUndoDelete: function(undoData) {
		$scope.$broadcast('undo.delete',undoData);
	    }
	}

	if(item) {
	    params.docId = item.docId;
	}
	NodeModal(params);
    }

    $scope.$watch('$tabs.activeTab',function(newValue,oldValue){
	if(oldValue != newValue && newValue != undefined && oldValue != undefined)
	{
	    switch(newValue)
	    {
	    case 'folders':
	    case 'search':
		$state.go('app.docs.'+newValue);
		break;
	    default:
		for(var i=0; i<$scope.$tabs.length; i++) {
		    var tab = $scope.$tabs[i];
		    if(tab.name==newValue) {
			$state.go('app.docs.item',{ 
			    docId: tab.node.attrs.docId
			});
			break;
		    }
		}
		break;
	    }
	}
    });

    switch($state.current.name)
    {
    case 'app.docs.folders':
	$scope.$tabs.activeTab = 'folders';
	break;
    case 'app.docs.search':
	$scope.$tabs.activeTab = 'search';
	break;
    case 'app.docs':
	$state.go('app.docs.folders');
	$scope.$tabs.activeTab = 'folders';
	break;
    }


});

mod.controller('DocsNavTreeCtrl',function($rootScope,$scope,$alert,Search,Folders,Loading,NodeModal,DocOrder,FolderOrder,avalancheConfig) {

    $scope.aSortableDocListeners = {
	orderChanged: function(event) {
	    DocOrder.save({
		docId: event.source.itemScope.item['docId']
	    },{
		path: $scope.tree.get_selected_branch().data.path,
		offset: event.dest.index - event.source.index
	    }, function() {
	    }, function(resp) {
		var error = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    content: resp.data.statusMessage,
		    template: 'avAlert.html',
		    type: 'danger',
		    show: true,
		    container: 'body',
		    duration: 5,
		    dismissable: true
		});
	    });
	}
    }


    $scope.newDoc = function() {
	$scope.openDoc($scope.tree.get_selected_branch().data.path);
    }

    $scope.disableFolderUp = function() {
	var current = $scope.tree.get_selected_branch();
	if(!current)
	    return true;
	var parent = $scope.tree.get_parent_branch(current);
	return !parent || parent.children.indexOf(current)==0;
    }

    $scope.disableFolderDown = function() {
	var current = $scope.tree.get_selected_branch();
	if(!current)
	    return true;
	var parent = $scope.tree.get_parent_branch(current);
	return !parent || parent.children.indexOf(current)==parent.children.length-1;
    }

    $scope.setFolderOrder = function(up) {
	var current = $scope.tree.get_selected_branch();
	var parent = $scope.tree.get_parent_branch(current);
	var pos = parent.children.indexOf(current);

	// move current
	parent.children.splice(pos,1);
	parent.children.splice(pos+(up?-1:1),0,current);

	FolderOrder.save({
	    folderPath: current.data.path,
	    up: (up?'1':'0')
	}, function() {
	}, function(resp) {
	    parent.children.splice(pos,1);
	    parent.children.splice(pos+(up?+1:-1),0,current);
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }
    
    var openFolder = function(params) {
	var currBranch;
	var modalParams = {
	    title: 'Pasta',
	    folder: true,
	    buttons: { history: false, remove: true },
	    excludeFields:  ['userId','groupId','userRight','groupRight',
			     'otherRight','creationDate','lastChanged',
			     'lastChangedUserId','restrictedDeletion',
			     'menuTitle','title','descr','content',
			     'imageList','folderTables'],
	    extraFields: {
		folderTables: 10
	    },
	    onSave: function(node) {
		if(params.folderPath) {
		    var newNode = {
			label: node.attrs.path,
			data: { path: params.folderPath+node.attrs.path+'/'},
			children: []
		    };
		    $scope.tree.add_branch($scope.tree.get_selected_branch(),newNode);
		} else {
		    $scope.tree.get_selected_branch().label = node.attrs.path;
		}
	    },
	    onBeforeDelete: function() {
		currBranch = {
		    branch: $scope.tree.get_selected_branch(),
		    parent: $scope.tree.select_parent_branch()
		};
		currBranch.pos = currBranch.parent.children.indexOf(currBranch.branch);
		currBranch.parent.children.splice(currBranch.pos,1);
		return currBranch;
	    },
	    onUndoDelete: function() {
		currBranch.parent.children.splice(currBranch.pos,0,currBranch.branch);
	    }
	}
	angular.extend(modalParams,params);
	NodeModal(modalParams);

    }

    $scope.newFolder = function() {
	openFolder({ 
	    folderPath: $scope.tree.get_selected_branch().data.path
	});
    }

    $scope.editFolder = function() {
	openFolder({
	    folderId: $scope.tree.get_selected_branch().data.folderId
	});
    }

    $scope.$on('save',function(event,item,attrs){
	if($scope.docList) {
	    if(item) {
		for(var i=0; i<$scope.docList.children.length; i++) {
		    if($scope.docList.children[i].docId==item.docId)
			$scope.docList.children[i] = attrs;
		}
	    } else {
		$scope.docList.children.push(attrs);
	    }
	}
    });

    $scope.tree = {};

    $scope.treeData = [{
	label: 'Documentos',
	data: { path: avalancheConfig.path.documents },
	children: []
    }];

    var loadTree = function(path,children) {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	var folders = Folders.query({
	    folderId: '*',
	    path: path,
	    fixedOrder: '1'
	},function() {
	    for(var i=0; i<folders.length;i++) {
		var newNode = {
		    label: folders[i].name,
		    data: { 
			path: path+folders[i].name+'/',
			folderId: folders[i].folderId
		    },
		    children: []
		};

		loadTree(path+folders[i].name+'/',newNode.children);
		children.push(newNode);
	    }
	    load.resolve();
	},function(){
	    load.reject();
	});
    }

    loadTree(avalancheConfig.path.documents,$scope.treeData[0].children);

    $scope.onSelect = function(branch) {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	$scope.docList = undefined;
	$scope.docList = Search.search({
	    path: branch.data.path,
	    orderBy: 'fixedOrder'
	},function(){
	    load.resolve();
	},function(resp){
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	    load.reject();
	});
    }
});

mod.controller('DocsSearchCtrl', function($scope,$controller,$alert,Search,Loading,avalancheConfig){
    $controller('SearchCtrl', {$scope: $scope});

    $scope.setSearch(function() {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	$scope.search.path = avalancheConfig.path.documents;
	$scope.search.searchCriteria = avalancheConfig.searchCriteria.documents;
	$scope.search.recursiveSearch = '1';
	var data = Search.search($scope.search,function() {
	    $scope.docList = data;
	    if(data.children.length==0) {
		var error = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    content: 'Nenhum documento encontrado',
		    template: 'avAlert.html',
		    type: 'warning',
		    show: true,
		    container: 'body',
		    duration: 5,
		    dismissable: true
		});
	    }
	    load.resolve();
	}, function() {
	    load.reject();
	});
    });

    $scope.$on('save',function(event,item,attrs){
	if(item && $scope.docList) {
	    for(var i=0; i<$scope.docList.children.length; i++) {
		if($scope.docList.children[i].docId==item.docId)
		    $scope.docList.children[i] = attrs;
	    }
	}
    });

});

mod.controller('DocsItemCtrl', function($scope,$stateParams){
    $scope.openDocTab($stateParams);
});

mod.controller('DocsDocCtrl', function($scope,$alert,Form,Documents,Search,Loading,avalancheConfig){

    $scope.$on('save',function(event,item,attrs){
	if(attrs.docId == $scope.tab.node.attrs.docId) {
	    $scope.tab.node.attrs = attrs;
	}
    });

    Loading.beginTasks();
    var loadCommentDoc = Loading.pushTask();
    var loadCommentList = Loading.pushTask();
    Loading.watchTasks();

    var origCommentDoc;

    Form.get({
	path: avalancheConfig.path.comments,
	excludeFields: ['userId','groupId','userRight','groupRight',
			'otherRight','creationDate','lastChanged',
			'lastChangedUserId','keywords','commentType',
			'publicationDate']
    },function(node) {
	node.fields.comment.attrs.fieldName += '_'+$scope.tab.node.attrs.docId;
	origCommentDoc = node;
	$scope.commentDoc = angular.copy(origCommentDoc);
	loadCommentDoc.resolve();
    },function(resp){
	var error = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: resp.data.statusMessage,
	    template: 'avAlert.html',
	    type: 'danger',
	    show: true,
	    container: 'body',
	    duration: 5,
	    dismissable: true
	});
	loadCommentDoc.reject();
    });

    var saving = false;
    $scope.saveComment = function() {
	if(saving)
	    return;

	saving = true;
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	$scope.commentDoc.attrs.articleId = $scope.tab.node.attrs.docId;
	$scope.commentDoc.attrs.comment = $scope.commentDoc.attrs['comment_'+$scope.tab.node.attrs.docId];
	Documents.save({
	    path: avalancheConfig.path.comments
	},$scope.commentDoc.attrs, function(newComment){
	    saving = false;
	    $scope.commentList.unshift(newComment.attrs);
	    $scope.commentDoc = angular.copy(origCommentDoc);
	    load.resolve();
	}, function(resp) {
	    saving = false;
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	    load.reject()
	});
    }

    $scope.commentList = [];
    var commentList = Search.search({
	path: avalancheConfig.path.comments,
	exclusionFilters_numOfOptions: 1,
	exclusionFilters_1__fieldName: 'articleId',
	exclusionFilters_1__value: $scope.tab.node.attrs.docId,
	orderBy: 'creationDate',
	order: '0'
    }, function() {
	$scope.commentList = commentList.children;
	loadCommentList.resolve();
    }, function(resp) {
	var error = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: resp.data.statusMessage,
	    template: 'avAlert.html',
	    type: 'danger',
	    show: true,
	    container: 'body',
	    duration: 5,
	    dismissable: true
	});	
	loadCommentList.reject();
    });


});

mod.controller('GenerateReceipt', function($scope,Loading,Receipt,avalancheConfig,$alert) {
    $scope.datepickerFormat = avalancheConfig.datepickerFormat;

    $scope.receipt = {
	values: {},
	fields: {
	    processNumber: {
		attrs: {
		    fieldName: 'processNumber',
		    label: 'Número',
		    required: 'required'
		},
		children: []
	    },
	    value: {
		attrs: {
		    fieldName: 'value',
		    label: 'Valor',
		    required: 'required'
		}
	    },
	    date: {
		attrs: {
		    fieldName: 'date',
		    label: 'Data',
		    required: 'required'
		}
	    }
	}
    };

    for(var i=0; i<$scope.values['processNumbers'].length;i++) {
	$scope.receipt.fields.processNumber.children.push({ 
	    attrs: {
		id: $scope.values['processNumbers'][i],
		value: $scope.values['processNumbers'][i]
	    }
	});
    }

    $scope.gerar = function($hide,popoverForm) {
	if(!popoverForm.$valid) {
	    popoverForm.$setSubmitted();
	    return;
	}

	$hide();
	var savingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: 'Gerando...',
	    template: 'avAlert.html',
	    type: 'info',
	    show: true,
	    container: 'body',
	    dismissable: true
	});

	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	Receipt.save({
	    processId: $scope.values['docId'],
	    entityId: $scope.item.entityId
	}, $scope.receipt.values, function(receipt){
	    load.resolve();
	    savingAlert.hide();
	    $scope.receiptList.push(receipt);
	}, function(resp){
	    load.reject();
	    savingAlert.hide();
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }
});

mod.controller('CancelReceipt', function($scope,Loading,Receipt,$alert) {

    $scope.data = {
	values: {},
	fields: {
	    observations: {
		attrs: {
		    fieldName: 'observations',
		    label: 'Observações',
		    required: 'required'
		},
		children: []
	    }
	}
    };

    $scope.cancel = function($hide) {
	$scope.item.canceled = '0';
	$hide();
    }

    $scope.confirm = function($hide,popoverForm) {
	if(!popoverForm.$valid) {
	    popoverForm.$setSubmitted();
	    return;
	}

	$hide();
	var savingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: 'Cancelando recibo...',
	    template: 'avAlert.html',
	    type: 'info',
	    show: true,
	    container: 'body',
	    dismissable: true
	});

	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();

	Receipt.delete({
	    number: $scope.item['number'],
	    year: $scope.item['year'],
	    observations: $scope.data.values['observations']
	}, function(receipt){
	    load.resolve();
	    savingAlert.hide();
	    $scope.item.observations = receipt.observations;
	}, function(resp){
	    load.reject();
	    savingAlert.hide();
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }
});

mod.controller('CustomerObs', function($scope) {

    $scope.data = {
	values: { 
	    observations: $scope.item.reason 
	},
	fields: {
	    observations: {
		attrs: {
		    fieldName: 'observations',
		    label: 'Observações de ' + $scope.item.name
		},
		children: []
	    }
	}
    };

    $scope.update = function($hide,popoverForm) {
	if(!popoverForm.$valid) {
	    popoverForm.$setSubmitted();
	    return;
	}

	$hide();

	$scope.item.reason = $scope.data.values.observations;
    }
});


mod.controller('ConfigsCtrl',function($scope,$state,$timeout,$interval,$alert,Loading,Search,Form,avalancheConfig) {
    $scope.$tabs = [{
	title:'<i class="mdi mdi-settings"></i> Definições', 
	name: "settings", 
	template: 'legisSettings.html'
    },{ 
	title:'<i class="mdi mdi-table"></i> Tabelas', 
	name: "tables", 
	template: 'legisTables.html'
    },{ 
	title:'<i class="mdi mdi-web"></i> Website', 
	name: "website", 
	template: 'legisWebsite.html'
    }];
    $scope.$tabs.activeTab = 'settings';


    $scope.$tabs[0].excludeFields = ['userId','groupId','userRight','groupRight',
				    'otherRight','creationDate','lastChanged',
				    'lastChangedUserId','keywords','title','contentType'];

    if($scope.undoData.configs) {
	$scope.$tabs[0].origNode = $scope.undoData.configs.origNode;
	$scope.$tabs[0].node = $scope.undoData.configs.node;
	$timeout(function() {
	    $scope.$tabs[0].onNodeLoad();
	},0,false);
    } else {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	
	var configSearch = Search.search({
	    path: avalancheConfig.path.config
	},function() {
	    if(configSearch.children.length==0) {
		var error = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    content: 'Arquivo de configuração inexistente!',
		    template: 'avAlert.html',
		    type: 'danger',
		    show: true,
		    container: 'body',
		    duration: 5,
		    dismissable: true
		});
		load.reject();
		return;
	    }
	    
	    // utiliza serviço do Form para tratar campos do documento
	    Form.get({
		docId: configSearch.children[0].docId,
		excludeFields: $scope.$tabs[0].excludeFields
	    },function(node) {
		$scope.$tabs[0].origNode = node;
		$scope.$tabs[0].node = angular.copy(node);
		$scope.$tabs[0].onNodeLoad();
		load.resolve();
	    });
	},function(resp) {
	    savingAlert.hide();
	    load.reject();
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }

    $scope.setSettingsTitleStatus = function(dirty) {
	var title = 'Definições';
	if(dirty)
	    title = '<em>'+title+'</em>';
	$scope.$tabs[0].title = '<i class="mdi mdi-settings"></i> ' + title; 
    }

    var checkDirty = function() {
	return $scope.$tabs[0].node.dirty;
    }

    $scope.setCheckDirty(checkDirty);

    $scope.$on('$destroy',function(){
	$scope.setCheckDirty(null);

	if($scope.logginOut)
	    return;

	if(!checkDirty())
	    return;

	var parent = $scope.$parent;
	parent.undoData.configs = {
	    node: $scope.$tabs[0].node,
	    origNode: $scope.$tabs[0].origNode
	};

	var error = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    template: 'avDiscard.html',
	    type: 'warning',
	    show: true,
	    container: 'body',
	    dismissable: true,
	    scope: parent
	});
	var discartInterval = $interval(function() {
	    error.hide();
	    parent.undoData.configs = null;
	    parent.undoDiscard = null;
	},avalancheConfig.discardTimeout,1);
	parent.undoDiscard = function() {
	    $interval.cancel(discartInterval);
	    error.hide();
	    $state.go('app.configs.settings');
	    parent.undoDiscard = null;
	}
    });


    $scope.$watch('$tabs.activeTab',function(newValue,oldValue) {
	if(oldValue != newValue && newValue != undefined && oldValue != undefined)
	{
	    $state.go('app.configs.'+newValue);
	}
    });

    switch($state.current.name)
    {
    case 'app.configs.settings':
	$scope.$tabs.activeTab = 'settings';
	break;
    case 'app.configs.tables':
	$scope.$tabs.activeTab = 'tables';
	break;
    case 'app.configs.website':
	$scope.$tabs.activeTab = 'website';
	break;
    case 'app.configs':
	$state.go('app.configs.settings');
	$scope.$tabs.activeTab = 'settings';
	break;
    }
});

mod.controller('TablesCtrl',function($scope,NodeModal,avalancheConfig) {
    $scope.fields = {
	tables: {
	    attrs: {
		fieldName: 'tables',
		label: 'Tabela'
	    },
	    children: [{ attrs: {id: "justice", value: 'Justiça'} },
		       { attrs: {id: "actionType", value: 'Tipos de Ação'} },
		       { attrs: {id: "local", value: 'Local'} },
		       { attrs: {id: "phases", value: 'Fase Atual'} },
		       { attrs: {id: "subject", value: 'Assunto'} },
		       { attrs: {id: "situation", value: 'Situação'} },
		       { attrs: {id: "result", value: 'Resultado'} }]
	}
    };

    $scope.openItem = function(id) {
	var params = {
	    title: 'Item',
	    path: '/'+$scope.values.tables+'/',
	    buttons: { history: false, remove: true },
	    excludeFields:  ['userId','groupId','userRight','groupRight',
			     'otherRight','creationDate','lastChanged',
			     'lastChangedUserId','keywords','contentType',
			     'authors','descr'],
	    onSave: function(node) {
		$scope.$broadcast('reload-search-fields','id');
	    },
	    onBeforeDelete: function() {
		var children = $scope.fields.extra.id.children;
		for(var i=0;i<children.length;i++) {
		    if(children[i].attrs.id==id) {
			return children.splice(i,1);
		    }
		}
	    },
	    onUndoDelete: function(undoData) {
		$scope.$broadcast('reload-search-fields','id');
	    }
	}
    	if(id) {
	    params.docId = id;
	}
	NodeModal(params);
    }
});

mod.controller('WebsiteCtrl',function($scope,$alert,Search,Folders,Loading,NodeModal,DocOrder,FolderOrder,avalancheConfig) {
    $scope.aSortableDocListeners = {
	orderChanged: function(event) {
	    DocOrder.save({
		docId: event.source.itemScope.item['docId']
	    },{
		path: $scope.tree.get_selected_branch().data.path,
		offset: event.dest.index - event.source.index
	    }, function() {
	    }, function(resp) {
		var error = $alert({
		    animation: 'am-slide-top',
		    placement: 'top-right',
		    content: resp.data.statusMessage,
		    template: 'avAlert.html',
		    type: 'danger',
		    show: true,
		    container: 'body',
		    duration: 5,
		    dismissable: true
		});
	    });
	}
    }

    $scope.openPage = function(item) {
	var params = {
	    title: 'Página',
	    path: $scope.tree.get_selected_branch().data.path,
	    buttons: { history: false, remove: true },
	    excludeFields:  ['userId','groupId','userRight','groupRight',
			     'otherRight','creationDate','lastChanged',
			     'lastChangedUserId','keywords','contentType',
			     'authors','descr'],
	    taToolbar: [
		['bold', 'italics', 'underline', 'undo', 'redo', 'clear'],
		['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
		['insertImage','insertLink','insertVideo']
	    ],
	    onSave: function(node) {
		if($scope.docList) {
		    if(item) {
			var items = $scope.docList.children;
			items[items.indexOf(item)] = node.attrs;
		    } else {
			$scope.docList.children.push(node.attrs);
		    }
		}
	    },
	    onBeforeDelete: function() {
		var items = $scope.docList.children;
		return items.splice(items.indexOf(item),1);
	    },
	    onUndoDelete: function(undoData) {
		items.push(undoData[0]);
	    }
	}

	if(item) {
	    params.docId = item.docId;
	}
	NodeModal(params);
    }

    $scope.disableFolderUp = function() {
	var current = $scope.tree.get_selected_branch();
	if(!current)
	    return true;
	var parent = $scope.tree.get_parent_branch(current);
	return !parent || parent.children.indexOf(current)==0;
    }

    $scope.disableFolderDown = function() {
	var current = $scope.tree.get_selected_branch();
	if(!current)
	    return true;
	var parent = $scope.tree.get_parent_branch(current);
	return !parent || parent.children.indexOf(current)==parent.children.length-1;
    }

    $scope.setFolderOrder = function(up) {
	var current = $scope.tree.get_selected_branch();
	var parent = $scope.tree.get_parent_branch(current);
	var pos = parent.children.indexOf(current);

	// move current
	parent.children.splice(pos,1);
	parent.children.splice(pos+(up?-1:1),0,current);

	FolderOrder.save({
	    folderPath: current.data.path,
	    up: (up?'1':'0')
	}, function() {
	}, function(resp) {
	    parent.children.splice(pos,1);
	    parent.children.splice(pos+(up?+1:-1),0,current);
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }
    
    $scope.editFolder = function() {
	var currBranch;
	var modalParams = {
	    path: $scope.tree.get_selected_branch().data.path,
	    title: 'Área',
	    folder: true,
	    buttons: { history: false, remove: true },
	    excludeFields:  ['userId','groupId','userRight','groupRight',
			     'otherRight','creationDate','lastChanged',
			     'lastChangedUserId','restrictedDeletion',
			     'path','folderTables'],
	    extraFields: {
		folderTables: 10
	    },
	    onSave: function(node) {
		$scope.tree.get_selected_branch().label = node.attrs.path;
	    }
	}
	NodeModal(modalParams);

    }


    $scope.tree = {};

    $scope.treeData = [{
	label: 'Página Inicial',
	data: { path: avalancheConfig.path.website },
	children: []
    }];

    var loadTree = function(path,children) {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	var folders = Folders.query({
	    folderId: '*',
	    path: path,
	    fixedOrder: '1'
	},function() {
	    for(var i=0; i<folders.length;i++) {
		var newNode = {
		    label: folders[i].name,
		    data: { 
			path: path+folders[i].name+'/',
			folderId: folders[i].folderId
		    },
		    children: []
		};

		loadTree(path+folders[i].name+'/',newNode.children);
		children.push(newNode);
	    }
	    load.resolve();
	},function(){
	    load.reject();
	});
    }

    loadTree(avalancheConfig.path.website,$scope.treeData[0].children);

    $scope.onSelect = function(branch) {
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();
	$scope.docList = undefined;
	$scope.docList = Search.search({
	    path: branch.data.path,
	    orderBy: 'fixedOrder'
	},function(){
	    load.resolve();
	},function(resp){
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	    load.reject();
	});
    }

});

mod.controller('SettingsCtrl', function($scope,$alert,$interval,Documents,Form,avalancheConfig) {
    var saving = false;

    $scope.tab.onNodeLoad = function() {
	if($scope.tab.node.dirty) {
	    $scope.avForm.$setDirty();
	    $scope.setSettingsTitleStatus(true);
	} 

	// when the document is changed, the form should be set to pristine
	var unregDirtyWatch = null;

	var regDirtyWatch = function() {
	    unregDirtyWatch = $scope.$watch('avForm.$dirty',function(newValue,oldValue) {
		$scope.tab.node.dirty = newValue;
		$scope.setSettingsTitleStatus(newValue);
	    });
	}

	regDirtyWatch();

	$scope.$watch('tab.node', function(newValue,oldValue) {

	    if(newValue != oldValue && unregDirtyWatch) {
		unregDirtyWatch();
		unregDirtyWatch = null;
	    }

	    if(newValue && !unregDirtyWatch) {
		
		if(newValue.dirty) {
		    $scope.avForm.$setDirty();
		} else {
		    $scope.avForm.$setPristine();
		    $scope.avForm.$setUntouched();
		}

		regDirtyWatch();
	    }
	});
    }

    $scope.save = function() {
	if(saving)
	    return;

	var node = $scope.tab.node;
	saving = true;
	$scope.avForm.$setPristine();

	var savingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: $scope.translations['savingTag']+'...',
	    template: 'avAlert.html',
	    type: 'info',
	    show: true,
	    container: 'body',
	    dismissable: true
	});

	Documents.save({
	    docId: node.attrs.docId,
	    path: avalancheConfig.path.config
	},node.attrs, function(node) {
	    saving = false;

	    // utiliza serviço do Form para tratar campos do documento
	    Form.get({
		node: node,
		excludeFields: $scope.tab.excludeFields
	    },function(node) {
		$scope.tab.origNode = node;
		$scope.tab.node = angular.copy(node);
	    });
	    
	    savingAlert.hide();

	},function(resp) {
	    savingAlert.hide();
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }
    
    $scope.cancel = function() {
	if($scope.tab.node.dirty && !saving) {
	    var node = $scope.tab.node;
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		template: 'avDiscard.html',
		type: 'warning',
		show: true,
		container: 'body',
		dismissable: true,
		scope: $scope
	    });
	    var discartInterval = $interval(function() {
		error.hide();
	    },avalancheConfig.discardTimeout,1);
	    $scope.undoDiscard = function() {
		$interval.cancel(discartInterval);
		error.hide();
		$scope.tab.node = node;
	    }
	    $scope.tab.node = angular.copy($scope.tab.origNode);
	    $scope.scrollToTop();
	}
    }
});

mod.controller('GenericCollapseCtrl', function($scope) {
    $scope.active = 0;
});

mod.controller('RelatedMailModalCtrl', function($scope,$alert,Loading,FileViewer,PDFMail) {
    $scope.print = function() {
	var savingAlert = $alert({
	    animation: 'am-slide-top',
	    placement: 'top-right',
	    content: 'Gerando PDF para impressão...',
	    template: 'avAlert.html',
	    type: 'info',
	    show: true,
	    container: 'body',
	    dismissable: true
	});
	Loading.beginTasks();
	var load = Loading.pushTask();
	Loading.watchTasks();

	PDFMail.save({ 
	    params: $scope.node.attrs
	},function(result) {
	    load.resolve();
	    savingAlert.hide();
	    FileViewer({
		doc: result.attrs,
		container: '#file-viewer'
	    });

	},function(resp) {
	    load.reject();
	    savingAlert.hide();
	    var error = $alert({
		animation: 'am-slide-top',
		placement: 'top-right',
		content: resp.data.statusMessage,
		template: 'avAlert.html',
		type: 'danger',
		show: true,
		container: 'body',
		duration: 5,
		dismissable: true
	    });
	});
    }
});



//
// Directives
//
mod.directive('legisField',function(Parser) {
    return {
	restrict: "E",
	templateUrl: 'legisField.html',
	link: function(scope,element,attr) {
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('legisFolder',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '='
	},
	templateUrl: 'legisFolderField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	}
    };
});

mod.directive('legisCustomerReceipts',function($modal,$popover,Receipt,Loading,avalancheConfig) {
    return {
	restrict: "A",
	scope: {
	    values: '=',
	    item: '=',
	    translations: '='
	},
	link: function(scope,element,attr) {
	    scope.dateFormat = avalancheConfig.dateFormat;

	    var options = {
		show: false,
		template: 'legisCustomerReceipts.html',
		scope: scope
	    }

            // Initialize modal
            var modal = $modal(options);

            // Trigger
            element.on('click', function(){
		Loading.beginTasks();
		var load = Loading.pushTask();
		Loading.watchTasks();
		scope.receiptList = Receipt.query({
		    processId: scope.values['docId'],
		    entityId: scope.item.entityId
		},function() {
		    load.resolve();
		    modal.show();
		},function(){
		    load.reject();
		});
	    });

            // Garbage collection
            scope.$on('$destroy', function() {
		if (modal) modal.destroy();
		options = null;
		modal = null;
            });
	}
    }
});

mod.directive('legisClientList',function(SearchCustomer,CustomerLabels,FileViewer,Loading,$alert,$filter) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    translations: '=',
	    params: '=',
	    avForm: '='
	},
	templateUrl: 'legisClientList.html',
	link: function(scope,element,attr) {
	    scope.allSelected = false;
	    scope.allClear = true;

	    scope.getCustomer = function(filter) {
		if(typeof(filter)=='string') {
		    if(filter.trim()=='')
			return [];

		    return SearchCustomer.search({
			filter:filter,
			limit: 7,
			totalResults: '0'
		    }).$promise.then(function(result) {
			return result.children.map(function(child) {
			    child.entityId=child.docId;
			    child.header='0';
			    return child;
			});
		    });
		}
	    }

	    scope.$watch('customer',function(newValue,oldValue) {
		if(typeof(newValue)=='object' && newValue.entityId) {
		    // limpa typeahead
		    scope.customer = '';

		    if(!scope.values[scope.field.attrs.fieldName])
			scope.values[scope.field.attrs.fieldName] = [];
		    var values = scope.values[scope.field.attrs.fieldName];
		    
		    for(var i=0; i<values.length; i++) {
			if(values[i].entityId==newValue.entityId) {
			    var error = $alert({
				animation: 'am-slide-top',
				placement: 'top-right',
				content: 'Este cliente já está neste processo',
				template: 'avAlert.html',
				type: 'danger',
				show: true,
				container: 'body',
				duration: 5,
				dismissable: true
			    });
			    return;
			}
		    }
		    values.push(newValue);
		}
	    });


	    scope.remove = function(item) {
		var values = scope.values[scope.field.attrs.fieldName];
		values.splice(values.indexOf(item),1);
	    }
    
	    scope.labels = function() {
		Loading.beginTasks();
		var load = Loading.pushTask();
		Loading.watchTasks();
		var params = [];
		var values = scope.values[scope.field.attrs.fieldName];
		for(var i=0; i<values.length;i++) {
		    if(values[i].checked) {
			params.push({
			    'param': 'customer',
			    'customer': values[i].entityId
			});
		    }
		}

		var doc = CustomerLabels.save({
		    params: params
		}, function() {
		    var fileViewer = FileViewer({
			doc: {
			    docId: doc.docId,
			    contentType: 'application/pdf'
			},
			container: '#file-viewer'
		    });
		    load.resolve();
		}, function() {
		    load.reject();
		});
	    }
	}
    };
});

mod.directive('legisProcessNumbers',function($alert) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    params: '=',
	    translations: '='
	},
	templateUrl: 'legisProcessNumbers.html',
	link: function(scope,element,attr) {
	    scope.number = '';

	    scope.add = function() {
		var number = scope.number.trim();
		if(number == '')
		    return;

		var values = scope.values[scope.field.attrs.fieldName];
		if(values) {
		    if(values.indexOf(number)!=-1) {
			var error = $alert({
			    animation: 'am-slide-top',
			    placement: 'top-right',
			    content: 'Número já existente',
			    template: 'avAlert.html',
			    type: 'danger',
			    show: true,
			    container: 'body',
			    duration: 5,
			    dismissable: true
			});
			return;
		    }
		} else
		    values = scope.values[scope.field.attrs.fieldName] = [];
		values.push(number);
		scope.number = '';
	    }

	    scope.remove = function(number) {
		var values = scope.values[scope.field.attrs.fieldName];
		values.splice(values.indexOf(number),1);
	    }
	}
    };
});

mod.directive('legisProcessLocals',function() {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    avForm: '=',
	    nodeService: '=',
	    params: '=',
	    path: '='
	},
	templateUrl: 'legisProcessLocals.html',
	link: function(scope,element,attr) {
	    var values = scope.values[scope.field.attrs.fieldName];

	    if(values) {
		scope.activePanels = [];
		for(var i=0; i<values.length; i++) {
		    var local = values[i];
		    local.path = scope.values.path;
		    if(local.processLocalId || local.active=='1') {
			scope.activePanels.push(i);
			values[i].active = '1';
		    }
		    else
			values[i].active = '0';
		}
	    }

	    if(!scope.params.register) {
		for(var i=0; i<scope.field.children.length; i++) {
		    var local = scope.field.children[i];
		    for(var j=0; j<local.children.length; j++) {
			local.children[j].attrs.accessMode = 'r';
		    }
		}
	    }

	    scope.checkField = function(local, field) {
		switch(field.attrs.fieldName) {
		case 'uf_codigo':
		case 'cidade_codigo':
		    return local.attrs.cityFlag=='1';
		case 'auxField':
		    return local.attrs.auxFieldFlag=='1';
		default:
		    return true;
		}
	    }
	}
    };
});

mod.directive('legisProcessHistory',function(NodeModal,History,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    params: '='
	},
	templateUrl: 'legisProcessHistory.html',
	link: function(scope,element,attr) {
	    scope.dateFormat = avalancheConfig.dateFormat;
	    var items = scope.field.attrs.items;

	    scope.open = function(item) {
		var params = {
		    path: avalancheConfig.path.history,
		    nodeService: History,
		    formTemplate: 'legisForm.html',
		    title: 'Histórico',
		    buttons: { history: false, remove: true },
		    excludeFields:  ['userId','groupId','userRight','groupRight',
				     'otherRight','creationDate','lastChanged',
				     'lastChangedUserId','keywords'],
		    extraParams: {
			processId: scope.values.docId
		    },
		    onSave: function(node) {
			if(item!=undefined)
			    items[items.indexOf(item)] = node.attrs;
			else
			    items.push(node.attrs);
		    },
		    onBeforeDelete: function() {
			return items.splice(items.indexOf(item),1);
		    },
		    onUndoDelete: function(undoData) {
			items.push(undoData[0]);
		    }
		}
		if(item!=undefined)
		    params.docId = item.docId;
		NodeModal(params);
	    }

	}
    };
});

mod.directive('legisProcessCommitment',function(NodeModal,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '='
	},
	templateUrl: 'legisProcessCommitment.html',
	link: function(scope,element,attr) {
	    scope.dateFormat = avalancheConfig.dateFormat;
	    var items = scope.field.attrs.items;

	    scope.open = function(item) {
		var params = {
		    path: avalancheConfig.path.commitment,
		    title: 'Compromisso',
		    buttons: { history: false, remove: true },
		    excludeFields:  ['userId','groupId','userRight','groupRight',
				     'otherRight','creationDate','lastChanged',
				     'lastChangedUserId','keywords'],
		    extraFields: {
			processId: scope.values.docId
		    },
		    onSave: function(node) {
			if(item!=undefined)
			    items[items.indexOf(item)] = node.attrs;
			else
			    items.push(node.attrs);
		    },
		    onBeforeDelete: function() {
			return items.splice(items.indexOf(item),1);
		    },
		    onUndoDelete: function(undoData) {
			items.push(undoData[0]);
		    }
		}
		if(item!=undefined)
		    params.docId = item.docId;
		NodeModal(params);
	    }
	}
    };
});

mod.directive('legisHistoryFileList',function(FileList) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    translations: '=',
	    avForm: '='
	},
	templateUrl: 'legisHistoryFileList.html',
	link: function(scope,element,attr) {
	    FileList(scope);
	    scope.$watch('values.'+scope.field.attrs.fieldName,function(newValue,oldValue){
 		if(oldValue==newValue || oldValue && newValue && oldValue.length == newValue.length)
		    return;

		if(!newValue || newValue.length==0) {
		    scope.values.defaultDoc = '';
		} else {
		    if(!scope.values.defaultDoc || scope.values.defaultDoc=='') {
			scope.values.defaultDoc = newValue[0]['docId'];
		    } else {
			// search for defaultDoc in the list (newValue)
			var found = false;
			for(var i=0; i<newValue.length && !found; i++) {
			    if(newValue[i]['docId'] == scope.values.defaultDoc)
				found = true;
			}
			if(!found) 
			    scope.values.defaultDoc = newValue[0]['docId'];
		    }
		}
	    },true);
	}
    };
});

mod.directive('legisBody', function() {
    return {
	restrict: 'A',
	link: function(scope,element,attrs) {
	    scope.menuOpen = false;
	    scope.$watch('menuOpen', function(newValue,oldValue) {
		if(newValue)
		    element.addClass('menu-open');
		else
		    element.removeClass('menu-open');
	    });
	}
    }
});

mod.directive('legisSituation',function(Parser,$http,$templateCache,$compile) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    fieldList: '=',
	    dateFormat: '@',
	    avForm: '=',
	    fieldIndex: '=',
	    type: '@'
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);

	    // default element hidden
	    element.addClass('hidden');

	    var situations = {
		retirement: '1',
		death: '2',
		pensionerOf: '4'
	    }
	    scope.$watch('values.situationId', function(newValue,oldValue) {
		if(newValue && newValue.indexOf(situations[scope.field.attrs.fieldName])!=-1)
		    element.removeClass('hidden');
		else
		    element.addClass('hidden');
	    });

	    // carrega template de acordo com tipo
	    var templateUrl;
	    switch(scope.type){
	    case 'date':
		templateUrl = 'avDateField.html';
		break;
	    default:
		templateUrl = 'legisPensionerOf.html';
		break;
	    }
            $http.get(templateUrl, { 
		cache: $templateCache 
	    }).success(function(response) {
		var contents = element.html(response).contents();
		$compile(contents)(scope);
            });

	}
    };
});

mod.directive('legisDagger',function() {
    return {
	restrict: 'E',
	scope: {
	    situation: '='
	},
	link: function(scope,element,attr) {
	    if(scope.situation) {
		for(var i=0; i<scope.situation.length; i++) {
		    if(scope.situation[i].name=='Falecido') {
			element.html('&dagger;');
			return;
		    }
		}
	    }
	}
    }
});

mod.directive('legisCustomerProcessList',function($state) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '='
	},
	templateUrl: 'legisCustomerProcessList.html',
	link: function(scope,element,attr) {
	    scope.$state = $state;
	}
    }
});

mod.directive('legisRepresentativeList', function(NodeModal,Representative,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    params: '='
	},
	templateUrl: 'legisRepresentativeList.html',
	link: function(scope,element,attr) {
	    var items = scope.field.attrs.items;
	    var getListItem = function(node) {
		var item = {
		    docId: node.attrs.docId,
		    entityId: node.attrs.representative.docId,
		    name: node.attrs.representative.name,
		    situationId: node.attrs.representative.situationId
		}
		for(var i=0; (!item.type || node.attrs.kinship && !item.kinship) && i<node.children.length; i++) {
		    switch(node.children[i].attrs.fieldName) {
		    case 'type':
			for(var j=0; !item.type && j<node.children[i].children.length; j++) {
			    if(node.children[i].children[j].attrs.id==node.attrs.type)
				item.type = node.children[i].children[j].attrs.value;
			}
			break;
		    case 'kinship':
			for(var j=0; node.attrs.kinship && !item.kinship && j<node.children[i].children.length; j++) {
			    if(node.children[i].children[j].attrs.id==node.attrs.kinship)
				item.kinship = node.children[i].children[j].attrs.value;
			}
			break;
		    }
		}
		return item;
	    }

	    scope.Editar = function(item) {
		var params = {
		    nodeService: Representative,
		    path: avalancheConfig.path.representative,
		    formTemplate: 'legisForm.html',
		    title: 'Representante do Cliente',
		    buttons: { history: false, remove: true },
		    excludeFields:  ['userId','groupId','userRight','groupRight',
				     'otherRight','creationDate','lastChanged',
				     'lastChangedUserId','keywords'],
		    extraParams: {
			customer: scope.values.docId
		    },
		    onSave: function(node) {
			if(item!=undefined)
			    items[items.indexOf(item)] = getListItem(node);
			else
			    items.push(getListItem(node));
		    },
		    onBeforeDelete: function() {
			return items.splice(items.indexOf(item),1);
		    },
		    onUndoDelete: function(undoData) {
			items.push(undoData[0]);
		    }
		}
		if(item!=undefined) {
		    params.docId = item.docId;
		}
		NodeModal(params);
	    }
	}
    }
});


mod.directive('legisRepresentedList', function(NodeModal,Representative,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '='
	},
	templateUrl: 'legisRepresentedList.html'
    }
});

mod.directive('legisEntityRef', function($state,Parser,Search,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    avForm: '=',
	    values: '=',
	    entityType: '@',
	    tag: '@'
	},
	templateUrl: function(elem,attrs) {
	    return attrs.templateUrl || 'legisEntityRef.html';
	},
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);

	    scope.getEntity = function(filter) {
		if(typeof(filter)=='string') {
		    if(filter.trim()=='')
			return [];
		    return Search.search({
			filter:filter,
			limit: 7,
			totalResults: '0',
			path: avalancheConfig.path[scope.entityType],
			searchCriteria: avalancheConfig.searchCriteria[scope.entityType],
			recursiveSearch: '1'
		    }).$promise.then(function(result) {
			return result.children;
		    });
		}
	    }


	    scope.open = function() {
		$state.go('app.'+(scope.tag?scope.tag:scope.entityType)+'.item',{
		    docId: scope.values[scope.field.attrs.fieldName].docId
		});
	    }

	    scope.$watch('entity',function(newValue,oldValue) {
		if(typeof(newValue)=='object' && newValue.docId) {
		    // limpa typeahead
		    scope.entity = '';
		    scope.values[scope.field.attrs.fieldName] = newValue;		    
		}
	    });

	    scope.remove = function() {
		scope.values[scope.field.attrs.fieldName] = '';
	    }

	}
    };
});

mod.directive('legisLicense', function(Parser,Search,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    avForm: '=',
	    values: '='
	},
	templateUrl: 'legisLicense.html',
	link: function(scope,element,attr) {
	    scope.dateFormat = avalancheConfig.dateFormat;
	    scope.horizontal = Parser.bool(attr.horizontal,true);

	    scope.dateField = {
		attrs: {
		    fieldName: 'enabledDate',
		    accessMode: 'rw'
		}
	    };
	}
    };
});

mod.directive('legisCustomerUserRef', function(Parser,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    avForm: '=',
	    values: '='
	},
	templateUrl: 'legisCustomerUserRef.html',
	controller: function($scope) {
	    $scope.login = {
		attrs: {
		    fieldName: 'login',
		    label: 'Login',
		    accessMode: ''
		}		
	    };
	    
	    $scope.password = {
		attrs: {
		    fieldName: 'password',
		    label: 'Senha',
		}
	    };

	    $scope.status = {
		attrs: {
		    fieldName: 'status',
		    label: 'Conta ativa',
		}
	    };

	    var loginRef;
	    if(avalancheConfig.folderTables[$scope.values.folderTables]=="naturalPersonCustomer") {
		loginRef='cpf';
	    } else {
		loginRef='cnpj';
	    }

	    $scope.$watch('values.'+loginRef, function(newValue,oldValue) {
		$scope.values[$scope.field.attrs.fieldName].login = newValue;
		if(!$scope.values.docId)
		    $scope.password.attrs.required = (newValue && newValue.length>0)?'required':'';
	    });

	    $scope.avForm[$scope.field.attrs.fieldName] = {};
	    $scope.$watch('avForm.password.$invalid',function(newValue,oldValue) {
		$scope.avForm[$scope.field.attrs.fieldName].$invalid = true;
	    });

	},
	link: function(scope,element,attr) {
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	}
    };
});

mod.directive('legisUserRef', function(Parser,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    avForm: '=',
	    values: '=',
	    path: '='
	},
	templateUrl: 'legisUserRef.html',
	controller: function($scope) {

	    // gambiarra para algorítmo do collapsectrl identificar collapse certo
	    $scope.avForm[$scope.field.attrs.fieldName] = {};

	    $scope.$watch('avForm.password.$invalid',function(newValue,oldValue) {
		$scope.avForm[$scope.field.attrs.fieldName].$invalid = true;
	    });

	    $scope.$watch('avForm.groups.$invalid',function(newValue,oldValue) {
		$scope.avForm[$scope.field.attrs.fieldName].$invalid = true;
	    });

	    var authSecret = $scope.values[$scope.field.attrs.fieldName].authSecret;
	    $scope.$watch('values.'+$scope.field.attrs.fieldName+'.groups',function(newValue,oldValue){
		if(newValue && newValue.indexOf('23')==-1) {
		    $scope.values[$scope.field.attrs.fieldName].authSecret = '';
		} else {
		    $scope.values[$scope.field.attrs.fieldName].authSecret = authSecret;
		}
	    },true);

	},
	link: function(scope,element,attr) {
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	}
    };
});

mod.directive('legisRelatedMail',function(SearchMail,$alert,NodeModal,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '='
	},
	templateUrl: 'legisRelatedMail.html',
	link: function(scope,element,attr) {
	    scope.datetimeFormat = avalancheConfig.datetimeFormat;
	    var searchParams = {
		limit: 20,
		offset: 0,
		entityId: scope.values.docId
	    }

	    scope.$watch('search',function(newValue,oldValue) {
		searchParams.offset = 0
		searchParams.filter = newValue;
		SearchMail.search(searchParams, function(result) {
		    scope.field.attrs.itemList = result.children;
		},function(resp) {
		    var error = $alert({
			animation: 'am-slide-top',
			placement: 'top-right',
			content: resp.data.statusMessage,
			template: 'avAlert.html',
			type: 'danger',
			show: true,
			container: 'body',
			duration: 5,
			dismissable: true
		    });
		});
	    });

	    scope.getName =  function(address) {
		if(address.name)
		    return address.name;
		else
		    return address.mailbox+'@'+address.hostname;
	    }

	    scope.to = function(item) {
		var toAddress = ""
		var addressList = item.toAddress.concat(item.ccAddress);
		for(var i=0; i<=2 && i<addressList.length;i++) {
		    if(i>0)
			toAddress += ", ";
		    toAddress += scope.getName(addressList[i]);
		}
		return toAddress;
	    }

	    var getFullAddress =  function(address) {
		var ret = '';
		if(address.name) {
		    ret = address.name + ' ';
		}
		ret += '<'+address.mailbox+'@'+address.hostname+'>';
		return ret;
	    }

	    scope.open = function(item) {
		var params = {
		    docId: item.docId,
		    path: avalancheConfig.path.mail,
		    title: 'Mensagem',
		    buttons: { history: false, remove: true, ok: false },
		    labels: { cancelTag: 'Fechar' },
		    contentTemplate: 'legisRelatedMailModal.html',
		    controller: 'RelatedMailModalCtrl',
		    formTemplate: 'legisForm.html',		    
		    excludeFields:  ['userId','groupId','userRight','groupRight',
				     'otherRight','creationDate','lastChanged',
				     'lastChangedUserId','keywords'],
		    onBeforeDelete: function() {
			var itemList = scope.field.attrs.itemList;
			return itemList.splice(itemList.indexOf(item),1);
		    },
		    onUndoDelete: function(undoData) {
			scope.field.attrs.itemList.push(undoData[0]);
		    },
		    onOpen: function(scope) {
			scope.node.fields.fromAddress.attrs.type = 'text'
			scope.node.fields.fromAddress.attrs.accessMode = 'r'
			scope.node.attrs.fromAddress = getFullAddress(scope.node.attrs.fromAddress);

			if(scope.node.attrs.toAddress.length>0) {
			    scope.node.fields.toAddress.attrs.type = 'text'
			    scope.node.fields.toAddress.attrs.accessMode = 'r'
			    scope.node.attrs.toAddress = scope.node.attrs.toAddress.map(function(item) {
				return getFullAddress(item);
			    }).join(',');
			}

			if(scope.node.attrs.ccAddress.length>0) {
			    scope.node.fields.ccAddress.attrs.type = 'text'
			    scope.node.fields.ccAddress.attrs.accessMode = 'r'
			    scope.node.attrs.ccAddress = scope.node.attrs.ccAddress.map(function(item) {
				return getFullAddress(item);
			    }).join(',');
			} else
			    scope.node.attrs.ccAddress = '';

			scope.node.fields.msgDate.attrs.accessMode='r';
			scope.node.fields.subject.attrs.accessMode='r';
			scope.node.fields.message.attrs.accessMode='r';
		    }
		}

		NodeModal(params);

	    }

	}
    }
});

mod.directive('legisAttachment',function(FileList,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    translations: '=',
	    avForm: '='
	},
	templateUrl: 'legisAttachmentField.html',
	link: function(scope,element,attr) {
	    FileList(scope);
	}
    };
});

mod.directive('legisContractList',function(NodeModal,SelectField,avalancheConfig,$state,$timeout) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    fieldList: '=',
	    values: '='
	},
	templateUrl: 'legisContractList.html',
	link: function(scope,element,attr) {
	    scope.dateFormat = avalancheConfig.dateFormat;
	    var items = scope.field.attrs.items;

	    scope.open = function(item) {
		var params = {
		    path: avalancheConfig.path.contract,
		    title: 'Contrato',
		    formTemplate: 'legisForm.html',
		    buttons: { history: false, remove: true },
		    excludeFields:  ['userId','groupId','userRight','groupRight',
				     'otherRight','creationDate','lastChanged',
				     'lastChangedUserId','keywords'],
		    onSave: function(node) {

			var textList = SelectField.getTextFromChildren(['classId','typeId'],node.children,
								       node.attrs);

			node.attrs.classIdText = textList.classId;
			node.attrs.typeIdText = textList.typeId;

			if(item!=undefined)
			    items[items.indexOf(item)] = node.attrs;
			else
			    items.push(node.attrs);
		    },
		    onBeforeDelete: function() {
			return items.splice(items.indexOf(item),1);
		    },
		    onUndoDelete: function(undoData) {
			items.push(undoData[0]);
		    },
		    onOpen: function(modalScope) {
			var type = avalancheConfig.folderTables[scope.values.folderTables];
			switch(type) {
			case 'naturalPersonProvider':
			case 'legalEntityProvider':
			case 'user':
			    modalScope.node.fields.classId.attrs.accessMode='r';
			    if(modalScope.node.attrs.docId) {
				for(var i=0;i<modalScope.node.attrs.contactList.length; i++) {
				    if(modalScope.node.attrs.contactList[i].docId==scope.values.docId) {
					modalScope.node.attrs.contactList[i].noRemoveBtn = true;
					return;
				    }
				}
				return;
			    }

			    $timeout(function() {
				modalScope.node.attrs.classId = type=='user'?'2':'1';
			    },0,false);
			    modalScope.node.attrs.contactList = [{
				"docId": scope.values.docId,
				"name": scope.values.name,
				"noRemoveBtn": true
			    }];
			    break;

			default:
			    if(modalScope.node.attrs.docId) {
				for(var i=0;i<modalScope.node.attrs.processList.length; i++) {
				    if(modalScope.node.attrs.processList[i].docId==scope.values.docId) {
					modalScope.node.attrs.processList[i].noRemoveBtn = true;
					return;
				    }
				}
				return;
			    }

			    modalScope.node.attrs.processList = [{
				"docId": scope.values.docId,
				"number": scope.values.processNumbers,
				"situation": scope.values.situationId ? 
				    SelectField.getTextFromField(scope.fieldList.situationId,
								 scope.values.situationId):'',
				"subject": scope.values.subjectId ? 
				    SelectField.getTextFromField(scope.fieldList.subjectId,
								 scope.values.subjectId):'',
				"folder": scope.values.folder,
				"folderYear": scope.values.folderYear,
				"rootProcessId": scope.values.rootProcessId,
				"noRemoveBtn": true
			    }];

			    break;
			}
		    }
		}
		if(item!=undefined)
		    params.docId = item.docId;
		destroyModal = NodeModal(params);

	    }

	    var destroyModal = false;
	    scope.$on('$stateChangeStart', function(){
		if(destroyModal)
		    destroyModal();
	    });
	}
    };
});

mod.directive('legisContractProcessList',function($state,$alert,SearchProcess) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '='
	},
	templateUrl: 'legisContractProcessList.html',
	link: function(scope,element,attr) {
	    scope.$state = $state;
	    scope.getProcess = function(filter) {
		if(typeof(filter)=='string') {
		    if(filter.trim()=='')
			return [];

		    return SearchProcess.search({
			filter:filter,
			limit: 7,
			totalResults: '0'
		    }).$promise.then(function(result) {
			return result.children.map(function(child) {
			    child.title = 
				child.number.join(', ') + 
				' ('+child.folder+(child.folderYear?'/'+child.folderYear:'')+')';
			    return child;
			});
		    });
		}
	    }

	    scope.$watch('process',function(newValue,oldValue) {
		if(typeof(newValue)=='object' && newValue.docId) {
		    // limpa typeahead
		    scope.process = '';

		    if(!scope.values[scope.field.attrs.fieldName])
			scope.values[scope.field.attrs.fieldName] = [];
		    var values = scope.values[scope.field.attrs.fieldName];
		    
		    for(var i=0; i<values.length; i++) {
			if(values[i].docId==newValue.docId) {
			    var error = $alert({
				animation: 'am-slide-top',
				placement: 'top-right',
				content: 'Este processo já está incluso neste contrato',
				template: 'avAlert.html',
				type: 'danger',
				show: true,
				container: 'body',
				duration: 5,
				dismissable: true
			    });
			    return;
			}
		    }
		    values.push(newValue);
		}
	    });


	    scope.remove = function(item) {
		var values = scope.values[scope.field.attrs.fieldName];
		values.splice(values.indexOf(item),1);
	    }
	}
    }
});

mod.directive('legisContractEntityList',function($state,$alert,SearchProvider,SearchUser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '='
	},
	templateUrl: 'legisContractEntityList.html',
	link: function(scope,element,attr) {
	    scope.getEntity = function(filter) {
		if(typeof(filter)=='string') {
		    if(filter.trim()=='')
			return [];
		    var Search = (scope.values.classId == 1)?SearchProvider:SearchUser;

		    return Search.search({
			filter:filter,
			limit: 7,
			totalResults: '0'
		    }).$promise.then(function(result) {
			return result.children;
		    });
		}
	    }

	    scope.open = function(item) {
		$state.go((scope.values.classId==1)?
			  'app.provider.item':'app.user.item',
			  { docId: item.docId });
	    }

	    scope.$watch('values.classId',function(newValue,oldValue) {
		if(newValue != oldValue)
		    scope.values[scope.field.attrs.fieldName] = [];
	    });

	    scope.$watch('entity',function(newValue,oldValue) {
		if(typeof(newValue)=='object' && newValue.docId) {
		    // limpa typeahead
		    scope.entity = '';

		    if(!scope.values[scope.field.attrs.fieldName])
			scope.values[scope.field.attrs.fieldName] = [];
		    var values = scope.values[scope.field.attrs.fieldName];
		    
		    for(var i=0; i<values.length; i++) {
			if(values[i].docId==newValue.docId) {
			    var error = $alert({
				animation: 'am-slide-top',
				placement: 'top-right',
				content: 'Este '+
				    (scope.values.classId==1?'fornecedor':'colaborador')+
				    'já está incluso neste contrato',
				template: 'avAlert.html',
				type: 'danger',
				show: true,
				container: 'body',
				duration: 5,
				dismissable: true
			    });
			    return;
			}
		    }
		    values.push(newValue);
		}
	    });


	    scope.remove = function(item) {
		var values = scope.values[scope.field.attrs.fieldName];
		values.splice(values.indexOf(item),1);
	    }
	}
    }
});

mod.directive('legisEntryList',function(SelectField,avalancheConfig,$state) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    fieldList: '=',
	    values: '='
	},
	templateUrl: 'legisEntryList.html',
	link: function(scope,element,attr) {
	    scope.dateFormat = avalancheConfig.dateFormat;

	    scope.open = function(item) {
		$state.go('app.entry.item',{docId:item.docId});
	    }
	}
    };
});

mod.directive('legisEntryForm',function(avalancheConfig) {
    return {
	restrict: "A",
	link: function(scope,element,attr) {

	    var setHiddenFields = function() {
		// exibe campos adequados e ajusta restrições
		scope.node.fields.providerId.attrs.hidden = (scope.node.attrs.classId!=5);

		scope.node.fields.descrId.attrs.hidden = (scope.node.attrs.classId!=5);

		scope.node.fields.processId.attrs.hidden = (!scope.node.attrs.classId || 
							   scope.node.attrs.classId==5);
		scope.node.fields.processId.attrs.required = 
		    scope.node.fields.processId.attrs.hidden?'':'required';

		scope.node.fields.customerId.attrs.hidden = ( scope.node.attrs.classId!=3 && 
							     scope.node.attrs.classId!=6);
		scope.node.fields.customerId.attrs.required = 
		    scope.node.fields.customerId.attrs.hidden?'':'required';

		scope.node.fields.accountability.attrs.hidden = (scope.node.attrs.classId!=4);
		scope.node.fields.succumbing.attrs.hidden = (scope.node.attrs.classId!=1);
		scope.node.fields.discountOfCharter.attrs.hidden = (scope.node.attrs.classId!=3);
	    }

	    scope.$watch('node', function(newValue, oldValue) {
		setHiddenFields();
	    });

	    scope.$watch('node.attrs.classId', function(newValue, oldValue) {
		setHiddenFields();

		if(newValue==oldValue || !oldValue)
		    return;

		// reseta valores
		scope.node.attrs.providerId = '';
		scope.node.attrs.descrId = '';
		scope.node.attrs.processId = '';
		scope.node.attrs.customerId = '';
		scope.node.attrs.accountability = '1';
		scope.node.attrs.succumbing = '';
		scope.node.attrs.discountOfCharter = '0';

	    });
	    scope.$watchGroup(['node','node.attrs.expiration'],function(newValue,oldValue){
		scope.node.fields.paymentDate.attrs.required = scope.node.attrs.expiration?'':'required';
	    });
	}
    };
});

mod.directive('legisSelectTypeahead', function() {
    return {
	restrict: "A",
	link: function(scope,element,attr) {

	    scope.$watch(attr.ngModel,function(newValue,oldValue) {
		if(typeof(newValue)=='object' && newValue.entityId) {
		    // limpa typeahead
		    scope[attr.ngModel] = '';
		    newValue.docId = newValue.entityId;
		    scope.values[scope.field.attrs.fieldName] = newValue;
		    element[0].blur();
		}
	    });	    
	    
	}
    }
});

mod.directive('legisEntryCustomerRef', function($state,Parser,Search,avalancheConfig) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    avForm: '=',
	    values: '='
	},
	templateUrl: 'legisEntryCustomerRef.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);

	    scope.getEntity = function(filter) {
		if(scope.values.processId && 
		   scope.values.processId.clientList) {
		    return scope.values.processId.clientList.filter(function(child){
			return child.excluded != '1'
		    });
		}
	    }

	    scope.open = function() {
		$state.go('app.customer.item',{
		    docId: scope.values[scope.field.attrs.fieldName].docId
		});
	    }

	    scope.remove = function() {
		scope.values[scope.field.attrs.fieldName] = null;
	    }

	}
    };
});

mod.directive('legisEntryProcessRef',function($state,$alert,SearchProcess,Process,Loading) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    avForm: '=',
	    values: '='
	},
	templateUrl: 'legisEntryProcessRef.html',
	link: function(scope,element,attr) {
	    scope.getProcess = function(filter) {
		if(typeof(filter)=='string') {
		    if(filter.trim()=='')
			return [];

		    return SearchProcess.search({
			filter:filter,
			limit: 7,
			totalResults: '0'
		    }).$promise.then(function(result) {
			return result.children.map(function(child) {
			    child.processId=child.docId;
			    child.title = 
				child.number.join(', ') + 
				' ('+child.folder+(child.folderYear?'/'+child.folderYear:'')+')';
			    return child;
			});
		    });
		}
	    }

	    scope.$watch('values[field.attrs.fieldName]',function(newValue,oldValue) {
		if(newValue != oldValue)
		    scope.values.customerId = null;
	    });

	    scope.$watch('process',function(newValue,oldValue) {
		if(typeof(newValue)=='object' && newValue.docId) {
		    // limpa typeahead
		    scope.process = '';

		    if(scope.values.classId==6 || scope.values.classId==3) {
			Loading.beginTasks();
			var load = Loading.pushTask();
			Loading.watchTasks();
			Process.get({ 
			    docId: newValue.docId,
			    fieldName: 'clientList'
			}, function(data) {
			    newValue.clientList = data.attrs.clientList;
			    load.resolve();
			}, function() {
			    load.reject();
			});
		    }

		    scope.values[scope.field.attrs.fieldName] = newValue;		    
		}
	    });


	    scope.remove = function() {
		scope.values[scope.field.attrs.fieldName] = null;
	    }
	}
    }
});

mod.directive('legisDynReqDate',function(Parser) {
    return {
	restrict: "E",
	scope: {
	    field: '=',
	    values: '=',
	    dateFormat: '@',
	    avForm: '=',
	    fieldIndex: '='
	},
	templateUrl: 'legisDynReqDateField.html',
	link: function(scope,element,attr) {
	    scope.horizontal = Parser.bool(attr.horizontal,true);
	    scope.showLabel = Parser.bool(attr.showLabel,true);
	}
    };
});

mod.directive('legisAccCol', function($popover) {
    return {
	restrict: 'A',
	scope: {
	    column: '=',
	    accData: '='
	},
	link: function(scope,element,attrs) {
	    scope.data = {
		values: { 
		    title: ''
		},
		fields: {
		    title: {
			attrs: {
			    fieldName: 'title',
			    label: 'Título do campo',
			    required: 'required'
			},
			children: []
		    }
		}
	    };

	    scope.update = function($hide,popoverForm) {
		if(!popoverForm.$valid) {
		    popoverForm.$setSubmitted();
		    return;
		}

		$hide();
		
		// adiciona nova coluna
		if(!scope.column) {
		    var newCol = {
			title: scope.data.values.title,
			values: []
		    }
		    for(var i=0; i<scope.accData.clientList.length;i++) {
			newCol.values.push(0);
		    }
		    scope.accData.extraCols.push(newCol);
		} else {
		    scope.column.title = scope.data.values.title;
		}
	    }

	    var options = {
		content: attrs.avConfirmPopover,
		template: 'legisAccCol.html',
		autoClose: 1,
		placement: 'bottom',
		prefixEvent: 'popover',
		scope: scope
	    }
	    if(attrs.placement)
		options.placement = attrs.placement;
	    var popover = $popover(element, options);

	    scope.$on('popover.show.before',function(event,elem) {
		if(!scope.column) {
		    scope.data.values.title = '';
		} else {
		    scope.data.values.title = scope.column.title;
		}
	    });

	    scope.$on('popover.show',function(event,elem) {
		var input = elem.$element.find('input');
		if(input.length>0)
		    input[0].select();
	    });

            // Garbage collection
            scope.$on('$destroy', function() {
		if (popover) {
		    popover.destroy();
		}
            });
	    
	}
    }

});

mod.directive('legisGroupingReport', function() {
    return {
	restrict: "A",
	scope: {
	    chartData: '='
	},
	link: function(scope,element,attr) {
	    Highcharts.setOptions({
		lang: { 
		    decimalPoint: ',',
		    thousandsSep: '.'
		}
	    });
	    Highcharts.chart(element[0],{
		chart: {
		    plotBackgroundColor: null,
		    plotBorderWidth: null,
		    plotShadow: false
		},
		credits: { enabled: false },
		title: {
		    text: ''
		},
		tooltip: {
		    pointFormat: '<b>{point.y:.0f} - {point.percentage:.2f} %</b>'
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: false
			    //enabled: true,
			    //color: '#000000',
			    //connectorColor: '#000000',
			    //format: '<small>{point.name}</small><br /><small><b>{y} - {percentage:.2f} %</b></small>'
			},
			showInLegend: true
		    }
		},
		legend: {
		    labelFormat: '{name} ({y} - {percentage:.2f} %)'
		},
		series: [{
		    type: 'pie',
		    data: scope.chartData
		}],
		exporting: {
		    scale: 4
		}
	    });
	}
    };
});



//
// Filters
//

mod.filter('collapseCtrlFilter',function() {
    return function(collapseCtrl,scope) {
	var res = [];
	for(var i=0; i<collapseCtrl.length; i++) {
	    if(!collapseCtrl[i].collapse.condition ||
	       scope.$eval(collapseCtrl[i].collapse.condition)) {
		res.push(collapseCtrl[i]);
	    }
	}
	return res;
    }
});
