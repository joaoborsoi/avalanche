var mod = angular.module('avalanche',['avalanche.ctrls','avalanche.services','avalanche.directives','avalanche.filters']);

mod.constant('UNKNOWN_ERROR',      -1);
mod.constant('INDEX_VIOLATED',     -5);
mod.constant('ALREADY_EXISTS',     -5);
mod.constant('FAILED',             1000);
mod.constant('FATAL_ERROR',        1001);
mod.constant('STATIC_PROP',        1002);
mod.constant('READ_ONLY_PROP',     1003);
mod.constant('FIELD_REQUIRED',     1004);
mod.constant('INVALID_FIELD',      1005);
mod.constant('NOT_FOUND',          1006);
mod.constant('WARNING',            1007);
mod.constant('PERMISSION_DENIED',  2001);
mod.constant('INVALID_PATH',       2002);
mod.constant('FOLDER_NOT_EMPTY',   2003);
mod.constant('MAINTENANCE',        2004);
mod.constant('INVALID_RET_FIELD', 3001);
