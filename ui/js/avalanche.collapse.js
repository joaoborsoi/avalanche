'use strict';

angular.module('avalanche.collapse', [])

  .provider('$avCollapse', function() {

    var defaults = this.defaults = {
      animation: 'am-collapse',
      disallowToggle: false,
      activeClass: 'in',
      startCollapsed: false,
      allowMultiple: false
    };

    var controller = this.controller = function($scope, $element, $attrs) {
      var self = this;

      // Attributes options
      self.$options = angular.copy(defaults);
      angular.forEach(['animation', 'disallowToggle', 'activeClass', 'startCollapsed', 'allowMultiple'], function (key) {
        if(angular.isDefined($attrs[key])) self.$options[key] = $attrs[key];
      });

      // use string regex match boolean attr falsy values, leave truthy values be
      var falseValueRegExp = /^(false|0|)$/i;
      angular.forEach(['disallowToggle', 'startCollapsed', 'allowMultiple'], function(key) {
        if(angular.isDefined($attrs[key]) && falseValueRegExp.test($attrs[key]))
          self.$options[key] = false;
      });

      self.$toggles = [];
      self.$targets = [];

      // Please use $activePaneChangeListeners if you use `avActivePanels`
      // Because we removed `ngModel` as default, we rename viewChangeListeners to
      // activePaneChangeListeners to make more sense.
      self.$activePaneChangeListeners = self.$viewChangeListeners = [];

      self.$registerToggle = function(element) {
        self.$toggles.push(element);
      };
      self.$registerTarget = function(element) {
        self.$targets.push(element);
      };

      self.$unregisterToggle = function(element) {
        var index = self.$toggles.indexOf(element);
        // remove toggle from $toggles array
        self.$toggles.splice(index, 1);
      };
      self.$unregisterTarget = function(element) {
        var index = self.$targets.indexOf(element);

        // remove element from $targets array
        self.$targets.splice(index, 1);

        if (self.$options.allowMultiple) {
          // remove target index from $active array values
          deactivateItem(element);
        }

        // fix active item indexes
        fixActiveItemIndexes(index);

        self.$activePaneChangeListeners.forEach(function(fn) {
          fn();
        });
      };

      // use array to store all the currently open panels
      self.$targets.$active = !self.$options.startCollapsed ? [0] : [];
      self.$setActive = $scope.$setActive = function(value) {
        if(angular.isArray(value)) {
          self.$targets.$active = value;
        }
        else if(!self.$options.disallowToggle) {
          // toogle element active status
          isActive(value) ? deactivateItem(value) : activateItem(value);
        } else {
          activateItem(value);
        }

        self.$activePaneChangeListeners.forEach(function(fn) {
          fn();
        });
      };

      self.$activeIndexes = function() {
        return self.$options.allowMultiple ? self.$targets.$active :
          self.$targets.$active.length === 1 ? self.$targets.$active[0] : -1;
      };

      function fixActiveItemIndexes(index) {
        // item with index was removed, so we
        // need to adjust other items index values
        var activeIndexes = self.$targets.$active;
        for(var i = 0; i < activeIndexes.length; i++) {
          if (index < activeIndexes[i]) {
            activeIndexes[i] = activeIndexes[i] - 1;
          }

          // the last item is active, so we need to
          // adjust its index
          if (activeIndexes[i] === self.$targets.length) {
            activeIndexes[i] = self.$targets.length - 1;
          }
        }
      }

      function isActive(value) {
        var activeItems = self.$targets.$active;
        return activeItems.indexOf(value) === -1 ? false : true;
      }

      function deactivateItem(value) {
        var index = self.$targets.$active.indexOf(value);
        if (index !== -1) {
          self.$targets.$active.splice(index, 1);
        }
      }

      function activateItem(value) {
        if (!self.$options.allowMultiple) {
          // remove current selected item
          self.$targets.$active.splice(0, 1);
        }

        if (self.$targets.$active.indexOf(value) === -1) {
          self.$targets.$active.push(value);
        }
      }

    };

    this.$get = function() {
      var $avCollapse = {};
      $avCollapse.defaults = defaults;
      $avCollapse.controller = controller;
      return $avCollapse;
    };

  })

    .directive('avCollapse', function($window, $animate, $avCollapse, $parse) {

    var defaults = $avCollapse.defaults;

    return {
      require: ['?ngModel', 'avCollapse'],
      controller: ['$scope', '$element', '$attrs', $avCollapse.controller],
      link: function postLink(scope, element, attrs, controllers) {

        var ngModelCtrl = controllers[0];
        var avCollapseCtrl = controllers[1];

        if(ngModelCtrl) {

          // Update the modelValue following
          avCollapseCtrl.$activePaneChangeListeners.push(function() {
            ngModelCtrl.$setViewValue(avCollapseCtrl.$activeIndexes());
          });

          // modelValue -> $formatters -> viewValue
          ngModelCtrl.$formatters.push(function(modelValue) {
            // console.warn('$formatter("%s"): modelValue=%o (%o)', element.attr('ng-model'), modelValue, typeof modelValue);
            if (angular.isArray(modelValue)) {
              // model value is an array, so just replace
              // the active items directly
              avCollapseCtrl.$setActive(modelValue);
            }
            else {
              var activeIndexes = avCollapseCtrl.$activeIndexes();

              if (angular.isArray(activeIndexes)) {
                // we have an array of selected indexes
                if (activeIndexes.indexOf(modelValue * 1) === -1) {
                  // item with modelValue index is not active
                  avCollapseCtrl.$setActive(modelValue * 1);
                }
              }
              else if (activeIndexes !== modelValue * 1) {
                avCollapseCtrl.$setActive(modelValue * 1);
              }
            }
            return modelValue;
          });

        }

        if (attrs.avActivePanels) {
          // adapted from angularjs ngModelController bindings
          // https://github.com/angular/angular.js/blob/v1.3.1/src%2Fng%2Fdirective%2Finput.js#L1730
          var parsedAvActivePane = $parse(attrs.avActivePanels);

          // Update avActivePanels value with change
          avCollapseCtrl.$activePaneChangeListeners.push(function() {
            parsedAvActivePane.assign(scope, avCollapseCtrl.$activeIndexes());
          });

          // watch avActivePanels for value changes
          scope.$watch(attrs.avActivePanels, function(newValue, oldValue) {
            if (angular.isArray(newValue)) {
              // model value is an array, so just replace
              // the active items directly
              avCollapseCtrl.$setActive(newValue);
            }
            else {
              var activeIndexes = avCollapseCtrl.$activeIndexes();

              if (angular.isArray(activeIndexes)) {
                // we have an array of selected indexes
                if (activeIndexes.indexOf(newValue * 1) === -1) {
                  // item with newValue index is not active
                  avCollapseCtrl.$setActive(newValue * 1);
                }
              }
              else if (activeIndexes !== newValue * 1) {
                avCollapseCtrl.$setActive(newValue * 1);
              }
            }
          }, true);
        }
      }
    };

  })

  .directive('avCollapseToggle', function() {

    return {
      require: ['^?ngModel', '^avCollapse'],
      link: function postLink(scope, element, attrs, controllers) {

        var ngModelCtrl = controllers[0];
        var avCollapseCtrl = controllers[1];

        // Add base attr
        element.attr('data-toggle', 'collapse');

        // Push pane to parent avCollapse controller
        avCollapseCtrl.$registerToggle(element);

        // remove toggle from collapse controller when toggle is destroyed
        scope.$on('$destroy', function() {
          avCollapseCtrl.$unregisterToggle(element);
        });

        element.on('click', function() {
          var index = attrs.avCollapseToggle || avCollapseCtrl.$toggles.indexOf(element);
          avCollapseCtrl.$setActive(index * 1);
          scope.$apply();
        });

      }
    };

  })

  .directive('avCollapseTarget', function($animate) {

    return {
      require: ['^?ngModel', '^avCollapse'],
      // scope: true,
      link: function postLink(scope, element, attrs, controllers) {

        var ngModelCtrl = controllers[0];
        var avCollapseCtrl = controllers[1];

        // Add base class
        element.addClass('collapse');

        // Add animation class
        if(avCollapseCtrl.$options.animation) {
          element.addClass(avCollapseCtrl.$options.animation);
        }

        // Push pane to parent avCollapse controller
        avCollapseCtrl.$registerTarget(element);

        // remove pane target from collapse controller when target is destroyed
        scope.$on('$destroy', function() {
          avCollapseCtrl.$unregisterTarget(element);
        });

        function render() {
          var index = avCollapseCtrl.$targets.indexOf(element);
          var active = avCollapseCtrl.$activeIndexes();
          var action = 'removeClass';
          if (angular.isArray(active)) {
            if (active.indexOf(index) !== -1) {
              action = 'addClass';
            }
          }
          else if (index === active) {
            action = 'addClass';
          }

          $animate[action](element, avCollapseCtrl.$options.activeClass);
        }

        avCollapseCtrl.$activePaneChangeListeners.push(function() {
          render();
        });
        render();

      }
    };

  });
