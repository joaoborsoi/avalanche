#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include<string>
#include<mysql.h>

using namespace std;

int main(int argc, char **argv) {
  
  string query;

  MYSQL db;
  MYSQL_ROW row;
  MYSQL_RES *result;

  mysql_init(&db);
  if(! mysql_real_connect(&db, argv[1], argv[2], argv[3], argv[4], 0, NULL, 0))
      printf("Erro de conexao com o Banco de Dados");
 
  query = "SELECT MIN(lastRefresh) FROM CHAT_RoomMember";
  
  if(mysql_real_query(&db, query.c_str(), query.length()) != 0)
     printf("Erro de Banco de Dados => SELECT");    
  
  if((result = mysql_store_result(&db)) != NULL) 
  {     
      row = mysql_fetch_row(result);
      if (row[0] != NULL)
      {
          query = string("DELETE FROM CHAT_Buffer WHERE date < ") + row[0];

         if(mysql_real_query(&db, query.c_str(), query.length()) != 0)
            printf("Erro de Banco de Dados => DELETE");
      }
   }
      
   mysql_free_result(result);

   return 1;
}
