function SetReadOnly(id)
{
    var field = document.getElementById(id);
    if(field)
    {
	field.setAttribute('readOnly','readOnly');
	field.className += ' disabled';
	return true;
    }
    return false;
}

function SetDisabled(id)
{
    var field = document.getElementById(id);
    if(field)
    {
	field.setAttribute('disabled','disabled');
	field.className += ' disabled';
	return true;
    }
    return false;
}

function SetEnabled(id)
{
    var field = document.getElementById(id);
    if(field)
    {
	field.removeAttribute('disabled');
	field.className = field.className.replace(/disabled/g,"");
	return true;
    }
    return false;
}
