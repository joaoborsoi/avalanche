function CalculateTotal()
{
    var quant;
    var total = 0;
    var curr = 'R$';

    var i = 1;
    while((quant = $('item_quant_'+i)) != null)
    {
	// calcula valores
	var value = $('item_valor_unit_'+i).value;

	// campo do valor
	$('value_'+i).set('text',curr+FloatToStr(value/100));
	$('item_valor_'+i).value = value;

	// subtotal
	value = value*quant.value;
	$('subtotal_'+i).set('text',curr+FloatToStr(value/100));

	// computes total
	total += value;

	i++;
    }

    $('total').set('text',curr+FloatToStr(total/100));
}

function OnCartItemChange(id)
{
    CalculateTotal();

    // envia alteração de quantitade
    var body = $(document.body).addClass('ajax-loading');
    var log = $('log_res').addClass('ajax-loading');
    var submit = $('submitBtn').addClass('disabled');;
    submit.setProperty('disabled','disabled');
    
    var request = new Request({url:'shopSetQuantity.av',
			       evalResponse: true,
			       onComplete: function(response)
			       {
				   //alert(response);
				   body.removeClass('ajax-loading');
				   log.removeClass('ajax-loading');
				   submit.removeClass('disabled');
				   submit.removeProperty('disabled');
			       }
	});

    request.send('docId='+$('item_id_'+id).value+
		 '&num='+$('item_quant_'+id).value);
}

function OnRemoveFromCartClick(id)
{
    if(confirm(confirmDelMsg))
	window.location = 'shopCart.av?action=removeFromCart&docId='+id;
}

var confirmDelMsg;

function OnCartFormLoad(_confirmDelMsg)
{
    confirmDelMsg = _confirmDelMsg;
    $('cartFormObj').addEvent('submit',OnCartFormSubmit);
    CalculateTotal();
}

function SetError(id,msg)
{
    $('row_'+id).addClass('error');
    $('log_res').set('text',msg);
}

function UnsetError(id)
{
    $('row_'+id).removeClass('error');
    var errorList = $('products').getElements('tr[class*=error]');
    if(errorList.length == 0)
	$('log_res').empty();
}

function OnCartFormSubmit(e)
{
    // prevent the normal submit event
    e.stop();

    var errorList = $('products').getElements('tr[class*=error]');
    if(errorList.length > 0)
    {
	alert($('log_res').get('text'));
	return;
    }

    // sends checkout notify
    var body = $(document.body).addClass('ajax-loading');
    var log = $('log_res').addClass('ajax-loading');
    var submit = $('submitBtn').addClass('disabled');;
    submit.setProperty('disabled','disabled');
    
    var request = new Request({url:'shopCheckout.av',
			       evalResponse: true,
			       onComplete: function(response)
			       {
				   //alert(response);
				   body.removeClass('ajax-loading');
				   log.removeClass('ajax-loading');
				   submit.removeClass('disabled');
				   submit.removeProperty('disabled');
			       }
	});
    request.send();
}


function SubmitPagseguro()
{
    var form = $('cartFormObj');
    form.setProperty('action', 
	  'https://pagseguro.uol.com.br/security/webpagamentos/webpagto.aspx');
    form.set('accept-charset','iso-8859-1');
    form.submit();
}
