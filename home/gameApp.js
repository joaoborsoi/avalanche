gameApp = new Object();

gameApp.OnGameAppFormLoad = function()
{
    gameApp.status = $('status').get('value');

    $('popupForm').addEvent('submit', function() {

	// operações com status
	switch(gameApp.status)
	{
	default:
	case '1': // solicitado
	case '2': // agendado
	case '3': // notificado
	    // agendado e notificado vira solicitado
	    if($('status').get('value')!=11)
		$('status').set('value','1');
	    break;

	case '4': // em execução
	case '5': // finalizado	    
	case '6': // cancelado
	case '7': // iniciado
	case '8': // registrado
	case '9': // parado
	case '10': // parado
	case '11': // parado
	    // não podem ser alterados
	    $('status').set('value',gameApp.status);
	    break;
	}



	// searches first date
	var numOfOptions = parseInt($('schedule_numOfOptions').value);
	var firstDate = null;
	var firstDateText = "";
	for(var i = 1; i <= numOfOptions; i++)
	{
	    if($('schedule_'+i+'__start'))
	    {
		var year = $('schedule_'+i+'__startYear').get('value');
		var month = $('schedule_'+i+'__startMonth').get('value');
		var day = $('schedule_'+i+'__startDay').get('value');
		while(year.length<4)
		    year = '0'+year;
		while(month.length<2)
		    month='0'+month;
		while(day.length<2)
		    day='0'+day;
		var date = year+month+day;

		if(firstDate==null || date < firstDate)
		{
		    firstDate = date;
		    firstDateText = day+'/'+month+'/'+year;
		}
	    }
	}
	
	$('title').set('value',
		       $('placeId').getSelected().get('text')+' - '+
		       firstDateText+' - '+
		       $('responsible').get('value'));
	
    });

}

