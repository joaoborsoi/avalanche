game.labels = {
    logoutConfirmMsg: 'Do you want to exit?',
    updateConfirmMsg: "All plays will be lost. Are you sure?",
    restartConfirmMsg: "Restart game? You will loose every turn until now.",
    projectConfirmMsg: "Do you want to activate this project?",
    chartThousand: "thousand",
    chartMillions: "millions",
    chartBillions: "billions",
    of: "of ",
    currency: "$",
    months: "Months",
    thousandSep: ',',
    decimalSep: '.'
};
