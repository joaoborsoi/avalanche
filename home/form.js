var confirmCloseMsg;

//
// General funcions
//
function SetConfirmCloseMsg(msg)
{
    confirmCloseMsg = msg;
}

function OnChange()
{
    if(window.onbeforeunload || !$defined(confirmCloseMsg))
	return;
    window.onbeforeunload = function (event)
    {
	if(!event) event = window.event;
	event.returnValue = confirmCloseMsg;
	return event.returnValue;
    }
}

function OnSave()
{
    window.onbeforeunload = null;
}

function OnComposedFieldKeyUp(fieldName,fieldId,length)
{
    if($(fieldName+'_'+fieldId).value.length == length) 
	$(fieldName+'_'+(fieldId+1)).focus();
}

function OnComposedFieldChange(fieldName,numOfFields)
{
    var field = $(fieldName);
    field.value = "";
    for(var i=0;i<numOfFields;i++)
	field.value += $(fieldName+'_'+(i+1)).value;
    OnChange();
}

function SetReadOnly(id)
{
    var field = $(id);
    if(field)
    {
	field.set('readOnly',true);
	field.addClass('disabled');
    }
}

function SetDisabled(id)
{
    var field = $(id);
    if(field)
    {
	field.set('disabled',true);
	field.addClass('disabled');
    }
}

function SetEnabled(id)
{
    var field = $(id);
    if(field)
    {
	field.set('disabled',false);
	field.removeClass('disabled');
    }
}

//
// Entity reference fields 
//
function InitEntityRef(id, value)
{
    var search = (value=='');

    if(search)
    {
	document.getElementById(id+'_search').style.display='block';
	var changeSelection = document.getElementById(id+'_changeSelection');
	if(changeSelection)
	    changeSelection.style.display='block';
    }
    else
	document.getElementById(id+'_selected').style.display='block';
}

function UpdateEntityRef(entityId, id, title, fieldName, histEntityType)
{
    document.getElementById(fieldName).value = entityId;
    document.getElementById(fieldName).setAttribute('title',id);
    document.getElementById(fieldName+'_title').value = title;
    histEntityType = histEntityType;
    document.getElementById(fieldName + '_search').style.display='none';
    document.getElementById(fieldName + '_selected').style.display='block';
    OnChange();
}

function UnselectEntityRef(fieldName)
{
    document.getElementById(fieldName).value='';
    document.getElementById(fieldName + '_selected').style.display='none';
    document.getElementById(fieldName + '_search').style.display='block';
    OnChange();
}

function InitLEORNPEntityRef(id, value, entityRefType)
{
    InitEntityRef(id, value);
    if(entityRefType=='naturalPerson')
	document.getElementById(id+'_entityRefType1').checked = true;
}


//
// Text area
//
function OnTextAreaKeyPress(field,size)
{
    if(field.value.length >= size)
	field.value = field.value.substring(0,size);
}

//
// Date field
//
function OnDateFieldChange(fieldName)
{
    var field = $(fieldName);
    field.value = 
	$(fieldName+'Year').value+'-'+
	$(fieldName+'Month').value+'-'+
	$(fieldName+'Day').value;
    if(field.value=='--')
	field.value = '';
    OnChange();
}

function OnDateTimeFieldChange(fieldName)
{
    var field = $(fieldName);
    var second = $(fieldName+'Second');
    field.value = 
	$(fieldName+'Year').value+'-'+
	$(fieldName+'Month').value+'-'+
	$(fieldName+'Day').value+' '+
	$(fieldName+'Hour').value+':'+
	$(fieldName+'Minute').value+':'+
	((second)?second.value:'0');
    if(field.value=='-- ::')
	field.value = '';	
    OnChange();
}

function CreateImageList(fieldName,path,maxImages)
{
    eval(fieldName+'=new Object();');
    eval(fieldName+'.selected=0;');
    eval(fieldName+'.path="'+path+'";');
    eval(fieldName+'.maxImages="'+maxImages+'";');
    eval(fieldName+'.numImages=0;');
    MakeDragUpload(fieldName+'Block',path,
		   function OnUploadImages(event)
		   {
		       if (event.target.readyState != 4)
			   return;
		       /* If we got an error display it. */
		       //     alert(event.target.responseText);
		       result = eval("(" + event.target.responseText + ")");			
		       if(result.status != 0)
		       {
			   alert(result.message);
			   return;
		       }

		       for(var i=0; i<result.numFiles; i++)
			   AddImage(fieldName, 
				    result.docList[i].docId,
				    result.docList[i].title,
				    result.docList[i].type);
		       
		   },maxImages);
}

function AddImage(fieldName, imageId, title, contentType)
{
    var imageListBlockMsg = $(fieldName+'BlockMsg');
    if(imageListBlockMsg)
	imageListBlockMsg.style.display='none';

    var field = $(fieldName+'Block');
    var numOfOptions = $(fieldName+'_numOfOptions');
    var idx = parseInt(numOfOptions.value) + 1;
    var numImages = eval(fieldName+'.numImages') + 1;
    var maxImages = eval(fieldName+'.maxImages');
    if(maxImages>0)
    {
	if(numImages == maxImages)
	    SetDisabled(fieldName+'_insertImageBtn');
	else if (numImages > maxImages)
	    return;
    }
    eval(fieldName+'.numImages++');
    var ul;
    if(idx == 1)
    {
	ul = new Element("ul",{"id":fieldName});
	ul.inject(field);
    }
    else
	ul = $(fieldName);

    var li = new Element("li",{"id": fieldName+"_"+imageId});
    li.onclick=function(){SelectImage(fieldName,imageId);}

    li.inject(ul);

    var input = new Element("input",{
	    "type":"hidden",
	    "name": fieldName+"_"+idx+"__value",
	    "value": imageId
	});

    input.inject(li);

    var span = CreateImageNode(fieldName,imageId,title,contentType,true);
    span.inject(li);

    numOfOptions.value = idx;

    MakeDraggable(fieldName);
}

function CreateImageNode(fieldName,imageId,title,contentType,disableCache)
{
    var span = new Element("span",{"id":fieldName+"_"+imageId+"_span"});

    contentType = contentType.split("/");
    switch(contentType[0])
    {
    case 'image':
	var imgSrc = "adminGetFileContent.av?";
	if(contentType[1]!='svg+xml')
	    imgSrc += "size=100&";
	imgSrc += "fieldName=content&docId="+imageId;
	if(disableCache)
	{
	    tmp = new Date(); 
	    imgSrc += "&"+tmp.getTime();
	}
	var img = new Element("img",{
                "id": fieldName+"_"+imageId+"_img",
		"src":imgSrc,
		"alt":$defined(title)?title:imageId
				      });
	if(contentType[1]=='svg+xml')
	    img.set("width",100);
	img.inject(span);

	if($defined(title))
	{
	    var small = new Element("small",{
		    "id": fieldName+"_"+imageId+"_small",
		    'html':title
					    });
	    small.inject(span);
	}

	break;

    default:
	// <label>
	var type = new Element("label",{
		"class":"imageItem "+contentType[1].replace(/\./g,"-")
		});
	type.inject(span);

	// <a>
	var href = "adminGetFileContent.av";
	href += "?fieldName=content&docId="+imageId;
	if(!$defined(title))
	    title = contentType[1];
	var anchor = new Element("a",{
		"id": fieldName+"_"+imageId+"_a",
		"href":href,
                "html":title
		 });

	anchor.inject(span);
	break;
    }
    return span;
}

function UpdateImage(fieldName,imageId,title,contentType)
{
    if(!contentType)
    {
	var img = $(fieldName+"_"+imageId+"_img");
	if(img)
	    img.set("alt",title);
	var small = $(fieldName+"_"+imageId+"_small");
	if(small)
	    small.set("html",title);
	var a = $(fieldName+"_"+imageId+"_a");
	if(a)
	    a.set("html",title);
    }
    else
    {
	var span = CreateImageNode(fieldName,imageId,title,contentType,true);
	span.replaces($(fieldName+"_"+imageId+"_span"));
    }
}

/* Mootools */
function MakeDraggable(fieldName)
{
   if($(fieldName)){
	new Sortables($(fieldName), {
		constrain: false,
		clone: true,
		revert: false,
		cloneOpacity: 0.3,
		elementOpacity: 1,
		onComplete:function()
		{
		    var ids=this.serialize(false, 
					   function(element, index)
					   {
					       var inp=element.getElementsByTagName('input');
					       if(inp.length>0)
						   inp[0].setAttribute('name',
							   fieldName+'_'+(index+1)+'__value');
					   });
			//alert(ids);
		}
	});
   }	
}

function EmbedImageUploadForm(fieldName)
{
    var path='wikiImageUploadForm.av?fieldName='+fieldName;
    OpenWin(path,'imageUploadForm',500,300);
}

function SelectImage(fieldName,imageId)
{
    $(fieldName+"_"+imageId).addClass('selected');
    if(eval(fieldName+'.selected && '+fieldName+'.selected!='+imageId))
    {
	var aux = "$('"+fieldName+"_'+"+fieldName+".selected)";
	aux += ".removeClass('selected')";
	eval(aux);
    }
    eval(fieldName+'.selected='+imageId);
 }

function RemoveImage(fieldName)
{
    if(eval(fieldName+'.selected'))
    {
	var aux = "$('"+fieldName+"')";
	aux += ".removeChild($('";
	aux += fieldName+"_'+"+fieldName+".selected));";
	eval(aux);
	var imageListBlockMsg = $(fieldName+'BlockMsg');
	if(imageListBlockMsg)
	{
	    var imgList=$(fieldName).getElementsByTagName('li');
	    if(imgList.length == 0)
		imageListBlockMsg.style.display='block';
	}
	eval(fieldName+'.selected=0;');
	var maxImages = eval(fieldName+'.maxImages');
	var numImages = eval('--'+fieldName+'.numImages');
	if(numImages==maxImages - 1)
	    SetEnabled(fieldName+'_insertImageBtn');
    }
}


function InitUsageTerms(fieldName, value)
{
    if(value!='')
	$(fieldName+'Checkbox').checked = true;
}

function OnUsageTermsChange(fieldName)
{
    if($(fieldName+'Checkbox').checked)
	$(fieldName).value = $(fieldName+'DocId').value;
    else
	$(fieldName).value = '';
}

//
// Float fields
//
var fracDigits,decimalSep,thousandSep,locFracDigits,locDecimalSep,
    locThousandSep;
function SetLocaleConv(fracDigits, decimalSep, thousandSep)
{
    locFracDigits = fracDigits;
    locDecimalSep = decimalSep;
    locThousandSep = thousandSep;
}

function GetFracDigits(fracDigits)
{
    if(typeof(fracDigits)!='undefined')
	return fracDigits;
    if(typeof(locFracDigits)!='undefined')
	return locFracDigits;
    return 2;
}

function GetDecimalSep(decimalSep)
{
    if(typeof(decimalSep)!='undefiened')
	return decimalSep;
    if(typeof(locDecimalSep)!='undefined')
	return locDecimalSep;
    return '.';
}

function GetThousandSep(thousandSep)
{
    if(typeof(thousandSep)!='undefined')
	return thousandSep;
    if(typeof(locThousandSep)!='undefined')
	return locThousandSep;
    return ',';
}

function FloatToStr(num, fracDigits, decimalSep, thousandSep)
{
    fracDigits = GetFracDigits(fracDigits);
    decimalSep = GetDecimalSep(decimalSep);
    thousandSep = GetThousandSep(thousandSep);
	       
    if(!isFinite(num))
	num = 0;
    var result = '';
    var numInt;
    var numDec;
    var prefix = '';
    if(fracDigits > 0)
    {
        numInt = parseInt(num).toString();
	if(numInt==0 && numInt>num)
	    prefix='-';
	numDec = Math.round(Math.abs(num-numInt)*Math.pow(10,fracDigits)).toString();
 	while(numDec.length < fracDigits)
 	    numDec = '0' + numDec; 

	if(parseInt(numDec).toString().length > fracDigits)
        {
 	    numDec = numDec.substr(1);
	    numInt = parseInt(numInt)+1;
	    numInt = numInt.toString();
        }
    }
    else
        numInt = Math.round(num).toString();
    var numChar = 0;
    for(var i=numInt.length-1; i >= 0;i--)
    {
	if(numChar>0 && numChar%3 == 0 && !(num<0 && i==0))
	    result = thousandSep + result;
	result = numInt.charAt(i) + result;
	numChar++;
    }
    if(fracDigits > 0)
	result = prefix + result + decimalSep + numDec;
    return result;
}

function StrToFloat(num, decimalSep, thousandSep)
{
    decimalSep = GetDecimalSep(decimalSep);
    thousandSep = GetThousandSep(thousandSep);

    if(typeof num != 'string')
	return num;
    if(thousandSep == '.')
	num = num.replace(/\./g, '');
    else
	num = num.replace(RegExp(thousandSep, 'g'), '')
    if(decimalSep != '.')
	num = num.replace(decimalSep, '.');
    num = parseFloat(num);
    if(isNaN(num))
	return (parseFloat(0));
    return num;
}

function OnFloatFieldChange(field)
{
    field.value=FloatToStr(StrToFloat(field.value, decimalSep, thousandSep), 
			   fracDigits, decimalSep, thousandSep);
    OnChange();
    EvalFormulas();
}


//
// Integer fields
//
function OnIntegerFieldChange()
{
    OnChange();
    EvalFormulas();
}



//
// Select fields
//
var loadFieldPage;
function SetLoadFieldPage(page)
{
    loadFieldPage = page;
}

function OnSelectFieldChange(value,targetField,extraFields)
{
    OnChange();
    if(targetField != '')
    {
	var body = $(document.body).addClass('ajax-loading');
	var request = new Request({url:loadFieldPage,
  				   evalResponse: true,
			           onComplete: function(response)
			           {
				       body.removeClass('ajax-loading');
				       // $('log_res').set('text',response);

				   }});
	var data = 'fieldName='+targetField+'&sourceId=' + value;
	if($defined(extraFields))
	    data += '&'+extraFields;
	request.send(data);
    }
}

function ResetSelectField(fieldName,targetField)
{
    $(fieldName).length=1;
    OnSelectFieldChange(0,targetField);
    
}

function AddSelectOption(fieldName,value,text)
{
    var option  = new Element('option', {value: value,text: text});
    option.inject($(fieldName));
}

//
// Formula fields
//
var expList;
function InitFormulas()
{
    expList = new Array();
}

function AddFormula(expression)
{
    var idx = expList.length;
    expList[idx] = expression;
    return idx;
}

function InsertFormula(expression,idx)
{
    expList.splice(idx,0,expression);
}

function EvalFormulas()
{
    if(!$defined(expList))
	return;
    for(var i=0; i<expList.length;i++)
	eval(expList[i]);
}

function SetNodeValue(id, expression)
{
    eval('window.'+id+'='+expression);
    if(!isFinite(eval('window.'+id)))
	eval ('window.'+id+'=0');
    var value = FloatToStr(eval('window.'+id), fracDigits, decimalSep,
			   thousandSep);

    var node = document.getElementById(id);
    if(!node)
	return;

    switch(node.nodeName)
    {
    case 'TD':
	if(node.firstChild)
	    node.firstChild.nodeValue = value;
	else
	    node.appendChild(document.createTextNode(value));
	break;
    case 'INPUT':
	node.value = value;
	break;
    }
}

function GetNodeValue(id)
{
    if(eval('typeof window.'+ id) != 'undefined' &&
       eval('typeof window.'+ id) != 'object')
	return eval('window.' + id);

    var node = document.getElementById(id);
    if(!node)
    {
	alert(id + ' not found!');
	return '';
    }

    switch(node.nodeName)
    {
    case 'TD':
	if(node.firstChild) 
	    return node.firstChild.nodeValue;
	break;
    case 'INPUT':
	return node.value;
    }
}


//
// checkbox
//
function InitCheckbox(fieldName, value)
{
    if(value=='1')
	$(fieldName+'Checkbox').set('checked',true);
}

function OnCheckboxChange(fieldName)
{
    if($(fieldName+'Checkbox').checked)
	$(fieldName).value = '1';
    else
	$(fieldName).value = '0';
    OnChange();
}

//
// cep
//
function OnCepChange(fieldName)
{
    OnComposedFieldChange(fieldName,2);
    var cep = $(fieldName).value;
    if(cep.length>=8)
    {
	var body = $(document.body).addClass('ajax-loading');
	DisableAddress();
	var request = new Request({url:'adminCepLoadAddress.av',
  				   evalResponse: true,
			           onComplete: function(response)
			           {
				       body.removeClass('ajax-loading');
				       //$('log_res').set('text',response);

				   }});
	request.send('cep=' + cep);
    }
}

function DisableAddress()
{
    SetDisabled('uf_codigo');
    SetDisabled('cidade_codigo');
    SetDisabled('bairro');
    SetDisabled('logradouro');
}

function EnableAddress()
{
    SetEnabled('uf_codigo');
    SetEnabled('cidade_codigo');
    SetEnabled('bairro');
    SetEnabled('logradouro');
}

function LoadAddress(uf_codigo,cidade_codigo,bairro,logradouro)
{
    // ajusta estado
    var uf_codigo_field = $('uf_codigo');

    // carrega cidade
    var body = $(document.body).addClass('ajax-loading');
    var uf_match = 'option[value='+uf_codigo+']';
    var cidade_match = 'option[value='+cidade_codigo+']';
    var request = new Request({url:loadFieldPage,
			       evalResponse: true,
			       onComplete: function(response)
			       {
				       // $('log_res').set('text',response);
				       $('uf_codigo').getElement(uf_match).set('selected',true);
				       $('cidade_codigo').getElement(cidade_match).set('selected',true);
				       $('bairro').value = bairro;
				       $('logradouro').value = logradouro;
				       body.removeClass('ajax-loading');
				       EnableAddress();
			       }});
    var data = 'fieldName=cidade_codigo&sourceId=' + uf_codigo;
    request.send(data);
}

//
// Schedule
//
avForm = new Object();

avForm.OnScheduleFieldLoad = function(fieldName,value,tags)
{
    $(fieldName+'List').tags = tags;

    var scheduleItems = eval(value);
    if(scheduleItems.length > 0)
	for(var i = 0; i<scheduleItems.length; i++)
	    this.AddScheduleItem(fieldName,scheduleItems[i]);
    else
	this.AddScheduleItem(fieldName);
}

avForm.AddScheduleItem = function(fieldName,data)
{
    var list = $(fieldName+'List');
    var numOfOptions = $(fieldName+'_numOfOptions');
    var idx = parseInt(numOfOptions.value) + 1;
 
    var p = new Element("p");
    p.inject(list);

    var label = new Element('label',{ 
	'text': list.tags.appDateTag,
	'for': fieldName+'_'+idx+'__startDay'
    });
    label.inject(p);

    var date = new Element('input', {
	type: 'hidden',
	id: fieldName+'_'+idx+'__start',
	name: fieldName+'_'+idx+'__start'
    });
    date.inject(p);
    
    var day = new Element('input', {
	type: 'text',
	'class': 'textbox',
	value: $defined(data)?data.startDay:'',
	id: fieldName+'_'+idx+'__startDay',
	name: fieldName+'_'+idx+'__startDay', 
	maxlength: '2',
	size: '2',
	events: {
	    keyup: function() { 
		if(this.value.length == 2) 
		    $(fieldName+'_'+idx+'__startMonth').focus()
	    },
	    change: function() { 
		OnDateTimeFieldChange(fieldName+'_'+idx+'__start')
	    }
	}
    });
    day.inject(p);
    p.appendText('/');

    var month = new Element('input', {
	type: 'text',
	'class': 'textbox',
	value: $defined(data)?data.startMonth:'',
	id: fieldName+'_'+idx+'__startMonth',
	name: fieldName+'_'+idx+'__startMonth', 
	maxlength: '2',
	size: '2',
	events: {
	    keyup: function() { 
		if(this.value.length == 2) 
		    $(fieldName+'_'+idx+'__startYear').focus()
	    },
	    change: function() { 
		OnDateTimeFieldChange(fieldName+'_'+idx+'__start')
	    }
	}
    });
    month.inject(p);
    p.appendText('/');

    var year = new Element('input', {
	type: 'text',
	'class': 'textbox',
	value: $defined(data)?data.startYear:'',
	id: fieldName+'_'+idx+'__startYear',
	name: fieldName+'_'+idx+'__startYear', 
	maxlength: '4',
	size: '4',
	events: {
	    keyup: function() { 
		if(this.value.length == 4) 
		    $(fieldName+'_'+idx+'__startHour').focus()
	    },
	    change: function() { 
		OnDateTimeFieldChange(fieldName+'_'+idx+'__start')
	    }
	}
    });
    year.inject(p);
    p.appendText(' - ');

    var hour = new Element('input', {
	type: 'text',
	'class': 'textbox',
	value: $defined(data)?data.startHour:'',
	id: fieldName+'_'+idx+'__startHour',
	name: fieldName+'_'+idx+'__startHour', 
	maxlength: '2',
	size: '2',
	events: {
	    keyup: function() { 
		if(this.value.length == 2) 
		    $(fieldName+'_'+idx+'__startMinute').focus()
	    },
	    change: function() { 
		OnDateTimeFieldChange(fieldName+'_'+idx+'__start')
	    }
	}
    });
    hour.inject(p);
    p.appendText(':');

    var minute = new Element('input', {
	type: 'text',
	'class': 'textbox',
	value: $defined(data)?data.startMinute:'',
	id: fieldName+'_'+idx+'__startMinute',
	name: fieldName+'_'+idx+'__startMinute', 
	maxlength: '2',
	size: '2',
	events: {
	    keyup: function() { 
		if(this.value.length == 2) 
		    $(fieldName+'_'+idx+'__numOfHours').focus()
	    },
	    change: function() { 
		OnDateTimeFieldChange(fieldName+'_'+idx+'__start')
	    }
	}
    });
    minute.inject(p);
    p.appendText(' - ');

    OnDateTimeFieldChange(fieldName+'_'+idx+'__start');

    var numOfHours = new Element('input', {
	type: 'text',
	'class': 'textbox',
	value: $defined(data)?data.numOfHours:'',
	id: fieldName+'_'+idx+'__numOfHours',
	name: fieldName+'_'+idx+'__numOfHours', 
	maxlength: '2',
	size: '2',
    });
    numOfHours.inject(p);

    var removeBtn = new Element('button', {
	text: list.tags.removeTag,
	events: {
	    click: function() {
		if(confirm(list.tags.areYouSureMsg))
		{
		    var p = $(this).getParent();
		    p.getParent().removeChild(p);
		}
		return false;
	    }
	}
    });
    removeBtn.inject(p);

    numOfOptions.value = idx;

    return false;
}
