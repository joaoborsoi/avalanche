// upload functions
function MakeDragUpload(id,path,ResultHandler,max)
{
    var dropbox = $(id);
    if(dropbox.addEventListener)
    {
	dropbox.addEventListener('dragenter', 
				 function(e)
				 {
				     e.stopPropagation();
				     e.preventDefault();
				 }, false);

	dropbox.addEventListener('dragover', 
				 function(e)
				 {
				     e.stopPropagation();
				     e.preventDefault();
				     $(id).addClass('drag-over');
				 }, false);

	dropbox.addEventListener('dragleave', 
				 function(e)
				 {
				     e.stopPropagation();
				     e.preventDefault();
				     $(id).removeClass('drag-over');
				 }, false);

	dropbox.addEventListener('drop', 
				 function(e)
				 {
				     e.stopPropagation();
				     e.preventDefault();
				     
				     $(id).removeClass('drag-over');
    
				     if (window.FormData)
				     {
					 $(document.body).addClass('uploading');  
					 UploadUsingFormData(path,ResultHandler,e.dataTransfer.files,max);
				     }

				     return false;
				 }, false);
    }
}

function UploadUsingFormData(path,ResultHandler,filesToUpload,max)
{
    var f = new FormData();
    if(!$defined(max) || max==0 || max > filesToUpload.length)
	max = filesToUpload.length;
    for (var i=0; i<max; i++)
    {
	var file = filesToUpload[i];
	f.append("files[]", file);			
	f.append("path", path);			
    }
    var request = new XMLHttpRequest();
    request.open("POST", "adminUploadFiles.av", true);
    request.onload = function(event)
    {
	$(document.body).removeClass('uploading');  
	ResultHandler(event);
    };
    request.send(f);       
}

	
