// sets the format of returned value
avForm.GetPhoto = function(fieldName,source)
{
    if(typeof(source)=='undefined')
	source = navigator.camera.PictureSourceType.CAMERA;
    
    // Take picture using device camera and retrieve as uri
    navigator.camera.getPicture(
	// success
	function(imageURI) {
	    // Uncomment to view the base64-encoded image data
	    // console.log(imageData);
	    
	    // Show the captured photo
	    // The inline CSS rules are used to resize the image
	    $('#photoFieldImg').attr('src',imageURI).data('has-image','1').addClass('base64');

	},
	// fail
	function(message) {
	    //alert(message);
	},
	// options
	{ 
	    quality: 50,
	    targetWidth: 800,
	    targetHeight: 800,
	    destinationType: navigator.camera.DestinationType.FILE_URI,
	    correctOrientation: true,
	    sourceType: source
	});
}


avForm.OnPhotoFormLoad = function(id,photoFiled,onSubmit,onFail)
{
    var formField = $("#"+id);
    var photoFieldImg = $('#photoFieldImg');

    if(typeof(FileReader) != 'undefined')
    {
	$('#'+photoFiled).on('change', function(event) {
	    var reader = new FileReader();
	    var selectedFile = event.target.files[0];

	    reader.onload = function(event) {
		var src;
		photoFieldImg.attr('src',event.target.result);
		photoFieldImg.addClass('base64');
	    };

	    reader.readAsDataURL(selectedFile);
	});
    }
    

    if(pg.IsPhonegap())
    {
	formField.find('.browser').remove();

	formField.on('submit', function() {
	    if(!avForm.Submit(formField))
		return false;

	    if(photoFieldImg.data('has-image'))
	    {
		var photoField = photoFieldImg.data('id');
		
		var options = new FileUploadOptions();
		var fileURI =  photoFieldImg.attr('src');
		
		if(fileURI.substr(0,7)!='file://')
		    return false;
		options.fileKey = photoField;
		options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
		options.mimeType = "image/jpeg";
		var ft = new FileTransfer();
		ft.upload(fileURI,
			  encodeURI(formField.attr('action')+'?')+
			  formField.serialize(),
			  function(r) {
			      eval(r.response);
			      console.log("Code = " + r.responseCode);
			      console.log("Response = " + r.response);
			      console.log("Sent = " + r.bytesSent);
			  },
			  function(error) {
			      if(typeof(onFail)=='function')
				  onFail();
			      //avForm.OnSubmitError("Error Code = " + error.code);
			      console.log("upload error source " + error.source);
			      console.log("upload error target " + error.target);
			  },
			  options);
	    }
	    else
		$.ajax({
		    type: "POST",
		    url: formField.attr('action'),
		    data: formField.serialize(),
		    error: function() {
			if(typeof(onFail)=='function')
			    onFail();
		    },
		    dataType: 'script'
		});
	    
	    onSubmit();
	    return false;
	});
    }
    else
    {   // Browser
	formField.find('.phonegap').remove();

	$("#"+id).ajaxForm({
	    /*success:function(responseText)
	      {
	      console.log(responseText);
	      },*/
	    beforeSubmit:function(arr, $form, options)
	    {
		if(avForm.Submit($form))
		{
		    onSubmit();
		    return true;
		}
		return false;
	    },
	    error: function() {
		if(typeof(onFail)=='function')
		    onFail();
	    },
	    dataType: 'script'
	});
    }
}

avForm.ParentOnLoad = avForm.OnLoad;

avForm.OnLoad = function(params)
{
    if(typeof(params) != 'object')
	params = { id: params};

    if(typeof(params.onFail) == 'function')
	params._onFail = params.onFail;

    params.onFail = function()
    {
	console.log('avForm.onFail!');
	    
	alert(pg.labels.connectionError);

	// call parent function
	if(typeof(this._onFail) == 'function')
	    this._onFail(this);
	
	avForm.OnSubmit();
    }

    if(typeof(params.onSuccess)=='function')
	    params._onSuccess = params.onSuccess;
	    
    params.onSuccess = function(responseText,textStatus)
    {
	console.log('avForm.onSuccess!');

	// call parent function
	if(typeof(this._onSuccess) == 'function')
	    this._onSuccess(responseText,textStatus);

    }

    this.ParentOnLoad(params);

}
