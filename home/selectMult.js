function SM_AddSelection(fieldName,all,limit)
{
    SM_Move(document.getElementById(fieldName+'Options'),
	    document.getElementById(fieldName+'Selected'),all,limit);
    SM_SetSelected(document.getElementById(fieldName+'Selected'),
		   document.getElementById(fieldName));
}

function SM_RemoveSelection(fieldName,all,limit)
{
    SM_Move(document.getElementById(fieldName+'Selected'),
	    document.getElementById(fieldName+'Options'),all,limit);
    SM_SetSelected(document.getElementById(fieldName+'Selected'),
		   document.getElementById(fieldName));
}

function SM_SetSelected(selectList, field)
{
    field.value = '';
    for(var i = 0; i < selectList.options.length; i++)
    {
	if(i > 0)
	    field.value += ', ';
	field.value += selectList.options[i].value;
    }
}

function SM_Move(lista1,lista2,all,limit) 
{
    var removeList = new Array();
    for(var i=0; i < lista1.options.length; i++) 
    {
	var AUX = lista1.options[i];
	if ((AUX.selected || all == 1) && (limit==null || limit=='' || 
					   limit==0 || lista2.length < limit))
	{
	    lista2.options[lista2.options.length] = new Option( AUX.text, 
								AUX.value, 
								false, 
								false);
	    removeList[removeList.length] = i;
	}

    }
    SM_Remove(lista1,removeList,all);
}

 function SM_Remove(lista, removeList, all) 
 {
     if(all && removeList.length==lista.length)
	 lista.options.length = 0;
     else
	 for(var i=removeList.length-1; i >= 0; i--) 
	     lista.remove(removeList[i]);
 }

