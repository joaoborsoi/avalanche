game.labels = {
    logoutConfirmMsg: "Deseja sair?",
    updateConfirmMsg: "Todas as jogadas serão perdidas. Deseja atualizar o jogo?",
    restartConfirmMsg: "Reiniciar o jogo? Você perderá todas as jogadas feitas até agora.",
    projectConfirmMsg: "Deseja ativar o projeto?",
    chartThousand: "mil",
    chartMillions: "milhões",
    chartBillions: "bilhões",
    of: "de ",
    currency: "R$",
    months: "Meses",
    thousandSep: '.',
    decimalSep: ','
};
