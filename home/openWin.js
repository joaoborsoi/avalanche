function OpenWin (path,name,winW,winH)
{
    var iMyWidth;
    var iMyHeight;  

    //half the screen width minus half the new window width 
    //(plus 5 pixel borders).
    iMyWidth = (window.screen.width/2) - ((winW/2)+ 10); 

    //half the screen height minus half the new window height
    //(plus title and status bars).
    iMyHeight = (window.screen.height/2) - ((winH/2) + 50);

    var popup = window.open(path,name,"status,toolbar=0,height="+winH+
			    ",width="+winW+",resizable=yes,left=" + iMyWidth +
			    ",top=" + iMyHeight + ",screenX=" + iMyWidth + 
			    ",screenY=" + iMyHeight + ",scrollbars=yes");
    popup.focus();
    return popup;
}
