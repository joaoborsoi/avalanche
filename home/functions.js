//<![CDATA[

function addClassName(oElm, strClassName){
	var strCurrentClass = oElm.className;
	if(!new RegExp(strClassName, "i").test(strCurrentClass)){
		oElm.className = strCurrentClass + ((strCurrentClass.length > 0)? " " : "") + strClassName;
	}
}
function removeClassName(oElm, strClassName){
	var oClassToRemove = new RegExp((strClassName + "\s?"), "i");
	oElm.className = oElm.className.replace(oClassToRemove, "").replace(/^\s?|\s?$/g, "");
}
function $$(strId){
	return document.getElementsByTagName(strId);
}
function $(strId){
	return document.getElementById(strId);
}

function openWin(path,winW,winH, scroll) {
	if(!scroll)var sb='no';
	else var sb='yes';
    	var iMyWidth = (window.screen.width/2) - ((winW/2)+ 10); 
	var iMyHeight = (window.screen.height/2) - ((winH/2) + 50); 
	var win2 = window.open(path,'',"status,toolbar=0,height="+winH+",width="+winW+",resizable=yes,left=" + iMyWidth + ",top=" + iMyHeight + ",screenX=" + iMyWidth + ",screenY=" + iMyHeight + ",scrollbars="+sb);
    win2.opener=window;
    win2.focus();
    return false;
}

// CSS Browser Selector   v0.1.7
// Documentation:         http://rafael.adm.br/css_browser_selector
// License:               http://creativecommons.org/licenses/by/2.5/
// Author:                Rafael Lima (http://rafael.adm.br)
// Contributors:          http://rafael.adm.br/css_browser_selector#contributors
/*  CLASSES
    * ie - Internet Explorer
    * ie6 - Internet Explorer 6
    * ie5 - Internet Explorer 5
    * gecko - Mozilla, Firefox, Camino
    * opera - Opera
    * konqueror - Konqueror
    * webkit or safari - Safari, NetNewsWire, OmniWeb, Shiira
*/
var css_browser_selector = function() {
	var ua = navigator.userAgent.toLowerCase();
	var h = document.getElementsByTagName('html')[0];
	var c = h.className;

	if(ua.indexOf('msie') != -1 && !(ua.indexOf('opera') != -1) && (ua.indexOf('webtv') == -1) ) h.className='ie'+' ie'+ua.charAt(ua.indexOf('msie')+5)+' '+c;
	else if(ua.indexOf('gecko/') != -1) h.className=('gecko '+c);
	else if(ua.indexOf('opera') != -1) h.className=('opera '+c);
	else if(ua.indexOf('konqueror') != -1) h.className=('konqueror '+c);
	else if(ua.indexOf('applewebkit/') != - 1) h.className=('webkit safari '+c);
	else if(ua.indexOf('mozilla/') != -1) h.className=('gecko '+c);
}.apply(this);

sfHover = function() {
	var sfEls = document.getElementById("nav").getElementsByTagName("li");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			//this.className="sfhover";
			addClassName(this, "sfhover");
		}
		sfEls[i].onmouseout=function() {
			//this.className=this.className='';//.replace(new RegExp(" sfhover\\b"), "");
			removeClassName(this, "sfhover");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);
/* http://www.htmldog.com/articles/suckerfish/dropdowns/ */


function aMod_externalLink(){    
    var as,i,link;
    // grab all links, and loop over them
    as=document.getElementsByTagName('a');
    for(i=0;i<as.length;i++){
        var a =as[i];
        link=a.href; 
        var ext=link.split('.').pop();
        // and check if it contains the current location 
        var host = new String(window.location.hostname);
        rExp = /www./gi;
        domain = host.replace(rExp, "");

        if(link.indexOf(domain)==-1 && link.indexOf('javascript:')==-1){
            if(!a.getAttribute('title'))a.setAttribute('title',link);
            a.setAttribute("target","_blank"); 
            
            if(a.className!='')a.className+=' ext-link';
            else a.className='ext-link';                        
        } 
        if (ext=='pdf'){ 
        	a.className = 'pdf-link';
        	a.setAttribute("target","_blank"); 
        }else if (ext=='zip') a.className = 'zip-link';
        else if(ext=='mp3' ){
        	a.className = 'mp3-link ';        	
        	a.setAttribute("target","_blank"); 
        }
    }
}

try {
  document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}

///////////

aMod_externalLink();
 


//]]>
