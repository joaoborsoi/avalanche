var requiredError;

function OnArticleFormLoad()
{
    $('articleFormField').addEvent('submit',SubmitArticle);
}

function SaveTinyMce(properties) 
{
    var properties = properties || new Object();
    var instance = properties.instance || 'mce_editor_0';
    tinyMCE.execInstanceCommand(instance,'mceCleanup');
    tinyMCE.triggerSave(true,true);
    return true;
}

function SubmitForm(form,e)
{
    // prevent the normal submit event
    if($defined(e))
	e.stop();

    // clears any previous required errors
    ClearRequiredError();

    // This empties the log and shows the spinning indicator
    var log = $('log_res').empty().addClass('ajax-loading');
    var body = $(document.body).addClass('ajax-loading');

    // send takes care of encoding and returns the Ajax instance.
    // onComplete removes the spinner from the log, and the response is 
    // evaluated
    form.set('send',{
	    onComplete: function(response) 
	    {
		log.removeClass('ajax-loading');
		body.removeClass('ajax-loading');
		//console.log(response);
	    },
	    evalResponse: true
	});
    form.send();
}

function SubmitArticle(e)
{
    // save html editor content to textarea
    SaveTinyMce({instance:'content'});
    $('publish').value = 'none';
    SubmitForm(this,e);
}

function SubmitArticleAndPublish(publish)
{
    // save html editor content to textarea
    SaveTinyMce({instance:'content'});

    if(publish)
	$('publish').value = 'publish';
    else
	$('publish').value = 'unpublish';

    SubmitForm($('articleFormField'));
}

function SetRequiredError(fieldName)
{
    var field = $(fieldName+'Field');
    if(field)
    {
	$(fieldName+'Field').addClass('requiredError');
	var aux = $(fieldName);
	if(aux)
	    aux.focus();
	requiredError = fieldName;
    }
}

function ClearRequiredError()
{
    if($defined(requiredError))
	$(requiredError+'Field').removeClass('requiredError');
}

function Publish(docId,flag)
{
    var body = $(document.body).addClass('ajax-loading');
    var request = new Request({url:'wikiPublish.av',
			       evalResponse: true,
			       onComplete: function(response)
			       {
				   body.removeClass('ajax-loading');
			       }
	       
	});
    var data = 'docId='+docId+'&flag='+((flag)?'1':'0');
    request.send(data);
}

function SetPublishBtn(id,lastChanged)
{
    $('pubBtn').set('text',
	 GetLabel('unpublishTag')).removeClass('green').addClass('red');
    $('pubBtn').onclick = function(){Publish(id,false);}
    $('pubTxt').set('text',
	 GetLabel('publishedTag')).removeClass('red').addClass('green');
    $('lastChanged').set('text',lastChanged);
}

function SetUnpublishBtn(id,lastChanged)
{
    $('pubBtn').set('text',
		 GetLabel('publishTag')).removeClass('red').addClass('green');
    $('pubBtn').onclick = function(){Publish(id,true);}
    $('pubTxt').set('text',
	    GetLabel('notPublishedTag')).removeClass('green').addClass('red');
    $('lastChanged').set('text',lastChanged);
}

function RemoveArticle(docId,path)
{
    if(confirm(GetLabel('confirmRemoveDocTag')))
    {
	var body = $(document.body).addClass('ajax-loading');
	var request = new Request({url:'wikiRemoveDocument.av',
				   evalResponse: true,
				   onComplete: function(response)
				   {
				       body.removeClass('ajax-loading');
				   }
	    });
	request.send('docId='+docId+'&path='+path);
    }
}

function AddComment(articleId,quoteId)
{
    // if the comment form has already been loaded
    if($('commentForm').getStyle('display')!='none')
    {
	// confirm to cancel previous comment
	if(!confirm(GetLabel('confirmNewCommentTag')))
	    return;

	// re-initiate editor content
	tinyMCE.execCommand('mceRemoveControl',false,'comment');

	// hide comment form
	$('commentForm').setStyle('display','none');

	// empty target
	$('commentForm').set('text','');
    }

    var body = $(document.body).addClass('ajax-loading');

    var req = new Request.HTML(
	      {
		  url:'cmsCommentForm.av', 
		  onSuccess: function(html)
		  {
		      //Inject the new DOM elements into the results div.
		      $('commentForm').adopt(html);
		      $('commentFormField').addEvent('submit',SubmitComment);
		      $('articleId').set('value',articleId);
		      
		      // sets quote
		      if($defined(quoteId))
		      {
			  var quote = '<blockquote>';
			  quote += $('commentTxt'+quoteId).get('html');
			  quote += '</blockquote><p>&nbsp;</p>';
			  $('comment').set('text',quote);
			  $('quoteId').set('value',quoteId);
		      }

		      // load tinymce for comments
		      tinyMCE.init({
			      theme : "advanced",
			      mode : "textareas",
			      plugins: "searchreplace,spellchecker,table,safari",
			      theme_advanced_buttons1: "formatselect,separator,bold,italic,separator,bullist,numlist,separator,blockquote,separator,sub,sup,charmap,separator,link,unlink,separator,spellchecker,separator,undo,redo",
			      theme_advanced_buttons2:"tablecontrols,separator,removeformat,separator,cut,copy,paste,separator,search,replace",
			      theme_advanced_buttons3:"",
			      theme_advanced_blockformats : "p,pre",
			      theme_advanced_toolbar_location : "top",
			      theme_advanced_toolbar_align : "left",
			      content_css : "../tinymce.css",
			      inline_styles : true,
			      paste_use_dialog : false,
			      paste_auto_cleanup_on_paste :true,
			      paste_convert_headers_to_strong : false,
			      paste_strip_class_attributes : "all",
			      paste_remove_spans : false,
			      paste_remove_styles : false,
			      theme_advanced_disable : "underline,justifyleft,justifycenter,justifyright,justifyfull,strikethrough,outdent,indent,styleselect",
			      theme_advanced_resizing : false,
			      apply_source_formatting : true,
			      theme_advanced_statusbar_location: "bottom",
			      editor_selector : "mceEditor",
			      width : "100%",
			      height : "200",
			      spellchecker_languages : "+English=en",
			      auto_focus: "comment"
			  });

		      // shows comment form
		      $('commentForm').setStyle('display','block');

		      // position browser on comment field
		      window.location.hash = 'commentForm';

		  },
		  //onFailure method which will let the user know what
		  //happened.
		  onFailure: function() 
		  {
		      $('commentForm').set('text',
					   GetLabel('requestFailedTag'));
		  },
		  evalScripts: true,
		  onComplete: function(response)
		  {
		      body.removeClass('ajax-loading');
		  }
       	      });
    req.send();
}

function SubmitComment(e)
{
    // save html editor content to textarea
    SaveTinyMce({instance:'comment'});
    SubmitForm(this,e);
}

function RemoveComment(docId)
{
    if(confirm(GetLabel('confirmRemoveCommentTag')))
    {
	var body = $(document.body).addClass('ajax-loading');
	var request = new Request({
		url:'cmsRemoveComment.av',
		evalResponse: true,
		onComplete: function(response) 
		{
		    body.removeClass('ajax-loading');
		}
	    });
	request.send('docId='+docId);
    }
}

function OnRssPrefsFormLoad()
{
    var events = $('events').getElements('input');
    for(var i=0; i<events.length; i++)
	events[i].onclick = function() { OnFieldChange(); }
    $('mailCheckbox').onclick = function() { 
	OnCheckboxChange('mail');
	OnFieldChange(); 
    }
}

function OnFieldChange()
{
    // prepare fields
    var data = '';
    var j = 1;
    var inputList = $('tree').getElements('input');
    for(var i=0; i < inputList.length; i++)
    {
	// only send checked and enabled fields
 	if(inputList[i].getProperty('checked') &&
	   !inputList[i].getProperty('disabled'))
 	{
 	    if(j>1)
 		data += '&';
 	    data += 'prefs_'+j+'__folderId='+inputList[i].value;
	    j++;
 	}
    }

    if(j>1)
	data += '&';
    data += 'prefs_numOfOptions='+(j-1);


    // This shows the spinning indicator
    var body = $(document.body).addClass('ajax-loading');

    // send takes care of encoding and returns the Ajax instance.
    // onComplete removes the spinner from the log, and the response is 
    // evaluated
    var form = $('rssPrefsForm');

    form.set('send',{
	    url: 'cmsSaveRssPrefs.av?'+data,
	    onComplete: function(response) 
	    {
		//alert(response);
		body.removeClass('ajax-loading');
	    },
	    evalResponse: true,
            async: false
	});
    form.send();
}

function SetRSSFolder(folderId)
{
    // check/uncheck subitems
    var checked = $('rssInput'+folderId).getProperty('checked');
    if($('rssUl'+folderId))
    {
	inputList = $('rssUl'+folderId).getElements('input');
	for(var i=0; i < inputList.length; i++)
	{
	    inputList[i].setProperty('checked',checked);
	    inputList[i].setProperty('disabled',checked);
	}
    }
    OnFieldChange();
}

function OnLoginFormLoad()
{
    $('loginFormField').addEvent('submit',function(e){SubmitForm(this,e);});
}

function OnRegisterFormLoad()
{
    $('registerFormField').addEvent('submit',SubmitRegister);
    if($('userId').value != '')
    {
	SetDisabled('login');
	var changeEmailBtn = new Element('a',{
		'href': 'cmsChangeEmailForm.av',
		'text': 'Mudar'
	    });
	changeEmailBtn.inject($('loginField'));
    }

    SetLoadFieldPage('cmsRegisterFormField.av');
}

function SubmitRegister(e)
{
    // check if both passwords are equal
    var password = $('password').value;
    var password2 = $('password2').value;

    if(password != password2)
    {
	e.stop();
	alert('Senhas não iguais!');
	ClearRequiredError();
	SetRequiredError('password2');
	return;
    }

    if(password != '')
    {
	var input = new Element("input",{
		"name": "password",
		"type":"hidden",
		"value": password
	    });
	input.inject($('registerFormField'));
    }
    SubmitForm(this,e);
}

function OnContactFormLoad(id)
{
    $(id).addEvent('submit',function(e){
	if(checkform(this))
	    SubmitForm(this,e);
	else if($defined(e))
	    e.stop();
    });
}

function OnContactSuccess(formId, message)
{
    $(formId).set('text','');
    var p = new Element('p',{
	'class': 'alert',
	'text': message});
    p.inject($(formId));
 }

function OnRegisterSuccess()
{
    $('registerDiv').set('text','');
    $('registerDiv').removeClass('fieldbox');
    var p = new Element('p',{
	'class': 'alert',
	'text': GetLabel('registerSuccessTag')});
    p.inject($('registerDiv'));
 }

function OnRegisterUpdate()
{
    $('registerDiv').set('text','');
    $('registerDiv').removeClass('fieldbox');
    var p = new Element('p',{
	'class': 'alert',
	'text': GetLabel('accountUpdatedTag')});
    p.inject($('registerDiv'));
 }

function OnRecoverPasswordFormLoad()
{
    $('recoverPasswordFormField').addEvent('submit',
					   function(e){SubmitForm(this,e);});
}

function OnRecoverPasswordSuccess()
{
    $('recoverDiv').set('text','');
    var p = new Element('p',{
	'class': 'alert',
	'text': GetLabel('passwordSentTag')});
    p.inject($('recoverDiv'));
 }

function OnChangeEmailFormLoad()
{
    $('changeEmailFormField').addEvent('submit',
				       function(e){SubmitForm(this,e);});
}

function OnChangeEmailSuccess()
{
    $('changeEmailDiv').set('text','');
    var p = new Element('p',{
	'class': 'alert',
	'text': GetLabel('checkEmailSentTag')});
    p.inject($('changeEmailDiv'));
}

function OnSearchSubmit()
{
    // return false if empty
    return ($('filter').value.trim()!='');
}

/* ie6 flickering fix */
try {
  document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}
