game = new Object();

// properties
game.requiredError = null;


// methods
game.Confirm = function(message, onConfirm, onNotConfirm)
{
    if(confirm(message))
	onConfirm();
    else
	if(typeof(onNotConfirm)!='undefined')
	   onNotConfirm();
}

game.Alert = function(message)
{
    alert(message);
}

game.LoadLoginForm = function()
{
    popup.Open('gameLoginForm.av','login-form',null,'login',
	       function(){game.OnFormLoad('loginForm');});
}

game.OnFormLoad = function(formId)
{
    game.formId = formId;
    $("#"+formId).submit(function()
			 {
			     $(this).find('input[type=submit]').attr('disabled',
								     'disabled');
			     game.ClearRequiredError();
			     $.post($(this).attr('action'), $(this).serialize());
			     return false;
			 });
}

game.OnFormSubmitError = function(message)
{
    game.Alert(message);
    $("#"+game.formId).find('input[type=submit]').removeAttr('disabled');
}

game.OnLoginSuccess = function()
{
    popup.Close('login-form');
    this.Start();
}

game.Start = function()
{
    $('body').attr('id','mapa');
    this.Load();
}

game.Logout = function()
{
    game.Confirm(game.labels.logoutConfirmMsg, 
		 function()
		 {
		     ajaxLoad.Start();
		     $.getScript("gameLogout.av",function(){ajaxLoad.Complete();});
		 });
}

game.Update = function()
{
    game.Confirm(game.labels.updateConfirmMsg, 
		 function()
		 {
		     ajaxLoad.Start();
		     $.getScript("gameUpdate.av",function(){ajaxLoad.Complete();});
		 });
}

game.SetRequiredError = function(fieldName)
{
    var field = $('#'+fieldName+'Field');
    if(field)
    {
	field.addClass('requiredError');
	var fieldValue = $('#'+fieldName);
	if(fieldValue && fieldValue.attr('type') != 'hidden')
	    fieldValue.focus();
	else
	{
	    fieldValue = $('#'+fieldName+'_1');
	    if(fieldValue && fieldValue.attr('type') != 'hidden')
		fieldValue.focus();
	    else
	    {
		fieldValue = $('#'+fieldName+'Day');
		if(fieldValue && fieldValue.attr('type') != 'hidden')
		    fieldValue.focus();

	    }
	}

	this.requiredError = fieldName;
    }
}

game.ClearRequiredError = function()
{
    if(this.requiredError != null)
	$('#'+this.requiredError+'Field').removeClass('requiredError');
}

game.Restart = function()
{
    game.Confirm(game.labels.restartConfirmMsg,
		 function()
		 {
		     ajaxLoad.Start();
		     $.getScript("gameRestart.av",function(){ajaxLoad.Complete();});
		 });
}

game.NextAreaEvent = function()
{
    ajaxLoad.Start();
    $.getScript('gameAreaEvent.av?next=1', function(){ajaxLoad.Complete();});
}

game.OnProjectsLoad = function(formId)
{
    $('#'+formId).find('input').each(function(){
	game.SetupProjectField($(this));});
}

game.SetupProjectField = function(field)
{
    switch(field.attr('value'))
    {
    case '1':
	field.parent().addClass('disabled');
	field.attr('disabled','disabled');
	break;
    case '2':
	field.click(function()
		    {
			game.Confirm(game.labels.projectConfirmMsg, 
				     function()
				     {
					 field.attr('value','3');
					 game.SetupProjectField(field);
					 game.SetIndicator(field.attr('id'),3);
				     }, 
				     function()
				     {
					 field.removeAttr('checked');
				     });
		    });
 	break;
    case '3':
	field.parent().addClass('active');
	field.attr('checked','checked');
	field.attr('disabled','disabled');
	field.off('click');
    }
}

game.EnableControls = function()
{
    $('#play').on('click',this.Play).removeClass('disabled');
    $('#restart').on('click',this.Restart).removeClass('disabled');
    $('#logout').on('click',this.Logout).removeClass('disabled');
    var update = $('#update');
    if(update)
	update.on('click',this.Update).removeClass('disabled');
}

game.DisableControls = function()
{
    $('#play').off('click',this.Play).addClass('disabled');
    $('#restart').off('click',this.Restart).addClass('disabled');
    $('#logout').off('click',this.Logout).addClass('disabled');
    var update = $('#update');
    if(update)
	update.off('click',this.Update).addClass('disabled');
}


game.SetIndicator = function(id, value)
{
    game.DisableControls();
    ajaxLoad.Start();
    $.getScript('gameSetIndicators.av?'+id+'='+value,
		function()
		{
		    ajaxLoad.Complete();
		    game.EnableControls();
		});
}

game.OnReportLoad = function()
{
    chart.Hide();
}

game.XLabelFormatter = function(number)
{
    if(number-parseInt(number)>0)
	return FloatToStr(number,2,game.labels.decimalSep,game.labels.thousandSep);
    else 
	return number;
}

game.YLabelFormatter = function(number,type,scale)
{
    if(type=='percent')
	number *= 100;

    var fracDigits = 2;
    if(type=='text' && scale < 1000000)
	fracDigits = 0;

    if(number>999999)
    {
	number = number/1000000;
	return FloatToStr(number,fracDigits,
			  game.labels.decimalSep,
			  game.labels.thousandSep)+'e+6';
    }
    else
	return FloatToStr(number,fracDigits,
			  game.labels.decimalSep,
			  game.labels.thousandSep);
}

game.OpenChartPopup = function(element,fieldName,ylabel,type)
{
    var href = 'gameChart.av?fieldName='+fieldName;
    href += '&ylabel='+encodeURIComponent(ylabel);
    href += '&type='+type;

    var onSuccess = function()
    {
	game.CreateChart(fieldName,ylabel,type);
    }
    popup.Open(href,'graph',element,'chart-popup',onSuccess, null);
}

game.OpenChart = function(fieldName,ylabel,type)
{
    game.CreateChart(fieldName,ylabel,type,chart.Show);
}

game.CreateChart = function(fieldName,ylabel,type, onSuccess)
{
    game.yMaxValue = null;
    $.getScript('gameGetMaxIndicator.av?fieldName='+fieldName,
		function()
		{ 
		    if(typeof(onSuccess) != 'undefined')
			onSuccess();
		    game.DoCreateChart(fieldName,ylabel,type); 
		});
}

game.DoCreateChart = function(fieldName,ylabel,type)
{
    // prepara label do eixo y
    var scale = 0;
    var ylabelPostfix = "";
    if(game.yMaxValue != null && (type=='currency' || type=='text' || type=='float'))
    {
	if(game.yMaxValue > 999 && game.yMaxValue <= 999999 && type != 'text')
	{
	    scale = 1000;
	    ylabelPostfix = game.labels.chartThousand;
	}
	else if(game.yMaxValue > 999999 && game.yMaxValue <= 999999999)
	{
	    scale = 1000000;
	    ylabelPostfix = game.labels.chartMillions;
	}
	else if(game.yMaxValue > 999999 && game.yMaxValue <= 999999999)
	{
	    scale = 1000000000;
	    ylabelPostfix = game.labels.chartBillions;
	}
    }

    switch(type)
    {
    case 'percent':
	if(ylabelPostfix!="")
	    ylabelPostfix += " ";
	ylabelPostfix += '%';
	break;
    case 'currency':
	if(ylabelPostfix!="")
	    ylabelPostfix += " ";
	if(scale == 1000000 || scale == 1000000000)
	    ylabelPostfix += game.labels.of;
	ylabelPostfix += game.labels.currency;
	break;
    }

    if(ylabelPostfix!="")
	ylabel += " <small>(" + ylabelPostfix + ")</small>";

    dygraph = new Dygraph(
	document.getElementById('chart'),
	"gameChartData.av?fieldName="+fieldName+"&scale="+scale, // path to CSV file
	{
	    ylabel: '<h4>'+ylabel+'</h4>',
	    xlabel: '<h4>'+game.labels.months+'</h4>',
	    showRangeSelector: true,
	    yAxisLabelWidth: 75,
	    axes: 
	    {
		y: 
		{
		    valueFormatter: function(number)
		    {
			return game.YLabelFormatter(number,type,scale);
		    },
		    axisLabelFormatter: function(number)
		    {
			return game.YLabelFormatter(number,type,scale);
		    }
		},
		x: 
		{
		    axisLabelFormatter:function(number)
		    {
			return game.XLabelFormatter(number);
		    }
		}
	    }
	}
    );
}

game.OpenQuiz = function(id,targetDiv,type,element,sourceId,onSuccess)
{
    game.quizesId = sourceId;
    var popupId = element.parent().parent().parent().parent().parent().attr('id');
    popup.Close(popupId);
    popup.Open('gameArea.av?id='+id,targetDiv,null,type,onSuccess);
}

game.OnQuizSubmitted = function(targetDiv)
{
    ajaxLoad.Start();
    $.getScript("gameLoadIcons.av", function(){ajaxLoad.Complete();
					       popup.Close(targetDiv);});
}

game.Play = function()
{
    // seleciona eventos, incrementa turno, dispara eventos 
    // e recalcula indicadores
    game.DisableControls();
    if(typeof(game.OnPlay) != 'undefined' && game.OnPlay != null)
	game.OnPlay();
    ajaxLoad.Start();
    $.getScript('gamePlay.av',function(){ajaxLoad.Complete();});
}

game.Load = function()
{
    ajaxLoad.Start();
    var d1 = new $.Deferred();
    var d2 = new $.Deferred();
    $.when(d1,d2,$.getScript("gameLoadIcons.av")).then(function(){
	$.getScript('gameAreaEvent.av', function(){ajaxLoad.Complete()})});
    $('#userInfo').load('gameUserInfo.av',function(){d1.resolve();});
    $('#panel').load('gamePanel.av',function()
		     {
			 if(typeof(game.OnIndicatorsLoad!='undefined') &&
			    game.OnIndicatorsLoad!=null)
			     game.OnIndicatorsLoad();
			 d2.resolve();
		     });
}

game.Execute = function()
{
    ajaxLoad.Start();
    $.getScript("gameExecute.av",function(){ajaxLoad.Complete();});
}

$(document).ready(function() {
    game.Execute();
});


