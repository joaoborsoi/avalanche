function Initiate(currTab)
{
    tab = currTab;
    clipNode = null;
    clipType = null;
    cutNode = false;
    copyPath = null;
    copyTitle = null;
    RemoveFocusOutline();
}

function DisableButton(button, flag)
{
    var btn = document.getElementById(button);
    if(btn)
    {
	btn.disabled=flag;
	btn.className=flag?'disabled':null;
    }

}

function OnLoadFiles(path,node,type)
{
    currNode = null;
    currType = null;
    currPath = (path!="")?path:"/";

    DisableButton('editBtn', true);
    DisableButton('removeBtn', true);
    DisableButton('openBtn', true);
    DisableButton('copyBtn', true);
    DisableButton('cutBtn', true);
    DisableButton('upBtn', true);
    DisableButton('downBtn', true);

    DisableButton('selectBtn', true);

    if(node != null && ifiles.document.getElementById('node_'+node))
	OnNodeClick(node,type);

    // activate drag and drop files
    if($('adminLibBody'))
	ifiles.MakeDragUpload('filesDiv', '/library'+
			      decodeURIComponent(currPath).replace(/\+/g, " "),
			      OnUploadFiles);
}

function OnUploadFiles(event)
{
    if (event.target.readyState != 4)
	return;
    /* If we got an error display it. */
    result = eval("(" + event.target.responseText + ")");			
    if(result.status != 0)
	alert(result.message);
    else
	ReloadFiles();
}

function Reload()
{
    ReloadFolders();
    ReloadFiles();
}

function ReloadFiles()
{
    ifiles.location='?pageId='+tab+'Files&path='+currPath;
}

function ReloadFolders()
{
    ifolders.location='?pageId='+tab+'Folders&path='+currPath;
}

function OnNodeClick(nodeId, type)
{
    // enable buttons
    if(currNode==null)
    {
	DisableButton('editBtn', false);
	DisableButton('removeBtn', false);
	DisableButton('openBtn', false);
	DisableButton('upBtn', false);
	DisableButton('downBtn', false);

    }
    DisableButton('selectBtn',(type=='folder'));
    DisableButton('copyBtn',(type=='folder'));
    DisableButton('cutBtn', false);

    // select node
    var radio=ifiles.document.getElementById('radio_'+nodeId);
    var obj=ifiles.document.getElementById('node_'+nodeId);
    radio.checked=true;
    obj.className='selected';
    if(currNode && currNode != nodeId)
	ifiles.document.getElementById('node_'+currNode).className=null;
    currNode=nodeId;
    currType=type;
}

function OnNodeDblClick(nodeId, type)
{
    OnNodeClick(nodeId, type);
    OnOpenClick();
}

function OnCreateFolderClick()
{
    var w = 505;
    var h = 450;

    OpenWin('?pageId=adminFolderForm&tab='+tab+'&folderPath='+currPath,'folder', 
	    w, h);
}

function OnCreateFileClick()
{
    var w = 780;
    var h = 680;
    if(tab=='adminLib')
    {
	var w = 505;
	var h = 430;
    }
    OpenWin('?pageId=adminFileForm&tab='+tab+'&path='+currPath,'file',w,h);
}

function OnEditClick()
{
    switch(currType)
    {
    case 'folder':
	var w = 505;
	var h = 450;

	OpenWin('?pageId=adminFolderForm&tab='+tab+"&folderId="+currNode,
		'folder_'+currNode,w,h);
	break;

    case 'file':
	var w = 780;
	var h = 680;
	if(tab=='adminLib')
	{
	    var w = 505;
	    var h = 430;
	}
	OpenWin('?pageId=adminFileForm&tab='+tab+'&path='+currPath+'&docId='+
		currNode,'file_'+currNode, w,h);
	break;
    }
}

function OnRemoveClick()
{
    var loc;
    var title = ifiles.document.getElementById('title_'+currNode).innerHTML; 
    switch(currType)
    {
    case 'folder':
	if(!confirm(adminRemoveFolderMsg +"\""+ title + "\"?"))
	    return;
	loc = '?pageId=adminDelFolder&tab='+tab+"&folderId="+currNode;
	loc += '&path='+currPath;
	break;

    case 'file':
	if(!confirm(adminRemoveFileMsg +"\""+title +"\"?"))
	    return;
	loc = '?pageId=adminDelDocLink&tab='+tab+'&path='+currPath;
	loc += '&docId='+currNode+'&title='+title;
	break;
    }
    ifiles.location = loc;
}

function OnOpenClick()
{
    switch(currType)
    {
    case 'folder':
	var path = ifiles.document.getElementById('title_'+currNode).innerHTML;
	var path = currPath + encodeURIComponent(path) + '/';
	ifolders.location = '?pageId='+tab+'Folders&path='+path;
	ifiles.location = '?pageId='+tab+'Files&path='+path;
	break;
	
    case 'file':
	if(tab=='adminContent')
	    OnEditClick();
	else
	    window.open('?pageId=adminGetFileContent&fieldName=content&docId='+
			currNode,'fileData_'+currNode);
	break;
    }
}

function OnCopyClick(cut)
{
    if(typeof(cut)=='undefined')
	cut = false;

    clipNode = currNode;
    clipType = currType;
    cutNode = cut;
    copyPath = currPath;
    copyTitle = ifiles.document.getElementById('title_'+clipNode).innerHTML; 

    switch(clipType)
    {
    case 'folder':
	if(cutNode)
	    DisableButton('pasteBtn',false);
	break;

    case 'file':
	DisableButton('pasteBtn',false);
	if(!cutNode)
	    DisableButton('createLinkBtn',false);
	break;
    }
    var status = "O arquivo \"";
    status += ifiles.document.getElementById('title_'+currNode).innerHTML;
    status += "\" está na área de transferência";
    window.status = status;
}

function OnPasteClick()
{
    var location = '';
    switch(clipType)
    {
    case 'folder':
	if(cutNode=='1')
	{
	    location += '&pageId=adminPasteFolder';
	    location += '&folderId='+clipNode;
	    location += '&path='+currPath;
	    location += '&cutNode=1';
	    location += '&title='+copyTitle;
	}
	break;

    case 'file':
	location += '&pageId=adminPasteDoc';
	location += '&docId='+clipNode;
	location += '&path='+currPath;
	location += '&cutNode='+(cutNode?'1':'0');
	location += '&copyPath='+copyPath;
	location += '&title='+copyTitle;
	if(currNode!=null)
	    location += '&beforeDocId='+currNode;
	break;
    }

    if(location!='')
	ifiles.location = '?tab='+tab + location;
}

function OnCreateLinkClick()
{
    var location = '';
    switch(clipType)
    {
    case 'file':
	location += '&pageId=adminCreateDocLink';
	location += '&docId='+clipNode;
	location += '&path='+currPath;
	location += '&cutNode='+(cutNode?'1':'0');
	location += '&copyPath='+copyPath;
	location += '&title='+copyTitle;
	if(currNode!=null)
	    location += '&beforeDocId='+currNode;
	break;
    }

    if(location!='')
	ifiles.location = '?tab='+tab + location;
}

function OnPaste()
{
    DisableButton('pasteBtn', true);
    DisableButton('createLinkBtn', true);
}

function OnCreateLink()
{
    DisableButton('pasteBtn', true);
    DisableButton('createLinkBtn', true);
}


function SetOrder(up)
{
    var location = '?pageId=adminSetOrder&tab='+tab;
    location += '&up='+up+'&type='+currType;
    switch(currType)
    {
    case 'folder':
	var folderPath = currPath+
	    ifiles.document.getElementById('title_'+currNode).innerHTML + '/';
	location += '&folderPath='+folderPath+'&folderId='+currNode+'&path='+currPath;
	break;

    case 'file':
	location += '&path='+currPath+'&docId='+currNode;
	location += '&title='+ifiles.document.getElementById('title_'+currNode).innerHTML;
	break;
    }
    ifiles.location = location;
}

function OnSelectClick(returnFunction,fieldName)
{
    var selectOper = 'window.opener.'+ returnFunction + "('" + fieldName;
    selectOper += "'," +currNode + ",\"";
    selectOper += ifiles.$('title_'+currNode).get('text') + "\",\"";
    selectOper += ifiles.$('contentType_'+currNode).get('text')+ "\")";

    if(window.opener)
	eval(selectOper);
    close();
}

function OnCancelClick()
{
    close();
}

function GetIcon(mimeType)
{
    switch(mimeType)
    {
    case 'text/css': return 'css.png'; 
    case 'application/msword': return 'doc.png';
    case 'application/postscript': return 'eps.png'; 
    case 'application/octet-stream': return 'exe.png'; 
    case 'image/gif': return 'gif.png'; 
    case 'image/png': return 'png.png'; 
    case 'text/html': return 'html.png'; 
    case 'image/jpeg': 
    case 'image/pjpeg': return 'jpg.png'; 
    case 'application/x-javascript': return 'js.png'; 
    case 'audio/mpeg': return 'mp3.png'; 
    case 'application/pdf': return 'pdf.png'; 
    case 'audio/x-pn-realaudio': return 'ram.png'; 
    case 'application/x-shockwave-flash': return 'swf.png'; 
    case 'text/plain': return 'txt.png'; 
    case 'audio/x-wav': return 'wav.png'; 
    case 'application/vnd.ms-excel': return 'xls.png'; 
    case 'application/zip': return 'zip.png';
    default: return 'other.png';
    }
}

function OnLoadUsers()
{
    currUser = null;

    DisableButton('editUserBtn', true);
    DisableButton('removeUserBtn', true);
}

function OnLoadGroups(groupId)
{
    currGroup = null;
    OnGroupClick(groupId);
}

function OnUserClick(userId)
{
    // enable buttons
    if(currUser==null)
    {
	DisableButton('editUserBtn', false);
	DisableButton('removeUserBtn', false);
    }

    // select node
    var radio=iusers.document.getElementById('radio_'+userId);
    var obj=iusers.document.getElementById('user_'+userId);
    radio.checked=true;
    obj.className='selected';
    if(currUser && currUser != userId)
	iusers.document.getElementById('user_'+currUser).className=null;
    currUser=userId;
}

function OnUserDblClick(userId)
{
    OnUserClick(userId);
    OnEditUserClick();
}

function OnGroupClick(groupId)
{
    // select node
    var radio=igroups.document.getElementById('radio_'+groupId);
    var obj=igroups.document.getElementById('group_'+groupId);
    radio.checked=true;
    obj.className='selected';
    if(currGroup && currGroup != groupId)
	igroups.document.getElementById('group_'+currGroup).className=null;
    currGroup=groupId;

    // load group users
    iusers.location = '?pageId=adminUsers&groupId='+groupId;
}

function OnGroupDblClick(groupId)
{
    OnGroupClick(groupId);
    OnEditGroupClick();
}

function OnCreateGroupClick()
{
    OpenWin('?pageId=adminGroupForm&tab=adminUser','group',250,390);
}

function OnEditGroupClick()
{
    OpenWin('?pageId=adminGroupForm&tab=adminUser&groupId='+currGroup,'group_'+currGroup
    ,250,390);
}

function OnRemoveGroupClick()
{
    var title = igroups.document.getElementById('title_'+currGroup).innerHTML; 
    if(!confirm(adminRemoveGroupMsg + "\""+title +"\"?"))
	    return;
    igroups.location = '?pageId=adminDelGroup&groupId='+currGroup;
}

function OnCreateUserClick()
{
    OpenWin('?pageId=adminUserForm&tab=adminUser','user',500,390);
}

function OnEditUserClick()
{
    OpenWin('?pageId=adminUserForm&tab=adminUser&userId='+currUser,
	    'user_'+currUser,500,390);
}

function OnRemoveUserClick()
{
    var title = iusers.document.getElementById('title_'+currUser).innerHTML; 
    if(!confirm(adminRemoveUserMsg + "\"" + title + "\"?"))
	    return;
    var loc = '?pageId=adminDelUser&userId='+currUser+'&groupId='+currGroup;
    iusers.location = loc;
}

function Logout()
{
    if(!confirm(adminQuitMsg))
	return;

    window.location = '?doLogout=1&pageId=' + tab;
}

function OnSubmitUserRefresh()
{
    iusers.document.getElementById('groupId').value = currGroup;
}

function RemoveFocusOutline()
{
    var footer=document.getElementById('footer');
    if(footer)
    {
	var buttons=footer.getElementsByTagName('button');
	for(i=0;i<buttons.length;i++)
	    buttons[i].setAttribute('onfocus', "this.blur()");
    }
}



