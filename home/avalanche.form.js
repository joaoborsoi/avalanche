avForm = new Object();

//
// General funcions
//
avForm.SetDisabled = function(id)
{
    return $('#'+id).attr('disabled','disabled').addClass('disabled');
}

avForm.SetEnabled = function(id)
{
    return $('#'+id).removeAttr('disabled').removeClass('disabled');
}

avForm.OnChange = function()
{
    if(window.onbeforeunload || typeof(this.labels.confirmCloseMsg)=='undefined')
	return;
    window.onbeforeunload = function (event)
    {
	if(!event) event = window.event;
	event.returnValue = avForm.labels.confirmCloseMsg;
	return event.returnValue;
    }
}

avForm.OnSave = function()
{
    window.onbeforeunload = null;
}

// params {id,onSuccess,beforeSerialize,onFail}
avForm.OnLoad = function(params)
{
    // compatibilização com versão anterior com parâmetro unico sendo o id
    var selector;
    if(typeof(params)=='object')
    {
	if(typeof(params.selector)!='undefined')
	    selector = params.selector;
	else
	    selector = '#'+params.id;
    }
    else
	selector = '#'+params;

    var formParams = {
	success:function(responseText,textStatus)
	{
	    if(typeof(params.onSuccess)=='function')
		params.onSuccess(responseText,textStatus);
	    // console.log(responseText);
	},
	beforeSerialize:function($form, options) {
	    if(typeof(params.beforeSerialize)=='function')
		if(!params.beforeSerialize($form, options))
		    return false;

	    var password = $('#password');
	    if(password.length>0 && password.val()!='')
	    {
		var input = '<input id="hiddenPwd" name="password" ';
		input += 'type="hidden" value="'+password.val()+'" />';
		$("#registerFormField").append(input);
	    }
	    else
		$('#hiddenPwd').remove();
	    return avForm.Submit($form);
	},
	error: function(){
	    console.log('avForm.error');
	    if(typeof(params.onFail)=='function')
		params.onFail();
	}
    }

    if(typeof(params.timeout)!='undefined')
	formParams.timeout = params.timeout;
    if(typeof(params.dataType!='undefined'))
	formParams.dataType = params.dataType;


    var forms = $(selector);
    var dataFields = forms.find('.dateField').children('div');
    if(dataFields.length>0)
    {
	dataFields.datetimepicker({
	    pickTime: false,
	    language: $('html').attr('lang'),
	    useCurrent: false
	});
	dataFields.on("dp.error",function(e){
	    $(e.target).find('input:text').val('');
	});
	for(var i=0;i<dataFields.length;i++)
	{
	    var dataField = $(dataFields[i]);
	    var value = dataField.find('input.date-value').val();
	    if(value == '--' || value == "")
		value = null;
	    dataField.data('DateTimePicker').setDate(value);
	}
	dataFields.on("dp.change",function(e){
	    avForm.OnChange();
	});
    }
    
    forms.ajaxForm(formParams);
}

avForm.Submit = function(form)
{
    this.form = form;
    form.find('input[type=submit],button[type=submit]').attr('disabled',
							     'disabled').addClass('disabled');
    $(document.body).addClass('ajax-loading');

    // ajusta formato dos campos de data
    var dataFields = form.find('.dateField').children('div');
    for(var i=0; i<dataFields.length; i++)
    {
	var dataField = $(dataFields[i]);
	var date = dataField.data('DateTimePicker').getDate();
	if(date)
	    dataField.find('input.date-value').val(date.format('YYYY-MM-DD'));
    }

    var requiredList = form.find('.required');
    for(var i=0; i<requiredList.length;i++)
    {
	var field = $(requiredList[i]);
	if(field.hasClass('selectMultField'))
	{
	    if(field.find("input:checkbox:checked").length==0)
	    {
		var id = field.attr('id');
		this.OnFieldError(id.substr(0,id.length-5),this.labels.fieldRequired);
		return false;
	    }
	}
	else if (field.hasClass('dateField'))
	{
	    var input = field.find('input.date-value');
	    if($.trim(input.val())=='')
	    {
		this.OnFieldError(input.parent().attr('id'),this.labels.fieldRequired);
		return false;
	    }
	}
	else
	{
	    var fieldList = field.find("input[name],textarea[name],select[name]");
	    for(var j=0; j<fieldList.length ;j++) 
	    {
		var obj = $(fieldList[j]);
		if($.trim(obj.val())=='')
		{
		    this.OnFieldError(obj.attr('id'),this.labels.fieldRequired);
		    return false;
		}
	    }
	}
    }
    return true;
}

avForm.OnSubmit = function()
{
    $(document.body).removeClass('ajax-loading');
    this.form.find('input[type=submit],button[type=submit]').removeAttr('disabled').removeClass('disabled');
}

avForm.Alert = function(message)
{
    alert(message);
}

avForm.Confirm = function(message)
{
    return confirm(message);
}

avForm.OnSubmitError = function(message)
{
    this.OnSubmit();
    this.Alert(message);
}

avForm.OnFieldError = function(fieldName, message)
{
    this.OnSubmit();
    var field = $('#'+fieldName);
    if(field.length==0 || field.attr('type') == 'hidden')
	field = $('#'+fieldName+'Field');

    if(field.length==0 || field.attr('type') == 'hidden')
	avForm.Alert(message);
    else
    {
	field.focus();

	field.popover({
	    placement: 'bottom',
	    content: message,
	    html: true
	});
	field.on('keypress', function() {
	    field.popover('destroy');
	});
	field.popover('show');
	setTimeout(function(){
	    field.popover('destroy');
	},4000);
    }
}

//
// Composed fields
//
avForm.OnComposedFieldKeyUp = function(fieldName,fieldId,length)
{
    if($('#'+fieldName+'_'+fieldId).val().length == length) 
	$('#'+fieldName+'_'+(fieldId+1)).focus();
}

avForm.OnComposedFieldChange = function(fieldName,numOfFields)
{
    var field = $('#'+fieldName);
    field.val("");
    for(var i=0;i<numOfFields;i++)
	field.val(field.val()+$('#'+fieldName+'_'+(i+1)).val());
    this.OnChange();
}


//
// Text area
//
avForm.OnTextAreaKeyPress = function(field,size)
{
    if(field.value.length >= size)
	field.value = field.value.substring(0,size);
}


//
// Float fields
//
avForm.fracDigits = 2;
avForm.decimalSep = '.';
avForm.thousandSep = ',';

avForm.SetLocaleConv = function(fracDigits, decimalSep, thousandSep)
{
    this.fracDigits = fracDigits;
    this.decimalSep = decimalSep;
    this.thousandSep = thousandSep;
}

avForm.GetFracDigits = function(fracDigits)
{
    if(typeof(fracDigits)!='undefined')
	return fracDigits;
    return this.fracDigits;
}

avForm.GetDecimalSep = function(decimalSep)
{
    if(typeof(decimalSep)!='undefined')
	return decimalSep;
    return this.decimalSep;
}

avForm.GetThousandSep = function(thousandSep)
{
    if(typeof(thousandSep)!='undefined')
	return thousandSep;
    return this.thousandSep;
}

avForm.FloatToStr = function(num, fracDigits, decimalSep, thousandSep)
{
    var locFracDigits = this.GetFracDigits(fracDigits);
    var locDecimalSep = this.GetDecimalSep(decimalSep);
    var locThousandSep = this.GetThousandSep(thousandSep);
	       
    if(!isFinite(num))
	num = 0;
    var result = '';
    var numInt;
    var numDec;
    var prefix = '';
    if(locFracDigits > 0)
    {
        numInt = parseInt(num).toString();
	if(numInt==0 && numInt>num)
	    prefix='-';
	numDec = Math.round(Math.abs(num-numInt)*Math.pow(10,locFracDigits)).toString();
 	while(numDec.length < locFracDigits)
 	    numDec = '0' + numDec; 

	if(parseInt(numDec).toString().length > locFracDigits)
        {
 	    numDec = numDec.substr(1);
	    numInt = parseInt(numInt)+1;
	    numInt = numInt.toString();
        }
    }
    else
        numInt = Math.round(num).toString();
    var numChar = 0;
    for(var i=numInt.length-1; i >= 0;i--)
    {
	if(numChar>0 && numChar%3 == 0 && !(num<0 && i==0))
	    result = locThousandSep + result;
	result = numInt.charAt(i) + result;
	numChar++;
    }
    if(locFracDigits > 0)
	result = prefix + result + locDecimalSep + numDec;
    return result;
}

avForm.StrToFloat = function(num, decimalSep, thousandSep)
{
    var locDecimalSep = this.GetDecimalSep(decimalSep);
    var locThousandSep = this.GetThousandSep(thousandSep);

    if(typeof num != 'string')
	return num;
    if(locThousandSep == '.')
	num = num.replace(/\./g, '');
    else
	num = num.replace(RegExp(locThousandSep, 'g'), '')
    if(locDecimalSep != '.')
	num = num.replace(locDecimalSep, '.');
    num = parseFloat(num);
    if(isNaN(num))
	return (parseFloat(0));
    return num;
}

avForm.OnFloatFieldChange = function(field)
{
    field.value=this.FloatToStr(this.StrToFloat(field.value));
    this.OnChange();
//    this.EvalFormulas();
}

//
// Select fields
//
avForm.OnSelectFieldChange = function(value,targetField,extraFields)
{
    this.OnChange();
    if(targetField != '' && 
       typeof(this.loadFieldPage)!= 'undefined' && this.loadFieldPage != '')
    {
	var body = $(document.body).addClass('ajax-loading');
	var data = '?fieldName='+targetField+'&sourceId=' + value;
	if(typeof(extraFields)!='undefined')
	    data += '&'+extraFields;
	$.getScript(this.loadFieldPage+data, 
		    function() 
		    {
			body.removeClass('ajax-loading');
		    });
    }
}

avForm.ResetSelectField = function(fieldName,targetField)
{
    $('#'+fieldName).html("<option>&nbsp;</option>");
    this.OnSelectFieldChange(0,targetField);
    
}

avForm.AddSelectOption = function(fieldName,value,text)
{
    $('#'+fieldName).append($('<option />').val(value).text(text));
}

//
// cep
//
avForm.OnCepChange = function(fieldName)
{
    this.OnComposedFieldChange(fieldName,2);
    var cep = $('#'+fieldName).val();
    if(cep.length>=8)
    {
	var body = $(document.body).addClass('ajax-loading');
	this.DisableAddress();
	$.getScript('cepLoadAddress.av?cep='+cep,
		    function() 
		    {
			body.removeClass('ajax-loading');
		    });
    }
}

avForm.DisableAddress = function()
{
    this.SetDisabled('uf_codigo');
    this.SetDisabled('cidade_codigo');
    this.SetDisabled('bairro');
    this.SetDisabled('logradouro');
}

avForm.EnableAddress = function()
{
    this.SetEnabled('uf_codigo');
    this.SetEnabled('cidade_codigo');
    this.SetEnabled('bairro');
    this.SetEnabled('logradouro');
}

avForm.LoadAddress = function(uf_codigo,cidade_codigo,bairro,logradouro)
{
    // ajusta estado
    var uf_codigo_field = $('#uf_codigo');

    // carrega cidade
    var body = $(document.body).addClass('ajax-loading');
    var url = this.loadFieldPage+'?fieldName=cidade_codigo&sourceId=' + uf_codigo;
    $.getScript(url,
		function()
		{
		    // $('log_res').set('text',response);
		    $('#uf_codigo').val(uf_codigo);
		    $('#cidade_codigo').val(cidade_codigo);
		    $('#bairro').val(bairro);
		    $('#logradouro').val(logradouro);
		    body.removeClass('ajax-loading');
		    avForm.EnableAddress();
		});
}


// image list
avForm.CreateImageList = function(fieldName,path,maxImages)
{
    eval('this.'+fieldName+'=new Object();');
    eval('this.'+fieldName+'.selected=0;');
    eval('this.'+fieldName+'.path="'+path+'";');
    eval('this.'+fieldName+'.maxImages="'+maxImages+'";');
    eval('this.'+fieldName+'.numImages=0;');
    MakeDragUpload(fieldName+'Block',path,
		   function OnUploadImages(event)
		   {
		       if (event.target.readyState != 4)
			   return;
		       /* If we got an error display it. */
		       //     alert(event.target.responseText);
		       result = eval("(" + event.target.responseText + ")");			
		       if(result.status != 0)
		       {
			   alert(result.message);
			   return;
		       }

		       for(var i=0; i<result.numFiles; i++)
			   avForm.AddImage(fieldName, 
					   result.docList[i].docId,
					   result.docList[i].title,
					   result.docList[i].type);
		       
		   },maxImages);
}

avForm.AddImage = function(fieldName, imageId, title, contentType)
{
    $('#'+fieldName+'BlockMsg').hide();

    var field = $('#'+fieldName+'Block');
    var numOfOptions = $('#'+fieldName+'_numOfOptions');
    var idx = parseInt(numOfOptions.val()) + 1;
    var numImages = eval('this.'+fieldName+'.numImages') + 1;
    var maxImages = eval('this.'+fieldName+'.maxImages');
    if(maxImages>0)
    {
	if(numImages == maxImages)
	    this.SetDisabled(fieldName+'_insertImageBtn');
	else if (numImages > maxImages)
	    return;
    }
    eval('this.'+fieldName+'.numImages++');
    var ul;
    if(idx == 1)
	field.append(ul = $("<ul>").attr('id',fieldName));
    else
	ul = $('#'+fieldName);

    ul.append($("<li>")
	      .attr('id',fieldName+"_"+imageId)
	      .on('click',function(){ 
		  avForm.SelectImage(fieldName,imageId); 
	      })
	      .append($('<input>').attr({
		  "type":"hidden",
		  "name": fieldName+"_"+idx+"__value",
		  "value": imageId
	      }))
	      .append(this.CreateImageNode(fieldName,imageId,title,contentType,true)));

    numOfOptions.val(idx);

//    MakeDraggable(fieldName);
}

avForm.CreateImageNode = function(fieldName,imageId,title,contentType,disableCache)
{
    var span = $('<span>').attr({"id":fieldName+"_"+imageId+"_span"});

    contentType = contentType.split("/");
    switch(contentType[0])
    {
    case 'image':
	var imgSrc = "adminGetFileContent.av?size=100";
	imgSrc += "&fieldName=content&docId="+imageId;
	if(disableCache)
	{
	    tmp = new Date(); 
	    imgSrc += "&"+tmp.getTime();
	}
	span.append($('<img>').attr({
            "id": fieldName+"_"+imageId+"_img",
	    "src":imgSrc,
	    "alt":(typeof(title)!='undefined')?title:imageId
	}));

	if(typeof(title)!='undefined')
	{
	    span.append($('<small>').attr({
		"id": fieldName+"_"+imageId+"_small"
	    }).text(title));
	}
	break;

    default:
	// <label>
	span.append($('<label>').attr({
	    "class":"imageItem "+contentType[1].replace(/\./g,"-")
	}));

	// <a>
	var href = "adminGetFileContent.av";
	href += "?fieldName=content&docId="+imageId;
	if(!typeof(title)!='undefined')
	    title = contentType[1];
	span.append($('<a>').attr({
	    "id": fieldName+"_"+imageId+"_a",
	    "href":href
	}).text(title));
	break;
    }
    return span;
}

avForm.UpdateImage = function(fieldName,imageId,title,contentType)
{
    if(!contentType)
    {
	var img = $('#'+fieldName+"_"+imageId+"_img");
	if(img)
	    img.attr("alt",title);;
	var small = $('#'+fieldName+"_"+imageId+"_small");
	if(small)
	    small.text(title);;
	var a = $('#'+fieldName+"_"+imageId+"_a");
	if(a)
	    a.text(title);;
    }
    else
    {
	var span = CreateImageNode(fieldName,imageId,title,contentType,true);
	$('#'+fieldName+"_"+imageId+"_span").replaceWith(span);
    }
}

avForm.EmbedImageUploadForm = function(fieldName)
{
    var path='wikiImageUploadForm.av?fieldName='+fieldName;
    OpenWin(path,'imageUploadForm',500,300);
}

avForm.SelectImage = function(fieldName,imageId)
{
    $('#'+fieldName+"_"+imageId).addClass('selected');
    if(eval('this.'+fieldName+'.selected && '+'this.'+fieldName+'.selected!='+imageId))
    {
	var aux = "$('#"+fieldName+"_'+this."+fieldName+".selected)";
	aux += ".removeClass('selected')";
	eval(aux);
    }
    eval('this.'+fieldName+'.selected='+imageId);
 }

avForm.RemoveImage = function(fieldName)
{
    if(eval('this.'+fieldName+'.selected'))
    {
	var aux = "$('#"+fieldName+"')";
	aux += ".removeChild($('#";
	aux += fieldName+"_'+this."+fieldName+".selected));";
	eval(aux);
	var imageListBlockMsg = $('#'+fieldName+'BlockMsg');
	if(imageListBlockMsg)
	{
	    var imgList=$('#'+fieldName).find('li');
	    if(imgList.length == 0)
		imageListBlockMsg.show();
	}
	eval('this.'+fieldName+'.selected=0;');
	var maxImages = eval('this.'+fieldName+'.maxImages');
	var numImages = eval('--this.'+fieldName+'.numImages');
	if(numImages==maxImages - 1)
	    SetEnabled(fieldName+'_insertImageBtn');
    }
}

avForm.SetUploadError = function(message)
{
    $('#error').text('message');
}

avForm.SetUploadSuccess = function(data)
{
    if(opener)
	opener.avForm.AddImage(data.fieldName,data.docId,data.title,data.type);
    close();
 }


// usage terms
avForm.InitUsageTerms = function(selector)
{
    $(selector).find('div.usageTerms input[type=checkbox]').on('change',function(e){
	avForm.OnUsageTermsChange($(this));
    });
}

avForm.OnUsageTermsChange = function(field)
{
    if(field.attr('checked'))
	$('#'+field.data('id')).val(field.data('docid'));
    else
	$('#'+field.data('id')).val('');
}
