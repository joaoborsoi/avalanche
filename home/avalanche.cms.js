cms = new Object();

// metodos
cms.OnRegisterFormLoad = function(labels)
{
    avForm.labels = labels;
    avForm.OnLoad({
	selector: '#registerFormField',
	beforeSerialize: function($form, options){
	    return cms.SubmitRegister($form);
	}
    });

    if($('#userId').length > 0 && $('#userId').val() != '')
    {
	var button = $('<a class="btn" href="cmsChangeEmailForm.av">').text(labels.changeTag);
	avForm.SetDisabled('login').after(button);
	$('#loginField').addClass('input-append');
    }

    avForm.loadFieldPage = 'cmsRegisterFormField.av';
}

cms.SubmitRegister = function(e)
{
    // check if both passwords are equal
    var password = $('#password').val();
    var password2 = $('#password2').val();

    if(password != password2)
    {
	avForm.Alert('Senhas não iguais!');
	avForm.ClearRequiredError();
	avForm.SetRequiredError('password2');
	return false;
    }

    if(password != '')
    {
	var input = '<input name="password" type="hidden" value="'+password+'" />';
	$('#registerFormField').append(input);
    }
    return true;
}

cms.OnRegisterSuccess = function(message)
{
    avForm.OnSubmit();
    $('#registerDiv').text('').removeClass('fieldbox').append($("<p class='alert alert-success'>").text(message));
}

cms.OnRecoverPasswordSuccess = function(message)
{
    avForm.OnSubmit();
    $('#recoverDiv').text('').append($('<p class="alert alert-success">').text(message));
}

cms.OnChangeEmailFormLoad = function()
{
    $('#changeEmailFormField').ajaxForm({
	success:function(responseText)
	{
	    console.log(responseText);
	},
	beforeSubmit:function(arr, $form, options)
	{
	    return avForm.Submit($form);
	}});
}

cms.OnChangeEmailSuccess = function(msg)
{
    avForm.OnSubmit();
    $('#changeEmailDiv').html("<p class='alert alert-success'>"+msg+'</p>');
}

cms.OnContactPageLoad = function(labels)
{
    avForm.labels = labels;
    avForm.OnLoad({selector: '#contact-form'});
}

cms.OnContactSent = function(message)
{
    var msg = "<p class='alert alert-success'>"+message+"</p>";
    avForm.OnSubmit();
    var form = $('#contact-form');
    form[0].reset();
    form.after(msg);
    setTimeout(function(){
	form.next().fadeOut('slow');
    },3000);

}
