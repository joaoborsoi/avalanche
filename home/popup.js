browseFileDlg = null;
tinyMCEDlg = null
lastN = null;
formName='document.popupForm';

function trim(str){ 
  return String(str).replace(/^\s+|\s+$/g, '') 
};

function SwitchTab(n)
{
    var tab = n + 'Header';
    var win = n;

    document.getElementById( tab ).className='selected';
    document.getElementById( win ).style.display='block';
    if(lastN!=n && lastN!=null)
    {
	document.getElementById(lastN+'Header').className='fileTabs';
	document.getElementById(lastN).style.display='none';
    }
    lastN=n;
}

function SetRight(entity)
{
    entityRight = document.getElementById(entity);
    entityRight.value = (document.getElementById(entity+'_r').checked)?'r':'';
    entityRight.value += (document.getElementById(entity+'_w').checked)?'w':'';
}


function BrowseImages(fieldName)
{
    if(browseFileDlg) browseFileDlg.close();

    var url = 'index.php?pageId=adminBrowseFiles&tab=adminLib&returnFunction=AddImage';
    url += '&fieldName='+fieldName;
    browseFileDlg=OpenWin(url, 'browseFiles',720,450);
}

function EditImage(fieldName)
{
    var docId = eval(fieldName+'.selected');
    if(docId)
	OpenWin('?pageId=adminFileForm&tab=adminLib&docId='+
		docId+'&updateImage=1&imageListField='+fieldName,'file_'+
		docId,505,360);
}

function BrowseFiles(field_name, url, type, win)
{
    if(browseFileDlg) browseFileDlg.close();

    tinyMCEDlg = win;
    var url = '../../../../index.php?pageId=adminBrowseFiles&tab=adminLib&';
    url += 'returnFunction=AddFileLink&fieldName='+field_name;
    browseFileDlg=OpenWin(url, 'browseFiles',720,450);
    return false;
}

function AddFileLink(fieldName, docId, title, contentType)
{
    if(tinyMCEDlg)
    {
	//var url =/window.location.pathname;
	var url = 'images/'+docId;
	tinyMCEDlg.document.getElementById(fieldName).value = url;
    }
}

function OnUnloadPopup()
{
    if(browseFileDlg)
	browseFileDlg.close();
}

function TakeOwnership()
{
    if(!confirm(areYouSureMsg))
	return;

    var loc = 'index.php?pageId=adminTakeOwnership';
    var idField = document.getElementById('docId');
    if(idField)
	loc += '&docId='+idField.value;
    else
	loc += '&folderId='+document.getElementById('folderId').value;
    urlLoader.location = loc;
}

function OnFormSubmit()
{
    if(document.getElementById('tab').value == 'adminUser' &&
       document.getElementById('userId'))
    {
	if(document.getElementById('password').value !=
	   document.getElementById('password2').value)
	{
	    alert('As senhas não estão iguais!');
	    return false;
	}
	var password = document.getElementById('password').value;
	if(password != '')
	{
	    var input = document.createElement("input");
	    input.setAttribute("name","password");
	    input.setAttribute("type","hidden");
	    input.setAttribute("value",password);
	    document.popupForm.appendChild(input);
	}
    }
    return true;
}

function InitiateContent()
{
    document.getElementById('userRight_r').setAttribute('disabled','disabled');
    document.getElementById('userRight_w').setAttribute('disabled','disabled');
    SwitchTab('generalTab');
}

function InitiateFileContent(lang)
{
    // sets float formating parameters
    fracDigits = 2;
    if(lang=='en_US')
    {
	decimalSep = '.';
	thousandSep = ',';
    }
    else
    {
	decimalSep = ',';
	thousandSep = '.';
    }

    InitiateContent();

    selectedLanguages = new Array();
    // set initial language
    SetLanguage(lang);

    MakeDraggable();
}

function InitiateFolderContent(folderPath,lang)
{
    InitiateContent();
    if(folderPath)
	document.getElementById('folderPath').value = folderPath;

    if(document.getElementById('folderId').value != "")
	SetDisabled('folderTables');

    selectedLanguages = new Array();
    // set initial language
    SetLanguage(lang);

}

function SetLanguage(lang,event)
{
    // if ctrl not pressed, unselect all languages before
    if(event && event.ctrlKey!=1)
	while(selectedLanguages.length>0)
	{
	    var currLang = selectedLanguages.pop();
	    ToggleDisplay(currLang,'none');
	    document.getElementById(currLang).className='langTabs';
	}

    // checks if language is already selected
    for(var i=0; i<selectedLanguages.length; i++)
    {
	if(selectedLanguages[i] == lang)
	{
	    // unselect language
	    // do not unselect if it's the only language selected
	    if(selectedLanguages.length > 1)
	    {
		selectedLanguages.splice(i,1);
		document.getElementById(lang).className='langTabs';
		ToggleDisplay(lang,'none');
	    }
	    return;
	}
    }

    // if not selected, select the language
    selectedLanguages.push(lang);
    document.getElementById(lang).className='selected';
    ToggleDisplay(lang,'block');
}



function ToggleDisplay(theClass,display) 
{
    var element = document.getElementById('langCss');
    var sheet = element.sheet ? element.sheet : element.styleSheet;
    var imported = sheet.imports ? sheet.imports[0] : 
	sheet.cssRules[0].styleSheet;
    var rules = imported.rules ? imported.rules : imported.cssRules;

    for(var i = 0; i < rules.length; i++)
	if(rules[i].selectorText == '.'+theClass)
	    rules[i].style.display = display;
}


function InitiateUserContent(lang)
{
    // sets float formating parameters
    fracDigits = 2;
    if(lang=='en_US')
    {
	decimalSep = '.';
	thousandSep = ',';
    }
    else
    {
	decimalSep = ',';
	thousandSep = '.';
    }
    SetLoadFieldPage('adminRegisterFormField.av');
}

