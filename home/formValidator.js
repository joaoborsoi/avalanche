/*

* HTML:
class="required"
onsubmit="return checkform(this);"

* CSS:
.error
#errormsg

*/

//_lang='pt_BR'; // pt_BR

function checkform(of)
		{
		// Test if DOM is available
			if(!document.getElementById || !document.createTextNode){return;}
			//if(!document.getElementById('required')){return;}

		// Define error messages and split the required fields
			var errorID='errormsg';
			var errorClass='error';
			var errorImg='img/alert.gif';
			/*var errorMsg='Please enter or change the fields marked with a ';
			var errorAlt='Error';
			var errorTitle='This field has an error!';
			*/		
		// Set required fields
		var reqfields=Array();

		var inputs=of.getElementsByTagName('input');		
		for(var i=0;i<inputs.length;i++){
			if(inputs[i].className.substr(0,8) == 'required') reqfields[reqfields.length] = inputs[i].id;
		}	
		var txta=of.getElementsByTagName('textarea');		
		for(var i=0;i<txta.length;i++) {
			if(txta[i].className.substr(0,8) == 'required')	reqfields[reqfields.length] = txta[i].id;
		}
		var select=of.getElementsByTagName('select');		
		for(var i=0;i<select.length;i++) {
			if(select[i].className.substr(0,8) == 'required') reqfields[reqfields.length] = select[i].id;
		}
		//alert(reqfields);

		//var reqfields=document.getElementById('required').value.split(',');	

		// Cleanup old mess
			// if there is an old errormessage field, delete it
			if(document.getElementById(errorID))
			{
				var em=document.getElementById(errorID);
				em.parentNode.removeChild(em);
			}
			// remove old images and classes from the required fields
			for(var i=0;i<reqfields.length;i++)
			{
				var f=document.getElementById(reqfields[i]);
				if(!f){continue;}
				if(/img/i.test(f.parentNode.lastChild.nodeName))
				{
					f.parentNode.removeChild(f.parentNode.lastChild);
				}
				f.className='required';
			}
		// loop over required fields
			for(var i=0;i<reqfields.length;i++)
			{
		// check if required field is there
				var f=document.getElementById(reqfields[i]);
				if(!f){continue;}
		// test if the required field has an error, 
		// according to its type
				switch(f.type.toLowerCase())
				{
					case 'text':
						if(f.value=='' && f.id!='email'){cf_adderr(f)}							
		// email is a special field and needs checking
						if(f.id=='email' && !cf_isEmailAddr(f.value)){cf_adderr(f)}							
					break;
					case 'textarea':
						if(f.value==''){cf_adderr(f)}							
					break;
					case 'checkbox':
						if(!f.checked){cf_adderr(f)}							
					break;
					case 'select-one':
						if(!f.selectedIndex && f.selectedIndex==0){cf_adderr(f)}							
					break;
				}
			}
			return !document.getElementById(errorID);

			/* Tool methods */
			function cf_adderr(o)
			{
				// create image, add to and colourise the error fields
				var errorIndicator=document.createElement('img');
				errorIndicator.alt=errorAlt;
				errorIndicator.src=errorImg;
				errorIndicator.title=errorTitle;
				o.className+=' '+errorClass;
				//o.parentNode.appendChild(errorIndicator);
				o.parentNode.className+=(' block-'+errorClass);

			// Check if there is no error message
				if(!document.getElementById(errorID))
				{
				// create errormessage and insert before submit button
					var em=document.createElement('p');
					em.id=errorID;
					//var newp=document.createElement('p');
					em.appendChild(document.createTextNode(errorMsg))
					// clone and insert the error image
					//em.appendChild(errorIndicator.cloneNode(true));
					//em.appendChild(newp);
					// find the submit button 
					for(var i=0;i<of.getElementsByTagName('input').length;i++)
					{
						if(/submit/i.test(of.getElementsByTagName('input')[i].type))
						{
							var sb=of.getElementsByTagName('input')[i];
							break;
						}
					}
					if(sb)
					{
						sb.parentNode.insertBefore(em,sb);
					}	
				} 
			}
			function cf_isEmailAddr(str) 
			{		  //re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/;
			    return str.match(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/);
			}
		}
