pg = new Object();

pg.timeout = 5000;

pg.IsPhonegap = function()
{
    return document.location.protocol == "file:";
}

// params = { selector, url, onSuccess, onFail }
pg.Load = function(params)
{
    if(typeof(params.onFail) == 'function')
	params._onFail = params.onFail;

    params.onFail = function()
    {
	console.log('pg.onFail!');
	if(typeof(params.alertError) == 'undefined' ||
	   params.alertError)
	    alert(pg.labels.connectionError);

	// call parent function
	if(typeof(params._onFail) == 'function')
	    params._onFail(this);	    
    }
    var body = null;
    if(typeof(params.loading) == 'undefined' ||
       params.loading)
	body = $(document.body).addClass('ajax-loading');

    $.ajax({
        url: params.url,
        timeout: pg.timeout,
	complete: function() {
	    if(body != null)
		body.removeClass('ajax-loading');
	    if(typeof(params.onComplete)=='function')
		params.onComplete();
	},
        success: function(data) {
	    if(typeof(params.selector)!='undefined')
		$(params.selector).html(data);
	    if(typeof(params.element)!='undefined')
		params.element.html(data);

	    if(typeof(params.onSuccess)=='function')
		params.onSuccess(data);
        },
	error: function() {
	    if(typeof(params.onFail)=='function')
		params.onFail();
	}
    });
    
}
