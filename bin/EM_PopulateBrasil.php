<?php

// set base admin directory
$ADE = "/home/joao/work/ADE";
$adminDir = "$ADE/virtualhosts/visionario/admin/";
$avDir = "$ADE/lib/avalanche-dev/lib";
$chartDir = "$ADE/lib/ChartDirector/lib";

// set site ID
$SM_siteName    = "visionario";
$SM_siteID	= "visionario";

// PHP include dir
ini_set("include_path", ini_get("include_path") . ":$avDir:$chartDir");

require("AV_PageBuilder.inc");

// load in site settings through SM_siteManager
$SM_siteManager = new AV_PageBuilder();

$_GET['pageId'] = 'populateBrasil';

$SM_siteManager->Initiate($adminDir."config/SiteConfig.xsm");
$SM_siteManager->completePage();

?>
