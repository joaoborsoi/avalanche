ALTER TABLE UM_UserSessions
DROP KEY sessionId,
ADD INDEX (sessionId);
