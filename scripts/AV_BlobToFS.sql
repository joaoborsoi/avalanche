ALTER TABLE LIB_FileDocument
DROP content;

ALTER TABLE LIB_FileDocument
ADD content INTEGER UNSIGNED,
ADD FOREIGN KEY (content) REFERENCES LIB_BlobFS(id);

ALTER TABLE LIB_ImageThumbCache
DROP thumb;

ALTER TABLE LIB_ImageThumbCache
ADD thumb INTEGER UNSIGNED,
ADD FOREIGN KEY (thumb) REFERENCES LIB_BlobFS(id);

delimiter //
CREATE PROCEDURE OnFileDocumentNodeDelete(IN nodeId INTEGER UNSIGNED)
BEGIN
	DECLARE blobId INTEGER UNSIGNED;

	SELECT content INTO blobId 
	FROM LIB_FileDocument 
	WHERE docId=nodeId;

	IF blobId IS NOT NULL THEN
		UPDATE LIB_BlobFS SET deleted='1'
		WHERE id=blobId;

		UPDATE LIB_BlobFS SET deleted='1'
		WHERE id IN (SELECT thumb
		      	     FROM LIB_ImageThumbCache
			     WHERE docId=nodeId);
	END IF;
END //

CREATE TRIGGER OnNodeDelete BEFORE DELETE
ON LIB_Node FOR EACH ROW
BEGIN
   CALL OnFileDocumentNodeDelete(OLD.nodeId);
END //

CREATE TRIGGER OnImageThumbCacheDelete BEFORE DELETE
ON LIB_ImageThumbCache FOR EACH ROW
BEGIN
	UPDATE LIB_BlobFS SET deleted='1'
	WHERE id = OLD.thumb;
END //
delimiter ;
