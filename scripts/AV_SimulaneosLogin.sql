ALTER TABLE UM_UserSessions
DROP FOREIGN KEY UM_UserSessions_ibfk_1,
DROP KEY userId;

ALTER TABLE UM_UserSessions
ADD UNIQUE (userId,sessionId),
ADD FOREIGN KEY (userId) REFERENCES UM_User (userId) ON DELETE CASCADE;


