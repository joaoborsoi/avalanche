DELETE FROM LIB_Node
WHERE nodeId IN (
  SELECT docId 
  FROM LIB_DocumentLink dl, LIB_Folder f 
  WHERE path='/files/' AND f.folderId=dl.folderId 
  AND dl.docId NOT IN (
    SELECT fileId 
    FROM LIB_ContentFile
    ) 
  )
AND creationDate < DATE_SUB(NOW(),INTERVAL 1 DAY);

DELETE FROM LIB_Node
WHERE nodeId IN (
  SELECT docId 
  FROM LIB_DocumentLink dl, LIB_Folder f 
  WHERE path='/userImages/' AND f.folderId=dl.folderId 
  AND dl.docId NOT IN (
    SELECT fileId 
    FROM LIB_UserFile
    ) 
  )
AND creationDate < DATE_SUB(NOW(),INTERVAL 1 DAY);

COMMIT;
