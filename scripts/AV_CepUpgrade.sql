RENAME TABLE cidade TO localidade;

CREATE TABLE cidade (
       cidade_codigo INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
       cidade_descricao VARCHAR(72) NOT NULL,
       uf_codigo INT(11) NOT NULL,
       KEY (uf_codigo)
) ENGINE=InnoDB;

INSERT INTO cidade (cidade_descricao, uf_codigo)
SELECT DISTINCT cidade_descricao, uf_codigo
FROM localidade;

ALTER TABLE localidade
DROP PRIMARY KEY,
CHANGE cidade_codigo localidade_codigo INT(11) NOT NULL,
ADD PRIMARY KEY(localidade_codigo),
CHANGE cidade_cep localidade_cep VARCHAR(8) NOT NULL,
ADD cidade_codigo INT(11) NOT NULL;

UPDATE localidade
SET cidade_codigo=(
    SELECT cidade_codigo FROM cidade
    WHERE cidade.cidade_descricao = localidade.cidade_descricao
    AND cidade.uf_codigo=localidade.uf_codigo
);

ALTER TABLE localidade
DROP cidade_descricao;

ALTER TABLE bairro
CHANGE cidade_codigo localidade_codigo INT(11) NOT NULL;
