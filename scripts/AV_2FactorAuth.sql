INSERT INTO LANG_Label 
(labelId, lang, value) VALUES
('authSecretTag','pt_BR','Código QR para autenticação'),
('authSecretTag','en_US','QR Code for authentication');

INSERT INTO FORM_Field
(tableName,fieldName,labelId,type,size,attributes,orderby,consistType,required,allowNull,trimField,uniq,keyField,accessMode,staticProp,template,langTemplate,outputFuncRef,`maxValue`,minValue) VALUES
('UM_User','authSecret','authSecretTag','authSecret',16,NULL,1020,NULL,'0','1','1','0','0','rw','1',NULL,'0',NULL,NULL,NULL);

ALTER TABLE UM_User ADD authSecret CHAR(16);

CREATE TABLE UM_UsedAuthSecret
(
	id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	userId INTEGER UNSIGNED NOT NULL,
	authCode INTEGER UNSIGNED NOT NULL,
	UNIQUE(userId,authCode),
	FOREIGN KEY (userId) REFERENCES UM_User(userId) ON DELETE CASCADE
) Engine=InnoDB;
