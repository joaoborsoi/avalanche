SET NAMES UTF8;

DROP TABLE EM_Entity;

UPDATE FORM_Field set type='readOnlySelect' where tableName='LIB_Node' and fieldName='userId';

INSERT INTO LANG_Label
(labelId, lang, value) VALUES
('selectMultMaxLengthHtml','pt_BR','Selecionados'),
('selectMultMaxLengthHtml','en_US','Selected'),
('selectMultPlaceholder','pt_BR','Escolha entre os seguintes...'),
('selectMultPlaceholder','en_US','Choose among the following...'),
('changePasswordTag','pt_BR','Trocar senha'),
('changePasswordTag','en_US','Change password'),
('repeatPasswordTag','pt_BR','Repita a senha'),
('repeatPasswordTag','en_US','Repeat the password'),
('generatePasswordTag','pt_BR','Gerar senha'),
('generatePasswordTag','en_US','Generate password'),
('generatedPasswordTag','pt_BR','Senha gerada'),
('generatedPasswordTag','en_US','Generated password'),
('usePasswordTag','pt_BR','Usar senha'),
('usePasswordTag','en_US','Use password'),
('copiedPasswordTag','pt_BR','Copiei a senha em local seguro'),
('copiedPasswordTag','en_US','Copied password to a safe place'),
('addressAlreadyRegTag','pt_BR','Endereço já existente'),
('addressAlreadyRegTag','en_US','Address already registered'),
('lowerThanTag','pt_BR','Menor que'),
('lowerThanTag','en_US','Lower than'),
('betweenTag','pt_BR','Entre'),
('betweenTag','en_US','Between'),
('greaterThanTag','pt_BR','Maior que'),
('greaterThanTag','en_US','Greater than'),
('beforeThanTag','pt_BR','Antes que'),
('beforeThanTag','en_US','Before than'),
('afterThanTag','pt_BR','Depois que'),
('afterThanTag','en_US','After than');


INSERT INTO LANG_PartLabels
(part,labelId) VALUES
('form','selectMultMaxLengthHtml'),
('form','selectMultPlaceholder'),
('form','changePasswordTag'),
('form','repeatPasswordTag'),
('form','generatePasswordTag'),
('form','generatedPasswordTag'),
('form','usePasswordTag'),
('form','copiedPasswordTag'),
('form','addressAlreadyRegTag'),
('form','addTag'),
('form','lowerThanTag'),
('form','betweenTag'),
('form','greaterThanTag'),
('form','beforeThanTag'),
('form','afterThanTag'),
('cms','cmsContactSentTag');


ALTER TABLE LIB_NodeGroupRights
ADD FOREIGN KEY (groupId) REFERENCES UM_Group (groupId) ON DELETE CASCADE;


CREATE TABLE FORM_EmailListField (
       tableName VARCHAR(255),
       fieldName VARCHAR(50),
       PRIMARY KEY (tableName,fieldName),
       FOREIGN KEY (tableName,fieldName) REFERENCES FORM_Field(tableName,fieldName),
       listTable VARCHAR(255)
) ENGINE=InnoDB;

CREATE TABLE LIB_DocEmailList (
	docId INTEGER UNSIGNED NOT NULL,
	email VARCHAR(255) NOT NULL,
	idx INTEGER UNSIGNED NOT NULL,
	PRIMARY KEY(docId,email),
	UNIQUE (docId,idx),
	FOREIGN KEY (docId) REFERENCES LIB_Document (docId) ON DELETE CASCADE
) ENGINE=InnoDB;
	
#
# Etiquetas
#
ALTER TABLE L2PDF_Labels
ADD docId INTEGER UNSIGNED;
SET @nodeId = (SELECT MAX(nodeId) FROM LIB_Node);
SET @nextId = @nodeId;

INSERT INTO LIB_Node (nodeId,userId,groupId,userRight,groupRight,otherRight,creationDate,lastChanged,lastChangedUserId)
SELECT @nodeId:=@nodeId+1, 1, 8, 'rw', 'rw', '', NOW(), NOW(), 1
FROM L2PDF_Labels;

UPDATE L2PDF_Labels SET docId=@nextId:=@nextId+1;

INSERT INTO LIB_Document (docId) SELECT docId FROM L2PDF_Labels;

ALTER TABLE L2PDF_Labels
DROP PRIMARY KEY,
DROP id,
MODIFY docId INTEGER UNSIGNED NOT NULL,
ADD PRIMARY KEY (docId),
ADD FOREIGN KEY (docId) REFERENCES LIB_Document (docId) ON DELETE CASCADE;

INSERT INTO FORM_Field
(tableName,fieldName,labelId,type,size,attributes,orderby,consistType,required,allowNull,trimField,uniq,keyField,accessMode,staticProp,template,langTemplate,outputFuncRef,`maxValue`,minValue) VALUES
('L2PDF_Labels','docId',NULL,'dummy',NULL,NULL,0,'integer','0','1','1','1','1','rw','1',NULL,'0',NULL,NULL,NULL);

UPDATE FORM_SelectField SET optionTableKey='docId' WHERE tableName='L2PDF_LabelsSearchFields' and fieldName='id';

UPDATE FORM_OptionTable SET keyField='docId' WHERE tableName='L2PDF_Labels';

DELETE FROM FORM_Field WHERE tableName='L2PDF_Labels' and fieldName='id';

INSERT INTO LIB_FolderType
(id, name,contentModule) VALUES
(27,NULL,NULL);
INSERT INTO LIB_FolderTables 
(id, tableName, printOrder) VALUES
(27, 'LIB_Node', 1),
(27, 'LIB_Document', 2),
(27, 'L2PDF_Labels', 3);
INSERT INTO LIB_DocReturnFields
(id,tableName,fieldName,printOrder) VALUES
(27,'L2PDF_Labels','name',1);
INSERT INTO LIB_Node 
(nodeId,userId,groupId,userRight,groupRight,otherRight,creationDate,lastChanged,lastChangedUserId) VALUES
(NULL,1,8,'rw','rw','',NOW(),NOW(),1);
INSERT INTO LIB_Folder
(folderId,level,path,fixedOrder,folderTables,restrictedDeletion,title,content,contentType,menuTitle,defGroupId,defUserRight,defGroupRight,defOtherRight,descr) VALUES
(LAST_INSERT_ID(),2,'/labels/',1,27,0,NULL,NULL,NULL,NULL,8,'rw','rw','',NULL);
INSERT INTO LIB_NodeGroupRights 
(nodeId,groupId,groupRight) VALUES
(LAST_INSERT_ID(),22,'r');
delimiter //
CREATE TRIGGER OnLabelsInsert AFTER INSERT
ON L2PDF_Labels FOR EACH ROW
BEGIN
	INSERT INTO LIB_NodeGroupRights 
	(id,nodeId,groupId,groupRight) VALUES
	(NULL,NEW.docId,22,'r');
END //
delimiter ;


ALTER TABLE LIB_FolderType ADD views TEXT;


UPDATE FORM_Field SET size=255 WHERE type='langLabel';
UPDATE FORM_Field SET size=65536 WHERE type='langHtmlArea';

UPDATE FORM_Field SET template=NULL WHERE tableName='WIKI_Document' AND fieldName='imageList';

UPDATE FORM_Field SET attributes=NULL;

UPDATE LANG_Label SET value = 'Print' WHERE labelId='printTag' AND lang='en_US';

INSERT INTO FORM_Field
(tableName,fieldName,labelId,type,size,attributes,orderby,consistType,required,allowNull,trimField,uniq,keyField,accessMode,staticProp,template,langTemplate,outputFuncRef,`maxValue`,minValue) VALUES
('CMS_Contact','validator','validatorTag','text',NULL,NULL,50,NULL,'0','1','1','1','1','rw','1','captcha','0',NULL,NULL,NULL);

UPDATE FORM_Field SET template='email' WHERE tableName='CMS_Contact' AND fieldName='email';
UPDATE FORM_Field SET orderby=15 WHERE tableName='CMS_Contact' AND fieldName='name';
