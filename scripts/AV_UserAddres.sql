ALTER TABLE UM_User
ADD zipcode VARCHAR(50) NOT NULL,
ADD uf_codigo TINYINT,
ADD INDEX(uf_codigo),
ADD FOREIGN KEY (uf_codigo) REFERENCES uf(uf_codigo),
ADD cidade_codigo int(11),
ADD INDEX(cidade_codigo),
ADD FOREIGN KEY (cidade_codigo) REFERENCES cidade(cidade_codigo),
ADD bairro VARCHAR(50),
ADD logradouro VARCHAR(50),
ADD number varchar(10) default NULL,
ADD numberAux varchar(10) default NULL,
ADD phone VARCHAR(50) NOT NULL;

INSERT INTO FORM_Field
(tableName,fieldName,labelId,type,size,attributes,orderby,consistType,required,allowNull,trimField,uniq,keyField,accessMode,staticProp,template,langTemplate,outputFuncRef) VALUES
('UM_User','zipcode','zipcodeTag','cep',15,NULL,80,NULL,'1','0','1','0','0','rw','1',NULL,'0', NULL),
('UM_User','uf_codigo','stateEMTag','select',NULL,NULL,90,'integer','1','0','1','0','0','rw','1',NULL,'0',NULL),
('UM_User','cidade_codigo','cityEMTag','select',NULL,NULL,100,'integer','1','0','1','0','0','rw','1',NULL,'0',NULL),
('UM_User','bairro','quarterEMTag','text',50,'style=\'width:300px\'',105,NULL,'0','1','1','0','0','rw','1',NULL,'0',NULL),
('UM_User','logradouro','streetEMTag','text',50,'style=\'width:300px\'',110,NULL,'1','0','1','0','0','rw','1',NULL,'0',NULL),
('UM_User','number','numberEMTag','text',10,'style=\'width:100px\'',115,NULL,'1','0','1','0','0','rw','1',NULL,'0',NULL),
('UM_User','numberAux','numberAuxEMTag','text',10,'style=\'width:100px\'',116,NULL,'0','1','1','0','0','rw','1',NULL,'0',NULL),
('UM_User','phone','telephoneEMTag','text',50,NULL,120,NULL,'1','0','1','0','0','rw','1',NULL,'0',NULL);

INSERT INTO FORM_SelectField
(tableName,fieldName,optionTable,optionTableKey,optionTableValue,selectedTable,sourceField,targetField,orderField,ascOrder) VALUES
('UM_User','uf_codigo','uf','uf_codigo','uf_descricao',NULL,NULL,'cidade_codigo','uf_descricao',1),
('UM_User','cidade_codigo','cidade','cidade_codigo','cidade_descricao',NULL,'uf_codigo',NULL,'cidade_descricao',1);
