<?php

// vers�o do avalanche
$avVersion = '%avVersion%';

// set base admin directory
$siteDir = realpath("../");
$adminDir = realpath("../admin");
$libDir = realpath("../../..")."/lib";
$avDir = $libDir."/avalanche-$avVersion";
$SMDir = $libDir.'/siteManager';
$pearDir = $libDir.'/pear';
$siteLibDir = realpath("../admin/lib");

// PHP include dir
ini_set("include_path","$SMDir:$pearDir:$siteLibDir:$avDir/lib");

// set site ID
if(isset($_REQUEST['pageId']) && 
   substr($_REQUEST['pageId'],0,strlen('admin'))=='admin')
{
  $SM_siteName    = "admin";
  $SM_siteID	= "admin";
}
else
{
  $SM_siteName    = "%siteName%";
  $SM_siteID	= "%siteName%";
}

require_once("AV_PageBuilder.inc");

// load in site settings through SM_siteManager
$SM_siteManager = new AV_PageBuilder();
$SM_siteManager->Initiate($adminDir."/config/%siteConfig%", $avVersion);

$SM_siteManager->completePage();

?>
